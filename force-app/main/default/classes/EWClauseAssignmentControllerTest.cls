/**
 * Created by roystonlloyd on 2020-01-20.
 */
@isTest
public with sharing class EWClauseAssignmentControllerTest {

    static List<Quote> quotes;
    static List<vlocity_ins__DocumentClause__c> documentClauses;
    static Account insured;
    static Account broker;


    private static testMethod void testLoadRecordData(){
        setup(1,2);
        EW_ClauseAssignment__c clauseAssignment = EWClauseAssignmentDataFactory.createClauseAssignment(documentClauses[0].Id, quotes[0].Id, null, true);
        Map<String, Object> recordData;

        Test.startTest();
        recordData = EWClauseAssignmentController.loadRecordData(null);
        System.assertNotEquals(null, recordData);
        recordData = EWClauseAssignmentController.loadRecordData(quotes[0].Id);
        Test.stopTest();

        System.assertEquals(true, recordData.containsKey('options'));
        System.assertEquals(true, recordData.containsKey('values'));

        List<EWClauseAssignmentController.Clause> returnedOptions = (List<EWClauseAssignmentController.Clause>) recordData.get('options');
        List<String> returnedSelectedValues = (List<String>) recordData.get('values');

        System.assertEquals(2, returnedOptions.size());
        System.assertEquals(true, returnedOptions[1].label.contains(documentClauses[1].Name));
        System.assertEquals(documentClauses[1].Id, returnedOptions[1].value);
        System.assertEquals(documentClauses[0].Id, returnedSelectedValues[0]);

    }

    private static testMethod void testSelectingAndDeslectingClauses(){
        // tests saveSelectedAssignments
        // test scenario:
        //   2x standard clauses (1 selected, 1 deselected),
        //   3x restricted clauses (1 selected new, 1 selected was inactive, 1 deselected).

        setup(1,5);

        Id testQuoteId = quotes[0].Id;
        List<Id> standardClauseIds = new List<Id>{documentClauses[0].Id, documentClauses[1].Id};
        List<Id> restrictedClauseIds = new List<Id>{documentClauses[2].Id, documentClauses[3].Id, documentClauses[4].Id};

        // update clauses to restricted
        List<vlocity_ins__DocumentClause__c> restrictedClauses = new List<vlocity_ins__DocumentClause__c>();
        for(Id id : restrictedClauseIds) {
            restrictedClauses.add(new vlocity_ins__DocumentClause__c(
                    Id = id,
                    vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED
            ));
        }
        update restrictedClauses;

        // set the selected / deselected clauses
        List<Id> testSelectedClauseIds = new List<Id>{standardClauseIds[0], restrictedClauseIds[0], restrictedClauseIds[1]};
        List<Id> testDeselectedClauseIds = new List<Id>{standardClauseIds[1], restrictedClauseIds[2]};

        // set pre-existing assignments
        List<EW_ClauseAssignment__c> preAssigned = new List<EW_ClauseAssignment__c>();
        preAssigned.add(new EW_ClauseAssignment__c(EW_Quote__c = testQuoteId,
                EW_DocumentClause__c = restrictedClauseIds[1],  EW_Active__c = false));
        preAssigned.add(new EW_ClauseAssignment__c(EW_Quote__c = testQuoteId,
                EW_DocumentClause__c = restrictedClauseIds[2],  EW_Active__c = true));
        preAssigned.add(new EW_ClauseAssignment__c(EW_Quote__c = testQuoteId,
                EW_DocumentClause__c = standardClauseIds[1],  EW_Active__c = true));
        insert preAssigned;

        // confirm pre-test condition
        Integer assignedClauseCount = [SELECT Count() FROM EW_ClauseAssignment__c WHERE EW_Quote__c =: testQuoteId AND EW_Active__c = true];
        System.assertEquals(2,assignedClauseCount);

        // test prep
        Map<String,Object> inputMap = new Map<String,Object>();
        inputMap.put('recordId', testQuoteId);
        inputMap.put('selectedValues', testSelectedClauseIds);

        Test.startTest();
        Boolean isSuccess = EWClauseAssignmentController.saveSelectedAssignments(inputMap);
        System.assertEquals(true, isSuccess);
        Test.stopTest();

        Set<Id> assignedClauseIds = new Set<Id>();
        List<EW_ClauseAssignment__c> assignedClauses = [SELECT Id,EW_DocumentClause__c,EW_Active__c FROM EW_ClauseAssignment__c WHERE EW_Quote__c =: testQuoteId AND EW_Active__c = true];
        for(EW_ClauseAssignment__c ac: assignedClauses){
            assignedClauseIds.add(ac.EW_DocumentClause__c);
        }

        // confirm we only got what we chose
        for(Id id : testSelectedClauseIds){
            System.assertEquals(true, assignedClauseIds.contains(id));
        }
        for(Id id : testDeselectedClauseIds){
            System.assertEquals(false, assignedClauseIds.contains(id));
        }

    }

    private static testMethod void testSaveNewClause(){

        setup(1,2);
        Id testQuoteId = quotes[0].Id;

        // test prep
        // set the background information
        Map<String,Object> clauseOptionsObj = new Map<String,Object>();
        clauseOptionsObj.put('available', EWClauseAssignmentController.getAvailableClauses(testQuoteId));
        clauseOptionsObj.put('selected', new List<String>());
        EWClauseAssignmentController.ClauseOptions co = (EWClauseAssignmentController.ClauseOptions) JSON.deserialize(JSON.serialize(clauseOptionsObj), EWClauseAssignmentController.ClauseOptions.class);

        // set the input values
        Map<String,Object> newClauseObj = new Map<String,Object>();
        newClauseObj.put('name','testClauseName');
        newClauseObj.put('content','testClauseContent');
        EWClauseAssignmentController.NewClause nc = (EWClauseAssignmentController.NewClause) JSON.deserialize(JSON.serialize(newClauseObj), EWClauseAssignmentController.NewClause.class);

        // set the object for passing to controller
        Map<String,Object> clauseAssignmentObj = new Map<String,Object>();
        clauseAssignmentObj.put('inputValues', nc);
        clauseAssignmentObj.put('options', co);
        clauseAssignmentObj.put('recordId', testQuoteId);
        Map<String, Object> caMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(clauseAssignmentObj));

        Test.startTest();
        Map<String, Object> response = EWClauseAssignmentController.saveNewClause(caMap);
        System.assertEquals(true, (Boolean) response.get('newClauseSaved'));
        System.assertEquals(3, ((List<EWClauseAssignmentController.Clause>) response.get('options')).size());
        System.assertEquals(1, ((List<String>) response.get('values')).size());
        Test.stopTest();

    }



    private static void setup(Integer noOfQuotes, Integer noOfClauses){

        broker = EWAccountDataFactory.createBrokerAccount(null, true);
        insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

        quotes = new List<Quote>();
        for(Integer i=0 ; i<noOfQuotes ; i++){
            quotes.add(EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, false));
        }
        insert quotes;

        documentClauses = new List<vlocity_ins__DocumentClause__c>();
        for(Integer i=0 ; i<noOfClauses ; i++){
            vlocity_ins__DocumentClause__c dc = EWDocumentClauseDataFactory.createDocumentClause('Name'+i, null, true, false);
            dc.vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD;
            documentClauses.add(dc);
        }
        insert documentClauses;

    }

}