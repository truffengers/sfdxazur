({
    doInit : function(component, event, helper) {
        var svgns = "http://www.w3.org/2000/svg";
        var xlinkns = "http://www.w3.org/1999/xlink";
        var name = component.get("v.name");
        var classname = component.get("v.class");
        
        var svgroot = document.createElementNS(svgns, "svg");
        svgroot.setAttribute("name", name);
        svgroot.setAttribute("class", classname);
        
        var svgAttributes = JSON.parse(component.get("v.svgAttributes"));
        
        for(thisAttribute in svgAttributes) {
            svgroot.setAttribute(thisAttribute, svgAttributes[thisAttribute]);
        }
        
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "href", component.get("v.svgPath"));
        shape.setAttribute("class", component.get("v.useClass"));
        svgroot.appendChild(shape);
        var html = svgroot.outerHTML;
        // Firefox, Safari root NS issue fix
        html = html.replace('xlink=', 'xmlns:xlink=');
        // Safari xlink NS issue fix
        html = html.replace(/NS\d+:href/gi, 'xlink:href');
        component.set("v.htmlString", html);
    }
})