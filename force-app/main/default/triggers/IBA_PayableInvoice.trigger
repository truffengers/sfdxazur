/**
* @name: IBA_PayableInvoice
* @description: Trigger for the managed object c2g__codaPurchaseInvoice__c
*
*/

trigger IBA_PayableInvoice on c2g__codaPurchaseInvoice__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_PayableInvoiceTriggerHandler());
}