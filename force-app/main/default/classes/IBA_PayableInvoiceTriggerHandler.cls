/**
* @name: IBA_PayableInvoiceTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaPurchaseInvoice__c trigger functionality
*
*/

public with sharing class IBA_PayableInvoiceTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_PayableInvoiceTriggerHandler.IsDisabled != null ? IBA_PayableInvoiceTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, c2g__codaPurchaseInvoice__c> policyTransactionMap = new Map<Id, c2g__codaPurchaseInvoice__c>();
        for(Id recordId :newItems.keySet()) {
            c2g__codaPurchaseInvoice__c payableInvoice = (c2g__codaPurchaseInvoice__c)newItems.get(recordId);
            c2g__codaPurchaseInvoice__c payableInvoiceOld = (c2g__codaPurchaseInvoice__c)oldItems.get(recordId);
            if(payableInvoice.c2g__InvoiceStatus__c == IBA_UtilConst.PAYABLE_INVOICE_STATUS_COMPLETE 
                && payableInvoiceOld.c2g__InvoiceStatus__c != IBA_UtilConst.PAYABLE_INVOICE_STATUS_COMPLETE) {
                    policyTransactionMap.put(payableInvoice.IBA_PolicyTransaction__c, payableInvoice);
            }
        }
        if(policyTransactionMap.size() > 0) {
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(policyTransactionMap.keySet());
            transferData.addPayableInvoiceTotal(policyTransactionMap)
                .transfer();
        }

    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}