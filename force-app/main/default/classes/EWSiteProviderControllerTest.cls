@IsTest
private class EWSiteProviderControllerTest {
    @isTest
    public static void itShouldReturnSiteMetadata() {
        Test.startTest();

        system.assertEquals(null, EWSiteProviderController.getSiteMetadata().adminEmail);
        system.assertNotEquals(null, EWSiteProviderController.getSiteMetadata().pathPrefix);
        system.assertNotEquals(null, EWSiteProviderController.getSiteMetadata().isSandbox);

        Test.stopTest();
    }
}