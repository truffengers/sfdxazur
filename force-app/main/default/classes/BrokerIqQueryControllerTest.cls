/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 19/10/2017
 * @lastChangedBy: Antigoni D'Mello 08/11/2018 EWH-153 Overwritten the call to getAccount() due to DR Code Validation
 */

@isTest
private class BrokerIqQueryControllerTest {

    private static TestObjectGenerator tog = new TestObjectGenerator();

    @isTest static void testGetSingleRecord() {
        Account acc = TestDataFactory.createBrokerageAccount('BrokerIQ Query test account');
        insert acc;

        Account result = (Account)BrokerIqQueryController.getSingleRecord('SELECT Id FROM Account');

        System.assertEquals(acc.Id, result.Id);
    }

    @isTest static void testGetSingleRecordAllFields() {
        IQ_Content__c iqc = tog.getWebcastIqContent();

        IQ_Content__c result = (IQ_Content__c)BrokerIqQueryController.getSingleRecordAllFields('IQ_Content__c', iqc.Id);

        System.assertEquals(iqc.Id, result.Id);
    }

    @isTest static void testGetRecordsFieldSet() {
        Account acc = TestDataFactory.createBrokerageAccount('BrokerIQ Query test account');
        insert acc;

        List<Account> result = BrokerIqQueryController.getRecordsFieldSet(
                'Account',
                'Nebula_Tools__FieldSetUtilsTest',
                'Id != null'
        );

        System.assertEquals(1, result.size());
        System.assertEquals(acc.Id, result[0].Id);
    }

    @isTest static void testGetRecordsAllFields() {
        IQ_Content__c iqc = tog.getWebcastIqContent();

        List<IQ_Content__c> result = BrokerIqQueryController.getRecordsAllFields(
                'IQ_Content__c',
                'Id != null'
        );

        System.assertEquals(1, result.size());
        System.assertEquals(iqc.Id, result[0].Id);
    }
}