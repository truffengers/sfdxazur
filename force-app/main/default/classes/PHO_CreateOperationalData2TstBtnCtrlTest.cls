@IsTest
private class PHO_CreateOperationalData2TstBtnCtrlTest {
    @TestSetup
    private static void createTestData() {
        IBA_IBACustomSettings__c settings = new IBA_IBACustomSettings__c (PHO_PhoenixStagingRecordsLimit__c = 1);
        insert settings;

        PHO_PhoenixStaging__c stagingRecord = new PHO_PhoenixStaging__c();
        insert stagingRecord;
    }

    @IsTest
    private static void testCreateOperationalDataForSelectedRecordsSuccess() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = [SELECT Id FROM PHO_PhoenixStaging__c];

        // Perform test
        Test.startTest();
        Test.setCurrentPage(Page.PHO_CreateOperationalData2TestButton);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(stagingRecords);
        PHO_CreateOperationalData2TestButtonCtrl controller = new PHO_CreateOperationalData2TestButtonCtrl(standardSetController);
        PageReference result = controller.createOperationalDataForSelectedRecords();
        Test.stopTest();

        // Verify results
        Integer jobsQueued = [SELECT COUNT() FROM AsyncApexJob WHERE ApexClass.Name = :PHO_CreateOperationalData2Job.class.getName()];

        System.assertEquals(standardSetController.cancel().getUrl(), result.getUrl(), 'Should redirect to the original page');
        System.assertEquals(1, jobsQueued, 'One job for PHO_CreateOperationalData2Job class should be enqueued');
    }

    @IsTest
    private static void testCreateOperationalDataForSelectedRecordsErrorMessage() {
        // Prepare data
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        settings.PHO_PhoenixStagingRecordsLimit__c = 0;
        update settings;
        List<PHO_PhoenixStaging__c> stagingRecords = [SELECT Id FROM PHO_PhoenixStaging__c];

        // Perform test
        Test.startTest();
        Test.setCurrentPage(Page.PHO_CreateOperationalData2TestButton);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(stagingRecords);
        PHO_CreateOperationalData2TestButtonCtrl controller = new PHO_CreateOperationalData2TestButtonCtrl(standardSetController);
        PageReference result = controller.createOperationalDataForSelectedRecords();
        Test.stopTest();

        // Verify results
        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertEquals(null, result, 'Should not redirect');
        System.assertEquals(1, pageMessages.size(), 'There should be one error message on the page');
        System.assertEquals(ApexPages.Severity.ERROR, pageMessages[0].getSeverity(), 'There should be one error message on the page');
    }

    @IsTest
    private static void testGoBack() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = [SELECT Id FROM PHO_PhoenixStaging__c];

        // Perform test
        Test.startTest();
        Test.setCurrentPage(Page.PHO_CreateOperationalData2TestButton);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(stagingRecords);
        PHO_CreateOperationalData2TestButtonCtrl controller = new PHO_CreateOperationalData2TestButtonCtrl(standardSetController);
        PageReference result = controller.goBack();
        Test.stopTest();

        // Verify results
        System.assertEquals(standardSetController.cancel().getUrl(), result.getUrl(), 'Should redirect to the original page');
    }
}