@IsTest
public class EWCaseTriggerHelperTest {
    
    private static Account broker;
    private static User systemAdminUser, brokerUser,brokerUser2;
    
    private static testMethod void testCasewithNoBroker() {
		
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        
        String CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EWConstants.CASE_RECORD_TYPE_COMMUNITY).getRecordTypeId();
		
        System.runAs(systemAdminUser) {
            test.startTest();
            Case newCase = new Case(Subject='Test Case',Origin='Email',Status='New',RecordtypeId=CaseRecordTypeId, SuppliedEmail = 'test@brokerEmail.com');
            insert newCase;  
            test.stopTest();
             
            Case testCase = [Select id,type,accountId from Case where id = :newCase.Id];
            system.assertEquals( testCase.Type,'SmartHome Support');
          	system.assert(testCase.AccountId == null);
        }   
    }
    private static testMethod void testCasewithSingleBroker() {
		Contact brokerContact, brokerContact2;
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        System.runAs(systemAdminUser) {
            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            brokerContact = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
         }

        Profile brokerProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_EW_PARTNER_NAME);
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);
        
        String CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EWConstants.CASE_RECORD_TYPE_COMMUNITY).getRecordTypeId();
		
        System.runAs(systemAdminUser) {
            test.startTest();
            Case newCase = new Case(Subject='Test Case',Origin='Email',Status='New',RecordtypeId=CaseRecordTypeId, SuppliedEmail = brokerUser.Email);
            insert newCase;  
            test.stopTest();
             
            Case testCase = [Select id,type,accountId from Case where id = :newCase.Id];
            system.assertEquals( testCase.Type,'SmartHome Support');
          	system.assert(testCase.AccountId != null);
        }   
    }
    
     private static testMethod void testCasewithTwoBroker() {
		Contact brokerContact, brokerContact2;
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        System.runAs(systemAdminUser) {
            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            brokerContact = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
         }

        Profile brokerProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_EW_PARTNER_NAME);
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);
         
        brokerUser2 = EWUserDataFactory.createPartnerUser('testBroker34', brokerProfile.Id, brokerContact2.Id, true);
        brokerUser2.Email = brokerUser.Email;
        update brokerUser2;
        String CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EWConstants.CASE_RECORD_TYPE_COMMUNITY).getRecordTypeId();
		
        System.runAs(systemAdminUser) {
            test.startTest();
            Case newCase = new Case(Subject='Test Case',Origin='Email',Status='New',RecordtypeId=CaseRecordTypeId, SuppliedEmail = brokerUser.Email);
            insert newCase;  
            test.stopTest();
             
            Case testCase = [Select id,type,accountId from Case where id = :newCase.Id];
            system.assertEquals( testCase.Type,'SmartHome Support');
            system.assert(testCase.AccountId == null);
        }   
    }
  
}