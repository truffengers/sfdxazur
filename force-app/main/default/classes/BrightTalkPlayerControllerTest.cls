@isTest
private class BrightTalkPlayerControllerTest {
	
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings;
    
    static {
        settings = tog.getBrightTalkApiSettings();
    }

	@isTest static void noUserContact() {
		PageReference pageRef = Page.BrokerIqWebinars;
		Map<String, String> params = pageRef.getParameters();

		Test.setCurrentPage(pageRef);

		BrightTalkPlayerController controller = new BrightTalkPlayerController();

		List<ApexPages.Message> messages = ApexPages.getMessages();

		// TODO Figure out how to replace the apex message functionality in the PortalController class
		/*System.assertEquals(1, messages.size());
		System.assertEquals(Label.Not_a_portal_user_error, messages[0].getDetail());*/
	}

	@isTest static void basic() {
		tog.getUserContact();
		PageReference pageRef = Page.BrokerIqWebinars;
		Map<String, String> params = pageRef.getParameters();

		Test.setCurrentPage(pageRef);

		BrightTalkPlayerController controller = new BrightTalkPlayerController();

		List<ApexPages.Message> messages = ApexPages.getMessages();
		
		System.assertEquals(0, messages.size(), messages);

		System.assertEquals(settings.Endpoint__c,  controller.brightTalkEndpoint);
		System.assertNotEquals(null,  controller.realmToken);
		System.assertEquals(settings.Channel_Id__c,  Decimal.valueOf(controller.channelId));
	}

	@isTest static void getInstance() {
		tog.getUserContact();

		BrightTalkPlayerController controller = BrightTalkPlayerController.getInstance();

		System.assertEquals(settings.Endpoint__c,  controller.brightTalkEndpoint);
		System.assertNotEquals(null,  controller.realmToken);
		System.assertEquals(settings.Channel_Id__c,  Decimal.valueOf(controller.channelId));
	}

}