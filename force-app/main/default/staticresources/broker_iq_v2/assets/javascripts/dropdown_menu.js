TT.dropdownMenu = (function($) {

  'use strict';

  var currentIndex;
  var items;
  var $items;
  var isInited;
  var detectIt;



  function item(id, container, params) {

    var $container;
    var $list;
    var $links;

    var HIDING     = 'hiding';
    var REVEALING  = 'revealing';
    var CONCEALING = 'concealing';
    var SHOWING    = 'showing';

    var DISPLAY_CLASS = 'is-showing';
    var OPAQUE_CLASS  = 'is-opaque';

    var ON  = 'on';
    var OFF = 'off';

    var TRANSITION_DURATION = 200;

    var state;
    var isInteracting;



    function init() {
      referenceElements();
      turnListeners(ON);
      state = HIDING;
      isInteracting = false;
    }

    function referenceElements() {
      $container = $(container);
      $list      = $container.find('ul');
      $links     = $list.find('a');
    }

    function turnListeners(onOrOff) {
      params.isMouseInput ? turnHoverListeners(onOrOff) : turnTouchListeners(onOrOff);
      turnClickListeners(onOrOff);
    }

    function turnHoverListeners(onOrOff) {
      $container[onOrOff]('mouseenter', enterHandler);
      $container[onOrOff]('mouseleave', leaveHandler);
    }

    function turnTouchListeners(onOrOff) {
      $container[onOrOff]('touchstart', touchHandler);
      params.$body[onOrOff]('touchstart', touchOutsideHandler);
    }

    function turnClickListeners(onOrOff) {
      $links[onOrOff]('click', linkClickHandler);
    }

    function reveal() {
      isInteracting = true;
      if(state === HIDING) {
        state = REVEALING;
        $list.addClass(DISPLAY_CLASS).redraw().addClass(OPAQUE_CLASS);
        window.setTimeout(function() {
          state = SHOWING;
          if(!isInteracting) { params.conceal(id); }
        }, TRANSITION_DURATION);
      }
    }

    function conceal() {
      isInteracting = false;
      if(state === SHOWING) {
        state = CONCEALING;
        $list.removeClass(OPAQUE_CLASS);
        window.setTimeout(function() {
          state = HIDING;
          $list.removeClass(DISPLAY_CLASS);
          if(isInteracting) { params.reveal(id); }
        }, TRANSITION_DURATION);
      }
    }

    function enterHandler() {
      params.reveal(id);
    }

    function leaveHandler() {
      params.conceal(id);
    }

    function getTouchedList(event) {
      return $list.is(event.target) || $list.has(event.target).length > 0;
    }

    function touchHandler(event) {
      state === SHOWING && !getTouchedList(event) ? params.conceal(id) : params.reveal(id);
    }

    function linkClickHandler() {
      params.conceal(id);
    }

    function touchOutsideHandler(event) {
      if(!$container.is(event.target) && $container.has(event.target).length === 0) {
        params.conceal(id);
      }
    }

    function teardown() {
      turnListeners(OFF);
      $list.removeClass(OPAQUE_CLASS);
      $list.removeClass(DISPLAY_CLASS);
      id            = undefined;
      container     = undefined;
      params        = undefined;
      $container    = undefined;
      $list         = undefined;
      $links        = undefined;
      state         = undefined;
      isInteracting = undefined;
    }

    init();


    return {
      reveal   : reveal,
      conceal  : conceal,
      teardown : teardown
    };

  }



  function referenceElements() {
    items = $items.map(function(index, element) {
      return item(index, element, {
        isMouseInput : getIsMousePrimaryInput(),
        conceal      : conceal,
        reveal       : reveal,
        $body        : $('body')
      });
    }).toArray();
  }

  function conceal(index) {
    items[index].conceal();
    currentIndex = undefined;
  }

  function reveal(newIndex) {
    $.each(items, function(index, item) {
      if(index === newIndex) {
        currentIndex = newIndex;
        item.reveal();
      } else {
        item.conceal();
      }
    });
  }

  function getIsMousePrimaryInput() {
    return detectIt.primaryInput === 'mouse';
  }

  function teardown() {
    if(isInited) {
      $.each(items, function(index, item) { item.teardown(); });
      items    = undefined;
      $items   = undefined;
      isInited = undefined;
      detectIt = undefined;
    }
  }

  function init(params) {
    teardown();
    $items = $(params.selector);
    detectIt = params.detectIt;
    if($items.length) {
      referenceElements();
    }
    isInited = true;
  }



  return { init: init };

}(jQuery));