/**
* @name: EWQuoteExpirationBatchTest
* @description: test class for EWQuoteExpirationBatchTest  
* @author: Aleksejs Jedamenko ajedamenko@azuruw.com
* @date: 12/03/2020
*
*/
@isTest
public class EWQuoteExpirationBatchTest {
 /**
    * @methodName: setup
    * @description: setup test values for following test methods
    * @dateCreated: 12/03/2020
    */
	@testSetup
    static void setup(){
        
        Account testBroker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account testInsured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );

        insert new List<Account>{
                testBroker, testInsured
        };

        Opportunity testOpportunity = EWOpportunityDataFactory.createOpportunity(null, testInsured.Id, testBroker.Id, true);

        Asset testExpireIssuedPolicy = EWPolicyDataFactory.createPolicy(null, testOpportunity.Id, testBroker.Id, testInsured.Id, false);
        testExpireIssuedPolicy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        testExpireIssuedPolicy.vlocity_ins__EffectiveDate__c = Date.today().addDays(-300);
        testExpireIssuedPolicy.vlocity_ins__InceptionDate__c = Date.today().addDays(-300);
        testExpireIssuedPolicy.vlocity_ins__ExpirationDate__c = Date.today().addDays(-3);
        
        Asset testIssuedPolicy = EWPolicyDataFactory.createPolicy(null, testOpportunity.Id, testBroker.Id, testInsured.Id, false);
        testIssuedPolicy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        testIssuedPolicy.vlocity_ins__EffectiveDate__c = Date.today().addDays(-300);
        testIssuedPolicy.vlocity_ins__InceptionDate__c = Date.today().addDays(-300);
        testIssuedPolicy.vlocity_ins__ExpirationDate__c = Date.today().addDays(10);
        insert new List<Asset> {testExpireIssuedPolicy, testIssuedPolicy};
        
        Quote testQuotedRenewalExpiredQuote= EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testQuotedRenewalExpiredQuote.Status = EWConstants.QUOTE_STATUS_QUOTED;
        testQuotedRenewalExpiredQuote.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        testQuotedRenewalExpiredQuote.EW_Source_Policy__c = testExpireIssuedPolicy.Id;
        
        Quote testQuotedRenewalNonExpiredQuote = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testQuotedRenewalNonExpiredQuote.Status = EWConstants.QUOTE_STATUS_QUOTED;
        testQuotedRenewalNonExpiredQuote.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        testQuotedRenewalNonExpiredQuote.EW_Source_Policy__c = testIssuedPolicy.Id;
        insert new List<Quote> {testQuotedRenewalExpiredQuote, testQuotedRenewalNonExpiredQuote};
    }
    
     /**
    * @methodName: quoteStatusUpdateTest
    * @description: test EWQuoteExpirationBatch batch. Scenario: test setup creates 2 policy records, 1 expired and 1 non-expired and corresponding related quote records 
    * After batch run we check that 1 quote record with status Quoted and 1 quote record with Expired exist in the database
    * @dateCreated: 12/03/2020
    */
    @isTest
    private static void quoteStatusUpdateTest(){
        
        Test.startTest();
        Id batchId = Database.executeBatch(new EWQuoteExpirationBatch());
        Test.stopTest();
        List<Quote> expectedExpiredQuotes = [SELECT Status FROM Quote WHERE Status = :EWConstants.QUOTE_STATUS_EXPIRED];
        List<Quote> expectedQuotedPolicies = [SELECT Status FROM Quote WHERE Status = :EWConstants.QUOTE_STATUS_QUOTED];
        System.assertEquals(1, expectedExpiredQuotes.size());
        System.assertEquals(1, expectedQuotedPolicies.size());
    }
    
    
}