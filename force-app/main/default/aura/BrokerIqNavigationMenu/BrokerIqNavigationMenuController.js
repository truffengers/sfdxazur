({
    componentInitJs : function(component, event, helper) {
        TT.mediaMatcher.init(_, {
          mobile  : '(max-width: 860px)',
          desktop : '(min-width: 861px)'
        });

        TT.menu.init({
          mediaMatcher : TT.mediaMatcher
        });

        TT.brokerIqMenuItems.init({
          detectIt     : detectIt,
          menu         : TT.menu,
          item         : TT.brokerIqMenuItem,
          menuSelector : '#js-navigation',
          itemClass    : 'c-site-nav__item',
          linkClass    : 'c-site-nav__link',
          subLinkClass : 'c-select-nav__link',
          tabletStart  : 750,
          desktopStart : 1025
        });
    },
    onClick : function(component, event, helper) {
        var id = event.target.dataset.menuItemId;
        var recordtypeMetadataId = event.target.dataset.recordtypeMetadataId;
        if(recordtypeMetadataId) {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordtypeMetadataId
            });
            navEvt.fire();
            if(TT.menu && TT.menu.getIsOpen()) {
                TT.menu.openOrClose();
            }
        } else if (id) {
            component.find('menuBase').navigate(id);
            if(TT.menu && TT.menu.getIsOpen()) {
                TT.menu.openOrClose();
            }
        }
    },
    onBurgerMenuClick : function(component, event, helper) {
        var burgerMenuOpen = component.get("v.burgerMenuOpen");
        component.set("v.burgerMenuOpen", !burgerMenuOpen);
    },
    onSubMenuClick : function(component, event, helper) {
        subMenuLabel = event.currentTarget.dataset.label;
        subMenuOpenAttributeName = "v.subMenu" + subMenuLabel + "Open";
        subMenuCurrentlyOpen = component.get(subMenuOpenAttributeName);
        component.set(subMenuOpenAttributeName, !subMenuCurrentlyOpen);
    },
    subMenuOpen: function(component, event, helper) {
        subMenuLabel = event.currentTarget.dataset.label;
        subMenuOpenAttributeName = "v.subMenu" + subMenuLabel + "Open";
        component.set(subMenuOpenAttributeName, true);
    },
    subMenuClose: function(component, event, helper) {
        subMenuLabel = event.currentTarget.dataset.label;
        subMenuOpenAttributeName = "v.subMenu" + subMenuLabel + "Open";
        component.set(subMenuOpenAttributeName, false);
    }
})