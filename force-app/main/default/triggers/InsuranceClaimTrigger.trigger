trigger InsuranceClaimTrigger on vlocity_ins__InsuranceClaim__c (before insert, after insert, after delete) {
    TriggerDispatcher.Run(new InsuranceClaimTriggerHandler());
}