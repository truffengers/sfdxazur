@isTest
private class BrokerIqSearchControllerTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();

	private static PageReference pageRef;
	private static Map<String, String> params;

	static {
		pageRef = Page.BrokerIqSearch;
		Test.setCurrentPage(pageRef);
		params = pageRef.getParameters();
	}

	@isTest static void noParams() {
        Test.startTest();
		BrokerIqSearchController controller = new BrokerIqSearchController();
		Test.stopTest();

		System.assertEquals(0, controller.tabs[0].totalRecords);
	}

	private static BrightTALK__Webcast__c getWebcast() {
		BrightTALK_API_Settings__c settings = tog.getBrightTalkApiSettings();
		BrightTALK__Channel__c channel = tog.getBtChannel();
		channel.BrightTALK__Channel_Id__c = settings.Channel_Id__c;
		update channel;
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(123);
		webcast.BrightTALK__Start_Date__c = Date.today().addDays(-1);
		webcast.Status__c = 'recorded';
		webcast.IQ_Content__c = tog.getWebcastIqContent().Id;
		update webcast;

		return webcast;		
	}

	@isTest static void findRecord() {
		BrightTALK__Webcast__c webcast = getWebcast();

		params.put('q', webcast.BrightTALK__Name__c);

		Test.setFixedSearchResults(new List<Id>{webcast.Id});

        Test.startTest();
		BrokerIqSearchController controller = new BrokerIqSearchController();
		Test.stopTest();

		System.assertEquals(1, controller.tabs[0].totalRecords);
		System.assertEquals(1, controller.tabs[0].firstRecord);
		System.assertEquals(1, controller.tabs[0].lastRecord);
		BrokerIqWebinarPreviewController previewController = new BrokerIqWebinarPreviewController();
		previewController.webcastJsonP = controller.tabs[0].webcastsJson[0];
		System.assertEquals(webcast.BrightTALK__Name__c, previewController.thisWebcast.BrightTALK__Name__c);
	}
    
	@isTest static void category() {
		BrightTALK__Webcast__c webcast = getWebcast();
		Category__c category = tog.getCategory();
        IQ_Content__c iqContent = tog.getWebcastIqContent();
        iqContent.Category__c = category.Id;
        update iqContent;
        
		params.put('categoryId', category.Id);

        Test.startTest();
		BrokerIqSearchController controller = new BrokerIqSearchController();
		Test.stopTest();

		System.assertEquals(1, controller.tabs[0].totalRecords);
		System.assertEquals(1, controller.tabs[0].firstRecord);
		System.assertEquals(1, controller.tabs[0].lastRecord);
		System.assertEquals(category.Id, controller.category.Id);
	}
	
	@isTest static void pagination() {
		BrightTALK__Webcast__c webcast = getWebcast();
		Integer nWebcasts = 20;
		Integer itemsPerPage = 6;
		List<BrightTALK__Webcast__c> webcasts = new List<BrightTALK__Webcast__c>();
		for(Integer i=0; i < nWebcasts-1; i++) {
			BrightTALK__Webcast__c newWebcast = webcast.clone();
			newWebcast.BrightTALK__Webcast_Id__c += i+1;
			webcasts.add(newWebcast);
		}

		insert webcasts;

		List<Id> searchResults = new List<Id>{webcast.Id};
		for(BrightTALK__Webcast__c wc : webcasts) {
			searchResults.add(wc.Id);
		}

		params.put('q', webcast.BrightTALK__Name__c);

		Test.setFixedSearchResults(searchResults);

        Test.startTest();
		BrokerIqSearchController controller = new BrokerIqSearchController();
		Test.stopTest();

		controller.thisTabNumber = 0;
		controller.tabs[0].selectedItemsPerPage = itemsPerPage;
		controller.itemsPerPageChanged();

		System.assertEquals(nWebcasts, controller.tabs[0].totalRecords);
		System.assertEquals(1, controller.tabs[0].firstRecord);
		System.assertEquals(itemsPerPage, controller.tabs[0].lastRecord);

		controller.tabs[0].nextPage = 4;
		controller.toPage();

		System.assertEquals(19, controller.tabs[0].firstRecord);
		System.assertEquals(20, controller.tabs[0].lastRecord);
	}

}