({
    doInit: function (component, event, helper) {
        let getSiteMetadataAction = component.get('c.getSiteMetadata');

        getSiteMetadataAction.setCallback(this, function (response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                const siteMetadata = response.getReturnValue();
                component.set('v.siteMetadata', siteMetadata);
            }
        });

        $A.enqueueAction(getSiteMetadataAction);
    }
});