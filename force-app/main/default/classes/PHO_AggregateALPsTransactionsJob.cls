/**
* @name: PHO_AggregateALPsTransactionsJob
* @description: Asynchronous job to aggregate ALPs financial data on Policy Transactions.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 08/01/2020
* @lastChangedBy: Konrad Wlazlo   31/03/2020    PR-79 Added logic to send email with job result and create Azur Log in case of error
*/
public with sharing class PHO_AggregateALPsTransactionsJob implements Queueable {

    @TestVisible
    private List<IBA_PolicyTransaction__c> transactions;
    private Integer recordQueryLimit;

    public PHO_AggregateALPsTransactionsJob(Integer recordQueryLimit) {
        this.recordQueryLimit = recordQueryLimit;
        this.transactions = PHO_ALPsTransactionAggregator.retrieveTransactionsToAggregate(this.recordQueryLimit);
    }

    public void execute(QueueableContext context) {
        List<String> emailAddresses = new List<String> {System.UserInfo.getUserEmail()};
        List<Azur_Log__c> customLogs = new List<Azur_Log__c>();
        LogBuilder logBuilder = new LogBuilder();

        Savepoint sp = Database.setSavepoint();
        Boolean continueProcessing = true;
        try {
            if (!this.transactions.isEmpty()) {
                PHO_ALPsTransactionAggregator.aggregateTransactions(this.transactions);
            } else {
                continueProcessing = false;

                EmailUtil email = new EmailUtil(emailAddresses);
                email.plainTextBody('ALPs Transactions Aggregation process succeeded');
                email.subject('ALPs Transactions Aggregation process succeeded');
                email.sendEmail();
            }
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug(LoggingLevel.ERROR, 'PHO Exception occured. Queueable job terminated. Error message: '+e.getMessage() +'. Stack trace: '+e.getStackTraceString());
            continueProcessing = false;

            EmailUtil email = new EmailUtil(emailAddresses);
            email.plainTextBody('An error occurred during ALPs Transactions Aggregation process, please check the latest Azur logs for more details');
            email.subject('ALPs Transactions Aggregation process failed');
            email.sendEmail();
            Azur_Log__c newLog = logBuilder.createLogWithCodeInfo(e,'Phoenix Rising','PHO_AggregateALPsTransactionsJob','execute');
            customLogs.add(newLog);
            insert customLogs;
        }

        if (continueProcessing && !Test.isRunningTest()) {
            System.enqueueJob(new PHO_AggregateALPsTransactionsJob(this.recordQueryLimit));
        }
    }
}