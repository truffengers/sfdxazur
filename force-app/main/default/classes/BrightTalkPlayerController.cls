public class BrightTalkPlayerController extends PortalController {
    @AuraEnabled
    public String realmToken {get; set;}
    @AuraEnabled
    public String channelId {get; set;}
    @AuraEnabled
    public String brightTalkEndpoint {get; set;}

    public BrightTalkPlayerController() {
        BrightTalkApi api = new BrightTalkApi();
        BrightTALK_API_Settings__c settings = api.getSettings();

        if ((thisUserAsContact != null || (thisUserAsContact == null && userType == 'Standard')) && thisUser != null) {
            realmToken = EncodingUtil.urlEncode(api.getRealmToken(thisUserAsContact, thisUser), 'UTF-8');
        }
        channelId = '' + settings.Channel_Id__c.intValue();
        brightTalkEndpoint = api.getSettings().Endpoint__c;
    }
    
    @AuraEnabled
    public static BrightTalkPlayerController getInstance() {
        return new BrightTalkPlayerController();
    }
}