/**
* @name: EWUserDataFactory
* @description: Reusable factory methods for the user object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 05/09/2018
*/
@isTest
public class EWUserDataFactory {

    public static Profile getProfile(String profileName) {
        return [SELECT Id FROM Profile WHERE Name = :profileName LIMIT 1];
    }

    public static UserRole getUserRole(String userRoleDeveloperName) {
        return [SELECT Id FROM UserRole WHERE DeveloperName = :userRoleDeveloperName LIMIT 1];
    }

    public static User createSystemAdmistratorUser(String lastName,
                                                   Id roleId,
                                                   Boolean insertRecord) {

        Profile systemAdminProfile = getProfile(EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME);
        User systemAdminUser = createUser(lastName, systemAdminProfile.Id, roleId, false);

        if (insertRecord) {
            insert systemAdminUser;
        }

        return systemAdminUser;
    }

    public static User createUser(String lastName,
                                  Id profileId,
                                  Id roleId,
                                  Boolean insertRecord) {

        User user = new User();
        user.FirstName = 'Tester' + Math.random();
        user.LastName = lastName;
        user.Email = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
        user.Username = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
        user.Alias = 'ttest';
        user.EmailEncodingKey='UTF-8';
        user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.ProfileId = profileId;
        user.UserRoleId = roleId;
        user.TimeZoneSidKey = 'Europe/London';
        user.PortalRole = 'Manager';
        user.isActive = true;

        if (insertRecord) {
            insert user;
        }

        return user;
    }

    public static User createPartnerUser(String lastName,
                                         Id profileId,
                                         Id contactId,
                                         Boolean insertRecord) {

        User user = new User();
        user.FirstName = 'Tester' + Math.random();
        user.LastName = lastName;
        user.Email = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
        user.Username = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
        user.Alias = 'ttest';
        user.EmailEncodingKey='UTF-8';
        user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.ProfileId = profileId;
        user.ContactId = contactId;
        user.TimeZoneSidKey = 'Europe/London';
        user.isActive = true;

        if (insertRecord) {
            insert user;
        }

        return user;
    }
}