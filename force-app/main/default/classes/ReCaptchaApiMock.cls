public class ReCaptchaApiMock extends Nebula_API.NebulaApiMock {

	private Boolean success;

    public ReCaptchaApiMock(Boolean success) {
        super(200);
        this.success = success;
    }


    public override HttpResponse respond(HTTPRequest req, HTTPResponse rval) {

    	rval.setBody(JSON.serialize(new Map<String, Object>{
    		'success' => success
		}));

    	return rval;
	}    
}