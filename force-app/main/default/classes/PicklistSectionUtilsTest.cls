@IsTest
private class PicklistSectionUtilsTest {

    static testMethod void testPicklistSectionUtils_coverage() {
        PicklistSectionUtils.updateRecordsBasedOnStaticResource('test', 'test');
    }

    static testMethod void testPicklistSectionUtils() {
        List<String> csvData = new List<String>{
                'Name1,1', 'Name2,2', 'Name3,3'
        };

        RecordType oneRt = getOneRt();
        List<Picklist_Section__c> picklistSections = new List<Picklist_Section__c>();

        picklistSections.add(new Picklist_Section__c(Name = 'Test1', RecordTypeId = oneRt.Id));
        picklistSections.add(new Picklist_Section__c(Name = 'Test2', RecordTypeId = oneRt.Id));
        insert picklistSections;

        PicklistSectionUtils.updateRecords(csvData, oneRt.DeveloperName);

        picklistSections = new List<Picklist_Section__c>([
                SELECT Id
                FROM Picklist_Section__c
        ]);

        System.assert(!picklistSections.isEmpty());
        System.assertEquals(csvData.size(), picklistSections.size());
    }

    private static RecordType getOneRt() {
        RecordType rt = [
                SELECT Id, Name, DeveloperName
                FROM RecordType
                WHERE SobjectType = :PicklistSectionUtils.PS_OBJECT_API_NAME
                LIMIT 1
        ];

        return rt;
    }
}