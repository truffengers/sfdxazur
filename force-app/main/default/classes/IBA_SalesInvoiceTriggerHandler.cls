/**
* @name: IBA_SalesInvoiceTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaInvoice__c trigger functionality
*
*/

public with sharing class IBA_SalesInvoiceTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_SalesInvoiceTriggerHandler.IsDisabled != null ? IBA_SalesInvoiceTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, c2g__codaInvoice__c> policyTransactionMap = new Map<Id, c2g__codaInvoice__c>();
        Map<Id, c2g__codaInvoice__c> policyTransactionDueDateMap = new Map<Id, c2g__codaInvoice__c>();
        for(Id recordId :newItems.keySet()) {
            c2g__codaInvoice__c salesInvoice = (c2g__codaInvoice__c)newItems.get(recordId);
            c2g__codaInvoice__c salesInvoiceOld = (c2g__codaInvoice__c)oldItems.get(recordId);
            if(salesInvoice.c2g__InvoiceStatus__c == IBA_UtilConst.SALES_INVOICE_STATUS_COMPLETE 
                && salesInvoiceOld.c2g__InvoiceStatus__c != IBA_UtilConst.SALES_INVOICE_STATUS_COMPLETE) {
                    policyTransactionMap.put(salesInvoice.IBA_PolicyTransaction__c, salesInvoice);
            }
            if(salesInvoice.c2g__DueDate__c != salesInvoiceOld.c2g__DueDate__c) {
                policyTransactionDueDateMap.put(salesInvoice.IBA_PolicyTransaction__c, salesInvoice);
            }
        }

        if(policyTransactionMap.size() > 0 || policyTransactionDueDateMap.size() > 0) {
            Set<Id> allPolicyTransactionIds = new Set<Id>();
            allPolicyTransactionIds.addAll(policyTransactionMap.keySet());
            allPolicyTransactionIds.addAll(policyTransactionDueDateMap.keySet());
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(allPolicyTransactionIds);
            transferData.addInvoiceTotal(policyTransactionMap)
                .changeSalesInvoiceDueDate(policyTransactionDueDateMap)
                .transfer();
        }
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}