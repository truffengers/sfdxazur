@istest
public class BrightTalkSubscriberWebcastActivityTest {

    private static TestObjectGenerator tog = new TestObjectGenerator();

    @istest
    public static void parseXmlTest() {
        BrightTalkApiMock mock = new BrightTalkApiMock(tog.getBrightTalkApiSettings(), 200);

        Dom.Document doc = new Dom.Document();
        doc.load(mock.getMergedResponse('subscribers_webcast_activity_response'));

        
        
        List<Dom.XmlNode> nodes = doc.getRootElement().getChildElements();
        List<BrightTalkSubscriberWebcastActivity> was = new List<BrightTalkSubscriberWebcastActivity>();
        
        for(Dom.XmlNode node : nodes) {
            if(node.getName() == 'subscriberWebcastActivity') {
                BrightTalkSubscriberWebcastActivity parsed = 
                    new BrightTalkSubscriberWebcastActivity();
                parsed.load(node);
                was.add(parsed);
            }
        }
        
        System.assertEquals(Integer.valueOf(mock.responseParams.get('total_viewings')), was[0].getTotalViewings() );
        DateTime lastUpdated = (DateTime)(new XmlRepresentableMap.DateTimeParser()).convert(mock.responseParams.get('last_updated'));
        System.assertEquals(lastUpdated, was[0].getLastUpdated() );
        System.assertEquals(mock.responseParams.get('webcast_id'), was[0].getWebcastId() );
        System.assertEquals(mock.responseParams.get('realm_user_id'), was[0].getUserRealmId() );
        
        was.sort();
        System.assertEquals(8, was.size());
        
        for(Integer i=1; i < was.size(); i++) {
            System.assert(was[i-1].getLastUpdated() <= was[i].getLastUpdated());
            was[i-1].setSortOnField('totalViewings');
            was[i].setSortOnField('totalViewings');
        }
        was.sort();
        
        for(Integer i=1; i < was.size(); i++) {
            System.assert(was[i-1].getTotalViewings() <= was[i].getTotalViewings());
        }
    }
    
}