/**
 * Created by roystonlloyd on 2019-05-24.
 */

public without sharing class EWClauseAssignmentHelper {

    // updates the named clauses on the quote record.  Called by EWClauseAssignmentTriggerHandler
    public void refreshClauses(List<SObject> triggerNew, Map<Id, SObject> oldMap){
        List<EW_ClauseAssignment__c> clauseAssignments = (List<EW_ClauseAssignment__c>) triggerNew;

        // get the Quote records to update
        Set<Id> quoteIds = new Set<Id>();
        for(EW_ClauseAssignment__c ca : clauseAssignments){
            if(ca.EW_Quote__c != null){
                quoteIds.add(ca.EW_Quote__c);
            }else if(oldMap != null && oldMap.containsKey(ca.Id)){
                EW_ClauseAssignment__c oldCA = (EW_ClauseAssignment__c) oldMap.get(ca.Id);
                if(oldCA.EW_Quote__c != null){
                    quoteIds.add(oldCA.EW_Quote__c);
                }
            }
        }

        if(!quoteIds.isEmpty()){
            List<Quote> quotes = [SELECT Id,(SELECT EW_DocumentClause__r.EW_Clause_Name__c FROM ClauseAssignments__r)  FROM Quote WHERE Id = :quoteIds];

            For(Quote q : quotes){

                Set<String> clauseSet = new Set<String>();
                For(EW_ClauseAssignment__c ca : q.ClauseAssignments__r){
                    clauseSet.add(ca.EW_DocumentClause__r.EW_Clause_Name__c);
                }
                String clauses = clauseSet.isEmpty() ? '' : '• ' + String.join(new List<String>(clauseSet),' • ');
                q.EW_Clauses__c = clauses.left(255);
            }
            List<Database.SaveResult> srList = Database.update(quotes, false);
            for (Database.SaveResult sr : srList) {
                if(!sr.isSuccess()){
                    String errorMessage = 'Error updating Clause Assignments on Quote Id '+ sr.getId()+'. Details= ';

                    for(integer i = 0; i < sr.getErrors().size() ; i++){
                        Database.Error err = sr.getErrors()[i];
                        errorMessage += (i+1) +') '+ err.getStatusCode() + ': ' + err.getMessage() + ' ';
                    }
                    LogBuilder.insertLog(new clauseAssignmentException(errorMessage));

                }
            }
        }

    }

    public static void manualClauseAssignments(Id boundQuoteId, Id quoteId, String transactionType, Map<String, Object> outputMap){
        // on MTA/Renewal, compares the boundQuote and quote for underwriter assigned clauses that need to be kept.

        if(quoteId == null || boundQuoteId == null || transactionType == null){
            outputMap.put('Error','Error: quoteId = '+ quoteId + '. boundQuoteId = '+boundQuoteId +'. transactionType = ' +transactionType);
            return;
        }

        // get and sort the manually added Clauses
        Set<Id> quoteIds = new Set<Id>{boundQuoteId, quoteId};
        Map<String,vlocity_ins__DocumentClause__c> boundClauses = new Map<String,vlocity_ins__DocumentClause__c>();
        Set<String> quoteClauses = new Set<String>();
        List<EW_ClauseAssignment__c> clauseAssignments = [SELECT Id, EW_Active__c, EW_DocumentClause__r.Id, EW_DocumentClause__r.EW_Clause_Name__c, EW_DocumentClause__r.EW_Start_Date__c, EW_Quote__c, EW_Assigned_By_Underwriter__c FROM EW_ClauseAssignment__c WHERE EW_Quote__c IN: quoteIds AND EW_Active__c = true AND EW_RemovedInSuccessiveQuote__c = false];
        for(EW_ClauseAssignment__c mc : clauseAssignments){
            if(mc.EW_Quote__c == boundQuoteId && mc.EW_Assigned_By_Underwriter__c) boundClauses.put(mc.EW_DocumentClause__r.EW_Clause_Name__c, mc.EW_DocumentClause__r);
            if(mc.EW_Quote__c == quoteId) quoteClauses.add(mc.EW_DocumentClause__r.EW_Clause_Name__c);
        }

        // for renewal only, get the recent clause versions
        List<vlocity_ins__DocumentClause__c> appropriateClauseVersions = transactionType == EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL
                ? getAppropriateClauses(quoteId, boundClauses.keySet())
                : new List<vlocity_ins__DocumentClause__c>();
        Map<String,Id> appropriateClauseVersionMap = new Map<String,Id>();
        for(vlocity_ins__DocumentClause__c dc : appropriateClauseVersions){
            appropriateClauseVersionMap.put(dc.EW_Clause_Name__c, dc.Id);
        }

        // compare the records
        Map<String,EW_ClauseAssignment__c> newClauseAssignments = new Map<String,EW_ClauseAssignment__c>();
        for(String clause : boundClauses.keySet()){
            if(!quoteClauses.contains(clause)){
                newClauseAssignments.put(clause, new EW_ClauseAssignment__c(
                        EW_DocumentClause__c  = appropriateClauseVersionMap.containsKey(clause) ? appropriateClauseVersionMap.get(clause) : boundClauses.get(clause).Id,
                        EW_Quote__c = quoteId,
                        EW_Active__c = true,
                        EW_Assigned_By_Underwriter__c = true
                ));
            }
        }

        // insert new records if they are needed
        if(!newClauseAssignments.isEmpty()) insert newClauseAssignments.values();
    }


    public static Map<String,Object> getStandardClauses(Id quoteId, Set<String> clauseNames){

        Map<String,Object> outMap = new Map<String,Object>();
        outMap.put('clauseNames', String.join(new List<String>(clauseNames),','));

        List<vlocity_ins__DocumentClause__c> sortedClauses = getAppropriateClauses(quoteId, clauseNames);
        for(vlocity_ins__DocumentClause__c dc : sortedClauses){

            String objJSON = JSON.serialize(dc);
            Map<String,Object> objMap = (Map<String,Object>) JSON.deserializeUntyped(objJSON);

            objMap.put('quoteId', quoteId);
            objMap.remove('attributes');
            outMap.put(dc.EW_Clause_Name__c, objMap);
        }
        return outMap;
    }


    private static List<vlocity_ins__DocumentClause__c> getAppropriateClauses(Id quoteId, Set<String> clauseNames){

        String queryString = 'SELECT id, EW_Clause_Name__c, EW_Start_Date__c FROM vlocity_ins__DocumentClause__c WHERE vlocity_ins__Category__c = \''+EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD+'\'';
        if(!clauseNames.isEmpty()) queryString += ' AND EW_Clause_Name__c in: clauseNames';
        queryString += ' ORDER BY EW_Start_Date__c ASC NULLS FIRST';

        List<vlocity_ins__DocumentClause__c> unsortedClauses = Database.query(queryString);
        return getAppropriateClauseVersions(quoteId, unsortedClauses);
    }


    public static List<vlocity_ins__DocumentClause__c> getAppropriateClauseVersions(Id quoteId, List<vlocity_ins__DocumentClause__c> unsortedClauses){
        // finds the versions that were applicable based on the quote effective date.

        Quote quote = [SELECT Id, vlocity_ins__EffectiveDate__c, EW_TypeofTransaction__c, EW_Source_Policy__r.EW_Quote__r.vlocity_ins__EffectiveDate__c, EW_Source_Policy__r.vlocity_ins__OriginalVersionId__r.EW_Quote__r.vlocity_ins__EffectiveDate__c FROM Quote WHERE Id = :quoteId];
        Date effectiveDate = quote.vlocity_ins__EffectiveDate__c;  // (New Business / Renewal)

        // if MTA, gets the effective date from the originalVersion quote
        if(quote.EW_TypeofTransaction__c == EWConstants.QUOTE_TRANSACTION_TYPE_MTA && quote.EW_Source_Policy__c != null ) {

            effectiveDate = quote.EW_Source_Policy__r.vlocity_ins__OriginalVersionId__c != null
                    ? quote.EW_Source_Policy__r.vlocity_ins__OriginalVersionId__r.EW_Quote__r.vlocity_ins__EffectiveDate__c
                    : quote.EW_Source_Policy__r.EW_Quote__r.vlocity_ins__EffectiveDate__c;
        }

        // sort all the clauses to get the applicable clause for the effective date
        Map<String, vlocity_ins__DocumentClause__c> clauseMap = new Map<String, vlocity_ins__DocumentClause__c>();
        for(vlocity_ins__DocumentClause__c dc : unsortedClauses){
            if((dc.EW_Start_Date__c == null && !clauseMap.containsKey(dc.EW_Clause_Name__c)) || effectiveDate >= dc.EW_Start_Date__c ) clauseMap.put(dc.EW_Clause_Name__c, dc);
        }

        return clauseMap.values();
    }



    /**
    * @methodName: updateRemovedFlag
    * @description: void method called by EWClauseAssignmentTriggerHandler (afterInsert, afterDelete, afterUnDelete).  updates the clause assignments on the related boundQuote record.
    * @author: Roy Lloyd
    * @date: 6/3/2020
    */
    public void updateRemovedFlag(Map<Id, SObject> newMap, Map<Id, SObject> oldMap){

        List<EW_ClauseAssignment__c> updateClauseAssignments = new List<EW_ClauseAssignment__c>();
        Map<Id,String> docClauseMap = new Map<Id,String>();

        Set<Id> quoteIds = new Set<Id>();
        List<EW_ClauseAssignment__c> clauseAssignments = newMap != null ? newMap.values() : oldMap.values();
        for(EW_ClauseAssignment__c ca : clauseAssignments){
            quoteIds.add(ca.EW_Quote__c);
        }

        // get boundQuoteIds
        Set<Id> boundQuoteIds = new Set<Id>();
        Map<Id,Quote> quoteMap = new Map<Id,Quote>([SELECT Id, EW_Source_Policy__r.EW_Quote__c FROM Quote WHERE Id in: quoteIds AND EW_Source_Policy__c != null AND EW_Source_Policy__r.EW_Quote__c != null]);
        for(Quote q : quoteMap.values()){
            boundQuoteIds.add(q.EW_Source_Policy__r.EW_Quote__c);
        }

        // get the active clause assignments
        Map<Id,Map<String,EW_ClauseAssignment__c>> activeClauseAssignmentMap = new Map<Id,Map<String,EW_ClauseAssignment__c>>();
        List<EW_ClauseAssignment__c> activeClauseAssignments = [SELECT Id, EW_DocumentClause__r.EW_Clause_Name__c, EW_Quote__c FROM EW_ClauseAssignment__c WHERE EW_Active__c = true AND EW_Quote__c in: boundQuoteIds ];
        for(EW_ClauseAssignment__c ca : activeClauseAssignments){
            Map<String,EW_ClauseAssignment__c> caMap = activeClauseAssignmentMap.containsKey(ca.EW_Quote__c)
                    ? activeClauseAssignmentMap.get(ca.EW_Quote__c)
                    : new Map<String,EW_ClauseAssignment__c>();
            caMap.put(ca.EW_DocumentClause__r.EW_Clause_Name__c, ca);
            activeClauseAssignmentMap.put(ca.EW_Quote__c, caMap);
            docClauseMap.put(ca.EW_DocumentClause__c, ca.EW_DocumentClause__r.EW_Clause_Name__c);
        }

        // prepare to update clauseAssignments

        if(newMap == null){  // trigger is afterDelete
            for(EW_ClauseAssignment__c ca : (List<EW_ClauseAssignment__c>) oldMap.values()){

                if(quoteMap.containsKey(ca.EW_Quote__c)){
                    Id boundQuoteId = quoteMap.get(ca.EW_Quote__c).EW_Source_Policy__r.EW_Quote__c;

                    if(activeClauseAssignmentMap.containsKey(boundQuoteId)){
                        Map<String,EW_ClauseAssignment__c> caMap = activeClauseAssignmentMap.get(boundQuoteId);
                        String clauseName = docClauseMap.containsKey(ca.EW_DocumentClause__c) ? docClauseMap.get(ca.EW_DocumentClause__c) : '';

                        if(caMap.containsKey(clauseName)) updateClauseAssignments.add(new EW_ClauseAssignment__c(Id = caMap.get(clauseName).Id, EW_RemovedInSuccessiveQuote__c = true));
                    }
                }
            }
        }else if(oldMap == null){ // trigger is afterInsert / afterUnDelete
            for(EW_ClauseAssignment__c ca : (List<EW_ClauseAssignment__c>) newMap.values()){

                if(quoteMap.containsKey(ca.EW_Quote__c)) {
                    Id boundQuoteId = quoteMap.get(ca.EW_Quote__c).EW_Source_Policy__r.EW_Quote__c;

                    if(activeClauseAssignmentMap.containsKey(boundQuoteId)){
                        Map<String,EW_ClauseAssignment__c> caMap = activeClauseAssignmentMap.get(boundQuoteId);
                        String clauseName = docClauseMap.containsKey(ca.EW_DocumentClause__c) ? docClauseMap.get(ca.EW_DocumentClause__c) : '';

                        if(caMap.containsKey(clauseName)) updateClauseAssignments.add(new EW_ClauseAssignment__c(Id = caMap.get(clauseName).Id, EW_RemovedInSuccessiveQuote__c = false));
                    }
                }
            }
        }
        if(!updateClauseAssignments.isEmpty()) update updateClauseAssignments;
    }


    public class clauseAssignmentException extends Exception {}


}