({

	handleSave : function(component, event, helper) {
        component.find("edit").get("e.recordSave").fire();
	},
    
    handleSaveSuccess: function (component, event, helper) {
        // e.force:navigateToSObject does not display updated record values due to Lightning caching
        window.parent.location = '/lightning/r/sObject/' + component.get('v.recordId') + '/view';
	},
    
    handleCancel: function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": component.get('v.sObjectName')
        });
        homeEvent.fire();
	}

});