({
    initJs: function (component, state) {
        // console.log('component ', component);
        // console.log('state ', state);
        try {
            var hasOtherDependency = component.get('v.hasOtherDependency');

            if (typeof state.isRendered === 'undefined') {
                state.isRendered = component.get('v.isRendered');
            }

            if (typeof state.librariesLoaded === 'undefined') {
                state.librariesLoaded = component.get('v.librariesLoaded');
            }

            if (hasOtherDependency && typeof state.otherDependencyLoaded === 'undefined') {
                state.otherDependencyLoaded = component.get('v.otherDependencyLoaded');
            }

            if ((component.get('v.allowMultipleInit') || !component.get('v.jsInitialised'))
                && state.isRendered
                && state.librariesLoaded
                && (!hasOtherDependency || state.otherDependencyLoaded)) {

                if (!TT.imageParallax) {
                    TT.imageParallax = TT.createImageParallax(jQuery, _, TT.imageParallaxItem);
                }

                var initJsComponent = component.get('v.initJsComponent');

                if (initJsComponent) {

                    if (initJsComponent.isInstanceOf('c:BrokerIqInitJs')) {
                        //   this.log(component, 'calling initJsComponent');
                        console.log('componentInitJs ', initJsComponent);
                        initJsComponent.componentInitJs();
                        component.set('v.jsInitialised', true);
                    } else {
                        console.error('BrokerIqLibs was passed an initJsComponent but it does not implement c:BrokerIqInitJs');
                        console.error(initJsComponent);
                    }
                }
            }
        } catch(err) {
            console.error(err);
        }
    },
    initTT: function (component) {
        if (TT !== undefined) {
            if (TT.dropdownMenu !== undefined && TT.dropdownMenu.init !== undefined) {
                TT.dropdownMenu.init({
                    selector: '#js-subdomain-menu, #js-user-menu',
                    detectIt: detectIt
                });
            }

            if (TT.scrollBroadcaster !== undefined && TT.mediaMatcher !== undefined && TT.menuVisibilityManager !== undefined) {
                let scrollBroadcaster = TT.scrollBroadcaster(jQuery, {
                    window: window,
                    isLogging: false,
                    trailing: true
                });

                TT.mediaMatcher.init(_, {
                    mobile: '(max-width: 860px)',
                    desktop: '(min-width: 861px)'
                });

                TT.menuVisibilityManager.init({
                    scrollBroadcaster: scrollBroadcaster,
                    mediaMatcher: TT.mediaMatcher
                });

                scrollBroadcaster.init();
                scrollBroadcaster.enable();
            }
        }
    },
    log: function (component, event) {
        // var interestingComponent = 'c:BrokerIqHistory';
        // var initJsComponent = component.get('v.initJsComponent');
        // if(initJsComponent && initJsComponent.isInstanceOf(interestingComponent)) {
        //  console.log(component + ' ' + event);
        // }
    }
})