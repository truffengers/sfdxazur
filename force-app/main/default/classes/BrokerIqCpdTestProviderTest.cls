/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 14/10/2017
 * Description: (if required)
 */

@isTest
private class BrokerIqCpdTestProviderTest {
    private static TestObjectGenerator tog = new TestObjectGenerator();

    @isTest static void testBehavior() {
        CPD_Test__c cpdTest = tog.getCpdTest();

        CPD_Test__c result = BrokerIqCpdTestProviderController.getCpdTest(cpdTest.Id);

        System.assertEquals(result.Id, cpdTest.Id);
    }
}