TT.parallax = function(_, params) {

  'use strict';

  var $elements             = params.$elements;
  var data                  = params.data;
  var $scrollable           = params.$scrollable;
  var scrollRatioCalculator = params.scrollRatioCalculator;
  var lastHeight;
  var $document;
  var checkHeightInterval;
  var THROTTLE_DELAY = 10;
  var scrollThrottleFunction;
  var resizeThrottleFunction;

  var ON  = 'on';
  var OFF = 'off';



  function init() {
    $document = $(document);
    initThrottleFunctions();
    turnListeners(ON);
    startCheckingHeight();
    move();
    reveal();
  }

  function initThrottleFunctions() {
    scrollThrottleFunction = _.throttle(scrollHandler, THROTTLE_DELAY);
    resizeThrottleFunction = _.throttle(resizeHandler, THROTTLE_DELAY);
  }

  function turnListeners(onOrOff) {
    $scrollable[onOrOff]('scroll', scrollThrottleFunction);
    $scrollable[onOrOff]('resize', resizeThrottleFunction);
  }

  function startCheckingHeight() {
    checkHeightInterval = window.setInterval(checkHeight, THROTTLE_DELAY);
  }

  function checkHeight() {
    var height = $document.height();
    if(height !== lastHeight) {
      lastHeight = height;
      resizeHandler();
    }
  }

  function resizeHandler() {
    scrollRatioCalculator.recalculateHeight();
    move();
  }

  function scrollHandler() {
    move();
  }

  function percentageToPixels(yOffset) {
    return (parseFloat(yOffset) * scrollRatioCalculator.getTotalHeight()) / 100;
  }

  function calculatePosition(yOffset, inertia) {
    return Math.round(percentageToPixels(yOffset) + (scrollRatioCalculator.getRatio() * scrollRatioCalculator.getTotalHeight() * inertia)) + 'px';
  }

  function getPositions() {
    return $.map(data, function(e) {
      return calculatePosition(e.yOffset, e.inertia);
    });
  }

  function move() {
    var positions = getPositions();
    $.each($elements, function(i, element) {
      $(element).css({transform: 'translate3d(0,' + positions[i] + ',0)'});
    });
  }

  function reveal() {
    $elements.addClass('is-opaque');
  }


  init();



  return {

    teardown: function() {
      turnListeners(OFF);
      window.clearInterval(checkHeightInterval);
      scrollThrottleFunction = null;
      resizeThrottleFunction = null;
      // scrollRatioCalculator.teardown();
      data                  = null;
      $scrollable           = null;
      scrollRatioCalculator = null;
      checkHeightInterval   = null;
    }

  };

};