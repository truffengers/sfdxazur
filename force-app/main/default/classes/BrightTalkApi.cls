public class BrightTalkApi extends Nebula_Api.NebulaApi {
    private static BrightTALK_API_Settings__c  settings;
    
    public BrightTalkApi() {
        super('BrightTalk');
    }
    
    public BrightTALK_API_Settings__c getSettings() {
        if(settings == null) {
            settings = BrightTALK_API_Settings__c.getInstance();
        }
        return settings;
    }
    
    public String getUserDocument(Contact c, User u) {
        BrightTalkUser btu = new BrightTalkUser(c, u);
        
        return btu.toXml(true);
    }
    
    public Blob getEncryptedUserToken(Contact c, User u) {
        String doc = getUserDocument(c, u);
        BrightTALK_API_Settings__c theseSettings = getSettings();
        return Crypto.encrypt('AES128', 
                       EncodingUtil.convertFromHex(theseSettings.Realm_Encryption_Key__c), 
                       EncodingUtil.convertFromHex(theseSettings.Realm_Initialisation_Vector__c),
                       Blob.valueOf(doc));
        
    }
    
    public String getRealmToken(Contact c, User u) {
        Blob encryptedUserToken = getEncryptedUserToken(c, u);
        BrightTALK_API_Settings__c theseSettings = getSettings();
        return theseSettings.Realm_ID__c + ':' + EncodingUtil.base64Encode(encryptedUserToken);
    }
    
    private HttpRequest getRequest(String method, String relativePath) {
        BrightTALK_API_Settings__c  theseSettings = getSettings();
        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        req.setHeader('Content-Type', 'application/xml');
        req.setHeader('Accept', 'application/xml');
        req.setHeader('Authorization', 'Basic ' 
                      + EncodingUtil.base64Encode(Blob.valueOf(theseSettings.API_Key__c 
                                                               + ':' 
                                                               + theseSettings.API_Secret__c)));
        String thisEndpoint = theseSettings.API_Endpoint__c + relativePath;
        req.setEndpoint(thisEndpoint);
        return req;
    }

    public List<BrightTalkSubscriberWebcastActivity> getActivitiesFromSf(Id contactId) {
        List<BrightTALK__Webcast_Activity__c> sfActivities = [SELECT Id, BrightTALK__Total_Viewings__c, BrightTALK__Last_Updated__c,
                                                            BrightTALK__Webcast__r.BrightTALK__Webcast_Id__c
                                                            FROM BrightTALK__Webcast_Activity__c
                                                            WHERE BrightTALK__Webcast__r.BrightTALK__Channel__r.BrightTALK__Channel_Id__c = :getSettings().Channel_Id__c
                                                            AND BrightTALK__Contact__c = :contactId];

        List<BrightTalkSubscriberWebcastActivity> rval = new List<BrightTalkSubscriberWebcastActivity>();

        for(BrightTALK__Webcast_Activity__c sfActivity : sfActivities) {
            rval.add(new BrightTalkSubscriberWebcastActivity((Integer)sfActivity.BrightTALK__Total_Viewings__c, 
                                                             sfActivity.BrightTALK__Last_Updated__c, 
                                                             '' + (Integer)sfActivity.BrightTALK__Webcast__r.BrightTALK__Webcast_Id__c,
                                                             contactId));
        }
        
        rval.sort();

        return rval;
    }
    
    public List<BrightTalkSubscriberWebcastActivity> getActivities(DateTime since) {
        return getActivities(since, null);
    }
    private List<BrightTalkSubscriberWebcastActivity> getActivities(DateTime since, String nextArgs) {
        String url = '/v1/channel/' + (Integer)getSettings().Channel_Id__c + '/subscribers_webcast_activity';
        String args = '';
        if(since != null) {
            String timeString = JSON.serialize(since);
            args += 'since=' + timeString.substring(1, timeString.length()-2);
        }
        if(nextArgs != null) {
            args += (args.length() > 0 ? '&' : '') + nextArgs;
        }

        if(args.length() > 0) {
            url += '?' + args;
        }
        HttpRequest req = getRequest('GET', url);

        HttpResponse resp = makeCallout(req);
        
        List<Dom.XmlNode> childNodes;
        List<BrightTalkSubscriberWebcastActivity> was = new List<BrightTalkSubscriberWebcastActivity>();
        
        try {
            childNodes = resp.getBodyDocument().getRootElement().getChildren();
        } catch(XmlException e) {
            return was;
        }
        
        
        String nextPageUrl;

        for(Dom.XmlNode node : childNodes) {
            if(node.getName() == 'subscriberWebcastActivity') {
                BrightTalkSubscriberWebcastActivity parsed = 
                    new BrightTalkSubscriberWebcastActivity();
                parsed.load(node);
                was.add(parsed);
            } else if(node.getName() == 'link' && node.getAttribute('rel', null) == 'next') {
                nextPageUrl = node.getAttribute('href', null);
            }
        }

        if(nextPageUrl != null) {
            nextArgs = nextPageUrl.substring(nextPageUrl.indexOf('?')+1);
            was.addAll(getActivities(null, nextArgs));
        }

        was.sort();
        
        return was;
    }

    public List<BrightTALK__Webcast__c> getWebcastsFromFeed() {
        List<BrightTALK__Webcast__c> rval = new List<BrightTALK__Webcast__c>();
        BrightTALK_API_Settings__c  theseSettings = getSettings();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        String thisEndpoint = theseSettings.Endpoint__c + '/channel/' + (Integer)theseSettings.Channel_Id__c + '/feed';
        req.setEndpoint(thisEndpoint);

        HttpResponse resp = makeCallout(req);

        List<Dom.XmlNode> childNodes = resp.getBodyDocument().getRootElement().getChildren();
        String ns = 'http://brighttalk.com/2009/atom_extensions';

        for(Dom.XmlNode entry : childNodes) {
            if(entry.getName() == 'entry') {
                Dom.XmlNode idNode = entry.getChildElement('communication', ns);
                BrightTALK__Webcast__c thisWebcast = new BrightTALK__Webcast__c(BrightTALK__Webcast_Id__c = Integer.valueOf(idNode.getAttribute('id', null)));

                List<Dom.XmlNode> entryChildren = entry.getChildren();

                for(Dom.XmlNode entryChild : entryChildren) {
                    if(entryChild.getName() == 'link') {
                        if(entryChild.getAttribute('rel', null) == 'enclosure' && entryChild.getAttribute('title', null) =='thumbnail') {
                            thisWebcast.Preview_Thumbnail__c = entryChild.getAttribute('href', null);
                        } else if(entryChild.getAttribute('rel', null) == 'related' && entryChild.getAttribute('title', null) =='preview') {
                            thisWebcast.Preview_Image__c = entryChild.getAttribute('href', null);
                        }
                    } else if(entryChild.getName() == 'duration') {
                        thisWebcast.Duration_s__c = Integer.valueOf(entryChild.getText());
                    } else if(entryChild.getName() == 'status') {
                        thisWebcast.Status__c = entryChild.getText();
                    }
                }

                rval.add(thisWebcast);
            }
        }

        return rval;
    }
}