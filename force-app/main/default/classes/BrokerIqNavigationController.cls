public class BrokerIqNavigationController {
	
    public class NavigationLink {
        public String label {get; set;}
        public String link {get; set;}

        public NavigationLink(String label, String link) {
            this.label = label;
            this.link = link;
        }
    }
    
    public String navigationListJsonProp {get; set {
        String reQuoted = (String)value.replaceAll('\'', '"');
        links = (List<NavigationLink>)Json.deserializeStrict(reQuoted, List<NavigationLink>.class);
        lastLink = links.remove(links.size()-1);
    }}
    
    public List<NavigationLink> links {get; set;}
    public NavigationLink lastLink {get; set;}
    
    public BrokerIqNavigationController() {
        
    }
}