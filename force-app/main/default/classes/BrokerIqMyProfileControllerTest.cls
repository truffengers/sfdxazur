@isTest
private class BrokerIqMyProfileControllerTest {
	
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings;
    private static BrokerIqMyProfileController controller;
    
    static {
        settings = tog.getBrightTalkApiSettings();
		tog.getUserContact();

        PageReference pageRef = Page.BrokerIqMyProfile;
		Test.setCurrentPage(pageRef);
            
        controller = new BrokerIqMyProfileController();
    }

	@isTest static void noPassword() {
		controller.save();
		List<ApexPages.Message> messages = ApexPages.getMessages();
		
		System.assertEquals(1, messages.size(), messages);
		System.assertEquals(Label.Password_Required, messages[0].getDetail(), messages);
	}

    @isTest static void incorrectPassword() {
		controller.currentPassword = 'not_the_right_password';
		controller.save();
		List<ApexPages.Message> messages = ApexPages.getMessages();
		
		System.assertEquals(1, messages.size(), messages);
		System.assertEquals(Label.Current_password_incorrect, messages[0].getDetail(), messages);
	}
	
	@isTest static void updateEmail() {
		BrokerIqMyProfileController.forceCorrectPassword = true;
		String newEmail = 'a+' + controller.thisUser.Email;
		controller.thisUser.Email = newEmail;
		controller.newPassword = 'newPw';
		controller.verifyNewPassword = 'newPw';

		controller.save();

		User u = [SELECT Id, Email, Username FROM User WHERE Id  = :UserInfo.getUserId() ];
		System.assertEquals(newEMail, u.Email);
//		System.assertEquals(newEMail, u.Username);
	}
	@isTest static void badEmailAddress() {
		BrokerIqMyProfileController.forceCorrectPassword = true;
		String newEmail = 'notanemailaddress';
		controller.thisUser.Email = newEmail;

		controller.save();

		User u = [SELECT Id, Email, Username FROM User WHERE Id  = :UserInfo.getUserId() ];
		System.assertNotEquals(newEMail, u.Email);
	}
}