public with sharing class CpdTestCloneExtension {

    private CPD_Test__c theTest;
    
    public CpdTestCloneExtension(ApexPages.StandardController controller) {
        Id cpdTestId = controller.getId();
        String cpdTestQuery = Nebula_Tools.FieldSetUtils.queryAllFields('CPD_Test__c', 'Id = :cpdTestId');
        List<CPD_Test__c> theTestL = Database.query(cpdTestQuery);
        if(theTestL.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No CPD Test Id supplied'));
        } else {
            theTest = theTestL[0];
        }
    }
    
    public PageReference doClone() {
        
        if(theTest == null) {
            return null;
        }
        
        CPD_Test__c cloneTest = theTest.clone();
        
        SavePoint sp = Database.setSavepoint();
        try {
            cloneTest.Status__c = 'Draft';
        
            insert cloneTest;
            Id theTestId = theTest.Id;
            String questionQuery = Nebula_Tools.FieldSetUtils.queryAllFields('CPD_Test_Question__c', 'CPD_Test__c = :theTestId');
            List<CPD_Test_Question__c> questions = Database.query(questionQuery);
            List<CPD_Test_Question__c> cloneQuestions = new List<CPD_Test_Question__c>();
            
            for(CPD_Test_Question__c q : questions) {
                CPD_Test_Question__c qClone = q.clone();
                qClone.CPD_Test__c = cloneTest.Id;
                cloneQuestions.add(qClone);
            }
            
            insert cloneQuestions;
        } catch(Exception e) {
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
        }
        return new PageReference('/' + cloneTest.Id);
    }
}