/**
* @name: EWInsurableItemHelperTest
* @description: Test class for EWInsurableItemHelper
* @author: Steve Loftus sloftus@azuruw.com
* @date: 24/09/2018
*/
@isTest
public with sharing class EWInsurableItemHelperTest {

    private static User systemAdminUser;
    private static Account broker;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static Product2 product;
    private static vlocity_ins__InsurableItem__c originalInsurableItem;
    private static vlocity_ins__InsurableItem__c newInsurableItem;
    private static EWInsurableItemHelper.PropertyItemDetails propertyItem;
    private static EWInsurableItemHelper.PropertyItemResults propertyResults;
    private static EWInsurableItemHelper.SpecifiedItemsDetails specifiedItemsDetails;
    private static List<EWInsurableItemHelper.SpecifiedItem> specifiedItemList = new List<EWInsurableItemHelper.SpecifiedItem>();

    @isTest static void PostPropertyItemNoOriginalTest() {
        setup(true, false);

        Test.startTest();

        System.runAs(systemAdminUser) {
            propertyResults = EWInsurableItemHelper.PostPropertyItem(propertyItem);
        }

        Test.stopTest();

        List<vlocity_ins__InsurableItem__c> insurableItems = [SELECT Id FROM vlocity_ins__InsurableItem__c];

        System.assertEquals(1, insurableItems.size(), 'Should be just 1 item');
    }

    @isTest static void PostPropertyItemWithOriginalTest() {
        setup(true, true);

        Test.startTest();

        System.runAs(systemAdminUser) {
            propertyResults = EWInsurableItemHelper.PostPropertyItem(propertyItem);
        }

        Test.stopTest();

        List<vlocity_ins__InsurableItem__c> insurableItems = [SELECT Id FROM vlocity_ins__InsurableItem__c];

        System.assertEquals(2, insurableItems.size(), 'Should be 2 items');
    }

    @isTest static void enrichSpecifiedItemsTest() {
        setup(false, true);

        Test.startTest();

        System.assertEquals(1, specifiedItemList.size());
        System.runAs(systemAdminUser) {
            EWInsurableItemHelper.enrichSpecifiedItems(specifiedItemsDetails, specifiedItemList);
        }

        Test.stopTest();

        List<vlocity_ins__InsurableItem__c> insurableItems = [SELECT Id FROM vlocity_ins__InsurableItem__c];

        System.assertEquals(1, insurableItems.size(), 'Should be just 1 item');
    }

    @isTest static void testTest() {
        setup(true, false);
        Test.startTest();

        System.runAs(systemAdminUser) {
            propertyResults = EWInsurableItemHelper.PostPropertyItem(propertyItem);

            List<vlocity_ins__InsurableItem__c> insurableItems = [
                    SELECT Id, EW_YearBuilt__c
                    FROM vlocity_ins__InsurableItem__c
            ];

            EW_QuoteJSONData__c quoteJSONData = new EW_QuoteJSONData__c();

            quoteJSONData.EW_ClientNode__c = '{"message": "OK"}';
            quoteJSONData.EW_PropertyNode__c = '{"message": "OK"}';
            quoteJSONData.EW_AmbNode__c = '{"message": "OK"}';
            quoteJSONData.EW_SubsNode__c = '{"message": "OK"}';
            quoteJSONData.EW_OutraNode__c = '{"message": "OK"}';
            quoteJSONData.EW_CoverageNode__c = '{"message": "OK"}';
            quoteJSONData.EW_RepricingUserInputsNode__c = '{"message": "OK"}';
            quoteJSONData.EW_RepricingInsuredItemsNode__c = '{"insuredProperty": {"message": "OK"}}';
            quoteJSONData.EW_Quote__c = quote.Id;
            insert quoteJSONData;

            String newYearBuildValue = '1919';

            insurableItems.get(0).EW_YearBuilt__c = newYearBuildValue;
            update insurableItems;

            quoteJSONData = EWJSONHelper.getQuoteJSONDataRecord(quote.Id);

            System.assert(quoteJSONData.EW_OutraNode__c.contains(newYearBuildValue));
            System.assert(quoteJSONData.EW_RepricingUserInputsNode__c.contains(newYearBuildValue));
            System.assert(quoteJSONData.EW_RepricingInsuredItemsNode__c.contains(newYearBuildValue));
        }

        Test.stopTest();
    }


    /**
    * @methodName: testCheckMainResidenceUPRNpositive
    * @description: positive test method for EWInsurableItemHelper.checkMainResidenceUPRN method
    * @author: Roy Lloyd
    * @dateCreated: 27/2/2020
    */
    static testMethod void testCheckMainResidenceUPRNpositive(){

        setup(true, false);
        vlocity_ins__InsurableItem__c mainResidence;

        String testPostcode = 'BR6 6BN';

        System.runAs(systemAdminUser) {

            Id propertyRTId = vlocity_ins__InsurableItem__c.getSObjectType().getDescribe()
                    .getRecordTypeInfosByDeveloperName().get(EWConstants.INSURABLE_ITEM_RT_DEV_NAME_PROPERTY).getRecordTypeId();

            mainResidence = new vlocity_ins__InsurableItem__c(
                    EW_Quote__c = quote.Id,
                    EW_Type__c = 'House',
                    Name = 'Main residence',
                    RecordTypeId = propertyRTId,
                    EW_PostalCode__c = testPostcode,
                    EW_Active__c = true
            );
            insert mainResidence;
        }

        Map<String, Object> inputMap = new Map<String, Object>{
                'outraAPIResponse' =>  new Map<String,Object>{'postcode' => testPostcode},
                'ambAPIResponse' =>  new Map<String,Object>{'postcode' => testPostcode}
        };
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{'boundQuoteId' => quote.Id};

        Test.startTest();

        // positive test

        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        EWVlocityRemoteActionHelper.noAzurLog = true;
        raHelper.invokeMethod('checkMainResidenceUPRN', inputMap, outputMap, optionMap);

        Boolean response = (Boolean) outputMap.get('renewalUPRNnotRight');
        System.assertEquals(false, response);

        Test.stopTest();

    }

    /**
    * @methodName: testCheckMainResidenceUPRNnegative
    * @description: negative test method for EWInsurableItemHelper.checkMainResidenceUPRN method
    * @author: Roy Lloyd
    * @dateCreated: 27/2/2020
    */
    static testMethod void testCheckMainResidenceUPRNnegative(){

        setup(true, false);
        vlocity_ins__InsurableItem__c mainResidence;

        String testPostcode = 'BR6 6BN';

        System.runAs(systemAdminUser) {

            Id propertyRTId = vlocity_ins__InsurableItem__c.getSObjectType().getDescribe()
                    .getRecordTypeInfosByDeveloperName().get(EWConstants.INSURABLE_ITEM_RT_DEV_NAME_PROPERTY).getRecordTypeId();

            mainResidence = new vlocity_ins__InsurableItem__c(
                    EW_Quote__c = quote.Id,
                    EW_Type__c = 'House',
                    Name = 'Main residence',
                    RecordTypeId = propertyRTId,
                    EW_PostalCode__c = 'AB12 3CD',
                    EW_Active__c = true
            );
            insert mainResidence;
        }

        Map<String, Object> inputMap = new Map<String, Object>{
                'outraAPIResponse' =>  new Map<String,Object>{'postcode' => testPostcode},
                'ambAPIResponse' =>  new Map<String,Object>{'postcode' => testPostcode}
        };
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{'boundQuoteId' => quote.Id};

        Test.startTest();

        // negative test

        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        EWVlocityRemoteActionHelper.noAzurLog = true;
        raHelper.invokeMethod('checkMainResidenceUPRN', inputMap, outputMap, optionMap);

        Boolean response = (Boolean) outputMap.get('renewalUPRNnotRight');
        System.assertEquals(true, response);

        Test.stopTest();

    }



    static void setup(Boolean isPropertyItem, Boolean haveOriginalInsurableItem) {

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                                    EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                                    techteamUserRole.Id,
                                    true);

        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);

            insured = EWAccountDataFactory.createInsuredAccount(
                                                    'Mr',
                                                    'Test',
                                                    'Insured',
                                                    Date.newInstance(1969, 12, 29),
                                                    true);

            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, false);
            quote.Status = 'Draft';
            insert quote;

            product = EWProductDataFactory.createProduct(null, true);

            Id recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                                                                            'vlocity_ins__InsurableItem__c',
                                                                            (isPropertyItem)
                                                                            ? EWConstants.INSURABLE_ITEM_RECORD_TYPE_EW_PROPERTY
                                                                            : EWConstants.INSURABLE_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM);

            if (haveOriginalInsurableItem) {
                originalInsurableItem = EWInsurableItemDataFactory.createInsurableItem(null,
                                                                                       broker.Id,
                                                                                       insured.Id,
                                                                                       product.Id,
                                                                                       quote.Id,
                                                                                       recordTypeId,
                                                                                       true,
                                                                                       true);
            }

            if (isPropertyItem) {

                propertyItem = new EWInsurableItemHelper.PropertyItemDetails();
                propertyItem.recordId = (haveOriginalInsurableItem) ? originalInsurableItem.Id : null;
                propertyItem.name = 'testInsurableItem2';
                propertyItem.brokerAccountId = broker.Id;
                propertyItem.insuredAccountId = insured.Id;
                propertyItem.quoteId = quote.Id;
                propertyItem.masterProductId = product.Id;
                propertyItem.recordTypeId = recordTypeId;

            } else {

                EWInsurableItemHelper.SpecifiedItem specifiedItem = new EWInsurableItemHelper.SpecifiedItem();
                specifiedItem.itemId = (haveOriginalInsurableItem) ? originalInsurableItem.Id : null;
                specifiedItem.recordTypeId = recordTypeId;
                specifiedItem.textItemName = 'testInsurableItem2';
                specifiedItemList.add(specifiedItem);

                specifiedItemsDetails = new EWInsurableItemHelper.SpecifiedItemsDetails();
                specifiedItemsDetails.brokerAccountId = broker.Id;
                specifiedItemsDetails.insuredAccountId = insured.Id;
                specifiedItemsDetails.quoteId = quote.Id;
                specifiedItemsDetails.masterProductId = product.Id;

            }
        }
    }
}