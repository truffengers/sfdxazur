import { LightningElement, track } from 'lwc';

export default class Ew_loading extends LightningElement {
    @track progressText = 'Verifying client details...';

    connectedCallback() {
        this.setAttribute('hidden', '');
        // this.initiateAnimation();
    }

    initiateAnimation() {
        const progressTexts = [
            'Looking up property information...',
            'Evaluating property risk...',
            'Asking the underwriters for a few favours...',
            'Looking up flood information...',
            'Looking up theft information...',
            'Reimagining insurance...',
            'Checking underwriting rules...',
            'Maybe go and get yourself a cup of tea...',
            'Evaluating prior claims risk...',
            'Evaluating overall risk...',
            'Generating unique quote...',
            'Compiling coverage table...',
            'Almost there, we promise...',
            'Calculating premium...',
            'Running out of excuses...',
            'Writing up coverages and limits...',
            'Adding the final flourishes...'
        ];

        let progressTextsCounter = 0;
        const textAnim = () => {
            if (progressTextsCounter > progressTexts.length - 1) {
                progressTextsCounter = 0; // eslint-disable-line
            }

            this.progressText = progressTexts[progressTextsCounter];
            return progressTextsCounter++;
        }

        setInterval(textAnim, 5000); // eslint-disable-line
    }

    set hidden(val) {
        if (val) {
            this.setAttribute('hidden', '');
        } else {
            this.removeAttribute('hidden');
            this.initiateAnimation();
        }
    }
}