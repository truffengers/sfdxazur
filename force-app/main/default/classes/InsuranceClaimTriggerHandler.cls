public with sharing class InsuranceClaimTriggerHandler implements ITriggerHandler {
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return InsuranceClaimTriggerHandler.IsDisabled != null ? InsuranceClaimTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
        EWClaimsHelper.setIfClaimsUnderAzurInsOrNot((List<vlocity_ins__InsuranceClaim__c>) newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
        EWClaimsHelper.checkForOpenRenewalQuotes(((List<vlocity_ins__InsuranceClaim__c>) newItems.values()));

    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
        EWClaimsHelper.updateQuoteJSONDataAfterClaimDelete((Map<Id, vlocity_ins__InsuranceClaim__c>) oldItems);
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
    }
}