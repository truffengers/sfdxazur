/**
* @name: AZIntermediationPipelineHelper
* @description: Helper class for Remote Action calls from Vlocity
* @author: Azur Group
* @date: 03/01/2021
*/
global with sharing class AZIntermediationPipelineHelper implements vlocity_ins.VlocityOpenInterface {
    
    public static Boolean noAzurLog = false;
    
    public Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> optionMap) {
        
        Boolean result = true;
        integer maxSize = 32767;
        
        noAzurLog = optionMap.get('skipLogCreation') != null ? (Boolean) optionMap.get('skipLogCreation') : false;
        Azur_Log__c traceLog = new Azur_Log__c();
        
        try {
            switch on methodName {
                when 'generateBudgetRecords' {
                    generateBudgetRecords(inputMap, outputMap, optionMap);
                }
                when 'createBudgetVersion' {
                    createBudgetVersion(inputMap, outputMap, optionMap);
                }
                when else {
                    result = false;
                }
            }
            
        } catch (Exception e) {
            LogBuilder logBuilder = new LogBuilder();
            
            Azur_Log__c log = logBuilder.createGenericLog(e);
            log.Class__c = AZIntermediationPipelineHelper.class.getName();
            log.Method__c = 'invokeMethod(\'' + methodName + '\')';
            log.Log_message__c = log.Log_message__c + '\n\n method name = ' + methodName;
            log.Log_message__c += '\n\nln: ' + e.getLineNumber();
            log.Log_message__c += '\n\nst: ' + e.getStackTraceString();
            
            System.debug('The following exception has occurred: ' + e.getMessage());
            insert log;
            
            result = false;
            
        } finally {
            String additionalLogMessage = traceLog.Log_message__c + '\n\n' + 'outputMap:' + '\n' + JSON.serializePretty(outputMap);
            
            if (additionalLogMessage.length() > maxSize) {
                traceLog.Log_message__c = additionalLogMessage.substring(0, maxSize);
            } else {
                traceLog.Log_message__c = additionalLogMessage;
            }
            if(!noAzurLog) insert traceLog;
        }
        
        return result;
    }
    
    private static String generateBudgetRecords(
        Map<String, Object> inputMap,
        Map<String, Object> outputMap,
        Map<String, Object> optionMap) {
            
            String budgetId = (String)inputMap.get('ContextId');
            Map<String,String> values = new Map<String,String>();
            Map<Id, AZ_BudgetLineItem__c> priorBudget = new Map<Id, AZ_BudgetLineItem__c>();
            Set<Id> accountList = new Set<Id>();
            List<AZ_BudgetLineItem__c> budgetLines = new List<AZ_BudgetLineItem__c>();
            
            //Check if there are any existing line items attached to this budget and delete them. 
            List<AZ_BudgetLineItem__c> lineItems = [SELECT Id FROM AZ_BudgetLineItem__c WHERE AZ_Budget__r.Id = :budgetId];
            if(lineItems != null){
                Delete lineItems;
            }
            
            //Get budget AZ_Budget__c record to get values to be used in the formulas below for newBusinessWeighting and renewalWeighting
            AZ_Budget__c budgetRecord = [SELECT AZ_Product__c, AZ_BudgetYear__c,AZ_NewBusinessRate__c, AZ_PremiumAdjustmentRate__c, AZ_CancellationRate__c,AZ_RetentionRate__c, AZ_BrokerCommissionNewBiz__c, AZ_BrokerCommissionRenewal__c 
                                         FROM AZ_Budget__c
                                         WHERE Id = :budgetId
                                         LIMIT 1];
            String budgetProduct = budgetRecord.AZ_Product__c;
            String productCode = [SELECT Id, IBA_AIGProduct__c FROM Product2 WHERE Id = :budgetProduct LIMIT 1].IBA_AIGProduct__c;
            Integer year = Integer.valueOf(budgetRecord.AZ_BudgetYear__c) - 1;
            Integer currentMonth = System.today().month();
            String sYear = String.valueOf(year);
            
            // Need to get previous years budget records for use in current months
            String lineItemsFields = 'Id, AZ_AprNewBusiness__c, AZ_AprRenewals__c, AZ_AprTotal__c, AZ_AugNewBusiness__c, AZ_AugRenewals__c, '
                + 'AZ_Broker__c, AZ_Budget__c, CurrencyIsoCode, AZ_DecNewBusiness__c, AZ_DecRenewals__c, AZ_FebNewBusiness__c, AZ_FebRenewals__c, '
                + 'AZ_JanNewBusiness__c, AZ_JanRenewals__c, AZ_JulNewBusiness__c, AZ_JulRenewals__c, AZ_JunNewBusiness__c, AZ_JunRenewals__c, '
                + 'AZ_MarNewBusiness__c, AZ_MarRenewals__c, AZ_MayNewBusiness__c, AZ_MayRenewals__c, AZ_NovNewBusiness__c, AZ_NovRenewals__c, '
                + 'AZ_OctNewBusiness__c, AZ_OctRenewals__c, AZ_SepNewBusiness__c, AZ_SepRenewals__c';
            
            String lineItemsQuery = 'SELECT ' +  lineItemsFields +  ' FROM AZ_BudgetLineItem__c WHERE AZ_Budget__r.AZ_Product__c = :budgetProduct AND AZ_Budget__r.AZ_BudgetYear__c = :sYear AND AZ_Budget__r.AZ_LatestVersion__c = true ';
            AZ_BudgetLineItem__c[] lineItemsList = Database.query(lineItemsQuery);

            for (AZ_BudgetLineItem__c aresult : lineItemsList)  {
                accountList.add(aresult.AZ_Broker__c);
                priorBudget.put(aresult.AZ_Broker__c, aresult);
            }
            
            //Get aggregated records from FF Policy Transactions
            AggregateResult[] groupedResults
                = [SELECT IBA_OriginalPolicy__r.IBA_ProducingBrokerAccount__c, IBA_TransactionType__c ,SUM(EPM_GBPTotalGWP__c), calendar_month(IBA_OriginalPolicy__r.vlocity_ins__InceptionDate__c), SUM(EPM_GBPTotalIPT__c)
                   FROM IBA_PolicyTransaction__c 
                   WHERE IBA_ClassofBusiness__c = :productCode AND calendar_year(IBA_OriginalPolicy__r.vlocity_ins__InceptionDate__c) = :year AND IBA_OriginalPolicy__c != NULL AND IBA_OriginalPolicy__r.IBA_ProducingBrokerAccount__c != NULL
                   GROUP BY ROLLUP (IBA_OriginalPolicy__r.IBA_ProducingBrokerAccount__c, IBA_TransactionType__c, calendar_month(IBA_OriginalPolicy__r.vlocity_ins__InceptionDate__c)) ];
            
            //Add the results to 2 lists - Set of accountIds and map of values for each accountId
            for (AggregateResult ar : groupedResults)  {
                accountList.add((Id)ar.get('IBA_ProducingBrokerAccount__c'));
                Integer expr0, expr2;
                if (ar.get('expr0') == null)
                    expr0 = 0;
                else 
                    expr0 = Integer.valueOf(ar.get('expr0'));

                if (ar.get('expr2') == null)
                    expr2 = 0;
                else 
                    expr2 = Integer.valueOf(ar.get('expr2'));

                values.put((String)ar.get('IBA_ProducingBrokerAccount__c')+'|'+String.valueOf(ar.get('expr1'))+'|'+(String)ar.get('IBA_TransactionType__c'), String.valueOf(expr0 + expr2));
                
            }
            //Loop through the list of accounts and calculate the additional premiums
            for (Id acc : accountList){
                
                //Get new business, MTA and Cancellation values
                Decimal janNewBusiness = values.containsKey(acc+'|1|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|1|ORIGINAL PREMIUM')) : 0;
                Decimal febNewBusiness = values.containsKey(acc+'|2|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|2|ORIGINAL PREMIUM')) : 0;
                Decimal marNewBusiness = values.containsKey(acc+'|3|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|3|ORIGINAL PREMIUM')) : 0;
                Decimal aprNewBusiness = values.containsKey(acc+'|4|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|4|ORIGINAL PREMIUM')) : 0;
                Decimal mayNewBusiness = values.containsKey(acc+'|5|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|5|ORIGINAL PREMIUM')) : 0;
                Decimal junNewBusiness = values.containsKey(acc+'|6|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|6|ORIGINAL PREMIUM')) : 0;
                Decimal julNewBusiness = values.containsKey(acc+'|7|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|7|ORIGINAL PREMIUM')) : 0;
                Decimal augNewBusiness = values.containsKey(acc+'|8|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|8|ORIGINAL PREMIUM')) : 0;
                Decimal sepNewBusiness = values.containsKey(acc+'|9|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|9|ORIGINAL PREMIUM')) : 0;
                Decimal octNewBusiness = values.containsKey(acc+'|10|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|10|ORIGINAL PREMIUM')) : 0;
                Decimal novNewBusiness = values.containsKey(acc+'|11|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|11|ORIGINAL PREMIUM')) : 0;
                Decimal decNewBusiness = values.containsKey(acc+'|12|ORIGINAL PREMIUM') ? Decimal.valueOf(values.get(acc+'|12|ORIGINAL PREMIUM')) : 0;

                Decimal janMta = values.containsKey(acc+'|1|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|1|ENDORSEMENT')) : 0;
                Decimal febMta = values.containsKey(acc+'|2|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|2|ENDORSEMENT')) : 0;
                Decimal marMta = values.containsKey(acc+'|3|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|3|ENDORSEMENT')) : 0;
                Decimal aprMta = values.containsKey(acc+'|4|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|4|ENDORSEMENT')) : 0;
                Decimal mayMta = values.containsKey(acc+'|5|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|5|ENDORSEMENT')) : 0;
                Decimal junMta = values.containsKey(acc+'|6|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|6|ENDORSEMENT')) : 0;
                Decimal julMta = values.containsKey(acc+'|7|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|7|ENDORSEMENT')) : 0;
                Decimal augMta = values.containsKey(acc+'|8|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|8|ENDORSEMENT')) : 0;
                Decimal sepMta = values.containsKey(acc+'|9|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|9|ENDORSEMENT')) : 0;
                Decimal octMta = values.containsKey(acc+'|10|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|10|ENDORSEMENT')) : 0;
                Decimal novMta = values.containsKey(acc+'|11|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|11|ENDORSEMENT')) : 0;
                Decimal decMta = values.containsKey(acc+'|12|ENDORSEMENT') ? Decimal.valueOf(values.get(acc+'|12|ENDORSEMENT')) : 0;
                
                Decimal janCancellation = values.containsKey(acc+'|1|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|1|CANCELLATION')) : 0;
                Decimal febCancellation = values.containsKey(acc+'|2|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|2|CANCELLATION')) : 0;
                Decimal marCancellation = values.containsKey(acc+'|3|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|3|CANCELLATION')) : 0;
                Decimal aprCancellation = values.containsKey(acc+'|4|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|4|CANCELLATION')) : 0;
                Decimal mayCancellation = values.containsKey(acc+'|5|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|5|CANCELLATION')) : 0;
                Decimal junCancellation = values.containsKey(acc+'|6|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|6|CANCELLATION')) : 0;
                Decimal julCancellation = values.containsKey(acc+'|7|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|7|CANCELLATION')) : 0;
                Decimal augCancellation = values.containsKey(acc+'|8|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|8|CANCELLATION')) : 0;
                Decimal sepCancellation = values.containsKey(acc+'|9|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|9|CANCELLATION')) : 0;
                Decimal octCancellation = values.containsKey(acc+'|10|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|10|CANCELLATION')) : 0;
                Decimal novCancellation = values.containsKey(acc+'|11|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|11|CANCELLATION')) : 0;
                Decimal decCancellation = values.containsKey(acc+'|12|CANCELLATION') ? Decimal.valueOf(values.get(acc+'|12|CANCELLATION')) : 0;
                
                Decimal jannull = values.containsKey(acc+'|1|null') ? Decimal.valueOf(values.get(acc+'|1|null')) : 0;
                Decimal febnull = values.containsKey(acc+'|2|null') ? Decimal.valueOf(values.get(acc+'|2|null')) : 0;
                Decimal marnull = values.containsKey(acc+'|3|null') ? Decimal.valueOf(values.get(acc+'|3|null')) : 0;
                Decimal aprnull = values.containsKey(acc+'|4|null') ? Decimal.valueOf(values.get(acc+'|4|null')) : 0;
                Decimal maynull = values.containsKey(acc+'|5|null') ? Decimal.valueOf(values.get(acc+'|5|null')) : 0;
                Decimal junnull = values.containsKey(acc+'|6|null') ? Decimal.valueOf(values.get(acc+'|6|null')) : 0;
                Decimal julnull = values.containsKey(acc+'|7|null') ? Decimal.valueOf(values.get(acc+'|7|null')) : 0;
                Decimal augnull = values.containsKey(acc+'|8|null') ? Decimal.valueOf(values.get(acc+'|8|null')) : 0;
                Decimal sepnull = values.containsKey(acc+'|9|null') ? Decimal.valueOf(values.get(acc+'|9|null')) : 0;
                Decimal octnull = values.containsKey(acc+'|10|null') ? Decimal.valueOf(values.get(acc+'|10|null')) : 0;
                Decimal novnull = values.containsKey(acc+'|11|null') ? Decimal.valueOf(values.get(acc+'|11|null')) : 0;
                Decimal decnull = values.containsKey(acc+'|12|null') ? Decimal.valueOf(values.get(acc+'|12|null')) : 0;
                
                Decimal janNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_JanNewBusiness__c : 0;
                Decimal febNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_FebNewBusiness__c : 0;
                Decimal marNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_MarNewBusiness__c : 0;
                Decimal aprNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_AprNewBusiness__c : 0;
                Decimal mayNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_MayNewBusiness__c : 0;
                Decimal junNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_JunNewBusiness__c : 0;
                Decimal julNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_JulNewBusiness__c : 0;
                Decimal augNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_AugNewBusiness__c : 0;
                Decimal sepNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_SepNewBusiness__c : 0;
                Decimal octNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_OctNewBusiness__c : 0;
                Decimal novNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_NovNewBusiness__c : 0;
                Decimal decNewBiz = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_DecNewBusiness__c : 0;
                
                //Get Renewal actuals
                Decimal janRenewal = values.containsKey(acc+'|1|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|1|MANUAL RENEWAL')) : 0;
                Decimal febRenewal = values.containsKey(acc+'|2|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|2|MANUAL RENEWAL')) : 0;
                Decimal marRenewal = values.containsKey(acc+'|3|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|3|MANUAL RENEWAL')) : 0;
                Decimal aprRenewal = values.containsKey(acc+'|4|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|4|MANUAL RENEWAL')) : 0;
                Decimal mayRenewal = values.containsKey(acc+'|5|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|5|MANUAL RENEWAL')) : 0;
                Decimal junRenewal = values.containsKey(acc+'|6|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|6|MANUAL RENEWAL')) : 0;
                Decimal julRenewal = values.containsKey(acc+'|7|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|7|MANUAL RENEWAL')) : 0;
                Decimal augRenewal = values.containsKey(acc+'|8|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|8|MANUAL RENEWAL')) : 0;
                Decimal sepRenewal = values.containsKey(acc+'|9|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|9|MANUAL RENEWAL')) : 0;
                Decimal octRenewal = values.containsKey(acc+'|10|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|10|MANUAL RENEWAL')) : 0;
                Decimal novRenewal = values.containsKey(acc+'|11|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|11|MANUAL RENEWAL')) : 0;
                Decimal decRenewal = values.containsKey(acc+'|12|MANUAL RENEWAL') ? Decimal.valueOf(values.get(acc+'|12|MANUAL RENEWAL')) : 0;
                
                //Get Renewal previous budget
                Decimal janRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_JanRenewals__c : 0;
                Decimal febRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_FebRenewals__c : 0;
                Decimal marRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_MarRenewals__c : 0;
                Decimal aprRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_AprRenewals__c : 0;
                Decimal mayRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_MayRenewals__c : 0;
                Decimal junRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_JunRenewals__c : 0;
                Decimal julRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_JulRenewals__c : 0;
                Decimal augRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_AugRenewals__c : 0;
                Decimal sepRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_SepRenewals__c : 0;
                Decimal octRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_OctRenewals__c : 0;
                Decimal novRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_NovRenewals__c : 0;
                Decimal decRenew = priorBudget.containsKey(acc) ? priorbudget.get(acc).AZ_DecRenewals__c : 0;
                
                //Pulling in values from the AZ_Budget__c record
                Decimal newBusinessWeighting = 1 * (1 + budgetRecord.AZ_NewBusinessRate__c/100);
                Decimal renewalWeighting = 1 * ((budgetRecord.AZ_RetentionRate__c/100) * (1 - budgetRecord.AZ_CancellationRate__c/100) * (1 + budgetRecord.AZ_PremiumAdjustmentRate__c/100));
                
                
                //Create a line item record
                AZ_BudgetLineItem__c bud = new AZ_BudgetLineItem__c();
                bud.AZ_Broker__c = acc;
                bud.AZ_Budget__c = budgetId;
                bud.AZ_BrokerCommissionNewBusiness__c = budgetRecord.AZ_BrokerCommissionNewBiz__c;
                bud.AZ_BrokerCommissionRenewals__c = budgetRecord.AZ_BrokerCommissionRenewal__c;
                
                bud.AZ_JanNewBusiness__c  = currentMonth > 1 ? newBusinessWeighting * janNewBusiness : newBusinessWeighting * janNewBiz; 
                bud.AZ_FebNewBusiness__c  = currentMonth > 2 ? newBusinessWeighting * febNewBusiness : newBusinessWeighting * febNewBiz; 
                bud.AZ_MarNewBusiness__c  = currentMonth > 3 ? newBusinessWeighting * marNewBusiness : newBusinessWeighting * marNewBiz; 
                bud.AZ_AprNewBusiness__c  = currentMonth > 4 ? newBusinessWeighting * aprNewBusiness : newBusinessWeighting * aprNewBiz; 
                bud.AZ_MayNewBusiness__c  = currentMonth > 5 ? newBusinessWeighting * mayNewBusiness : newBusinessWeighting * mayNewBiz; 
                bud.AZ_JunNewBusiness__c  = currentMonth > 6 ? newBusinessWeighting * junNewBusiness : newBusinessWeighting * junNewBiz; 
                bud.AZ_JulNewBusiness__c  = currentMonth > 7 ? newBusinessWeighting * julNewBusiness : newBusinessWeighting * julNewBiz; 
                bud.AZ_AugNewBusiness__c  = currentMonth > 8 ? newBusinessWeighting * augNewBusiness : newBusinessWeighting * augNewBiz; 
                bud.AZ_SepNewBusiness__c  = currentMonth > 9 ? newBusinessWeighting * sepNewBusiness : newBusinessWeighting * sepNewBiz; 
                bud.AZ_OctNewBusiness__c  = currentMonth > 10 ? newBusinessWeighting * octNewBusiness : newBusinessWeighting * octNewBiz; 
                bud.AZ_NovNewBusiness__c  = currentMonth > 11 ? newBusinessWeighting * novNewBusiness : newBusinessWeighting * novNewBiz; 
                bud.AZ_DecNewBusiness__c  = currentMonth > 12 ? newBusinessWeighting * decNewBusiness : newBusinessWeighting * decNewBiz; 

                bud.AZ_JanRenewals__c =  currentMonth > 1 ? (janNewBusiness + janMta + janCancellation + jannull + janRenewal) * renewalWeighting : renewalWeighting * (janNewBiz + janRenew);
                bud.AZ_FebRenewals__c =  currentMonth > 2 ? (febNewBusiness + febMta + febCancellation + febnull + febRenewal) * renewalWeighting : renewalWeighting * (febNewBiz + febRenew);
                bud.AZ_MarRenewals__c =  currentMonth > 3 ? (marNewBusiness + marMta + marCancellation + marnull + marRenewal) * renewalWeighting : renewalWeighting * (marNewBiz + marRenew);
                bud.AZ_AprRenewals__c =  currentMonth > 4 ? (aprNewBusiness + aprMta + aprCancellation + aprnull + aprRenewal) * renewalWeighting : renewalWeighting * (aprNewBiz + aprRenew);
                bud.AZ_MayRenewals__c =  currentMonth > 5 ? (mayNewBusiness + mayMta + mayCancellation + maynull + mayRenewal) * renewalWeighting : renewalWeighting * (mayNewBiz + mayRenew);
                bud.AZ_JunRenewals__c =  currentMonth > 6 ? (junNewBusiness + junMta + junCancellation + junnull + junRenewal) * renewalWeighting : renewalWeighting * (junNewBiz + junRenew);
                bud.AZ_JulRenewals__c =  currentMonth > 7 ? (julNewBusiness + julMta + julCancellation + julnull + julRenewal) * renewalWeighting : renewalWeighting * (julNewBiz + julRenew);
                bud.AZ_AugRenewals__c =  currentMonth > 8 ? (augNewBusiness + augMta + augCancellation + augnull + augRenewal) * renewalWeighting : renewalWeighting * (augNewBiz + augRenew); 
                bud.AZ_SepRenewals__c =  currentMonth > 9 ? (sepNewBusiness + sepMta + sepCancellation + sepnull + sepRenewal) * renewalWeighting : renewalWeighting * (sepNewBiz + sepRenew);
                bud.AZ_OctRenewals__c =  currentMonth > 10 ? (octNewBusiness + octMta + octCancellation + octnull + octRenewal) * renewalWeighting : renewalWeighting * (octNewBiz + octRenew);
                bud.AZ_NovRenewals__c =  currentMonth > 11 ? (novNewBusiness + novMta + novCancellation + novnull + novRenewal) * renewalWeighting : renewalWeighting * (novNewBiz + novRenew);
                bud.AZ_DecRenewals__c =  currentMonth > 12 ? (decNewBusiness + decMta + decCancellation + decnull + decRenewal) * renewalWeighting : renewalWeighting * (decNewBiz + decRenew);
                
                budgetLines.add(bud);  
            }
            
            //Insert line item records
            if (budgetLines.size() > 0){
                insert budgetLines;
            }
            outputMap.put('RunBudgetStatus','Success');
            return 'Success';
        }
    
    private static Boolean createBudgetVersion(
        Map<String, Object> inputMap,
        Map<String, Object> outputMap,
        Map<String, Object> optionMap){
            
            String budgetId = (String)inputMap.get('ContextId');
            
            String budgetFields = 'Id, Name,AZ_Product__c, AZ_BudgetYear__c, AZ_Version__c, AZ_AzurCommissionNewBiz__c, AZ_AzurCommissionRenewal__c, AZ_CancellationRate__c, '
                + 'AZ_Baselined__c,AZ_BrokerCommissionNewBiz__c,AZ_BrokerCommissionRenewal__c,AZ_GrossCommissionNewBiz__c,AZ_GrossCommissionRenewal__c, '
                + 'AZ_NewBusinessRate__c,AZ_PremiumAdjustmentRate__c,AZ_RetentionRate__c, AZ_Status__c';
            
            String lineItemFields = 'Id, AZ_AprNewBusiness__c, AZ_AprRenewals__c, AZ_AprTotal__c, AZ_AugNewBusiness__c, AZ_AugRenewals__c, '
                + 'AZ_Broker__c, AZ_Budget__c, CurrencyIsoCode, AZ_DecNewBusiness__c, AZ_DecRenewals__c, AZ_FebNewBusiness__c, AZ_FebRenewals__c, '
                + 'AZ_JanNewBusiness__c, AZ_JanRenewals__c, AZ_JulNewBusiness__c, AZ_JulRenewals__c, AZ_JunNewBusiness__c, AZ_JunRenewals__c, '
                + 'AZ_MarNewBusiness__c, AZ_MarRenewals__c, AZ_MayNewBusiness__c, AZ_MayRenewals__c, AZ_NovNewBusiness__c, AZ_NovRenewals__c, '
                + 'AZ_OctNewBusiness__c, AZ_OctRenewals__c, AZ_SepNewBusiness__c, AZ_SepRenewals__c';
            
            String actionFields = 'Id, Name, AZ_Description__c, AZ_Due_Date__c, Az_Status__c';
            
            List<AZ_BudgetLineItem__c> lineItemsCopy = new List<AZ_BudgetLineItem__c>();
            List<AZ_BudgetActions__c> actionsCopy    = new List<AZ_BudgetActions__c>();
            
            String query = 'SELECT ' +  budgetFields + ' FROM AZ_Budget__c WHERE Id = :budgetId ';
            AZ_Budget__c budgetRecord = Database.query(query);
            if(budgetRecord != null){
                String budgetYear = budgetRecord.AZ_BudgetYear__c;
                Id productId = budgetRecord.AZ_Product__c;
                
                //Query to get any other AZ_Budget__c records with the same productId and year to work out what the next version number should be
                Decimal latestVersion = 0;
                List<AZ_Budget__c> budgetList = [SELECT AZ_Version__c FROM AZ_Budget__c 
                                                 WHERE AZ_Product__c = :productId AND AZ_BudgetYear__c = :budgetYear];
                For (AZ_Budget__c loopBudget : budgetList){
                    if(loopBudget.AZ_Version__c > latestVersion){
                        latestVersion = loopBudget.AZ_Version__c;
                    }
                }
                //Clone the AZ_Budget__c record passed into method with budgetId. Change the version number to next number and status = draft
                AZ_Budget__c cloneBudget = budgetRecord.clone(false, true, false, false);
                cloneBudget.AZ_Version__c = latestVersion + 1;
                cloneBudget.AZ_Status__c = 'Draft';
                cloneBudget.AZ_LatestVersion__c = false;
                if(cloneBudget != null){
                    insert cloneBudget;
                    
                    //Clone the AZ_BudgetLineItem__c records linked to the original AZ_Budget__c record
                    String lineItemQuery = 'SELECT ' +  lineItemFields + ' FROM AZ_BudgetLineItem__c WHERE AZ_Budget__r.Id = :budgetId ';
                    AZ_BudgetLineItem__c[] lineItemsList = Database.query(lineItemQuery);
                    if (lineItemsList != null){
                        List<AZ_BudgetLineItem__c> lineItemsClone = lineItemsList.deepClone();
                        for(AZ_BudgetLineItem__c loopLineItem : lineItemsClone){
                            loopLineItem.AZ_Budget__c = cloneBudget.Id;
                            LineItemsCopy.add(loopLineItem);
                        }
                        if(LineItemsCopy != null)
                            insert LineItemsCopy;
                    }
                    
                    String actionsQuery = 'SELECT ' + actionFields + ' FROM AZ_BudgetActions__c WHERE AZ_Budget__r.Id = :budgetId ';
                    AZ_BudgetActions__c[] actionsList = Database.query(actionsQuery);
                    
                    if (actionsList != null){
                        List<AZ_BudgetActions__c> actionsClone = actionsList.deepClone();
                        for(AZ_BudgetActions__c loopActions : actionsClone){
                            loopActions.AZ_Budget__c = cloneBudget.Id;
                            actionsCopy.add(loopActions);
                        }
                        if(actionsCopy != null)
                            insert actionsCopy;
                    }
                    outputMap.put('newBudgetId', cloneBudget.Id);
                }
            }
            
            return true;
        }
}