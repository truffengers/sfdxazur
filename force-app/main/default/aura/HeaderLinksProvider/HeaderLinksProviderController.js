({
    doInit: function (component, event, helper) {
        let getHeaderLinksMetadataAction = component.get('c.getHeaderLinks');

        getHeaderLinksMetadataAction.setCallback(this, function (response) {
            const state = response.getState();
            if (state === "SUCCESS") {
                let headerLinksMetadata = response.getReturnValue();
                if (!headerLinksMetadata.azuruw_com) {
                    headerLinksMetadata.azuruw_com = '/';
                }
                if (!headerLinksMetadata.broker_iq) {
                    headerLinksMetadata.broker_iq = '/';
                }
                if (!headerLinksMetadata.broker_hub) {
                    headerLinksMetadata.broker_hub = '/';
                }

                component.set('v.headerLinksMetadata', headerLinksMetadata);
            }
        });

        $A.enqueueAction(getHeaderLinksMetadataAction);
    }
});