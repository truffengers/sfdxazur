/**
* @name: PHO_PhoenixStagingTriggerHandler
* @description: Trigger Handler for Phoenix Staging object
* @author: Unknown
* @lastChangedBy: Antigoni D'Mello  08/10/2020  PR-112: Added 'assignDRCode' on before insert
*/
public with sharing class PHO_PhoenixStagingTriggerHandler implements ITriggerHandler{
    public static Boolean isDisabled;

    public void BeforeInsert(List<SObject> newItems) {
        PHO_PhoenixStagingService.assignDRCode(newItems);
        PHO_PhoenixStagingService.phoToSFFieldMappingOnInsert(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        PHO_PhoenixStagingService.phoToSFFieldMappingOnUpdate(newItems, oldItems);
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
    }

    public Boolean IsDisabled() {
        return PHO_PhoenixStagingTriggerHandler.isDisabled != null
                ? PHO_PhoenixStagingTriggerHandler.isDisabled
                : false;
    }
}