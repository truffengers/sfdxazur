/**
 * Created by andrewxu on 21/05/2020.
 */

@IsTest
private class EWDuplicateBlockHelperTest {
    @IsTest
    static void testQueryAccounts() {
        Date testBirthdate = Date.newInstance(1979, 12, 12);

        Account testAccount = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Testfirstname',
                'Testlastname',
                testBirthDate,
                true
        );

        Test.startTest();

        List<Account> retrievedAccounts = EWDuplicateBlockHelper.queryAccounts(
                'Testfirstname',
                'Testlastname',
                Datetime.newInstance(1979, 12, 12, 00, 00, 00)
        );

        Test.stopTest();

        List<Account> expectedResults = [
                SELECT Id, EW_IsBlocked__c, EW_BrokerAccount__c
                FROM Account
                WHERE IsPersonAccount = TRUE
                AND FirstName = 'Testfirstname'
                AND LastName = 'Testlastname'
                AND PersonBirthdate = :testBirthdate
        ];

        System.assertEquals(expectedResults[0], retrievedAccounts[0], 'Account is fetched as expected');
    }

    @IsTest
    static void testQueryMainResidences() {
        Test.startTest();
        List<vlocity_ins__AssetInsuredItem__c> results = EWDuplicateBlockHelper.queryMainResidences('12345');
        Test.stopTest();

        System.assertEquals(1, results.size(), 'Main Residences are correctly queried');
    }

    @IsTest
    static void testQueryActivePolicies() {
        Asset policy = [SELECT Id FROM Asset LIMIT 1];

        Test.startTest();
        List<Asset> results = EWDuplicateBlockHelper.queryActivePolicies(policy.Id);
        Test.stopTest();

        System.assertEquals(1, results.size(), 'Live policies are correctly queried');
    }

    @testSetup static void setup() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );
        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.Status = 'Issued';
        policy.Policy_Status__c = 'Active';
        insert policy;

        String uprn = '12345';

        vlocity_ins__AssetInsuredItem__c residence = new vlocity_ins__AssetInsuredItem__c();
        residence.vlocity_ins__PolicyAssetId__c = policy.Id;
        residence.EW_UPRN__c = uprn;

        insert residence;
    }
}