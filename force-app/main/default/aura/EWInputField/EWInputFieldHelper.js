({
    validateUsernameSufix: function (component, value, isSubmit) {
        const brokerHubSuffix = '.brokerhub';
        let extendUsername = true;

        if (!value.endsWith(brokerHubSuffix)) {
            if (value.includes('.')) {
                const valueAfterLastDot = value.substring(value.lastIndexOf("."), value.length);
                if (valueAfterLastDot.length < brokerHubSuffix.length && brokerHubSuffix.startsWith(valueAfterLastDot)) {
                    const valueBeforeLastDot = value.substring(0, value.lastIndexOf("."));

                    component.set('v.value', valueBeforeLastDot + brokerHubSuffix);
                    extendUsername = false;
                }
            }
        } else {
            extendUsername = false;
        }

        if (extendUsername) {
            component.set('v.value', value + brokerHubSuffix);
        }

        if(isSubmit){
            component.getEvent("submit").fire();
        }

    }
});