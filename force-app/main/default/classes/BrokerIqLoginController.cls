public class BrokerIqLoginController {
    
    public String username {get; set;}
    public String password {get; set;}
    
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}
    
    public String startPath {get; set;}
    
    public BrokerIqLoginController() {
    }
    
    public PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        if(String.isBlank(startPath)) {
            startPath = startUrl;
        }
        Boolean tryLogin = true;
        if(username == null || username == '') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Username_Required));
            tryLogin = false;
        }
        if(password == null || password == '') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Password_Required));
            tryLogin = false;
        }
        if(tryLogin) {
            updateUsernameMaybeFromEmail();
            PageReference rval = Site.login(username, password, startPath);
            if(rval == null) {
                rval = ApexPages.currentPage();
                rval.getParameters().put('retry', 'true');
                return rval;
            }
            return rval;
        } else {
            PageReference rval = ApexPages.currentPage();
            rval.getParameters().put('retry', 'true');
            return rval;
        }
    }

    private String updateUsernameMaybeFromEmail() {
        List<User> usersByUsername = [SELECT Id FROM User WHERE Username = :username AND IsActive = true];
        if(usersByUsername.isEmpty()) {
            List<User> usersByEmail = [SELECT Id, Username FROM User WHERE Email = :username AND IsPortalEnabled = true AND IsActive = true];
            if(usersByEmail.size() == 1) {
                username = usersByEmail[0].Username;
            }
        }
        return username;
    }
    
    public void forgotPassword() {
        updateUsernameMaybeFromEmail();
        if(Site.forgotPassword(username)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.Password_Reset_Success));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Password_Reset_Fail));            
        }
    }

    public PageReference setPassword() {
		return Site.changePassword(newPassword, verifyNewPassword);        
    }    
}