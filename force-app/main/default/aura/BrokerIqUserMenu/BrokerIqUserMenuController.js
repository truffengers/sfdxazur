({
	componentInitJs : function(component, event, helper) {
        TT.dropdownMenu.init({
          selector: '#js-subdomain-menu, #js-user-menu',
          detectIt: detectIt
        });
    },
    handleCloseBurgerMenu : function(component, event, helper) {
	    if (component.get('v.burgerMenuOpen')) {
            component.set('v.burgerMenuOpen', false);
        }
    },
    toggleUserMenu : function(component, event, helper) {
	    userMenuCurrentlyOpen = component.get('v.userMenuOpen');
	    component.set('v.userMenuOpen', !userMenuCurrentlyOpen);
    },
    closeUserMenu : function(component, event, helper) {
	    component.set('v.userMenuOpen', false);
    }
})