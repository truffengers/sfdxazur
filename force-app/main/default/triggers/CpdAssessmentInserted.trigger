trigger CpdAssessmentInserted on CPD_Assessment__c (after insert) {

    Set<Id> cpdTestIds = new Set<Id>();
    
    for(CPD_Assessment__c thisAssessment : Trigger.new) {
        cpdTestIds.add(thisAssessment.CPD_Test__c);
    }
    
    Nebula_Tools.sObjectIndex questionsByAssessment = new Nebula_Tools.sObjectIndex(
    	'CPD_Test__c',
        [SELECT Id, CPD_Test__c
         FROM CPD_Test_Question__c
         WHERE CPD_Test__c IN :cpdTestIds]
    );
    
    List<CPD_Assessment_Answer__c> answersToInsert = new List<CPD_Assessment_Answer__c>();
    
    for(CPD_Assessment__c thisAssessment : Trigger.new) {
        List<CPD_Test_Question__c> qs = questionsByAssessment.getAll(thisAssessment.CPD_Test__c);
        
        for(CPD_Test_Question__c q : qs) {
            answersToInsert.add(new CPD_Assessment_Answer__c(CPD_Assessment__c = thisAssessment.Id,
                                                            CPD_Test_Question__c = q.Id));
        }
    }
    
    if(!answersToInsert.isEmpty()) {
        insert answersToInsert;
    }
}