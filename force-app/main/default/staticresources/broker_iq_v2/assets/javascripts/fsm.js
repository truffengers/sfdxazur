TT.fsm = (function($) {


  function null_logger() {
    return { info: function() {}, warn: function() {} };
  }


  function fsm_state(fsm_name, name, callback, error_callback, logger) {

    var self,
        transitions = {},
        entry_actions,
        exit_actions,
        then;



    function raise_error(msg) {
      error_callback(['State:', name].concat(msg));
    }

    function call_actions(type, actions, payload) {
      if(actions !== undefined) {
        $.each(actions, function(i,e) {
          logger.info(fsm_name.toUpperCase()+' '+type+' action: '+e);
          if(callback[e] === undefined) {
            raise_error(['Action method:', e, 'NOT DEFINED']);
          }
          callback[e].apply(null, payload || []);
        });
      }
    }



    self = {

      face: {

        entry: function(actions) {
          if(entry_actions !== undefined) {
            raise_error(['Entry actions ALREADY DEFINED']);
          }
          entry_actions = actions.constructor === Array ? actions : [actions];
          return self.face;
        },

        exiting: function(actions) {
          if(exit_actions !== undefined) {
            raise_error(['Exit actions ALREADY DEFINED']);
          }
          exit_actions = actions.constructor === Array ? actions : [actions];
          return self.face;
        },

        transition: function(trigger, state) {
          if(transitions[trigger] !== undefined) {
            raise_error('Transition:', trigger, 'ALREADY DEFINED');
          }
          transitions[trigger] = state;
          return self.face;
        },

        then: function(state) {
          if(then !== undefined) {
            raise_error('Then is ALREADY DEFINED');
          }
          then = state;
          return self.face;
        }

      },

      name: name,

      actions: function() {
        return (entry_actions || []).concat(exit_actions || []);
      },

      transition_states: function() {
        return $.map(transitions, function(v,k) {
          return v;
        });
      },

      enter: function(payload) {
        call_actions('Entry', entry_actions, payload);
      },

      exit: function(payload) {
        call_actions('Exit', exit_actions, payload);
      },

      state_from_trigger: function(trigger) {
        return transitions[trigger];
      },

      then: function() {
        return then;
      }

    };



    return self;
  };





  function fsm(name, callback, logger) {

    var self,
        states = {},
        state,
        first_state_name;


    logger = logger || null_logger();


    function raise_error(msg) {
      throw ['FSM:'].concat(msg).join(' ');
    }

    function check_undefined_state(value) {
      if(states[value] !== undefined) {
        raise_error([name, 'State:', value, 'ALREADY DEFINED']);
      }
    }

    function get_actions() {
      return $.map(states, function(v, k) {
        return v.actions();
      });
    }

    function get_transition_states() {
      return $.map(states, function(v, k) {
        return v.transition_states();
      });
    }

    function check_methods_exist_for_actions() {
      var i, actions = get_actions(), length = actions.length, action;
      for(i=0; i<length; i++) {
        action = actions[i];
        if(callback[action] === undefined || callback[action].constructor !== Function) {
          raise_error([name, 'Action method:', action, 'NOT DEFINED']);
        }
      }
    }

    function check_states_exist_for_transitions() {
      var i, transition_states = get_transition_states(), length = transition_states.length, transition_state;
      for(i=0; i<length; i++) {
        transition_state = transition_states[i];
        if(states[transition_state] === undefined && transition_state !== false) {
          raise_error([name, 'Transition state:', transition_state, 'NOT DEFINED']);
        }
      }
    }

    function exit_state(payload) {
      state.exit(payload);
    }

    function enter_state(state_name, payload, skip_entry) {
      logger.info(name.toUpperCase() + ' State: '+state_name);
      state = states[state_name];
      if(skip_entry !== true) {
        state.enter(payload);
      }
      if(state.then() !== undefined) {
        change_state_to(state.then(), payload);
      }
    }

    function advance_state(payload) {
      if(state.then() !== undefined) {
        change_state_to(state.then(), payload);
      }
    }

    function change_state_to(state_name, payload) {
      exit_state(payload);
      enter_state(state_name, payload);
      advance_state(payload);
    }



    self = {

      define: function(value) {
        check_undefined_state(value);
        states[value] = fsm_state(name, value, callback, raise_error, logger);
        if(first_state_name === undefined) {
          first_state_name = value;
        }
        return states[value].face;
      },

      begin: function(spec) {
        spec = spec || {};
        check_methods_exist_for_actions();
        check_states_exist_for_transitions();
        if(spec.state !== undefined) {
          first_state_name = spec.state;
        }
        enter_state(first_state_name, spec.payload, spec.skip_entry);
      },

      trigger: function(trigger, payload) {
        if(trigger.constructor === Object) {
          trigger = trigger.data.trigger;
          payload = $.makeArray(arguments).slice(1);
        }
        logger.info(name.toUpperCase()+' Trigger: '+trigger);
        var new_state_name = state.state_from_trigger(trigger);
        if(new_state_name === undefined) {
          logger.warn(name.toUpperCase() + ' Unexpected trigger: '+trigger);
        } else if(new_state_name !== false) {
          change_state_to(new_state_name, payload);
        }
      },

      get_state: function() {
        return state.name;
      },

      bind_to_trigger: function(object, event_name, trigger_name) {
        object = object.jquery === undefined ? $(object) : object;
        $(object).bind(event_name, {trigger:trigger_name || event_name}, this.trigger);
      }

    };



    return self;
  };


  return fsm;

}( jQuery ));