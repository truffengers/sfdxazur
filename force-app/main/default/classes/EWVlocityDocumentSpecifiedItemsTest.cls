/**
* @name: EWVlocityDocumentSpecifiedItemsTest
* @description: Test class for EWVlocityDocumentSpecifiedItems
* @author: Steve Loftus sloftus@azuruw.com
* @date: 18/12/2018
*/
@isTest
public with sharing class EWVlocityDocumentSpecifiedItemsTest {

    private static User systemAdminUser;
    private static Account broker;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static Product2 product;
    private static vlocity_ins__InsurableItem__c specifiedItem;
    private static EWVlocityDocumentSpecifiedItems remoteAction;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> outputMap;

    @isTest static void GetSpecifiedItemsTableWithContentTest() {
        setup(true);

        Test.startTest();

        System.runAs(systemAdminUser) {
            remoteAction.invokeMethod(EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD,
            						  inputMap,
            						  outputMap,
            						  null);
        }

        Test.stopTest();

        String sectionContent = (String)outputMap.get('sectionContent');
        System.assertNotEquals(null, sectionContent, 'Should have some content');
        System.assert(sectionContent.startsWith('<p>'), 'Should start with <p>');
        System.assert(sectionContent.contains('Name'), 'Should contain Name');
    }
    
    @isTest static void GetSpecifiedItemsTableWithoutContentTest() {
        setup(false);

        Test.startTest();

        System.runAs(systemAdminUser) {
            remoteAction.invokeMethod(EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD,
            						  inputMap,
            						  outputMap,
            						  null);
        }

        Test.stopTest();

        String sectionContent = (String)outputMap.get('sectionContent');
        System.assertNotEquals(null, sectionContent, 'Should have some content');
        System.assert(sectionContent.startsWith('<p>'), 'Should start with <p>');
        System.assert(!sectionContent.contains('Name'), 'Should not contain Name');
    }
        
    static void setup(Boolean createItem) {

    	remoteAction = new EWVlocityDocumentSpecifiedItems();

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                                    EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                                    techteamUserRole.Id,
                                    true);
        
        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);

            insured = EWAccountDataFactory.createInsuredAccount(
                                                    'Mr',
                                                    'Test',
                                                    'Insured',
                                                    Date.newInstance(1969, 12, 29),
                                                    true);

            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);

            product = EWProductDataFactory.createProduct(null, true);

            Id recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                                                                            'vlocity_ins__InsurableItem__c',
                                                                            EWConstants.INSURABLE_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM);

            if (createItem) {
                specifiedItem = EWInsurableItemDataFactory.createInsurableItem(null,
                                                                               broker.Id,
                                                                               insured.Id,
                                                                               product.Id,
                                                                               quote.Id,
                                                                               recordTypeId,
                                                                               true,
                                                                               false);

                specifiedItem.EW_Type__c = 'jewelleryAndWatches';
                specifiedItem.EW_MarketValue__c = 10000;

                insert specifiedItem;
            }

            Map<String, Object> contextData = new Map<String, Object>();
            contextData.put('contextId', quote.Id);

            Map<String, Object> documentData = new Map<String, Object>();
            documentData.put('contextData', contextData);

            String dataJSON = JSON.serialize(documentData);

            inputMap = new Map<String, Object>();
            inputMap.put('dataJSON', dataJSON);

            outputMap = new Map<String, Object>();
        }
    }
}