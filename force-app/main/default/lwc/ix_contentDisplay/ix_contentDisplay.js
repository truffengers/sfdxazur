import { LightningElement, api, track } from 'lwc';
import { snakeToSentence, findItemByName } from 'c/ix_utils';
import { CONSTANTS } from 'c/ix_constants';

export default class Ix_ContentDisplay extends LightningElement {
    @track displayData;

    /*
    For analytics
    */
    @api currentlySelectedItem;

    @api
    get itemData() {
        return this._itemData;
    }
    set itemData(value) {
        if (!value) { this.displayData = null; }
        /*
        Cloning the object because cached objects are read-only. Make the name
        of the search result that was clicked the `result-heading`.
        */

        // TODO: maybe use lodash or something to make this a lot cleaner. I'm
        // getting away with it currently because the data does not have
        // functions or undefineds in it.
        let results = JSON.parse(JSON.stringify({
            ...value,
            _is_result_heading: true
        }));

        if (!results._value) {
            return;
        }

        /*
        If the _value of an item is not an array, it is a leaf and display it as
        a large-heading on top under the result-heading
        */
        if (Array.isArray(results._value)) {
            results._value = results._value.map(item => {
                if (Array.isArray(item._value)) {
                    return item;
                }
                return { ...item, _is_large_heading: true };
            })
        }

        /*
        A list of attributes you want to give to certain items. These determine
        how the data items will be styled.
        */
        const giveCSSProperties = {
            '_is_rich_text': [
                CONSTANTS.YOUR_COVER,
                CONSTANTS.EXCLUSIONS,
                CONSTANTS.CLAUSES,
                CONSTANTS.CONDITIONS,
                CONSTANTS.ALL_LIABILITY_EXCLUSIONS,
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.GENERAL_EXCLUSIONS),
                CONSTANTS.MAKE_A_CLAIM,
                CONSTANTS.MAKING_A_CLAIM,
                CONSTANTS.CANCELLING_YOUR_POLICY,
                CONSTANTS.INFORMATION_YOU_PROVIDED,
                CONSTANTS.OTHER_THINGS_AWARE,
                CONSTANTS.WHEN_PAYING_A_LOSS,
                CONSTANTS.SUBHEADING,
                CONSTANTS.ACCORDION_VALUE,
                ...Object.values(
                    CONSTANTS.DEFINITIONS_TITLES.GENERAL_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_TITLES.LEGAL_EXPENSES_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_TITLES.PERSONAL_CYBER_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_TITLES.HOME_EMERGENCY_DEFINITIONS)
            ],
            '_is_accordion_item': [
                CONSTANTS.YOUR_COVER,
                CONSTANTS.EXCLUSIONS,
                CONSTANTS.CLAUSES,
                CONSTANTS.CONDITIONS,
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS),
                ...Object.values(
                    CONSTANTS.EXCLUSION_NAMES.GENERAL_EXCLUSIONS),
                CONSTANTS.YOU_PROMISE_US,
                CONSTANTS.WE_PROMISE_YOU,
                CONSTANTS.CANCELLING_YOUR_POLICY,
                CONSTANTS.INFORMATION_YOU_PROVIDED,
                CONSTANTS.OTHER_THINGS_AWARE,
                ...Object.values(CONSTANTS.PROMISES.YOUR_PROMISES),
                ...Object.values(CONSTANTS.PROMISES.OUR_PROMISES),
                CONSTANTS.WHEN_PAYING_A_LOSS,
                CONSTANTS.GENERAL_EXCLUSIONS,
                CONSTANTS.GENERAL_CONDITIONS,
                CONSTANTS.ALL_LIABILITY_EXCLUSIONS,
                CONSTANTS.ALL_PERSONAL_CYBER_EXCLUSIONS,
                CONSTANTS.ALL_HOME_EMERGENCY_EXCLUSIONS,
                ...Object.values(CONSTANTS.CLAUSES_NAMES),
                CONSTANTS.MAKE_A_CLAIM,
                CONSTANTS.MAKING_A_CLAIM,
                ...Object.values(CONSTANTS.CLAIM_NAMES),
                ...Object.values(CONSTANTS.DEFINITIONS_TITLES)
            ],
            '_is_link': [
                CONSTANTS.CONTENTS_ADDITIONAL_COVERS,
                CONSTANTS.BUILDINGS_ADDITIONAL_COVERS,
                CONSTANTS.LIABILITY_ADDITIONAL_COVERS,
                CONSTANTS.LEGAL_EXPENSES_ADDITIONAL_COVERS,
                CONSTANTS.INNER_LIMITS_TITLE,
                CONSTANTS.CYBER_HOME_SYSTEMS_DAMAGE,
                CONSTANTS.CYBER_CRIME,
                CONSTANTS.CYBER_ONLINE_LIABILITY,
                CONSTANTS.BUILDINGS,
                CONSTANTS.OTHER_PERMANENT_STRUCTURES,
                CONSTANTS.CONTENTS,
                CONSTANTS.PEDAL_CYCLES,
                CONSTANTS.ART_AND_COLLECTABLES,
                CONSTANTS.JEWELLERY_AND_WATCHES,
                ...Object.values(CONSTANTS.ADDITIONAL_COVERS),
                ...Object.values(CONSTANTS.INNER_LIMITS),
                ...Object.values(CONSTANTS.HOME_EMERGENCY_COVERS),
                CONSTANTS.BUILDINGS_CLAIMS,
                CONSTANTS.LIABILITY_CLAIMS,
                CONSTANTS.CYBER_CLAIMS,
                CONSTANTS.LEGAL_EXPENSES_CLAIMS,
                CONSTANTS.HOME_EMERGENCY_CLAIMS,
                CONSTANTS.LEGAL_AND_TAX_HELPLINE,
                CONSTANTS.ALL_PROPERTY_EXCLUSIONS
            ],
            '_is_subheading': [
                CONSTANTS.SUBHEADING
            ],
            '_is_hidden': [
                ...Object.keys(CONSTANTS.QUICK_LINK_NAMES).map(a => {
                    return CONSTANTS.QUICK_LINK_NAMES[a].TITLE;
                }),
                CONSTANTS.GENERAL_INFORMATION
            ]
        }

        /*
        This is like `giveCSSProperties`, but overwrites it. For instance,
        "Clauses" is usually styled one way, but when it is a definition, it
        needs to be styled a different way. This allows for items that already
        have the property `_special_styling` set to `true` to have special
        properties that overwrite the ones given in `giveCSSProperties`.
        */
        const specialStyling = {
            '_is_accordion_item': [
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.GENERAL_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.LEGAL_EXPENSES_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.PERSONAL_CYBER_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.HOME_EMERGENCY_DEFINITIONS)
            ],
            '_is_rich_text': [
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.GENERAL_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.LEGAL_EXPENSES_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.PERSONAL_CYBER_DEFINITIONS),
                ...Object.values(
                    CONSTANTS.DEFINITIONS_NAMES.HOME_EMERGENCY_DEFINITIONS)
            ]
        }

        Object.keys(giveCSSProperties).forEach(key => {
            giveCSSProperties[key].forEach(itemName => {
                /*
                A given item cannot have a special class and also be a large
                heading or a result heading
                */
                const specialClasses = [
                    '_is_accordion_item',
                    '_is_rich_text',
                    '_is_hidden'
                ];

                /*
                A given item cannot be any of these classes and also be an
                accordion
                */
                const notAccordions = [
                    '_is_subheading'
                ]

                const recursivelyApplyClass = (inputArray, currentDepth) => {
                    let branchDepth = 1;

                    if (currentDepth) {
                        branchDepth = currentDepth + 1;
                    }

                    inputArray.forEach(item => {
                        if (Array.isArray(item._value)) {
                            recursivelyApplyClass(item._value, branchDepth);
                        }

                        /*
                        Branch depth is the depth as calculated relative to the
                        present item; ie. not starting from the root of the data
                        structure.
                        */
                        item._branch_depth = branchDepth;
                        if (item && item._name === itemName) {
                            item[key] = true;

                            if (specialClasses.includes(key)) {
                                item._is_result_heading = false;
                                item._is_large_heading = false;
                            }
                            
                            if (notAccordions.includes(key)) {
                                item._is_accordion_item = false;
                            }

                            // Each class in `depth1ExcludedCls` will be
                            // set to false for any items for which
                            // `item._branch_depth` is 1
                            const classesAppliedByDepth = [
                                {
                                    depth: 1,
                                    excludedCls: ['_is_accordion_item'],
                                    includedCls: ['_is_large_heading'],
                                    includedItems: [
                                        ...Object.values(
                                            CONSTANTS.EXCLUSION_NAMES
                                            .PROPERTY_EXCLUSIONS)
                                    ]
                                }
                            ];

                            classesAppliedByDepth.forEach(classSet => {
                                if (item._branch_depth === classSet.depth
                                    && classSet.includedItems
                                        .includes(item._name)) {
                                        classSet.excludedCls.forEach(cls => {
                                            item[cls] = false;
                                        });
                                        classSet.includedCls.forEach(cls => {
                                            item[cls] = true;
                                        });
                                    }
                            });
                        }
                    })
                }
                recursivelyApplyClass([results]);

                /*
                If the root-level item has _is_link attribute and it is true,
                make it false because the root-level item should never be a
                clickthrough link.
                */
                if (results._is_link) {
                    results._is_link = false;
                }
            })
        });

        /*
        Apply special styling
        */
        Object.keys(specialStyling).forEach(key => {
            specialStyling[key].forEach(itemName => {
                const recursivelyApplyClass = (inputArray) => {
                    inputArray.forEach(item => {
                        if (Array.isArray(item._value)) {
                            recursivelyApplyClass(item._value);
                        }

                        if (item && item._name === itemName
                            && item._special_styling) {
                            Object.keys(giveCSSProperties)
                                .forEach(unwantedKey => {
                                    if (Object.keys(specialStyling)
                                            .includes(unwantedKey)
                                        && specialStyling[unwantedKey]
                                            .includes(itemName)) {
                                        return;
                                    }
                                    item[unwantedKey] = false;
                                })

                            item[key] = true;
                        }
                    })
                }
                recursivelyApplyClass([results]);

                // If the root-level item has _is_link attribute and it is true,
                // make it false because the root-level item should never be a
                // clickthrough link.
                if (results._is_link) {
                    results._is_link = false;
                }
            })
        })

        // A list of attributes you want to give to the children of certain
        // items - eg. to make all the children of a table header into table
        // rows
        const giveChildrenCSSProperties = {
            '_is_accordion_item': [
                CONSTANTS.CONDITIONS
            ],
            '_is_rich_text': [
                CONSTANTS.CONDITIONS
            ],
            '_is_link': [
                CONSTANTS.ALL_PROPERTY_EXCLUSIONS
            ]
        }

        Object.keys(giveChildrenCSSProperties).forEach(key => {
            giveChildrenCSSProperties[key].forEach(itemName => {
                const itemWithChildren = findItemByName(
                    snakeToSentence(itemName),
                    [results]
                );

                if (!itemWithChildren._is_parent) {
                    // Something was wrong when you decided which items to style
                    // the children of
                    return;
                }

                // These classes will be overwritten by anything in
                // `giveChildrenCSSProperties`
                const lowPriorityClasses = [
                    '_is_accordion_item', '_is_link', '_is_rich_text'
                ];

                itemWithChildren._value.forEach(childItem => {
                    lowPriorityClasses.forEach((cls) => {
                        childItem[cls] = false;
                    });

                    if (!childItem._is_subheading) {
                        childItem[key] = true;
                    }

                    childItem._value = this.giveLeafChildrenCSSClass(
                        childItem,
                        key
                    )
                })
            })
        })

        this.displayData = results;
    }

    giveLeafChildrenCSSClass(item, keyName) {
        /*
        Gives the children of the item the same flag as the parent. This is
        used when for instance, the sum insured for Art and Collectables is
        just as important as the name, or when children need a different style
        from their parent.
        */
        if (!Array.isArray(item._value)) {
            return item._value;
        }

        return item._value.map(child => {
            if (child._is_parent) {
                return child;
            } else if (child._is_subheading) {
                return child;
            }
            return {
                ...child,
                [keyName]: true,
                [keyName + '_leaf_child']: true
            }
        })
    }

    displayResultingItem(event) {
        this.dispatchEvent(new CustomEvent('displayresultingitem', {
            detail: { value: event.detail.value }
        }))
    }
}