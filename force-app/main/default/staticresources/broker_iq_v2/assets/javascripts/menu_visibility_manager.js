TT.menuVisibilityManager = (function($) {

  'strict';

  var $banner;
  var $modifiable;
  var previousPosition = 0;
  var lastId           = 0;
  var isModified       = false;
  var disableIds       = [];
  var bannerHeight;
  var settings;

  var defaults = {
    modifierClass           : 'is-hidden',
    modifiableSelector      : '#js-header',
    bannerSelector          : '#js-header',
    sensitivity             : 15,
    unmodifyWhenScrollingUp : true,
    modifyWhenHovering      : false
  };



  function setSettings(params) {
    settings = $.extend({}, defaults, params || {});
  }

  function isBannerModifiable() {
    return settings.modifiableSelector === settings.bannerSelector;
  }

  function getJQueryInstance(selectorOrJqueryInstance) {
    return selectorOrJqueryInstance instanceof jQuery ? selectorOrJqueryInstance : $(selectorOrJqueryInstance);
  }

  function referenceElements() {
    $modifiable = getJQueryInstance(settings.modifiableSelector);
    $banner = isBannerModifiable() ? $modifiable : getJQueryInstance(settings.bannerSelector);
  }

  function addListeners() {
    $(settings.scrollBroadcaster).on('scroll', scrollHandler);
    $(settings.mediaMatcher).on('mediaChange', mediaChangeHandler);
  }

  function isHovering() {
    return $modifiable.is(':hover');
  }

  function modify() {
    $modifiable.addClass(settings.modifierClass);
    isModified = true;
  }

  function unmodify() {
    $modifiable.removeClass(settings.modifierClass);
    isModified = false;
  }

  function unmodifyImmediately() {
    $modifiable.css({transition: 'none'});
    unmodify();
    $modifiable.redraw();
    $modifiable.css({transition: ''});
  }

  function getIsEnabled() {
    return disableIds.length === 0;
  }

  function scrollHandler(event, data) {

    if(!getIsEnabled()) { return; }

    var currentPosition = data.topTrigger;
    var difference      = Math.abs(currentPosition - previousPosition);
    var isScrollingDown = currentPosition > previousPosition;
    var isScrollingFast = difference >= settings.sensitivity;
    var isAtTop         = currentPosition <= bannerHeight;

    function shouldUnmodify() {
      if(settings.unmodifyWhenScrollingUp) {
        return isModified && (isScrollingFast || isAtTop) && !isScrollingDown;
      } else {
        return isModified && isAtTop;
      }
    }

    function shouldModify() {
      return !isModified && !isAtTop && isScrollingFast && isScrollingDown && (settings.modifyWhenHovering || !isHovering());
    }

    if(shouldModify()) {
      modify();
    } else if(shouldUnmodify()) {
      unmodify();
    }

    previousPosition = currentPosition;
  }

  function mediaChangeHandler() {
    recordBannerHeight();
  }

  function recordBannerHeight() {
    bannerHeight = $banner.height();
  }

  function getNewId() {
    return ++lastId;
  }



  return {

    init: function(params) {
      setSettings(params);
      referenceElements();
      addListeners();
      recordBannerHeight();
    },

    disable: function() {
      var id = getNewId();
      disableIds.push(id);
      return id;
    },

    enable: function(idToRemove) {
      disableIds = disableIds.filter(function(id) { return id !== idToRemove; });
    },

    hide            : modify,
    show            : unmodify,
    showImmediately : unmodifyImmediately
  };

}(jQuery));