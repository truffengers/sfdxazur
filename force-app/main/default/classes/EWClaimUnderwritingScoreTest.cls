/**
* @name: EWClaimUnderwritingScoreTest
* @description: Test class for EWClaimUnderwritingScore
* @author: Demiro Massessi dmassessi@azuruw.com
* @date: 15/10/2018
*/

@isTest
public class EWClaimUnderwritingScoreTest {
    
    @isTest
    public static void testGetClaimScoreSingleClaim() {
        
        Map<String, Object> input = new Map<String, Object>();
		Map<String, Object> output = new Map<String, Object>();
		Map<String, Object> options = new Map<String, Object>();
		Map<String, Object> jsonObj = new Map<String, Object>();
		Map<String, Object> jsonObj2 = new Map<String, Object>();
        
        jsonObj.put('selectClaimType', 'Fire');
		jsonObj.put('textClaimValue', 4000);
		jsonObj.put('radioClaimStillOpen', 'No');
		jsonObj.put('radioClaimAtCurrentProperty', 'No');
		jsonObj2.put('editBlockClaimsHistory', jsonObj);
		input.put('propertyDetails', jsonObj2);
        
        Claim_Underwriting_Score__c claimScoreMatrix1 = new Claim_Underwriting_Score__c();
        claimScoreMatrix1.Name = 'Fire';
        claimScoreMatrix1.Claim_Less_Than_5000__c = 1;
        claimScoreMatrix1.Claim_Greater_Than_5000__c = 2;
        claimScoreMatrix1.Claim_Greater_Than_30000__c = 3;
        insert claimScoreMatrix1;
        
        Claim_Underwriting_Score__c claimScoreMatrix2 = new Claim_Underwriting_Score__c();
        claimScoreMatrix2.Name = 'Storm';
        claimScoreMatrix2.Claim_Less_Than_5000__c = 1;
        claimScoreMatrix2.Claim_Greater_Than_5000__c = 2;
        claimScoreMatrix2.Claim_Greater_Than_30000__c = 3;
        insert claimScoreMatrix2;
        
        Claim_Underwriting_Score__c claimScoreMatrix3 = new Claim_Underwriting_Score__c();
        claimScoreMatrix3.Name = 'Other';
        claimScoreMatrix3.Claim_Less_Than_5000__c = 1;
        claimScoreMatrix3.Claim_Greater_Than_5000__c = 2;
        claimScoreMatrix3.Claim_Greater_Than_30000__c = 3;
        insert claimScoreMatrix3;
        
        Test.startTest();
        
        EWClaimUnderwritingScore vlocity = new EWClaimUnderwritingScore();
		vlocity.invokeMethod('getScore', input, output, options);
        Test.stopTest();
    }
}