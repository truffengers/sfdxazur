trigger BrokerIqContributorTrigger on Broker_iQ_Contributor__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  (new Nebula_Tools.MetaDataTriggerManager(Broker_iQ_Contributor__c.sObjectType)).handle();
}