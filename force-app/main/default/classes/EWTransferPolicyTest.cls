/**
* @name: EWTransferPolicyTest
* @description: Test Class for EWTransferPolicy.
* @author: Bharati Pal
* @dateCreated: 23/12/2020
*/
@isTest 
public with sharing class EWTransferPolicyTest {
    static Quote quotes;
    static List<vlocity_ins__DocumentClause__c> documentClauses;
    static Account insured;
    static Account InsuredJoint;
    static Account broker;
    private static Contact brokerContact1;
    private static Contact brokerContact2;
    private static User brokerUser1;
    private static User brokerUser2;
    
    public static testMethod void TestpolicyTransfer() {
        // setup(1);
        
        Id partnerProfileId = [
            SELECT Id
            FROM Profile
            WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME
        ].Id;
        
        broker = EWAccountDataFactory.createBrokerAccount(null, true);
        broker.EW_IsFinancialAccount__c = true;
        update broker;
        
        brokerContact1 = EWContactDataFactory.createContact(null, null, broker.Id, true);
        brokerUser1 = EWUserDataFactory.createPartnerUser('User1', partnerProfileId, brokerContact1.Id, true);
        brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
        brokerUser2 = EWUserDataFactory.createPartnerUser('User2', partnerProfileId, brokerContact2.Id, true);
        insured = EWAccountDataFactory.createInsuredAccount(
            'Mr',
            'Test',
            'Insured',
            Date.newInstance(1969, 12, 29),
            true);
        
        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        opportunity.OwnerId = brokerUser1.Id;
        
        quotes = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, false);       
        insert quotes;
        
        
        
        documentClauses = new List<vlocity_ins__DocumentClause__c>();
        documentClauses.add(EWDocumentClauseDataFactory.createDocumentClause('Name1', null, true, false));
        documentClauses.add(EWDocumentClauseDataFactory.createDocumentClause('Name2', null, true, false));
        insert documentClauses;
        
        Id policyRecId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get(EWConstants.POLICY_EW_RECORD_TYPE).getRecordTypeId();
        
        Asset policy = new Asset();
        policy.name =  'testpolicy';
        policy.Opportunity__c = opportunity.Id;
        policy.EW_Quote__c = quotes.Id;
        policy.RecordTypeId = policyRecId;
        policy.AssetProvidedById = broker.Id;
        policy.AccountId = insured.Id;
        policy.OwnerId = brokerUser1.Id;
        policy.IBA_ProducingBrokerAccount__c = broker.Id;
        policy.vlocity_ins__EffectiveDate__c = Date.today();
        policy.vlocity_ins__InceptionDate__c = Date.today();
        policy.vlocity_ins__ExpirationDate__c = Date.today().addYears(1);
        policy.Status = 'Issued';
        insert policy;
        
        quotes.EWQuotePrefix__c = 'Test'; 
        quotes.EW_Source_Policy__c = policy.Id;
        update quotes;
        
        String newBrokerAccountId = String.valueOf(broker.Id);
        String newBrokerUserId =String.valueOf(brokerUser2.Id);
        String oldBrokerAccountId = String.valueOf(broker.Id);
        
        String testresult = EWTransferPolicy.transferPolicy(policy.Id,newBrokerAccountId,newBrokerUserId,oldBrokerAccountId);
        String testresultfalse = EWTransferPolicy.transferPolicy(policy.Id,newBrokerAccountId,oldBrokerAccountId,oldBrokerAccountId);
        
        System.assertEquals(policy.IBA_ProducingBrokerAccount__c,newBrokerAccountId);        
        System.assertEquals(testresult,'Success');
        
        EWTransferPolicy.updatePolicyBrokerage(policy.Id);
          
        System.assertEquals(policy.EW_BrokerageBeforeTransfer__c,null);
        System.assertEquals(policy.IBA_ProducingBrokerAccount__c,oldBrokerAccountId);
    }
    
    public static testMethod void TestpolicyTransferHelper() {
        
        Id partnerProfileId = [
            SELECT Id
            FROM Profile
            WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME
        ].Id;
        
        broker = EWAccountDataFactory.createBrokerAccount(null, true);
        
        brokerContact1 = EWContactDataFactory.createContact(null, null, broker.Id, true);
        brokerUser1 = EWUserDataFactory.createPartnerUser('User1 45', partnerProfileId, brokerContact1.Id, true);
        brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
        brokerUser2 = EWUserDataFactory.createPartnerUser('User2 45', partnerProfileId, brokerContact2.Id, true);
        
        insured = EWAccountDataFactory.createInsuredAccount(
            'Mr',
            'Test 45',
            'Insured',
            Date.newInstance(1969, 12, 29),
            true);
        
        InsuredJoint = EWAccountDataFactory.createInsuredAccount(
            'Mr',
            'Test Joint 45',
            'InsuredJOint',
            Date.newInstance(1989, 12, 29),
            true);
        
        InsuredJoint.EW_PrimaryInsured__c = insured.Id;
        update InsuredJoint;
        
        
        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        opportunity.OwnerId = brokerUser1.Id;
        
        quotes = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, false);
        insert quotes;        
        
        documentClauses = new List<vlocity_ins__DocumentClause__c>();
        documentClauses.add(EWDocumentClauseDataFactory.createDocumentClause('Name1 45', null, true, false));
        documentClauses.add(EWDocumentClauseDataFactory.createDocumentClause('Name2 45', null, true, false));
        insert documentClauses;
        
        Id policyRecId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get(EWConstants.POLICY_EW_RECORD_TYPE).getRecordTypeId();
        
        Asset policy = new Asset();
        policy.name =  'testpolicy 45';
        policy.Opportunity__c = opportunity.Id;
        policy.EW_Quote__c = quotes.Id;
        policy.RecordTypeId = policyRecId;
        policy.AssetProvidedById = broker.Id;
        policy.AccountId = insured.Id;
        policy.OwnerId = brokerUser1.Id;
        policy.IBA_ProducingBrokerAccount__c = broker.Id;
        policy.vlocity_ins__EffectiveDate__c = Date.today();
        policy.vlocity_ins__InceptionDate__c = Date.today();
        policy.vlocity_ins__ExpirationDate__c = Date.today().addYears(1);
        policy.Status = 'Issued';
        insert policy;
        
        
        quotes.EWQuotePrefix__c = 'Test'; 
        quotes.EW_Source_Policy__c = policy.Id;
        update quotes;
        
        
        Map<String, Object> inputMap = new  Map<String, Object>();
        
        inputMap.put('policyId',policy.Id);
        inputMap.put('newBrokerAccountId',String.valueOf(broker.id));
        inputMap.put('newBrokerUserId',String.valueOf(brokerUser2.id));
        inputMap.put('oldBrokerAccountId',String.valueOf(broker.id));        
        
        EWVlocityRemoteActionHelper.transferPolicy(inputMap,null,null);
       
        System.assertEquals(policy.IBA_ProducingBrokerAccount__c,String.valueOf(broker.id));        
        
        EWVlocityRemoteActionHelper.updatePolicyBrokerage(inputMap,null,null);
        System.assertEquals(policy.EW_BrokerageBeforeTransfer__c,null);
        System.assertEquals(policy.IBA_ProducingBrokerAccount__c,String.valueOf(broker.id));
    }
}