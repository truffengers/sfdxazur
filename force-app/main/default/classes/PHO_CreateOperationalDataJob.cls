/**
* @name: PHO_CreateOperationalDataJob
* @description: Asynchronous job to translate Phoenix Rising records into Salesforce entities.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 18/12/2019
* @lastChangedBy: Antigoni D'Mello    24/03/2020    PR-35 Added logic to send email with job result
*/
public with sharing class PHO_CreateOperationalDataJob implements Queueable {

    @TestVisible
    private List<PHO_PhoenixStaging__c> stagingRecords;
    private Integer recordLimit;
    private Set<Id> recordIds;

    public PHO_CreateOperationalDataJob(Integer recordLimit, Set<Id> recordIds) {
        this.recordLimit = recordLimit;
        this.recordIds = recordIds;
        this.stagingRecords = groupStagingRecordsByEventNumber(
                Database.query(
                        'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        (this.recordIds != null ? ' AND Id IN :this.recordIds' : '') +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                        ' LIMIT :this.recordLimit'
                )
        );
    }

    public void execute(QueueableContext context) {
        List<String> emailAddresses = new List<String> {System.UserInfo.getUserEmail()};
        List<Azur_Log__c> customLogs = new List<Azur_Log__c>();
        LogBuilder logBuilder = new LogBuilder();
        Savepoint sp = Database.setSavepoint();
        Boolean continueProcessing = true;

        try {
            if (!this.stagingRecords.isEmpty()) {
                PHO_PhoenixStagingTranslator.translateToSalesforceObjects(this.stagingRecords);
            } else {
                continueProcessing = false;
                EmailUtil email = new EmailUtil(emailAddresses);
                email.plainTextBody('Phoenix Operational Data Creation process succeeded');
                email.subject('Phoenix Operational Data Creation process succeeded');
                email.sendEmail();
            }
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug(LoggingLevel.ERROR, 'PHO Exception occured. Queueable job terminated. Error message: '+e.getMessage() +'. Stack trace: '+e.getStackTraceString());
            continueProcessing = false;
            EmailUtil email = new EmailUtil(emailAddresses);
            email.plainTextBody('An error occurred during Phoenix Operational Data Creation process, please check the latest Azur logs for more details');
            email.subject('Phoenix Operational Data Creation process failed');
            email.sendEmail();
            Azur_Log__c newLog = logBuilder.createLogWithCodeInfo(e,'Phoenix Rising','PHO_CreateOperationalDataJob','execute');
            customLogs.add(newLog);
            insert customLogs;
        }

        if (continueProcessing && !Test.isRunningTest()) {
            System.enqueueJob(new PHO_CreateOperationalDataJob(this.recordLimit, this.recordIds));
        }
    }

    private static List<PHO_PhoenixStaging__c> groupStagingRecordsByEventNumber(List<PHO_PhoenixStaging__c> queriedRecords) {
        // Ensure records with the same Event Number will be processed together
        // and will not be split into different runs of chained queueable job.
        // We can ensure that by removing all records with the last Event Number in the list
        // (they will be picked up and processed by next job run)
        if (!queriedRecords.isEmpty()) {
            List<Integer> indexToRemove = new List<Integer>();
            Decimal lastEventNumber = queriedRecords[queriedRecords.size()-1].PHO_EventNumber__c;
            Boolean moreThanOneEventNumber = false;
            for (Integer i = queriedRecords.size()-1; i >= 0; i--) {
                if(lastEventNumber == queriedRecords[i].PHO_EventNumber__c) {
                    indexToRemove.add(i);
                } else {
                    moreThanOneEventNumber = true;
                    break;
                }
            }

            if (moreThanOneEventNumber) {
                for (Integer i : indexToRemove) {
                    queriedRecords.remove(i);
                }
            } // else keep all of the records in the original list
        }
        return queriedRecords;
    }
}