({
    doInit: function (component, event) {
        
        // get the Quote Id (v.recordId is not available on related list table)
        var pageRef = component.get("v.pageReference");
        var state = pageRef.state;
        var base64Context = state.inContextOfRef;
        if (base64Context.startsWith("1\.")) {
            base64Context = base64Context.substring(2);
        }
        var addressableContext = JSON.parse(window.atob(base64Context));
        var recordId = addressableContext.attributes.recordId;
        component.set("v.recordId", recordId);
        
        // get the clauses
        let action = component.get("c.loadRecordData");
        action.setParam('id', recordId);
        action.setCallback(this, function (response) {
            var responseJson = response.getReturnValue();
            
            if (responseJson) {
                component.set("v.options", responseJson.options);
                component.set("v.values", responseJson.values);
                component.set("v.requiredOptions", responseJson.required);
            }
  
        });
        $A.enqueueAction(action);
    },


    handleSave: function (component, event) {

        var recordId = component.get('v.recordId');
        var selectedValues = component.get('v.values');
        var saveSelected = {recordId: recordId, selectedValues: selectedValues};

        let action = component.get("c.saveSelectedAssignments");
        action.setParam('inputMap', saveSelected);
        action.setCallback(this, function (response) {
            var responseJson = response.getReturnValue();
            
            if (responseJson == true) {
                $A.enqueueAction(component.get('c.redirectToQuoteRecord'));
            }
  
        });
        $A.enqueueAction(action);
    },


    handleCancel: function (component, event) {
        $A.enqueueAction(component.get('c.redirectToQuoteRecord'));
	},


    redirectToQuoteRecord: function(component){
        // e.force:navigateToSObject does not display updated record values due to Lightning caching
        window.parent.location = '/lightning/r/sObject/' + component.get('v.recordId') + '/view';
    },


    newClause: function(component, event){
    	component.set("v.newClause", true);
	},


    cancelNewClause: function(component, event){
    	component.set("v.newClause", false);
	},


    checkValidAndAddNewClause: function(component, event){
        
		// validate
		var isValid = false;
        var inputClauseName = component.find("inputName");
        var inputClauseContent = component.find("inputContent");
        
        if(!inputClauseName.get("v.value")){
            inputClauseName.setCustomValidity("please advise");
        }else{
            inputClauseName.setCustomValidity("");
            isValid = true;
        }
        inputClauseName.reportValidity();
        
        if(!inputClauseContent.get("v.value")){
            inputClauseContent.setCustomValidity("please advise");
        }else{
            inputClauseContent.setCustomValidity("");
            isValid = true;
        }
        inputClauseContent.reportValidity();
        
        if(isValid){

            // retain in Salesforce
            var recordId = component.get('v.recordId');
            var inputValues = {name: inputClauseName.get("v.value") , content: inputClauseContent.get("v.value")};
            var options = {available:  component.get('v.options'), selected: component.get('v.values')};
            var toSave = {recordId: recordId, inputValues: inputValues, options: options};

            let saveClauseAction = component.get("c.saveNewClause");
            saveClauseAction.setParam('inputMap', toSave);
            saveClauseAction.setCallback(this, function (response) {
            	var responseJson = response.getReturnValue();
                if (responseJson.newClauseSaved == true) {
                    component.set("v.newClause", false);
                    component.set("v.options", responseJson.options);
                    component.set("v.values", responseJson.values);
                }
            });
            $A.enqueueAction(saveClauseAction); 
        }
    }  
    
});