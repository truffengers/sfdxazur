/*
* @name: EWRecordTypeHelper
* @description:  Used to cache record type ids
* @author: Steve Loftus sloftus@azuruw.com
* @date: 04/11/2018
*/
public class EWRecordTypeHelper {
    
    private static EWRecordTypeHelper instance { get; set; }
    
    private static Map<string, Schema.RecordTypeInfo> cache = null;
    
    private static Map<String, Schema.SObjectType> globalDescribe = null;

    private static Map<String, Schema.SObjectType> getGlobalDescribe() {

        if (globalDescribe == null) {
            globalDescribe = Schema.getGlobalDescribe();
        }
        return globalDescribe;
    }
    
    private static Map<string, Schema.RecordTypeInfo> getRecordTypeCache() {

        if (cache == null) {
            cache = new Map<string, Schema.RecordTypeInfo>();
        }
        return cache;
    }
    
    private EWRecordTypeHelper() {}

    public static EWRecordTypeHelper getInstance() {

        if (instance == null) {
            instance = new EWRecordTypeHelper();
        }
        return instance;
    }
    
    public ID getRecordTypeIdByDeveloperName(final String objectName, final String recordTypeDeveloperName) {

        return getRecordTypeIdByDeveloperName(objectName, recordTypeDeveloperName, true);
    }
    
    public ID getRecordTypeIdByDeveloperName(final String objectName, final String recordTypeDeveloperName, final Boolean checkAvailability) {

        ID recordTypeId = null;
        Schema.RecordTypeInfo recordTypeInfo = null;

        if ((objectName != null) && (recordTypeDeveloperName != null)) {

            string key = objectName + '_' + recordTypeDeveloperName;

            // search from local cache first
            if (getRecordTypeCache().containsKey(key)) {

                // record type is cached
                recordTypeInfo = getRecordTypeCache().get(key);

                // get the id if the type is available
                if (!checkAvailability || recordTypeInfo.isAvailable()) {
                    recordTypeId = recordTypeInfo.getRecordTypeId();
                }
            }

            // not cached, so retrieve and cache
            if ((recordTypeId == null) && (recordTypeInfo == null)) {

                recordTypeInfo = getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName);


                if (recordTypeInfo != null) {
                    getRecordTypeCache().put(key, recordTypeInfo);

                    if (!checkAvailability || recordTypeInfo.isAvailable()) {
                        recordTypeId = recordTypeInfo.getRecordTypeId();
                    }
                }
            }
        }

        return recordTypeId;
    }

    public Map<String, Id> getRecordTypeIdMapByObjectName(final String objectName) {

        Map<String, Id> recordTypeIdByNameMap = new Map<String, Id>();

        if (EWConstants.RECORD_TYPE_NAME_BY_OBJECT.containsKey(objectName)) {

            for (String recordTypeName : EWConstants.RECORD_TYPE_NAME_BY_OBJECT.get(objectName)) {

                Id recordTypeId = getInstance().getRecordTypeIdByDeveloperName(objectName, recordTypeName);

                if (recordTypeId != null) {
                    recordTypeIdByNameMap.put(recordTypeName, recordTypeId);
                }
            }
        }

        return recordTypeIdByNameMap;
    }
}