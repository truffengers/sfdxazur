@isTest
public with sharing class EWButtonControllerTest {

    static Account broker, insured;
    static Opportunity opportunity;
    static Asset policy;
    static Quote quote;

    static {

        broker = EWAccountDataFactory.createBrokerAccount(null, true);
        insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, true);
        quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);

    }

    static testMethod void testNegativePath(){

        Test.startTest();

        // test a different Id
        Map<String, Object> testMap = EWButtonController.loadRecordData(UserInfo.getUserId());
        System.assertEquals(false, testMap.containsKey(EWButtonController.BUTTON_TITLES_KEY));

        Test.stopTest();
    }

    static testMethod void testPolicyButtons(){

        Test.startTest();

        // test an Asset Id - to NOT hide the button
        Map<String, Object> testMap = EWButtonController.loadRecordData(policy.Id);
        System.assertEquals(false, testMap.containsKey(EWButtonController.BUTTON_TITLES_KEY));

        // test an Asset Id - to hide the button
        policy.Status = 'Draft';
        update policy;
        testMap = EWButtonController.loadRecordData(policy.Id);
        System.assertEquals(true, testMap.containsKey(EWButtonController.BUTTON_TITLES_KEY));
        Set<Object> buttonsToHide = getContents(testMap.get(EWButtonController.BUTTON_TITLES_KEY));
        //String mapJson = JSON.serialize(testMap.get(EWButtonController.BUTTON_TITLES_KEY));
        //Set<Object> buttonsToHide = new Set<Object>( (List<Object>) JSON.deserializeUntyped(mapJson) );
        System.assertEquals(true, buttonsToHide.contains(EWConstants.BUTTON_TITLE_MTA));

        Test.stopTest();

    }

    static testMethod void testQuoteButtons(){

        quote.Status = EWConstants.QUOTE_STATUS_QUOTED;
        update quote;

        Test.startTest();

        // test NOT TO hide buttons
        Map<String, Object> testMap = EWButtonController.loadRecordData(quote.Id);
        System.assertEquals(false, testMap.containsKey(EWButtonController.BUTTON_TITLES_KEY));

        // test to hide Bind button
        quote.Status = EWConstants.QUOTE_STATUS_UW_APPROVED;
        update quote;
        testMap = EWButtonController.loadRecordData(quote.Id);
        System.assertEquals(true, testMap.containsKey(EWButtonController.BUTTON_TITLES_KEY));
        Set<Object> hiddenButtons = getContents(testMap.get(EWButtonController.BUTTON_TITLES_KEY));
        System.assertEquals(true, hiddenButtons.contains(EWConstants.BUTTON_TITLE_BIND));

        //Set<String> hiddenButtons = new Set<String>((Set<String>) testMap.get(EWButtonController.BUTTON_TITLES_KEY));
        //System.assertEquals(true, hiddenButtons.contains(EWConstants.BUTTON_TITLE_BIND));

        // test to hide all buttons
        quote.Status = null;
        update quote;
        testMap = EWButtonController.loadRecordData(quote.Id);
        System.assertEquals(true, testMap.containsKey(EWButtonController.BUTTON_TITLES_KEY));
        hiddenButtons = getContents(testMap.get(EWButtonController.BUTTON_TITLES_KEY));
        //hiddenButtons = new Set<String>((Set<String>) testMap.get(EWButtonController.BUTTON_TITLES_KEY));
        System.assertEquals(true, hiddenButtons.contains(EWConstants.BUTTON_TITLE_NTU));
        System.assertEquals(true, hiddenButtons.contains(EWConstants.BUTTON_TITLE_EDIT));
        System.assertEquals(true, hiddenButtons.contains(EWConstants.BUTTON_TITLE_BIND));

        Test.stopTest();
    }


    static testMethod void testCheckIfCommunity(){

        Map<String,Object> testMap = EWButtonController.checkIfCommunity();
        System.assertEquals(true, testMap.containsKey(EWButtonController.IS_COMMUNITY_KEY));
        System.assertEquals(false, testMap.get(EWButtonController.IS_COMMUNITY_KEY));
    }

    static Set<Object> getContents(Object o){
        String mapJson = JSON.serialize(o);
        return new Set<Object>( (List<Object>) JSON.deserializeUntyped(mapJson));
    }


}