/**
* @name: ITriggerHandler
* @description: This interface dictates which methods every Trigger Handler must implement
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 03/11/2018
*
*/

public interface ITriggerHandler
{
    void BeforeInsert(List<SObject> newItems);

    void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);

    void BeforeDelete(Map<Id, SObject> oldItems);

    void AfterInsert(Map<Id, SObject> newItems);

    void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);

    void AfterDelete(Map<Id, SObject> oldItems);

    void AfterUndelete(Map<Id, SObject> oldItems);

    Boolean IsDisabled();
}