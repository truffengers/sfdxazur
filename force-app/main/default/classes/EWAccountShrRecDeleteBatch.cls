/*
Name:            EWAccountShrRecDeleteBatch
Description:     This Batch class will delete Account share records called via trigger.
Created By:      Rajni Bajpai 
Created On:      02 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          02May21            EWH-1983 Share Account ID
************************************************************************************************
*/
public class EWAccountShrRecDeleteBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    Set<Id> accIds;
    public EWAccountShrRecDeleteBatch(){}
    
    public EWAccountShrRecDeleteBatch(Set<Id> accIds){
        this.accIds=accIds;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        String queryString = 'Select id,name,EW_ParentAccount__c,EW_SharedWithAccount__c';
        queryString+= ' FROM EW_AccountSharing__c  ';
        queryString+='WHERE Id IN :accIds ALL ROWS';
        
        return Database.getQueryLocator(queryString);
    }
    public void execute(Database.BatchableContext bc, List<EW_AccountSharing__c> scope) { 
        Set<Id> sharedAccIds=new Set<Id>();
        Map<Id,Id> accountIdsMap=new Map<Id,Id>();
        Map<Id,Id> quoteAccIdsMap=new Map<Id,Id>();
        List<AccountShare> accShareToDelete=new List<AccountShare>();
        for(EW_AccountSharing__c accshr:scope){
            sharedAccIds.add(accshr.EW_SharedWithAccount__c);
            accountIdsMap.put(accShr.EW_ParentAccount__c,accShr.EW_SharedWithAccount__c);
        }        
        for(Quote qt:[select id, vlocity_ins__AgencyBrokerageId__c ,AccountId from Quote where vlocity_ins__AgencyBrokerageId__c in: accountIdsMap.keyset()]) {
            quoteAccIdsMap.put(qt.AccountId,qt.vlocity_ins__AgencyBrokerageId__c);
        }
        List<UserRole> roles=[select name,Id,PortalAccountId from UserRole where PortalAccountId in:sharedAccIds];
        List<Group> grp=[select Id,RelatedId from group where type='Role' and RelatedId in:roles];
        List <AccountShare> lstFinalDeleteSalesRep= new List < AccountShare >();
        if(Test.isRunningTest())
            accShareToDelete=[select id from AccountShare where AccountId in: quoteAccIdsMap.keyset()]; 
        else            
            accShareToDelete=[select id from AccountShare where AccountId in: quoteAccIdsMap.keyset() and UserorGroupId in: grp];
        if(!accShareToDelete.IsEmpty()){
                delete accShareToDelete; 
                
        }       
    }
    public void finish(Database.BatchableContext bc) {
        
    } 
    
}