import { LightningElement, api, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getQuoteTreeExt from '@salesforce/apex/EWActivityExplorerController.getActivityHistory';

export default class AZActivityExplorer extends NavigationMixin(LightningElement) {
    @api recordId;
    @track shouldDisplay;
    @track quoteItems;
    @track latestQuote;
    @track displayDetail;
    @track recordBody;
    @track recordEntryType;
    @api activityId;
    @track recordSubject;
    @track buttonLabel = 'Open';
    @track showBody;
    @track showPreview;
    @track recordPreview;
    @track previewLabel;

    constructor() {
        super();
        this.isLoading = true;
        this.displayDetail = false;
        this.gridColumns = [
            {
                type: 'text',
                fieldName: 'entryType',
                label: 'Type',
                cellAttributes: { 
                    class: { fieldName: 'customCSSClass' }
                }
            },
            {
                type: 'date',
                fieldName: 'createdOn',
                label: 'Created DateTime',
                typeAttributes: {
                    year: "numeric",
                    month: "numeric",
                    day: "numeric",
                    hour: "2-digit",
                    minute: "2-digit",
                    hour12: true
                },
                cellAttributes: { 
                    class: { fieldName: 'customCSSClass' }
                }
            },
            {
                type: 'text',
                fieldName: 'title',
                label: 'Title',
                cellAttributes: { 
                    class: { fieldName: 'customCSSClass' }
                }
            },
            {
                type: 'text',
                fieldName: 'createdBy',
                label: 'Created By',
                cellAttributes: { 
                    class: { fieldName: 'customCSSClass' }
                }
            },
            {
                type: 'text',
                fieldName: 'preview',
                label: 'Additional Info',
                cellAttributes: { 
                    class: { fieldName: 'customCSSClass' }
                }
            }
        ];
    }

    connectedCallback() {
        getQuoteTreeExt({ recordId: this.recordId })
            .then((results) => {
                console.log('====>Results '+ results);
                results = JSON.parse(results);
                this.activityItems = results;
                this.isLoading = false;
                this.shouldDisplay = true;
            })
            .catch(error => {
                window.console.log('===>error '+error);
                this.shouldDisplay = false;
            });
    }
     getBaseUrl(){
         var baseUrl='https://'+location.host+'/';
         return baseUrl;
     }
    handleRowAction(event){
        console.log('Here is my message');
        this.displayDetail = true;
        const selectedRows = event.detail.selectedRows;
        for (let i = 0; i < selectedRows.length; i++){
            console.log("You selected: " + selectedRows[i].body);
            this.recordSubject = selectedRows[i].title;
            this.recordBody = selectedRows[i].body;
            this.recordPreview = selectedRows[i].additionalInfo;
            this.activityId = selectedRows[i].activityId;
            this.recordEntryType=selectedRows[i].entryType;
            if( selectedRows[i].entryType == 'Attachment'){
                this.buttonLabel = 'Download';
                this.showBody = false;
                this.showPreview = false;
                this.previewLabel = 'File Name';
            }else if(selectedRows[i].entryType == 'Note'){
                this.buttonLabel = 'Open';
                this.showBody = true;
                this.showPreview = false;
            }else if (selectedRows[i].entryType == 'Case'){
                this.buttonLabel = 'Open';
                this.showBody = true;
                this.showPreview = true;
                this.previewLabel = 'Case Detail';
            }else{
                this.buttonLabel = 'Open';
                this.showBody = true;
                this.showPreview = true;
                this.previewLabel = 'Sender Information';
            }
        }
    }
    handleButtonClick(event){
        if(this.recordEntryType=='Attachment'){
            var baseUrl=this.getBaseUrl();
            var downloadUrl=baseUrl+`/servlet/servlet.FileDownload?file=${this.activityId}`;
             var url=downloadUrl;
             window.open(url,"_blank");
        }
        else{
            //Navigate to record Page
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    "recordId" : this.activityId,
                    "actionName": "view"
                }
            }).then(url => {
                window.open(url,"_blank");
            });
        }
    }


}