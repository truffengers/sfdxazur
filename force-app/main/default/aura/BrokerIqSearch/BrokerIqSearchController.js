({
	doInit : function(component, event, helper) {
        helper.search(component);
	},
    filtersChanged : function(component, event, helper) {
        var params = event.getParams();
        if($A.util.isEmpty(params.oldValue)
            || (!$A.util.isEmpty(params.oldValue) && $A.util.isEmpty(params.value))
            || params.oldValue.value !== params.value.value ) {
            helper.search(component);
            component.set('v.currentPage', 1);
            component.set('v.upcomingCurrentPage', 1);
        }
    },
    currentPageChanged : function(component, event, helper) {
     	helper.search(component);
        
        var disableEvent = $A.get("e.c:BrokerIqMenuEvent");
        disableEvent.setParam('operation', 'disable');
        disableEvent.fire();
        var header$ = $('header');
        $(' body').animate(
            {
                scrollTop: $('.c-filters').offset().top - (header$.hasClass('is-hidden') ? 0 : header$.height()) + 'px'
            }, 
            {
                duration: 'medium',
                complete: $A.getCallback(function() {
                        window.setTimeout($A.getCallback(function() {
                            var enableEvent = $A.get("e.c:BrokerIqMenuEvent");
                            enableEvent.setParam('operation', 'enable');
                            enableEvent.fire();
                        }), 200);
                    })
            }
        );
    }
})