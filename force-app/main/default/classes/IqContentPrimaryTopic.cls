global class IqContentPrimaryTopic implements Nebula_Tools.AfterInsertI, Nebula_Tools.AfterUpdateI {

	global void handleAfterInsert(List<IQ_Content__c> newList) {
		handleAfterUpdate(null, newList);
	}

	global void handleAfterUpdate(List<IQ_Content__c> oldList, List<IQ_Content__c> newList) {
		List<IQ_Content__c> interestingIqContent = new List<IQ_Content__c>();
		Set<String> primaryTopics = new Set<String>();

		for(Integer i=0; i < newList.size(); ++i) {
			IQ_Content__c newIqContent = newList[i];

			if(newIqContent.Primary_Topic__c != null 
			   && (oldList == null || newIqContent.Primary_Topic__c != oldList[i].Primary_Topic__c)) {
				interestingIqContent.add(newIqContent);
				primaryTopics.add(newIqContent.Primary_Topic__c);
			}
		}

		if(interestingIqContent.isEmpty()) {
			return;
		}

		List<Topic> existingTopics = [SELECT Id, Name FROM Topic WHERE Name IN :primaryTopics AND NetworkId = null];

		Nebula_Tools.sObjectIndex topicsByName = new Nebula_Tools.sObjectIndex.Builder()
			.setIndexField('Name') 
   			.setData(existingTopics)
   			.setIsCaseInsensitive(true)
   			.build();

		List<Topic> topicsToInsert = new List<Topic>();

		for(String thisPrimaryTopic : primaryTopics) {
			if(topicsByName.get(thisPrimaryTopic) == null) {
				Topic newTopic = new Topic(Name = thisPrimaryTopic);
				topicsToInsert.add(newTopic);
			}
		}

		// We need to check for Test.isRunningTest() to avoid clashing with duplicate names in the
		// non-test world see:
		// https://salesforce.stackexchange.com/questions/12258/pushtopic-test-duplicate-name-bug
		// https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_data_access.htm#TestDataNotes
		if(Test.isRunningTest() &&  [SELECT Id, Name FROM Topic].isEmpty()) {
			return;
		}

		if(!topicsToInsert.isEmpty()) {
			insert topicsToInsert;
			for(Topic t : topicsToInsert) {
				t.Name = t.Name.toLowerCase();
				topicsByName.put(t);
			}
		}

		// SF prunes duplicates for this object, so just insert without checking
		List<TopicAssignment> topicAssignmentsToInsert = new List<TopicAssignment>();

		for(IQ_Content__c thisIqContent : interestingIqContent) {
			topicAssignmentsToInsert.add(new TopicAssignment(TopicId = topicsByName.get(thisIqContent.Primary_Topic__c).Id,
			                                                 EntityId = thisIqContent.Id));
		}

		insert topicAssignmentsToInsert;
	}

}