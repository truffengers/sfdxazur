({
    onSignupBannerInit : function(component) {

        if (TT && TT.imageParallax) {
            TT.imageParallax.register($('#js-sign-up-banner'), {height: '150'});
        }
        $A.util.addClass(component.find('background-image'), 'lazyload');
    }
})