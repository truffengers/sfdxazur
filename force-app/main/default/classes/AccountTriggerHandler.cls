/**
* @name: AccountTriggerHandler
* @description: This class is handling and dispatching the relevant account trigger functionality
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 03/11/2018

* Last Update:
* @description: Switching off the AccountDRCodeValidator
* @author: Agata Baraniak abaraniak@azuruw.com
* @date: 22/05/2019 
*/

public with sharing class AccountTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled()
    {
        return AccountTriggerHandler.IsDisabled != null ? AccountTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems)
    {
      //  AccountDRCodeValidator.handleAccounts(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
    {
      //  AccountDRCodeValidator.handleAccounts(newItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        AuditLogUtil.createAuditLog('Account', newItems.values(), null );
        EWAccountHelper.createBrokerShares(newItems);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        AuditLogUtil.createAuditLog('Account', newItems.values(), oldItems );
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}