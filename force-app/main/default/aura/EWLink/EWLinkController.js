({
	onClick : function(component, event, helper) {
		var recordId = component.get('v.recordId');
		var navEvt;
		if(recordId) {
			navEvt = $A.get("e.force:navigateToSObject");
		    navEvt.setParam("recordId", recordId);
		} else {
			navEvt = $A.get("e.force:navigateToURL");
		    navEvt.setParam("url", component.get('v.relativeUrl'));
		}
	    navEvt.fire();
	}
})