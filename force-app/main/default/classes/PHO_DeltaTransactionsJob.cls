/**
* @name: PHO_DeltaTransactionsJob
* @description: Asynchronous job pick up and handle processing of Phoenix 2019 Data.
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 30/03/2020
*/

global with sharing class PHO_DeltaTransactionsJob implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext context) {
        return  Database.getQueryLocator(buildQuery());
    }

    global void execute(Database.BatchableContext context, List<SObject> scope) {
        List<IBA_PolicyTransaction__c> deltaPolicyTransactions = (List<IBA_PolicyTransaction__c>)scope;
        PHO_DeltaTransactionsJobHelper.handleDeltaTransactions(scope);
    }

    global void finish(Database.BatchableContext context) {

    }

    private String buildQuery() {
        return 'SELECT Id, '+ String.join(PHO_Constants.DELTA_POLICY_TRANSACTION_FIELDS, ',') + ',' +
                    '(SELECT Id, '+ String.join(PHO_Constants.DELTA_POLICY_TRANSACTION_LINE_FIELDS, ',') +
                    ' FROM Policy_Transaction_Lines__r), ' +
                    '(SELECT Id, ' + String.join(PHO_Constants.DELTA_POLICY_CARRIER_LINE_FIELDS, ',') +
                    ' FROM FF_Policy_Carrier_Lines__r) ' +
                'FROM IBA_PolicyTransaction__c ' +
                'WHERE PHO_DeltaProcessed__c = false ' +
                'AND PHO_DoNotProcessDelta__c = false ' +
                'AND PHO_2019Delta__c = true';
    }
}