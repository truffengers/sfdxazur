public with sharing class ProductRequirementTriggerHandler implements ITriggerHandler {
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return ProductRequirementTriggerHandler.IsDisabled != null ? ProductRequirementTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
        EWProductRequirementHelper.onAfterInsert((Map<Id, vlocity_ins__ProductRequirement__c>) newItems);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
    }
}