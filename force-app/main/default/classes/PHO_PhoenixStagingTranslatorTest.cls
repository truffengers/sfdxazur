/**
* @name: PHO_PhoenixStagingTranslatorTest
* @description: This class contains test methods for PHO_PhoenixStagingTranslator class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 14/01/2020
* @lastChangedBy: Antigoni D'Mello  21/01/2021  PR-113: Changed carrier DR Code and added Carrier in products
* @lastChangedBy: Antigoni D'Mello  14/01/2021  PR-129: Added fixed commission rate in products
* @lastChangedBy: Antigoni D'Mello  14/04/2021  PR-135: Added assertion for transaction line Azur DD Fee
*/
@IsTest
public class PHO_PhoenixStagingTranslatorTest {

    private static final String CURRENCY_GBP = 'GBP';

    @TestSetup
    private static void createTestData() {
        String brokerDrCode = 'BAD';
        String carrierDrCode = 'CR0001';
        String mgaDrCode = 'MAD';

        Account dummyAccount = TestDataFactory.createBrokerageAccount('Dummy Account');

        Account producingBrokerAccount1 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAccount_DR8059');
        producingBrokerAccount1.EW_DRCode__c = 'DR8059';
        producingBrokerAccount1.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;
        Account producingBrokerAccount2 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAccount_DR8522');
        producingBrokerAccount2.EW_DRCode__c = 'DR8522';
        producingBrokerAccount2.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account accountingBrokerAccount = TestDataFactory.createBrokerageAccount('TestAccountingBrokerAccount');
        accountingBrokerAccount.EW_DRCode__c = brokerDrCode;
        accountingBrokerAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account carrierAccount = TestDataFactory.createCarrierAccount('TestCarrierAccount');
        carrierAccount.EW_DRCode__c = carrierDrCode;
        carrierAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account mgaAccount = TestDataFactory.createBrokerageAccount('TestMGAAccount');
        mgaAccount.EW_DRCode__c = mgaDrCode;
        mgaAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        insert new List<Account>{
                dummyAccount,
                producingBrokerAccount1,
                producingBrokerAccount2,
                accountingBrokerAccount,
                carrierAccount,
                mgaAccount
        };

        IBA_IBACustomSettings__c settings = new IBA_IBACustomSettings__c (
                PHO_PhoenixAccBrokerAccountDRCode__c = brokerDrCode,
                PHO_PhoenixMGAAccountDRCode__c = mgaDrCode,
                PHO_FixedHomeBrokerCommission__c = 36.7,
                PHO_FixedMotorBrokerCommission__c = 26.45,
                IBA_DummyAccount__c = dummyAccount.Id
        );
        insert settings;

        carrierAccount = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Carrier'];

        Product2 p1 = new Product2(Name = 'Motor Comprehensive', PHO_PhoenixProductCode__c = 'MOT01',  PHO_PhoenixSectionCode__c = 'MT01',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor', IBA_FixedBrokerCommissionRate__c = 26.45);
        Product2 p2 = new Product2(Name = 'Motor Uninsured Loss Recovery', PHO_PhoenixProductCode__c = 'MOT01', PHO_PhoenixSectionCode__c = 'MT02',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor', IBA_FixedBrokerCommissionRate__c = 26.45);
        Product2 p3 = new Product2(Name = 'Motor Breakdown', PHO_PhoenixProductCode__c = 'MOT01', PHO_PhoenixSectionCode__c = 'MT03',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor', IBA_FixedBrokerCommissionRate__c = 26.45);
        insert new List<Product2>{p1,p2,p3};

        List<PHO_PhoenixStaging__c> stagingRecords = Test.loadData(PHO_PhoenixStaging__c.SObjectType, 'PHO_PhoenixStagingTestDataSet1');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsSingleStagingRecord() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                        ' LIMIT 1');
        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                        (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT IBA_MGACommission__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(1, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'This Policy should be active');
        System.assertEquals(1, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 1 transaction record assigned as it\'s an active Policy');
        System.assertEquals(1, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(1, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(1, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        System.assertEquals(1, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        System.assertEquals(stagingRecords[0].PHO_MGACommission__c, transactionLines[0].IBA_MGACommission__c, 'Transaction Line should contain financial data from staging record');

        System.assertEquals(1, [SELECT COUNT() FROM c2g__codaDimension1__c], 'Wrong number of Dimension records created');
        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsAllNewRecords() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                        'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c');
        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                        IBA_ActivePolicy__c,
                        (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT IBA_MGACommission__c,
                        PHO_PolicyTransactionLineSeed__c,
                        IBA_AzurDDFee__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(3, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'This Policy should be active');
        System.assertNotEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[1].Policy_Status__c, 'This Policy should not be active');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[2].Policy_Status__c, 'This Policy should be active');
        System.assert(policies[1].Name.endsWith('V1'), 'This Policy should be a version 1 snapshot policy');
        System.assertEquals(policies[0].Id, policies[1].IBA_ActivePolicy__c, 'Snapshot Policy should be assigned to active Policy');
        System.assertEquals(2, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 2 transaction records assigned as it\'s an active Policy');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(5, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        Map<String, Decimal> MGACommissionByTransactionLineSeed = new Map<String, Decimal>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            Decimal aggregatedCommission = MGACommissionByTransactionLineSeed.get(stagingRecord.PHO_PolicyTransactionLineSeed__c);
            if (aggregatedCommission != null) {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeed__c, aggregatedCommission + stagingRecord.PHO_MGACommission__c);
            } else {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeed__c, stagingRecord.PHO_MGACommission__c);
            }
        }
        System.assertEquals(5, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        for (IBA_PolicyTransactionLine__c transactionLine : transactionLines) {
            System.assertEquals(MGACommissionByTransactionLineSeed.get(transactionLine.PHO_PolicyTransactionLineSeed__c), transactionLine.IBA_MGACommission__c, 'Transaction Line should contain financial data aggregated from all staging records with the same transaction line seed');
            System.assertEquals(0, transactionLine.IBA_AzurDDFee__c, 'Transaction Line Azur DD Fee should be zero');
        }

        System.assertEquals(5, [SELECT COUNT() FROM c2g__codaDimension1__c], 'Wrong number of Dimension records created');
        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsUpdateRecords() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c');
        List<PHO_PhoenixStaging__c> stagingRecords1 = new List<PHO_PhoenixStaging__c>();
        List<PHO_PhoenixStaging__c> stagingRecords2 = new List<PHO_PhoenixStaging__c>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            if ('1504005871'.equals(stagingRecord.PHO_AzurPolicyNumber__c)
                    && stagingRecord.PHO_EffectiveDate__c.date() == Date.newInstance(2019, 1, 2)) {
                stagingRecords1.add(stagingRecord);
            } else {
                stagingRecords2.add(stagingRecord);
            }
        }
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(stagingRecords1);

        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(stagingRecords2);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                        IBA_ActivePolicy__c,
                        (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT IBA_MGACommission__c,
                        PHO_PolicyTransactionLineSeed__c,
                        IBA_AzurDDFee__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(3, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'This Policy should be active');
        System.assertNotEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[1].Policy_Status__c, 'This Policy should not be active');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[2].Policy_Status__c, 'This Policy should be active');
        System.assert(policies[1].Name.endsWith('V1'), 'This Policy should be a version 1 snapshot policy');
        System.assertEquals(policies[0].Id, policies[1].IBA_ActivePolicy__c, 'Snapshot Policy should be assigned to active Policy');
        System.assertEquals(2, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 2 transaction records assigned as it\'s an active Policy');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(5, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        Map<String, Decimal> MGACommissionByTransactionLineSeed = new Map<String, Decimal>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            Decimal aggregatedCommission = MGACommissionByTransactionLineSeed.get(stagingRecord.PHO_PolicyTransactionLineSeed__c);
            if (aggregatedCommission != null) {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeed__c, aggregatedCommission + stagingRecord.PHO_MGACommission__c);
            } else {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeed__c, stagingRecord.PHO_MGACommission__c);
            }
        }
        System.assertEquals(5, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        for (IBA_PolicyTransactionLine__c transactionLine : transactionLines) {
            System.assertEquals(MGACommissionByTransactionLineSeed.get(transactionLine.PHO_PolicyTransactionLineSeed__c), transactionLine.IBA_MGACommission__c, 'Transaction Line should contain financial data aggregated from all staging records with the same transaction line seed');
            System.assertEquals(0, transactionLine.IBA_AzurDDFee__c, 'Transaction Line Azur DD Fee should be zero');
        }

        System.assertEquals(5, [SELECT COUNT() FROM c2g__codaDimension1__c], 'Wrong number of Dimension records created');
        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsWindbacks() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c');
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(stagingRecords);

        PHO_PhoenixStaging__c windbackForNotProcessedPT = stagingRecords[4].clone();
        windbackForNotProcessedPT.PHO_EntryType__c = 'WB';
        windbackForNotProcessedPT.PHO_EventNumber__c += 0.1;
        windbackForNotProcessedPT.PHO_PaymentReference__c = '-'+windbackForNotProcessedPT.PHO_PaymentReference__c;

        PHO_PhoenixStaging__c windbackForAlreadyCancelledPT = stagingRecords[stagingRecords.size()-4].clone();
        windbackForAlreadyCancelledPT.PHO_EntryType__c = 'WB';
        windbackForAlreadyCancelledPT.PHO_EventNumber__c += 0.1;
        windbackForAlreadyCancelledPT.PHO_PaymentReference__c = '-'+windbackForAlreadyCancelledPT.PHO_PaymentReference__c;

        // need to cancel one policy
        IBA_PolicyTransaction__c policyTransaction = [SELECT Id FROM IBA_PolicyTransaction__c WHERE Id = :windbackForAlreadyCancelledPT.PHO_PolicyTransaction__c];
        policyTransaction.IBA_Cancelled__c = true;
        update policyTransaction;

        List<PHO_PhoenixStaging__c> windbackStagings = new List<PHO_PhoenixStaging__c>{windbackForNotProcessedPT, windbackForAlreadyCancelledPT};
        insert windbackStagings;

        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(windbackStagings);
        Test.stopTest();

        // Verify results
        System.assert([SELECT IBA_Cancelled__c FROM IBA_PolicyTransaction__c WHERE Id = :windbackForNotProcessedPT.PHO_PolicyTransaction__c].IBA_Cancelled__c, 'Policy Transaction should become Cancelled.');
    }

    @IsTest
    private static void volumeTestTranslateToSalesforceObjects() {
        Integer recordLimit = 200;
        // Prepare data
        Product2 p1 = new Product2(Name = 'MOT02_MT01', PHO_PhoenixProductCode__c = 'MOT02',  PHO_PhoenixSectionCode__c = 'MT01');
        Product2 p2 = new Product2(Name = 'MOT02_MT02', PHO_PhoenixProductCode__c = 'MOT02', PHO_PhoenixSectionCode__c = 'MT02');
        Product2 p3 = new Product2(Name = 'MOT02_MT03', PHO_PhoenixProductCode__c = 'MOT02', PHO_PhoenixSectionCode__c = 'MT03');
        Product2 p4 = new Product2(Name = 'MOT02_MT04', PHO_PhoenixProductCode__c = 'MOT02', PHO_PhoenixSectionCode__c = 'MT04');
        Product2 p5 = new Product2(Name = 'PCG01_PC01', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC01');
        Product2 p6 = new Product2(Name = 'PCG01_PC02', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC02');
        Product2 p7 = new Product2(Name = 'PCG01_PC03', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC03');
        Product2 p8 = new Product2(Name = 'PCG01_PC04', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC04');
        Product2 p9 = new Product2(Name = 'PCG01_PC05', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC05');
        Product2 p10 = new Product2(Name = 'PCG01_PC07', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC07');
        Product2 p11 = new Product2(Name = 'PCG01_PC08', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC08');
        Product2 p12 = new Product2(Name = 'PCG01_PC09', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC09');
        Product2 p13 = new Product2(Name = 'PCG01_PC10', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC10');
        Product2 p14 = new Product2(Name = 'PCG01_PC11', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC11');
        insert new List<Product2>{p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};

        Test.loadData(PHO_PhoenixStaging__c.SObjectType, 'PHO_PhoenixStagingTestDataSet2');

        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                        ' LIMIT :recordLimit');
        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslator.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All selected staging records should be processed');
    }

}