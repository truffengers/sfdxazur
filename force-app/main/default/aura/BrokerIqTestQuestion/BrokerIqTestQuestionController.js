({
	testChanged : function(component, event, helper) {
		helper.testOrProgressChanged(component);
	},
	progressChanged : function(component, event, helper) {
		var progressChangedEvent = $A.get('e.c:BrokerIqTestProgressChanged');
		progressChangedEvent.setParam('value', event.getParam('value'));
		progressChangedEvent.fire();
		helper.testOrProgressChanged(component);
	},
	radioChanged : function(component, event, helper) {
		component.set('v.currentAnswer.Answer__c', event.target.dataset.value);
	},
	checkboxChanged : function(component, event, helper) {
		var checkboxes = component.find('checkbox');
		if(!$A.util.isArray(checkboxes)) {
			checkboxes = [checkboxes];
		}

		var values = checkboxes.filter(function(thisCheckbox) { return thisCheckbox.getElement().checked; })
			.map(function(thisCheckbox) { return thisCheckbox.getElement().dataset.value});
		component.set('v.currentAnswer.Answer__c', values.join(';'));
	},
	nextClicked : function(component, event, helper) {
		var cpdTestRecord = component.get('v.cpdTestRecord');
		var cpdAssessment = component.get('v.cpdAssessment');
		var progress = component.get('v.progress');

		if(progress < cpdTestRecord.Test_Questions__r.length - 1) {
			helper.saveCurrentAnswer(component, progress);
			
		} else {
			helper.helperFunctionAsPromise(helper.saveCurrentAnswer, component, progress)
			.then(function() {
				return helper.helperFunctionAsPromise(helper.submitCpdAssessment, component, cpdAssessment);
			})
			.then(function() {
				var navEvt = $A.get("e.force:navigateToSObject");
				navEvt.setParam("recordId", cpdAssessment.Id);
				navEvt.fire();
			});
        }
	}
})