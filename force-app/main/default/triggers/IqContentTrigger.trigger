trigger IqContentTrigger on IQ_Content__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  (new Nebula_Tools.MetaDataTriggerManager(IQ_Content__c.sObjectType)).handle();
}