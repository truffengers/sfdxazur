/**
* @name: IBA_BulkReleaseForPaymentJob
* @description: Job takes all c2g__codaPurchaseInvoice__c records and 
* do the release for payment process in asynchronous way.
*
*/

public with sharing class IBA_BulkReleaseForPaymentJob implements Queueable {

    private List<c2g__codaPurchaseInvoice__c> invoices;

    public IBA_BulkReleaseForPaymentJob(List<c2g__codaPurchaseInvoice__c> invoices) {
        this.invoices = invoices;
    }

    public void execute(QueueableContext context) {
        IBA_InvoiceBulkReleaseForPayment.bulkReleaseForPayment(invoices);
    }
}