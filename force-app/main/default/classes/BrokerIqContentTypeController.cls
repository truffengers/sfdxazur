public with sharing class BrokerIqContentTypeController {
	
	@AuraEnabled
	public static BrokerIqSearchFilterWrapper getSearchFilterWrapper(String metadataTypeId) {
		List<Broker_iQ_Record_Type__mdt> metadata = [SELECT Id, DeveloperName 
													FROM Broker_iQ_Record_Type__mdt
													WHERE Id = :metadataTypeId];

		if(metadata.isEmpty()) {
			return null;
		} else {
			List<RecordType> rts = [SELECT Id, Name
									FROM RecordType 
									WHERE DeveloperName = :metadata[0].DeveloperName
									AND SobjectType = 'IQ_Content__c'];

			if(rts.isEmpty()) {
				return null;
			} else {
				return new BrokerIqSearchFilterWrapper(rts[0].Name, rts[0].Id, null);
			}
		}
	}
}