/**
* @name: EWVlocityDocumentSectionQuoteClaims
* @description: Helper class for the Quote Claims Section
* @author: Steve Loftus sloftus@azuruw.com
* @date: 07/12/2018
*/
global with sharing class EWVlocityDocumentSectionQuoteClaims implements vlocity_ins.VlocityOpenInterface {

    private Integer maxLogSize = 32767;
    private static String CLASS_NAME = EWVlocityDocumentSectionQuoteClaims.class.getName();
    private static String METHOD_INVOKE_METHOD = 'invokeMethod';
    private static String VLOCITY_DESCRIPTION = 'Vlocity DataRaptor Remote Action call';

    private static String HEADING = 'Previous Claims';
    private static String BORDER_STYLE_BASE = 'border: .5px solid black;border-collapse: collapse;';
    private static String FONT_STYLE_BASE = 'font-size: 10pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String FONT_STYLE_HEADING = 'font-size: 12pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String ROW_STYLE_BASE = 'height: 20px;';

    public EWVlocityDocumentSectionQuoteClaims() {
        
    }

    public Boolean invokeMethod(
                            String methodName,
                            Map<String, Object> inputMap,
                            Map<String, Object> outputMap,
                            Map<String, Object> optionMap) {

        Boolean result = true;

        Azur_Log__c traceLog = new Azur_Log__c();

        traceLog.Class__c = CLASS_NAME;
        traceLog.Method__c = METHOD_INVOKE_METHOD;
        traceLog.Log_description__c = VLOCITY_DESCRIPTION;

        string logMessage = 'methodName:' + '\n' +
                methodName + '\n\n' +
                'inputMap:' + '\n' +
                JSON.serializePretty(inputMap) + '\n\n' +
                'optionMap:' + '\n' +
                JSON.serializePretty(optionMap);

        if (logMessage.length() > maxLogSize ) {
            traceLog.Log_message__c = logMessage.substring(0, maxLogSize);
        } else {
            traceLog.Log_message__c = logMessage;
        }

        try {

            if (methodName == 'buildDocumentSectionContent') {

                buildDocumentSectionContent(inputMap, outputMap, optionMap);

            } else {

                result = false;
            }

        } catch(Exception e) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(e);
            log.Class__c = CLASS_NAME;
            log.Method__c = METHOD_INVOKE_METHOD;
            insert log;

            result = false;

        } finally {

            string additionalLogMessage = traceLog.Log_message__c + '\n\n' + 'outputMap:' + '\n' + JSON.serializePretty(outputMap);

            if (additionalLogMessage.length() > maxLogSize ) {
                traceLog.Log_message__c = additionalLogMessage.substring(0, maxLogSize);
            } else {
                traceLog.Log_message__c = additionalLogMessage;
            }

            insert traceLog;

        }

        return result;
    }

    private void buildDocumentSectionContent (
                                        Map<String, Object> inputMap,
                                        Map<String, Object> outputMap,
                                        Map<String, Object> optionMap) {

        String dataJSON = (String)inputMap.get('dataJSON');
        Map<String, Object> documentData = (Map<String, Object>)JSON.deserializeUntyped(dataJSON);
        Map<String, Object> quoteAndQLIs = (Map<String, Object>)documentData.get('quoteAndQLIs');

        Id quoteId = (Id)quoteAndQLIs.get('quoteId');

        List<vlocity_ins__InsuranceClaim__c> claimList = [SELECT
                                                          EW_Claim_Date__c,
                                                          EW_Claim_Type__c,
                                                          EW_Claim_Value__c
                                                          FROM vlocity_ins__InsuranceClaim__c
                                                          WHERE EW_Quote__c = :quoteId
                                                          ORDER By EW_Claim_Date__c DESC];

        String content = '';

        Integer maxBlankLines = 26;

        if (!claimList.isEmpty()) {

            maxBlankLines = maxBlankLines - claimList.size() - 3;

            //Prepare header
            Map<String, String> headerMap = new Map<String, String> ();

            headerMap.put('1', 'Date');
            headerMap.put('2', 'Type');
            headerMap.put('3', 'Amount');

            content = '<p><span style=\"' + FONT_STYLE_HEADING + '\">' + HEADING + '</span></p>';

            content += '<table style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 100%\">';

            content += '<thead><tr>';

            for (String key: headerMap.keySet()){

                content += '<th style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';

                content += '<viawrapper>' + headerMap.get(key) + '</viawrapper></th>';
            }

            content += '</tr></thead>';

            content += '<tbody>';

            for (vlocity_ins__InsuranceClaim__c claim : claimList) {

                content += '<tr style=\"' + ROW_STYLE_BASE + '\">';

                content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                
                content += '<span style=\"' + FONT_STYLE_BASE + '\">';

                content += '<viawrapper>' + claim.EW_Claim_Date__c + '</viawrapper>';

                content += '</span></td>';

                content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                
                content += '<span style=\"' + FONT_STYLE_BASE + '\">';

                content += '<viawrapper>' + claim.EW_Claim_Type__c + '</viawrapper>';

                content += '</span></td>';

                content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                
                content += '<span style=\"' + FONT_STYLE_BASE + '\">';

                content += '<viawrapper>£' + claim.EW_Claim_Value__c + '</viawrapper>';

                content += '</span></td>';

                content += '</tr>';
            }

            content += '</tbody></table>';
        }

        for (Integer i = 0; i < maxBlankLines; i++) {

            content += '<br/>';
        }

        content += '<table style=\"' + FONT_STYLE_BASE + 'width: 100%\">';

        content += '<tbody>';

        content += '<tr style=\"' + ROW_STYLE_BASE + '\">';

        content += '<td style=\"' + ROW_STYLE_BASE + 'width: 100%; text-align: right;\">';

        content += '<strong>Page 8 of 9</strong>';

        content += '</td></tr></tbody></table>';

        outputMap.put('sectionContent', content);
    }
}