/**
* @name: IBA_TransactionLineItem
* @description: Trigger for the managed object c2g__codaTransactionLineItem__c
*
*/

trigger IBA_TransactionLineItem on c2g__codaTransactionLineItem__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_TransactionLineItemTriggerHandler());
}