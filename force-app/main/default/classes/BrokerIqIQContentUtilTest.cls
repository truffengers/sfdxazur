/**
* @name: BrokerIqIQContentUtilTest
* @description: BrokerIqIQContentUtil test class
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 18/12/2017
* @Modified: 
*/
@isTest
public class BrokerIqIQContentUtilTest {
    //Object name constants
    private static final String USER_SYSTEMADMIN_LASTNAME = 'Testing last name system admin'; 
    private static final String CATEGORY_NAME = 'Test class compliance'; 
    private static final String CONTRIBUTOR_NAME = 'Test class compliance'; 
    private static final String BT_CHANNEL_NAME = 'test class Azur underwriting'; 
    //Variables
    private static User systemAdmin;
    private static Category__c category;
    private static Broker_iQ_Contributor__c biqContributor;
    private static BrightTALK__Channel__c btChannel;
    
    @testSetup
    public static void setup() {
        //Insert system administrator
        systemAdmin = BrokerIqTestDataFactory.createSystemAdmistratorUser(USER_SYSTEMADMIN_LASTNAME);
        insert systemAdmin;
        
        System.runAs(systemAdmin) {
            //Insert category
            category = BrokerIqTestDataFactory.createCategory(CATEGORY_NAME);
            insert category;
            
            //Insert biq contributor
            Account account = BrokerIqTestDataFactory.createBusinessAccount('Test class business account');
            insert account;
            Contact contact = BrokerIqTestDataFactory.createBroker(account.Id, 'Test class contact');
            insert contact;
            biqContributor = BrokerIqTestDataFactory.createBrokerIQContributor(CONTRIBUTOR_NAME, contact.Id);
			insert biqContributor;
            
            //Insert channel
            btChannel = BrokerIqTestDataFactory.createBrightTalkChannel(BT_CHANNEL_NAME);
			insert btChannel;
        } 
    }
    
    private static User getSystemAdmin(){
        return [SELECT Id FROM User WHERE LastName =: USER_SYSTEMADMIN_LASTNAME AND isActive = true LIMIT 1];
    }
    
    private static void getSetupRecords(){
        category = [SELECT Id FROM Category__c WHERE Name =: CATEGORY_NAME  LIMIT 1];
        biqContributor = [SELECT Id FROM Broker_iQ_Contributor__c WHERE Name =: CONTRIBUTOR_NAME  LIMIT 1];
        btChannel = [SELECT Id FROM BrightTALK__Channel__c WHERE Name =: BT_CHANNEL_NAME  LIMIT 1];
    }
    
  	testMethod static void testGetUpcomingWebinars(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            	//Initialize variables
            	getSetupRecords();
            	DateTime startDateWebinar = Datetime.now().addHours(1); //Set upcoming webinar
                String webinarName = 'Test class webinar';
            	Integer limitRecords = 3;
            
 				//Insert webinar 
        	    IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, true);
          	    insert webinar;
            
            	//Insert webcast
            	BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateWebinar);
				insert btWebcast;            
            	
            	//Test 
           		List<Id> iqContentIds = new List<Id>{webinar.Id};
             	List<IQ_Content__c> upcomingWebinarsResult = BrokerIqIQContentUtil.getUpcomingWebinars(iqContentIds, new List<Id>(), limitRecords);
            Test.stopTest();

            //Assess results
            System.assertEquals(1, upcomingWebinarsResult.size());
            System.assertEquals(webinar.Id, upcomingWebinarsResult[0].Id);
            System.assertEquals(btWebcast.Id, upcomingWebinarsResult[0].BrightTALK_Webcast__c);
        } 
    } 
    
    testMethod static void testGetUpcomingWebinarsNoQueriedRecords(){
 		System.runAs(getSystemAdmin()) {
            Test.startTest();
            	//Initialize variables
            	getSetupRecords();
            	DateTime startDateWebinar = Datetime.now().addHours(1); //Set upcoming webinar
                String webinarName = 'Test class webinar';
            	Integer limitRecords = 3;
            
 				//Insert webinar 
        	    IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, false);
          	    insert webinar;
            
            	//Insert webcast
            	BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateWebinar);
				insert btWebcast;            
            	
            	//Test 
           		List<Id> iqContentIds = new List<Id>{webinar.Id};
             	List<IQ_Content__c> upcomingWebinarsResult = BrokerIqIQContentUtil.getUpcomingWebinars(new List<Id>(), iqContentIds, limitRecords);
            Test.stopTest();

            //Assess results
            System.assertEquals(0, upcomingWebinarsResult.size());
        }        
    }

    testMethod static void testGetUpcomingWebinarsNoGetRecentRecords(){
		System.runAs(getSystemAdmin()) {
            Test.startTest();
            	//Initialize variables
            	getSetupRecords();
            	DateTime startDateWebinar = Datetime.now().addHours(-1); //Set recent webinar
                String webinarName = 'Test class webinar';
            	Integer limitRecords = 3;
            
 				//Insert webinar 
        	    IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, false);
          	    insert webinar;
            
            	//Insert webcast
            	BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateWebinar);
				insert btWebcast;            
            	
            	//Test 
           		List<Id> iqContentIds = new List<Id>{webinar.Id};
             	List<IQ_Content__c> upcomingWebinarsResult = BrokerIqIQContentUtil.getUpcomingWebinars(iqContentIds, new List<Id>(), limitRecords);
            Test.stopTest();

            //Assess results
            System.assertEquals(0, upcomingWebinarsResult.size());
        }
    }
 
	testMethod static void testGetRecentWebinars(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            	//Initialize variables
            	getSetupRecords();
            	DateTime startDateWebinar = Datetime.now().addHours(-1); //Set recent webinar
                String webinarName = 'Test class webinar';
            	Integer limitRecords = 3;
            
 				//Insert webinar 
        	    IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, false);
          	    insert webinar;
            
            	//Insert webcast
            	BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateWebinar);
				insert btWebcast;            
            	
            	//Test 
           		List<Id> iqContentIds = new List<Id>{webinar.Id};
             	List<IQ_Content__c> recentWebinarsResult = BrokerIqIQContentUtil.getRecentWebinars(iqContentIds, new List<Id>(), limitRecords);
            Test.stopTest();

            //Assess results
            System.assertEquals(1, recentWebinarsResult.size());
            System.assertEquals(webinar.Id, recentWebinarsResult[0].Id);
            System.assertEquals(btWebcast.Id, recentWebinarsResult[0].BrightTALK_Webcast__c);
        } 
    } 

	testMethod static void testGetRecentWebinarsNoQueriedRecords(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            	//Initialize variables
            	getSetupRecords();
            	DateTime startDateWebinar = Datetime.now().addHours(-1); //Set recent webinar
                String webinarName = 'Test class webinar';
            	Integer limitRecords = 3;
            
 				//Insert webinar 
        	    IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, false);
          	    insert webinar;
            
            	//Insert webcast
            	BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateWebinar);
				insert btWebcast;            
            	
            	//Test 
           		List<Id> iqContentIds = new List<Id>{webinar.Id};
             	List<IQ_Content__c> recentWebinarsResult = BrokerIqIQContentUtil.getRecentWebinars(new List<Id>(), iqContentIds, limitRecords);
            Test.stopTest();

            //Assess results
            System.assertEquals(0, recentWebinarsResult.size());
        } 
    } 
    
	testMethod static void testGetRecentWebinarsNoGetUpcomingRecords(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            	//Initialize variables
            	getSetupRecords();
            	DateTime startDateWebinar = Datetime.now().addHours(1); //Set upcoming webinar
                String webinarName = 'Test class webinar';
            	Integer limitRecords = 3;
            
 				//Insert webinar 
        	    IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, false);
          	    insert webinar;
            
            	//Insert webcast
            	BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateWebinar);
				insert btWebcast;            
            	
            	//Test 
           		List<Id> iqContentIds = new List<Id>{webinar.Id};
             	List<IQ_Content__c> recentWebinarsResult = BrokerIqIQContentUtil.getRecentWebinars(iqContentIds, new List<Id>(), limitRecords);
            Test.stopTest();

            //Assess results
            System.assertEquals(0, recentWebinarsResult.size());
        } 
    } 
    
}