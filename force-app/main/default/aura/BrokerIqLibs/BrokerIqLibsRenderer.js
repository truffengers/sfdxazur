({
    afterRender: function (component, helper) {
        this.superAfterRender();
      //  helper.log(component, 'afterRender');

        if(!component.get('v.hasOtherDependency') || component.get('v.otherDependencyLoaded')) {
       //     helper.log(component, 'afterRender.otherDependencyLoaded');
            component.set('v.isRendered', true);
            helper.initJs(component, {isRendered: true});
        }
    },
    rerender : function(component, helper){
        this.superRerender();
    //    helper.log(component, 'rerender');
        if(!component.get('v.hasOtherDependency') || component.get('v.otherDependencyLoaded')) {
         //   helper.log(component, 'rerender.otherDependencyLoaded');
            component.set('v.isRendered', true);
            helper.initJs(component, {isRendered: true});
        }
    },
    unrender : function (component, helper) {
        this.superUnrender();
        component.set('v.jsInitialised', false);
        component.set('v.isRendered', false);

     //   helper.log(component, 'unrender');
    }
})