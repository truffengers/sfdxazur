/**
 * Created by roystonlloyd on 2019-05-24.
 */

trigger EW_ClauseAssignmentTrigger on EW_ClauseAssignment__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new EWClauseAssignmentTriggerHandler());
}