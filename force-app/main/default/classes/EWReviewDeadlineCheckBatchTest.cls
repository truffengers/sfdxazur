/**
* @name: EWReviewDeadlineCheckBatchTest
* @description: test class for EWReviewDeadlineCheckBatch  
* @author: Aleksejs Jedamenko ajedamenko@azuruw.com
* @date: 30/09/2020
*
*/
@isTest
private class EWReviewDeadlineCheckBatchTest {
    
    /**
    * @methodName: setup
    * @description:  creates a set of test data: accounts, oppty and 2 quotes
    * @dateCreated: 30/09/2020
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    */
    @testSetup
    static void setup(){
        
        Account testBroker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account testInsured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );

        insert new List<Account>{
                testBroker, testInsured
        };

        Opportunity testOpportunity = EWOpportunityDataFactory.createOpportunity(null, testInsured.Id, testBroker.Id, true);

        Date todayDate = Date.today();
        
        Quote testQuoteToDisableDD = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testQuoteToDisableDD.Status = EWConstants.QUOTE_STATUS_HOLD_ON_COVER;
        testQuoteToDisableDD.EW_DDReviewDeadlineDate__c = todayDate.addDays(-3);
        
        Quote testQuoteToCheckInPCL = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testQuoteToCheckInPCL.Status = EWConstants.QUOTE_STATUS_HOLD_ON_COVER;
        testQuoteToCheckInPCL.EW_DDReviewDeadlineDate__c = todayDate.addDays(3);
        insert new List<Quote> {testQuoteToDisableDD, testQuoteToCheckInPCL};
    }
    
     /**
    * @methodName: reviewDeadlineCheckBatchTest
    * @description: test EWPolicyLapsingBatch batch. Scenario: Test setup creates 2 records with a status Hold on Cover. After a batch class invocation assertions confirm that one of them with
    * EW_IsDDDisabled__c flag set to true, but second one with populated EW_PCLAgreementId__c field.
    * @dateCreated: 30/09/2020
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    */
    @isTest
    private static void reviewDeadlineCheckBatchTest(){
        
        Test.startTest();
        Id batchId = Database.executeBatch(new EWReviewDeadlineCheckBatch(),50);
        Test.stopTest();
        List<Quote> expectedDDDisabledQuotes = [SELECT Status FROM Quote WHERE Status = :EWConstants.QUOTE_STATUS_QUOTED and EW_IsDDDisabled__c=true];
        List<Quote> expectedQuotesWithPoulatedPCLId = [SELECT Status FROM Quote WHERE Status = :EWConstants.QUOTE_STATUS_HOLD_ON_COVER and EW_PCLAgreementId__c !=null];       
        System.assertEquals(1, expectedDDDisabledQuotes.size());
        System.assertEquals(1, expectedQuotesWithPoulatedPCLId.size());
    
    }

}