/**
* @name: EWEmailDataFactory
* @description: Reusable factory methods for email object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 24/09/2018
*/
@isTest
public class EWEmailDataFactory {

	public static EmailTemplate createTextEmailTemplate(String name,
                                          		  		String uniqueName,
                                               			Id folderId,
                                               			Boolean insertRecord) {

		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.isActive = true;
		emailTemplate.Name = (name == null) ? 'testTemplate' : name;
		emailTemplate.DeveloperName = (uniqueName == null) ? 'testTemplate' : uniqueName;
		emailTemplate.TemplateType = 'text';
		emailTemplate.FolderId = folderId;

        if (insertRecord) {
        	insert emailTemplate;
        }

        return emailTemplate;
	}
}