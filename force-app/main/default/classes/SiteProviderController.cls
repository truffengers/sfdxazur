/**
* @name: SiteProviderController
* @description: Class to provide information about the community
* @author: Aidan Harding Nebula Consulting
* @date:01 /11/2017
* @modifiedBy: Antigoni D'Mello	21/12/2017	AZ-77: Added private method getBaseSecureUrl
* @modifiedBy: Andrew Xu	BIQ-133: Added private method getSiteAdminEmail
*/
public class SiteProviderController {

	public class SiteMetadata {
		@AuraEnabled
		public String adminEmail = getSiteAdminEmail();
		@AuraEnabled
		public String baseSecureUrl = Site.getBaseSecureUrl();
		@AuraEnabled
		public String pathPrefix = Site.getPathPrefix();
		@AuraEnabled
		public Boolean isSandbox = OrgInformationUtility.isSandbox();
	}

	@AuraEnabled
	public static SiteMetadata getSiteMetadata() {
        	SiteMetadata sm = new SiteMetadata();
		return sm;
	}
    
	private static String getSiteAdminEmail() {
	        try {
	        	String adminEmail = Site.getAdminEmail();
	        	return adminEmail;		
	        } catch (Exception ex) {
	            	// We can't get hold of site admin so emailing ci@azuruw.com
	            	System.debug('Exception caught in SiteProviderController.getSiteAdminEmail '+ex);
	            	Azur_Log__c al = new Azur_Log__c();
	            	al.Method__c = 'getSiteAdminEmail';
	            	al.Class__c = 'SiteProviderController';
	            	al.Log_description__c = ex.getStackTraceString();
	            	al.Log_message__c = ex.getMessage();
	            	insert al;

			String ciEmail = System.Label.CIEmail;
            		return ciEmail;
        }    
    }
        

	/**
	* Originally, the baseSecureUrl was obtained using the following ternary operator:
	*
	* `OrgInformationUtility.isSandbox() ? Site.getBaseSecureUrl()+'/brokeriq21/s' : Site.getBaseSecureUrl();`
	*
	* It now appears to be broken because the `/brokeriq21/s` being added is extra - this part of the URL is already
	* included in the baseSecureUrl itself (see EWSiteProviderController for the Broker Hub equivalent).
	*
	* - Andrew Xu, 06/07/2020
	* */
}