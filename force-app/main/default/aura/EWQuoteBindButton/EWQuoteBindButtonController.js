({
    doInit: function (component, event, helper) {
        const recordId = component.get('v.recordId');
        let action = component.get("c.loadBindSetupData");
        action.setParam('quoteId', recordId);

        action.setCallback(this, function (response) {
            const responseJson = response.getReturnValue();
            let redirectToRecord = false;
            
            let urlPath; 
            switch(responseJson.quoteTransactionType) {
                case 'MTA':
                    urlPath = '/bind-mta-quote?recordId='
                    break;
                default:
                	urlPath = '/bind-quote?recordId=';
            }
            
            if (!responseJson.isCommunity) {
                redirectToRecord = true;
            } else if (responseJson.hasSavedJsonData) {
                let urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    'url': urlPath + recordId
                });
                urlEvent.fire();
            }

            if (redirectToRecord) {
                let sObjectEvent = $A.get("e.force:navigateToSObject");
                sObjectEvent.setParams({
                    "recordId": recordId,
                });
                sObjectEvent.fire();
            }
        });

        $A.enqueueAction(action);
    }
});