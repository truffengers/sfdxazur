/**
 * Created by llyrjones on 04/12/2018.
 */

public with sharing class EWQuoteLineItemHelper {

    public static void handleVlocityAttributes(List<SObject> newItems) {

        // prevent field population on non-applicable QLI updates
        Set<String> bypassedQuoteStatuses = new Set<String>{
                EWConstants.QUOTE_STATUS_REVERSED, EWConstants.QUOTE_STATUS_REVERSAL
        };
        Set<Id> quoteIds = new Set<Id>();
        for (QuoteLineItem qli : (List<QuoteLineItem>) newItems) {
            quoteIds.add(qli.QuoteId);
        }
        Map<Id,Quote> quoteMap = new Map<Id,Quote>([SELECT Id, Status FROM Quote WHERE Id IN: quoteIds]);
        for (QuoteLineItem qli : (List<QuoteLineItem>) newItems) {
            if (quoteMap.containsKey(qli.QuoteId) && bypassedQuoteStatuses.contains(quoteMap.get(qli.QuoteId).Status)) return;
        }

        // reset currency values prior to population from the JSON
        EWJSONHelper.resetRecordValues(newItems, EWConstants.ATTRIBUTE_SELECTED_VALUE_JSON_TO_API_MAPPINNG);

        //populate values from JSON string
        EWJSONHelper.mapJsonFieldsToSFFields(newItems, EWConstants.ATTRIBUTE_SELECTED_VALUE_JSON_TO_API_MAPPINNG);

        Set<Id> quotesIds = new Set<Id>();

        for (SObject obj : newItems) {
            QuoteLineItem qli = (QuoteLineItem) obj;

            quotesIds.add(qli.QuoteId);

            if (!String.isEmpty(qli.vlocity_ins__AttributeSelectedValues__c)) {
                qli.EW_AzurCommissionValue__c = qli.EW_AzurCommissionValue__c == null ? 0 : qli.EW_AzurCommissionValue__c;
                qli.EW_BrokerCommissionValue__c = qli.EW_BrokerCommissionValue__c == null ? 0 : qli.EW_BrokerCommissionValue__c;
                qli.EW_IPTTotal__c = qli.EW_IPTTotal__c == null ? 0 : qli.EW_IPTTotal__c;
                qli.EW_PremiumGrossExIPT__c = qli.EW_PremiumGrossExIPT__c == null ? 0 : qli.EW_PremiumGrossExIPT__c;
                qli.EW_PremiumGrossIncIPT__c = qli.EW_PremiumGrossIncIPT__c == null ? 0 : qli.EW_PremiumGrossIncIPT__c;
                qli.EW_PremiumNet__c = qli.EW_PremiumNet__c == null ? 0 : qli.EW_PremiumNet__c;
                qli.Quantity = 1;

                Map<String, Object> attributeSelectedValues = (Map<String, Object>) JSON.deserializeUntyped(qli.vlocity_ins__AttributeSelectedValues__c);
                if (attributeSelectedValues.containsKey(EWConstants.ATTR_ART_VALUE)
                        || attributeSelectedValues.containsKey(EWConstants.ATTR_JEWELLERY_VALUE)
                        || attributeSelectedValues.containsKey(EWConstants.ATTR_PEDAL_VALUE)) {
                    qli.EW_RatePerSI__c =
                            (qli.EW_PremiumGrossExIPT__c == null || qli.EW_SpecifiedItemValue__c == null || qli.EW_SpecifiedItemValue__c == 0)
                                    ? 0
                                    : qli.EW_PremiumGrossExIPT__c / qli.EW_SpecifiedItemValue__c;
                }
            }
        }

        Map<Id, Quote> quotes = new Map<Id, Quote>([
                SELECT Id, EW_TypeofTransaction__c, EW_BrokerCommissionPercent__c
                FROM Quote
                WHERE Id IN :quotesIds
        ]);

        for (SObject obj : newItems) {
            QuoteLineItem qli = (QuoteLineItem) obj;
            Quote parentQuote = quotes.get(qli.QuoteId);

            if (parentQuote.EW_BrokerCommissionPercent__c != null && qli.EW_PremiumGrossExIPT__c != null) {
                Decimal brokerCommissionVal = (parentQuote.EW_BrokerCommissionPercent__c / 100 * qli.EW_PremiumGrossExIPT__c)
                        .setScale(2, RoundingMode.HALF_UP);

                if (qli.EW_BrokerCommissionValue__c != brokerCommissionVal) {
                    qli.EW_BrokerCommissionValue__c = brokerCommissionVal;
                }
            }
            // Condition for renewal was added on 11/02/2020 by Aleksejs Jedamenko
            if (parentQuote.EW_TypeofTransaction__c == EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE || parentQuote.EW_TypeofTransaction__c == EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL) {
                qli.EW_Transactional_IPT__c = qli.EW_IPTTotal__c;
                qli.EW_Transactional_PremiumNet__c = qli.EW_PremiumNet__c;
                qli.EW_Transactional_AzurCommissionValue__c = qli.EW_AzurCommissionValue__c;
                qli.EW_Transactional_BrokerCommissionValue__c = qli.EW_BrokerCommissionValue__c;
                qli.EW_Transactional_PremiumGrossExIPT__c = qli.EW_PremiumGrossExIPT__c;
                qli.EW_Transactional_PremiumGrossIncIPT__c = qli.EW_PremiumGrossIncIPT__c;
            }
        }
    }

    public static void updateAttributeSelectedValues(Id quoteId, Map<String, Object> jsonClientDetails) {

        String JSONString;

        List<QuoteLineItem> qLineItem = [
                SELECT vlocity_ins__AttributeSelectedValues__c
                FROM QuoteLineItem
                WHERE Quote.Id = :quoteId
                AND Product2.Name = 'Insured Entity'
                LIMIT 1
        ];

        If (qLineItem.size() > 0 || !qLineItem.isEmpty()) {

            JSONString = qLineItem[0].vlocity_ins__AttributeSelectedValues__c;
            JSONString = EWJSONHelper.updateJSONstring(JSONString, jsonClientDetails, false, null);
            qLineItem[0].vlocity_ins__AttributeSelectedValues__c = JSONString;

            update qLineItem;
        }
    }

    
    public static Map<String, Object> quoteLineItemsForCancellation(Quote targetQuote, Integer daysRemaining, Boolean doClone, String daysFromInception) {
        Map<String, Object> cloneQLIResMap = new Map<String, Object>();
        List<QuoteLineItem> qLICloneList = new List<QuoteLineItem>();
        String targetQuoteId = targetQuote.Id;
        Decimal multiplier = (1 / (1 - targetQuote.EW_TotalCommissionPercent__c / 100)).setScale(5, RoundingMode.HALF_UP);
        List<String> quoteLIFieldsApiNameList = new List<String>(
                QuoteLineItem.SObjectType.getDescribe().fields.getMap().keySet()
        );
        String quoteLIQueryString = 'SELECT ' + String.join(quoteLIFieldsApiNameList, ',') + ' FROM QuoteLineItem WHERE quoteId = :targetQuoteId';
        List<QuoteLineItem> qLIList = Database.query(quoteLIQueryString);

        for (QuoteLineItem qLI : qLIList) {
            QuoteLineItem qLIClone = doClone ? qLI.clone(false, true) : qLI;

            if(String.isNotBlank(daysFromInception) && Integer.valueOf(daysFromInception) > 15){
                qLIClone.EW_Transactional_PremiumNet__c = -((qLIClone.EW_PremiumNet__c / 365) * daysRemaining).setScale(2, RoundingMode.HALF_UP);
                qLIClone.EW_Transactional_PremiumGrossExIPT__c = (qLIClone.EW_Transactional_PremiumNet__c * multiplier).setScale(2, RoundingMode.HALF_UP);
                qLIClone.EW_Transactional_IPT__c = (qLIClone.EW_Transactional_PremiumGrossExIPT__c * targetQuote.EW_IPTPercent__c / 100).setScale(2, RoundingMode.HALF_UP);
                qLIClone.EW_Transactional_PremiumGrossIncIPT__c = (qLIClone.EW_Transactional_PremiumGrossExIPT__c + qLIClone.EW_Transactional_IPT__c).setScale(2, RoundingMode.HALF_UP);
                qLIClone.EW_Transactional_AzurCommissionValue__c = (qLIClone.EW_Transactional_PremiumGrossExIPT__c * targetQuote.EW_AzurCommissionPercent__c / 100).setScale(2, RoundingMode.HALF_UP);
                qLIClone.EW_Transactional_BrokerCommissionValue__c = (qLIClone.EW_Transactional_PremiumGrossExIPT__c * targetQuote.EW_BrokerCommissionPercent__c / 100).setScale(2, RoundingMode.HALF_UP);

            }else{
                qLIClone.EW_Transactional_PremiumNet__c = -qLIClone.EW_PremiumNet__c;
                qLIClone.EW_Transactional_PremiumGrossExIPT__c = -qLIClone.EW_PremiumGrossExIPT__c;
                qLIClone.EW_Transactional_IPT__c = -qLIClone.EW_IPTTotal__c;
                qLIClone.EW_Transactional_PremiumGrossIncIPT__c = -qLIClone.EW_PremiumGrossIncIPT__c;
                qLIClone.EW_Transactional_AzurCommissionValue__c = -qLIClone.EW_AzurCommissionValue__c;
                qLIClone.EW_Transactional_BrokerCommissionValue__c = -qLIClone.EW_BrokerCommissionValue__c;
            }

            if(qLIClone.vlocity_ins__ProductName__c == EWConstants.EW_PROD_NAME_EMERGING_WEALTH){
                cloneQLIResMap.put('EW_Gross_Premium_Excluding_IPT__c', qLIClone.EW_Transactional_PremiumGrossExIPT__c);
                cloneQLIResMap.put('vlocity_ins__StandardPremium__c', qLIClone.EW_Transactional_PremiumGrossIncIPT__c);
                cloneQLIResMap.put('EW_Gross_Premium_IPT__c', qLIClone.EW_Transactional_IPT__c);
            }
            qLICloneList.add(qLIClone);
        }
        cloneQLIResMap.put('qLICloneList', qLICloneList);

        return cloneQLIResMap;
    }

}