@isTest
public class IXHelperTest {
    private static User brokerUser;
    
    private static Account brokerAccount;
    private static Contact brokerContact;
    private static Profile brokerProfile;
    
	static testMethod void testCheckTermsAgreed() {
        Boolean testResult;
        
        setup();
        User brokerUser;
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);
        
        System.assertEquals(false, brokerUser.ix_Terms_Agreed__c);
 
        System.runAs(brokerUser) {
        	Test.startTest();
        	testResult = IXHelper.checkTermsAgreed();
        	Test.stopTest();
        }
        
        System.assertEquals(false, testResult);
    }
    
    static testMethod void testAgreeToTerms() {
        User returnedUser;
        
        setup();
        User brokerUser;
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);
        
        System.assertEquals(false, brokerUser.ix_Terms_Agreed__c);
        
        System.runAs(brokerUser) {
            Test.startTest();
            returnedUser = IXHelper.agreeToTerms();
            Test.stopTest();
        }
        
        System.assertEquals(true, returnedUser.ix_Terms_Agreed__c);
    }
    
    static testMethod void testDisagreeToTerms() {
        User returnedUser;
        
        setup();
        User brokerUser;
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, false);
        
        brokerUser.ix_Terms_Agreed__c = true;
        insert brokerUser;
        
        System.assertEquals(true, brokerUser.ix_Terms_Agreed__c);
        
        System.runAs(brokerUser) {
            Test.startTest();
            returnedUser = IXHelper.disagreeToTerms();
            Test.stopTest();
        }
        
        System.assertEquals(false, returnedUser.ix_Terms_Agreed__c);
    }
    
    static void setup() {
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);
        User systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            techteamUserRole.Id,
            true);

        System.runAs(systemAdminUser) {
            brokerAccount = EWAccountDataFactory.createBrokerAccount(null, true);
            brokerContact = EWContactDataFactory.createContact(null, null, brokerAccount.Id, true);
            brokerProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_EW_PARTNER_NAME);
        }
    }
}