/**
* @name: TestDataFactory
* @description: Reusable factory methods for apex test classes
* @author: Antigoni Tsouri atsouri@azuruw.com
* @lastChangedBy: Antigoni Tsouri 	13/04/2017
* @lastChangedBy: Rahma Belghouti 	10/05/2017 Add record type name in the parameters to make methods generic for multi recordType
* @lastChangedBy: Rahma Belghouti 	10/05/2017 Fix errors in opportunity and quote methods 
* @lastChangedBy: Rahma Belghouti 	10/05/2017 Add policy member relationship method
* @lastChangedBy: Rahma Belghouti 	10/05/2017 Add II parameter to coverage	
* @lastChangedBy: Antigoni Tsouri 	26/05/2017 Add createProduct, createCarrierAccount, createBusiness methods
* @lastChangedBy: Antigoni Tsouri 	03/07/2017 Add createPolicyMaster method 
* @lastChangedBy: Ignacio Sarmiento 09/10/2017 Add 'insertProducts' and 'insertAssets' method
* @lastChangedBy: Ignacio Sarmiento 19/10/2017 Add 'insertAccounts', 'insertAttachments', 'insertNotes', 'insertGoogleDocs', 'insertContentDocuments' methods
* @lastChangedBy: Ignacio Sarmiento 23/10/2017 Fix bug 'Duplicate user' and add 'lastname attribute in 'createUser' method
* @lastChangedBy: Antigoni Tsouri   24/10/2017 Added 'createCarrierRangeSettings'
* @lastChangedBy: Antigoni D'Mello	01/11/2018 EWH-153 Replaced old Brokerage DR Code field and added new method getRandomDRCode
*/
@isTest
public class TestDataFactory 
{
    //Constants
    public static final String PROFILE_SYSTEMADMINISTRATOR_NAME = 'System Administrator';
    public static final String PROFILE_AZURUNDERWRITER_NAME ='Azur Underwriter';
	public static final String ROLE_TECHTEAM_NAME = 'Tech team';

	public static Account createBrokerageAccount(String name) 
	{
		Id brokerageRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency/Brokerage').getRecordTypeId();
		Account acc = new Account();
		acc.name = name;
		acc.EW_DRCode__c = 'DR' + Datetime.now().getTime();
		acc.BillingStreet = '1 ApexTest BillingStreet';
		acc.BillingCity = 'London';
		acc.BillingPostalCode = 'AP1 2TE';
		acc.BillingCountry = 'United Kingdom';
		acc.RecordTypeId = brokerageRecId; 
		acc.c2g__CODATaxCalculationMethod__c = 'Gross';
		acc.CurrencyIsoCode = 'GBP';
		acc.c2g__CODAAccountTradingCurrency__c = 'GBP';
		return acc;
	}

	public static Account createPersonAccount(String salutation, String fname, String lname) 
	{
		Id personRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		Account acc = new Account();
		acc.Salutation = salutation;
		acc.FirstName = fname;
		acc.LastName = lname;
		acc.PersonBirthdate = Date.newInstance(1985, 03, 10);
		acc.Marital_Status__c = 'Married';
		acc.BillingStreet = '1 ApexTest InsuredPerson';
		acc.BillingCity = 'London';
		acc.BillingPostalCode = 'AP2 1TE';
		acc.BillingCountry = 'United Kingdom';
		acc.PersonEmail = 'person.test@testEmail.com';
        acc.gender__pc = 'Male';
		acc.RecordTypeId = personRecId;
		return acc;
	}

	public static Account createCarrierAccount(String name) 
	{
		Id carrierRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier').getRecordTypeId();
		Account acc = new Account();
		acc.Name = name;
		acc.RecordTypeId = carrierRecId;
		return acc;
	}

	public static Account createBusinessAccount(String name) 
	{
		Id businessRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
		Account acc = new Account();
		acc.Name = name;
		acc.RecordTypeId = businessRecId;
		return acc;
	}
	
	public static Contact createBroker(Id accId, String salutation, String fname, String lname) 
	{
		Id brokerRecId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Producer').getRecordTypeId();
		Contact con = new Contact();
		con.AccountId = accId;
		con.Salutation = salutation;
		con.FirstName = fname;
		con.LastName = lname;
		con.Email = 'broker.test@testEmail.com';
		con.MailingStreet = '1 ApexTest BrokerStreet';
		con.MailingCity = 'London';
		con.MailingPostalCode = 'BR1 2AT';
		con.MailingCountry = 'United Kingdom';
		con.RecordTypeId = brokerRecId;
		return con;
	}

	public static Opportunity createOpportunity(Id accId, String recordTypeName) 
	{
	    if(String.isBlank(recordTypeName)) recordTypeName = 'Motor';
		Id motorOppRecId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
		Opportunity opp = new Opportunity();
		opp.Name = 'testOpportunity';
		opp.CloseDate = System.today().addYears(1);
		opp.StageName = 'Open';
		opp.RecordTypeId = motorOppRecId;
		return opp;
	}
    
    public static Policy_Member_Rel__c createMember(Id accId, Id quoteId, String type) 
	{
	   Policy_Member_Rel__c member = new Policy_Member_Rel__c();
	   member.account__c = accId;
	   member.Type__c = type;
	   member.policy__c = quoteId;
	   return member;
	}

	public static Asset createQuote(Id oppId, Id brokerId, Id policyholderId, String recordTypeName) 
	{
	    if(String.isBlank(recordTypeName)) recordTypeName = 'Quote';
		Id motorRecId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
		Asset asset = new Asset();
		asset.name = 'testAsset';
		asset.Opportunity__c = oppId;
		asset.RecordTypeId = motorRecId;
		asset.AccountId__c = policyholderId;
		asset.AccountId = policyholderId;
		asset.vlocity_ins__PrimaryProducerId__c = brokerId;
		asset.Quote_Received_Date__c = System.now();
		asset.Inception_Start_Date_Time__c = System.now();
		asset.Inception_End_Date_Time__c = System.now().addYears(1);
		asset.Auto_Renew__c = true;
		asset.Commission__c = 3;
		return asset;
	}
    
    //Insert assets
    public static void insertAssets(list<string> AssetNames, id RecordTypeId, id AccountId, id ProductId)
    {
        list<Asset> AssetsToInsert = new list<Asset>();
        for(string an: AssetNames){
            Asset asset = new Asset();
        	asset.Name = an;
            asset.RecordTypeId = RecordTypeId;
            asset.AccountId__c = AccountId;
        	asset.AccountId = AccountId;
            asset.Product2Id = ProductId;
            AssetsToInsert.add(asset);
        }
        try{
            insert AssetsToInsert;
        }catch(Exception ex){
			LogBuilder.insertLog(ex);
        }
    }

	public static Vehicle__c createVehicle(String recordTypeName)
	{
	    if(String.isBlank(recordTypeName)) recordTypeName = 'Auto';
		Id autoRecId = Schema.SObjectType.Vehicle__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
		Vehicle__c vehicle = new Vehicle__c();
		vehicle.Make__c = 'ASTON MARTIN';
		vehicle.Model__c = '147 JTD LUSSO 8V - 1910CC';
		vehicle.Registration_Number__c = 'REG-1234';
		vehicle.VehicleAge__c = 3;
		vehicle.VehicleGroup__c = 1;
		vehicle.EngineSize__c = 12;
		vehicle.Vehicle_Type__c = 'Classic Cars';
		vehicle.Transmission__c = 'Automatic';
		vehicle.Fuel_Type__c = 'Electric';
		vehicle.Build_Year__c = 2014;
		vehicle.RecordTypeId = autoRecId; 
		return vehicle;
	}

	public static Insured_Item__c createInsuredVehicle(Id vehicleId, Id quoteId) 
	{
		Insured_Item__c insuredItem = new Insured_Item__c();
		insuredItem.Vehicle__c = vehicleId;
		insuredItem.Asset__c = quoteId;
		return insuredItem;
	}

	public static Coverage__c createCoverage(Id quoteId, String coverageType, String name, Id memberId, Id iItemId) 
	{
		Coverage__c coverage = new Coverage__c();
		coverage.Asset__c = quoteId;
		coverage.Name = name;
		coverage.Type__c = coverageType;
		coverage.Active__c = true;
		coverage.Primary_Driver__c = true;
		coverage.Rated_Driver__c = true;
		coverage.Limit__c = 123.00;
		coverage.Cover_Start_Date__c = System.now();
		coverage.Cover_End_Date__c = System.now().addYears(1);
		coverage.PremiumAmount__c = 250.00;
		coverage.Premium_AP__c = 250.00;
		coverage.Tax_Amount__c = 25.00;
		coverage.Tax_Amount_AP__c = 25.00;
		coverage.Gross_Premium__c = 275.00;
		coverage.Gross_Premium_AP__c = 275.00;
		coverage.Commission__c = 7.50;
		coverage.Commission_AP__c = 7.50;
		coverage.Azur_Commission__c = 80.00;
		coverage.Total_Azur_Commission_AP__c = 80.00;
		coverage.Chargeable_Premium_CP__c = 250.00;
		coverage.Chargeable_Tax_CP__c = 25.00;
		coverage.Chargeable_Gross_Premium_CP__c = 275.00;
		coverage.Chargeable_Commission_CP__c = 7.50;
		coverage.Chargeable_Azur_Commission__c = 80.00;
		coverage.Policy_Member_Relationship__c = memberId;
		coverage.Insured_Item__c = iItemId;
		return coverage;
	}

	public static c2g__codaCompany__c createCompany() 
	{
		c2g__codaCompany__c company = new c2g__codaCompany__c();
		company.Name = 'ApexTestCompany';
		company.RecordTypeId = Schema.SObjectType.c2g__codaCompany__c.RecordTypeInfosByName.get('VAT').RecordTypeId;
		return company;
	}

	public static c2g__codaGeneralLedgerAccount__c createGeneralLedger()
	{
		c2g__codaGeneralLedgerAccount__c gla = new c2g__codaGeneralLedgerAccount__c();
		gla.Name = 'ApexTestGeneralLedger';
		gla.c2g__ReportingCode__c = 'xyz1';
		gla.c2g__Type__c = 'Balance Sheet';
		return gla;
	}

	public static Risk__c createRisk(Id parentId)
	{
		Id recordTypeId;
		Map<String, Schema.RecordTypeInfo> rtInfos = Schema.SObjectType.Risk__c.getRecordTypeInfosByDeveloperName();

		if(parentId == null){
			recordTypeId = rtInfos.get('Parent_Risk').recordTypeId;
		}else{
			recordTypeId = rtInfos.get('Child_Risk').recordTypeId;
		}
		return createRisk(parentId, recordTypeId);
	}

	public static Risk__c createRisk(Id parentId, Id recordTypeId)
	{
		Risk__c risk = new Risk__c();
		risk.Gross_Risk_Impact__c = 2;
		risk.Gross_Risk_Likelihood__c = 2;
		risk.Impact__c = 'Test Impact';
		risk.Impact_Detail__c = 'Impact detail';
		risk.Mitigating_Controls_Detail__c = 'Test to mitigate controls';
		risk.Net_Risk_Impact__c = 3;
		risk.Net_Risk_Likelihood__c = 3;
		risk.Parent_Risk__c	= parentId;
		risk.Scenario__c = 'A risk scenario to test';
		risk.Scenario_Detail__c	= 'The scenarion detail';
		risk.RecordTypeId = recordTypeId;
		return risk;
	}

	public static User createUser(String lastName,Id profileId, Id roleId)
	{
		User user = new User();
		user.FirstName = 'Tester' + Math.random();
		user.LastName = lastName;
		user.Email = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
		user.Username = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
		user.Alias = 'ttest';
		user.EmailEncodingKey='UTF-8';
		user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.ProfileId = profileId;
        user.UserRoleId = roleId;
        user.TimeZoneSidKey = 'Europe/London';
        user.isActive = true;
		return user;
	}

	public static Product2 createProduct(String name)
	{
		Id stdRecordTypeId = SchemaUtility.getRecordTypeIdByName(Product2.SObjectType, 'Standard');
		Product2 product = new Product2();
		product.Name = name;
		product.IsActive = true;
		product.RecordTypeId = stdRecordTypeId;
		return product;
	}
    
            
    //Insert products
    public static void insertProducts(List<String> ProductNames, Id RecordTypeId, Id ProductMaster, String TaxRate)
    {
        List<Product2> ProductsToInsert = new List<Product2>();
        for(String pn: ProductNames){
            Product2 prod = new Product2();
        	prod.Name = pn;
            prod.RecordTypeId = RecordTypeId;
        	prod.Product_Master__c = ProductMaster;
            prod.Tax_Rate__c = TaxRate;
            prod.Max_Azur_Commission__c = 100;
            ProductsToInsert.add(prod);
        }
        try{
            insert ProductsToInsert;
        }catch(Exception ex){
			LogBuilder.insertLog(ex);
        }
    }

	public static Policy_Master__c createPolicyMaster()
	{
		Policy_Master__c policyMaster = new Policy_Master__c();
		return policyMaster;
	}
	
	//Insert Accounts
	public static void insertAccounts(List<String> accountNames, Id recordTypeId)
	{
		list<Account> accountsToInsert = new list<Account>();
		for(String accountName: accountNames){
			Account account = new Account();
			account.Name = accountName;
			account.RecordTypeId = recordTypeId;
			accountsToInsert.add(account);
		}
		try{
			insert accountsToInsert;
		}catch(Exception ex){
			LogBuilder.insertLog(ex);
		}
	}
	
	//Insert attachments
	public static void insertAttachments(List<String> attachmentNames, Id parentId)
	{
		list<Attachment> attachmentsToInsert = new list<Attachment>();
		for(String attachmentName: attachmentNames){
			Attachment attachment = new Attachment();
			attachment.name = attachmentName;
			attachment.ParentId = parentId;
			attachment.body = blob.valueOf('attachment body');
			attachmentsToInsert.add(attachment);
		}
		try{
			insert attachmentsToInsert;
		}catch(Exception ex){
			LogBuilder.insertLog(ex);
		}
	}
	
	//Insert notes
	public static void insertNotes(List<String> noteTitles, Id parentId)
	{
		list<Note> notesToInsert = new list<Note>();
		for(String noteTitle: noteTitles){
			Note note = new Note();
			note.title = noteTitle;
			note.ParentId = parentId;
			note.body = 'note body';
			notesToInsert.add(note);
		}
		try{
			insert notesToInsert;
		}catch(Exception ex){
			LogBuilder.insertLog(ex);
		}
	}
	
	//Insert google docs
	public static void insertGoogleDocs(List<String> googleDocNames, Id parentId)
	{
		list<GoogleDoc> googleToInsert = new list<GoogleDoc>();
		for(String googleDocName: googleDocNames){
			GoogleDoc googleDoc = new GoogleDoc();
			googleDoc.name = googleDocName;
			googleDoc.ParentId = parentId;
			googleDoc.url = 'https://docs.google.com/document/test';
			googleToInsert.add(googleDoc);
		}
		try{
			insert googleToInsert;
		}catch(Exception ex){
			LogBuilder.insertLog(ex);
		}
	}
	
	//Insert content documents
	public static void insertContentDocuments(List<String> contentDocumentTitles, Id parentId)
	{
		list<ContentVersion> contentVersionsToInsert = new list<ContentVersion>();
		for(String contentDocumentTitle: contentDocumentTitles){
			ContentVersion contentVersion = new ContentVersion();
			contentVersion.title = contentDocumentTitle;
			contentVersion.PathOnClient =  contentDocumentTitle + '.jpg';
			contentVersion.VersionData = Blob.valueOf('This is a test blob');
			contentVersionsToInsert.add(contentVersion);
		}
		try{
			insert contentVersionsToInsert;
		}catch(Exception ex){
			LogBuilder.insertLog(ex);
		}
	}
    
    //Insert content documents
	public static void insertProfilePermissionCustomSettings(List<Id> profileIds, Boolean canDeleteDocuments)
	{
		list<EW_Profile_Permissions__c> profilePermissionCustomSettingToInsert = new list<EW_Profile_Permissions__c>();
		for(Id profileId: profileIds){
			EW_Profile_Permissions__c profilePermissionCustomSetting = new EW_Profile_Permissions__c();
			profilePermissionCustomSetting.SetupOwnerId  = profileId;
            profilePermissionCustomSetting.Name  ='EW Profile Permissions (Profile)';
            profilePermissionCustomSetting.CanDeleteDocuments__c = canDeleteDocuments;
			profilePermissionCustomSettingToInsert.add(profilePermissionCustomSetting);
		}
		try{
			insert profilePermissionCustomSettingToInsert;
		}catch(Exception ex){
			LogBuilder.insertLog(ex);
		}
	}

	public static List<Carrier_Policy_Range__c> createCarrierRangeSettings()
	{
		List<Carrier_Policy_Range__c> carrierRangeSettings = new List<Carrier_Policy_Range__c>();
        Carrier_Policy_Range__c pwcRange = new Carrier_Policy_Range__c();
        Carrier_Policy_Range__c hnwMotorRange = new Carrier_Policy_Range__c();

        pwcRange.Name = 'Emerging_wealth_household';
        pwcRange.Minimum_Number__c = '1501005171';
        pwcRange.Maximum_Number__c = '1501007170';
        pwcRange.Product_Name__c = 'PWC Scheme';

        hnwMotorRange.Name = 'High_net_worth_motor';
        hnwMotorRange.Minimum_Number__c = '1504100001';
        hnwMotorRange.Maximum_Number__c = '1504110000';
        hnwMotorRange.Product_Name__c = 'Motor';

        carrierRangeSettings.add(pwcRange);
        carrierRangeSettings.add(hnwMotorRange);
		return carrierRangeSettings;
	}

	public static String getRandomDRCode()
	{
		String drCode;
		Double randomDouble = Math.random();
		drCode = 'DR' + String.valueOf(randomDouble.intValue()) + Datetime.now().getTime();
		return drCode;
	}
}