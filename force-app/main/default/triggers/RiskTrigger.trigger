/**
* @name: RiskTrigger
* @description: Tirgger for custom Risk object
* @author: Antigoni Tsouri atsouri@azuruw.com
* @lastChangedBy: Antigoni Tsouri 24/04/2017
* 
* 
*/
trigger RiskTrigger on Risk__c (after insert, after update, after delete)
{
	if(Trigger.isBefore)
	{
		if(Trigger.isUpdate)
		{
	//		RiskTriggerHandler.handleRecords(Trigger.new, Trigger.oldMap, 'update');
		}
	}
	if(Trigger.isAfter)
	{
		if(Trigger.isInsert)
		{
			RiskTriggerHandler.handleRecords(Trigger.new, Trigger.oldMap, 'insert');
		}
		if(Trigger.isUpdate)
		{
			RiskTriggerHandler.handleRecords(Trigger.new, Trigger.oldMap, 'update');
		}
		if(Trigger.isDelete)
		{
			RiskTriggerHandler.handleRecords(Trigger.old, Trigger.oldMap, 'delete');
		}
	}
}