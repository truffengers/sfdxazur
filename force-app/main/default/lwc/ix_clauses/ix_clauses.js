import { LightningElement, api, track } from 'lwc';

export default class Ix_Clauses extends LightningElement {
    @api itemData;
    @track displayedClauses = [];
    @track clauseData = [
        {
            name: 'Flat Roof Inspection condition',
            appliesTo: 'This condition applies to the following section(s)',
            sectionsApplied: [
                'PART IV - Buildings, contents, pedal cycles and collections'
            ],
            clauseBody: 'All flat roofs at the home must be inspected and maintained as recommended by suitably qualified persons at least every 10 years. Proof of inspection must be provided to us prior to our obligation to provide settlement of a related covered loss. In the event any flat roof is not inspected or any proof of inspection cannot be provided any loss arising directly or indirectly as a consequence or a result of that flat roof\'s lack of maintenance shall be excluded.'
        },
        {
            name: 'Valuations for Collections',
            appliesTo: 'This clause applies to the following section(s)',
            sectionsApplied: [
                'PART IV - Buildings, contents, pedal cycles and collections',
                'Jewellery and Watches - for individual items over £15,000',
                'Art and Collectables - for individual items over £15,000'
            ],
            clauseBody: 'Valuations for the above must be provided to us within 30 days of inception. If valuations for every item over the limits above are not received by 30 days of inception, then in the event of loss or damage we will not pay any claim for the item(s) over these limits unless you provide us with an independent valuation from a qualified valuer. Such documents must be dated no more than 5 years prior to the date of loss or damage and cannot be dated after the date of loss or damage.'
        },
        {
            name: 'Jewellery and Watches - Personal Custody Clause',
            appliesTo: 'This clause applies to the following section(s)',
            sectionsApplied: [
                'PART IV - Buildings, contents, pedal cycles and collections'
            ],
            clauseBody: 'We do not cover loss of or damage to individual items of Jewellery and Watches over £30,000 unless such items are:\n\na) being worn by you at the time of such loss or damage;\n\nb) being carried by hand by you, or under your personal supervision in a room that you are occupying at the time of such loss or damage;\n\nc) deposited in a bank or locked safe approved by us, or while you are staying at a hotel or motel, when such items are kept in the principal safe of the hotel or motel, not the room safe at the time of such loss or damage;\n\nd) contained in any luggage, baggage or bag which is being carried by you, is in your personal custody and underyour personal supervision at the time of such loss or damage."'
        },
        {
            name: 'Flood Exclusion',
            appliesTo: 'This exclusion applies to the following section(s)',
            sectionsApplied: [
                'PART IV – Buildings, contents, pedal cycles and collections'
            ],
            clauseBody: 'We do not cover any loss or damage caused by flood including any cost of alternative accommodation arising from flood.\n\n“Flood” means water from any source external to a building, which enters a building:\n\na) at or below ground level; or above ground level, provided part of the body of such water is at ground level; and\nb) does so with a volume, weight or force which is substantial and abnormal.\n\nFor the avoidance of doubt the following do not constitute a Flood:\n\nc) the gradual seepage or percolation of water into a building (such as rising damp); and\nd) water escaping from a main, drain, sewer, pipe or other thing inside a building, unless such escape was solely the consequence of a flood falling in the above definition.'
        }, {
            name: 'Subsidence Exclusion',
            appliesTo: 'This exclusion applies to the following section(s)',
            sectionsApplied: [
                'PART IV – Buildings, contents, pedal cycles and collections'
            ],
            clauseBody: 'We will not pay for:\n\n- Any loss or damage caused by or resulting from subsidence, heave or landslip.\n- Any cost of alternative accommodation arising from loss or damage caused by or resulting from subsidence, heave or landslip.'
        }, {
            name: 'Free format clause',
            appliesTo: 'This clause applies to the following section(s)',
            sectionsApplied: [
                'PART IV – Buildings, contents, pedal cycles and collections'
            ],
            clauseBody: 'Please see the policy PDF for the full content of this clause.'
        }, {
            name: 'Interested Parties',
            appliesTo: 'This clause applies to the following section(s)',
            sectionsApplied: [
                'PART IV – Buildings, contents, pedal cycles and collections'
            ],
            clauseBody: 'Please see the policy PDF for the full content of this clause.'
        }
    ]

    connectedCallback() {
        this.displayedClauses = this.clauseData.filter(item => {
            return this.itemData._value.includes(item.name);
        });
    }

    handleAccordionClick(event) {
        const clauseToToggle = this.displayedClauses.find(clause => {
            return clause.name === event.target.dataset.id;
        })
        clauseToToggle.accordionOpen = !clauseToToggle.accordionOpen;
    }
}