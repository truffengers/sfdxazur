/*
Name:            AccountShareTrigger
Description:     This Class is called by AccountShare Trigger
Created By:      Rajni Bajpai 
Created On:      02 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          02May21            EWH-1983 Share Account ID
************************************************************************************************
*/

public with sharing class AccountShareTriggerHandler implements ITriggerHandler {
    
    public static Boolean IsDisabled;
    
    public Boolean IsDisabled(){
        return AccountShareTriggerHandler.IsDisabled != null ? AccountShareTriggerHandler.IsDisabled : false;
    }
    
    public void BeforeInsert(List<SObject> newItems) {}
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){}
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {
        if (IsDisabled() != true) {
            Database.executeBatch(new EWAccountShrRecInsertBatch(newItems.keyset()),2000);
            
        }
    }
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void AfterDelete(Map<Id, SObject> oldItems) {
        if (IsDisabled() != true) {
            Database.executeBatch(new EWAccountShrRecDeleteBatch(oldItems.keyset()),2000);
        }  
        
    }
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
}