/*
Name:            AccountShareTrigger
Description:     This trigger will share the Account records
Created By:      Rajni Bajpai 
Created On:      28 Apr 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          28Apr21            EWH-1983 Share Account ID
************************************************************************************************
*/

trigger AccountShareTrigger on EW_AccountSharing__c (after insert, after delete) {
    
     TriggerDispatcher.Run(new AccountShareTriggerHandler());

}