/**
* @name: RiskTriggerHandlerTest
* @description: Unit test for various risk scenarios
*/
@isTest
private class RiskTriggerHandlerTest
{

	//Constants
	private static final String USER_LAST_NAME = 'Testing last name user';
	private static final String PROFILE_AZUR_UNDERWRITER = EWConstants.PROFILE_AZURUNDERWRITER_NAME;


	@testSetup
	public static void createTestData()
	{
		Id profileId = [Select Id From Profile  Where Name =: PROFILE_AZUR_UNDERWRITER].Id;
		Id permissionSetId = [Select Id From PermissionSet where Name = 'Risk_System_User'].Id;
		User user = TestDataFactory.createUser(USER_LAST_NAME, profileId, null);
		insert user;

		PermissionSetAssignment perSetAssignm = new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = permissionSetId);
		insert perSetAssignm;

	}


	@isTest static void shouldCalculateAvgGrossRiskForFourLevelHierarchyOnInsert() {

		User user = [
				Select Id
				From User
				Where Profile.Name = :PROFILE_AZUR_UNDERWRITER
				And LastName = :USER_LAST_NAME
				And isActive = true
				Limit 1
		];

		System.runAs(user) {

			Map<String, Schema.RecordTypeInfo> rtInfos = Schema.SObjectType.Risk__c.getRecordTypeInfosByDeveloperName();

			Test.startTest();

			Risk__c parent1 = TestDataFactory.createRisk(null, rtInfos.get('Parent_Risk').recordTypeId);
			parent1.Gross_Risk_Impact__c = null;
			parent1.Gross_Risk_Likelihood__c = null;
			parent1.Net_Risk_Impact__c = null;
			parent1.Net_Risk_Likelihood__c = null;

			system.debug('=> inserting parent1 ');
			insert parent1;

			Risk__c midParent11 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent11.Gross_Risk_Impact__c = 2;
			midParent11.Gross_Risk_Likelihood__c = 2;
			midParent11.Net_Risk_Impact__c = 2;
			midParent11.Net_Risk_Likelihood__c = 2;
			system.debug('=> inserting midParent11 ');
			insert midParent11;

			Risk__c midParent12 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent12.Gross_Risk_Impact__c = null;
			midParent12.Gross_Risk_Likelihood__c = null;
			midParent12.Net_Risk_Impact__c = null;
			midParent12.Net_Risk_Likelihood__c = null;
			system.debug('=> inserting midParent12 ');
			insert midParent12;

			Risk__c child121 = TestDataFactory.createRisk(midParent12.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			child121.Gross_Risk_Impact__c = 3;
			child121.Gross_Risk_Likelihood__c = 3;
			child121.Net_Risk_Impact__c = 3;
			child121.Net_Risk_Likelihood__c = 3;
			system.debug('=> inserting child121 ');
			insert child121;

			Risk__c child122 = TestDataFactory.createRisk(midParent12.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			child122.Gross_Risk_Impact__c = 4;
			child122.Gross_Risk_Likelihood__c = 4;
			child122.Net_Risk_Impact__c = 4;
			child122.Net_Risk_Likelihood__c = 4;
			system.debug('=> inserting child122 ');
			insert child122;

			Risk__c midParent13 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent13.Gross_Risk_Impact__c = 2;
			midParent13.Gross_Risk_Likelihood__c = 2;
			midParent13.Net_Risk_Impact__c = 2;
			midParent13.Net_Risk_Likelihood__c = 2;
			system.debug('=> inserting midParent13 ');
			insert midParent13;

			Risk__c midParent14 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent14.Gross_Risk_Impact__c = null;
			midParent14.Gross_Risk_Likelihood__c = null;
			midParent14.Net_Risk_Impact__c = null;
			midParent14.Net_Risk_Likelihood__c = null;
			system.debug('=> inserting midParent14 ');
			insert midParent14;

			Risk__c midParent141 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent141.Gross_Risk_Impact__c = 5;
			midParent141.Gross_Risk_Likelihood__c = 5;
			midParent141.Net_Risk_Impact__c = 5;
			midParent141.Net_Risk_Likelihood__c = 5;
			system.debug('=> inserting midParent141 ');
			insert midParent141;

			Risk__c midParent142 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent142.Gross_Risk_Impact__c = 4;
			midParent142.Gross_Risk_Likelihood__c = 4;
			midParent142.Net_Risk_Impact__c = 4;
			midParent142.Net_Risk_Likelihood__c = 4;
			system.debug('=> inserting midParent142 ');
			insert midParent142;

			Risk__c midParent143 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent143.Gross_Risk_Impact__c = null;
			midParent143.Gross_Risk_Likelihood__c = null;
			midParent143.Net_Risk_Impact__c = null;
			midParent143.Net_Risk_Likelihood__c = null;
			system.debug('=> inserting midParent143 ');
			insert midParent143;

			Risk__c child1431 = TestDataFactory.createRisk(midParent143.Id, rtInfos.get('Child_Risk').recordTypeId);
			child1431.Gross_Risk_Impact__c = 1.5;
			child1431.Gross_Risk_Likelihood__c = 1.5;
			child1431.Net_Risk_Impact__c = 1.5;
			child1431.Net_Risk_Likelihood__c = 1.5;
			system.debug('=> inserting child1431 ');
			insert child1431;

			Risk__c child1432 = TestDataFactory.createRisk(midParent143.Id, rtInfos.get('Child_Risk').recordTypeId);
			child1432.Gross_Risk_Impact__c = 3.5;
			child1432.Gross_Risk_Likelihood__c = 3.5;
			child1432.Net_Risk_Impact__c = 3.5;
			child1432.Net_Risk_Likelihood__c = 3.5;
			system.debug('=> inserting child1432 ');
			insert child1432;

			Test.stopTest();



			Risk__c midParent143After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :midParent143.Id
					Limit 1
			];

			System.assertEquals(2.5, midParent143After.Average_Gross_Risk_Impact__c);
			System.assertEquals(2.5, midParent143After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(2.5, midParent143After.Average_Net_Risk_Impact__c);
			System.assertEquals(2.5, midParent143After.Average_Net_Risk_Likelihood__c);


			Risk__c midParent14After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :midParent14.Id
					Limit 1
			];

			System.assertEquals(3.8333333333333335, midParent14After.Average_Gross_Risk_Impact__c);
			System.assertEquals(3.8333333333333335, midParent14After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(3.8333333333333335, midParent14After.Average_Net_Risk_Impact__c);
			System.assertEquals(3.8333333333333335, midParent14After.Average_Net_Risk_Likelihood__c);

			Risk__c midParent12After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :midParent12.Id
					Limit 1
			];

			System.assertEquals(3.5, midParent12After.Average_Gross_Risk_Impact__c);
			System.assertEquals(3.5, midParent12After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(3.5, midParent12After.Average_Net_Risk_Impact__c);
			System.assertEquals(3.5, midParent12After.Average_Net_Risk_Likelihood__c);

			Risk__c parent1After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :parent1.Id
					Limit 1
			];



			System.assertEquals(2.8333333333333335, parent1After.Average_Gross_Risk_Impact__c);
			System.assertEquals(2.8333333333333335, parent1After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(2.8333333333333335, parent1After.Average_Net_Risk_Impact__c);
			System.assertEquals(2.8333333333333335, parent1After.Average_Net_Risk_Likelihood__c);
		}
	}

	@isTest static void shouldCalculateAvgGrossRiskForFourLevelHierarchyOnUpdate() {

		User user = [
				Select Id
				From User
				Where Profile.Name = :PROFILE_AZUR_UNDERWRITER
				And LastName = :USER_LAST_NAME
				And isActive = true
				Limit 1
		];

		System.runAs(user) {

			Map<String, Schema.RecordTypeInfo> rtInfos = Schema.SObjectType.Risk__c.getRecordTypeInfosByDeveloperName();



			Risk__c parent1 = TestDataFactory.createRisk(null, rtInfos.get('Parent_Risk').recordTypeId);
			parent1.Gross_Risk_Impact__c = null;
			parent1.Gross_Risk_Likelihood__c = null;
			parent1.Net_Risk_Impact__c = null;
			parent1.Net_Risk_Likelihood__c = null;

			system.debug('=> inserting parent1 ');
			insert parent1;

			Risk__c midParent11 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent11.Gross_Risk_Impact__c = 2;
			midParent11.Gross_Risk_Likelihood__c = 2;
			midParent11.Net_Risk_Impact__c = 2;
			midParent11.Net_Risk_Likelihood__c = 2;
			system.debug('=> inserting midParent11 ');
			insert midParent11;

			Risk__c midParent12 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent12.Gross_Risk_Impact__c = null;
			midParent12.Gross_Risk_Likelihood__c = null;
			midParent12.Net_Risk_Impact__c = null;
			midParent12.Net_Risk_Likelihood__c = null;
			system.debug('=> inserting midParent12 ');
			insert midParent12;

			Risk__c child121 = TestDataFactory.createRisk(midParent12.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			child121.Gross_Risk_Impact__c = 3;
			child121.Gross_Risk_Likelihood__c = 3;
			child121.Net_Risk_Impact__c = 3;
			child121.Net_Risk_Likelihood__c = 3;
			system.debug('=> inserting child121 ');
			insert child121;

			Risk__c child122 = TestDataFactory.createRisk(midParent12.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			child122.Gross_Risk_Impact__c = 4;
			child122.Gross_Risk_Likelihood__c = 4;
			child122.Net_Risk_Impact__c = 4;
			child122.Net_Risk_Likelihood__c = 4;
			system.debug('=> inserting child122 ');
			insert child122;

			Risk__c midParent13 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent13.Gross_Risk_Impact__c = 2;
			midParent13.Gross_Risk_Likelihood__c = 2;
			midParent13.Net_Risk_Impact__c = 2;
			midParent13.Net_Risk_Likelihood__c = 2;
			system.debug('=> inserting midParent13 ');
			insert midParent13;

			Risk__c midParent14 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent14.Gross_Risk_Impact__c = null;
			midParent14.Gross_Risk_Likelihood__c = null;
			midParent14.Net_Risk_Impact__c = null;
			midParent14.Net_Risk_Likelihood__c = null;
			system.debug('=> inserting midParent14 ');
			insert midParent14;

			Risk__c midParent141 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent141.Gross_Risk_Impact__c = 5;
			midParent141.Gross_Risk_Likelihood__c = 5;
			midParent141.Net_Risk_Impact__c = 5;
			midParent141.Net_Risk_Likelihood__c = 5;
			system.debug('=> inserting midParent141 ');
			insert midParent141;

			Risk__c midParent142 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent142.Gross_Risk_Impact__c = 4;
			midParent142.Gross_Risk_Likelihood__c = 4;
			midParent142.Net_Risk_Impact__c = 4;
			midParent142.Net_Risk_Likelihood__c = 4;
			system.debug('=> inserting midParent142 ');
			insert midParent142;

			Risk__c midParent143 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
			midParent143.Gross_Risk_Impact__c = null;
			midParent143.Gross_Risk_Likelihood__c = null;
			midParent143.Net_Risk_Impact__c = null;
			midParent143.Net_Risk_Likelihood__c = null;
			system.debug('=> inserting midParent143 ');
			insert midParent143;

			Risk__c child1431 = TestDataFactory.createRisk(midParent143.Id, rtInfos.get('Child_Risk').recordTypeId);
			child1431.Gross_Risk_Impact__c = 1.5;
			child1431.Gross_Risk_Likelihood__c = 1.5;
			child1431.Net_Risk_Impact__c = 1.5;
			child1431.Net_Risk_Likelihood__c = 1.5;
			system.debug('=> inserting child1431 ');
			insert child1431;

			Risk__c child1432 = TestDataFactory.createRisk(midParent143.Id, rtInfos.get('Child_Risk').recordTypeId);
			child1432.Gross_Risk_Impact__c = 3.5;
			child1432.Gross_Risk_Likelihood__c = 3.5;
			child1432.Net_Risk_Impact__c = 3.5;
			child1432.Net_Risk_Likelihood__c = 3.5;
			system.debug('=> inserting child1432 ');
			insert child1432;


            Test.startTest();
            child1431.Gross_Risk_Impact__c = 2;
            child1431.Gross_Risk_Likelihood__c = 2;
            child1431.Net_Risk_Impact__c = 2;
            child1431.Net_Risk_Likelihood__c = 2;

            midParent141.Gross_Risk_Impact__c = 1;
            midParent141.Gross_Risk_Likelihood__c = 1;
            midParent141.Net_Risk_Impact__c = 1;
            midParent141.Net_Risk_Likelihood__c = 1;
            system.debug('=> update midParent141 and child1431');

            List<Risk__c> risksToUpdate = new List<Risk__c>{midParent141, child1431};
            update risksToUpdate;

			Test.stopTest();



			Risk__c midParent143After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :midParent143.Id
					Limit 1
			];

			System.assertEquals(2.75, midParent143After.Average_Gross_Risk_Impact__c);
			System.assertEquals(2.75, midParent143After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(2.75, midParent143After.Average_Net_Risk_Impact__c);
			System.assertEquals(2.75, midParent143After.Average_Net_Risk_Likelihood__c);


			Risk__c midParent14After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :midParent14.Id
					Limit 1
			];

			System.assertEquals(2.5833333333333335, midParent14After.Average_Gross_Risk_Impact__c);
			System.assertEquals(2.5833333333333335, midParent14After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(2.5833333333333335, midParent14After.Average_Net_Risk_Impact__c);
			System.assertEquals(2.5833333333333335, midParent14After.Average_Net_Risk_Likelihood__c);

			Risk__c midParent12After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :midParent12.Id
					Limit 1
			];

			System.assertEquals(3.5, midParent12After.Average_Gross_Risk_Impact__c);
			System.assertEquals(3.5, midParent12After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(3.5, midParent12After.Average_Net_Risk_Impact__c);
			System.assertEquals(3.5, midParent12After.Average_Net_Risk_Likelihood__c);

			Risk__c parent1After = [
					Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
							Average_Net_Risk_Likelihood__c
					From Risk__c
					Where Id = :parent1.Id
					Limit 1
			];



			System.assertEquals(2.5208333333333335, parent1After.Average_Gross_Risk_Impact__c);
			System.assertEquals(2.5208333333333335, parent1After.Average_Gross_Risk_Likelihood__c);
			System.assertEquals(2.5208333333333335, parent1After.Average_Net_Risk_Impact__c);
			System.assertEquals(2.5208333333333335, parent1After.Average_Net_Risk_Likelihood__c);





		}
	}

    @isTest static void shouldCalculateAvgGrossRiskForFourLevelHierarchyOnDelete() {

        User user = [
                Select Id
                From User
                Where Profile.Name = :PROFILE_AZUR_UNDERWRITER
                And LastName = :USER_LAST_NAME
                And isActive = true
                Limit 1
        ];

        System.runAs(user) {

            Map<String, Schema.RecordTypeInfo> rtInfos = Schema.SObjectType.Risk__c.getRecordTypeInfosByDeveloperName();



            Risk__c parent1 = TestDataFactory.createRisk(null, rtInfos.get('Parent_Risk').recordTypeId);
            parent1.Gross_Risk_Impact__c = null;
            parent1.Gross_Risk_Likelihood__c = null;
            parent1.Net_Risk_Impact__c = null;
            parent1.Net_Risk_Likelihood__c = null;

            system.debug('=> inserting parent1 ');
            insert parent1;

            Risk__c midParent11 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent11.Gross_Risk_Impact__c = 2;
            midParent11.Gross_Risk_Likelihood__c = 2;
            midParent11.Net_Risk_Impact__c = 2;
            midParent11.Net_Risk_Likelihood__c = 2;
            system.debug('=> inserting midParent11 ');
            insert midParent11;

            Risk__c midParent12 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent12.Gross_Risk_Impact__c = null;
            midParent12.Gross_Risk_Likelihood__c = null;
            midParent12.Net_Risk_Impact__c = null;
            midParent12.Net_Risk_Likelihood__c = null;
            system.debug('=> inserting midParent12 ');
            insert midParent12;

            Risk__c child121 = TestDataFactory.createRisk(midParent12.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            child121.Gross_Risk_Impact__c = 3;
            child121.Gross_Risk_Likelihood__c = 3;
            child121.Net_Risk_Impact__c = 3;
            child121.Net_Risk_Likelihood__c = 3;
            system.debug('=> inserting child121 ');
            insert child121;

            Risk__c child122 = TestDataFactory.createRisk(midParent12.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            child122.Gross_Risk_Impact__c = 4;
            child122.Gross_Risk_Likelihood__c = 4;
            child122.Net_Risk_Impact__c = 4;
            child122.Net_Risk_Likelihood__c = 4;
            system.debug('=> inserting child122 ');
            insert child122;

            Risk__c midParent13 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent13.Gross_Risk_Impact__c = 2;
            midParent13.Gross_Risk_Likelihood__c = 2;
            midParent13.Net_Risk_Impact__c = 2;
            midParent13.Net_Risk_Likelihood__c = 2;
            system.debug('=> inserting midParent13 ');
            insert midParent13;

            Risk__c midParent14 = TestDataFactory.createRisk(parent1.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent14.Gross_Risk_Impact__c = null;
            midParent14.Gross_Risk_Likelihood__c = null;
            midParent14.Net_Risk_Impact__c = null;
            midParent14.Net_Risk_Likelihood__c = null;
            system.debug('=> inserting midParent14 ');
            insert midParent14;

            Risk__c midParent141 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent141.Gross_Risk_Impact__c = 5;
            midParent141.Gross_Risk_Likelihood__c = 5;
            midParent141.Net_Risk_Impact__c = 5;
            midParent141.Net_Risk_Likelihood__c = 5;
            system.debug('=> inserting midParent141 ');
            insert midParent141;

            Risk__c midParent142 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent142.Gross_Risk_Impact__c = 4;
            midParent142.Gross_Risk_Likelihood__c = 4;
            midParent142.Net_Risk_Impact__c = 4;
            midParent142.Net_Risk_Likelihood__c = 4;
            system.debug('=> inserting midParent142 ');
            insert midParent142;

            Risk__c midParent143 = TestDataFactory.createRisk(midParent14.Id, rtInfos.get('Mid_Parent_Risk').recordTypeId);
            midParent143.Gross_Risk_Impact__c = null;
            midParent143.Gross_Risk_Likelihood__c = null;
            midParent143.Net_Risk_Impact__c = null;
            midParent143.Net_Risk_Likelihood__c = null;
            system.debug('=> inserting midParent143 ');
            insert midParent143;

            Risk__c child1431 = TestDataFactory.createRisk(midParent143.Id, rtInfos.get('Child_Risk').recordTypeId);
            child1431.Gross_Risk_Impact__c = 1.5;
            child1431.Gross_Risk_Likelihood__c = 1.5;
            child1431.Net_Risk_Impact__c = 1.5;
            child1431.Net_Risk_Likelihood__c = 1.5;
            system.debug('=> inserting child1431 ');
            insert child1431;

            Risk__c child1432 = TestDataFactory.createRisk(midParent143.Id, rtInfos.get('Child_Risk').recordTypeId);
            child1432.Gross_Risk_Impact__c = 3.5;
            child1432.Gross_Risk_Likelihood__c = 3.5;
            child1432.Net_Risk_Impact__c = 3.5;
            child1432.Net_Risk_Likelihood__c = 3.5;
            system.debug('=> inserting child1432 ');
            insert child1432;

            Test.startTest();

            delete new List<Risk__c>{child1432, midParent141, child121};

            Test.stopTest();



            Risk__c midParent143After = [
                    Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
                            Average_Net_Risk_Likelihood__c
                    From Risk__c
                    Where Id = :midParent143.Id
                    Limit 1
            ];

            System.assertEquals(1.5, midParent143After.Average_Gross_Risk_Impact__c);
            System.assertEquals(1.5, midParent143After.Average_Gross_Risk_Likelihood__c);
            System.assertEquals(1.5, midParent143After.Average_Net_Risk_Impact__c);
            System.assertEquals(1.5, midParent143After.Average_Net_Risk_Likelihood__c);


            Risk__c midParent14After = [
                    Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
                            Average_Net_Risk_Likelihood__c
                    From Risk__c
                    Where Id = :midParent14.Id
                    Limit 1
            ];

            System.assertEquals(2.75, midParent14After.Average_Gross_Risk_Impact__c);
            System.assertEquals(2.75, midParent14After.Average_Gross_Risk_Likelihood__c);
            System.assertEquals(2.75, midParent14After.Average_Net_Risk_Impact__c);
            System.assertEquals(2.75, midParent14After.Average_Net_Risk_Likelihood__c);

            Risk__c midParent12After = [
                    Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
                            Average_Net_Risk_Likelihood__c
                    From Risk__c
                    Where Id = :midParent12.Id
                    Limit 1
            ];

            System.assertEquals(4, midParent12After.Average_Gross_Risk_Impact__c);
            System.assertEquals(4, midParent12After.Average_Gross_Risk_Likelihood__c);
            System.assertEquals(4, midParent12After.Average_Net_Risk_Impact__c);
            System.assertEquals(4, midParent12After.Average_Net_Risk_Likelihood__c);

            Risk__c parent1After = [
                    Select Id, Average_Gross_Risk_Impact__c, Average_Gross_Risk_Likelihood__c, Average_Net_Risk_Impact__c,
                            Average_Net_Risk_Likelihood__c
                    From Risk__c
                    Where Id = :parent1.Id
                    Limit 1
            ];



            System.assertEquals(2.6875, parent1After.Average_Gross_Risk_Impact__c);
            System.assertEquals(2.6875, parent1After.Average_Gross_Risk_Likelihood__c);
            System.assertEquals(2.6875, parent1After.Average_Net_Risk_Impact__c);
            System.assertEquals(2.6875, parent1After.Average_Net_Risk_Likelihood__c);
        }
    }
}