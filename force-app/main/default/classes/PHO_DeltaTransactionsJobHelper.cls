/**
* @name: PHO_DeltaTransactionsJobHelper
* @description: Delegate class to handle the detail logic for Delta Transactions Batch job.
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 30/03/2020
* @lastChangedBy: 12/08/2020    Antigoni D'Mello    PR-94 Carrier Line zero bug
*/
public with sharing class PHO_DeltaTransactionsJobHelper {

    private static Map<Id, IBA_PolicyTransaction__c> deltaTransactionsMap;
    private static Map<Id, IBA_PolicyTransaction__c> newToOldDeltaPTMap;
    private static Map<Id, IBA_FFPolicyCarrierLine__c> newCarrierLinesMap;
    private static List<IBA_PolicyTransaction__c> newTransactions;
    private static List<IBA_PolicyTransactionLine__c> newPolicyTransactionLines;

    public static void handleDeltaTransactions(List<IBA_PolicyTransaction__c> deltaTransactions) {
        List<c2g__codaDimension1__c> newDimensions = new List<c2g__codaDimension1__c>();
        newPolicyTransactionLines = new List<IBA_PolicyTransactionLine__c>();
        newToOldDeltaPTMap = new Map<Id, IBA_PolicyTransaction__c>();
        newTransactions = new List<IBA_PolicyTransaction__c> ();

        try {
            newTransactions = createNewTransactions(deltaTransactions);
            insert newTransactions;
            for (IBA_PolicyTransaction__c newPT: newTransactions) {
                newToOldDeltaPTMap.put(newPT.Id, deltaTransactionsMap.get(newPT.PHO_RelatedDeltaPolicyTransaction__c));
            }
            createNewCarrierLines(newTransactions);
            insert newCarrierLinesMap.values();
            newPolicyTransactionLines = createNewTransactionLines();
            insert newPolicyTransactionLines;
            newDimensions = createNewDimensions();
            insert newDimensions;
            update linkTransactionLinesToDimensions(newPolicyTransactionLines);
            update deltaTransactions;
        }
        catch(System.Exception exc) {
            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c deltAzurLog = logBuilder.createLogWithCodeInfo(exc, 'Phoenix Rising', 'PHO_DeltaTransactionsJob', 'handleDeltaTransactions');
            insert deltAzurLog;
        }
    }

    public static List<IBA_PolicyTransaction__c> createNewTransactions(List<IBA_PolicyTransaction__c> deltaPolicyTransactions) {
        List<IBA_PolicyTransaction__c> newPolicyTransactions = new List<IBA_PolicyTransaction__c>();
        deltaTransactionsMap = new Map<Id, IBA_PolicyTransaction__c>();
        for(IBA_PolicyTransaction__c deltaPT: deltaPolicyTransactions) {
            IBA_PolicyTransaction__c newTransaction = deltaPT.clone();
            newTransaction.PHO_DeltaTransaction__c = true;
            newTransaction.PHO_RelatedDeltaPolicyTransaction__c = deltaPT.Id;
            newTransaction.IBA_PeriodInput__c = '2020/004';
            newPolicyTransactions.add(newTransaction);
            deltaTransactionsMap.put(deltaPT.Id, deltaPT);
            deltaPT.PHO_DeltaProcessed__c = true;
        }
        return newPolicyTransactions;
    }

    public static List<IBA_FFPolicyCarrierLine__c> createNewCarrierLines(List<IBA_PolicyTransaction__c> newTransactions) {
        List<IBA_FFPolicyCarrierLine__c> newPolicyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();
        newCarrierLinesMap = new Map<Id, IBA_FFPolicyCarrierLine__c>();
        for(IBA_PolicyTransaction__c newPT: newTransactions){
            // Loop through the initial Delta Policy Carrier Lines
            for(IBA_FFPolicyCarrierLine__c oldPCL: newToOldDeltaPTMap.get(newPT.Id).FF_Policy_Carrier_Lines__r) {
                if(oldPCL.IBA_OwedtoCarrier__c != 0) {
                    IBA_FFPolicyCarrierLine__c newPCL = oldPCL.clone();
                    newPCL.IBA_PolicyTransaction__c = newPT.Id;
                    newPolicyCarrierLines.add(newPCL);
                    // Build a map to link the new Policy Carrier Lines with the Old Policy Carrier Line Ids
                    newCarrierLinesMap.put(oldPCL.Id, newPCL);
                }
            }
        }
        return newPolicyCarrierLines;
    }

    public static List<IBA_PolicyTransactionLine__c> createNewTransactionLines() {
        List<IBA_PolicyTransactionLine__c> newPolicyTransactionLines = new List<IBA_PolicyTransactionLine__c>();
        for(IBA_PolicyTransaction__c oldPT: deltaTransactionsMap.values()) {
            for (IBA_PolicyTransactionLine__c oldPTL: oldPT.Policy_Transaction_Lines__r) {
                if(deltaDataOnPolicyTransactionLine(oldPTL)) {
                    IBA_PolicyTransactionLine__c newPTL = oldPTL.clone();
                    if(newCarrierLinesMap.get(oldPTL.IBA_PolicyCarrierLine__c) != null) {
                        newPTL.IBA_PolicyCarrierLine__c = newCarrierLinesMap.get(oldPTL.IBA_PolicyCarrierLine__c).Id;
                        newPTL.IBA_PolicyTransaction__c = newCarrierLinesMap.get(oldPTL.IBA_PolicyCarrierLine__c).IBA_PolicyTransaction__c;
                    }
                    else{
                        newPTL.IBA_PolicyTransaction__c = oldPTL.IBA_PolicyTransaction__c;
                    }
                    newPTL.IBA_GrossWrittenPremium__c = oldPTL.PHO_2019DeltaGrossWritten__c;
                    newPTL.IBA_InsurancePremiumTax__c = oldPTL.PHO_2019DeltaInsurancePremium__c;
                    newPTL.IBA_ForeignTax__c = oldPTL.PHO_2019DeltaForeignTax__c;
                    newPTL.IBA_BrokerCommission__c = oldPTL.PHO_2019DeltaBrokerCommission__c;
                    newPTL.IBA_CarrierFee__c = oldPTL.PHO_2019DeltaCarrierFee__c;
                    newPTL.IBA_MGACommission__c = oldPTL.PHO_2019DeltaMGA__c;
                    newPTL.IBA_NetDuefromBroker__c = calculateNetDueFromBroker(oldPT, oldPTL);
                    newPTL.IBA_DuefromAccountingBroker__c = calculateDueFromAccountingBroker(oldPT, oldPTL);
                    newPTL.IBA_NetPayabletoCarrier__c = oldPTL.PHO_2019DeltaCarrierFee__c + oldPTL.PHO_2019DeltaInsurancePremium__c + oldPTL.PHO_2019DeltaForeignTax__c;
                    newPTL.IBA_DuetoMGA__c = (newPTL.IBA_NetDuefromBroker__c + newPTL.IBA_DuefromAccountingBroker__c) - newPTL.IBA_NetPayabletoCarrier__c;
                    newPolicyTransactionLines.add(newPTL);
                }
            }
        }

        return newPolicyTransactionLines;
    }

    public static List<c2g__codaDimension1__c> createNewDimensions() {
        Set<Id> ptlIds = new Set<Id>();
        List<c2g__codaDimension1__c> newDimensions = new List<c2g__codaDimension1__c>();

        for(IBA_PolicyTransactionLine__c ptl: newPolicyTransactionLines) {
            ptlIds.add(ptl.Id);
        }
        for(IBA_PolicyTransactionLine__c ptl: [SELECT Id, Name FROM IBA_PolicyTransactionLine__c WHERE Id in: ptlIds]) {
            c2g__codaDimension1__c newDimension = new c2g__codaDimension1__c();
            newDimension.Name = ptl.Name;
            newDimension.c2g__ReportingCode__c = ptl.Name;
            newDimension.IBA_PolicyTransactionLine__c = ptl.Id;
            newDimensions.add(newDimension);
        }
        return newDimensions;
    }

    public static List<IBA_PolicyTransactionLine__c> linkTransactionLinesToDimensions(List<IBA_PolicyTransactionLine__c> transactionLines) {
        Set<Id> ptlIds = new Set<Id>();
        for(IBA_PolicyTransactionLine__c ptl: transactionLines) {
            ptlIds.add(ptl.Id);
        }
        List<IBA_PolicyTransactionLine__c> queriedTransactionLines = [SELECT Id, (SELECT Id FROM Dimension_1__r) FROM IBA_PolicyTransactionLine__c WHERE Id in: ptlIds];
        for(IBA_PolicyTransactionLine__c ptl: queriedTransactionLines) {
            for(c2g__codaDimension1__c dimension1: ptl.Dimension_1__r) {
                ptl.IBA_Dimension1__c = dimension1.Id;
            }
        }
        return queriedTransactionLines;
    }

    private static Boolean deltaDataOnPolicyTransactionLine(IBA_PolicyTransactionLine__c ptl) {
        Boolean hasDeltaData = false;
        if(ptl.PHO_2019DeltaMGA__c != 0 || ptl.PHO_2019DeltaBrokerCommission__c != 0 || ptl.PHO_2019DeltaCarrierFee__c != 0 ||
                ptl.PHO_2019DeltaForeignTax__c != 0 || ptl.PHO_2019DeltaGrossWritten__c != 0 || ptl.PHO_2019DeltaInsurancePremium__c != 0 ||
                ptl.PHO_OngoingDeltaBrokerCommission__c != 0 || ptl.PHO_OngoingDeltaCarrierFee__c != 0 || ptl.PHO_OngoingDeltaMGA__c != 0 ||
                ptl.PHO_OngoingDeltaForeignTax__c != 0 || ptl.PHO_OngoingDeltaGrossWritten__c != 0 || ptl.PHO_OngoingDeltaInsurancePremium__c != 0) {
            hasDeltaData = true;
        }
        return hasDeltaData;
    }

    private static Decimal calculateNetDueFromBroker(IBA_PolicyTransaction__c pt, IBA_PolicyTransactionLine__c ptl) {
        Decimal netDueFromBroker;
        if(String.isEmpty(pt.IBA_AccountingBroker__c)) {
            netDueFromBroker = ptl.PHO_2019DeltaGrossWritten__c + ptl.PHO_2019DeltaInsurancePremium__c + ptl.PHO_2019DeltaForeignTax__c - ptl.PHO_2019DeltaBrokerCommission__c;
        }
        else {
            netDueFromBroker = ptl.PHO_2019DeltaBrokerCommission__c*(-1);
        }
        return netDueFromBroker;
    }

    private static Decimal calculateDueFromAccountingBroker(IBA_PolicyTransaction__c pt, IBA_PolicyTransactionLine__c ptl) {
        Decimal dueFromAccountingBroker;
        if(String.isEmpty(pt.IBA_AccountingBroker__c)) {
            dueFromAccountingBroker = 0;
        }
        else {
            dueFromAccountingBroker = ptl.PHO_2019DeltaGrossWritten__c + ptl.PHO_2019DeltaInsurancePremium__c + ptl.PHO_2019DeltaForeignTax__c;
        }
        return dueFromAccountingBroker;
    }
}