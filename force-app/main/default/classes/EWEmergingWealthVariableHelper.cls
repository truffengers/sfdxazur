/**
* @name: EWEmergingWealthVariableHelper
* @description: Helper class for Dataraptor calls from Vlocity
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 23/10/2018
*/

public with sharing class EWEmergingWealthVariableHelper {


    public static Map<String,Object> getRecords(){

        Map<String,Object> outMap = new Map<String,Object>();
        String query = 'SELECT {0} FROM EW_EmergingWealthVariable__c WHERE {1}';

        List<String> fields = new List<String>{'EW_Key__c','EW_Value__c'};
        String fieldString = String.join(fields,',');

        List<String> criteria = new List<String>{
                'EW_Value__c != null AND',
                'EW_Effective_From__c <= TODAY AND',
                '(EW_Effective_To__c >= TODAY OR EW_Effective_To__c = NULL)'
        };
        String criteriaString = String.join(criteria,' ');

        query = String.format(query,new list<String>{fieldString, criteriaString});
        List<EW_EmergingWealthVariable__c> records = Database.query(query);

        for(EW_EmergingWealthVariable__c record: records){
            String key = record.EW_Key__c;
            outMap.put(key, record.EW_Value__c);
        }

        return outMap;

    }


}