/**
* @name: EWQuoteHelperTest
* @description: This testclass is for the EWQuoteHelper class used for Quote related activities
* @author: Roy Lloyd, rlloyd@azuruw.com
* @date: 13/02/2019
*
*/

@isTest
public with sharing class EWQuoteHelperTest {


    private static Account broker;
    private static User systemAdminUser, brokerUser, underwriterUser;
    private static Product2 product;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static Quote mtaQuote;
    private static PricebookEntry pricebookEntry;
    private static Asset policy;

    private final static String MODE_NEW = 'new';
    private final static String MODE_EDIT = 'edit';

    public static final Set<String> coverageNameSet = new Set<String>{
            'Art and Collectables',
            'Buildings',
            'Contents',
            'Employers Liability',
            'Home Emergency',
            'Jewellery and Watches',
            'Legal Expenses',
            'Pedal Cycles',
            'Personal Cyber',
            'Public Liability',
            'Tenants Improvements',
            'Fixed Expense'
    };

    private static testMethod void testUWadjustment() {

        setup();

        QuoteLineItem preUpdate;
        QuoteLineItem postUpdate;
        Decimal adjustment = 3;

        system.debug('Quote ' + quote);

        Test.startTest();
        System.runAs(systemAdminUser) {

            preUpdate = [
                    select id, EW_AzurCommissionValue__c,
                            EW_BrokerCommissionValue__c,
                            EW_IPTTotal__c,EW_PremiumGrossExIPT__c,
                            EW_PremiumGrossIncIPT__c,
                            EW_PremiumNet__c,
                            vlocity_ins__AttributeSelectedValues__c,
                            Quote.vlocity_ins__StandardPremium__c
                    From QuoteLineItem
                    Where vlocity_ins__ParentItemId__c != null
                    Limit 1
            ];

            quote.EW_UW_Adjustment__c = adjustment;
            quote.EW_UW_Adjustment_Reason__c = 'Commercial Decision';
            update quote;

            postUpdate = [
                    select id, EW_AzurCommissionValue__c,
                            EW_BrokerCommissionValue__c,
                            EW_IPTTotal__c,EW_PremiumGrossExIPT__c,
                            EW_PremiumGrossIncIPT__c,
                            EW_PremiumNet__c,
                            vlocity_ins__AttributeSelectedValues__c,
                            Quote.vlocity_ins__StandardPremium__c
                    From QuoteLineItem
                    Where Id = :preUpdate.Id
                    Limit 1
            ];
        }
        Test.stopTest();

        system.debug('Quote ' + quote);
        system.debug('preUpdate ' + preUpdate);
        system.debug('postUpdate ' + postUpdate);

        system.assertNotEquals(preUpdate.vlocity_ins__AttributeSelectedValues__c, postUpdate.vlocity_ins__AttributeSelectedValues__c);

        System.assertEquals(getAdjustedValue(preUpdate.EW_AzurCommissionValue__c, adjustment), postUpdate.EW_AzurCommissionValue__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_BrokerCommissionValue__c, adjustment), postUpdate.EW_BrokerCommissionValue__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_IPTTotal__c, adjustment), postUpdate.EW_IPTTotal__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_PremiumGrossExIPT__c, adjustment), postUpdate.EW_PremiumGrossExIPT__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_PremiumGrossIncIPT__c, adjustment), postUpdate.EW_PremiumGrossIncIPT__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_PremiumNet__c, adjustment), postUpdate.EW_PremiumNet__c);
        System.assertNotEquals(preUpdate.Quote.vlocity_ins__StandardPremium__c, postUpdate.Quote.vlocity_ins__StandardPremium__c);

    }

    private static testMethod void testUwAdjustmentForMTA() {
        setup();

        QuoteLineItem preUpdate;
        QuoteLineItem postUpdate;
        Decimal adjustment = 3;


        Test.startTest();
        System.runAs(systemAdminUser) {

            preUpdate = [
                    SELECT Id, EW_AzurCommissionValue__c,
                            EW_BrokerCommissionValue__c,
                            EW_IPTTotal__c,EW_PremiumGrossExIPT__c,
                            EW_PremiumGrossIncIPT__c,
                            EW_PremiumNet__c,
                            vlocity_ins__AttributeSelectedValues__c,
                            Quote.vlocity_ins__StandardPremium__c
                    FROM QuoteLineItem
                    WHERE vlocity_ins__ParentItemId__c != NULL AND Quote.EW_TypeofTransaction__c = 'MTA'
                    LIMIT 1
            ];

            mtaQuote.EW_UW_Adjustment__c = adjustment;
            mtaQuote.EW_UW_Adjustment_Reason__c = 'Commercial Decision';
            update mtaQuote;

            postUpdate = [
                    SELECT Id, EW_AzurCommissionValue__c,
                            EW_BrokerCommissionValue__c,
                            EW_IPTTotal__c,EW_PremiumGrossExIPT__c,
                            EW_PremiumGrossIncIPT__c,
                            EW_PremiumNet__c,
                            vlocity_ins__AttributeSelectedValues__c,
                            Quote.vlocity_ins__StandardPremium__c
                    FROM QuoteLineItem
                    WHERE Id = :preUpdate.Id
                    LIMIT 1
            ];
        }
        Test.stopTest();

        System.assertNotEquals(preUpdate.vlocity_ins__AttributeSelectedValues__c, postUpdate.vlocity_ins__AttributeSelectedValues__c);

        System.assertEquals(getAdjustedValue(preUpdate.EW_AzurCommissionValue__c, adjustment), postUpdate.EW_AzurCommissionValue__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_BrokerCommissionValue__c, adjustment), postUpdate.EW_BrokerCommissionValue__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_IPTTotal__c, adjustment), postUpdate.EW_IPTTotal__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_PremiumGrossExIPT__c, adjustment), postUpdate.EW_PremiumGrossExIPT__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_PremiumGrossIncIPT__c, adjustment), postUpdate.EW_PremiumGrossIncIPT__c);
        System.assertEquals(getAdjustedValue(preUpdate.EW_PremiumNet__c, adjustment), postUpdate.EW_PremiumNet__c);
        System.assertNotEquals(preUpdate.Quote.vlocity_ins__StandardPremium__c, postUpdate.Quote.vlocity_ins__StandardPremium__c);
    }


    private static testMethod void testBindDuplicateCheck() {

        setup();

        System.runAs(systemAdminUser) {

            // secondary setup
            quote.vlocity_ins__EffectiveDate__c = Date.today();
            quote.Status = EWQuoteHelper.QUOTE_STATUS_ISSUED;
            update quote;

            String addressId = '123abc';
            vlocity_ins__InsurableItem__c insurableItem = new vlocity_ins__InsurableItem__c(
                    Name = EWQuoteHelper.MAIN_RESIDENCE, EW_Quote__c = quote.Id, EW_AddressId__c = addressId);
            insert insurableItem;

            //initial test
            Boolean hasExistingPolicyBefore = EWQuoteHelper.bindDuplicateCheck(quote.id);
            System.assertEquals(false, hasExistingPolicyBefore);

            Asset asset = new Asset(Name = 'testAsset', Status = EWQuoteHelper.QUOTE_STATUS_ISSUED, vlocity_ins__ExpirationDate__c = quote.vlocity_ins__EffectiveDate__c.addDays(2), AccountId = insured.Id);
            insert asset;
            vlocity_ins__AssetInsuredItem__c insuredItem = new vlocity_ins__AssetInsuredItem__c(Name = EWQuoteHelper.MAIN_RESIDENCE, vlocity_ins__PolicyAssetId__c = asset.id, EW_AddressId__c = addressId);
            insert insuredItem;
        }

        // key test
        Test.startTest();
        Boolean hasExistingPolicyAfter = EWQuoteHelper.bindDuplicateCheck(quote.id);
        System.assertEquals(true, hasExistingPolicyAfter);
        Test.stopTest();

    }

    private static testMethod void testCreateAuditLogs() {

        Test.startTest();
        setup();
        Test.stopTest();

        AuditLog__c[] auditLogs = [
                SELECT Id, Name, sObjectRecordId__c, sObjectType__c, fieldAPIName__c, fieldLabel__c,
                        previousValue__c, newValue__c, changedByUser__c
                FROM AuditLog__c
                WHERE sObjectType__c = 'Quote'
        ];
        System.assertEquals(true, (auditLogs.size() > 0));

        for (AuditLog__c al : auditLogs) {

            System.debug('al.sObjectRecordId__c :' + al.sObjectRecordId__c);

            System.debug('al.sObjectType__c :' + al.sObjectType__c);
            System.assertEquals('Quote', al.sObjectType__c);

            System.debug('al.fieldAPIName__c :' + al.fieldAPIName__c);
            System.debug('al.fieldLabel__c :' + al.fieldLabel__c);
            System.debug('al.previousValue__c :' + al.previousValue__c);
            System.debug('al.newValue__c :' + al.newValue__c);

            System.assertEquals(true, (al.previousValue__c != al.newValue__c));
        }


    }

    private static testMethod void testQLICarrierAccount() {

        setup();

        System.runAs(systemAdminUser) {
            setupCarrierAccountsAndCoverages();
        }

        for (QuoteLineItem qli : [
                SELECT Product2.Name, EW_Carrier__c
                FROM QuoteLineItem
                WHERE QuoteId = :quote.Id
        ]) {

            if (coverageNameSet.contains(qli.Product2.Name)) {

                System.assert(String.isBlank(qli.EW_Carrier__c), 'Carrier should be empty');
            }
        }

        Test.startTest();
        EWQuoteHelper.setQLICarrierAccount(quote.Id);
        Test.stopTest();

        for (QuoteLineItem qli : [
                SELECT Product2.Name, EW_Carrier__c
                FROM QuoteLineItem
                WHERE QuoteId = :quote.Id
        ]) {

            if (coverageNameSet.contains(qli.Product2.Name)) {

                System.assert(String.isNotBlank(qli.EW_Carrier__c), 'Carrier should NOT be empty');
            }
        }
    }

    private static testMethod void testGetTransactionCoverageValue() {

        Asset policy;
        Quote mtaQuote;
        Map<String, Object> returnedMap;

        setup();
        System.runAs(systemAdminUser) {
            policy = [SELECT Id,EW_Quote__c FROM Asset LIMIT 1];
            Quote policyQuote = ((List<Quote>) EWPolicyHelper.selectSObjectRecords('Quote', 'Id', policy.EW_Quote__c, true))[0];
            mtaQuote = policyQuote.clone();
            insert mtaQuote;
            List<QuoteLineItem> qliList = [SELECT Id, UnitPrice, PricebookEntryId, Quantity, Product2Id FROM QuoteLineItem WHERE QuoteId = :policyQuote.Id];
            List<QuoteLineItem> newQLIs = new List<QuoteLineItem>();
            for (QuoteLineItem qli : qliList) {
                QuoteLineItem newQli = qli.clone(false, true, false, false);
                newQli.UnitPrice = (newQli.UnitPrice == NULL || newQli.UnitPrice < 1) ? 10 : newQli.UnitPrice + 10;
                newQli.QuoteId = mtaQuote.Id;
                newQLIs.add(newQli);
            }
            insert newQLIs;
        }

        Test.startTest();
        System.runAs(brokerUser) {
            returnedMap = EWVlocityRemoteActionHelper.getTransactionCoverage(new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>{
                    'policyId' => policy.Id, 'quoteId' => mtaQuote.Id
            });
        }
        Test.stopTest();

        System.assertEquals(true, returnedMap.containsKey('transactionCoverage'));
        Map<String, Object> returnedData = (Map<String, Object>) returnedMap.get('transactionCoverage');
        System.assertNotEquals(null, returnedData.get('value'));

    }

    private static testMethod void testOverrideTransactionCoverageValue() {

        Asset policy;
        Quote mtaQuote;

        setup();
        System.runAs(brokerUser) {
            policy = [SELECT Id,EW_Quote__c FROM Asset LIMIT 1];
            Quote policyQuote = ((List<Quote>) EWPolicyHelper.selectSObjectRecords('Quote', 'Id', policy.EW_Quote__c, true))[0];
            mtaQuote = policyQuote.clone();
            mtaQuote.EW_IPTPercent__c = 12;
            insert mtaQuote;
            List<QuoteLineItem> qliList = [SELECT Id, UnitPrice, PricebookEntryId, Quantity, Product2Id FROM QuoteLineItem WHERE QuoteId = :policyQuote.Id];
            List<QuoteLineItem> newQLIs = new List<QuoteLineItem>();
            for (QuoteLineItem qli : qliList) {
                QuoteLineItem newQli = qli.clone(false, true, false, false);
                newQli.UnitPrice = (newQli.UnitPrice == NULL || newQli.UnitPrice < 1) ? 10 : newQli.UnitPrice + 10;
                newQli.QuoteId = mtaQuote.Id;
                newQLIs.add(newQli);
            }
            insert newQLIs;
        }

        QuoteTriggerHandler.IsDisabled = false;
        System.assertEquals(false, QuoteTriggerHandler.IsDisabled);
        System.assertEquals(true, [SELECT HasEditAccess,RecordId FROM UserRecordAccess WHERE UserId = :underwriterUser.Id AND RecordId = :mtaQuote.Id].HasEditAccess);

        Test.startTest();
        System.runAs(underwriterUser) {
            mtaQuote.UW_Override_MTA_Price__c = 123;
            update mtaQuote;
        }
        Test.stopTest();

        System.assertEquals(123, [SELECT vlocity_ins__StandardPremium__c FROM Quote WHERE Id = :mtaQuote.Id].vlocity_ins__StandardPremium__c);

    }


    static Decimal getAdjustedValue(Decimal oldValue, Decimal adjustment) {
        if (oldValue == null) oldValue = 0;
        Decimal returnValue = oldValue * (1 + (adjustment / 100));
        return returnValue.setScale(5);
    }

    public static void setupCarrierAccountsAndCoverages() {

        Set<String> carrierNameSet = new Set<String>{
                'AIG', 'ARAG', 'HSB Engineering'
        };

        Map<String, Account> carrierAccountByNameMap = new Map<String, Account>();

        Id carrierAccountRecordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                'Account',
                EWConstants.ACCOUNT_RECORD_TYPE_CARRIER);


        for (String carrierName : carrierNameSet) {

            Account carrierAccount = new Account(
                    Name = carrierName,
                    CurrencyIsoCode = EWConstants.CURRENCY_ISO_CODE_GBP,
                    EW_DRCode__c = String.valueOf(Math.random()),
                    RecordTypeId = carrierAccountRecordTypeId);

            carrierAccountByNameMap.put(carrierName, carrierAccount);
        }

        insert carrierAccountByNameMap.values();

        List<EW_Coverages__c> coverageList = new List<EW_Coverages__c>();

        for (String coverageName : coverageNameSet) {

            String coverageKey = coverageName + '-' + EWConstants.CURRENCY_ISO_CODE_GBP;

            switch on coverageName {

                when 'Home Emergency', 'Legal Expenses' {
                    coverageList.add(new EW_Coverages__c(
                            Name = coverageKey,
                            CarrierId__c = carrierAccountByNameMap.get('ARAG').Id));
                }

                when 'Personal Cyber' {
                    coverageList.add(new EW_Coverages__c(
                            Name = coverageKey,
                            CarrierId__c = carrierAccountByNameMap.get('HSB Engineering').Id));
                }

                when else {
                    coverageList.add(new EW_Coverages__c(
                            Name = coverageKey,
                            CarrierId__c = carrierAccountByNameMap.get('AIG').Id));
                }
            }
        }

        insert coverageList;
    }

    static void setup() {

        Contact brokerContact;
        Product2 product, childProduct;
        List<PricebookEntry> pbes;
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            brokerContact = EWContactDataFactory.createContact(null, null, broker.Id, true);

            List<Product2> products = new List<Product2>();
            product = EWProductDataFactory.createProduct(null, false);
            product.ProductCode = EWConstants.PRODUCT2_PRODUCT_CODE_EMERGING_WEALTH;
            products.add(product);
            childProduct = EWProductDataFactory.createProduct(null, false);
            childProduct.ProductCode = 'buildings';
            products.add(childProduct);
            insert products;

            pbes = new List<PricebookEntry>();
            PricebookEntry priceBookEntry = new PricebookEntry (Product2Id = products[0].id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
            PricebookEntry priceBookEntry2 = new PricebookEntry (Product2Id = products[1].id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
            pbes.add(priceBookEntry);
            pbes.add(priceBookEntry2);
            insert pbes;

            // set Fixed Expense
            vlocity_ins__CalculationMatrix__c basePremiumMatrix = EWVlocityDataFactory.createCalculationMatrix('BasePremium');
            vlocity_ins__CalculationMatrix__c coverageTypeMatrix = EWVlocityDataFactory.createCalculationMatrix('CoverageType');
            vlocity_ins__CalculationMatrixVersion__c bpmVersion = EWVlocityDataFactory.createMatrixVersion(basePremiumMatrix);
            vlocity_ins__CalculationMatrixVersion__c ctmVersion = EWVlocityDataFactory.createMatrixVersion(coverageTypeMatrix);
            EWVlocityDataFactory.activateCalculationMatrix(bpmVersion.Id);
            EWVlocityDataFactory.activateCalculationMatrix(ctmVersion.Id);

            List<vlocity_ins__CalculationMatrixRow__c> matrixRows = new List<vlocity_ins__CalculationMatrixRow__c>();
            vlocity_ins__CalculationMatrixRow__c basePremiumRow = EWVlocityDataFactory.createCalculationMatrixRow('BP', '{"basePremium":"1"}', '{"FE":22.4994}', bpmVersion.Id);
            vlocity_ins__CalculationMatrixRow__c coverageTypeRow1 = EWVlocityDataFactory.createCalculationMatrixRow('CT', '{"coverage":"Building & Contents"}', '{"FE":1.0}', ctmVersion.Id);
            vlocity_ins__CalculationMatrixRow__c coverageTypeRow2 = EWVlocityDataFactory.createCalculationMatrixRow('CT', '{"coverage":"Building Only"}', '{"FE":0.5}', ctmVersion.Id);
            vlocity_ins__CalculationMatrixRow__c coverageTypeRow3 = EWVlocityDataFactory.createCalculationMatrixRow('CT', '{"coverage":"Contents Only"}', '{"FE":0.5}', ctmVersion.Id);
            matrixRows.add(basePremiumRow);
            matrixRows.add(coverageTypeRow1);
            matrixRows.add(coverageTypeRow2);
            matrixRows.add(coverageTypeRow3);
            insert matrixRows;

        }

        Profile brokerProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_EW_PARTNER_NAME);
        Profile uwProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_AZURUNDERWRITER_NAME);
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);
        underwriterUser = EWUserDataFactory.createUser('underWriter', uwProfile.Id, NULL, true);

        System.runAs(brokerUser) {

            insured = EWAccountDataFactory.createInsuredAccount(
                    'Mr',
                    'Test',
                    'Insured',
                    Date.newInstance(1969, 12, 29),
                    true);


            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), false);
            quote.vlocity_ins__StandardPremium__c = 27.4579;
            quote.EW_IPTPercent__c = 12.0;
            quote.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE;
            insert quote;

            mtaQuote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), false);
            mtaQuote.vlocity_ins__StandardPremium__c = 27.4579;
            mtaQuote.EW_TypeofTransaction__c = 'MTA';
            mtaQuote.EW_IPTPercent__c = 12.0;
            insert mtaQuote;

            QuoteLineItem qli = new QuoteLineItem();
            qli.QuoteId = quote.Id;
            qli.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
            qli.PricebookEntryId = pbes[0].Id;
            qli.Quantity = 1;
            qli.UnitPrice = 0;
            qli.Product2Id = product.Id;

            QuoteLineItem qli2 = new QuoteLineItem();
            qli2.QuoteId = quote.Id;
            qli2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
            qli2.PricebookEntryId = pbes[1].Id;
            qli2.Quantity = 1;
            qli2.UnitPrice = 0;
            qli2.Product2Id = childProduct.Id;

            QuoteLineItem mtaQli = new QuoteLineItem();
            mtaQli.QuoteId = mtaQuote.Id;
            mtaQli.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
            mtaQli.PricebookEntryId = pbes[0].Id;
            mtaQli.Quantity = 1;
            mtaQli.UnitPrice = 0;
            mtaQli.Product2Id = product.Id;

            QuoteLineItem mtaQli2 = new QuoteLineItem();
            mtaQli2.QuoteId = mtaQuote.Id;
            mtaQli2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
            mtaQli2.PricebookEntryId = pbes[1].Id;
            mtaQli2.Quantity = 1;
            mtaQli2.UnitPrice = 0;
            mtaQli2.Product2Id = childProduct.Id;

            QuoteTriggerHandler.IsDisabled = true;
            insert new List<QuoteLineItem>{
                    qli, mtaQli
            };
            qli2.vlocity_ins__ParentItemId__c = qli.Id;
            mtaQli2.vlocity_ins__ParentItemId__c = mtaQli.Id;
            insert new List<QuoteLineItem>{
                    qli2, mtaQli2
            };
            QuoteTriggerHandler.IsDisabled = false;

            policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
            policy.IBA_InsuredName__c = 'insName';
            policy.Status = 'Draft';
            policy.EW_Quote__c = quote.Id;
            policy.vlocity_ins__StandardPremium__c = 123;
            policy.vlocity_ins__AnnualPremium__c = 456;
            policy.vlocity_ins__TotalPremiumForTerm__c = 789;
            policy.EW_FixedExpense__c = 40;
            policy.IBA_InsurancePremiumTaxRate__c = 0;
            policy.IBA_MGACommissionRate__c = 0;
            policy.IBA_BrokerCommissionRate__c = 0;
            insert policy;

            mtaQuote.EW_Source_Policy__c = policy.Id;

        }


    }

    private static testMethod void testReverseQuote(){

        setup();

        System.runAs(brokerUser) {

            new EWVlocityRemoteActionHelper().invokeMethod('reverseRecord', new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>{
                    'recordId' => quote.Id
            });

            EWQuoteHelper.doReverseQuote(quote.Id,  new Map<String, Object>());
        }

        List<Quote> quotes = [SELECT Id,vlocity_ins__StandardPremium__c FROM Quote WHERE OpportunityId = :opportunity.Id AND EW_TypeofTransaction__c = :EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE ];
        System.assertEquals(2,quotes.size());
        System.assertEquals(0, quotes[0].vlocity_ins__StandardPremium__c + quotes[1].vlocity_ins__StandardPremium__c);
    }

    private static testMethod void cloneQuoteAndQLIsForNewCancellationTest() {
        cloneQuoteAndQLIsForCancellation(MODE_NEW);
    }

    private static testMethod void cloneQuoteAndQLIsForEditCancellationTest() {
        cloneQuoteAndQLIsForCancellation(MODE_EDIT);
    }


    private static void cloneQuoteAndQLIsForCancellation(String testMode){
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        Quote clonedQuote;
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote newBusinessQuote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), false);
        newBusinessQuote.EW_AzurCommissionPercent__c = 12;
        newBusinessQuote.EW_BrokerCommissionPercent__c = 25;
        newBusinessQuote.EW_TotalCommissionPercent__c = 37;
        newBusinessQuote.EW_IPTPercent__c = 12;
        newBusinessQuote.EW_IPTPercent__c = 12;
        newBusinessQuote.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE;
        insert newBusinessQuote;

        String cConfReasonDetail = 'testConfReason';
        String cConfReason = 'Broker lost client';
        String cConfDate = '2019-11-19T23:00:00.000Z';
        String quoteStatus = 'Quoted';
        Integer daysRemaining = testMode == MODE_NEW ? 116 : 360;
        Integer daysFromInception = 365 - daysRemaining;

        Product2 product = EWProductDataFactory.createProduct(null, true);
        PricebookEntry priceBookEntry = new PricebookEntry (Product2Id = product.id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
        insert priceBookEntry;

        QuoteLineItem qli = new QuoteLineItem();
        qli.QuoteId = newBusinessQuote.Id;
        qli.vlocity_ins__AttributeSelectedValues__c = '{"excess":500,"paymentMethod":"Broker Collection","azurCommissionValue":160.38346,"iptPercent":12,"brokerCommissionValue":334.13221,"premiumGrossExIPT":1336.52882,"premiumNet":842.014,"basePremium":1,"brokerPercentMaximum":30,"brokerPercentMinimum":0,"brokerPercentCommission":25,"points":0,"randomNumber":2,"policyAge":0,"campaign":"Base Campaign","quoteDateOfIssue":"2019-03-13T00:00:00.000Z","quoteExpiryDate":"2019-04-12T08:41:24.093Z","azurPercentCommission":12,"underwriterAdjustmentPercent":0,"totalPercentCommission":0.37,"totalCommission":1.5873,"premiumGrossIncIPT":1496.91228,"jointPolicyCategory":"Not a joint policy","iptTotal":160.38346,"coverage":"Building & Contents","calculateBuildingsPremium":"true","aigPremiumNet":778.104,"aigPremiumGrossExIPT":1235.08448,"aigBrokerCommission":308.77112,"aigAzurCommissionValue":148.21014}';
        qli.PricebookEntryId = priceBookEntry.Id;
        qli.Quantity = 1;
        qli.UnitPrice = 0;
        qli.Product2Id = product.Id;
        insert qli;

        policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.EW_Quote__c = newBusinessQuote.Id;
        insert policy;

        newBusinessQuote.EW_Policy__c = policy.Id;
        update newBusinessQuote;

        vlocity_ins__InsuranceClaim__c claim = new vlocity_ins__InsuranceClaim__c(
                Name = 'testClaim',
                EW_Quote__c = newBusinessQuote.Id,
                vlocity_ins__PrimaryPolicyAssetId__c = policy.Id
        );
        insert claim;
        
        vlocity_ins__InsurableItem__c item = new vlocity_ins__InsurableItem__c(
                Name = 'testItem',
                EW_Quote__c = newBusinessQuote.Id
        );
        insert item;

        if(testMode == MODE_EDIT){
            // create the quote that is being edited.   For New, this will be created in the test "EWVlocityRemoteActionHelper().invokeMethod('cloneQuoteAndQLIs'..."
            clonedQuote = newBusinessQuote.clone();
            clonedQuote.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_CANCELLATION;
            clonedQuote.EW_Source_Policy__c =  policy.Id;
            clonedQuote.EW_Policy__c = null;
            insert clonedQuote;

            QuoteLineItem qliClone = qli.clone();
            qliClone.QuoteId = clonedQuote.Id;
            insert qliClone;
            
            claim.EW_Quote__c = clonedQuote.Id; //added by Aleksejs Jedamenko on 11/06/2020 after claim recreation was removed from EWQuoteHelper.createUpdateCancellationQuote method
            update claim;
        }

        String cancellationQuoteJSON = testMode == MODE_EDIT ? '{"Id" : "'+ clonedQuote.Id +'"}' : '{}';
        Object cancellationQuote = JSON.deserializeUntyped(cancellationQuoteJSON);
        Map<String,Object> cancellationQuoteMap = new Map<String,Object>{'CancellationQuote' => cancellationQuote};
        String pDetailsJSON = testMode == MODE_EDIT ? JSON.serialize(cancellationQuoteMap) : '{}';
        Object pDetails = JSON.deserializeUntyped(pDetailsJSON);


        Test.startTest();
        inputMap.put('pDetails', pDetails);
        inputMap.put('quoteId', newBusinessQuote.Id);
        inputMap.put('cConfReasonDetail', cConfReasonDetail);
        inputMap.put('cConfReason', cConfReason);
        inputMap.put('cConfDate', cConfDate);
        inputMap.put('quoteStatus', quoteStatus);
        inputMap.put('daysRemaining', daysRemaining);
        inputMap.put('daysFromInception', daysFromInception);

        new EWVlocityRemoteActionHelper().invokeMethod('cloneQuoteAndQLIs', inputMap, outputMap, optionMap);
        Test.stopTest();

        clonedQuote = [
                SELECT Id, vlocity_ins__EffectiveDate__c, EW_Cancellation_Reason__c,
                        EW_Cancellation_Reason_Detail__c, Status
                FROM Quote
                WHERE EW_TypeofTransaction__c = :EWConstants.QUOTE_TRANSACTION_TYPE_CANCELLATION
                LIMIT 1
        ];

        System.assert(outputMap.containsKey('cancellationQuoteId'));
        System.assertEquals(clonedQuote.vlocity_ins__EffectiveDate__c, Date.valueOf(cConfDate));
        System.assertEquals(clonedQuote.EW_Cancellation_Reason__c, cConfReason);
        System.assertEquals(clonedQuote.EW_Cancellation_Reason_Detail__c, cConfReasonDetail);

        QuoteLineItem qliTest = [
                SELECT EW_Transactional_PremiumNet__c, EW_Transactional_PremiumGrossExIPT__c, EW_Transactional_PremiumGrossIncIPT__c,
                        EW_Transactional_AzurCommissionValue__c, EW_Transactional_BrokerCommissionValue__c, EW_Transactional_IPT__c,
                        EW_PremiumNet__c, EW_PremiumGrossExIPT__c, EW_PremiumGrossIncIPT__c, EW_AzurCommissionValue__c, EW_BrokerCommissionValue__c, EW_IPTTotal__c
                FROM QuoteLineItem
                WHERE QuoteId = :clonedQuote.Id
                LIMIT 1
        ];

        Decimal expectedEW_PremiumNet = testMode == MODE_NEW ? -267.60000 : -qliTest.EW_PremiumNet__c;
        Decimal expectedEW_PremiumGrossExIPT = testMode == MODE_NEW ? -424.76000 : -qliTest.EW_PremiumGrossExIPT__c;
        Decimal expectedEW_PremiumGrossIncIPT = testMode == MODE_NEW ? -475.73000 : -qliTest.EW_PremiumGrossIncIPT__c;
        Decimal expectedEW_AzurCommissionValue = testMode == MODE_NEW ? -50.97000 : -qliTest.EW_AzurCommissionValue__c;
        Decimal expectedEW_BrokerCommissionValue = testMode == MODE_NEW ? -106.19000 : -qliTest.EW_BrokerCommissionValue__c;
        Decimal expectedEW_IPTTotal = testMode == MODE_NEW ? -50.97000 : -qliTest.EW_IPTTotal__c;
        
        System.assertEquals(expectedEW_PremiumNet, qliTest.EW_Transactional_PremiumNet__c);
        System.assertEquals(expectedEW_PremiumGrossExIPT, qliTest.EW_Transactional_PremiumGrossExIPT__c);
        System.assertEquals(expectedEW_PremiumGrossIncIPT, qliTest.EW_Transactional_PremiumGrossIncIPT__c);
        System.assertEquals(expectedEW_AzurCommissionValue, qliTest.EW_Transactional_AzurCommissionValue__c);
        System.assertEquals(expectedEW_BrokerCommissionValue, qliTest.EW_Transactional_BrokerCommissionValue__c);
        System.assertEquals(expectedEW_IPTTotal, qliTest.EW_Transactional_IPT__c);

        List<vlocity_ins__InsuranceClaim__c> expectedClaim = [SELECT Id FROM vlocity_ins__InsuranceClaim__c WHERE EW_Quote__c = :clonedQuote.Id];

        System.assert(expectedClaim.size() > 0);
   	
        List<vlocity_ins__InsurableItem__c> expectedItem = [SELECT Id FROM vlocity_ins__InsurableItem__c WHERE EW_Quote__c = :newBusinessQuote.Id];
        system.assert(expectedItem.size() > 0);
    }

    @IsTest
    private static void cloneQuoteAndQLIsForCancellationExpTest(){
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);

        String cConfReasonDetail = 'testConfReason';
        String cConfReason = 'no such reason';
        String cConfDate = '2019-11-19T23:00:00.000Z';
        String quoteStatus = 'Cancellation';

        String expFistMessage = 'INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST';
        String expTypeName = 'System.DmlException';

        Test.startTest();
        inputMap.put('quoteId', quote.Id);
        inputMap.put('cConfReasonDetail', cConfReasonDetail);
        inputMap.put('cConfReason', cConfReason);
        inputMap.put('cConfDate', cConfDate);
        inputMap.put('quoteStatus', quoteStatus);
        try{
            new EWVlocityRemoteActionHelper().invokeMethod('cloneQuoteAndQLIs', inputMap, outputMap, optionMap);
        }catch(Exception exp){
            System.assert(exp.getMessage().contains(expFistMessage));
            System.assert(exp.getTypeName() == expTypeName);
        }
        Test.stopTest();
    }

    @IsTest
    private static void testQuoteCheckPropertyDuplicate(){
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );
        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.Status = 'Issued';
        policy.Policy_Status__c = 'Active';
        insert policy;

        String uprn = '12345';

        insert new EWFeatureSwitches__c(LivePolicyChecking__c = true);

        vlocity_ins__AssetInsuredItem__c residence = new vlocity_ins__AssetInsuredItem__c();
        residence.vlocity_ins__PolicyAssetId__c = policy.Id;
        residence.EW_UPRN__c = uprn;

        insert residence;

        Boolean results;

        Test.startTest();
        results = EWQuoteHelper.quoteCheckPropertyDuplicate(uprn);
        Test.stopTest();

        System.assertEquals(true, results);
    }


    /**
    * @methodName: testCreateQuotePDF
    * @description: test method for EWQuoteHelper.createQuotePDF
    * @author: Roy Lloyd
    * @dateCreated: 27/3/2020
    */
    private static testMethod void testCreateQuotePDF() {

        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);

        vlocity_ins__Document__c doc = new vlocity_ins__Document__c(Name = 'testDoc', EW_Quote__c = quote.Id, EW_EmailBrokerOnPDFCreation__c = true);
        insert doc;

        insert new EWFeatureSwitches__c(AutomaticDocumentsAfterApproval__c = true);

        // tests sync
        EWQuoteHelper.createQuotePDF(doc.Id);

        // tests async
        test.startTest();
        new EWVlocityRemoteActionHelper().invokeMethod('createPDF',
                new Map<String,Object>(),
                new Map<String,Object>(),
                new Map<String,Object>{'docId' => doc.Id}
        );
        System.assertEquals(1, [SELECT count() FROM Attachment WHERE ParentId = :quote.Id]);
        test.stopTest();

        System.assertEquals(2, [SELECT count() FROM Attachment WHERE ParentId = :quote.Id]);
        System.assertNotEquals(0, Limits.getEmailInvocations());

    }
    
    /**
    * @methodName: testPCLQuoteReviewDeadlineCalculation
    * @description: test method for EWQuoteHelper.calculatePCLQuoteReviewDeadline
    * @author: Aleksejs Jedamenko
    * @dateCreated: 28/09/2020
    */
    @IsTest
    private static void testPCLQuoteReviewDeadlineCalculation(){
      
        Date testRequestDate = Date.today();
        Date testHoliday = testRequestDate.addDays(-1);
        String testHolidayString = String.valueOf(testHoliday);
        Test.startTest();
        Date testDeadlineDate = EWQuoteHelper.calculatePCLQuoteReviewDeadline(testHolidayString);
        System.assert(testDeadlineDate!=null);
        Test.stopTest();       
    }
}