/**
* @name: IBA_IBAStagingTriggerHandler
* @description: This class is handling and dispatching the relevant IBA_IBAStaging__c trigger functionality
*
*/

public with sharing class IBA_IBAStagingTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled()
    {
        return IBA_IBAStagingTriggerHandler.IsDisabled != null ? IBA_IBAStagingTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems)
    {
        // this method populates additional fields on records based on input data
        IBA_IBAStagingTriggerHandlerHelper.matchFields(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) 
    {
        // this method populates additional fields on records based on input data
        IBA_IBAStagingTriggerHandlerHelper.matchFields(newItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}