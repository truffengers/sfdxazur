public without sharing class EWBotPreChatCmpController {

    @AuraEnabled
    public static String getCurrentUser(){
        Map<String,Object> output = new Map<String,Object>();
        User u = [SELECT Id, FirstName, LastName, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        if(u.ContactId != null){
            output.put('userId', u.Id);
            output.put('firstName', u.FirstName);
            output.put('lastName', u.LastName);
            output.put('token', encodeUserId(u.Id));
        }else{
            output.put('userId', '');
        }
        return JSON.serialize(output);
    }
    
    @AuraEnabled
    public static String encodeUserId(String userId){
        return (EncodingUtil.base64Encode(Crypto.signWithCertificate('RSA-SHA256',Blob.valueOf(userId),'AzurChatbot'))).subString(0,255);
    }
    
    public class PrechatOutput{
        @InvocableVariable
        public String status;
    }
    
    public class PrechatInput{
        @InvocableVariable
        public String sChatKey;
    }
    
    @InvocableMethod(label='Bot Validate User')
    public static List<PrechatOutput> validateUser(List<PrechatInput> inputParameters){
        String sChatKey = inputParameters[0].sChatKey;
        List<prechatoutput> outputParameters = new List<prechatoutput>();
        PrechatOutput outputParameter = new PrechatOutput();
        
        if (sChatKey != null && sChatKey != '')
        {
            List<LiveChatTranscript> transcripts = [SELECT Id, AZ_UserId__c, AZ_Token__c FROM LiveChatTranscript WHERE ChatKey = :sChatKey];
            if (transcripts.size()>0)
            {
                if(encodeUserId(transcripts[0].AZ_UserId__c) == transcripts[0].AZ_Token__c){
                    outputParameter.status = 'success';
                } else {
                    outputParameter.status = 'error: Token Before: ' + transcripts[0].AZ_Token__c + ' | Token After: '+encodeUserId(transcripts[0].AZ_UserId__c) + ' | User Id: '+ transcripts[0].AZ_UserId__c;
                }
            }
        }
        outputParameters.add(outputParameter);
        return outputParameters;
    }
}