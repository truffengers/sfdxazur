TT.menu = (function($) {

  'use strict';

  var $button;
  var $panel;
  var $buttonAndPanel;
  var $body;

  var mediaMatcher;

  var isAnimating = false;
  var isOpen      = false;

  var ON  = 'on';
  var OFF = 'off';



  function init(params) {
    mediaMatcher = params.mediaMatcher;
    referenceElements();
    addListeners();
  }

  function referenceElements() {
    $button         = $('#js-navigation-button');
    $panel          = $('#js-navigation');
    $buttonAndPanel = $button.add($panel);
    $body           = $('body');
  }

  function addListeners() {
    $button.click(buttonClickHandler);
  }

  function buttonClickHandler(event) {
    event.preventDefault();
    openOrClose();
  }

  function openOrClose(callback) {
    if(!isAnimating) {
      isAnimating = true;
      isOpen = !isOpen;
      $buttonAndPanel[isOpen  ? 'addClass' : 'removeClass']('is-open');
      $buttonAndPanel[!isOpen ? 'addClass' : 'removeClass']('is-closed');
      $body[isOpen ? 'addClass' : 'removeClass']('is-scrolling-prevented');
      turnBodyTouchMovePrevention(isOpen ? ON : OFF);
      window.setTimeout(function() {
        isAnimating = false;
        if(callback) { callback(); }
      }, getPauseDuration());
    }
  }

  function preventEventDefault(event) {
    event.preventDefault();
  }

  function turnBodyTouchMovePrevention(onOrOff) {
    $body[onOrOff]('touchmove', preventEventDefault);
  }

  function getPauseDuration() {
    return mediaMatcher.getIsMobile() ? 500 : 0;
  }

  function getIsAnimating() {
    return isAnimating;
  }

  function getIsOpen() {
    return isOpen;
  }



  return {
    init           : init,
    openOrClose    : openOrClose,
    getIsAnimating : getIsAnimating,
    getIsOpen      : getIsOpen
  };

}(jQuery));