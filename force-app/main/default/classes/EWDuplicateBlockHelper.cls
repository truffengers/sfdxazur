/**
 * Created by andrewxu on 21/05/2020.
 *
 * This class is created WITHOUT SHARING so that when checking for blocked Accounts, the
 * broker can see all Accounts from all other brokerages. If we block a user, we do not
 * want them to be able to create a quote at ANY brokerage.
 */

public without sharing class EWDuplicateBlockHelper {
    /**
    * @methodName: quoteCheckPropertyDuplicate
    * @description: Used in EWAccountHelper.checkAccount.
    * @author: Andrew Xu
    */
    public static List<Account> queryAccounts(String firstName, String lastName, Datetime birthDateTime) {
        return [
                SELECT Id, EW_IsBlocked__c, EW_BrokerAccount__c
                FROM Account
                WHERE IsPersonAccount = TRUE
                AND FirstName = :firstName
                AND LastName = :lastName
                AND PersonBirthdate = :birthDateTime.date()
        ];
    }

    /**
    * @methodName: queryMainResidences
    * @description: Used in EWQuoteHelper.quotecheckPropertyDuplicate.
    * @author: Andrew Xu
    */
    public static List<vlocity_ins__AssetInsuredItem__c> queryMainResidences(String uprn) {
        return [
                SELECT Id, vlocity_ins__PolicyAssetId__c, EW_UPRN__c
                FROM vlocity_ins__AssetInsuredItem__c
                WHERE EW_UPRN__c=:uprn
        ];
    }

    /**
    * @methodName: queryActivePolicies
    * @description: Used in EWQuoteHelper.quotecheckPropertyDuplicate.
    * @author: Andrew Xu
    */
    public static List<Asset> queryActivePolicies(String policyId) {
        return [
                SELECT Id, Status, Policy_Status__c
                FROM Asset
                WHERE Id=:policyId
                AND Status=:EWConstants.EW_POLICY_STATUS_ISSUED
                AND Policy_Status__c=:EWConstants.POLICY_COVERAGE_IBA_STATUS_ACTIVE
        ];
    }
}