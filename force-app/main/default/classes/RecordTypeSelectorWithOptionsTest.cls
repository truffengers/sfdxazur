/*
* @name: RecordTypeSelectorWithOptionsTest
* @description: RecordTypeSelectorWithOptions apex test controller
* @author: Dimitris Schizas dschizas@azuruw.com
* @date: 11/04/2018
* @modifiedBy:
*/
@isTest
public class RecordTypeSelectorWithOptionsTest {

    testMethod static void invokeMethodTest(){
        //Set params
        String methodName = 'RecordTypeSelector';
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new  Map<String,Object>{'objectType' => 'Account'};
        //Execute the test
        Test.startTest();
            RecordTypeSelectorWithOptions recordTypeSelectorWithOptions = new RecordTypeSelectorWithOptions();
            RecordTypeSelectorWithOptions.invokeMethod(methodName, inputMap, outMap, options);
        Test.stopTest();
    } 
}