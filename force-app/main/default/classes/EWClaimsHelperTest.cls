/**
* @name: EWClaimsHelper
* @description: Helper class for claim scoring
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 02/11/2018
*/

@isTest
public with sharing class EWClaimsHelperTest {


    private static testMethod void testRemoteActionClaimScoring() {

        dataSetup();
        Map<String, Object> inputMap = testInputMapData(false, false);

        Test.startTest();
        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        Boolean check = raHelper.invokeMethod('calculateClaimScore', inputMap, new Map<String, Object>(), new Map<String, Object>());
        Test.stopTest();

        System.assertEquals(4, EWClaimsHelper.scoredClaimMap.size());
        System.assertEquals(true, check);
    }

    private static testMethod void testRemoteActionClaimScoring_Renewal() {

        dataSetup();
        Map<String, Object> inputMap = testInputMapData(false, true);

        Test.startTest();
        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        Boolean check = raHelper.invokeMethod('calculateClaimScore', inputMap, new Map<String, Object>(), new Map<String, Object>());
        Test.stopTest();

        System.assertEquals(4, EWClaimsHelper.scoredClaimMap.size());
        System.assertEquals(true, check);
    }


    private static testMethod void testRemoteActionPostClaims() {

        dataSetup();
        Map<String,Object> testInputData = (Map<String,Object>) testInputMapData(true, false);
        Map<String, Object> inputMap = (Map<String, Object>) testInputData.get(EWClaimsHelper.PROPERTY_DETAILS_JSON_KEY);

        Integer claimCount_PreTest = [SELECT Count() FROM vlocity_ins__InsuranceClaim__c];

        Test.startTest();
        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        Boolean check = raHelper.invokeMethod('postClaims',inputMap, new Map<String, Object>(), new Map<String, Object>());
        Test.stopTest();

        Integer claimCount_PostTest = [SELECT Count() FROM vlocity_ins__InsuranceClaim__c];
        // Commented out so that we can fix this test - EWH-224
        //  System.assertEquals(claimCount_PreTest + 4, claimCount_PostTest);

    }

    private static Map<String, Object> testInputMapData(Boolean withClaimScores, Boolean isRenewal) {

        EWClaimsHelper.scoredClaimMap.clear();
        String returnKey = isRenewal ? EWClaimsHelper.CLAIMS_LIST_READONLY_JSON_KEY : EWClaimsHelper.CLAIMS_LIST_JSON_KEY;

        List<Object> claims = new List<Object>();
        Set<String> claimTypes = new Set<String>{
                'Accidental damage', 'Theft from the Home', 'Storm', 'Water damage'
        };

        Date claimDate = Date.today().addDays(-180);
        String claimDateText = claimDate.day() + '/' + claimDate.month() + '/' + claimDate.year();
        integer index = 0;
        for (String claimType : claimTypes) {
            String claimJson;
            Object claim;

            if(isRenewal){
                claimJson = '{"selectClaimTypeRO":"' + claimType + '","textClaimValueRO":100,"claimDateTextRO":"' + claimDateText + '","claimYearRO":1,"radioClaimStillOpenRO":"No","radioClaimAtCurrentPropertyRO":"Yes"}';
                claim = (EWClaimsHelper.OutputClaim) JSON.deserialize(claimJson, EWClaimsHelper.OutputClaim.class);

            }else{
                claimJson = '{"claimKey":"' + index + '","selectClaimType":"' + claimType + '","textClaimValue":100,"claimDateText":"' + claimDateText + '","claimYear":1,"radioClaimStillOpen":"No","radioClaimAtCurrentProperty":"Yes"';
                claimJson += withClaimScores ? ',"claimScore":"' + (Math.random() * 3) + '"}' : '}';
                claim = (EWClaimsHelper.InputClaim) JSON.deserialize(claimJson, EWClaimsHelper.InputClaim.class);
            }

            claims.add(claim);

            if (withClaimScores) {
                EWClaimsHelper.scoredClaimMap.put(String.valueOf(index), (EWClaimsHelper.InputClaim) claim);
            }

            index++;
        }

        Map<String, Object> returnMap = new Map<String, Object>();

        if (withClaimScores) {

            // create Quote
            Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
            Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
            Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

            returnMap.put('claimScores', new Map<String, Object>{
                    'scoredClaims' => claims, 'quoteId' => quote.Id
            });

        } else {
            returnMap.put(returnKey, claims);
        }

        return new Map<String,Object>{ EWClaimsHelper.PROPERTY_DETAILS_JSON_KEY => returnMap };
    }


    private static void dataSetup(){

        // create Calculation Procedures
        Set<String> calculationProcedureNames = new Set<String>{'EmergingWealthSingleClaimCalculation','EmergingWealthClaimScores'};
        List<vlocity_ins__CalculationProcedure__c> procedures = EWVlocityDataFactory.createCalculationProcedures(calculationProcedureNames);

        // create Calculation Procedure Versions
        Map<String, Id> versionIdMap = EWVlocityDataFactory.createCalculationProcedureVersions(procedures);

        // create Calculation Matrix
        vlocity_ins__CalculationMatrix__c matrix = EWVlocityDataFactory.createCalculationMatrix('ClaimPeril');
        vlocity_ins__CalculationMatrixVersion__c matrixVersion = EWVlocityDataFactory.createMatrixVersion(matrix);
        createMatrixRows(matrixVersion.Id);
        EWVlocityDataFactory.activateCalculationMatrix(matrixVersion.Id);

        // create Calculation Procedure Variables and Steps
        addCalculationProcedureVariablesAndSteps(versionIdMap, matrix.Id);

        // activate Calculation Procedures
        EWVlocityDataFactory.activateCalculationProcedures(versionIdMap);

    }


    private static void addCalculationProcedureVariablesAndSteps(Map<String, Id> versionIdMap, Id matrixId ){

        List<vlocity_ins__CalculationProcedureVariable__c> variables = new List<vlocity_ins__CalculationProcedureVariable__c>();
        List<vlocity_ins__CalculationProcedureStep__c> steps = new List<vlocity_ins__CalculationProcedureStep__c>();

        variables.add(EWVlocityDataFactory.createCalculationProcedureVariable('ClaimScore', 'Number', '1.25', versionIdMap.get('EmergingWealthSingleClaimCalculation')) );
        variables.add(EWVlocityDataFactory.createCalculationProcedureVariable('claimKey', 'Text', '', versionIdMap.get('EmergingWealthSingleClaimCalculation')) );
        variables.add(EWVlocityDataFactory.createCalculationProcedureVariable('OtherClaimScore', 'Number', '1', versionIdMap.get('EmergingWealthClaimScores')) );

        // create Steps for the variables
        Map<Id,Integer> sequenceMap = new Map<Id,Integer>();

        for(vlocity_ins__CalculationProcedureVariable__c var : variables) {
            Id versionId = var.vlocity_ins__CalculationProcedureVersionId__c;
            System.debug('>>> Var = '+ var);
            Integer i = sequenceMap.containsKey(versionId) ? sequenceMap.get(versionId) : 0;
            i++;
            steps.add(EWVlocityDataFactory.createProcedureStep(var.Name, var.vlocity_ins__DataType__c, versionId, i, null) );
            sequenceMap.put(versionId, i);
        }

        // create Procedure Step for the Matrix lookup
        vlocity_ins__CalculationProcedureStep__c matrixStep = EWVlocityDataFactory.createProcedureStep(null, 'Text', versionIdMap.get('EmergingWealthSingleClaimCalculation'), sequenceMap.get(versionIdMap.get('EmergingWealthSingleClaimCalculation'))+1, matrixId);
        matrixStep.vlocity_ins__Input__c = '[{"name":"claimType","listValues":null,"label":null,"dataType":"Text","columnKey":"a65ab8"}]';
        matrixStep.vlocity_ins__OutputJSON__c ='[{"name":"peril","listValues":null,"label":null,"dataType":"Text","columnKey":"c9096c"}]';
        matrixStep.vlocity_ins__OutputMappingJSON__c = '{"peril":"ClaimPeril__peril"}';
        steps.add(matrixStep);

        insert variables;
        insert steps;

    }


    private static void createMatrixRows(Id versionId){

        List<vlocity_ins__CalculationMatrixRow__c> rows = new List<vlocity_ins__CalculationMatrixRow__c>{
                EWVlocityDataFactory.createCalculationMatrixRow('Header', '{"name":"claimType","listValues":null,"lineItemId":null,"label":null,"headerType":"Input","displayOrder":1,"dataType":"Text","columnKey":"a65ab8"}', null, versionId),
                EWVlocityDataFactory.createCalculationMatrixRow('Header', '{"name":"peril","listValues":null,"lineItemId":null,"label":null,"headerType":"Output","displayOrder":2,"dataType":"Text","columnKey":"c9096c"}', null, versionId),
                EWVlocityDataFactory.createCalculationMatrixRow(null, '{"claimType":"Other"}', '{"peril":"OtherClaimInput"}', versionId)

        };
        insert rows;
    }

    private static testMethod void testUpdateQuoteJSONDataAfterClaimDelete() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote1 = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);
        Quote quote2 = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        vlocity_ins__InsuranceClaim__c claim1 = new vlocity_ins__InsuranceClaim__c(EW_Quote__c = quote1.Id, Name = 'claim1');
        vlocity_ins__InsuranceClaim__c claim2_1 = new vlocity_ins__InsuranceClaim__c(EW_Quote__c = quote2.Id, Name = 'claim2_1');
        vlocity_ins__InsuranceClaim__c claim2_2 = new vlocity_ins__InsuranceClaim__c(EW_Quote__c = quote2.Id, Name = 'claim2_2');

        insert new List<vlocity_ins__InsuranceClaim__c>{
                claim1, claim2_1, claim2_2
        };

        Map<String, Object> objClaim1 = new Map<String, Object>{
                EWClaimsHelper.CLAIM_ID_JSON_KEY => claim1.Id,
                'name' => claim1.Name
        };
        Map<String, Object> objClaim2_1 = new Map<String, Object>{
                EWClaimsHelper.CLAIM_ID_JSON_KEY => claim2_1.Id,
                'name' => claim2_1.Name
        };
        Map<String, Object> objClaim2_2 = new Map<String, Object>{
                EWClaimsHelper.CLAIM_ID_JSON_KEY => claim2_2.Id,
                'name' => claim2_2.Name
        };

        // just one claim
        EW_QuoteJSONData__c quoteJSONData1 = new EW_QuoteJSONData__c();
        quoteJSONData1.EW_Quote__c = quote1.Id;

        Map<String, Object> propertyNode1 = new Map<String, Object>();
        propertyNode1.put(EWClaimsHelper.CLAIMS_LIST_JSON_KEY, (Object) objClaim1);
        quoteJSONData1.EW_PropertyNode__c = JSON.serializePretty((Object) propertyNode1);

        // list of claims
        EW_QuoteJSONData__c quoteJSONData2 = new EW_QuoteJSONData__c();
        quoteJSONData2.EW_Quote__c = quote2.Id;

        List<Object> claimsList = new List<Object>{
                (Object) objClaim2_1, (Object) objClaim2_2
        };

        Map<String, Object> propertyNode2 = new Map<String, Object>();
        propertyNode2.put(EWClaimsHelper.CLAIMS_LIST_JSON_KEY, (Object) claimsList);
        quoteJSONData2.EW_PropertyNode__c = JSON.serializePretty((Object) propertyNode2);

        insert new List<EW_QuoteJSONData__c>{
                quoteJSONData1, quoteJSONData2
        };

        delete new List<vlocity_ins__InsuranceClaim__c>{
                claim1, claim2_2
        };

        List<EW_QuoteJSONData__c> jsonDataList = new List<EW_QuoteJSONData__c>([
                SELECT Id, EW_PropertyNode__c, EW_Quote__c
                FROM EW_QuoteJSONData__c
                WHERE EW_Quote__c IN :new Set<Id>{
                        quote1.Id, quote2.Id
                }
        ]);

        for (EW_QuoteJSONData__c jsonData : jsonDataList) {
            if (jsonData.EW_Quote__c == quote1.Id) {
                // there was just one clam, got deleted and the editBlockClaimsHistory should be empty
                System.assert(!jsonData.EW_PropertyNode__c.contains(claim1.Id));
            } else if (jsonData.EW_Quote__c == quote2.Id) {
                // there was a list of claims, one got deleted and the editBlockClaimsHistory should hold another one
                System.assert(jsonData.EW_PropertyNode__c.contains(claim2_1.Id));
                System.assert(!jsonData.EW_PropertyNode__c.contains(claim2_2.Id));
            }
        }
    }

    private static testMethod void testSetIdClaimsUnderAzurInsOrNot() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        Asset policy = new Asset(Name = 'testAsset', Status = EWQuoteHelper.QUOTE_STATUS_ISSUED, vlocity_ins__ExpirationDate__c = quote.vlocity_ins__EffectiveDate__c.addDays(2), AccountId = insured.Id);
        insert policy;

        vlocity_ins__InsuranceClaim__c claim1_1 = new vlocity_ins__InsuranceClaim__c(EW_Quote__c = quote.Id, Name = 'claim1');
        vlocity_ins__InsuranceClaim__c claim1_2 = new vlocity_ins__InsuranceClaim__c(EW_Quote__c = quote.Id, Name = 'claim1');
        vlocity_ins__InsuranceClaim__c claim2_1 = new vlocity_ins__InsuranceClaim__c(Name = 'claim2');
        vlocity_ins__InsuranceClaim__c claim2_2 = new vlocity_ins__InsuranceClaim__c(Name = 'claim2');
        vlocity_ins__InsuranceClaim__c claim3_1 = new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id);
        vlocity_ins__InsuranceClaim__c claim3_2 = new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id);

        List<vlocity_ins__InsuranceClaim__c> claims = new List<vlocity_ins__InsuranceClaim__c>{
                claim1_1, claim1_2, claim2_1, claim2_2, claim3_1, claim3_2
        };

        insert claims;

        claims = new List<vlocity_ins__InsuranceClaim__c>([
                SELECT Id
                FROM vlocity_ins__InsuranceClaim__c
                WHERE EW_Azur_Claim__c = true
        ]);

        System.assert(!claims.isEmpty());
        System.assertEquals(2, claims.size());
    }


    private static testMethod void testGetPolicyClaims() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);
        Asset policy = new Asset(Name = 'testAsset', Status = EWQuoteHelper.QUOTE_STATUS_ISSUED, vlocity_ins__ExpirationDate__c = quote.vlocity_ins__EffectiveDate__c.addDays(2), AccountId = insured.Id);
        insert policy;

        vlocity_ins__InsuranceClaim__c claim1 = new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id);
        vlocity_ins__InsuranceClaim__c claim2 = new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id);
        List<vlocity_ins__InsuranceClaim__c> claims = new List<vlocity_ins__InsuranceClaim__c>{
                claim1, claim2
        };
        insert claims;

        Test.startTest();
        List<EWClaimsHelper.InputClaim> inputClaims = EWClaimsHelper.getPolicyClaims(policy.Id);
        Test.stopTest();

        System.assertEquals(claims.size(), inputClaims.size());

    }


    /**
    * @methodName: testCheckForOpenRenewalQuotes
    * @description: method to test the processing of claim records afterInsert, and to expire any open renewal quotes.  Called by the class InsuranceClaimTriggerHandler.
    * @author: Roy Lloyd
    * @date: 9/3/2020
    */
    private static testMethod void testCheckForOpenRenewalQuotes() {


        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);
        Asset policy = new Asset(Name = 'testAsset', Status = EWQuoteHelper.QUOTE_STATUS_ISSUED, vlocity_ins__ExpirationDate__c = quote.vlocity_ins__EffectiveDate__c.addDays(2), AccountId = insured.Id);
        insert policy;
        quote.Status = EWConstants.QUOTE_STATUS_BOUND;
        update quote;
        Quote renewalQuote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, false);
        renewalQuote.Status = EWConstants.QUOTE_STATUS_QUOTED;
        renewalQuote.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        renewalQuote.EW_Source_Policy__c = policy.Id;
        insert renewalQuote;

        vlocity_ins__InsuranceClaim__c claim1 = new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id);
        vlocity_ins__InsuranceClaim__c claim2 = new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id);
        List<vlocity_ins__InsuranceClaim__c> claims = new List<vlocity_ins__InsuranceClaim__c>{
                claim1, claim2
        };

        Test.startTest();
        insert claims;
        Test.stopTest();
        System.assertEquals(EWConstants.QUOTE_STATUS_EXPIRED , [SELECT Status FROM Quote WHERE Id = :renewalQuote.Id].Status);
      
    }


}