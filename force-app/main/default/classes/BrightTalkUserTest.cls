@istest
public class BrightTalkUserTest {

    private static TestObjectGenerator tog = new TestObjectGenerator();
    
    private static Contact c;
    private static User u;
    
    static {
        c = tog.getUserContact();
        c.email = 'foo@bar.com';
        update c;
        
        u = new User(Id = UserInfo.getUserId(), TimeZoneSidKey = UserInfo.getTimeZone().getID());
    }
    
    private static void assertEqual(BrightTalkUser btu_after) {
        System.assertEquals(c.Id, btu_after.getData('realmUserId'));
        System.assertEquals(c.FirstName, btu_after.getData('firstName'));
        System.assertEquals(c.LastName, btu_after.getData('lastName'));
        System.assertEquals(c.Email, btu_after.getData('email'));
        System.assertEquals(u.TimeZoneSidKey, btu_after.getData('timeZone'));
        System.assertEquals('', btu_after.getData('companyName'));        
    }
    
    @istest
    public static void basic() {
        BrightTalkUser btu = new BrightTalkUser(c, u);
        
        String xmlRep = btu.toXml(true);
        Dom.Document doc = new Dom.Document();
        doc.load(xmlRep);
        
        BrightTalkUser btu_after = new BrightTalkUser();
        btu_after.load(doc.getRootElement());
    	
        assertEqual(btu_after);
    }
    
    @istest
    public static void escapeChars() {
        c.FirstName = 'A & B';
        update c;
        
        BrightTalkUser btu = new BrightTalkUser(c, u);
        
        String xmlRep = btu.toXml(true);
        System.debug(xmlRep);
        Dom.Document doc = new Dom.Document();
        doc.load(xmlRep);
        
        BrightTalkUser btu_after = new BrightTalkUser();
        btu_after.load(doc.getRootElement());
    	
        assertEqual(btu_after);
    }
    
}