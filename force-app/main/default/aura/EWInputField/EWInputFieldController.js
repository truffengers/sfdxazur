({
    onKeyUp: function (component, event, helper) {
        if (event.keyCode === 13) {
            const isBrokerHubUsername = component.get('v.isBrokerHubUsername');
            if (isBrokerHubUsername) {
                helper.validateUsernameSufix(component, event.target.value, true);
            }
        }
    },

    onChange: function (component, event, helper) {
        component.set('v.value', event.target.value);
    },

    onBlur: function (component, event, helper) {
        const isBrokerHubUsername = component.get('v.isBrokerHubUsername');
        if (isBrokerHubUsername) {
            helper.validateUsernameSufix(component, event.target.value, false);
        }
    }
});