@IsTest
public class EWBotPreChatCmpControllerTest {
    
    @TestSetup
    static void testSetup(){
        
        Account acc = EWAccountDataFactory.createBrokerAccount('BotAccount', true);
        
        Contact cont = EWContactDataFactory.createContact('Bot', 'Test' , acc.Id , true);
        Id profileId = EWUserDataFactory.getProfile('EW Vlocity Partner Community User').Id;
        Id userRoleId = EWUserDataFactory.getUserRole('Business_user').Id;
        User partnerUser = EWUserDataFactory.createPartnerUser('BotTest',profileId, cont.Id, true);
        
        LiveChatVisitor lcv = new LiveChatVisitor();
        insert lcv;
        
        LiveChatTranscript lct = new LiveChatTranscript();
        lct.LiveChatVisitorid = lcv.id;
        lct.chatKey = 'testChatKey123';
        lct.AZ_UserId__c = partnerUser.Id;
        insert lct;
    }
    
    @isTest
    static void shouldReturnCurrentUser(){
        Map<String, Object> result = new Map<String, Object>();
        List<EWBotPreChatCmpController.PrechatOutput> pco = new List<EWBotPreChatCmpController.PrechatOutput>();
        User partner = [SELECT Id FROM User WHERE LastName = 'BotTest'];
        
        System.runAs(partner) {
            String response = EWBotPreChatCmpController.getCurrentUser();
            result = (Map<String, Object>) JSON.deserializeUntyped(response);
        }
        
        LiveChatTranscript lct = [SELECT Id, AZ_Token__c FROM LiveChatTranscript WHERE ChatKey = 'testChatKey123'];
        lct.AZ_Token__c = (String)result.get('token');
        update lct;
        
        system.assertEquals(true, result.containsKey('token'), 'Has a token');
        
        List<EWBotPreChatCmpController.PrechatInput> pcis = new List<EWBotPreChatCmpController.PrechatInput>();
        EWBotPreChatCmpController.PrechatInput pci = new EWBotPreChatCmpController.PrechatInput();
        pci.sChatKey = 'testChatKey123';
        pcis.add(pci);
        System.runAs(partner){
        	pco = EWBotPreChatCmpController.validateUser(pcis);
        }
        
        system.assertEquals('success', pco[0].status, 'Validation has been a success');
    }
}