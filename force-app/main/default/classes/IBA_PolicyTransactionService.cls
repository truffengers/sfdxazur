/**
* @name: IBA_PolicyTransactionService
* @description: This class contains business logic used in trigger for IBA_PolicyTransaction__c object.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 02/01/2020
*/
public with sharing class IBA_PolicyTransactionService {

    private static final String CURRENCY_GBP = 'GBP';
    private static final String COMPANY_NAME_IBA_TRUST_ACCOUNTS = 'IBA Trust Accounts';

    /**
    * @methodName: populateExchangeRate
    * @description: Populates Exchange Rate on Policy Transaction using matching Exchange Rate records.
    * @dateCreated: 02/01/2020
    */
    public static void populateExchangeRate (List<SObject> newItems) {
        Set<String> currencies = new Set<String>();
        List<IBA_PolicyTransaction__c> policyTransactionsToProcess = new List<IBA_PolicyTransaction__c>();
        for (IBA_PolicyTransaction__c policyTransaction : (List<IBA_PolicyTransaction__c>) newItems) {
            if (policyTransaction.IBA_ExchangeRate__c == null && policyTransaction.IBA_TransactionCurrency__c != null) {
                if (CURRENCY_GBP.equals(policyTransaction.IBA_TransactionCurrency__c)) {
                    policyTransaction.IBA_ExchangeRate__c = 1;
                } else {
                    currencies.add(policyTransaction.IBA_TransactionCurrency__c);
                    policyTransactionsToProcess.add(policyTransaction);
                }
            }
        }

        if (!policyTransactionsToProcess.isEmpty()) {
            List<c2g__codaExchangeRate__c> exchangeRates = [
                    SELECT c2g__StartDate__c,
                            c2g__Rate__c,
                            c2g__ExchangeRateCurrency__r.Name
                    FROM c2g__codaExchangeRate__c
                    WHERE c2g__ExchangeRateCurrency__r.Name IN :currencies
                    AND c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__r.Name = :COMPANY_NAME_IBA_TRUST_ACCOUNTS
            ];

            for (IBA_PolicyTransaction__c policyTransaction : policyTransactionsToProcess) {
                if (String.isNotBlank(policyTransaction.IBA_PeriodCalc__c) && policyTransaction.IBA_PeriodCalc__c.length() == 8) {
                    Date matchingStartDate = Date.newInstance(Integer.valueOf(policyTransaction.IBA_PeriodCalc__c.left(4)), Integer.valueOf(policyTransaction.IBA_PeriodCalc__c.right(3)), 1);
                    for (c2g__codaExchangeRate__c exchangeRate : exchangeRates) {
                        if (matchingStartDate == exchangeRate.c2g__StartDate__c
                                && policyTransaction.IBA_TransactionCurrency__c.equals(exchangeRate.c2g__ExchangeRateCurrency__r.Name)) {
                            policyTransaction.IBA_ExchangeRate__c = exchangeRate.c2g__Rate__c;
                            break;
                        }
                    }
                }
            }
        }
    }

}