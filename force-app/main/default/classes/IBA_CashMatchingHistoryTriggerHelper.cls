/**
* @name: IBA_CashMatchingHistoryTriggerHelper
* @description: IBA_CashMatchingHistoryTriggerHandler helper class
*
*/
public with sharing class IBA_CashMatchingHistoryTriggerHelper {

    public static void updateMatchingReference(Map<Id, SObject> newItems) {
        List<c2g__codaMatchingReference__c> matchingReferencesToUpdate = new List<c2g__codaMatchingReference__c>();
        for(c2g__codaCashMatchingHistory__c cashMatchingHistory :[SELECT Id, IBA_TransactionType__c, c2g__MatchingReference__r.IBA_CashEntry__c,
            IBA_ARCashTransactionID__c, c2g__MatchingReference__c, c2g__MatchingReference__r.IBA_ARCashTransaction__c,
            c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__CashEntry__c, c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__CashEntry__r.fflbx__Receipt__c
            FROM c2g__codaCashMatchingHistory__c WHERE Id IN :newItems.keySet()]) {
                c2g__codaMatchingReference__c matchingReferenceToUpdate = cashMatchingHistory.c2g__MatchingReference__r;
                populateMatchingReferenceARCashTransaction(cashMatchingHistory, matchingReferenceToUpdate);
                populateCashEntryLink(cashMatchingHistory, matchingReferenceToUpdate);
                if(matchingReferenceToUpdate != null && matchingReferenceToUpdate.IBA_CashEntry__c != null &&
                    matchingReferenceToUpdate.IBA_ARCashTransaction__c != null) {
                        matchingReferencesToUpdate.add(matchingReferenceToUpdate);
                }
        }
        if(matchingReferencesToUpdate.size() > 0) {
            Map<Id, c2g__codaMatchingReference__c> matchingReferencesMap = new Map<Id, c2g__codaMatchingReference__c>();
            matchingReferencesMap.putAll(matchingReferencesToUpdate);
            update matchingReferencesMap.values();
        }
    }    

    private static void populateCashEntryLink(c2g__codaCashMatchingHistory__c cashMatchingHistory, c2g__codaMatchingReference__c matchingReferenceToUpdate) {
        if(cashMatchingHistory.IBA_TransactionType__c == IBA_UtilConst.TRANSACTION_TYPE_CASH &&
            cashMatchingHistory.c2g__MatchingReference__c != null &&
            cashMatchingHistory.c2g__MatchingReference__r.IBA_CashEntry__c == null) {
                matchingReferenceToUpdate.IBA_CashEntry__c = cashMatchingHistory.c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__CashEntry__c;
        }
    }

    private static void populateMatchingReferenceARCashTransaction(c2g__codaCashMatchingHistory__c cashMatchingHistory, c2g__codaMatchingReference__c matchingReferenceToUpdate) {
        if(cashMatchingHistory.IBA_ARCashTransactionID__c != null &&
            cashMatchingHistory.c2g__MatchingReference__c != null &&
            cashMatchingHistory.c2g__MatchingReference__r.IBA_ARCashTransaction__c == null) {
                matchingReferenceToUpdate.IBA_ARCashTransaction__c = cashMatchingHistory.c2g__TransactionLineItem__r.c2g__Transaction__r.c2g__CashEntry__r.fflbx__Receipt__c;
        }
    }

    public static void calculateNumbersOnRelatedRecords(Map<Id, SObject> newItems) {
        Map<Id, c2g__codaPurchaseCreditNote__c> policyTransactionPayableCreditNoteMap = new Map<Id, c2g__codaPurchaseCreditNote__c>();
        Map<Id, c2g__codaInvoice__c> policyTransactionSalesInvoiceMap = new Map<Id, c2g__codaInvoice__c>();
        Map<Id, c2g__codaCreditNote__c> policyTransactionSalesCreditNoteMap = new Map<Id, c2g__codaCreditNote__c>();
        Map<Id, c2g__codaPurchaseInvoice__c> policyTransactionPayableInvoiceMap = new Map<Id, c2g__codaPurchaseInvoice__c>();
        Map<Id, List<c2g__codaJournalLineItem__c>> policyTransactionJournalLineItemMap = new Map<Id, List<c2g__codaJournalLineItem__c>>();

        Map<Id, Date> policyTransactionDateMap = new Map<Id, Date>();
        Map<Id, List<IBA_CashMatchingHistoryWrapper>> policyTransactionCashMatchingHistoryMap = new Map<Id, List<IBA_CashMatchingHistoryWrapper>>();
        List<IBA_CashMatchingHistoryWrapper> cashMatchingHistoryWrapperList = new List<IBA_CashMatchingHistoryWrapper>();
        Set<Id> transactionIds = new Set<Id>();
        Set<Id> transactionLineItemIds = new Set<Id>();
        Set<Id> matchingReferenceIds = new Set<Id>();
        
        for(Id recordId :newItems.keySet()) {
            c2g__codaCashMatchingHistory__c cashMatchingHistory = (c2g__codaCashMatchingHistory__c)newItems.get(recordId);
            IBA_CashMatchingHistoryWrapper cmhw = new IBA_CashMatchingHistoryWrapper(
                null,
                cashMatchingHistory,
                null,
                null
            );
            cashMatchingHistoryWrapperList.add(cmhw);
            transactionLineItemIds.add(cashMatchingHistory.c2g__TransactionLineItem__c);
            matchingReferenceIds.add(cashMatchingHistory.c2g__MatchingReference__c);
        }

        for(c2g__codaTransactionLineItem__c tli :[SELECT Id, c2g__Transaction__r.c2g__TransactionType__c, c2g__Transaction__r.c2g__TransactionDate__c, c2g__Transaction__r.IBA_PolicyTransaction__c 
            FROM c2g__codaTransactionLineItem__c WHERE Id IN :transactionLineItemIds]) {
                transactionIds.add(tli.c2g__Transaction__c);
                for(IBA_CashMatchingHistoryWrapper cmhw :cashMatchingHistoryWrapperList) {
                    if(cmhw.cashMatchingHistory.c2g__TransactionLineItem__c == tli.Id) {
                        cmhw.transactionType = tli.c2g__Transaction__r.c2g__TransactionType__c;
                        if(cmhw.transactionType == IBA_UtilConst.TRANSACTION_TYPE_CASH) {
                            cmhw.paidDate = tli.c2g__Transaction__r.c2g__TransactionDate__c;
                        } else {
                            cmhw.paidDate = cmhw.cashMatchingHistory.c2g__MatchingDate__c;
                        }
                        if(cmhw.cashMatchingHistory.c2g__Action__c == IBA_UtilConst.CASH_MATCHING_HISTORY_ACTION_UNDO_MATCH) {
                            cmhw.paidDate = null;
                        }
                        cmhw.policyTransactionId = tli.c2g__Transaction__r.IBA_PolicyTransaction__c;
                        if(cmhw.policyTransactionId != null) {
                            List<IBA_CashMatchingHistoryWrapper> cashMatchingHistoryList = policyTransactionCashMatchingHistoryMap.get(cmhw.policyTransactionId);
                            if(cashMatchingHistoryList == null) {
                                cashMatchingHistoryList = new List<IBA_CashMatchingHistoryWrapper>();
                                policyTransactionCashMatchingHistoryMap.put(cmhw.policyTransactionId, cashMatchingHistoryList);
                            }
                            cashMatchingHistoryList.add(cmhw);
                        }
                    }
                }
        }

        for(Id reference :matchingReferenceIds) {
            List<IBA_CashMatchingHistoryWrapper> relatedRecords = new List<IBA_CashMatchingHistoryWrapper>();
            for(IBA_CashMatchingHistoryWrapper cmhw :cashMatchingHistoryWrapperList) {
                if((cmhw.cashMatchingHistory.c2g__MatchingReference__c == reference)
                    && cmhw.transactionType != IBA_UtilConst.TRANSACTION_TYPE_JOURNAL) {
                        relatedRecords.add(cmhw);
                }
            }
            Date cashDate, salesInvoiceDate, salesCreditNoteDate;

            for(IBA_CashMatchingHistoryWrapper cmhw :relatedRecords) {
                if(cmhw.paidDate != null) {
                    if(cmhw.transactionType == IBA_UtilConst.TRANSACTION_TYPE_CASH &&
                        (cashDate < cmhw.paidDate || cashDate == null)) {
                            cashDate = cmhw.paidDate;
                    } else if(cmhw.transactionType == IBA_UtilConst.TRANSACTION_TYPE_INVOICE &&
                        (salesInvoiceDate < cmhw.paidDate || salesInvoiceDate == null)) {
                            salesInvoiceDate = cmhw.paidDate;
                    } else if(cmhw.transactionType == IBA_UtilConst.TRANSACTION_TYPE_CREDIT_NOTE &&
                        (salesCreditNoteDate < cmhw.paidDate || salesCreditNoteDate == null)) {
                            salesCreditNoteDate = cmhw.paidDate;
                    }
                }
            }

            for(IBA_CashMatchingHistoryWrapper cmhw :relatedRecords) {
                if(cmhw.transactionType == IBA_UtilConst.TRANSACTION_TYPE_INVOICE) {
                    if(cashDate != null) {
                        policyTransactionDateMap.put(cmhw.policyTransactionId, cashDate);
                    } else if(salesCreditNoteDate != null) {
                        policyTransactionDateMap.put(cmhw.policyTransactionId, salesCreditNoteDate);
                    } else {
                        policyTransactionDateMap.put(cmhw.policyTransactionId, cmhw.paidDate);
                    }
                } else if(cmhw.transactionType == IBA_UtilConst.TRANSACTION_TYPE_CREDIT_NOTE) {
                    if(cashDate != null) {
                        policyTransactionDateMap.put(cmhw.policyTransactionId, cashDate);
                    } else if(salesInvoiceDate != null) {
                        policyTransactionDateMap.put(cmhw.policyTransactionId, salesInvoiceDate);
                    } else {
                        policyTransactionDateMap.put(cmhw.policyTransactionId, cmhw.paidDate);
                    }
                }
            }
        }

        if(policyTransactionCashMatchingHistoryMap.size() > 0) {

            for(c2g__codaPurchaseInvoice__c payableInvoice :[SELECT Id, c2g__InvoiceStatus__c, IBA_PolicyTransaction__c, c2g__Transaction__c 
                FROM c2g__codaPurchaseInvoice__c 
                WHERE c2g__InvoiceStatus__c = :IBA_UtilConst.PAYABLE_INVOICE_STATUS_COMPLETE AND 
                c2g__Transaction__c IN :transactionIds AND
                IBA_PolicyTransaction__c IN :policyTransactionCashMatchingHistoryMap.keySet()]) {
                    for(Id policyTransactionId :policyTransactionCashMatchingHistoryMap.keySet()) {
                        if(payableInvoice.IBA_PolicyTransaction__c == policyTransactionId) {
                            policyTransactionPayableInvoiceMap.put(policyTransactionId, payableInvoice);
                        }
                    }
            }

            for(c2g__codaCreditNote__c salesCreditNote :[SELECT Id, c2g__CreditNoteStatus__c, IBA_PolicyTransaction__c, c2g__Transaction__c
                FROM c2g__codaCreditNote__c
                WHERE c2g__CreditNoteStatus__c = :IBA_UtilConst.SALES_CREDIT_NOTE_STATUS_COMPLETE AND 
                c2g__Transaction__c IN :transactionIds AND
                IBA_PolicyTransaction__c IN :policyTransactionCashMatchingHistoryMap.keySet()]) {
                    for(Id policyTransactionId :policyTransactionCashMatchingHistoryMap.keySet()) {
                        if(salesCreditNote.IBA_PolicyTransaction__c == policyTransactionId) {
                            policyTransactionSalesCreditNoteMap.put(policyTransactionId, salesCreditNote);
                        }
                    }
            }

            for(c2g__codaInvoice__c salesInvoice :[SELECT Id, c2g__InvoiceStatus__c, IBA_PolicyTransaction__c, c2g__Transaction__c
                FROM c2g__codaInvoice__c 
                WHERE c2g__InvoiceStatus__c = :IBA_UtilConst.SALES_INVOICE_STATUS_COMPLETE AND 
                c2g__Transaction__c IN :transactionIds AND
                IBA_PolicyTransaction__c IN :policyTransactionCashMatchingHistoryMap.keySet()]) {
                    for(Id policyTransactionId :policyTransactionCashMatchingHistoryMap.keySet()) {
                        if(salesInvoice.IBA_PolicyTransaction__c == policyTransactionId) {
                            policyTransactionSalesInvoiceMap.put(policyTransactionId, salesInvoice);
                        }
                    }
            }

            for(c2g__codaPurchaseCreditNote__c payableCreditNote :[SELECT Id, c2g__CreditNoteStatus__c, IBA_PolicyTransaction__c, c2g__Transaction__c
                FROM c2g__codaPurchaseCreditNote__c 
                WHERE c2g__CreditNoteStatus__c = :IBA_UtilConst.PAYABLE_CREDIT_NOTE_STATUS_COMPLETE AND 
                c2g__Transaction__c IN :transactionIds AND
                IBA_PolicyTransaction__c IN :policyTransactionCashMatchingHistoryMap.keySet()]) {
                    for(Id policyTransactionId :policyTransactionCashMatchingHistoryMap.keySet()) {
                        if(payableCreditNote.IBA_PolicyTransaction__c == policyTransactionId) {
                            policyTransactionPayableCreditNoteMap.put(policyTransactionId, payableCreditNote);
                        }
                    }
            }

            for(c2g__codaJournal__c journal :[SELECT Id, c2g__JournalStatus__c, IBA_PolicyTransaction__c, c2g__Transaction__c, (SELECT Id, c2g__Value__c, c2g__Account__c FROM c2g__JournalLineItems__r) 
                FROM c2g__codaJournal__c 
                WHERE c2g__JournalStatus__c = :IBA_UtilConst.JOURNAL_STATUS_COMPLETE AND 
                c2g__Transaction__c IN :transactionIds AND
                IBA_PolicyTransaction__c IN :policyTransactionCashMatchingHistoryMap.keySet()]) {
                    for(Id policyTransactionId :policyTransactionCashMatchingHistoryMap.keySet()) {
                        if(journal.IBA_PolicyTransaction__c == policyTransactionId) {
                            policyTransactionJournalLineItemMap.put(policyTransactionId, journal.c2g__JournalLineItems__r);
                        }
                    }
            }
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(policyTransactionCashMatchingHistoryMap.keySet());
            transferData.subtractCashMatchingHistory(policyTransactionCashMatchingHistoryMap, policyTransactionSalesCreditNoteMap, policyTransactionSalesInvoiceMap, policyTransactionJournalLineItemMap)
                .addCashMatchingHistory(policyTransactionCashMatchingHistoryMap, policyTransactionPayableInvoiceMap, policyTransactionPayableCreditNoteMap, policyTransactionJournalLineItemMap)
                .changeBrokerPaidDate(policyTransactionSalesInvoiceMap, policyTransactionSalesCreditNoteMap, policyTransactionDateMap)
                .transfer();
        }
    }

}