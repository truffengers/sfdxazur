public with sharing class EWProductRequirementHelper {

    public static void onAfterInsert(Map<Id, vlocity_ins__ProductRequirement__c> newItems) {
        fixProductRequirementUWRuleLink(newItems);
    }

    private static void fixProductRequirementUWRuleLink(Map<Id, vlocity_ins__ProductRequirement__c> newItems) {
        Set<String> prProductNames = new Set<String>();
        Set<Id> prProductIds = new Set<Id>();
        Set<String> prProductRequirementNames = new Set<String>();
        Map<String, Map<String, vlocity_ins__ProductRequirement__c>> prReqByProductIdAndReqName = new Map<String, Map<String, vlocity_ins__ProductRequirement__c>>();


        for (vlocity_ins__ProductRequirement__c pr : newItems.values()) {
            prProductIds.add(pr.vlocity_ins__Product2Id__c);
        }

        Map<Id, Product2> productMap = new Map<Id, Product2>([
                SELECT Id, Name
                FROM Product2
                WHERE Id IN :prProductIds
        ]);

        for (vlocity_ins__ProductRequirement__c pr : newItems.values()) {
            if (String.isBlank(pr.vlocity_ins__Product2Id__c)) {
                continue;
            }

            String productName = productMap.get(pr.vlocity_ins__Product2Id__c).Name;
            String productReqName = pr.Name;

            prProductNames.add(productName);
            prProductRequirementNames.add(productReqName);

            if (!prReqByProductIdAndReqName.containsKey(productName)) {
                prReqByProductIdAndReqName.put(
                        productName,
                        new Map<String, vlocity_ins__ProductRequirement__c>{
                                productReqName => pr
                        }
                );
            } else {
                Map<String, vlocity_ins__ProductRequirement__c> tmpMap = prReqByProductIdAndReqName.get(productName);
                tmpMap.put(productReqName, pr);

                prReqByProductIdAndReqName.put(
                        productName,
                        tmpMap
                );
            }
        }
        
        Database.executeBatch(new EWProductRequirementBatch(prProductNames, prProductRequirementNames, prReqByProductIdAndReqName), 2000);
    }
}