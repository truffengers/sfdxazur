/**
* @name: EWReviewDeadlineCheckBatch
* @description: Batch class which queries all quotes in Hold on Cover status without PCL agreement id and EW_IsDDDisabled__c = false.SELECT
* Following possible scenarios exist in execute method of this batch:
* 1. Review Deadline date on the record is in the past. In this case record EW_IsDDDisabled__c flag will be updated to true and Status to Quoted. No further handling for such records.
* 2. Review Deadline date on the record is in the future. In this case execute method makes a callout to the external system using EW_PCLAgreementCallout integration procedure. 
* In this case we have 2 possible subscenarios:
* a) PCL Reference is received in a callout response. We update EW_PCLAgreementId__c on the record with a received value and send a notification email to the underwriter.
* b) PCL Reference is not received in a callout response. No further handling for such quotes. 
* @author: Aleksejs Jedamenko ajedamenko@azuruw.com
* @date: 30/09/2020
*
*/
public class EWReviewDeadlineCheckBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {

    /**
    * @methodName: start
    * @description: Batch class start method, compose a query locator which fetches all quotes in Hold on Cover status without PCL agreement id and EW_IsDDDisabled__c = false.SELECT
    * @dateCreated: 30/09/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    */
public Database.QueryLocator start(Database.BatchableContext bc) {

    String queryString = 'SELECT Id, AccountId, Status, EW_IsDDDisabled__c, EW_PCLAgreementId__c, EW_DDReviewDeadlineDate__c, EWQuoteDisplayNumber__c FROM Quote WHERE EW_IsDDDisabled__c = FALSE AND Status = \''+EWConstants.QUOTE_STATUS_HOLD_ON_COVER+'\' AND EW_PCLAgreementId__c = NULL';
    return Database.getQueryLocator(queryString);
}
    /**
    * @methodName: execute
    * @description: This method handles records according a scenarios you can find in a class description
    * @dateCreated: 30/09/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    */
public void execute(Database.BatchableContext bc, List<Quote> scope) {
    
    try{

        Date todayDate = Date.today();
        List<Quote> quotesToDisableDD = new List<Quote>();
        List<Quote> quotesToCheckInPCL = new List<Quote>();
        List<Quote> quotesWithPCLReference = new List<Quote>(); 
        Set<Id> accsToBeCleanedIds = new Set<Id>();
        for (Quote quoteUnderReview : scope) {
            
            // this condition block composes a list of quotes with Review Deadline Date in the past, changes their Status to Quoted and set EW_IsDDDisabled__c to true
            if (todayDate>quoteUnderReview.EW_DDReviewDeadlineDate__c) {
                
                quoteUnderReview.EW_IsDDDisabled__c = true;
                quoteUnderReview.Status = EWConstants.QUOTE_STATUS_QUOTED;
                quoteUnderReview.EW_DateMovedIn__c = null;
                quotesToDisableDD.add(quoteUnderReview);
                accsToBeCleanedIds.add(quoteUnderReview.AccountId);
                
            // this condition block composes a list of quotes with Review Deadline Date in the future, we will send their quote numbers to PCL external system to check, if PclReference(PCL agreement Id) was asigned.
            } else {
                
                quotesToCheckInPCL.add(quoteUnderReview);
            }
        }
        // sad, but every quote needs a separate callout, it means that a size of batch shouldn't be more than 50
        for (Quote quoteToCheck : quotesToCheckInPCL ){

            Map<String, Object> inputMap = new Map<String, Object>();
            inputMap.put('quoteId',quoteToCheck.Id);
            inputMap.put('calloutType','PCLReviewCheck');
            inputMap.put('clientReference',quoteToCheck.EWQuoteDisplayNumber__c);
            Object getIPResult = vlocity_ins.IntegrationProcedureService.runIntegrationService('EW_PCLAgreementCallout', inputMap, new Map<String, Object>{'isDebug' => false});
            Map<String,Object> responseJSON = (Map<String,Object>) getIPResult;
            if(Test.isRunningTest()){
                
                String testResponseString = '{"PCLReviewCheckResponse": {"AgreementHeader":{"PclReference":"TEST11"}}}';
                responseJSON = (Map<String, Object>) JSON.deserializeUntyped(testResponseString);
            }
            if (!responseJSON.containsKey('PCLReviewCheckResponse')) {

                continue;
            } else {

                Map<String,Object> pclReviewCheckResponse = (Map<String,Object>) responseJSON.get('PCLReviewCheckResponse');
                if (!pclReviewCheckResponse.containsKey('AgreementHeader')) {

                    continue;
                } else {

                    Map<String,Object> agreementHeader = (Map<String,Object>) pclReviewCheckResponse.get('AgreementHeader');
                    if (!agreementHeader.containsKey('PclReference')) {

                        continue;
                    } else {

                        // if we received a PclReference(PCL agreement Id) as a response, we should populate a field EW_PCLAgreementId__c on the quote with this value
                        quoteToCheck.EW_PCLAgreementId__c = (String) agreementHeader.get('PclReference');
                        quotesWithPCLReference.add(quoteToCheck);
                    }
                }
            }
        }
        
        if (quotesToDisableDD.size()>0) {
            
            update quotesToDisableDD;
            // if Direct Debit option is declined, sensitive data should be cleaned on parent accounts 
            removeSensitiveDataFromAccs(accsToBeCleanedIds);
        }
        // An underwriter will be notified about every quote in a status Hold on Cover, which wasn't accepted by PCL in ten days 
        for (Quote quoteWithoutPCLReference : quotesToDisableDD) {

            sendNotificationEmailToUW(quoteWithoutPCLReference.Id, EWConstants.EMAIL_TEMPLATE_NAME_UWDDQUEUEDQUOTEDECLINED);
        }
        if(quotesWithPCLReference.size() > 0) {

            update quotesWithPCLReference;
        }
        // An underwriter will be notified about every quote in a status Hold on Cover, which received PclReference(PCL agreement Id) from the external system
        for (Quote quoteWithPCLReference : quotesWithPCLReference) {

            sendNotificationEmailToUW(quoteWithPCLReference.Id, EWConstants.EMAIL_TEMPLATE_NAME_UWDDQUEUEDQUOTEAPPROVED);
        }
    } catch (Exception ex) {

        LogBuilder logBuilder = new LogBuilder();
        Azur_Log__c log = logBuilder.createGenericLog(ex);
        log.Class__c = 'EWReviewDeadlineCheckBatch';
        log.Method__c = 'execute';
        log.Log_message__c = log.Log_message__c
                + '\n\n method name = EWReviewDeadlineCheckBatch.execute'
                + '\n\n ln: ' + ex.getLineNumber()
                + '\n\n st: ' + ex.getStackTraceString();
        System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
        insert log;
        throw ex;
    }

}
    /**
    * @methodName: finish
    * @description: Sends a message regarding batch job results to users mentioned in ApexEmailNotification object
    * @dateCreated: 30/09/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    */
public void finish(Database.BatchableContext bc) {

    try {

        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, TotalJobItems FROM AsyncApexJob where Id =:bc.getJobId()]; 
        List<ApexEmailNotification> emailNotifications = [SELECT ID, User.Email FROM ApexEmailNotification];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> toAddresses = new List<String> ();   
        for (ApexEmailNotification emailNotification : emailNotifications) {

            toAddresses.add(emailNotification.User.Email);
        }
        mail.setToAddresses(toAddresses);
        mail.setSubject('EWAutomatic Renewal Batch ' + a.Status);
        mail.setPlainTextBody('Number of processed batches: ' + a.TotalJobItems +
       '\n  Number of failed batches: '+ a.NumberOfErrors + '\n\n Detailed information about failures can be found in Azur Log records');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    } catch (Exception ex) {

        LogBuilder logBuilder = new LogBuilder();
        Azur_Log__c log = logBuilder.createGenericLog(ex);
        log.Class__c = 'EWReviewDeadlineCheckBatch';
        log.Method__c = 'finish';
        log.Log_message__c = log.Log_message__c
                + '\n\n method name = EWReviewDeadlineCheckBatch.finish'
                + '\n\n ln: ' + ex.getLineNumber()
                + '\n\n st: ' + ex.getStackTraceString();
        System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
        insert log;
        throw ex;
    }
}
    /**
    * @methodName: sendNotificationEmailToUW
    * @description: Helper method which sends an email message about a new quote with pclReference(PCL agreement Id). UsesEW_UWDDQUEUEDQUOTEAPPROVED email template.
    * @dateCreated: 30/09/2020
    * @parameters: String quoteId - quote Id which is used as whatId inside email template to merge values
    */
private void sendNotificationEmailToUW (String quoteId, String emailTemplate) {

    Map<String, Object> emailOptionMap = new Map<String, Object>();
    EWVlocityRemoteActionHelper remoteHelper = new EWVlocityRemoteActionHelper();
    emailOptionMap.put('saveAsActivity', false);
    emailOptionMap.put('templateName', emailTemplate);
    emailOptionMap.put('whatId', quoteId);
    EW_Global_Setting__mdt defaultGlobalSetting = [SELECT Id, EW_UWDefaultEmailAddress__c FROM EW_Global_Setting__mdt WHERE DeveloperName = 'Default' LIMIT 1];
    emailOptionMap.put('toAddress',defaultGlobalSetting.EW_UWDefaultEmailAddress__c);
    remoteHelper.invokeMethod('sendEmail',new Map<String, Object>(), new Map<String, Object>(), emailOptionMap);
}

    /**
    * @methodName: removeSensitiveDataFromAccs
    * @description: helper method to remove a sensitive data from accounts
    * @dateCreated: 02/10/2020
    * @parameters: Set<Id> accountIds - ids of accounts which should be cleaned from a sensitive data: phone, email, bamk account and sort code
    */
private void removeSensitiveDataFromAccs(Set<Id> accountIds) {

    if (accountIds.size()==0) {

        return;
    }
    List<Account> accsToBeCleaned = [SELECT Id, EW_PhoneForDDRequest__c, EW_EmailForDDRequest__c, EW_BankAccountForDDRequest__c, EW_SortCodeForDDRequest__c FROM Account WHERE ID in :accountIds];
    for(Account acc : accsToBeCleaned) {

        acc.EW_PhoneForDDRequest__c = null;
        acc.EW_EmailForDDRequest__c = null;
        acc.EW_BankAccountForDDRequest__c = null;
        acc.EW_SortCodeForDDRequest__c = null;
    }
    update accsToBeCleaned;
}

}