public virtual class XmlRepresentableMap implements Comparable {

    public interface StringToObject {
        Object convert(String s);
    }    
    
    public class IntegerParser implements StringToObject {
        public Object convert(String s) {
            return s == '' ? null : Integer.valueOf(s);
        }
    }
    
    public class DateTimeParser implements StringToObject {
        public Object convert(String s) {
            if(s == null) {
                return null;
            } else {
            	return JSON.deserializeStrict('"' + s + '"', DateTime.class);
            }
        }
    }
    
	protected Map<String, Object> data;
    protected Map<String, StringToObject> fieldNameToConvertors;
	protected List<String> fields;
    private String sortOnField;
	private String enclosingTag;
    
    public void setSortOnField(String f) {
        this.sortOnField = f;
    }
    
    protected XmlRepresentableMap(List<String> fields, String enclosingTag) {
        this(fields, enclosingTag, new Map<String, StringToObject>());
    }
	protected XmlRepresentableMap(List<String> fields, String enclosingTag, Map<String, StringToObject> fieldNameToConvertors) {
		this.data = new Map<String, Object>();
		this.fields = fields;
        this.enclosingTag = enclosingTag;
        this.fieldNameToConvertors = fieldNameToConvertors;
	}
	
    public void load(Dom.XmlNode node) {
        for(String f : fields) {
            String stringVal = getValueString(f, node);
            StringToObject convertor = fieldNameToConvertors.get(f);
            if(convertor != null) {
                data.put(f, convertor.convert(stringVal));
            } else {
	            data.put(f, stringVal);
            }
        }        
	}
    
    private String getValueString(String f, Dom.XmlNode node) {
        
        List<String> fieldByNodePath = f.split('->');
        
        if(fieldByNodePath.size() == 1) {
	        List<String> fieldByAttribute = f.split('\\.');
    	    Dom.XmlNode valNode = node.getChildElement(fieldByAttribute[0], null);

            if(valNode == null) {
                return null;
            } else if(fieldByAttribute.size() == 1) {
                return valNode.getText();
            } else if(fieldByAttribute.size() == 2){
                return valNode.getAttribute(fieldByAttribute[1], null);
            } else {
                throw new XmlRepresentableException('Invalid field "' + f + '"');
            }
        } else {
            return getValueString(fieldByNodePath[1], node.getChildElement(fieldByNodePath[0], null));
        }
    }
	
	public String toXml(Boolean includeHeader) {
        String rval =  includeHeader ? '<?xml version="1.0" encoding="utf-8"?>' : '';

		rval += '<' + enclosingTag + '>';

        //TODO: Use f.attr notation to set attributes on the field
		for(String f : fields) {
			Object val = data.get(f);
            String valString;
			if(val == null) {
				valString = '';
            } else {
                valString = '' + val;
            }
			if(data.containsKey(f)) {
				rval += '<' + f + '>' + valString.escapeXml() + '</' + f + '>';
			}
		}
        
        rval += '</' + enclosingTag + '>';
        return rval;		
	}
    
    public Integer compareTo(Object compareTo) {
    	XmlRepresentableMap other = (XmlRepresentableMap)compareTo;
        Object thisVal = data.get(sortOnField);
        Object otherVal = other.data.get(sortOnField);
        
        if(thisVal instanceOf Comparable) {
        	return ((Comparable)thisVal).compareTo(otherVal);
        } else if (thisVal instanceof DateTime) {
            DateTime thisDt = (DateTime)thisVal;
            DateTime otherDt = (DateTime)otherVal;
            Long result = thisDt.getTime() - otherDt.getTime();
            
            // Don't return a long cast down to Integer or it might overflow
            // and mix up the sign
            return result > 0 ? 1 : (result < 0 ? -1 : 0);
        }  else if (thisVal instanceof Integer) {
            Integer thisInt = (Integer)thisVal;
            Integer otherInt = (Integer)otherVal;
            
            return thisInt - otherInt;
        } else {
            throw new XmlRepresentableException('Cannot compare incomparable field "' + sortOnField + '"');
        }
    }
    
    public Object getData(String key) {
        return data.get(key);
    }
}