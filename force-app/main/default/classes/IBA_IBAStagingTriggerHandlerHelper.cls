/**
* @name: IBA_IBAStagingTriggerHandlerHelper
* @description: IBA_IBAStagingTriggerHandler helper class
*
*/

public with sharing class IBA_IBAStagingTriggerHandlerHelper {
    
    /**
    * 1. Select only those records that should be processed
    * 2. Query matching Accounts for these selected records
    * 3. Query matching Products for these selected records
    * 4. Populate account and product lookup fields on selected records
    */
    public static void matchFields(List<SObject> newItems) {
        Set<String> originalCurrencyCodes = new Set<String>();
        Set<String> accountCodes = new Set<String>();
        Set<String> productSeeds = new Set<String>();
        List<IBA_IBAStaging__c> selectedStagingRecords = selectStagingRecords((List<IBA_IBAStaging__c>)newItems, 
            originalCurrencyCodes, accountCodes, productSeeds);
        List<Account> matchingAccounts = findAccounts(accountCodes, originalCurrencyCodes);
        List<Product2> matchingProducts = findProducts(productSeeds);
        populateFields(selectedStagingRecords, matchingAccounts, matchingProducts);
    }

    private static List<IBA_IBAStaging__c> selectStagingRecords(List<IBA_IBAStaging__c> newStagingRecords, 
        Set<String> originalCurrencyCodes, Set<String> accountCodes, Set<String> productSeeds) {
            List<IBA_IBAStaging__c> selectedStagingRecords = new List<IBA_IBAStaging__c>();
            for(IBA_IBAStaging__c staging : newStagingRecords) {
                Boolean shouldBeProcessed = false;
                if(String.isNotBlank(staging.IBA_ProducerBrokerCode__c) 
                    && String.isNotBlank(staging.IBA_OriginalCurrencyCode__c)
                    && String.isBlank(staging.IBA_ProducingBroker__c)) {
                        shouldBeProcessed = true;
                        accountCodes.add(staging.IBA_ProducerBrokerCode__c);
                        originalCurrencyCodes.add(staging.IBA_OriginalCurrencyCode__c);
                }
                if(String.isNotBlank(staging.IBA_CarrierCode__c)
                    && String.isNotBlank(staging.IBA_OriginalCurrencyCode__c)
                    && String.isBlank(staging.IBA_CarrierAccount__c)) {
                        shouldBeProcessed = true;
                        accountCodes.add(staging.IBA_CarrierCode__c);
                        originalCurrencyCodes.add(staging.IBA_OriginalCurrencyCode__c);
                }
                if(String.isNotBlank(staging.IBA_OriginalCurrencyCode__c)
                    && String.isBlank(staging.IBA_MGAAccount__c)) {
                        shouldBeProcessed = true;
                        accountCodes.add(staging.IBA_MGACode__c);
                        originalCurrencyCodes.add(staging.IBA_OriginalCurrencyCode__c);
                }
                if(String.isNotBlank(staging.IBA_AccountBrokerCode__c)
                    && String.isNotBlank(staging.IBA_OriginalCurrencyCode__c)
                    && String.isBlank(staging.IBA_AccountingBroker__c)) {
                        shouldBeProcessed = true;
                        accountCodes.add(staging.IBA_AccountBrokerCode__c);
                        originalCurrencyCodes.add(staging.IBA_OriginalCurrencyCode__c);
                }
                String aigProductSeed = getUniqueProductCode(staging);
                if(String.isNotBlank(aigProductSeed)
                    && String.isBlank(staging.IBA_Product__c)) {
                        shouldBeProcessed = true;
                        productSeeds.add(aigProductSeed);
                }
                if(shouldBeProcessed) {
                    selectedStagingRecords.add(staging);
                }
            }
            return selectedStagingRecords;
    }
    private static String getUniqueProductCode(IBA_IBAStaging__c staging) {
        String policyNumber = '';
        if(String.isNotBlank(staging.IBA_AIGPolicyNumber__c)) {
            if(staging.IBA_AIGProductName__c == IBA_UtilConst.PRODUCT_NAME_CLASSIC_CAR || 
                staging.IBA_AIGProductName__c == IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY) {
                policyNumber = staging.IBA_AIGPolicyNumber__c.left(6);
            } else {
                if(staging.IBA_AIGPolicyNumber__c.left(4) == IBA_UtilConst.AIG_POLICY_SPECIAL_NUMBER2) {
                    policyNumber = IBA_UtilConst.AIG_POLICY_SPECIAL_NUMBER1;
                } else {
                    policyNumber = staging.IBA_AIGPolicyNumber__c.left(4);
                }
                
            }
        }
        return staging.IBA_AIGProductName__c + staging.IBA_MinorLine__c + policyNumber;
    }

    private static List<Product2> findProducts(Set<String> productSeeds) {
        return [SELECT Id, IBA_AIGProductSeed__c FROM Product2 WHERE IBA_AIGProductSeed__c IN : productSeeds];
    }

    private static List<Account> findAccounts(Set<String> accountCodes, Set<String> originalCurrencyCodes) {
        return [SELECT Id, EW_DRCode__c, c2g__CODAAccountTradingCurrency__c FROM Account 
            WHERE EW_DRCode__c IN :accountCodes AND c2g__CODAAccountTradingCurrency__c IN :originalCurrencyCodes];
    }

    private static void populateFields(List<IBA_IBAStaging__c> selectedStagingRecords, List<Account> matchingAccounts,
        List<Product2> matchingProducts) {
            for(IBA_IBAStaging__c staging : selectedStagingRecords) {
                for(Account acc : matchingAccounts) {
                    if(acc.EW_DRCode__c == staging.IBA_ProducerBrokerCode__c 
                        && acc.c2g__CODAAccountTradingCurrency__c == staging.IBA_OriginalCurrencyCode__c) {
                            staging.IBA_ProducingBroker__c = acc.Id;
                    }
                    if(acc.EW_DRCode__c == staging.IBA_CarrierCode__c 
                        && acc.c2g__CODAAccountTradingCurrency__c == staging.IBA_OriginalCurrencyCode__c) {
                            staging.IBA_CarrierAccount__c = acc.Id;
                    }
                    if(acc.EW_DRCode__c == staging.IBA_MGACode__c
                        && acc.c2g__CODAAccountTradingCurrency__c == staging.IBA_OriginalCurrencyCode__c) {
                            staging.IBA_MGAAccount__c = acc.Id;
                    }
                    if(acc.EW_DRCode__c == staging.IBA_AccountBrokerCode__c
                        && acc.c2g__CODAAccountTradingCurrency__c == staging.IBA_OriginalCurrencyCode__c) {
                            staging.IBA_AccountingBroker__c = acc.Id;
                    }
                }
                String aigProductSeed = getUniqueProductCode(staging);
                for(Product2 p : matchingProducts) {
                    if(p.IBA_AIGProductSeed__c == aigProductSeed) {
                        staging.IBA_Product__c = p.Id; 
                    }
                }
            }
    }
}