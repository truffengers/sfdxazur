public class PardotResponse {
    public class Attributes {
        public String stat;
        public integer version;
        public String err_code;
    }
    
    public Attributes attributes;
    public String api_key;
    public String err;
}