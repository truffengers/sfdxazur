@isTest
private class TopicAssignmentIqContentTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();
	private static IQ_Content__c iqc;
	private static TopicAssignment testTopicAssignment;
	private static Topic testTopic;
	
	static {
		testTopic = new Topic(Name = 'Not really a topic 999');

		insert testTopic;
	}
	@isTest static void basic() {
		iqc = tog.getWebcastIqContent();

		testTopicAssignment = new TopicAssignment(EntityId = iqc.Id, TopicId = testTopic.Id);
		
		insert testTopicAssignment;

		List<TopicAssignment> topicAssignments = [SELECT Id, Topic.Name, NetworkId 
													FROM TopicAssignment
													WHERE EntityId = :iqc.Id];

		List<Network> networks = [SELECT Id FROM Network];

		System.assertEquals(networks.size() + 1, topicAssignments.size());
	}

	@isTest static void removal() {
		basic();

		delete testTopicAssignment;

		List<TopicAssignment> topicAssignments = [SELECT Id, Topic.Name, NetworkId 
													FROM TopicAssignment
													WHERE EntityId = :iqc.Id];

		System.assertEquals(0, topicAssignments.size());
	}

	@isTest static void notInteresting() {
		Contact c = tog.getContact();

		testTopicAssignment = new TopicAssignment(EntityId = c.Id, TopicId = testTopic.Id);
		
		insert testTopicAssignment;

		List<TopicAssignment> topicAssignments = [SELECT Id, Topic.Name, NetworkId 
													FROM TopicAssignment
													WHERE EntityId = :c.Id];

		System.assertEquals(1, topicAssignments.size());

		delete testTopicAssignment;

	}
	
}