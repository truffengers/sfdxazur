/**
* @name: IBA_PolicyTransaction
* @description: Trigger for the custom object IBA_PolicyTransaction__c
*
*/

trigger IBA_PolicyTransaction on IBA_PolicyTransaction__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_PolicyTransactionTriggerHandler());
}