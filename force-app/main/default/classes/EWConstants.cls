/**
* @name: EWConstants
* @description: Magic string holder
* @author: Steve Loftus sloftus@azuruw.com
* @date: 04/09/2018
*/
public with sharing class EWConstants {
    public EWConstants() {

    }
    // currencies
    public static final String CURRENCY_ISO_CODE_GBP = 'GBP';
    public static final Integer CURRENCY_DECIMAL_PLACES = 2;

    public static final String ORG_WIDE_EMAIL_NAME = 'Azur Smart Home';
    public static final String EMAIL_TEMPLATE_EXPIRED_RENEWAL_QUOTES = 'Claim Added To Policy With Pending Renewal Internal Notification';
    public static final String EMAIL_TEMPLATE_NAME_BROKERQUOTE = 'EW_BROKERQUOTE';
    public static final String EMAIL_TEMPLATE_NAME_BROKERRENEWALQUOTE = 'EW_BROKERRENEWALQUOTE';
    public static final String EMAIL_TEMPLATE_NAME_BROKERRENEWALQUOTEREMINDER = 'EW_BROKERRENEWALQUOTEREMINDER';
    public static final String EMAIL_TEMPLATE_NAME_UWAPPROVEDQUOTE = 'EW_UWApprovedEmail';
    public static final String EMAIL_TEMPLATE_NAME_UWDDQUEUEDQUOTEAPPROVED = 'EW_UWDDQUEUEDQUOTEAPPROVED';
    public static final String EMAIL_TEMPLATE_NAME_UWDDQUEUEDQUOTEDECLINED = 'EW_UWDDQUEUEDQUOTEDECLINED';
    public static final String PROFILE_BROKERHUB_GUEST_PROFILE_NAME = 'BrokerHub Profile';
    public static final String PROFILE_SYSTEMADMINISTRATOR_NAME = 'System Administrator';
    public static final String PROFILE_AZURUNDERWRITER_NAME = 'Azur UW Manager';
    public static final String PROFILE_EW_PARTNER_NAME = 'EW Vlocity Partner Community User';
    public static final String PROFILE_AZURCLAIMSUSER_NAME = 'Azur Claims User';
    public static final String ROLE_TECHTEAM_NAME = 'Tech_team';

    public static final String ACCOUNT_BROKER_RECORD_TYPE = 'Agency/Brokerage';
    public static final String ACCOUNT_PERSON_RECORD_TYPE = 'Person Account';
    public static final String ACCOUNT_PERSON_RECORD_TYPE_DEVNAME = 'PersonAccount';

    public static final String OPPORTUNITY_EW_RECORD_TYPE = 'EW Opportunity';
    public static final String OPPORTUNITY_STAGE_CLOSED_WON = 'Closed Won';
    public static final String OPPORTUNITY_STAGE_CLOSED_LOST = 'Closed Lost';

    public static final String QUOTE_EW_RECORD_TYPE = 'EW Quote';
    public static final String QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE = 'New Business';
    public static final String QUOTE_TRANSACTION_TYPE_CANCELLATION = 'Cancellation';
    public static final String QUOTE_TRANSACTION_TYPE_MTA = 'MTA';
    public static final String QUOTE_TRANSACTION_TYPE_RENEWAL = 'Renewal';
    public static final String QUOTE_STATUS_ISSUED = 'Issued';
    public static final String QUOTE_STATUS_APPROVED = 'Approved';
    public static final String QUOTE_STATUS_UW_APPROVED = 'UW Approved';
    public static final String QUOTE_STATUS_UW_REFERRED = 'UW Referred';
    public static final String QUOTE_STATUS_QUOTED = 'Quoted';
    public static final String QUOTE_STATUS_REVERSED = 'Reversed';
    public static final String QUOTE_STATUS_REVERSAL = 'Reversal';
    public static final String QUOTE_STATUS_NTU = 'NTU';
    public static final String QUOTE_STATUS_EXPIRED = 'Expired';
    public static final String QUOTE_STATUS_BOUND = 'Bound';
    public static final String QUOTE_STATUS_BROKER_REFERRED = 'Broker Referred';
    public static final String QUOTE_STATUS_HOLD_ON_COVER = 'Hold on Cover';
    public static final String POLICY_EW_RECORD_TYPE = 'EW Policy';
    public static final String POLICY_EW_RECORD_TYPE_DEVNAME = 'EW_Policy';
    public static final String POLICY_TRANSACTION_TYPE_NB = 'New Business';
    public static final String POLICY_TRANSACTION_TYPE_MTA = 'MTA';
    public static final String POLICY_TRANSACTION_TYPE_RENEWAL = 'Renewal';
    public static final String POLICY_TRANSACTION_TYPE_CANCELLATION = 'Cancellation';
    public static final String POLICY_COVERAGE_IBA_STATUS_ACTIVE = 'Active';
    public static final String EW_POLICY_STATUS_DRAFT = 'Draft';
    public static final String EW_POLICY_STATUS_ISSUED = 'Issued';
    public static final String EW_POLICY_STATUS_NON_ACTIVE = 'Non Active';
    public static final String EW_POLICY_STATUS_ADJUSTED = 'Adjusted';
    public static final String EW_POLICY_STATUS_REVERSAL = 'Reversal';
    public static final String EW_POLICY_STATUS_REVERSED = 'Reversed';
    public static final String EW_POLICY_STATUS_CANCELLED = 'Cancelled';
    public static final String EW_POLICY_STATUS_PENDING_CANCELLATION = 'Pending Cancellation';
    public static final String EW_POLICY_STATUS_LAPSED = 'Lapsed';

    public static final String POLICY_TRANSACTION_STATUS_CANCELLATION = 'Cancellation';
    public static final String POLICY_TRANSACTION_STATUS_NO_DATA_PROCESSED = 'No Data Processed';
    public static final String POLICY_TRANSACTION_STATUS_PROCESS_COMPLETE = 'Process Complete';

    public static final String DEFAULT_AIG_CLASS_FOR_BUSINESS_FOR_FF_TRANSACTION_RECORD = 'Azur Emerging Wealth';
    public static final String FIXED_EXPENSE_PRODUCT_NAME = 'Fixed Expense';

    public static final String DIRECT_DEBIT_PAYMENT_METHOD = 'Direct Debit';
    //Added by Rajni Bajpai
    public static final String MGA_FEE_ACCOUNT_DR_CODE = 'MG0002';
    // vlocity remote methods
    public static final String DOCUMENT_CUSTOM_SECTION_METHOD = 'buildDocumentSectionContent';
    public static final String VLOCITY_ATTRIBUTE_SEL_VALS_JSON_FIELD_API_NAME = 'vlocity_ins__AttributeSelectedValues__c';

    // Insurable Items
    public static final String INSURABLE_ITEM_COVER_TYPE_PROPERTY = 'Property';
    public static final String INSURABLE_ITEM_COVER_TYPE_SPECIFIED_ITEM = 'Specified Item';
    public static final String INSURABLE_ITEM_RT_DEV_NAME_PROPERTY = 'EW_Property';
    public static final String INSURABLE_ITEM_RT_DEV_NAME_SPECIFIED_ITEM = 'EW_SpecifiedItem';
    public static final Map<String, String> SPECIFIED_ITEM_TYPE_PRODUCT_NAME_MAPPING = new Map<String, String>{
            'contents' => 'Contents',
            'jewelleryAndWatches' => 'Jewellery and Watches',
            'art' => 'Art and Collectables',
            'pedalCycles' => 'Pedal Cycles'
    };

    // Insured Items
    public static final String INSURED_ITEM_COVER_TYPE_PROPERTY = 'Property';
    public static final String INSURED_ITEM_COVER_TYPE_SPECIFIED_ITEM = 'Specified Item';

    // Document Clauses
    public static final String DOCUMENT_CLAUSE_CATEGORY_STANDARD = 'Standard';
    public static final String DOCUMENT_CLAUSE_CATEGORY_RESTRICTED = 'Restricted';
    
    // Document Templates
    public static final String DOCUMENT_TEMPLATE_POLICY = 'Emerging Wealth Policy Document';
    public static final String DOCUMENT_TEMPLATE_QUOTE = 'Emerging Wealth Quote Sheet';

    // Record Type Lists
    public static final String ACCOUNT_RECORD_TYPE_AGENCY_BROKERAGE = 'AgencyBrokerage';
    public static final String ACCOUNT_RECORD_TYPE_BUSINESS_ACCOUNT = 'Business_Account';
    public static final String ACCOUNT_RECORD_TYPE_CARRIER = 'Carrier';
    public static final String ACCOUNT_RECORD_TYPE_GROUP = 'Group';
    public static final String ACCOUNT_RECORD_TYPE_MOBILE_PHONE_ACCOUNT = 'MobilePhoneAccount';
    public static final String ACCOUNT_RECORD_TYPE_PERSON_ACCOUNT = 'PersonAccount';
    public static final String ACCOUNT_RECORD_TYPE_PHARMACY = 'Pharmacy';
    public static final String ACCOUNT_RECORD_TYPE_PROVIDER = 'Provider';
    public static final String ACCOUNT_RECORD_TYPE_STANDARD_ACCOUNT = 'StandardAccount';
    public static final String ACCOUNT_RECORD_TYPE_SUPPLIER = 'Supplier';

    public static final String CONTACT_RECORD_TYPE_PROVIDER = 'Provider';
    public static final String CONTACT_RECORD_TYPE_CONTACT = 'Contact';
    public static final String CONTACT_RECORD_TYPE_MOBILE_PHONE_CONTACT = 'MobilePhoneContact';
    public static final String CONTACT_RECORD_TYPE_PRODUCER = 'Producer';

    public static final String HOUSEHOLD_RECORD_TYPE_GROUP = 'Group';
    public static final String HOUSEHOLD_RECORD_TYPE_HOUSEHOLD = 'Household';

    public static final String INSURABLE_ITEM_RECORD_TYPE_EW_PROPERTY = 'EW_Property';
    public static final String INSURABLE_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM = 'EW_SpecifiedItem';

    public static final String INSURED_ITEM_RECORD_TYPE_EW_PROPERTY = 'EW_Property';
    public static final String INSURED_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM = 'EW_SpecifiedItem';

    public static final String OPPORTUNITY_RECORD_TYPE_EW_OPPORTUNITY = 'EW_Opportunity';
    public static final String OPPORTUNITY_RECORD_TYPE_MOBILE_PHONE_OPPORTUNITY = 'MobilePhoneOpportunity';

    public static final String PICKLIST_SECTION_RECORD_TYPE_BUSINESS_TYPE = 'Business_Type';
    public static final String PICKLIST_SECTION_RECORD_TYPE_OCCUPATION = 'Occupation';
    public static final String PICKLIST_SECTION_RECORD_TYPE_TITLE = 'Title';
    public static final String PICKLIST_SECTION_RECORD_TYPE_NATIONALITY = 'Nationality';

    public static final String PRODUCT2_PRODUCT_CODE_EMERGING_WEALTH = 'emergingWealth';
    public static final String PRODUCT2_RECORD_TYPE_COVERAGE_SPEC = 'CoverageSpec';
    public static final String PRODUCT2_RECORD_TYPE_INSURED_ITEM_SPEC = 'InsuredItemSpec';
    public static final String PRODUCT2_RECORD_TYPE_STANDARD = 'Standard';
    public static final String PRODUCT2_RECORD_TYPE_INSURED_PARTY_SPEC = 'InsuredPartySpec';
    public static final String PRODUCT2_RECORD_TYPE_PRODUCT_MASTER = 'Product_Master';
    public static final String PRODUCT2_RECORD_TYPE_PRODUCT_COVERAGE = 'Product_Coverage';
    public static final String PRODUCT2_RECORD_TYPE_PRODUCT_TEMPLATE = 'ProductTemplate';
    public static final String PRODUCT2_RECORD_TYPE_PRODUCT = 'Product';
    public static final String PRODUCT2_RECORD_TYPE_CLASS = 'Class';
    public static final String PRODUCT2_RECORD_TYPE_CLAIM_INJURY_SPEC = 'ClaimInjurySpec';
    public static final String PRODUCT2_RECORD_TYPE_CLAIM_PRODUCT = 'ClaimProduct';
    public static final String PRODUCT2_RECORD_TYPE_CLAIM_PROPERTY_SPEC = 'ClaimPropertySpec';
    public static final String PRODUCT2_RECORD_TYPE_RATING_FACT_SPEC = 'RatingFactSpec';

    public static final String QUOTE_RECORD_TYPE_EW_QUOTE = 'EW_Quote';

    public static final String CLAIM_RECORD_TYPE_EW_QUOTE = 'EW_Quote';

    public static final String CASE_RECORD_TYPE_COMMUNITY = 'Community';
    public static final List<String> ACCOUNT_RECORD_TYPES =
            new List<String>{
                    ACCOUNT_RECORD_TYPE_AGENCY_BROKERAGE,
                    ACCOUNT_RECORD_TYPE_BUSINESS_ACCOUNT,
                    ACCOUNT_RECORD_TYPE_CARRIER,
                    ACCOUNT_RECORD_TYPE_GROUP,
                    ACCOUNT_RECORD_TYPE_MOBILE_PHONE_ACCOUNT,
                    ACCOUNT_RECORD_TYPE_PERSON_ACCOUNT,
                    ACCOUNT_RECORD_TYPE_PHARMACY,
                    ACCOUNT_RECORD_TYPE_PROVIDER,
                    ACCOUNT_RECORD_TYPE_STANDARD_ACCOUNT,
                    ACCOUNT_RECORD_TYPE_SUPPLIER
            };

    public static final List<String> CONTACT_RECORD_TYPES =
            new List<String>{
                    CONTACT_RECORD_TYPE_PROVIDER,
                    CONTACT_RECORD_TYPE_CONTACT,
                    CONTACT_RECORD_TYPE_MOBILE_PHONE_CONTACT,
                    CONTACT_RECORD_TYPE_PRODUCER
            };

    public static final List<String> HOUSEHOLD_RECORD_TYPES =
            new List<String>{
                    HOUSEHOLD_RECORD_TYPE_GROUP,
                    HOUSEHOLD_RECORD_TYPE_HOUSEHOLD
            };

    public static final List<String> INSURABLE_ITEM_RECORD_TYPES =
            new List<String>{
                    INSURABLE_ITEM_RECORD_TYPE_EW_PROPERTY,
                    INSURABLE_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM
            };

    public static final List<String> INSURED_ITEM_RECORD_TYPES =
            new List<String>{
                    INSURED_ITEM_RECORD_TYPE_EW_PROPERTY,
                    INSURED_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM
            };

    public static final List<String> OPPORTUNITY_RECORD_TYPES =
            new List<String>{
                    OPPORTUNITY_RECORD_TYPE_EW_OPPORTUNITY,
                    OPPORTUNITY_RECORD_TYPE_MOBILE_PHONE_OPPORTUNITY
            };

    public static final List<String> PICKLIST_SECTION_RECORD_TYPES =
            new List<String>{
                    PICKLIST_SECTION_RECORD_TYPE_BUSINESS_TYPE,
                    PICKLIST_SECTION_RECORD_TYPE_OCCUPATION,
                    PICKLIST_SECTION_RECORD_TYPE_TITLE,
                    PICKLIST_SECTION_RECORD_TYPE_NATIONALITY
            };

    public static final List<String> PRODUCT2_RECORD_TYPES =
            new List<String>{
                    PRODUCT2_RECORD_TYPE_COVERAGE_SPEC,
                    PRODUCT2_RECORD_TYPE_COVERAGE_SPEC,
                    PRODUCT2_RECORD_TYPE_INSURED_ITEM_SPEC,
                    PRODUCT2_RECORD_TYPE_STANDARD,
                    PRODUCT2_RECORD_TYPE_INSURED_PARTY_SPEC,
                    PRODUCT2_RECORD_TYPE_PRODUCT_MASTER,
                    PRODUCT2_RECORD_TYPE_PRODUCT_COVERAGE,
                    PRODUCT2_RECORD_TYPE_PRODUCT_TEMPLATE,
                    PRODUCT2_RECORD_TYPE_PRODUCT,
                    PRODUCT2_RECORD_TYPE_CLASS,
                    PRODUCT2_RECORD_TYPE_CLAIM_INJURY_SPEC,
                    PRODUCT2_RECORD_TYPE_CLAIM_PRODUCT,
                    PRODUCT2_RECORD_TYPE_CLAIM_PROPERTY_SPEC,
                    PRODUCT2_RECORD_TYPE_RATING_FACT_SPEC
            };

    public static final List<String> QUOTE_RECORD_TYPES =
            new List<String>{
                    QUOTE_RECORD_TYPE_EW_QUOTE
            };

    public static final List<String> CLAIM_RECORD_TYPES =
            new List<String>{
                    CLAIM_RECORD_TYPE_EW_QUOTE
            };


    // Record Type Map
    public static final Map<String, List<String>> RECORD_TYPE_NAME_BY_OBJECT =
            new Map<String, List<String>>{
                    'Account' => ACCOUNT_RECORD_TYPES,
                    'Contact' => CONTACT_RECORD_TYPES,
                    'vlocity_ins__Household__c' => HOUSEHOLD_RECORD_TYPES,
                    'vlocity_ins__InsurableItem__c' => INSURABLE_ITEM_RECORD_TYPES,
                    'vlocity_ins__AssetInsuredItem__c' => INSURED_ITEM_RECORD_TYPES,
                    'vlocity_ins__InsuranceClaim__c' => CLAIM_RECORD_TYPES,
                    'Opportunity' => OPPORTUNITY_RECORD_TYPES,
                    'Picklist_Section__c' => PICKLIST_SECTION_RECORD_TYPES,
                    'Product2' => PRODUCT2_RECORD_TYPES,
                    'Quote' => QUOTE_RECORD_TYPES
            };

    public static final String FILE_UPLOAD_OPERATION_ADD = 'contentDocumentAdd';
    public static final String FILE_UPLOAD_OPERATION_DELETE = 'contentDocumentDelete';

    // Button Titles / Labels
    public static final String BUTTON_TITLE_MTA = 'MTA';
    public static final String BUTTON_TITLE_CANCEL = 'Cancel Policy';
    public static final String BUTTON_TITLE_NTU = 'Not Taken Up';
    public static final String BUTTON_TITLE_EDIT = 'Edit/Continue';
    public static final String BUTTON_TITLE_BIND = 'Bind';

    // EW Product Codes
    public static final String EW_PROD_CODE_BUILDINGS = 'buildings';
    public static final String EW_PROD_CODE_CONTENTS = 'contents';
    public static final String EW_PROD_CODE_ART = 'artAndCollectables';
    public static final String EW_PROD_CODE_JEWELLERY = 'jewelleryAndWatches';
    public static final String EW_PROD_CODE_PUBLIC_LIABILITY = 'publicLiability';
    public static final String EW_PROD_CODE_EMPLOYERS_LIABILITY = 'employersLiability';
    public static final String EW_PROD_CODE_TENANTS = 'tenantsImprovements';
    public static final String EW_PROD_CODE_PEDAL_CYCLE = 'pedalCycles';
    public static final String EW_PROD_CODE_PERSONAL_CYBER = 'personalCyber';
    public static final String EW_PROD_CODE_LEGAL_EXPENSES = 'legalExpenses';
    public static final String EW_PROD_CODE_HOME_EMERGANCY = 'homeEmergency';
    public static final String EW_PROD_NAME_EMERGING_WEALTH = 'Emerging Wealth';

    public static final String ATTR_AZUR_COMMISSION_VALUE = 'azurCommissionValue';
    public static final String ATTR_BROKER_COMMISSION_VALUE = 'brokerCommissionValue';
    public static final String ATTR_AIG_PREMIUM_GROSS_EX_IPT = 'aigPremiumGrossExIPT';
    public static final String ATTR_PREMIUM_GROSS_EX_IPT = 'premiumGrossExIPT';
    public static final String ATTR_PREMIUM_NET = 'premiumNet';
    public static final String ATTR_PREMIUM_GROSS_INC_IPT = 'premiumGrossIncIPT';
    public static final String ATTR_IPT_TOTAL = 'iptTotal';
    public static final String ATTR_BUILDINGS_LIMIT_TOTAL = 'buildingsLimitTotal';
    public static final String ATTR_OTHER_PERMANENT_STRUCTURES = 'otherPermanentStructures';
    public static final String ATTR_TENANTS_VALUE = 'tenantsValue';
    public static final String ATTR_CONTENTS_LIMIT_TOTAL = 'contentsLimitTotal';
    public static final String ATTR_ART_VALUE = 'artValue';
    public static final String ATTR_ART_SPECIFIED = 'artSpecified';
    public static final String ATTR_ART_UNSPECIFIED = 'artUnspecified';
    public static final String ATTR_JEWELLERY_VALUE = 'jewelleryValue';
    public static final String ATTR_JEWELLERY_SPECIFIED = 'jewellerySpecified';
    public static final String ATTR_JEWELLERY_UNSPECIFIED = 'jewelleryUnspecified';
    public static final String ATTR_PEDAL_VALUE = 'pedalValue';
    public static final String ATTR_CYCLES_SPECIFIED = 'cyclesSpecified';
    public static final String ATTR_CYCLES_UNSPECIFIED = 'cyclesUnspecified';
    public static final String ATTR_EMPLOYERS_LIABILITY = 'employersLiability';
    public static final String ATTR_PUBLIC_LIABILITY = 'publicLiability';
    public static final String ATTR_PERSONAL_CYBER_COVERAGE_LIMIT = 'personalCyberCoverageLimit';
    public static final String ATTR_LEGAL_EXPENSES_COVERAGE_LIMIT = 'legalExpensesCoverageLimit';
    public static final String ATTR_HOME_EMERGENCY_COVERAGE_LIMIT = 'homeEmergencyCoverageLimit';

    public static final String API_AzurCommissionValue = 'EW_AzurCommissionValue__c';
    public static final String API_BrokerCommissionValue = 'EW_BrokerCommissionValue__c';
    public static final String API_IPTTotal = 'EW_IPTTotal__c';
    public static final String API_PremiumGrossExIPT = 'EW_PremiumGrossExIPT__c';
    public static final String API_PremiumGrossIncIPT = 'EW_PremiumGrossIncIPT__c';
    public static final String API_PremiumNet = 'EW_PremiumNet__c';
    public static final String API_PermanentStructures = 'EW_PermanentStructures__c';
    public static final String API_SpecifiedItemValue = 'EW_SpecifiedItemValue__c';
    public static final String API_SpecifiedValue = 'EW_SpecifiedValue__c';
    public static final String API_UnspecifiedValue = 'EW_UnspecifiedValue__c';
    public static final String API_LimitTotal = 'EW_LimitTotal__c';

    public static final Map<String, String> ATTRIBUTE_SELECTED_VALUE_JSON_TO_API_MAPPINNG = new Map<String, String>{
            ATTR_AZUR_COMMISSION_VALUE => API_AzurCommissionValue,
            ATTR_BROKER_COMMISSION_VALUE => API_BrokerCommissionValue,
            ATTR_IPT_TOTAL => API_IPTTotal,
            ATTR_PREMIUM_GROSS_EX_IPT => API_PremiumGrossExIPT,
            ATTR_PREMIUM_GROSS_INC_IPT => API_PremiumGrossIncIPT,
            ATTR_PREMIUM_NET => API_PremiumNet,
            ATTR_BUILDINGS_LIMIT_TOTAL => API_LimitTotal,
            ATTR_OTHER_PERMANENT_STRUCTURES => API_PermanentStructures,
            ATTR_TENANTS_VALUE => API_SpecifiedItemValue,
            ATTR_CONTENTS_LIMIT_TOTAL => API_LimitTotal,
            ATTR_ART_VALUE => API_SpecifiedItemValue,
            ATTR_ART_SPECIFIED => API_SpecifiedValue,
            ATTR_ART_UNSPECIFIED => API_UnspecifiedValue,
            ATTR_JEWELLERY_VALUE => API_SpecifiedItemValue,
            ATTR_JEWELLERY_SPECIFIED => API_SpecifiedValue,
            ATTR_JEWELLERY_UNSPECIFIED => API_UnspecifiedValue,
            ATTR_PEDAL_VALUE => API_SpecifiedItemValue,
            ATTR_CYCLES_SPECIFIED => API_SpecifiedValue,
            ATTR_CYCLES_UNSPECIFIED => API_UnspecifiedValue,
            ATTR_EMPLOYERS_LIABILITY => API_SpecifiedItemValue,
            ATTR_PUBLIC_LIABILITY => API_SpecifiedItemValue,
            ATTR_PERSONAL_CYBER_COVERAGE_LIMIT => API_LimitTotal,
            ATTR_LEGAL_EXPENSES_COVERAGE_LIMIT => API_LimitTotal,
            ATTR_HOME_EMERGENCY_COVERAGE_LIMIT => API_LimitTotal
    };

}