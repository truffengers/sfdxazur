/**
 * Created by roystonlloyd on 2020-01-31.
 */
@isTest
public with sharing class EWDocumentClauseTriggerHandlerTest {


    static testMethod void testClauseVersionInsert(){

        List<vlocity_ins__DocumentClause__c> clauses = new List<vlocity_ins__DocumentClause__c>{
            EWDocumentClauseDataFactory.createDocumentClause('a','ContentA', true, false),
            EWDocumentClauseDataFactory.createDocumentClause('b[v1]','ContentB', true, false),
            EWDocumentClauseDataFactory.createDocumentClause('c','ContentC', true, false)
        };
        insert clauses;


        List<vlocity_ins__DocumentClause__c> clausesCheck = [SELECT Id, Name, EW_Version_Number__c FROM vlocity_ins__DocumentClause__c];
        for(vlocity_ins__DocumentClause__c c : clausesCheck){
            System.assertEquals(1, c.EW_Version_Number__c);
            System.assertEquals(true, c.Name.contains('v1'));
        }

        vlocity_ins__DocumentClause__c newClause = EWDocumentClauseDataFactory.createDocumentClause('a','ContentA', true, false);
        newClause.EW_Version_Number__c = 2;
        insert newClause;
        vlocity_ins__DocumentClause__c newClauseCheck = [SELECT Id, Name, EW_Version_Number__c FROM vlocity_ins__DocumentClause__c WHERE Id = :newClause.Id];
        System.assertEquals(2, newClauseCheck.EW_Version_Number__c);
        System.assertEquals(true, newClauseCheck.Name.contains('v2'));
    }


    static testMethod void testClauseVersionUpdate(){

        EWDocumentClauseTriggerHandler.IsDisabled = true;
        List<vlocity_ins__DocumentClause__c> clauses = new List<vlocity_ins__DocumentClause__c>{
                EWDocumentClauseDataFactory.createDocumentClause('a','ContentA', true, false),
                EWDocumentClauseDataFactory.createDocumentClause('b','ContentB', true, false),
                EWDocumentClauseDataFactory.createDocumentClause('c','ContentC', true, false)
        };
        insert clauses;

        List<vlocity_ins__DocumentClause__c> clausesCheck = [SELECT Id, Name, EW_Version_Number__c FROM vlocity_ins__DocumentClause__c];
        for(vlocity_ins__DocumentClause__c c : clausesCheck){
            System.assertEquals(null, c.EW_Version_Number__c);
            System.assertEquals(false, c.Name.contains('v1'));
        }

        EWDocumentClauseTriggerHandler.IsDisabled = false;
        update clauses;

        clausesCheck = [SELECT Id, Name, EW_Version_Number__c FROM vlocity_ins__DocumentClause__c];
        for(vlocity_ins__DocumentClause__c c : clausesCheck){
            System.assertEquals(1, c.EW_Version_Number__c);
            System.assertEquals(true, c.Name.contains('v1'));
        }

    }

}