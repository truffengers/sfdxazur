({
	topicObjectChanged : function(component, event, helper) {
		var selectedTopic = {
			value: component.get('v.recordId'), 
			label: component.get('v.topicObject').Name
		};
		component.set('v.selectedTopic', selectedTopic);
	}
})