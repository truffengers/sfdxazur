({
	componentInitJs : function(component, event, helper) {
        helper.setupCarousel(component);
	},
    doInit : function(component, event, helper) {
        var getCarouselArticlesAction = component.get('c.getCarouselArticles');
        
        getCarouselArticlesAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.carouselArticles', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(getCarouselArticlesAction);        
    }
})