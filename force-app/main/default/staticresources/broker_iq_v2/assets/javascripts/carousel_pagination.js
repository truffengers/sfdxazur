TT.carouselPagination = function($, options) {

  'use strict';

  var listSelector = options.listSelector;
  var activeClass  = options.activeClass || 'is-active';
  var visibleClass = options.visibleClass || 'is-visible';
  var callback;
  var $list;
  var $links;
  var index;
  var count;
  var ON = 'on';
  var OFF = 'off';



  function referenceElements() {
    $list = $(listSelector);
    duplicateItems();
    $links = $list.find('a');
  }

  function turnListeners(onOrOff) {
    $links[onOrOff]('click', clickHandler);
  }

  function clickHandler(event) {
    event.preventDefault();
    callback($links.index(event.currentTarget));
  }

  function hightlight() {
    $links.eq(index).addClass(activeClass);
  }

  function unhightlight() {
    $links.eq(index).removeClass(activeClass);
  }

  function duplicateItems() {
    var $item = $list.find('li');
    for(var i=1; i<count; i++) {
      $item.clone().insertAfter($item);
    }
  }

  function reveal() {
    $list.addClass(visibleClass);
  }



  return {

    init: function(params) {
      count = params.count;
      if(count) {
        index = 0;
        callback = params.callback;
        referenceElements();
        turnListeners(ON);
        hightlight();
        reveal();
      }
    },

    update: function(newIndex) {
      unhightlight();
      index = newIndex;
      hightlight();
    },

    teardown: function() {
      if(count) {
        unhightlight();
        turnListeners(OFF);
        listSelector = null;
        activeClass  = null;
        visibleClass = null;
        callback     = null;
        $list        = null;
        $links       = null;
        index        = null;
        count        = null;
      }

    }

  };

};