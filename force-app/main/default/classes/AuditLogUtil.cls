/*
 * This class contains utility methods to create,
 * Auditlog__c table records.
 * You would call methods from this class from a trigger, process builder,
 * batch job, from your house, neighbours house.
 */
public class AuditLogUtil {

    // create auditlog record for the sobject
    // call this method from afterInsert and afterUpdate context in your trigger
    public static void createAuditLog(String sObjectAPIName, List<sObject> newList, Map<Id, sObject> oldMap) {
        try {
            // Get the objects and fields to be tracked / audited from the
            // metadata reference table
            List<AuditLogReference__mdt> auditLogRefs = new List<AuditLogReference__mdt>([
                    SELECT fieldAPIName__c, fieldLabel__c, sObjectAPIName__c
                    FROM AuditLogReference__mdt
                    WHERE sObjectAPIName__c = :sObjectAPIName
            ]);

            List<AuditLog__c> auditLogs = new List<AuditLog__c>();

            for (AuditLogReference__mdt logRef : auditLogRefs) {

                System.debug('Checking for field : ' + logRef.fieldAPIName__c);

                for (sObject sObj : newList) {
                    sObject oldSobj = null;
                    String sObjId = (String) sObj.get('Id');

                    // check if the new value is different from
                    // previous value, otherwise skip and continue
                    Boolean hasValueChanged = false;

                    if (oldMap == null || oldMap.size() == 0) {
                        // this means that its an insert
                        // but also check if the new value is null
                        // if the value is null, we don't want an audit
                        // log for that
                        if (sObj.get(logRef.fieldAPIName__c) != null) {
                            hasValueChanged = true;
                        }

                    } else { // this is an update
                        // check if old value is not the same as new value
                        oldSobj = oldMap.get(sObjId);

                        if (oldSobj.get(logRef.fieldAPIName__c) != sObj.get(logRef.fieldAPIName__c)) {
                            hasValueChanged = true;
                        }
                    }

                    if (!hasValueChanged) {
                        System.debug('Skipping the loop because the value has not changed for the field : ' + logRef.fieldAPIName__c);
                        // skip this loop
                        continue;
                    }

                    AuditLog__c auditLog = new AuditLog__c();
                    auditLog.sObjectType__c = sObjectAPIName;
                    auditLog.sObjectRecordId__c = sObjId;
                    auditLog.fieldAPIName__c = logRef.fieldAPIName__c;
                    auditLog.fieldLabel__c = logRef.fieldLabel__c;
                    auditLog.previousValue__c = oldSobj == null ? '' : String.valueOf(oldSobj.get(logRef.fieldAPIName__c));
                    auditLog.newValue__c = String.valueOf(sObj.get(logRef.fieldAPIName__c));
                    auditLog.timestamp__c = Datetime.now();
                    auditLog.changedByUser__c = UserInfo.getUserId();
                    auditLog.changedByUsername__c = UserInfo.getName();
                    auditLogs.add(auditLog);
                }
            }

            insert auditLogs;

        } catch (Exception ex) {
            System.debug('Something went horribly wrong in AuditLogUtil ' + ex.getMessage() + ' and the cause is : ' + ex.getCause());
            LogBuilder.insertLog(ex);
        }
    }
}