/**
* @name: PHO_CreateOperationalData2JobTest
* @description: Test class for PHO_CreateOperationalData2Job class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 23/03/2020
* @lastChangedBy: Konrad Wlazlo   31/03/2020    PR-75 Updated test method to cover scenario with Azur Log creation in case of exception
* @lastChangedBy: Antigoni D'Mello  26/01/2021  PR-113: Changed carrier DR Code and added Carrier in products
*/
@IsTest
private class PHO_CreateOperationalData2JobTest{

    private static final String CURRENCY_GBP = 'GBP';

    @TestSetup
    private static void createTestData() {
        String brokerDrCode = 'BAD';
        String carrierDrCode = 'CR0007';
        String mgaDrCode = 'MAD';
        String alternateReference = 'DR8059';

        Account dummyAccount = TestDataFactory.createBrokerageAccount('Dummy Account');

        Account producingBrokerAccount1 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAccount_DR8059');
        producingBrokerAccount1.EW_DRCode__c = alternateReference;
        producingBrokerAccount1.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account accountingBrokerAccount = TestDataFactory.createBrokerageAccount('TestAccountingBrokerAccount');
        accountingBrokerAccount.EW_DRCode__c = brokerDrCode;
        accountingBrokerAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account carrierAccount = TestDataFactory.createCarrierAccount('TestCarrierAccount');
        carrierAccount.EW_DRCode__c = carrierDrCode;
        carrierAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account mgaAccount = TestDataFactory.createBrokerageAccount('TestMGAAccount');
        mgaAccount.EW_DRCode__c = mgaDrCode;
        mgaAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        insert new List<Account>{
                dummyAccount,
                producingBrokerAccount1,
                accountingBrokerAccount,
                carrierAccount,
                mgaAccount
        };
        IBA_IBACustomSettings__c settings = new IBA_IBACustomSettings__c (
                PHO_PhoenixAccBrokerAccountDRCode__c = brokerDrCode,
                PHO_PhoenixMGAAccountDRCode__c = mgaDrCode,
                PHO_FixedHomeBrokerCommission__c = 36.7,
                PHO_FixedMotorBrokerCommission__c = 26.45,
                IBA_DummyAccount__c = dummyAccount.Id
        );
        insert settings;

        carrierAccount = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Carrier'];

        Product2 p1 = new Product2(Name = 'Motor Comprehensive', PHO_PhoenixProductCode__c = 'MOT01',  PHO_PhoenixSectionCode__c = 'MT01',
                                vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor');
        insert p1;

        PHO_PhoenixStaging__c stagingRecord = new PHO_PhoenixStaging__c(
                PHO_CurrencyType__c = CURRENCY_GBP,
                PHO_AlternateReference__c = alternateReference,
                PHO_ProductCode__c = 'MOT01',
                PHO_SectionCode__c = 'MT01',
                PHO_Premium__c = 67,
                PHO_Commission__c = 10.05,
                PHO_ExpiryDate__c = Datetime.now(),
                PHO_InceptionDate__c = Datetime.now(),
                PHO_EffectiveDate__c = Datetime.now(),
                PHO_EntryType__c = 'NB',
                PHO_PaymentMethod__c = 'Money',
                PHO_CarrierDRCode__c = carrierDrCode,
                PHO_CompanyCode__c = 'BRD01'
        );
        insert stagingRecord;
    }

    @IsTest
    private static void testExecuteSuccess() {
        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_CreateOperationalData2Job(1, null));
        Test.stopTest();

        // Verify results
        PHO_PhoenixStaging__c stagingRecordQueried = [
                SELECT PHO_Premium__c
                FROM PHO_PhoenixStaging__c
        ];
        IBA_PolicyTransactionLine__c transactionLineQueried = [
                SELECT PHO_PHGrossWrittenPremium__c
                FROM IBA_PolicyTransactionLine__c
        ];

        System.assertEquals(stagingRecordQueried.PHO_Premium__c, transactionLineQueried.PHO_PHGrossWrittenPremium__c, 'Financial data should be copied from staging record to Transaction Line Item');
    }

    @IsTest
    private static void testExecuteNoRecords() {
        // Prepare data
        PHO_PhoenixStaging__c stagingRecord = [
                SELECT PHO_ProductCode__c,
                        PHO_Product__c
                FROM PHO_PhoenixStaging__c
        ];
        // with no product staging record will have a status "Missing data" and so will not be queried in job below
        stagingRecord.PHO_ProductCode__c = 'x';
        stagingRecord.PHO_Product__c = null;
        update stagingRecord;

        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_CreateOperationalData2Job(1, null));
        Test.stopTest();

        // Verify results
        System.assertEquals(0, [SELECT COUNT() FROM Asset], 'No operational data should be created');
    }

    @IsTest
    private static void testExecuteException() {
        // Prepare data
        PHO_PhoenixStaging__c stagingRecord = [
                SELECT Id
                FROM PHO_PhoenixStaging__c
        ];
        // empty PHO_InceptionDate__c field causes an exception
        stagingRecord.PHO_InceptionDate__c = null;
        update stagingRecord;

        // Perform test
        Test.startTest();
        System.enqueueJob(new PHO_CreateOperationalData2Job(1, null));
        Test.stopTest();

        // Verify results
        System.assertEquals(0, [SELECT COUNT() FROM Asset], 'No operational data should be created');
        Integer azurLogs = [
                SELECT COUNT()
                FROM Azur_Log__c
                WHERE Class__c = 'PHO_CreateOperationalData2Job'
        ];
        System.assertEquals(1, azurLogs, 'There should be one Azur Log record created');
    }
}