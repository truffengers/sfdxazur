import { LightningElement, track } from 'lwc';

export default class Ew_flashMessage extends LightningElement {

    @track displayFlashMessage = true;
    isUnsupportedBrowser = false;
    browserSupportText = 'From the 1st of January 2020 Broker Hub will no longer support your browser.';

    connectedCallback() {
        const agent = window.navigator.userAgent.toLowerCase();
        if (agent.indexOf('edge') > -1) {
            this.isUnsupportedBrowser = true;
        }

        const today = new Date();
        const janFirst = new Date('2021-01-01T00:00:00');

        if (today >= janFirst) {
            this.browserSupportText = 'Broker Hub no longer supports your browser.';
        }
    }

    dismissFlashMessage() {
        this.displayFlashMessage = false;
    }
}