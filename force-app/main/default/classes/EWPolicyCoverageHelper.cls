/**
 * Created by tijojoy on 04/03/2019.
 */

public with sharing class EWPolicyCoverageHelper {

    public static void handleVlocityAttributes(List<SObject> newItems) {
        EWJSONHelper.mapJsonFieldsToSFFields(newItems, EWConstants.ATTRIBUTE_SELECTED_VALUE_JSON_TO_API_MAPPINNG);

        for (SObject obj : newItems) {
            vlocity_ins__AssetCoverage__c pcDetails = (vlocity_ins__AssetCoverage__c) obj;

            if (!String.isEmpty(pcDetails.vlocity_ins__AttributeSelectedValues__c)) {
                pcDetails.EW_AzurCommissionValue__c = pcDetails.EW_AzurCommissionValue__c == null ? 0 : pcDetails.EW_AzurCommissionValue__c;
                pcDetails.EW_BrokerCommissionValue__c = pcDetails.EW_BrokerCommissionValue__c == null ? 0 : pcDetails.EW_BrokerCommissionValue__c;
                pcDetails.EW_IPTTotal__c = pcDetails.EW_IPTTotal__c == null ? 0 : pcDetails.EW_IPTTotal__c;
                pcDetails.EW_PremiumGrossExIPT__c = pcDetails.EW_PremiumGrossExIPT__c == null ? 0 : pcDetails.EW_PremiumGrossExIPT__c;
                pcDetails.EW_PremiumGrossIncIPT__c = pcDetails.EW_PremiumGrossIncIPT__c == null ? 0 : pcDetails.EW_PremiumGrossIncIPT__c;
                pcDetails.EW_PremiumNet__c = pcDetails.EW_PremiumNet__c == null ? 0 : pcDetails.EW_PremiumNet__c;

                Map<String, Object> attributeSelectedValues = (Map<String, Object>) JSON.deserializeUntyped(pcDetails.vlocity_ins__AttributeSelectedValues__c);
                if (attributeSelectedValues.containsKey(EWConstants.ATTR_ART_VALUE)
                        || attributeSelectedValues.containsKey(EWConstants.ATTR_JEWELLERY_VALUE)
                        || attributeSelectedValues.containsKey(EWConstants.ATTR_PEDAL_VALUE)) {
                    pcDetails.EW_RatePerSI__c =
                            (pcDetails.EW_PremiumGrossExIPT__c == null || pcDetails.EW_SpecifiedItemValue__c == null || pcDetails.EW_SpecifiedItemValue__c == 0)
                                    ? 0
                                    : pcDetails.EW_PremiumGrossExIPT__c / pcDetails.EW_SpecifiedItemValue__c;
                }
            }
        }
    }

    public static void handleDuplicatingCoverages(List<SObject> newItems) {
        // called on insert event

        // get product ids for the policies in the insert trigger event
        Map<String, Set<Id>> triggerNewMap = new Map<String, Set<Id>>();
        for (SObject obj : newItems) {
            vlocity_ins__AssetCoverage__c ac = (vlocity_ins__AssetCoverage__c) obj;

            if (ac.vlocity_ins__PolicyAssetId__c != null && ac.vlocity_ins__Product2Id__c != null) {
                Set<Id> productIds = triggerNewMap.containsKey(ac.vlocity_ins__PolicyAssetId__c) ? triggerNewMap.get(ac.vlocity_ins__PolicyAssetId__c) : new Set<Id>();
                productIds.add(ac.vlocity_ins__Product2Id__c);
                triggerNewMap.put(ac.vlocity_ins__PolicyAssetId__c, productIds);
            }
        }

        // check existing records for duplicating coverages
        List<vlocity_ins__AssetCoverage__c> existingCoverages = [SELECT Id,vlocity_ins__PolicyAssetId__c,vlocity_ins__Product2Id__c FROM vlocity_ins__AssetCoverage__c WHERE vlocity_ins__PolicyAssetId__c in:triggerNewMap.keySet()];
        List<vlocity_ins__AssetCoverage__c> oldCoveragesToRemove = new List<vlocity_ins__AssetCoverage__c>();
        for (vlocity_ins__AssetCoverage__c ec : existingCoverages) {

            if (triggerNewMap.containsKey(ec.vlocity_ins__PolicyAssetId__c)) {
                Set<Id> productIds = triggerNewMap.get(ec.vlocity_ins__PolicyAssetId__c);
                if (ec.vlocity_ins__Product2Id__c != null && productIds.contains(ec.vlocity_ins__Product2Id__c)) {
                    oldCoveragesToRemove.add(ec);
                }
            }
        }

        // remove duplicating coverages
        if (!oldCoveragesToRemove.isEmpty()) {
            delete oldCoveragesToRemove;
        }

    }

}