public class IBA_AccountConductorController {
    private final static String urlFieldName = 'IBA_CongaBatchFormula__c';
    private String partnerServerUrl;
  private ApexPages.StandardSetController controller;
    private List<String> accountIds = new List<Id>();
    
    public String conductorUrl {get; private set;}
    
    public IBA_AccountConductorController(ApexPages.StandardSetController controller) {
        this.controller = controller;
        
        for (Account account : (List<Account>)controller.getSelected()){ 
            accountIds.add(account.Id);
        }
        
        partnerServerUrl = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+'/services/Soap/u/26.0/'+UserInfo.getOrganizationId();
    }
    
    public PageReference prepareConductorUrl() {
        conductorUrl = 'https://conductor.congamerge.com' +
            '?MOID=' + String.join(new List<String>(accountIds),',') +
      '&SessionId=' + UserInfo.getSessionID() +
      '&ServerUrl=' + partnerServerUrl +
      '&UrlFieldName=' + urlFieldName;
        
        return null;
    }
 
}