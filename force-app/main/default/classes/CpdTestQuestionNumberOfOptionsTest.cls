@isTest
private class CpdTestQuestionNumberOfOptionsTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();
	private static CPD_Test_Question__c q = tog.getCpdTestQuestion();

	private static void testValue(String option, Integer expectedResult) {
		q.Answer__c = option;

		update q;

		q = [SELECT Id, Number_of_Options_to_Choose__c FROM CPD_Test_Question__c WHERE Id = :q.Id];

		System.assertEquals(expectedResult, q.Number_of_Options_to_Choose__c);

	}

	@isTest static void basic() {
		testValue('1', 1);
		testValue('1;2', 2);
		testValue(null, 0);
	}
	
}