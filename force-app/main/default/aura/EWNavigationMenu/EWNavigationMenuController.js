({
    onClick: function (component, event, helper) {
        const id = event.target.dataset.menuItemId;
        component.find('menuBase').navigate(id);
    }
});