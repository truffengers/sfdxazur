({

    doInit: function (component) {
        //component.set("v.gridData", JSON.parse(helper.getSampleJSON());
        this.loadJSONReport(component);
    },
    loadJSONReport: function (component) {
        let action = component.get("c.getReportData");

        action.setCallback(this, function (response) {
            const responseJson = response.getReturnValue();

            if (responseJson) {
                console.log('bdec // responseJson: ', responseJson);

                component.set("v.gridData", JSON.parse(responseJson));
            }

        });
        $A.enqueueAction(action);
    },
    getSampleJSON: function () {
        // 3. Variable declaration and assigning value to show in component.
        let jsonStr = '{"rows":[{"vals":[{"val":"Salesforce","cssClass":""},{"val":"SFO","cssClass":""},{"val":"info@Salesforce.com","cssClass":""},{"val":"8602229632","cssClass":"responsiveHide"}]},{"vals":[{"val":"Microsoft","cssClass":""},{"val":"SFO","cssClass":""},{"val":"info@microsoft.com","cssClass":""},{"val":"8602283222","cssClass":"responsiveHide"}]},{"vals":[{"val":"SAP","cssClass":""},{"val":"SFO","cssClass":""},{"val":"info@SAP.com","cssClass":""},{"val":"8600942222","cssClass":"responsiveHide"}]},{"vals":[{"val":"Google","cssClass":""},{"val":"SFO","cssClass":""},{"val":"info@Google.com","cssClass":""},{"val":"8602479222","cssClass":"responsiveHide"}]}],"headers":[{"title":"ClientName","isSortable":false,"dataType":"","cssClass":"true"},{"title":"Address","isSortable":false,"dataType":"","cssClass":""},{"title":"Email","isSortable":false,"dataType":"","cssClass":""},{"title":"Mobile","isSortable":false,"dataType":"","cssClass":"responsiveHide"}]}';
        return jsonStr;
    }
});