/*
Name:            EWAccountShareToBroker
Description:     This Class will share the client account to shared with Account User.
Created By:      Rajni Bajpai 
Created On:      05 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          05May21            EWH-1983 Share Account ID
************************************************************************************************
*/

public without sharing class EWAccountShareToBroker {
    
     public static void shareClientAccount(Map<Id, SObject> newAccounts){
        Map<Id,Id> clientAccountParMap=new Map<Id,Id>();
        Map<Id,Id> parentAccSharedAccMap=new Map<Id,Id>();
        Set<Id> sharedAccIds=new Set<Id>();
        Set<Id> parentAccIdsSet=new Set<Id>();
        List<AccountShare> accShareNewList=new List<AccountShare>();
        For (Account acc:[select id,Owner.AccountId from Account where Id in:newAccounts.keyset()]){
            parentAccIdsSet.add(acc.Owner.AccountId);
            clientAccountParMap.put(acc.id,acc.Owner.AccountId);
        }      

        for(EW_AccountSharing__c accshr:[select id,name,EW_ParentAccount__c,EW_SharedWithAccount__c  from EW_AccountSharing__c where EW_ParentAccount__c in:parentAccIdsSet]){
            parentAccSharedAccMap.put(accshr.EW_ParentAccount__c,accshr.EW_SharedWithAccount__c);
            sharedAccIds.add(accShr.EW_SharedWithAccount__c);                        
        }

        if(parentAccSharedAccMap.size()>0){
            List<UserRole> roles=[select name,Id,PortalAccountId from UserRole where PortalAccountId in:sharedAccIds];
            Map<Id,Id> roleAccMap=new Map<Id,Id>();
            for(UserRole rl:roles){
                roleAccMap.put(rl.PortalAccountId,rl.Id); 
            }

        Map<String,String> groupMap=new Map<String,String>();        
        for(group grp:[select Id,RelatedId from group where type='Role' and RelatedId in:roles]){
            
            groupMap.put(grp.RelatedId,grp.Id);
        }
        for(Id cAcc:clientAccountParMap.keyset()){
            id grpId=groupMap.get(roleAccMap.get(parentAccSharedAccMap.get(clientAccountParMap.get(cAcc))));

                if(grpId!=null){
                    AccountShare accShareNew= new AccountShare();
                    accShareNew.AccountId=cAcc;
                    accShareNew.UserOrGroupId=grpId;
                    accShareNew.AccountAccessLevel='Edit';
                    accShareNew.OpportunityAccessLevel='Read';
                    accShareNewList.add(accShareNew); 
                }     
            }        
        }
        
        if(!accShareNewList.isEmpty()){
            try{
                if(!Test.isRunningTest()){
                    insert accShareNewList;
                }
            }
            catch(Exception e){
                LogBuilder logBuilder = new LogBuilder();
                Azur_Log__c log = logBuilder.createGenericLog(e);
                log.Class__c = 'EWAccountShareToBroker';
                log.Method__c = 'shareClientAccount';
                log.Log_message__c = log.Log_message__c
                    + '\n\n method name = shareClientAccount'
                    + '\n\n ln: ' + e.getLineNumber()
                    + '\n\n st: ' + e.getStackTraceString();
                insert log;
                throw e;
            }
        }       
    }

}