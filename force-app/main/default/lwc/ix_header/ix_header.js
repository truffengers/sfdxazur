import { LightningElement, track, api } from 'lwc';
import { analyticsEvent } from 'c/ix_utils';

export default class Ix_Header extends LightningElement {
    @api currentOpenQuickLinkTitle;

    @api quickLinksArray;

    quickLinkClickEvent(event) {
        let eventDescription;
        
        if (this.currentOpenQuickLinkTitle !== event.target.dataset.value) {
            // The user has opened a quick link
            this.currentOpenQuickLinkTitle = event.target.dataset.value;
            eventDescription = `"${event.target.dataset.value}" quick link \
                opened.`;
        } else {
            // The user has closed a quick link
            this.currentOpenQuickLinkTitle = null;
            eventDescription = `"${event.target.dataset.value}" quick link \
                closed.`;
        }

        analyticsEvent({
            eventName: 'Quick Link Clicked',
            eventData: event.target.dataset.value,
            eventDescription
        })
            .catch(() => null);

        const quickLinkClickEvent = new CustomEvent('quicklinkclick', {
            detail: { value: event.target.dataset.value }
        });
        this.dispatchEvent(quickLinkClickEvent);
    }

    handleWordingLinkClick() {
        // Dispatch the analytics event for when you click the link to the
        // policy wording PDF
        analyticsEvent({
            eventName: 'Quick Link Clicked',
            eventDescription: 'Policy Wording quick link clicked.'
        })
            .catch(() => null);
    }
    handleIPIDLinkClick() {
        // Dispatch the analytics event for when you click the link to the
        // ipid PDF
        analyticsEvent({
            eventName: 'Quick Link Clicked',
            eventDescription: 'IPID quick link clicked.'
        })
            .catch(() => null);
    }
}