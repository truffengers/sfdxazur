@isTest
public with sharing class EWPolicyCancellationBatchTest {
    
    @testSetup
    static void setup(){
        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );

        insert new List<Account>{
                broker, insured
        };

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.Status = EWConstants.EW_POLICY_STATUS_PENDING_CANCELLATION;
        policy.vlocity_ins__EffectiveDate__c = Date.today().addDays(-1);

        insert policy;
    }
    
    @isTest
    private static void cancellationStatusUpdateTest(){
        Test.startTest();
        Id batchId = Database.executeBatch(new EWPolicyCancellationBatch());
        Test.stopTest();

        Asset expectedPolicy = [SELECT Status FROM Asset];
        System.assert(expectedPolicy.Status == EWConstants.EW_POLICY_STATUS_CANCELLED);
    }
}