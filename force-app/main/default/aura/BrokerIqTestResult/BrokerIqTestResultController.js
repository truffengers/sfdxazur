({
	doInit : function(component, event, helper) {
		var recordId = component.get('v.recordId');

		if(!$A.util.isEmpty(recordId)) {
			var getRecordsFieldSetAction = component.get('c.getRecordsFieldSet');

			getRecordsFieldSetAction.setParams({
				'typeName': 'CPD_Assessment_Answer__c',
				'fieldSetName': 'Wrong_Answers',
				'whereClause': 'CPD_Assessment__c = \'' + recordId + '\' AND Correct__c = \'false\' ORDER BY CPD_Test_Question__r.Question_Order__c'
			});

			getRecordsFieldSetAction.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === 'SUCCESS') {
	            	var wrongAnswers = response.getReturnValue();

	            	wrongAnswers.forEach(function(thisWrongAnswer){
	            		var rightAnswerNumbers = thisWrongAnswer.CPD_Test_Question__r.Answer__c.split(';');
	            		thisWrongAnswer.rightAnswerStrings = rightAnswerNumbers.map(function(thisAnswerNumber) { return thisWrongAnswer.CPD_Test_Question__r['Option_' + thisAnswerNumber + '__c'];});
	            	});

	                component.set('v.wrongAnswers', wrongAnswers);
		        }
	        });
	        
	        $A.enqueueAction(getRecordsFieldSetAction);	
		}
	}
})