public class EWPolicyCancellationBatch implements Database.Batchable<SObject>{

    public Database.QueryLocator start(Database.BatchableContext bc) {
        Date todayDate = Date.today();
        String policyStatus = EWConstants.EW_POLICY_STATUS_PENDING_CANCELLATION;
        String queryString = 'SELECT Id, Status FROM Asset WHERE Status = :policyStatus AND vlocity_ins__EffectiveDate__c <= :todayDate';

        return Database.getQueryLocator(queryString);
    }

    public void execute(Database.BatchableContext bc, List<Asset> scope) {
        for(Asset policyToCancel : scope){
            policyToCancel.Status = EWConstants.EW_POLICY_STATUS_CANCELLED;
        }

        update scope;
    }

    public void finish(Database.BatchableContext bc) {

    }

}