public with sharing class BrokerIqFurtherContentController {

	@AuraEnabled
	public static List<IQ_Content__c> getFurtherContent(String existingId) {

		IQ_Content__c existingContent = [SELECT Id, RecordTypeId FROM IQ_Content__c WHERE Id = :existingId];
		Id existingRecordTypeId = existingContent.RecordTypeId;

		String query = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 
                                                                'BiQ_2_Card_Grid', 
                                                                new Set<String>(), 
                                                                'RecordTypeId = :existingRecordTypeId AND Id != :existingId AND Active__c = true', 
                                                                'Content_Date__c DESC', 
                                                                'LIMIT 3');
		return Database.query(query);
	}
}