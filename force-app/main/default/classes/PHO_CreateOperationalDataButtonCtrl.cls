/**
* @name: PHO_CreateOperationalDataButtonCtrl
* @description: Controller class for PHO_CreateOperationalDataButton page.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 17/12/2019
*/
public with sharing class PHO_CreateOperationalDataButtonCtrl {

    private ApexPages.StandardSetController standardSetController;
    private PageReference originalPage;

    public PHO_CreateOperationalDataButtonCtrl(ApexPages.StandardSetController standardSetController) {
        this.standardSetController = standardSetController;
        this.originalPage = standardSetController.cancel();
    }

    /**
    * @methodName: createOperationalData
    * @description: Enqueues a job which translates Phoenix Staging records into Salesforce entities
    * @dateCreated: 17/12/2019
    */
    public PageReference createOperationalData() {
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        Integer recordLimit = (Integer) settings.PHO_PhoenixStagingRecordsLimit__c;
        if (recordLimit > 0) {
            System.enqueueJob(new PHO_CreateOperationalDataJob(recordLimit, null));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to set PHO_PhoenixStagingRecordsLimit__c in IBA_IBACustomSettings__c custom setting to execute this action'));
            return null;
        }
        return this.originalPage;
    }

    /**
    * @methodName: goBack
    * @description: Redirects to original context page
    * @dateCreated: 17/12/2019
    */
    public PageReference goBack() {
        return this.originalPage;
    }
}