public with sharing class EWCarouselHelper {
    @AuraEnabled
    public static List<cloudx_cms__SS_Carousel_Slide__c> EWCarouselHelper() {
        Boolean showBrokerNetworkContent = EWAccountHelper.checkIfBrokerNetworkBroker();

        cloudx_cms__SS_Carousel__c carousel = [SELECT Id, cloudx_cms__Name__c
                                        FROM cloudx_cms__SS_Carousel__c
                                        WHERE cloudx_cms__Name__c = 'EW_CRSL_001'];

        String carouselId = carousel.Id;

        List<cloudx_cms__SS_Carousel_Slide__c> slides = [SELECT cloudx_cms__Carousel__c,
                    cloudx_cms__Title__c,
                    cloudx_cms__Description__c,
                    cloudx_cms__Button_URL__c,
                    cloudx_cms__Image_URL__c,
                    EW_showForBrokerNetworkBrokers__c
            FROM cloudx_cms__SS_Carousel_Slide__c
            WHERE cloudx_cms__Carousel__c = :carouselId
            AND EW_showForBrokerNetworkBrokers__c = :showBrokerNetworkContent];

        return slides;
    }
}