trigger CpdTestQuestionNumberOfOptions on CPD_Test_Question__c (before insert, before update) {

    for(CPD_Test_Question__c q : Trigger.new) {
        if(q.Answer__c != null) {
            q.Number_of_Options_to_Choose__c = q.Answer__c.split(';').size();
        } else {
        	q.Number_of_Options_to_Choose__c = 0;
        }
    }
    
}