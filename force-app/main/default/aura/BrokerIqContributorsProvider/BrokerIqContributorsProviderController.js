({
	doInit : function(component, event, helper) {
		var getContributorsAction = component.get('c.getContributors');
		
		getContributorsAction.setStorable();
		
		getContributorsAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.contributors', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(getContributorsAction);		
  	}
})