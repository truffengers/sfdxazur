({
    componentInitJs : function(component, event, helper) {
        var scrollBroadcaster = TT.scrollBroadcaster(jQuery, { window: window, isLogging: false, trailing: true });

        TT.mediaMatcher.init(_, {
          mobile  : '(max-width: 860px)',
          desktop : '(min-width: 861px)'
        });

        TT.menuVisibilityManager.init({
          scrollBroadcaster : scrollBroadcaster,
          mediaMatcher      : TT.mediaMatcher
        });

        scrollBroadcaster.init();
        scrollBroadcaster.enable();

        // Really, #js-user-menu should be initialised in BrokerIqUserMenu,
        // but that seems to cause problems, so we do both
        TT.dropdownMenu.init({
          selector: '#js-subdomain-menu, #js-user-menu',
          detectIt: detectIt
        });

    },
    handleMenuEvent : function(component, event, helper) {
        var operation = event.getParam('operation');
        if(operation === 'disable') {
            component.set('v.menuId', TT.menuVisibilityManager.disable());
        } else {
            var menuId = component.get('v.menuId');
            if(menuId) {
                TT.menuVisibilityManager.enable(menuId);
            }
        }
    },
    toggleSubdomainMenu : function(component, event, helper) {
        subdomainMenuCurrentlyOpen = component.get('v.subdomainMenuOpen');
        component.set('v.subdomainMenuOpen', !subdomainMenuCurrentlyOpen);
    }
})