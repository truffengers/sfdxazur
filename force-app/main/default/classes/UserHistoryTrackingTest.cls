@istest
public class UserHistoryTrackingTest {

    @istest
    public static void basic() {
        String oldEmail = UserInfo.getUserEmail();
        String newEmail = 'a+' + oldEmail;
        User u = new User(Id = UserInfo.getUserId(), Email = newEmail);
        
        Test.startTest();
        update u;
        Test.stopTest();
        
        List<User_History__c> histories = [SELECT User__c, Field_Developer_Name__c, 
                                           New_Value__c, Old_Value__c 
                                           FROM User_History__c];
        
        System.assertEquals(1, histories.size());
        System.assertEquals(u.Id, histories[0].User__c);
        System.assertEquals('Email', histories[0].Field_Developer_Name__c);
        System.assertEquals(newEmail, JSON.deserialize(histories[0].New_Value__c, String.class));
        System.assertEquals(oldEmail, JSON.deserialize(histories[0].Old_Value__c, String.class));
    }
    
}