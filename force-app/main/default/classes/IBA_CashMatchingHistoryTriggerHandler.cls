/**
* @name: IBA_CashMatchingHistoryTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaCashMatchingHistory__c trigger functionality
*
*/

public with sharing class IBA_CashMatchingHistoryTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_CashMatchingHistoryTriggerHandler.IsDisabled != null ? IBA_CashMatchingHistoryTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        // this method update numbers on cash matching history related records
        IBA_CashMatchingHistoryTriggerHelper.calculateNumbersOnRelatedRecords(newItems);
        // this method links AR Cash Transaction and populate cash entry link
        IBA_CashMatchingHistoryTriggerHelper.updateMatchingReference(newItems);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        // this method links AR Cash Transaction and populate cash entry link
        IBA_CashMatchingHistoryTriggerHelper.updateMatchingReference(newItems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}