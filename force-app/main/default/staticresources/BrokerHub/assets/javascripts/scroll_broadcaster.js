TT.scrollBroadcaster = function($, options) {

  "use strict";

  var $window;
  var $document;
  var lastTop = 0;
  var lastHeight;
  var self = { UP: 'up', DOWN: 'down' };
  var $self = $(self);
  var scrollThrottleFunction;
  var settings;
  var isEnabled = false;
  var eventData;
  var heightChangeIntervalId;

  var ON  = 'on';
  var OFF = 'off';

  var defaults = {
    scrollInterval : 30,
    resizeInterval : 1000,
    window         : window,
    document       : document,
    isLogging      : false,
    trailing       : false
  };



  function setDefaults() {
    settings = $.extend({}, defaults, options || {});
  }

  function referenceElements() {
    $window   = $(settings.window);
    $document = $(settings.document);
  }

  function turnScrollListener(onOrOff) {
    $window[onOrOff]('scroll', scrollThrottleFunction);
  }

  function turnResizeListener(onOrOff) {
    if(onOrOff === ON) {
      heightChangeIntervalId = window.setInterval(resizeHandler, settings.resizeInterval);
    } else {
      window.clearInterval(heightChangeIntervalId);
    }
  }

  function initThrottleFunctions() {
    scrollThrottleFunction = _.throttle(scrollHandler, settings.scrollInterval, { trailing: settings.trailing });
  }

  function getHeight() {
    switch(typeof settings.window.innerHeight) {
    case 'number':
      return settings.window.innerHeight;
    case 'function':
      return settings.window.innerHeight();
    case undefined:
      return settings.window.clientHeight;
    }
  }

  function getTop() {
    var top = settings.window.scrollTop && settings.window.scrollTop();
    return top === undefined ? window.pageYOffset : top;
  }

  function getDirection(top) {
    return top >= lastTop ? self.DOWN : self.UP;
  }

  function dispatchScrollEvent() {
    log();
    $self.triggerHandler('scroll', [eventData]);
  }

  function log() {
    if(settings.isLogging) { console.info(eventData); }
  }

  function storeScrollEventData(direction, topTrigger, bottomTrigger, triggerY, height) {
    eventData = {
      direction     : direction,
      topTrigger    : topTrigger,
      bottomTrigger : bottomTrigger,
      triggerY      : triggerY,
      height        : height
    };
  }

  function scrollHandler(event, options) {
    var skipDispatching = options && options.skipDispatching === true ? true : false;
    var top             = getTop();
    var direction       = getDirection(top);
    var height          = getHeight();
    var topTrigger      = top;
    var bottomTrigger   = top + height;
    var triggerY        = direction === self.UP ? top : top + height;
    storeScrollEventData(direction, topTrigger, bottomTrigger, triggerY, height);
    if(!skipDispatching && isEnabled) { dispatchScrollEvent(); }
    lastTop = top;
  }



  // Height change recalculations

  function resizeHandler() {
    var height = $document.height();
    if(height !== lastHeight) {
      lastHeight = height;
      dispatchHeightChangeEvent();
    }
  }

  function dispatchHeightChangeEvent() {
    $self.triggerHandler('heightChange', [lastHeight]);
  }


  $.extend(self, {

    init: function() {
      setDefaults();
      referenceElements();
      initThrottleFunctions();
      turnScrollListener(ON);
    },

    enable: function() {
      isEnabled = true;
      turnResizeListener(ON);
      scrollHandler();
    },

    disable: function() {
      isEnabled = false;
      turnResizeListener(OFF);
    },

    lastScrollEventData: function() {
      if(!eventData) { scrollHandler(null, { skipDispatching: true }); }
      return eventData;
    },

    getCurrentData: function() {
      scrollHandler(null, { skipDispatching: true });
      return eventData;
    },

    getWindowTop: function() {
      return settings.window === window ? 0 : $window.offset().top;
    },

    teardown: function() {
      turnScrollListener(OFF);
      turnResizeListener(OFF);
      $window                = null;
      $document              = null;
      lastTop                = null;
      lastHeight             = null;
      self                   = null;
      $self                  = null;
      scrollThrottleFunction = null;
      settings               = null;
      isEnabled              = null;
      eventData              = null;
      heightChangeIntervalId = null;
    }

  });



  return self;

};