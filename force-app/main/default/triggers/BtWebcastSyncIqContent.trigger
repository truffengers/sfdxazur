trigger BtWebcastSyncIqContent on BrightTALK__Webcast__c (after insert, after update) {
    Map<Id, IQ_Content__c> contentToUpdate = new Map<Id, IQ_Content__c>();
    
    for(Integer i=0; i < Trigger.size; i++) {
        BrightTALK__Webcast__c thisWebcast = Trigger.new[i];
        
        if(thisWebcast.IQ_Content__c != null 
           && (Trigger.isInsert || thisWebcast.IQ_Content__c != Trigger.old[i].IQ_Content__c)) {
               IQ_Content__c thisContent = new IQ_Content__c(Id = thisWebcast.IQ_Content__c,
                                                             BrightTALK_Webcast__c = thisWebcast.Id);
               contentToUpdate.put(thisContent.Id, thisContent);
           }
    }
    
    if(!contentToUpdate.isEmpty()) {
        List<IQ_Content__c> existingContent = [SELECT Id, BrightTALK_Webcast__c 
                                               FROM IQ_Content__c
                                               WHERE Id IN :contentToUpdate.keySet()];
        List<IQ_Content__c> toReallyUpdate = new List<IQ_Content__c>();
        
        for(IQ_Content__c thisExistingContent : existingContent) {
            IQ_Content__c thisMaybeUpdate = contentToUpdate.get(thisExistingContent.Id);
            if(thisExistingContent.BrightTALK_Webcast__c != thisMaybeUpdate.BrightTALK_Webcast__c) {
                toReallyUpdate.add(thisMaybeUpdate);
            }
        }
        if(!toReallyUpdate.isEmpty()) {
          update toReallyUpdate;
        }
    }

}