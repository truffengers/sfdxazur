@isTest
public with sharing class IqContentSanitiseArticleContentTest {

	private static TestObjectGenerator tog = new TestObjectGenerator();
	private static String data = '<span style="color: black;"><span style="font-size: 12.0pt;">The Internet of Things (IoT) is a term that the majority of people may not be familiar with now but which will become ubiquitous. The Internet of Things already exists but most of us are not yet aware of its impact. We soon will be. It will have a dramatic influence on our personal lives and is already completely transforming the way companies and industries work. Along with developments such as AI, cloud computing, robotics, 3D printing and biotechnology, IoT is part of the fourth industrial revolution currently underway.</span></span><br>';
	private static String expected = '<span><span>The Internet of Things (IoT) is a term that the majority of people may not be familiar with now but which will become ubiquitous. The Internet of Things already exists but most of us are not yet aware of its impact. We soon will be. It will have a dramatic influence on our personal lives and is already completely transforming the way companies and industries work. Along with developments such as AI, cloud computing, robotics, 3D printing and biotechnology, IoT is part of the fourth industrial revolution currently underway.</span></span><br>';

	@isTest
	public static void insertTest() {

		IQ_Content__c iqc = tog.getWebcastIqContentNoInsert();

		iqc.Content__c = data;
		insert iqc;

		iqc = [SELECT Id, Content__c FROM IQ_Content__c WHERE Id = :iqc.Id];

		System.assertEquals(expected, iqc.Content__c);
	}

	@isTest
	public static void updateTest() {

		IQ_Content__c iqc = tog.getWebcastIqContent();

		iqc.Content__c = data;
		update iqc;

		iqc = [SELECT Id, Content__c FROM IQ_Content__c WHERE Id = :iqc.Id];

		System.assertEquals(expected, iqc.Content__c);
	}

	@isTest
	public static void dontMessWithA() {

		IQ_Content__c iqc = tog.getWebcastIqContentNoInsert();
		String aLink = '<a href="http://www.google.com" target="_blank">Link to google</a>';
		iqc.Content__c = data + aLink;
		insert iqc;

		iqc = [SELECT Id, Content__c FROM IQ_Content__c WHERE Id = :iqc.Id];

		System.assert(iqc.Content__c.contains(aLink), iqc.Content__c);
	}

}