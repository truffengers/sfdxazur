({
	doInit : function(component, event, helper) {
		var getFurtherContentAction = component.get('c.getFurtherContent');

        getFurtherContentAction.setParam('existingId', component.get('v.recordId'));

		getFurtherContentAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.furtherContent', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(getFurtherContentAction);		
    }
})