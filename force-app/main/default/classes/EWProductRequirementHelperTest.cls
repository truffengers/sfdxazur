@IsTest
private class EWProductRequirementHelperTest {

    static testMethod void testProductRequirementHelper_onAfterInsert_fixProductReqUWRuleLink() {

        Product2 emergingWealth = EWProductDataFactory.createProduct('Emerging Wealth', false);
        Product2 insuredEntity = EWProductDataFactory.createProduct('Insured Entity', false);

        insert new List<Product2>{
                emergingWealth,
                insuredEntity
        };

        String ruleName1 = 'Total Premium Over £5000';
        String ruleName2 = 'Total Premium Under £800';
        String ruleName3 = 'Bankrupt declaration';

        EW_Underwriting_Rule__c uwRule1 = new EW_Underwriting_Rule__c();
        uwRule1.EW_ProductRequirementName__c = ruleName1;
        uwRule1.EW_ProductName__c = emergingWealth.Name;

        EW_Underwriting_Rule__c uwRule2 = new EW_Underwriting_Rule__c();
        uwRule2.EW_ProductRequirementName__c = ruleName2;
        uwRule2.EW_ProductName__c = emergingWealth.Name;

        EW_Underwriting_Rule__c uwRule3 = new EW_Underwriting_Rule__c();
        uwRule3.EW_ProductRequirementName__c = ruleName3;
        uwRule3.EW_ProductName__c = insuredEntity.Name;

        insert new List<EW_Underwriting_Rule__c>{
                uwRule1, uwRule2, uwRule3
        };

        vlocity_ins__ProductRequirement__c prRule1 = new vlocity_ins__ProductRequirement__c();
        prRule1.vlocity_ins__Product2Id__c = emergingWealth.Id;
        prRule1.Name = ruleName1;

        vlocity_ins__ProductRequirement__c prRule2 = new vlocity_ins__ProductRequirement__c();
        prRule2.vlocity_ins__Product2Id__c = emergingWealth.Id;
        prRule2.Name = ruleName2;

        vlocity_ins__ProductRequirement__c prRule3 = new vlocity_ins__ProductRequirement__c();
        prRule3.vlocity_ins__Product2Id__c = insuredEntity.Id;
        prRule3.Name = ruleName3;

        
        Test.startTest();
        insert new List<vlocity_ins__ProductRequirement__c> {
            prRule1, prRule2, prRule3
        };
        Test.stopTest();

        List<EW_Underwriting_Rule__c> uwRules = new List<EW_Underwriting_Rule__c>([
                SELECT Id, EW_ProductName__c, EW_ProductRequirementName__c, EW_Product_Requirement__c, EW_Product_Requirement__r.Name
                FROM EW_Underwriting_Rule__c
        ]);

        for (EW_Underwriting_Rule__c uwRule : uwRules) {
            if (uwRule.EW_ProductRequirementName__c == ruleName2) {
                System.assertEquals(uwRule.EW_Product_Requirement__c, prRule2.Id);
            } else if (uwRule.EW_ProductRequirementName__c == ruleName3) {
                System.assertEquals(uwRule.EW_Product_Requirement__c, prRule3.Id);
            }
        }
    }
}