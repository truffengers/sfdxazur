/**
* @name: EWQuoteExpirationBatch
* @description: Batch changes quote status to 'Expired' after reaching quote expiration date 
* @author: Aleksejs Jedamenko ajedamenko@azuruw.com
* @date: 12/03/2020
*
*/
public class EWQuoteExpirationBatch implements Database.Batchable<SObject>{

    /**
    * @methodName: start
    * @description: Fetch all quote records with passed expiration date which status is equal to 'Quoted', 'UW Approved', or 'Approved'
    * @dateCreated: 12/03/2020 v2: 19/11/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    */
    public Database.QueryLocator start(Database.BatchableContext bc) {

        String quoteQuotedStatus = EWConstants.QUOTE_STATUS_QUOTED;
        String quoteApprovedStatus = EWConstants.QUOTE_STATUS_APPROVED;
        String quoteUWApprovedStatus = EWConstants.QUOTE_STATUS_UW_APPROVED;
        String queryString = 'SELECT Id, Status FROM Quote WHERE EW_Expiration_Date__c < TODAY AND (Status =: quoteQuotedStatus OR Status =: quoteApprovedStatus OR Status =: quoteUWApprovedStatus)';
        return Database.getQueryLocator(queryString);
    }

    /**
    * @methodName: execute
    * @description: changes quote status to 'Expired'
    * @dateCreated: 12/03/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    */
    public void execute(Database.BatchableContext bc, List<Quote> scope) {
        
        try {

            for (Quote expiredQuote : scope) {

                expiredQuote.Status = EWConstants.QUOTE_STATUS_EXPIRED;
            }
            update scope;
        } catch (Exception ex) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = 'EWQuoteExpirationBatch';
            log.Method__c = 'execute';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWQuoteExpirationBatch.execute'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
            insert log;
            System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            throw ex;
        }
        
    }

    /**
    * @methodName: finish
    * @description: Sends a message regarding batch job results to users mentioned in ApexEmailNotification object
    * @dateCreated: 12/03/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    */
    public void finish(Database.BatchableContext bc) {

        try {

            AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, TotalJobItems FROM AsyncApexJob where Id =:BC.getJobId()]; 
            List<ApexEmailNotification> emailNotifications = [SELECT ID, User.Email FROM ApexEmailNotification];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> toAddresses = new List<String> ();   
            for (ApexEmailNotification emailNotification : emailNotifications) {

                toAddresses.add(emailNotification.User.Email);
            }
            mail.setToAddresses(toAddresses);
            mail.setSubject('Quote Expiration Batch ' + a.Status);
            mail.setPlainTextBody('Number of processed batches: ' + a.TotalJobItems +
           '\n  Number of failed batches: '+ a.NumberOfErrors + '\n\n Detailed information about failures can be found in Azur Log records');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception ex) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = 'EWQuoteExpirationBatch';
            log.Method__c = 'finish';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWQuoteExpirationBatch.finish'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
            System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            insert log;
            throw ex;
        }
    }

}