import { LightningElement, track } from 'lwc';
import Moment from '@salesforce/resourceUrl/Moment';

import { loadScript } from 'lightning/platformResourceLoader';
import { analyticsEvent } from 'c/ix_utils';

import { QUICK_LINKS } from 'c/ix_generalData';
import { CONSTANTS } from 'c/ix_constants';

import agreeToTerms from '@salesforce/apex/IXHelper.agreeToTerms';
/*
For testing purposes only. Sets `User.SmartDocs_Terms_Agreed__c` to false for
the current user.
*/

// import disagreeToTerms from '@salesforce/apex/IXHelper.disagreeToTerms';

import checkTermsAgreed from '@salesforce/apex/IXHelper.checkTermsAgreed';

export default class Ix_Wrapper extends LightningElement {
    /*
    Used to track the time the session started, for analytics purposes
    */
    @track sessionStartTime;
    /*
    Used to track whether the user has made any click at all on the app for
    analytics purposes
    */
    @track userInteracted;

    @track overlayOpen = false;
    @track feedbackFormOpen = false;

    @track quickLinksTitle;
    @track quickLinksData;
    @track quickLinksArray
        = Object.values(CONSTANTS.QUICK_LINK_NAMES).map(name => {
            return {
                name,
                itemData: QUICK_LINKS[name],
                currentlyOpen: false
            }
        });

    /*
    Whether the user has clicked to agree to the Terms of Use
    */
    @track termsAgreed = false;
    @track currentlyLoading = true;

    connectedCallback() {
        loadScript(this, `${Moment}/moment.js`)
            .then(() => { this.sessionStartTime = moment() })
            .catch(error => console.error(error));

        /*
        For analytics purposes, generate a random Session ID in order to know
        which analytics events were triggered within the same session.
        */
        const randomSessionID = Math.random().toString().split('.')[1];
        window.sessionStorage.setItem('sessionID', randomSessionID);

        analyticsEvent({ eventName: 'Session Started' })
            .catch(() => null);

        window.addEventListener('beforeunload', () => {
            const sessionLengthMins = moment().diff(
                this.sessionStartTime, 'minutes');

            const sessionLengthSeconds = moment().diff(
                this.sessionStartTime,'seconds') % 60;

            let eventDescription = `Session was ${sessionLengthMins} mins, 
                ${sessionLengthSeconds} seconds`;
            
            if (!this.userInteracted) {
                eventDescription += `, and the user bounced without any
                    interaction`
            }

            analyticsEvent({ eventName: 'Session Ended', eventDescription })
                .catch(() => null);
        })

        /*
        The user has clicked anywhere on the app. This is used to calculate the
        bounce rate.
        */
        document.addEventListener('click', () => this.userInteracted = true);
        
        checkTermsAgreed()
            .then(data => {
                this.currentlyLoading = false;
                if (data) { this.termsAgreed = true; }
            })
            .catch(error => console.error(error));

        /*
        Run for testing purposes only. Sets `User.SmartDocs_Terms_Agreed__c` to
        false for the current user.
        */
        // disagreeToTerms()
        //     .catch(error => {
        //         console.error(JSON.parse(JSON.stringify(error)));
        //     })
    }

    handleTermsAgreed() {
        this.termsAgreed = true;

        agreeToTerms()
            .then(() => {
                analyticsEvent({ eventName: 'Agreed to terms' });
            }) 
            .catch(error => console.error(error));
    }

    handleQuickLinkClick(event) {
        const quickLinkName = event.detail ? event.detail.value : null;

        const openOneLink = () => {
            this.quickLinksArray = this.quickLinksArray.map(link => {
                return link.name === quickLinkName
                ? { ...link, currentlyOpen: true }
                : { ...link, currentlyOpen: false };
            })
        }

        if (!this.overlayOpen) {
            /*
            The user is opening a Quick Link
            */
            this.overlayOpen = true;
            this.quickLinksTitle = quickLinkName;
            this.quickLinksData = {
                [quickLinkName]: QUICK_LINKS[quickLinkName]
            };
            openOneLink();
        } else {
            if (quickLinkName && this.quickLinksTitle !== quickLinkName) {
                /*
                The user is switching from one Quick Link to another
                */
                this.quickLinksData = {
                    [quickLinkName]: QUICK_LINKS[quickLinkName]
                };
                this.quickLinksTitle = quickLinkName;
                openOneLink();
            } else {
                /*
                The user is closing the Quick Link completely
                */
                this.overlayOpen = false;
                this.quickLinksTitle = null;
                this.quickLinksData = null;

                this.quickLinksArray = this.quickLinksArray.map(link => {
                    return { ...link, currentlyOpen: false };
                })
            }
        }
    }

    closeFeedbackModal() {
        this.feedbackFormOpen = false;
    }

    handleOpenFeedback() {
        this.feedbackFormOpen = true;
    }
}