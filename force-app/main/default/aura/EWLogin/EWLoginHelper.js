({
    qsToEventMap: {
        'startURL': 'e.c:setStartUrl'
    },

    mustBeRedirectedToLoginPage: function () {
        var partialUrl = window.location.pathname;
        return !partialUrl.includes("/login/");
    },

    redirectToLoginPage: function () {
        var loginPageUrl = this.getLoginPageUrl();
        window.location.replace(loginPageUrl);
    },

    getLoginPageUrl: function () {
        var result = "https://" + window.location.hostname;
        if (this.isSandbox()) {
            result += '/BrokerHub';
        }
        result += '/s/login/';
        return result;
    },

    isSandbox: function (url) {
        var productionUrl = 'broker-iq.azuruw.com';
        var currentUrl = window.location.href;
        return !currentUrl.includes(productionUrl);
    },

    handleLogin: function (component, event, helpler) {
        var username = component.find("username").get('v.value');
        var password = component.find("password").get('v.value');
        var action = component.get("c.login");
        var startUrl = component.get("v.startUrl");
        var forgotPassResetLinkMessage = component.get("v.forgotPassResetLinkMessage");

        action.setParams({username: username, password: password, startUrl: startUrl});
        action.setCallback(this, function (a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set("v.errorMessage", rtnValue + forgotPassResetLinkMessage);
                component.set("v.showError", true);
            }
        });
        $A.enqueueAction(action);
    },

    getIsUsernamePasswordEnabled: function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function (a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled', rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    getIsSelfRegistrationEnabled: function (component, event, helpler) {
        var action = component.get("c.getIsSelfRegistrationEnabled");
        action.setCallback(this, function (a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isSelfRegistrationEnabled', rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    getCommunityForgotPasswordUrl: function (component, event, helpler) {
        var action = component.get("c.getForgotPasswordUrl");
        action.setCallback(this, function (a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communityForgotPasswordUrl', rtnValue);

                let forgotPassResetLinkMessage = component.get('v.forgotPassResetLinkMessage').replace('[forgotPasswordUrl]', rtnValue);
                component.set('v.forgotPassResetLinkMessage', forgotPassResetLinkMessage);
            }
        });
        $A.enqueueAction(action);
    },

    getCommunitySelfRegisterUrl: function (component, event, helpler) {
        var action = component.get("c.getSelfRegistrationUrl");
        action.setCallback(this, function (a) {
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communitySelfRegisterUrl', rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
});