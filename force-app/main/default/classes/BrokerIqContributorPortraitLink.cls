global class BrokerIqContributorPortraitLink implements Nebula_Tools.BeforeUpdateI {

    global void handleBeforeUpdate(List<Broker_iQ_Contributor__c> oldList, List<Broker_iQ_Contributor__c> newList) {
        ImageFileToLinkService imageFileService = new ImageFileToLinkService(
			new List<ImageFileToLinkService.ImageFieldPair>{
				new ImageFileToLinkService.ImageFieldPair(Broker_iQ_Contributor__c.Portrait__c, Broker_iQ_Contributor__c.Portrait_URL__c)
		});

        for(Integer i=0; i < newList.size(); i++) {
            imageFileService.addPotentialObject(oldList[i], newList[i]);
        }

        if(!imageFileService.isEmpty()) {
            imageFileService.updateUrls();
        }

    }
}