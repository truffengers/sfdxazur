/*
* @name: TestObjectGenerator
* @description: Utility class to generate Broker iQ related test data
* @author: Nebula Consulting - aidan@nebulaconsulting.co.uk
* @lastChangedBy: Antigoni D'Mello 08/11/2018 EWH-153 Updated getContactNoInsert to provide our own implementation
*/
public class TestObjectGenerator extends Nebula_Tools.TestObjectGenerator {

    public override Contact getContactNoInsert() {
        Account account = TestDataFactory.createBrokerageAccount('BrokerIQ test parent account');
        insert account;
        Contact rval = new Contact();
        rval.AccountId = account.Id;
        rval.LastName = 'BrokerIQTester';
        rval.FirstName = 'Dave';
        return rval;
    }
    public override Contact getContact() {
        if(userContact == null) {
            if(testContact == null) {
                testContact = getContactNoInsert();
                insert testContact;
            }
            
            return testContact;
        } else {
            return userContact;
        }
    }

    private ReCaptcha__c reCaptcha;
    
    public ReCaptcha__c getReCaptcha() {
        if(reCaptcha == null) {
            reCaptcha = new ReCaptcha__c(Secret_Key__c = 'secret', Site_Key__c = 'site');
            
            insert reCaptcha;
        }
        return reCaptcha;
    }

    private CPD_Assessment__c cpdAssessment;
    
    public CPD_Assessment__c getCpdAssessment() {
        if(cpdAssessment == null) {
            cpdAssessment = new CPD_Assessment__c(Contact__c = getContact().Id, CPD_Test__c = getCpdTest().Id);
            
            insert cpdAssessment;
        }
        return cpdAssessment;
    }

    private IQ_Content__c webcastIqContent;
    
    public IQ_Content__c getWebcastIqContentNoInsert() {
        return new IQ_Content__c(RecordTypeId = Nebula_Tools.NamedSObjectCache.getRecordType('Brighttalk', 'IQ_Content__c').Id,
                                                 CPD_Time_minutes__c = 120,
                                                 Category__c = getCategory().Id);
    }
    public IQ_Content__c getWebcastIqContent() {
        if(webcastIqContent == null) {
            webcastIqContent = getWebcastIqContentNoInsert();
            
            insert webcastIqContent;
        }
        return webcastIqContent;
    }

    private CPD_Test__c cpdTest;
    
    public CPD_Test__c getCpdTest() {
        if(cpdTest == null) {
            cpdTest = new CPD_Test__c(Pass_Score__c = 75, IQ_Content__c = getWebcastIqContent().Id);
            
            insert cpdTest;
        }
        return cpdTest;
    }

    private CPD_Test_Question__c cpdTestQuestion;
    
    public CPD_Test_Question__c getCpdTestQuestion() {
        if(cpdTestQuestion == null) {
            cpdTestQuestion = new CPD_Test_Question__c(CPD_Test__c = getCpdTest().Id,
                                                       Question__c = 'How much wood would a woodchuck chuck?',
                                                       Answer__c = '1');
            
            insert cpdTestQuestion;
        }
        return cpdTestQuestion;
    }
    
    private Contact userContact;
    
    public Contact getUserContact() {
        if(userContact == null) {
            User thisUser = new User(Id = UserInfo.getUserId());
            System.runAs(thisUser) {
                userContact = new Contact(FirstName = UserInfo.getFirstName(),
                                          LastName = UserInfo.getLastName());
                insert userContact;
                thisUser.Contact_Id__c = userContact.Id;
                update thisUser;
            }
        }
        return userContact;
    }

    private BrightTALK__Channel__c btChannel;
    
    public BrightTALK__Channel__c getBtChannel() {
        if(btChannel == null) {
            btChannel = new BrightTALK__Channel__c(BrightTALK__Name__c = 'Test BT Channel', 
                                                   BrightTALK__Channel_Id__c = getBrightTalkApiSettings().Channel_Id__c);
            insert btChannel;
        }
        return btChannel;
    }
    
    private BrightTALK__Webcast__c btWebcast;
    
    public BrightTALK__Webcast__c getBtWebcast(Decimal webcastId) {
        if(btWebcast == null) {
            btWebcast = new BrightTALK__Webcast__c(
                        BrightTALK__Channel__c = getBtChannel().Id,
                        BrightTALK__Webcast_Id__c = webcastId,
                        BrightTALK__Name__c = 'Test BT Webcast',
                        BrightTALK__Start_Date__c = Datetime.newInstance(2016, 10, 18));
            insert btWebcast;
        }
        return btWebcast;
    }

    private Category__c category;
    
    public Category__c getCategory() {
        if(category == null) {
            category = new Category__c(Name = 'Insurance', 
                                       Colour__c = 'hokey',
                                       Icon__c = 'icon-umbrella',
                                       Placeholder_Image__c = '/assets/images/posts/recovering-losses-thumb.jpg');
            insert category;
        }
        return category;
    }
    
    private Broker_iQ_Contributor__c contributor;
    
    public Broker_iQ_Contributor__c getContributor(Boolean active) {
        if(contributor == null) {
            Contact c = getContact();
            c.Is_Broker_iQ_Contributor__c = true;
            update c;
            contributor = [SELECT Contact__c, Name 
                           FROM Broker_iQ_Contributor__c 
                           WHERE Contact__c = :c.Id];
            if(active) {
                contributor.Active__c = true;
                ContentVersion cv = getContentVersion('foo.txt', Blob.valueOf('foo'));
                contributor.Portrait__c = cv.ContentDocumentId;
                update contributor;
            }
        }
        return contributor;
    }

    private BrightTALK_API_Settings__c brightTalkApiSettings;
    
    public BrightTALK_API_Settings__c getBrightTalkApiSettings() {
        if(brightTalkApiSettings == null) {
            User thisUser = new User(Id = UserInfo.getUserId());
            System.runAs(thisUser) {
                brightTalkApiSettings = new BrightTALK_API_Settings__c(
                    Realm_ID__c = '234',
                    Endpoint__c = 'https://www.stage.brighttalk.net',
                    API_Key__c = 'iudabgiuda',
                    API_Endpoint__c = 'https://api.stage.brighttalk.net',
                    API_Secret__c = 'asugbadubg',
                    Realm_Initialisation_Vector__c = '895cc4ca3a5602dedd9175cce5d40792',
                    Realm_Encryption_Key__c = '17dd33b46ca71f66d31eabd4f0643e54',
                    Channel_Id__c = 1243
                );
                insert brightTalkApiSettings;
            }
        }
        return brightTalkApiSettings;
    }

    private BrightTALK__Webcast_Activity__c webcastActivity;
    
    public BrightTALK__Webcast_Activity__c getWebcastActivity(Datetime lastUpdated, BrightTALK__Webcast__c webcast) {
        if(webcastActivity == null) {
            webcastActivity = new BrightTALK__Webcast_Activity__c(
                BrightTALK__Total_Viewings__c = 2,
                BrightTALK__Last_Updated__c = lastUpdated,
                BrightTALK__Contact__c = getUserContact().Id,
                BrightTALK__Webcast__c = webcast.Id                
            );
            insert webcastActivity;
        }
        return webcastActivity;
    }

    public ContentVersion getContentVersion(String title, Blob data) {
        ContentVersion cv = new ContentVersion(Title = title, VersionData = data, PathOnClient = '/' + title);
        insert cv;
        return [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id];
    }

    private List<ConnectAPI.ManagedTopic> navigationalTopics;

    private ConnectAPI.ManagedTopic getNavigationalManagedTopic(String topicName) {
        ConnectAPI.ManagedTopic rval = new ConnectAPI.ManagedTopic();
        rval.topic = new  ConnectApi.Topic();
        rval.topic.name = topicName;
        return rval;
    }

    public List<ConnectAPI.ManagedTopic> getNavigationalTopics() {
        if(navigationalTopics == null) {
            navigationalTopics = new List<ConnectAPI.ManagedTopic>{
                    getNavigationalManagedTopic('Business Development'),
            getNavigationalManagedTopic('Compliance'),
            getNavigationalManagedTopic('Insurance'),
            getNavigationalManagedTopic('Insurtech'),
            getNavigationalManagedTopic('Risk management')
            };
        }
        return navigationalTopics;
    }
}