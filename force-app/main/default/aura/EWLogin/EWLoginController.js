({
    initialize: function (component, event, helper) {
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent": helper.qsToEventMap}).fire();

        component.set('v.forgotPassResetLinkMessage',
            ' If you need to reset your password please <strong><a href="[forgotPasswordUrl]">click here</a></strong>'
        );

        helper.getIsUsernamePasswordEnabled(component, event, helper)
        helper.getIsSelfRegistrationEnabled(component, event, helper);
        helper.getCommunityForgotPasswordUrl(component, event, helper);
        helper.getCommunitySelfRegisterUrl(component, event, helper);
        if (helper.mustBeRedirectedToLoginPage()) {
            helper.redirectToLoginPage();
        }
    },

    handleLogin: function (component, event, helpler) {
        event.preventDefault();
        helpler.handleLogin(component, event, helpler);
    },

    setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if (startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },

    onKeyUp: function (component, event, helpler) {
        //checks for "enter" key
        if (event.getParam('keyCode') === 13) {
            helpler.handleLogin(component, event, helpler);
        }
    },

    navigateToForgotPassword: function (cmp, event, helper) {
        var forgotPwdUrl = cmp.get("v.communityForgotPasswordUrl");
        if ($A.util.isUndefinedOrNull(forgotPwdUrl)) {
            forgotPwdUrl = cmp.get("v.forgotPasswordUrl");
        }
        var attributes = {url: forgotPwdUrl};
        $A.get("e.force:navigateToURL").setParams(attributes).fire();
    },

    navigateToSelfRegister: function (cmp, event, helper) {
        var selrRegUrl = cmp.get("v.communitySelfRegisterUrl");
        if (selrRegUrl == null) {
            selrRegUrl = cmp.get("v.selfRegisterUrl");
        }

        var attributes = {url: selrRegUrl};
        $A.get("e.force:navigateToURL").setParams(attributes).fire();
    }
})