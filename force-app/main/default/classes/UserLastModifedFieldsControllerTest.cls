@istest
public class UserLastModifedFieldsControllerTest {
    @istest
    public static void basic() {
        String oldEmail = UserInfo.getUserEmail();
        String newEmail = 'a+' + oldEmail;
        User u = new User(Id = UserInfo.getUserId(), Email = newEmail);
		Test.startTest();
        update u;
		Test.stopTest();
        
        UserLastModifedFieldsController controller = new UserLastModifedFieldsController();
        controller.userIdParam = UserInfo.getUserId();
        List<User_History__c> histories = controller.getModifiedFields();
        
        System.assertEquals(1, histories.size());
        System.assertEquals('Email', histories[0].Field_Developer_Name__c);
        System.assertEquals(newEmail, JSON.deserialize(histories[0].New_Value__c, String.class));
        System.assertEquals(oldEmail, JSON.deserialize(histories[0].Old_Value__c, String.class));
    }
    
}