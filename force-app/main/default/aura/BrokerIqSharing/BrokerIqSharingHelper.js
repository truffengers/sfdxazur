({
    downloadPdfFile : function(component,event,helper){
        var action = component.get('c.getURL');        
        action.setParam('iqContentId', component.get('v.recordId'));        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                debugger;
                var downloadLink = document.createElement("a");
                console.log('return url'+response.getReturnValue());
                downloadLink.href =response.getReturnValue();
                var pdfName=component.get('v.contentName');
                if(pdfName.length>200)
                    pdfName=pdfName.substring(0,200);
                downloadLink.download = pdfName;
                downloadLink.click();  
            }
        });        
        $A.enqueueAction(action);    
    },
})