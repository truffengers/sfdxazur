public class EWQuoteNotTakenUpController {
    @AuraEnabled
    public static Boolean isCommunity() {
        return Site.getSiteId() != null;
    }
}