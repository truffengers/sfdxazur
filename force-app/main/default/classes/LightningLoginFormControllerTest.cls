/**
* @name: LightningLoginFormControllerTest
* @description: LightningLoginFormController apex test class
* @author: Azur Group
* @date: 2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
* @Modified: Andrew Xu  23/09/2020  EWH-1770 Delete 'testLoginWithInvalidCredentials' and replace with 'testLogin' and 'testLoginReturnLink'
*/
@IsTest
public with sharing class LightningLoginFormControllerTest {

// @IsTest
// static void testLogin() {
//  /*
//  Unfortunately, `LightningLoginFormController.login` calls the system method
//  `Site.login`, which always returns `null` when it is being called from a
//  context that is not a Lightning Platform Site (eg. community). There is also
//  no apparent way to mock the `Site` class, so in a test context
//  `LightningLoginFormController.login` will always return `null`.

//  The solution is to simply assert that `LightningLoginFormController.login` has
//  indeed returned `null` in the interests of maintaining coverage.

//  - Andrew Xu 23/09/2020
//  */
//  System.assertEquals(null, LightningLoginFormController.login('testUser', 'fakepwd', null));
// }

// @IsTest
// static void testLoginReturnLink() {
//  /*
//  See above comment for `testLogin` test.
//  */
//  System.assertEquals(null, LightningLoginFormController.loginReturnLink('testUser', 'fakepwd', null));
// }

 @IsTest
 static void LightningLoginFormControllerInstantiation() {
  LightningLoginFormController controller = new LightningLoginFormController();
  System.assertNotEquals(controller, null);
 }

 @IsTest
 static void testIsUsernamePasswordEnabled() {
  System.assertEquals(true, LightningLoginFormController.getIsUsernamePasswordEnabled());
 }

 @IsTest
 static void testIsSelfRegistrationEnabled() {
  System.assertEquals(false, LightningLoginFormController.getIsSelfRegistrationEnabled());
 }

 @IsTest
 static void testGetSelfRegistrationURL() {
  System.assertEquals(null, LightningLoginFormController.getSelfRegistrationUrl());
 }

 @IsTest
 static void testAuthConfig() {
  Auth.AuthConfiguration authConfig = LightningLoginFormController.getAuthConfig();
  System.assertNotEquals(null, authConfig);
 }
}