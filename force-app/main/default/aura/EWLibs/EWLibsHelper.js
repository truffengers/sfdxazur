({
    initTT: function () {
        if (TT !== undefined) {
            if (TT.dropdownMenu !== undefined && TT.dropdownMenu.init !== undefined) {
                TT.dropdownMenu.init({
                    selector: '#js-subdomain-menu, #js-user-menu',
                    detectIt: detectIt
                });
            }

            if (TT.scrollBroadcaster !== undefined && TT.mediaMatcher !== undefined && TT.menuVisibilityManager !== undefined) {
                let scrollBroadcaster = TT.scrollBroadcaster(jQuery, {
                    window: window,
                    isLogging: false,
                    trailing: true
                });

                TT.mediaMatcher.init(_, {
                    mobile: '(max-width: 860px)',
                    desktop: '(min-width: 861px)'
                });

                TT.menuVisibilityManager.init({
                    scrollBroadcaster: scrollBroadcaster,
                    mediaMatcher: TT.mediaMatcher
                });

                scrollBroadcaster.init();
                scrollBroadcaster.enable();
            }

            if (TT.mediaMatcher !== undefined && TT.menu !== undefined && TT.brokerIqMenuItems !== undefined) {
                TT.mediaMatcher.init(_, {
                    mobile: '(max-width: 860px)',
                    desktop: '(min-width: 861px)'
                });

                TT.menu.init({
                    mediaMatcher: TT.mediaMatcher
                });

                TT.brokerIqMenuItems.init({
                    detectIt: detectIt,
                    menu: TT.menu,
                    item: TT.brokerIqMenuItem,
                    menuSelector: '#js-navigation',
                    itemClass: 'c-site-nav__item',
                    linkClass: 'c-site-nav__link',
                    subLinkClass: 'c-select-nav__link',
                    tabletStart: 750,
                    desktopStart: 1025
                });
            }
        }
    }
});