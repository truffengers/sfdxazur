/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 06/10/2017
 */

@isTest
private class BrokerIqHistoriesControllerTest {
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings;
    private static BrightTalkApiMock mock;
    static {
        tog.getUserContact();
        settings = tog.getBrightTalkApiSettings();
        mock = new BrightTalkApiMock(settings, 200);
        Nebula_API.NebulaApi.setMock(mock);

        BrokerIqHistoriesController.navigationalTopicMocks = tog.getNavigationalTopics();
    }

    @isTest
    static void noData() {
        Test.startTest();
        Map<Integer, List<BrokerIqHistoriesController.TopicData>> result = BrokerIqHistoriesController.getYearToPrimaryTopicToData();
        Test.stopTest();

        System.assertEquals(0, result.size());
    }

    @isTest
    static void withWebcastInSf() {
        BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
        IQ_Content__c iqc = tog.getWebcastIqContent();
        iqc.BrightTALK_Webcast__c = webcast.Id;
        iqc.Primary_Topic__c = 'Insurance';
        update iqc;

        BrightTALK__Webcast_Activity__c sfActivity = tog.getWebcastActivity(
                (DateTime)JSON.deserialize('"' + mock.responseParams.get('last_updated') + '"', Datetime.class),
                webcast);

        Test.startTest();
        Map<Integer, List<BrokerIqHistoriesController.TopicData>> result = BrokerIqHistoriesController.getYearToPrimaryTopicToData();
        Test.stopTest();

        System.assertEquals(1, result.size());

        List<BrokerIqHistoriesController.TopicData> theData = result.values()[0];

        System.assertEquals(BrokerIqHistoriesController.navigationalTopicMocks.size(), theData.size());

        Boolean sawInsurance = false;
        for(BrokerIqHistoriesController.TopicData thisData : theData) {
            if(thisData.primaryTopic.topic.name == 'Insurance') {
                sawInsurance = true;
                System.assertEquals(1, thisData.historyData.size());
                System.assertEquals(webcast.Id, thisData.historyData[0].webcast.Id);
            }
        }
        System.assert(sawInsurance);
    }

    @isTest
    static void withCpdTestNoNotSubmitted() {
        BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
        IQ_Content__c iqc = tog.getWebcastIqContent();
        iqc.BrightTALK_Webcast__c = webcast.Id;
        iqc.Primary_Topic__c = 'Insurance';
        update iqc;

        CPD_Test_Question__c question = tog.getCpdTestQuestion();
        CPD_Assessment__c assessment = tog.getCpdAssessment();

        BrightTALK__Webcast_Activity__c sfActivity = tog.getWebcastActivity(
                (DateTime)JSON.deserialize('"' + mock.responseParams.get('last_updated') + '"', Datetime.class),
                webcast);

        Test.startTest();
        Map<Integer, List<BrokerIqHistoriesController.TopicData>> result = BrokerIqHistoriesController.getYearToPrimaryTopicToData();
        Test.stopTest();

        System.assertEquals(1, result.size());

        List<BrokerIqHistoriesController.TopicData> theData = result.values()[0];

        System.assertEquals(BrokerIqHistoriesController.navigationalTopicMocks.size(), theData.size());

        Boolean sawInsurance = false;
        for(BrokerIqHistoriesController.TopicData thisData : theData) {
            if(thisData.primaryTopic.topic.name == 'Insurance') {
                sawInsurance = true;
                System.assertEquals(1, thisData.historyData.size());
                System.assertEquals(webcast.Id, thisData.historyData[0].webcast.Id);
            }
        }
        System.assert(sawInsurance);
    }

    @isTest
    static void noMatchingTopicData() {
        for(Integer i=0; i < BrokerIqHistoriesController.navigationalTopicMocks.size(); i++) {
            ConnectAPI.ManagedTopic thisTopic = BrokerIqHistoriesController.navigationalTopicMocks[i];

            if(thisTopic.topic.name == 'Insurance') {
                BrokerIqHistoriesController.navigationalTopicMocks.remove(i);
                break;
            }
        }


        BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
        IQ_Content__c iqc = tog.getWebcastIqContent();
        iqc.BrightTALK_Webcast__c = webcast.Id;
        iqc.Primary_Topic__c = 'Insurance';
        update iqc;

        BrightTALK__Webcast_Activity__c sfActivity = tog.getWebcastActivity(
                (DateTime)JSON.deserialize('"' + mock.responseParams.get('last_updated') + '"', Datetime.class),
                webcast);

        Test.startTest();
        Map<Integer, List<BrokerIqHistoriesController.TopicData>> result = BrokerIqHistoriesController.getYearToPrimaryTopicToData();
        Test.stopTest();


    }

}