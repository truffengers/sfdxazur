({
    receivedVisitorIdCallback : function(component, visitorId) {
        $A.getCallback( function() {
            var getProspectFromVisitorIdAction = component.get('c.isProspectFromVisitorId');
            
            getProspectFromVisitorIdAction.setParam('visitorId', visitorId);
            
            getProspectFromVisitorIdAction.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    var isProspect = response.getReturnValue();
                    if(!isProspect) {
                        component.set('v.needsNag', true);
                    } else {
                        document.cookie = 'letMeInComplete=true';
                    }
                }
            });
            
            $A.enqueueAction(getProspectFromVisitorIdAction);
        })();
    }
})