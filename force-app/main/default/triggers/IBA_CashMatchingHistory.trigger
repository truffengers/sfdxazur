/**
* @name: IBA_CashMatchingHistory
* @description: Trigger for the managed object c2g__codaCashMatchingHistory__c
*
*/

trigger IBA_CashMatchingHistory on c2g__codaCashMatchingHistory__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_CashMatchingHistoryTriggerHandler());
}