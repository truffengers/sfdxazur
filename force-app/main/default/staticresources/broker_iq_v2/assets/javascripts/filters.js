TT.filters = function($, selector) {

  'use strict';

  var categories = [];
  var groups = [];

  var visitingCategoryIndex;
  var visibleCategoryIndex;
  var queuedVisibleCategoryIndex;

  var CONCEALED  = 'concealed';
  var REVEALING  = 'revealing';
  var REVEALED   = 'revealed';
  var CHANGING   = 'changing';
  var CONCEALING = 'concealing';

  var state = CONCEALED;

  var $container;
  var $tagGroups;







  function createCategory(id, element, callback) {

    var OPENED_CLASS = 'is-expanded';
    var CLOSED_CLASS = 'is-collapsed';

    var $button;
    var isOpen;



    function init() {
      referenceElements();
      addListeners();
      isOpen = $button.hasClass(OPENED_CLASS);
    }

    function referenceElements() {
      $button = $(element).find('button');
    }

    function addListeners() {
      $button.click(clickHandler);
    }

    function clickHandler(event) {
      event.preventDefault();
      callback(id);
    }

    function open() {
      $button.removeClass(CLOSED_CLASS).addClass(OPENED_CLASS);
      isOpen = true;
    }

    function close() {
      $button.removeClass(OPENED_CLASS).addClass(CLOSED_CLASS);
      isOpen = false;
    }

    function getIsOpen() {
      return isOpen;
    }



    return {
      init         : init,
      open         : open,
      close        : close,
      getIsOpen    : getIsOpen
    };

  }





  function createGroup(id, element, resetCallback) {

    var isShowing;

    var $element;
    var $resetButton;

    var DISPLAY_CLASS = 'is-expanded';



    function init() {
      referenceElements();
      addListeners();
      isShowing = $element.hasClass(DISPLAY_CLASS);
    }

    function referenceElements() {
      $element = $(element);
      $resetButton = $element.find('button');
    }

    function addListeners() {
      $resetButton.click(resetHandler);
    }

    function resetHandler() {
      resetCallback();
    }

    function show() {
      $element.css({opacity:0}).addClass(DISPLAY_CLASS);
      isShowing = true;
    }

    function reveal(callback) {
      $element.transition({opacity: 1}, callback);
    }

    function hide() {
      $element.removeClass(DISPLAY_CLASS);
      isShowing = false;
    }


    return {
      init          : init,
      show          : show,
      hide          : hide,
      reveal        : reveal
    };

  }





  function referenceElements() {
    $container = $(selector);
    $tagGroups = $container.find('.c-filters__tag-groups');
  }

  function createCategories() {
    $container.find('.c-filters__list li').each(function(index, element) {
      var category = createCategory(index, element, categoryClickHandler);
      category.init();
      if(category.getIsOpen()) {
        visibleCategoryIndex = index;
        state = REVEALED;
      }
      categories.push(category);
    });
  }

  function createGroups() {
    $container.find('.c-filters__tags').each(function(index, element) {
      var group = createGroup(index, element, resetClickHandler);
      group.init();
      groups.push(group);
    });
  }

  function categoryClickHandler(index) {
    switch(state) {
    case CONCEALED:
      reveal(index);
      break;
    case REVEALED:
      index === visibleCategoryIndex ? conceal() : change(index);
      break;
    default:
      queuedVisibleCategoryIndex = index;
      break;
    }
  }

  function conceal() {
    state = CONCEALING;
    queuedVisibleCategoryIndex = undefined;
    categories[visibleCategoryIndex].close();
    $tagGroups.height($tagGroups.height());
    groups[visibleCategoryIndex].hide();
    visibleCategoryIndex = undefined;
    $tagGroups.transition({height: 0}, function() {
      $tagGroups.height('auto');
      state = CONCEALED;
      if(queuedVisibleCategoryIndex !== undefined) {
        reveal(queuedVisibleCategoryIndex);
      }
    });
  }

  function reveal(index) {
    state = REVEALING;
    queuedVisibleCategoryIndex = undefined;
    visibleCategoryIndex = index;
    categories[index].open();
    groups[index].show();
    var height = $tagGroups.height();
    $tagGroups.height(0);
    $tagGroups.transition({height: height}, function() {
      $tagGroups.height('auto');
      groups[index].reveal(function() {
        state = REVEALED;
        if(queuedVisibleCategoryIndex !== undefined && queuedVisibleCategoryIndex !== index) {
          change(queuedVisibleCategoryIndex);
        }
      });
    });
  }

  function change(index) {
    state = CHANGING;
    queuedVisibleCategoryIndex = undefined;
    categories[visibleCategoryIndex].close();
    categories[index].open();
    var startHeight = $tagGroups.height();
    groups[visibleCategoryIndex].hide();
    visibleCategoryIndex = index;
    groups[visibleCategoryIndex].show();
    var finishHeight = $tagGroups.height();
    $tagGroups.height(startHeight);
    $tagGroups.transition({height: finishHeight}, function() {
      $tagGroups.height('auto');
      groups[index].reveal(function() {
        state = REVEALED;
        if(queuedVisibleCategoryIndex !== undefined && queuedVisibleCategoryIndex !== visibleCategoryIndex) {
          change(queuedVisibleCategoryIndex);
        }
      });
    });
  }

  function resetClickHandler() {
    if(state !== REVEALED) { return; }
    categories[visibleCategoryIndex].close();
    conceal();
    visitingCategoryIndex = undefined;
    visibleCategoryIndex = undefined;
  }



  function init() {
    referenceElements();
    createCategories();
    createGroups();
  }

  init();

};