global class EWLightningForgotPasswordController {

    public static final String INVALID_USERNAME = 'Invalid username!';

    @AuraEnabled
    public static String forgotPassword(String username, String checkEmailUrl) {
        try {
            List<User> users = new List<User>([
                    SELECT Id, Username
                    FROM User
                    WHERE Username = :username AND Profile.Name = :EWConstants.PROFILE_EW_PARTNER_NAME
            ]);

            if (!Site.isValidUsername(username)) {
                return Label.Site.invalid_email;
            }
            if (users.isEmpty()) {
                return INVALID_USERNAME;
            }
            Site.forgotPassword(username);
            ApexPages.PageReference checkEmailRef = new PageReference(checkEmailUrl);
            if (!Test.isRunningTest()) {
                Aura.redirect(checkEmailRef);
            }
            return null;
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

}