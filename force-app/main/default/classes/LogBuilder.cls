/**
* @name: LogBuilder
* @description: Class with methods to create custom logs
* @author: Antigoni Tsouri atsouri@azuruw.com
* @date: 04/04/2017
* @Modified: Antigoni Tsouri     04/10/2017 Changes method to be more readable
* @Modified: Antigoni Tsouri     05/10/2017 Added new method createLogWithCodeInfo
* @Modified: Ignacio Sarmiento	 09/10/2017 Add 'createLog' method and comments
* @Modified: Antigoni D'Mello    25/08/2020 PR-107: Added createInformationLog method
*/
public with sharing class LogBuilder 
{
    //Properties
    public final static string DEBUG_MESSAGE = label.EWDebugMessageAzurLog;

    
    //Create Azur log record
	public Azur_Log__c createGenericLog(System.Exception exc)
    {
        Azur_Log__c log = new Azur_Log__c();
        String description = exc.getTypeName()+' '+exc.getMessage();
        log.Log_description__c = description.left(255);
        log.Log_message__c = exc.getStackTraceString();
		return log;
    }
    
    //Insert a log
    public static void insertLog(System.Exception exc)
	{	
        System.debug(DEBUG_MESSAGE + ': ' + exc.getMessage());
        String description = exc.getTypeName()+' '+exc.getMessage();
        Azur_Log__c log =  new Azur_Log__c(Log_description__c = description.left(255), Log_message__c = exc.getStackTraceString());
        insert log;
	}

    public Azur_Log__c createLogWithCodeInfo(System.Exception exc, String projectName, String className, String methodName)
    {
        Azur_Log__c log = new Azur_Log__c();
        String description = exc.getTypeName()+' '+exc.getMessage();
        log.Log_description__c = description.left(255);
        log.Log_message__c = exc.getStackTraceString();
        log.Project__c = projectName;
        log.Class__c = className;
        log.Method__c = methodName;
        return log;
    }

    public class Log {
        public LogMessage message { get; set; }
        public String className { get; set; }
        public String methodName { get; set; }
        public String description { get; set; }
        public String projectName { get; set; }
        public String subProjectName { get; set; }

        public Log(LogMessage message, String className, String methodName, String description, String projectName, String subProjectName) {
            this.message = message;
            this.className = className;
            this.methodName = methodName;
            this.description = description;
            this.projectName = projectName;
            this.subProjectName = subProjectName;
        }
    }

    public virtual class LogMessage {
        public Id recordId { get; set; }
        public String message { get; set; }
        public String type { get; set; }

        public LogMessage() {
        }

        public LogMessage(Id recordId, String type, String message) {
            this.recordId = recordId;
            this.type = type;
            this.message = message;
        }
    }

    public class ErrorLogMessage extends LogMessage {
        Integer lineNumber { get; set; }
        String stackTraceString { get; set; }
    
        public ErrorLogMessage(Id recordId, Integer lineNumber, String stackTraceString, String type, String message) {
            this.recordId = recordId;
            this.lineNumber = lineNumber;
            this.stackTraceString = stackTraceString;
            this.type = type;
            this.message = message;
        }
    }

    public static void createLog(Log logToCreate) {
        try {
            if(Limits.getDmlRows() < Limits.getLimitDmlRows() && 
                Limits.getDmlStatements() < Limits.getLimitDmlStatements()) {
                Azur_Log__c log = new Azur_Log__c();
                log.Class__c = logToCreate.className;
                log.Method__c = logToCreate.methodName;
                log.Log_description__c = logToCreate.description;
                log.Log_message__c	= JSON.serialize(logToCreate.message);
                log.Project__c = logToCreate.projectName;
                log.Subproject__c = logToCreate.subProjectName;
                insert log;
            } else {
                System.debug('The governor limit have already been exhausted and hence failed to create a log!');
            }
        } catch(DmlException ex) {
            System.debug('Something fatal has occurred and hence failed to create a log! Error: ' + ex.getMessage());
        }
    }

    public static void createLogs(List<Log> logsToCreate) {
        try {
            if(Limits.getDmlRows() < Limits.getLimitDmlRows() && 
                Limits.getDmlStatements() < Limits.getLimitDmlStatements()) {
                    List<Azur_Log__c> azurLogsToCreate = new List<Azur_Log__c>();
                    for(Log logToCreate :logsToCreate) {
                        Azur_Log__c log = new Azur_Log__c();
                        log.Class__c = logToCreate.className;
                        log.Method__c = logToCreate.methodName;
                        log.Log_description__c = logToCreate.description;
                        log.Log_message__c	= JSON.serialize(logToCreate.message);
                        log.Project__c = logToCreate.projectName;
                        log.Subproject__c = logToCreate.subProjectName;
                        azurLogsToCreate.add(log);
                    }
                    insert azurLogsToCreate;
            } else {
                System.debug('The governor limit have already been exhausted and hence failed to create a log!');
            }
        } catch(DmlException ex) {
            System.debug('Something fatal has occurred and hence failed to create a log! Error: ' + ex.getMessage());
        }
    }

    // Return a log with customised information and no exception input
    public static Azur_Log__c createPhoenixInformationLog(Id phoenixStaging,
                                                        String description,
                                                        String message,
                                                        String projectName,
                                                        String className,
                                                        String methodName) {
        return new Azur_Log__c(Log_description__c = description,
                                Log_message__c = message,
                                Project__c = projectName,
                                Class__c = className,
                                Method__c = methodName,
                                PhoenixStaging__c = phoenixStaging);
    }
}