/**
* @name: CommunitiesLandingControllerTest
* @description: An apex page controller that takes the user to the right start page based on credentials or lack
*               thereof
* @author: Azur Group
* @date: 2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
*/
@IsTest public with sharing class CommunitiesLandingControllerTest {
    @IsTest
    public static void testCommunitiesLandingController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
        PageReference pageRef = controller.forwardToStartPage();
        //PageRef is either null or an empty object in test conhttps://cs83.salesforce.com/_ui/common/apex/debug/ApexCSIPage#text
        if(pageRef != null){
            String url = pageRef.getUrl();
            if(url != null){
                System.assertEquals(true, String.isEmpty(url));
                //show up in perforce
            }
        }
    }
}