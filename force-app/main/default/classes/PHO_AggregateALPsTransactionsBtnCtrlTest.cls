/**
* @name: PHO_AggregateALPsTransactionsBtnCtrlTest
* @description: Test class for PHO_AggregateALPsTransactionsButtonCtrl class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 08/01/2020
*/
@IsTest
public class PHO_AggregateALPsTransactionsBtnCtrlTest{

    private static final String ACCOUNTING_BROKER = 'Accounting Broker Test';
    private static final String POLICY_NUMBER_1 = '1';

    @TestSetup
    private static void createTestData() {
        IBA_TestDataFactory.createCustomSettings();
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        Account accountingBroker = IBA_TestDataFactory.createAccount(true, ACCOUNTING_BROKER, IBA_UtilConst.ACCOUNT_RT_BROKER);
        Asset policy = IBA_TestDataFactory.createPolicy(false, POLICY_NUMBER_1, settings.IBA_DummyAccount__c);
        policy.IBA_AccountingBroker__c = accountingBroker.Id;
        policy.Policy_Number__c = POLICY_NUMBER_1;
        insert policy;
        IBA_PolicyTransaction__c policyTransaction = IBA_TestDataFactory.createPolicyTransaction(false, policy.Id);
        policyTransaction.IBA_EffectiveDate__c = Date.newInstance(2019, 1, 1);
        insert policyTransaction;
    }

    @IsTest
    private static void testAggregateALPsTransactionsSuccess() {
        // Prepare data
        List<IBA_PolicyTransaction__c> policyTransactions = [SELECT Id FROM IBA_PolicyTransaction__c];

        // Perform test
        Test.startTest();
        Test.setCurrentPage(Page.PHO_AggregateALPsTransactionsButton);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(policyTransactions);
        PHO_AggregateALPsTransactionsButtonCtrl controller = new PHO_AggregateALPsTransactionsButtonCtrl(standardSetController);
        PageReference result = controller.aggregateALPsTransactions();
        Test.stopTest();

        // Verify results
        Integer jobsQueued = [SELECT COUNT() FROM AsyncApexJob WHERE ApexClass.Name = :PHO_AggregateALPsTransactionsJob.class.getName()];

        System.assertEquals(standardSetController.cancel().getUrl(), result.getUrl(), 'Should redirect to the original page');
        System.assertEquals(1, jobsQueued, 'One job for PHO_AggregateALPsTransactionsJob class should be enqueued');
    }

    @IsTest
    private static void testAggregateALPsTransactionsErrorMessage() {
        // Prepare data
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        settings.PHO_ALPsDataAggregationRecordsLimit__c = 0;
        update settings;
        List<IBA_PolicyTransaction__c> policyTransactions = [SELECT Id FROM IBA_PolicyTransaction__c];

        // Perform test
        Test.startTest();
        Test.setCurrentPage(Page.PHO_AggregateALPsTransactionsButton);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(policyTransactions);
        PHO_AggregateALPsTransactionsButtonCtrl controller = new PHO_AggregateALPsTransactionsButtonCtrl(standardSetController);
        PageReference result = controller.aggregateALPsTransactions();
        Test.stopTest();

        // Verify results
        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertEquals(null, result, 'Should not redirect');
        System.assertEquals(1, pageMessages.size(), 'There should be one error message on the page');
        System.assertEquals(ApexPages.Severity.ERROR, pageMessages[0].getSeverity(), 'There should be one error message on the page');
    }

    @IsTest
    private static void testGoBack() {
        // Prepare data
        List<IBA_PolicyTransaction__c> policyTransactions = [SELECT Id FROM IBA_PolicyTransaction__c];

        // Perform test
        Test.startTest();
        Test.setCurrentPage(Page.PHO_AggregateALPsTransactionsButton);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(policyTransactions);
        PHO_AggregateALPsTransactionsButtonCtrl controller = new PHO_AggregateALPsTransactionsButtonCtrl(standardSetController);
        PageReference result = controller.goBack();
        Test.stopTest();

        // Verify results
        System.assertEquals(standardSetController.cancel().getUrl(), result.getUrl(), 'Should redirect to the original page');
    }
}