/**
* @name: EWVlocityDocumentSpecifiedItems
* @description: Helper class for the Specified Items Section
* @author: Steve Loftus sloftus@azuruw.com
* @date: 12/12/2018
*/
global with sharing class EWVlocityDocumentSpecifiedItems implements vlocity_ins.VlocityOpenInterface {

    private Integer maxLogSize = 32767;
    private static String CLASS_NAME = EWVlocityDocumentSpecifiedItems.class.getName();
    private static String METHOD_INVOKE_METHOD = 'invokeMethod';
    private static String VLOCITY_DESCRIPTION = 'Vlocity Document Remote Action call';

    private static String HEADING = 'Specified Items';
    private static String BORDER_STYLE_BASE = 'border: .5px solid black;border-collapse: collapse;';
    private static String FONT_STYLE_BASE = 'font-size: 10pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String FONT_STYLE_HEADING = 'font-size: 12pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String ROW_STYLE_BASE = 'height: 20px;';
    private static String QUOTE_PREFIX = '0Q0';
    private static String NONE = 'None';

    private static String CONTENTS_KEY = 'contents';
    private static String JEWELLERY_KEY = 'jewelleryAndWatches';
    private static String ART_KEY = 'art';
    private static String CYCLE_KEY = 'pedalCycles';

    private static String CONTENTS_VALUE = 'Contents';
    private static String JEWELLERY_VALUE = 'Jewellery & Watches';
    private static String ART_VALUE = 'Art & Collectables';
    private static String CYCLE_VALUE = 'Pedal Cycles';

    private static Set<String> itemKeySet = new Set<String>{ CONTENTS_KEY, JEWELLERY_KEY, ART_KEY, CYCLE_KEY };

    public EWVlocityDocumentSpecifiedItems() {
        
    }

    public Boolean invokeMethod(
                            String methodName,
                            Map<String, Object> inputMap,
                            Map<String, Object> outputMap,
                            Map<String, Object> optionMap) {

        Boolean result = true;

        Azur_Log__c traceLog = new Azur_Log__c();

        traceLog.Class__c = CLASS_NAME;
        traceLog.Method__c = METHOD_INVOKE_METHOD;
        traceLog.Log_description__c = VLOCITY_DESCRIPTION;

        string logMessage = 'methodName:' + '\n' +
                methodName + '\n\n' +
                'inputMap:' + '\n' +
                JSON.serializePretty(inputMap) + '\n\n' +
                'optionMap:' + '\n' +
                JSON.serializePretty(optionMap);

        if (logMessage.length() > maxLogSize ) {
            traceLog.Log_message__c = logMessage.substring(0, maxLogSize);
        } else {
            traceLog.Log_message__c = logMessage;
        }

        try {

            if (methodName == EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD) {

                buildDocumentSectionContent(inputMap, outputMap, optionMap);

            } else {

                result = false;
            }

        } catch(Exception e) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(e);
            log.Class__c = CLASS_NAME;
            log.Method__c = METHOD_INVOKE_METHOD;
            insert log;

            result = false;

        } finally {

            string additionalLogMessage = traceLog.Log_message__c + '\n\n' + 'outputMap:' + '\n' + JSON.serializePretty(outputMap);

            if (additionalLogMessage.length() > maxLogSize ) {
                traceLog.Log_message__c = additionalLogMessage.substring(0, maxLogSize);
            } else {
                traceLog.Log_message__c = additionalLogMessage;
            }

            insert traceLog;

        }

        return result;
    }

    private void buildDocumentSectionContent (
                                        Map<String, Object> inputMap,
                                        Map<String, Object> outputMap,
                                        Map<String, Object> optionMap) {

        String dataJSON = (String)inputMap.get('dataJSON');
        Map<String, Object> documentData = (Map<String, Object>)JSON.deserializeUntyped(dataJSON);
        Map<String, Object> contextData = (Map<String, Object>)documentData.get('contextData');

        Id contextId = (Id) contextData.get('contextId');
        Schema.sObjectType objectAPIName = contextId.getSobjectType();

        If (objectAPIName == Quote.sObjectType)
            {
            buildDocumentSectionContentInsurableItem(inputMap, outputMap, optionMap);
            }

         If (objectAPIName == Asset.sObjectType)
            {
            buildDocumentSectionContentInsuredItem(inputMap,outputMap,optionMap);
            }
    }

     private void buildDocumentSectionContentInsurableItem(Map<String, Object> inputMap,
            Map<String, Object> outputMap,
            Map<String, Object> optionMap)
     {

         String dataJSON = (String) inputMap.get('dataJSON');
         Map<String, Object> documentData = (Map<String, Object>) JSON.deserializeUntyped(dataJSON);
         Map<String, Object> contextData = (Map<String, Object>) documentData.get('contextData');
         Id contextId = (Id) contextData.get('contextId');


         Id recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                'vlocity_ins__InsurableItem__c',
                EWConstants.INSURABLE_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM);

        String query = 'SELECT Name, EW_Type__c, EW_MarketValue__c FROM vlocity_ins__InsurableItem__c';
        query += ' WHERE ' + ((((String)contextId).startsWith(QUOTE_PREFIX)) ? 'EW_Quote__c' : 'EW_Policy__c');
        query += ' =: contextId';
        query += ' AND RecordTypeId =: recordTypeId';
        query += ' AND EW_Active__c = True';
        query += ' ORDER BY EW_MarketValue__c DESC';

        Map<String, List<vlocity_ins__InsurableItem__c>> itemListByItemTypeMap = new Map<String, List<vlocity_ins__InsurableItem__c>>();

        for (vlocity_ins__InsurableItem__c item : Database.query(query)) {

            List<vlocity_ins__InsurableItem__c> itemList = (itemListByItemTypeMap.containsKey(item.EW_Type__c))
                                                            ? itemListByItemTypeMap.get(item.EW_Type__c)
                                                            : new List<vlocity_ins__InsurableItem__c>();

            itemList.add(item);

            itemListByItemTypeMap.put(item.EW_Type__c, itemList);
        }

        String content = '';

        Map<String, String> headerMap = new Map<String, String>();
        headerMap.put(CONTENTS_KEY, CONTENTS_VALUE);
        headerMap.put(JEWELLERY_KEY, JEWELLERY_VALUE);
        headerMap.put(ART_KEY, ART_VALUE);
        headerMap.put(CYCLE_KEY, CYCLE_VALUE);

        Map<String, String> subHeaderMap = new Map<String, String>();
        subHeaderMap.put('1', 'Name');
        subHeaderMap.put('2', 'Value');

        for (String key : itemKeySet) {

            content += '<p><span style=\"' + FONT_STYLE_BASE + '\">';
            content += '<viawrapper><strong>' + headerMap.get(key) + '</strong></viawrapper>';
            content += '</span></p>';

            if (itemListByItemTypeMap.containsKey(key)) {

                content += '<table style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 100%\">';
                content += '<thead><tr>';

                for (String subKey : subHeaderMap.keySet()) {
                    
                    content += '<th style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 50%;\">';
                    content += '<viawrapper>' + subHeaderMap.get(subKey) + '</viawrapper></th>';
                }

                content += '</tr></thead>';
                content += '<tbody>';

                List<vlocity_ins__InsurableItem__c> itemList = itemListByItemTypeMap.get(key);

                for (vlocity_ins__InsurableItem__c item : itemList) {

                    content += '<tr style=\"' + ROW_STYLE_BASE + '\">';
                    content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 50%;\">';
                    content += '<span style=\"' + FONT_STYLE_BASE + '\">';
                    content += '<viawrapper>' + item.Name + '</viawrapper>';
                    content += '</span></td>';
                    content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 50%;\">';
                    content += '<span style=\"' + FONT_STYLE_BASE + '\">';

                    Decimal marketValue = item.EW_MarketValue__c;

                    String marketValueString = marketValue.format().contains('.')
                                                ? marketValue.format()
                                                : marketValue.format() + '.00';

                    content += '<viawrapper>£' + marketValueString + '</viawrapper>';
                    content += '</span></td>';
                    content += '</tr>';
                }

                content += '</tbody></table>';

            } else {

                content += '<p><span style=\"' + FONT_STYLE_BASE + '\">';
                content += '<viawrapper>' + NONE + '</viawrapper>';
                content += '</span></p>';
            }
        }
    outputMap.put('sectionContent', content);
    }

    private void buildDocumentSectionContentInsuredItem(Map<String, Object> inputMap,
            Map<String, Object> outputMap,
            Map<String, Object> optionMap)
    {

        String dataJSON = (String) inputMap.get('dataJSON');
        Map<String, Object> documentData = (Map<String, Object>) JSON.deserializeUntyped(dataJSON);
        Map<String, Object> contextData = (Map<String, Object>) documentData.get('contextData');
        Id contextId = (Id) contextData.get('contextId');


        Id recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                'vlocity_ins__AssetInsuredItem__c',
                EWConstants.INSURED_ITEM_RECORD_TYPE_EW_SPECIFIED_ITEM);

        String query = 'SELECT Name, vlocity_ins__Type__c, vlocity_ins__MarketValue__c FROM vlocity_ins__AssetInsuredItem__c';
        query += ' WHERE ' + ((((String) contextId).startsWith(QUOTE_PREFIX)) ? 'EW_Quote__c' : 'vlocity_ins__PolicyAssetId__c');
        query += ' =: contextId';
        query += ' AND RecordTypeId =: recordTypeId';
        //query += ' AND EW_Active__c = True';
        query += ' AND Name NOT IN (\'insuredEntity\', \'insuredProperty\' )';
        query += ' ORDER BY vlocity_ins__MarketValue__c DESC';

        Map<String, List<vlocity_ins__AssetInsuredItem__c>> itemListByItemTypeMap = new Map<String, List<vlocity_ins__AssetInsuredItem__c>>();

        for (vlocity_ins__AssetInsuredItem__c item : Database.query(query)) {

            List<vlocity_ins__AssetInsuredItem__c> itemList = (itemListByItemTypeMap.containsKey(item.vlocity_ins__Type__c))
                    ? itemListByItemTypeMap.get(item.vlocity_ins__Type__c)
                    : new List<vlocity_ins__AssetInsuredItem__c>();

            itemList.add(item);

            itemListByItemTypeMap.put(item.vlocity_ins__Type__c, itemList);
        }

        String content = '';

        Map<String, String> headerMap = new Map<String, String>();
        headerMap.put(CONTENTS_KEY, CONTENTS_VALUE);
        headerMap.put(JEWELLERY_KEY, JEWELLERY_VALUE);
        headerMap.put(ART_KEY, ART_VALUE);
        headerMap.put(CYCLE_KEY, CYCLE_VALUE);

        Map<String, String> subHeaderMap = new Map<String, String>();
        subHeaderMap.put('1', 'Name');
        subHeaderMap.put('2', 'Value');

        for (String key : itemKeySet) {

            content += '<p><span style=\"' + FONT_STYLE_BASE + '\">';
            content += '<viawrapper><strong>' + headerMap.get(key) + '</strong></viawrapper>';
            content += '</span></p>';

            if (itemListByItemTypeMap.containsKey(key)) {

                content += '<table style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 100%\">';
                content += '<thead><tr>';

                for (String subKey : subHeaderMap.keySet()) {

                    content += '<th style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 50%;\">';
                    content += '<viawrapper>' + subHeaderMap.get(subKey) + '</viawrapper></th>';
                }

                content += '</tr></thead>';
                content += '<tbody>';

                List<vlocity_ins__AssetInsuredItem__c> itemList = itemListByItemTypeMap.get(key);

                for (vlocity_ins__AssetInsuredItem__c item : itemList) {

                    content += '<tr style=\"' + ROW_STYLE_BASE + '\">';
                    content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 50%;\">';
                    content += '<span style=\"' + FONT_STYLE_BASE + '\">';
                    content += '<viawrapper>' + item.Name + '</viawrapper>';
                    content += '</span></td>';
                    content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 50%;\">';
                    content += '<span style=\"' + FONT_STYLE_BASE + '\">';

                    Decimal marketValue = item.vlocity_ins__MarketValue__c;

                    String marketValueString = marketValue.format().contains('.')
                            ? marketValue.format()
                            : marketValue.format() + '.00';

                    content += '<viawrapper>£' + marketValueString + '</viawrapper>';
                    content += '</span></td>';
                    content += '</tr>';
                }

                content += '</tbody></table>';

            } else {

                content += '<p><span style=\"' + FONT_STYLE_BASE + '\">';
                content += '<viawrapper>' + NONE + '</viawrapper>';
                content += '</span></p>';
            }
        }
        outputMap.put('sectionContent', content);
    }
}