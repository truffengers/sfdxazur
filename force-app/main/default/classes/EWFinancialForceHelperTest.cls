/*
* @name: EWFinancialForceHelperTest
* @description: Test class for Policy Transaction login for smarthome policies.
* @author: Unknown
* @lastChangedBy: Antigoni D'Mello  06/05/2021  PR-138: Assert Azur DD Fee is zero for non direct debit policies
*/
@IsTest
private class EWFinancialForceHelperTest {

    static testMethod void testCreateFFPolicyRecords() {
        Asset policy = [
                SELECT Id
                FROM Asset
                LIMIT 1
        ];

        Test.startTest();
        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        update policy;
        Test.stopTest();

        List<IBA_PolicyTransaction__c> policyTransactions = new List<IBA_PolicyTransaction__c>([
                SELECT Id, IBA_TotalAzurDDFee__c, (
                        SELECT Id
                        FROM FF_Policy_Carrier_Lines__r
                ), (
                        SELECT Id, IBA_AzurDDFee__c
                        FROM Policy_Transaction_Lines__r
                )
                FROM IBA_PolicyTransaction__c
                WHERE IBA_ActivePolicy__c = :policy.Id
        ]);

        System.assertEquals(1, policyTransactions.size());
        System.assertNotEquals(0, policyTransactions.get(0).FF_Policy_Carrier_Lines__r.size());
        System.assertNotEquals(0, policyTransactions.get(0).Policy_Transaction_Lines__r.size());
        System.assertEquals(0, policyTransactions.get(0).IBA_TotalAzurDDFee__c);
        System.assertEquals(0, policyTransactions.get(0).Policy_Transaction_Lines__r.get(0).IBA_AzurDDFee__c);
    }

    @TestSetup
    static void setup() {
        EWQuoteHelperTest.setupCarrierAccountsAndCoverages();

        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );
        Account mgaFeeAccount = EWAccountDataFactory.createBrokerAccount(null, false);
        mgaFeeAccount.EW_DRCode__c = EWConstants.MGA_FEE_ACCOUNT_DR_CODE;
        insert new List<Account>{
                broker, insured, mgaFeeAccount
        };

        Product2 product1 = EWProductDataFactory.createProduct('Home Emergency', false);
        Product2 product2 = EWProductDataFactory.createProduct('Contents', false);
        insert new List<Product2>{
                product1, product2
        };

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
		Quote quoteDD = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
        
        PricebookEntry pbe1 = new PricebookEntry (Product2Id = product1.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
        PricebookEntry pbe2 = new PricebookEntry (Product2Id = product2.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);

        insert new List<PricebookEntry>{
                pbe1, pbe2
        };
		
        QuoteLineItem qli1 = new QuoteLineItem();
        qli1.QuoteId = quote.Id;
        qli1.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        qli1.Product2Id = product1.Id;
        qli1.PricebookEntryId = pbe1.Id;
        qli1.Quantity = 1;
        qli1.UnitPrice = pbe1.UnitPrice;
        qli1.EW_Transactional_PremiumGrossExIPT__c = pbe1.UnitPrice;

        QuoteLineItem oli2 = new QuoteLineItem();
        oli2.QuoteId = quote.Id;
        oli2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        oli2.Product2Id = product2.Id;
        oli2.PricebookEntryId = pbe1.Id;
        oli2.Quantity = 1;
        oli2.UnitPrice = pbe1.UnitPrice;
        oli2.EW_Transactional_PremiumGrossExIPT__c = pbe1.UnitPrice;
        
        QuoteLineItem qliDD = new QuoteLineItem();
        qliDD.QuoteId = quoteDD.Id;
        qliDD.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        qliDD.Product2Id = product1.Id;
        qliDD.PricebookEntryId = pbe1.Id;
        qliDD.Quantity = 1;
        qliDD.UnitPrice = pbe1.UnitPrice;
        qliDD.EW_Transactional_PremiumGrossExIPT__c = pbe1.UnitPrice;

        insert new List<QuoteLineItem>{
                qli1, oli2, qliDD
        };    
            
        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.IBA_InsuredName__c = 'insName';
        policy.Status = 'Draft';
        policy.EW_FixedExpense__c = 40;
        policy.IBA_InsurancePremiumTaxRate__c = 0;
        policy.IBA_MGACommissionRate__c = 0;
        policy.IBA_BrokerCommissionRate__c = 0;
        policy.EW_Quote__c = quote.Id;
        
        Asset policyDD = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policyDD.IBA_InsuredName__c = 'insNameDD';
        policyDD.Status = 'Draft';
        policyDD.EW_FixedExpense__c = 40;
        policyDD.IBA_InsurancePremiumTaxRate__c = 0;
        policyDD.IBA_MGACommissionRate__c = 0;
        policyDD.IBA_BrokerCommissionRate__c = 0;
        policyDD.EW_Quote__c = quoteDD.Id;
        policyDD.EW_Payment_Method__c = EWConstants.DIRECT_DEBIT_PAYMENT_METHOD;
		
        insert new List<Asset>{
                policy, policyDD
        };
            
        quote.EW_Policy__c = policy.Id;
        quoteDD.EW_Policy__c = policyDD.Id;
        quoteDD.EW_DD_Interest_Percentage__c = 6;
        update new List<quote>{
                quote, quoteDD
        };
        
        vlocity_ins__AssetCoverage__c coverage1 = new vlocity_ins__AssetCoverage__c();
        coverage1.vlocity_ins__PolicyAssetId__c = policy.Id;
        coverage1.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverage1.vlocity_ins__Product2Id__c = product1.Id;
        coverage1.CurrencyIsoCode = 'GBP';

        vlocity_ins__AssetCoverage__c coverage2 = new vlocity_ins__AssetCoverage__c();
        coverage2.vlocity_ins__PolicyAssetId__c = policy.Id;
        coverage2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverage2.vlocity_ins__Product2Id__c = product2.Id;
        coverage2.CurrencyIsoCode = 'GBP';
        
        vlocity_ins__AssetCoverage__c coverageDD = new vlocity_ins__AssetCoverage__c();
        coverageDD.vlocity_ins__PolicyAssetId__c = policyDD.Id;
        coverageDD.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverageDD.vlocity_ins__Product2Id__c = product1.Id;
        coverageDD.CurrencyIsoCode = 'GBP';

        insert new List<vlocity_ins__AssetCoverage__c>{
                coverage1, coverage2, coverageDD
        };
    }

    static testMethod void testFindOriginalPolicyBusinessType() {

        Asset policy = [
                SELECT Id, EW_TypeofTransaction__c
                FROM Asset
                LIMIT 1
        ];

        policy.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        update policy;

        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );
        insert new List<Account>{
                broker, insured
        };
        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true); 

        Asset policy2 = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy2.vlocity_ins__PreviousVersionId__c = policy.Id;
        policy2.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_MTA;
        insert policy2;

        Test.startTest();
        String result = EWFinancialForceHelper.findOriginalPolicyBusinessType(policy2.Id);
        Test.stopTest();

        System.assertEquals(EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL, result);
    }
    
    static testMethod void testDirectDebitPolicies() {
        Asset policy = [
                SELECT Id, EW_Payment_Method__c, IBA_InsuredName__c
                FROM Asset
            	WHERE IBA_InsuredName__c = 'insNameDD'
                LIMIT 1
        ];
		
        Test.startTest();
        policy.EW_Payment_Method__c = EWConstants.DIRECT_DEBIT_PAYMENT_METHOD;
        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        update policy;
        Test.stopTest();
		
        List<IBA_PolicyTransaction__c> policyTransactions = new List<IBA_PolicyTransaction__c>([
                SELECT Id, IBA_TotalAzurDDFee__c, IBA_TotalDirectDebitFee__c, IBA_TotalPCLDDFee__c 
                FROM IBA_PolicyTransaction__c
                WHERE IBA_ActivePolicy__c = :policy.Id
        ]);
        System.debug('====> PT '+policyTransactions);
        System.assertEquals(0.52, policyTransactions[0].IBA_TotalAzurDDFee__c);
        System.assertEquals(3, policyTransactions[0].IBA_TotalDirectDebitFee__c);
        System.assertEquals(2.48, policyTransactions[0].IBA_TotalPCLDDFee__c);
    }
}