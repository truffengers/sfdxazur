({
	doInit: function (component, event, helper) {
		const recordId = component.get('v.recordId');
		let action = component.get("c.loadRecordData");
		action.setParam('id', recordId);

		action.setCallback(this, function (response) {
			const responseJson = response.getReturnValue();

			if (responseJson && responseJson.hideButtons){
				component.set('v.hiddenButtons', responseJson.hideButtons);
			}

			// There are only 3 buttons on the Record Banner in total.
			// If all 3 buttons returned in responseJson, hide the parent
			// of the buttons too.
			if (responseJson && responseJson.hideButtons
				&& responseJson.hideButtons.length >= 3) {
				component.set('v.hideButtonsParent', true);
			}
		});
		$A.enqueueAction(action);
	}
});