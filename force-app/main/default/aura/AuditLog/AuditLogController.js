({
	init : function(component, event, helper) {
		component.set('v.columns', [
            {label: 'Field', fieldName: 'fieldLabel__c', type: 'text'},
            {label: 'Previous Value', fieldName: 'previousValue__c', type: 'text'},
            {label: 'New Value', fieldName: 'newValue__c', type: 'text'},
            {label: 'Changed By', fieldName: 'changedByUserName__c', type: 'text' },
            {label: 'Timestamp', fieldName: 'timestamp__c', type: 'datetime'}
        ]);
        
        // Get a reference to the getWeather() function defined in the Apex controller
		var action = component.get("c.getAuditLogs");
        action.setParams({
            "sobjectRecordId": component.get("v.recordId"),
            "sobjectType": component.get("v.sobjectType")
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS'){
                component.set('v.data', response.getReturnValue());
                $A.get('e.force:refreshView').fire();
            } else {
                 //do something
                 console.log('Everything is not awesome.');
            }
            
        });
        // Invoke the service
        $A.enqueueAction(action);
	}
})