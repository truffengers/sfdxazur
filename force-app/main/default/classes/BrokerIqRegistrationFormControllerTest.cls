/*
* @name: BrokerIqRegistrationFormControllerTest
* @description: BrokerIqRegistrationFormController apex test controller
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 14/01/2018
* @modifiedBy: Ignacio Sarmiento 	23/04/2018 	BIQ-4 - test functionality if an account already exists, add the contact to the existing account
*/
@isTest
public class BrokerIqRegistrationFormControllerTest {
    private static final String USER_SYSTEMADMIN_LASTNAME = 'Testing last name system admin';
    private static final String BROKER_IQ2_GUEST_USER = 'Testing biq2 guest user';
    private static final String FIRSTNAME_EXAMPLE = 'first name';
    private static final String LASTNAME_EXAMPLE = 'last name';
    private static final String COMPANY_EXAMPLE = 'Test class Azur';
    private static final String JOBTITLE_EXAMPLE = 'IT';
    private static final String EMAIL_EXAMPLE = 'email@hotmail.com';
    private static final String PHONE_EXAMPLE = '0789123456';
    private static final String EXISTING_ACCOUNT_NAME = 'Existing acc';
    private static final String COUNTRY_EXAMPLE = 'United Kingdom';


    @testSetup
    static void setup() {
        //Insert system administrator
        User systemAdminUser = BrokerIqTestDataFactory.createSystemAdmistratorUser(USER_SYSTEMADMIN_LASTNAME);
        insert systemAdminUser;
        
        System.runAs(systemAdminUser) {
            //Insert users 
            list<User> usersToInsert = new list<User>();
            User biq2GuestUserInsideCIIGroup = BrokerIqTestDataFactory.createBrokerIq2GuestUser(BROKER_IQ2_GUEST_USER);
            usersToInsert.add(biq2GuestUserInsideCIIGroup);
            insert usersToInsert;
            //Insert business account
            Account accountToInsert = BrokerIqTestDataFactory.createAgencyBrokerageAccount(EXISTING_ACCOUNT_NAME);
            insert accountToInsert;
        } 
    }
    
    private static User getUser(String lastName){
        return [SELECT Id FROM User WHERE LastName =: lastName AND isActive = true LIMIT 1];
    }

    testMethod static void testRegisterBrokerIq2User(){
        User guestUser = getUser(BROKER_IQ2_GUEST_USER);
        System.runAs(guestUser) {
            //Set params
            String firstName = 'test class name';
            String lastName = 'test class last name';
    		String jobTitle = 'IT'; 
        	String company = 'test class Azur';
    		String email =  'email@hotmail.com'; 
    		String phone = '0789123456';
    		String country = 'United Kingdom';
            //Call class
            String errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, true, country);
            //Asset result
            System.assertEquals(true, string.isBlank(errors));
            list<Account> insertedAccount = [SELECT Id, Name FROM Account WHERE Name =: company];
            System.assertEquals(1, insertedAccount.size());
            list<Contact> insertedContact = [SELECT Id, Name FROM Contact WHERE LastName =: lastName];
            System.assertEquals(1, insertedContact.size());
        }
    } 
    
    testMethod static void testValidationMessages(){
        User guestUser = getUser(BROKER_IQ2_GUEST_USER);
        System.runAs(guestUser) {
            //Set params
            String firstName;
            String lastName;
    		String jobTitle; 
        	String company;
    		String email; 
    		String phone; 
    		String country;
            Boolean opt_in = false;
            String errors;
            Test.startTest();
                //Test firstName validation
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors));
                //Test lastName validation
                firstName = FIRSTNAME_EXAMPLE;
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
                //Test jobTitle validation
                lastName = LASTNAME_EXAMPLE;
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
                //Test company validation
                jobTitle = JOBTITLE_EXAMPLE;
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
                //Test email validation
                company = COMPANY_EXAMPLE;
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
                email = [SELECT Id, Email FROM User LIMIT 1].Email; //Existing user email
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
                //Test phone validation
                email = EMAIL_EXAMPLE;
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
                //Test opt_in validation
                phone = PHONE_EXAMPLE;
                errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
                System.assertEquals(true, string.isNotBlank(errors)); 
            Test.stopTest();
            //It wasn't created an account or contact
            list<Account> insertedAccount = [SELECT Id, Name FROM Account WHERE Name =: company];
            System.assertEquals(0, insertedAccount.size());
            list<Contact> insertedContact = [SELECT Id, Name FROM Contact WHERE LastName =: lastName];
            System.assertEquals(0, insertedContact.size());
        }
    } 
    
    testMethod static void testAddContactToExistingCompany(){
        User guestUser = getUser(BROKER_IQ2_GUEST_USER);
        User systemAdmin = getUser(USER_SYSTEMADMIN_LASTNAME);
        System.runAs(guestUser) {
           	//Set params
            String firstName = FIRSTNAME_EXAMPLE;
            String lastName = LASTNAME_EXAMPLE;
    		String jobTitle = JOBTITLE_EXAMPLE; 
            String company = EXISTING_ACCOUNT_NAME; //Get existing company name to test
            String email = EMAIL_EXAMPLE;
            String phone = PHONE_EXAMPLE;
            String country = COUNTRY_EXAMPLE;
            Boolean opt_in = true;
            //Call class
            Test.startTest();
                String errors = BrokerIqRegistrationFormController.registerBrokerIq2User(firstName, lastName, jobTitle, company, email, phone, opt_in ,country);
            Test.stopTest();
            //Asset result
            System.assertEquals(true, string.isBlank(errors)); 
            Integer numberOfaccountsWithExistingName = [SELECT count() FROM Account WHERE Name =: EXISTING_ACCOUNT_NAME];
            System.assertEquals(1, numberOfaccountsWithExistingName); 
            Integer numberOfinsertedContact = [SELECT count() FROM Contact WHERE LastName =: LASTNAME_EXAMPLE];
            System.assertEquals(1, numberOfinsertedContact);
        }
    }
    
}