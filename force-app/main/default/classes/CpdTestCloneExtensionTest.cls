@istest
public class CpdTestCloneExtensionTest {
    
    private static TestObjectGenerator tog = new TestObjectGenerator();
    /* Commenting out due to deployment issue
    @istest 
    public static void basic() {
        PageReference pageRef = Page.CpdTestClone;
        
        CPD_Test__c cpdTest = tog.getCpdTest();
        CPD_Test_Question__c cpdTestQuestion = tog.getCpdTestQuestion();
        
        CpdTestCloneExtension controllerExtension = new 
            CpdTestCloneExtension(new ApexPages.StandardController(cpdTest));
        
        PageReference nextPage = controllerExtension.doClone();
        
        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(0, messages.size(), messages);

        CPD_Test__c cloneTest = [SELECT Id, Status__c, Pass_Score__c, IQ_Content__c
                                 FROM CPD_Test__c
                                 WHERE Id != :cpdTest.Id];
        
        System.assertEquals('/' + cloneTest.Id, nextPage.getUrl());
        System.assertEquals(cpdTest.Pass_Score__c, cloneTest.Pass_Score__c);
        System.assertEquals(cpdTest.IQ_Content__c, cloneTest.IQ_Content__c);
        
        CPD_Test_Question__c cloneQuestion = [SELECT Id, Question__c, Answer__c FROM
                                              CPD_Test_Question__c
                                              WHERE CPD_Test__c = :cloneTest.Id];
        
        System.assertEquals(cpdTestQuestion.Question__c, cloneQuestion.Question__c);
        System.assertEquals(cpdTestQuestion.Answer__c, cloneQuestion.Answer__c);
    } */

    @istest 
    public static void noId() {
        PageReference pageRef = Page.CpdTestClone;
        
        CPD_Test__c cpdTest = tog.getCpdTest();
        CPD_Test_Question__c cpdTestQuestion = tog.getCpdTestQuestion();
        
        CpdTestCloneExtension controllerExtension = new 
            CpdTestCloneExtension(new ApexPages.StandardController(new CPD_Test__c()));
        
        PageReference nextPage = controllerExtension.doClone();
        
        List<ApexPages.Message> messages = ApexPages.getMessages();
        System.assertEquals(1, messages.size(), messages);
        System.assert(messages[0].getSummary().containsIgnoreCase('No CPD Test Id supplied'));

        System.assertEquals(null, nextPage);
    }
}