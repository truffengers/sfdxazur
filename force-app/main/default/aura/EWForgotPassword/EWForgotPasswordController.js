({
    handleForgotPassword: function (component, event, helpler) {
        helpler.handleForgotPassword(component, event, helpler);
    },
    onKeyUp: function (component, event, helpler) {
        if (event.getParam('keyCode') === 13) {
            helpler.handleForgotPassword(component, event, helpler);
        }
    }
});