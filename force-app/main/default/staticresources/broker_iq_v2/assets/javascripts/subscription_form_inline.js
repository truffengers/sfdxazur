TT.subscriptionFormInline = (function($) {

  'use strict';

  var $form;
  var $input;
  var $successMessage;
  var $errorMessage;



  function referenceElements(selector) {
    $form           = selector instanceof jQuery ? selector : $(selector);
    $input          = $form.find('.c-input-send');
    $successMessage = $form.find('.c-success');
    $errorMessage   = $form.find('.c-error');
  }

  function success() {
    $input.hide();
    $successMessage.css({opacity: 0}).show().transition({opacity: 1});
    $errorMessage.css({opacity: 0}).slideUp(function() {
      $errorMessage.text('').hide();
    });
  }

  function failure(message) {
    $errorMessage.css({opacity: 0}).text(message).slideDown().transition({opacity: 1});
  }

  function clear() {
    $input.show();
    $successMessage.hide();
    $errorMessage.css({opacity: 0}).slideUp(function() {
      $errorMessage.text('').hide();
    });
  }

  function init(selector) {
    referenceElements(selector);
  }

  function teardown() {
    $form           = undefined;
    $input          = undefined;
    $successMessage = undefined;
    $errorMessage   = undefined;
  }



  return {
    init     : init,
    success  : success,
    failure  : failure,
    clear    : clear,
    teardown : teardown
  };

}(jQuery));
