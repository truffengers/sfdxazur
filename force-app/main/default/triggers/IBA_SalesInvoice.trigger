/**
* @name: IBA_SalesInvoice
* @description: Trigger for the managed object c2g__codaInvoice__c
*
*/

trigger IBA_SalesInvoice on c2g__codaInvoice__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_SalesInvoiceTriggerHandler());
}