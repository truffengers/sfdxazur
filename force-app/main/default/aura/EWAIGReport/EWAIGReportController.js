({
    doInit: function (component, event, helper) {
        helper.doInit(component);
    },
    downloadCSV: function (component, event, helper) {
        let gridData = component.get('v.gridData');
        let gridDataHeaders = gridData['headers'];
        let gridDataRows = gridData['rows'];

        let csv = '';
        for (let i = 0; i < gridDataHeaders.length; i++) {
            let gridHeaders = '\"' + gridDataHeaders[i]['val'] + '\"';
            csv += (i === (gridDataHeaders.length - 1)) ? gridHeaders : gridHeaders + ',';
        }
        csv += '\n';
        let data = [];
        for (let i = 0; i < gridDataRows.length; i++) {
            let gridRowIns = gridDataRows[i];
            let gridRowInsVals = gridRowIns;
            let tempRow = [];
            for (let j = 0; j < gridRowInsVals.length; j++) {
                let tempValue = gridRowInsVals[j]['val'];
                if (tempValue.includes(',')) {
                    tempValue = "\"" + tempValue + '\"';
                }
                tempValue = tempValue.replace(/(\r\n|\n|\r)/gm, '');
                tempRow.push(tempValue);
            }
            data.push(tempRow);
        }
        data.forEach(function (row) {
            csv += row.join(',');
            csv += '\n';
        });

        let hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = 'AIR_Report' + '.csv';
        hiddenElement.click();
    }

});