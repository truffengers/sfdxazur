@IsTest
private class EWVlocityCustomFunctionHelperTest {

    static testMethod void testBehavior_extractCoverageValue_success() {
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        Integer excess = 1234;

        String input = '[{"Id":"01t58000006YHT8AAO","attributeCategories":{"totalSize":1,"records":[{"displaySequence":3,"Code__c":"AttrQuote","Name":"Quote","id":"a2D25000000DLffEAG","productAttributes":{"totalSize":39,"records":[{"code":"excess","dataType":"number","inputType":"range","required":false,"readonly":false,"disabled":false,"filterable":true,"min":250,"max":1000,"attributeId":"a2E25000000DurjEAC","label":"Excess","displaySequence":61,"hasRules":false,"hidden":false,"values":[{"readonly":false,"disabled":false}],"userValues":' + excess + ',"$$hashKey":"insobject:0038"}]},"$$hashKey":"insobject:00"}]},"$$hashKey":"insobject:0"},"excess"]';

        inputMap.put('arguments', JSON.deserializeUntyped(input));

        EWVlocityCustomFunctionHelper customFunctionHelper = new EWVlocityCustomFunctionHelper();

        customFunctionHelper.invokeMethod('extractCoverageValue', inputMap, outputMap, optionMap);

        System.debug('bdec // outputMap: ' + outputMap);
        System.assert(outputMap.containsKey('result'));
        System.assertEquals(excess, (Integer) outputMap.get('result'));
    }

    static testMethod void testBehavior_extractCoverageValue_fail() {
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        String broken_input = '[{"Id":"01t58000006YHT8AAO","attributeCategories":{"totalSize":1,"records":[{"displaySequence":3,"Code__c":"AttrQuote","Name":"Quote","id":"a2D25000000DLffEAG","productAttributes":{"totalSize":39,"records_":[]},"$$hashKey":"insobject:00"}]},"$$hashKey":"insobject:0"},"excess"]';

        inputMap.put('arguments', JSON.deserializeUntyped(broken_input));

        EWVlocityCustomFunctionHelper customFunctionHelper = new EWVlocityCustomFunctionHelper();

        Boolean result = customFunctionHelper.invokeMethod('extractCoverageValue', inputMap, outputMap, optionMap);

        System.assert(outputMap.isEmpty());
        System.assert(!result);
    }

    static testMethod void testBehavior_capitaliseFirstLetter() {
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        String inputLowerCases = 'input';
        String inputCapitalised = 'Input';

        String funcInput = '["' + inputLowerCases + '"]';

        inputMap.put('arguments', JSON.deserializeUntyped(funcInput));

        EWVlocityCustomFunctionHelper customFunctionHelper = new EWVlocityCustomFunctionHelper();

        customFunctionHelper.invokeMethod('capitaliseFirstLetter', inputMap, outputMap, optionMap);

        System.assert(!outputMap.isEmpty());
        System.assertEquals(inputCapitalised, (String) outputMap.get('result'));
    }
}