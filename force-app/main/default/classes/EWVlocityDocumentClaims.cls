/**
* @name: EWVlocityDocumentClaims
* @description: Helper class for the Claims Section
* @author: Steve Loftus sloftus@azuruw.com
* @date: 07/12/2018
*/
global with sharing class EWVlocityDocumentClaims implements vlocity_ins.VlocityOpenInterface {

    private Integer maxLogSize = 32767;
    private static String CLASS_NAME = EWVlocityDocumentClaims.class.getName();
    private static String METHOD_INVOKE_METHOD = 'invokeMethod';
    private static String VLOCITY_DESCRIPTION = 'Vlocity Document Remote Action call';

    private static String HEADING = 'Previous Claims';
    private static String BORDER_STYLE_BASE = 'border: .5px solid black;border-collapse: collapse;';
    private static String FONT_STYLE_BASE = 'font-size: 10pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String FONT_STYLE_HEADING = 'font-size: 12pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String ROW_STYLE_BASE = 'height: 20px;';
    private static String QUOTE_PREFIX = '0Q0';

    public EWVlocityDocumentClaims() {
        
    }

    public Boolean invokeMethod(
                            String methodName,
                            Map<String, Object> inputMap,
                            Map<String, Object> outputMap,
                            Map<String, Object> optionMap) {

        Boolean result = true;

        Azur_Log__c traceLog = new Azur_Log__c();

        traceLog.Class__c = CLASS_NAME;
        traceLog.Method__c = METHOD_INVOKE_METHOD;
        traceLog.Log_description__c = VLOCITY_DESCRIPTION;

        string logMessage = 'methodName:' + '\n' +
                methodName + '\n\n' +
                'inputMap:' + '\n' +
                JSON.serializePretty(inputMap) + '\n\n' +
                'optionMap:' + '\n' +
                JSON.serializePretty(optionMap);

        if (logMessage.length() > maxLogSize ) {
            traceLog.Log_message__c = logMessage.substring(0, maxLogSize);
        } else {
            traceLog.Log_message__c = logMessage;
        }

        try {

            if (methodName == 'buildDocumentSectionContent') {

                buildDocumentSectionContent(inputMap, outputMap, optionMap);

            } else {

                result = false;
            }

        } catch(Exception e) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(e);
            log.Class__c = CLASS_NAME;
            log.Method__c = METHOD_INVOKE_METHOD;
            insert log;

            result = false;

        } finally {

            string additionalLogMessage = traceLog.Log_message__c + '\n\n' + 'outputMap:' + '\n' + JSON.serializePretty(outputMap);

            if (additionalLogMessage.length() > maxLogSize ) {
                traceLog.Log_message__c = additionalLogMessage.substring(0, maxLogSize);
            } else {
                traceLog.Log_message__c = additionalLogMessage;
            }

            insert traceLog;

        }

        return result;
    }

    private void buildDocumentSectionContent (
                                        Map<String, Object> inputMap,
                                        Map<String, Object> outputMap,
                                        Map<String, Object> optionMap) {

        String dataJSON = (String)inputMap.get('dataJSON');
        Map<String, Object> documentData = (Map<String, Object>)JSON.deserializeUntyped(dataJSON);
        Map<String, Object> contextData = (Map<String, Object>)documentData.get('contextData');

        Id contextId = (Id)contextData.get('contextId');

        String query = 'SELECT vlocity_ins__LossDate__c, vlocity_ins__LossCause__c, vlocity_ins__TotalIncurred__c FROM vlocity_ins__InsuranceClaim__c';
        query += ' WHERE ' + ((((String)contextId).startsWith(QUOTE_PREFIX)) ? 'EW_Quote__c' : 'vlocity_ins__PrimaryPolicyAssetId__c');
        query += ' =: contextId ORDER BY vlocity_ins__LossDate__c DESC';

        List<vlocity_ins__InsuranceClaim__c> claimList = Database.query(query);

        String content = '';

        if (!claimList.isEmpty()) {

            //Prepare header
            Map<String, String> headerMap = new Map<String, String> ();

            headerMap.put('1', 'Date');
            headerMap.put('2', 'Type');
            headerMap.put('3', 'Amount');

            content = '<p><span style=\"' + FONT_STYLE_HEADING + '\">' + HEADING + '</span></p>';
            content += '<table style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 100%\">';
            content += '<thead><tr>';

            for (String key: headerMap.keySet()){

                content += '<th style=\"' + FONT_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                content += '<viawrapper>' + headerMap.get(key) + '</viawrapper></th>';
            }

            content += '</tr></thead>';
            content += '<tbody>';

            for (vlocity_ins__InsuranceClaim__c claim : claimList) {

                content += '<tr style=\"' + ROW_STYLE_BASE + '\">';
                content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                content += '<span style=\"' + FONT_STYLE_BASE + '\">';
                content += '<viawrapper>' + DateTime.newInstance(claim.vlocity_ins__LossDate__c.year(), claim.vlocity_ins__LossDate__c.month(), claim.vlocity_ins__LossDate__c.day()).format('dd-MM-yyyy') + '</viawrapper>';
                content += '</span></td>';
                content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                content += '<span style=\"' + FONT_STYLE_BASE + '\">';
                content += '<viawrapper>' + claim.vlocity_ins__LossCause__c + '</viawrapper>';
                content += '</span></td>';
                content += '<td style=\"' + ROW_STYLE_BASE + BORDER_STYLE_BASE + 'width: 33%;\">';
                content += '<span style=\"' + FONT_STYLE_BASE + '\">';
                content += '<viawrapper>£' + claim.vlocity_ins__TotalIncurred__c + '</viawrapper>';
                content += '</span></td>';
                content += '</tr>';
            }

            content += '</tbody></table>';
        }

        outputMap.put('sectionContent', content);
    }
}