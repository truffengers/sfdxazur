public with sharing class BrokerIqQueryController {
	
	@AuraEnabled
	public static sObject getSingleRecord(String query) {
		Id networkId = Network.getNetworkId();
		return Database.query(query);
	}
	
	@AuraEnabled
	public static sObject getSingleRecordAllFields(String typeName, String recordId) {
		Id networkId = Network.getNetworkId();
		String q = Nebula_Tools.FieldSetUtils.queryAllFields(typeName, 'Id = :recordId');
		List<sObject> result = Database.query(q);
		return !result.isEmpty() ? result.get(0) : null;
	}

	@AuraEnabled
	public static List<sObject> getRecordsFieldSet(String typeName, String fieldSetName, String whereClause) {
		String q = Nebula_Tools.FieldSetUtils.queryFieldSet(typeName, fieldSetName, whereClause);
		return Database.query(q);
	}

	@AuraEnabled
	public static List<sObject> getRecordsAllFields(String typeName, String whereClause) {
		String q = Nebula_Tools.FieldSetUtils.queryAllFields(typeName, whereClause);
		return Database.query(q);
	}
}