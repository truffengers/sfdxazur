import { LightningElement, track } from 'lwc';
import { prepareForMarkup, processScheduleData } from 'c/ix_utils';
import { CONSTANTS } from 'c/ix_constants';
import { QUICK_LINKS } from 'c/ix_generalData';

export default class Ix_TermsAgreement extends LightningElement {
    @track workingTermsData;
    @track itemData = {
        [CONSTANTS.QUICK_LINK_NAMES.TERMS_OF_USE]
            : QUICK_LINKS[CONSTANTS.QUICK_LINK_NAMES.TERMS_OF_USE]
    }

    connectedCallback() {
        this.workingTermsData = prepareForMarkup(
            processScheduleData(this.itemData)
        )[0];
        this.workingTermsData._name = '';
    }

    handleBackLink() {
        window.history.back();
    }

    handleTermsAgree() {
        const termsAgreed = new CustomEvent('termsagreed');
        this.dispatchEvent(termsAgreed);
    }
}