public class BrokerIqFiltersController {

	public class FilterControllerResult {
		@AuraEnabled
		public List<BrokerIqSearchFilterWrapper> topics;
		@AuraEnabled
		public List<BrokerIqSearchFilterWrapper> contentTypes;
		@AuraEnabled
		public List<BrokerIqSearchFilterWrapper> contributors;
	}


    @AuraEnabled
    public static FilterControllerResult getFilterValues() {
        try {
            FilterControllerResult rval = new FilterControllerResult();

            rval.topics = getTopics();
            rval.contentTypes = getContentTypes();
            rval.contributors = getContributors();

            return rval;
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
        }
    }

    private static List<BrokerIqSearchFilterWrapper> wrapperAggregateResults(List<AggregateResult> result) {
        List<BrokerIqSearchFilterWrapper> wrappers = new List<BrokerIqSearchFilterWrapper>();

        for(AggregateResult res : result) {
            wrappers.add(
                new BrokerIqSearchFilterWrapper(
                    (String) res.get('label'), 
                    (String) res.get('value'),
                    (Integer) res.get('recCount')
                )
            );
        }

        return wrappers;
    }

    private static List<BrokerIqSearchFilterWrapper> getContentTypes() {
        
        return wrapperAggregateResults([SELECT count(Id) recCount, MIN(RecordType.Name) label, RecordTypeId value
            					   FROM IQ_Content__c 
                                   WHERE Active__c = true 
                                   GROUP BY RecordTypeId
                                   ORDER BY MIN(RecordType.Name) ASC]);
    }

    private static List<BrokerIqSearchFilterWrapper> getContributors() {
        return wrapperAggregateResults([
            SELECT Count(Id) recCount, Broker_iQ_Contributor__c value, MIN(Broker_iQ_Contributor__r.Full_Name__c) label 
            FROM IQ_Content__c 
            WHERE Active__c = true AND Broker_iQ_Contributor__c != null
            GROUP BY Broker_iQ_Contributor__c
            ORDER BY MIN(Broker_iQ_Contributor__r.Name)
        ]);
    }
    
    private static List<BrokerIqSearchFilterWrapper> getTopics() {
         List<ConnectAPI.ManagedTopic> navTopics = ConnectAPI.ManagedTopics.getManagedTopics(Network.getNetworkId(), ConnectApi.ManagedTopicType.Navigational).managedTopics;
         Set<Id> navTopicIds = new Set<Id>();

         for(ConnectAPI.ManagedTopic thisNavTopic : navTopics) {
            navTopicIds.add(thisNavTopic.topic.Id);
         }

        return wrapperAggregateResults([
			SELECT COUNT(Id) recCount, TopicId value, MIN(Topic.Name) label 
			FROM TopicAssignment 
			WHERE EntityType = 'IQ_Content' AND NetworkId = :Network.getNetworkId()
            AND TopicId IN :navTopicIds
			GROUP BY TopicId
			ORDER BY MIN(Topic.Name)
        ]);
    }
}