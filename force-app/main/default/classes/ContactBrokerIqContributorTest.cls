@isTest
public with sharing class ContactBrokerIqContributorTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();

	@isTest
	public static void basic() {
		Contact c = tog.getContactNoInsert();
		c.Is_Broker_iQ_Contributor__c = true;

		insert c;

		c = [SELECT Id, Broker_iQ_Contributor__c FROM Contact WHERE Id = :c.Id];

		System.assertNotEquals(null, c.Broker_iQ_Contributor__c);
	}

}