global class EWVlocityCustomFunctionHelper implements vlocity_ins.VlocityOpenInterface {

    global Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> optionMap) {
        Boolean result = true;
        integer maxSize = 32767;

        Azur_Log__c traceLog = new Azur_Log__c();

        try {
            traceLog.Class__c = EWVlocityCustomFunctionHelper.class.getName();
            traceLog.Method__c = 'invokeMethod(\'' + methodName + '\')';
            traceLog.Log_description__c = 'Vlocity Remote Action call';

            String logMessage = 'methodName:' + '\n' +
                    methodName + '\n\n' +
                    'inputMap:' + '\n' +
                    JSON.serializePretty(inputMap) + '\n\n' +
                    'optionMap:' + '\n' +
                    JSON.serializePretty(optionMap);

            if (logMessage.length() > maxSize) {
                traceLog.Log_message__c = logMessage.substring(0, maxSize);
            } else {
                traceLog.Log_message__c = logMessage;
            }

            switch on methodName {
                when 'extractCoverageValue' {
                    extractCoverageValue(inputMap, outputMap, optionMap);
                }
                when 'capitaliseFirstLetter' {
                    capitaliseFirstLetter(inputMap, outputMap, optionMap);
                }
                when else {
                    result = false;
                }
            }

        } catch (Exception ex) {
            LogBuilder logBuilder = new LogBuilder();

            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = EWVlocityCustomFunctionHelper.class.getName();
            log.Log_description__c = EWVlocityCustomFunctionHelper.class.getName();
            log.Method__c = 'invokeMethod(\'' + methodName + '\')';
            log.Log_message__c = log.Log_message__c + '\n\n method name = ' + methodName;
            log.Log_message__c += '\n\nln: ' + ex.getLineNumber();
            log.Log_message__c += '\n\nst: ' + ex.getStackTraceString();
            System.debug('FF DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());

            insert log;

            result = false;

        } finally {
            String additionalLogMessage = traceLog.Log_message__c + '\n\n' + 'outputMap:' + '\n' + JSON.serializePretty(outputMap);

            if (additionalLogMessage.length() > maxSize) {
                traceLog.Log_message__c = additionalLogMessage.substring(0, maxSize);
            } else {
                traceLog.Log_message__c = additionalLogMessage;
            }
            insert traceLog;
        }

        return result;
    }

    private static void capitaliseFirstLetter
            (Map<String, Object> inputMap,
                    Map<String, Object> outputMap,
                    Map<String, Object> optionMap) {

        List<Object> arguments = (List<Object>) inputMap.get('arguments');
        if (arguments.isEmpty()) {
            return;
        }
        String input = (String) arguments.get(0);
        String result = '';

        if (String.isNotBlank(input)) {
            result = input.substring(0, 1).toUpperCase() + input.substring(1);
        }

        outputMap.put('result', result);
    }

    private static void extractCoverageValue(
            Map<String, Object> inputMap,
            Map<String, Object> outputMap,
            Map<String, Object> optionMap) {

        List<Object> arguments = (List<Object>) inputMap.get('arguments');
        Map<String, Object> coverageMap = (Map<String, Object>) arguments.get(0);
        String recordCode = (String) arguments.get(1);

        Map<String, Object> attributeCategories = (Map<String, Object>) coverageMap.get('attributeCategories');
        Map<String, Object> attributeCategoriesRecords = (Map<String, Object>) ((List<Object>) attributeCategories.get('records')).get(0);

        Map<String, Object> productAttributes = (Map<String, Object>) attributeCategoriesRecords.get('productAttributes');
        List<Object> productAttributesRecords = (List<Object>) productAttributes.get('records');

        for (Object productAttribute : productAttributesRecords) {
            Map<String, Object> productAttributeMap = (Map<String, Object>) productAttribute;
            if (productAttributeMap.get('code') == recordCode) {
                outputMap.put('result', productAttributeMap.get('userValues'));
            }
        }
    }
}