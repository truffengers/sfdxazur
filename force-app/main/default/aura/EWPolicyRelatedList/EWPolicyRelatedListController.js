({
    doInit: function(component, event, helper) {
        
        helper.getBaseUrl(component);

        component.set('v.columns',[
            {label: 'Name', fieldName:'linkName', type:'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_top'}},
            {label: 'Status', fieldName:'Status', type:'text'},
            {label: 'Effective Date', fieldName:'vlocity_ins__EffectiveDate__c', type:'date'},
            {label: 'Expiration Date', fieldName:'vlocity_ins__ExpirationDate__c', type:'date'}
            
        ]);
        
        helper.fetchData(component);
    }

});