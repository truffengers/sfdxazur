/**
* @name: IBA_PayableCreditNoteTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaPurchaseCreditNote__c trigger functionality
*
*/

public with sharing class IBA_PayableCreditNoteTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_PayableCreditNoteTriggerHandler.IsDisabled != null ? IBA_PayableCreditNoteTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, c2g__codaPurchaseCreditNote__c> policyTransactionMap = new Map<Id, c2g__codaPurchaseCreditNote__c>();
        for(Id recordId :newItems.keySet()) {
            c2g__codaPurchaseCreditNote__c payableCreditNote = (c2g__codaPurchaseCreditNote__c)newItems.get(recordId);
            c2g__codaPurchaseCreditNote__c payableCreditNoteOld = (c2g__codaPurchaseCreditNote__c)oldItems.get(recordId);
            if(payableCreditNote.c2g__CreditNoteStatus__c == IBA_UtilConst.PAYABLE_CREDIT_NOTE_STATUS_COMPLETE 
                && payableCreditNoteOld.c2g__CreditNoteStatus__c != IBA_UtilConst.PAYABLE_CREDIT_NOTE_STATUS_COMPLETE) {
                    policyTransactionMap.put(payableCreditNote.IBA_PolicyTransaction__c, payableCreditNote);
            }
        }
        if(policyTransactionMap.size() > 0) {
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(policyTransactionMap.keySet());
            transferData.subtractPayableCreditNoteTotal(policyTransactionMap)
                .transfer();
        }
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}