({
	componentInitJs : function(component, event, helper) {
		TT.sharing(jQuery, {
          container : '#js-sticky-social',
          selector  : '.c-btn-sharing',
          interpolations : {
          	url : component.get('v.shareUrl'),
          	title : component.get('v.title')
          }
        });
        
        TT.stickySocial.init({ id: 'js-sticky-social' });
        component.set('v.jsInitialised', true);
	},
	doneRendering : function(component, event, helper) {
		if(typeof TT != 'undefined'
		&& typeof TT.stickySocial != 'undefined'
		&& component.find('sticky-social').getElement() != null
		&& component.get('v.jsInitialised')) {
		    try {
			    TT.stickySocial.resizeHandler();
            } catch(err) {
                console.error(err);
            }
		}
	},
    doInit : function(component, event, helper) {
        var getSharingLinkForAction = component.get('c.getSharingLinkFor');
        
        getSharingLinkForAction.setParam('iqContentId', component.get('v.recordId'));

        getSharingLinkForAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var resultMap=response.getReturnValue();
                for(var key in resultMap){
                    if(key==='url') 
                        component.set('v.shareUrl', resultMap[key]);
                    if(key==='Name') 
                        component.set('v.contentName', resultMap[key]);
                    if(key==='RecordTypeName') 
                        component.set('v.conRecordTypeName', resultMap[key]);
                }                
            }
        });
        
        $A.enqueueAction(getSharingLinkForAction);        
    },
    downloadDocument : function(component, event, helper){
        helper.downloadPdfFile(component,event,helper); 
    }
})