public with sharing class CpdTestTakeController {
    
    public List<CPD_Assessment_Answer__c> answers {get; set;}
    public List<List<SelectOption>> questionOptions {get; set;}
    public CPD_Test__c theTest {get; set;}
    public CPD_Assessment__c thisAssessment {get; set;}
    public Boolean isValid {get; set;}
    private Contact currentUserAsContact;
    public Integer questionNumber {get; set;}
    public String currentAnswer {get; set;}
        
    public CpdTestTakeController() {
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        isValid = false;
        questionNumber = 0;
        questionOptions = new List<List<SelectOption>>();
        
        String iqContentId = parameters.get('iqContentId');
        try {
            theTest = [SELECT Id, Name, Pass_Score__c, Number_of_Questions__c, IQ_Content__r.CPD_Hours_In_Words__c,
                       IQ_Content__r.Category__r.Colour__c, IQ_Content__r.Category__r.Icon__c, IQ_Content__r.Category__r.Name
                       FROM CPD_Test__c
                       WHERE IQ_Content__c = :iqContentId
                       AND Status__c = 'Published'
                       ORDER BY LastModifiedDate DESC
                       LIMIT 1];
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No test was found matching IQ Content with id=' + iqContentId));
            return;
        }
        
        try {
            currentUserAsContact = PortalUserService.getCurrentUserAsContact();
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This page is only suitable for a portal user or an internal user with a correct value in Contact_Id__c'));
            return;
        }
        
        isValid = true;
    }
    
    public void getAssessment() {
        if(!isValid) {
            return;
        }
        
        List<CPD_Assessment__c> existingAssessments = 
            [SELECT Id, Status__c
             FROM CPD_Assessment__c
             WHERE CPD_Test__c = :theTest.Id
             AND Contact__c = :currentUserAsContact.Id
             AND Status__c = 'In Progress'];
        if(existingAssessments.isEmpty()) {
            thisAssessment = new CPD_Assessment__c(Contact__c = currentUserAsContact.Id,
                                                   CPD_Test__c = theTest.Id,
                                                   Status__c = 'In Progress');
            insert thisAssessment; 
        } else {
            thisAssessment = existingAssessments[0];
        }
        answers = [SELECT Id, Answer__c, CPD_Test_Question__r.Question__c, 
                   CPD_Test_Question__r.Option_1__c, CPD_Test_Question__r.Option_2__c, CPD_Test_Question__r.Option_3__c, CPD_Test_Question__r.Option_4__c,
                   CPD_Test_Question__r.Number_of_Options_to_Choose__c
                   FROM CPD_Assessment_Answer__c
                   WHERE CPD_Assessment__c = :thisAssessment.Id
                   ORDER BY CPD_Test_Question__r.Question_Order__c ASC];
        for(CPD_Assessment_Answer__c ans : answers) {
            List<SelectOption> theseOptions = new List<SelectOption>();
            for(Integer i=1; i <= 4; i++) {
                String thisOption = (String)ans.CPD_Test_Question__r.get('Option_' + i + '__c');
                if(thisOption != null) {
                    theseOptions.add(new SelectOption('' + i, thisOption));
                }
            }
            questionOptions.add(theseOptions);
        }
        
    }
    
    public void next() {
        try {
            if(currentAnswer != null) {
                currentAnswer = currentAnswer.replaceAll(',', ';');
            }
            answers[questionNumber].Answer__c = currentAnswer;
            update answers[questionNumber];
            questionNumber++;
            if(questionNumber >= answers.size()) {
                thisAssessment.Status__c = 'Submitted';
                update thisAssessment;
            }
        } catch(Exception e) {
            ApexPages.addMessages(e);
        }
    }
}