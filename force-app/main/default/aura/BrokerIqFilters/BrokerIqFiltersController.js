({
    componentInitJs : function(component) {
	    TT.filters(jQuery, '#js-filters');
    },
	doInit : function(component, event, helper) {
		var getFilterValuesAction = component.get('c.getFilterValues');
		
		getFilterValuesAction.setStorable();
		
		getFilterValuesAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	var filters = response.getReturnValue();
                component.set('v.topics', filters.topics);
                component.set('v.contentTypes', filters.contentTypes);
                component.set('v.contributors', filters.contributors);
            }
        });
        
        $A.enqueueAction(getFilterValuesAction);		

	}
})