import { LightningElement, api, track } from 'lwc';
import { prepareForMarkup, processScheduleData } from 'c/ix_utils';

export default class Ix_LinkOverlay extends LightningElement {
    @api itemTitle;
    @api itemData;

    @track workingItemData;
    @track renamedItemData;

    renderedCallback() {
        // HACK: Because the LWC doesn't seem to rerender on deep changes in
        // objects, judge whether the _name of the workingItemData is the same
        // as the name in the itemData. If so, reassign this.workingItemData,
        // forcing a rerender.
        const rerender = this.workingItemData
            && this.itemTitle !== this.workingItemData._name
            && this.workingItemData._name !== Object.keys(this.itemData)[0];

        if (!this.workingItemData || rerender) {
            this.workingItemData = prepareForMarkup(
                processScheduleData(this.itemData)
            )[0];

            // HACK: A crude way of hiding the name of a Quick Link so it won't
            // display.
            this.renamedItemData = { ...this.workingItemData, _name: '' };
        }
    }

    onCloseOverlay() {
        const closeQuickLinkEvent = new CustomEvent('closequicklink');
        this.dispatchEvent(closeQuickLinkEvent);
    }
}