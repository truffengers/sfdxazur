/**
* @name: PHO_CreateOperationalDataTestButtonCtrl
* @description: Controller class for PHO_CreateOperationalDataTestButton page.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 16/12/2019
*/
public with sharing class PHO_CreateOperationalDataTestButtonCtrl {

    private ApexPages.StandardSetController standardSetController;
    private PageReference originalPage;

    public PHO_CreateOperationalDataTestButtonCtrl(ApexPages.StandardSetController standardSetController) {
        this.standardSetController = standardSetController;
        this.originalPage = standardSetController.cancel();
    }

    /**
    * @methodName: createOperationalDataForSelectedRecords
    * @description: Enqueues a job which translates Phoenix Staging records into Salesforce entities.
    *               Runs only for records selected on list view.
    * @dateCreated: 16/12/2019
    */
    public PageReference createOperationalDataForSelectedRecords() {
        Set<Id> selectedIds = new Set<Id>();
        for (PHO_PhoenixStaging__c stagingRecord : (List<PHO_PhoenixStaging__c>) standardSetController.getSelected()) {
            selectedIds.add(stagingRecord.Id);
        }
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        Integer recordLimit = (Integer) settings.PHO_PhoenixStagingRecordsLimit__c;
        if (recordLimit > 0) {
            System.enqueueJob(new PHO_CreateOperationalDataJob(recordLimit, selectedIds));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to set PHO_PhoenixStagingRecordsLimit__c in IBA_IBACustomSettings__c custom setting to execute this action'));
            return null;
        }
        return this.originalPage;
    }

    /**
    * @methodName: goBack
    * @description: Redirects to original context page
    * @dateCreated: 16/12/2019
    */
    public PageReference goBack() {
        return this.originalPage;
    }
}