({
	doInit : function(component, event, helper) {
		var getUserAction = component.get('c.getUser');
		
		getUserAction.setStorable();
		
		getUserAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.user', response.getReturnValue());
	        }
        });
        
        $A.enqueueAction(getUserAction);		
  	},
  	save : function(component, event, helper) {
		var doSaveAction = component.get('c.doSave');
		
		doSaveAction.setParams({
			userToUpdate: component.get('v.user'),
			currentPassword: component.get('v.currentPassword'),
			newPassword: null,
			verifyNewPassword: null
		});

        var formAlerts = component.find('formAlerts');
        formAlerts.clear();

		doSaveAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.user', response.getReturnValue());
                component.set('v.editMode', false);
                formAlerts.showAlert('Profile Updated', 'Success');

	        } else {
	        	formAlerts.showServerErrors(response.getError());
	        }
        });
        
        $A.enqueueAction(doSaveAction);		

  	}
})