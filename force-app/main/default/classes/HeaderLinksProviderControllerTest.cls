@IsTest
private class HeaderLinksProviderControllerTest {
    static testMethod void testBehavior() {
        List<Azur_Community_Header_Link__mdt> mtdtList = new List<Azur_Community_Header_Link__mdt>([
                SELECT Id
                FROM Azur_Community_Header_Link__mdt
        ]);

        Map<String, String> headerLinks = HeaderLinksProviderController.getHeaderLinks();

        System.assertEquals(mtdtList.size(), headerLinks.size());
    }
}