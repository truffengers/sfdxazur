@isTest
private class BrightTalkWebcastPreviewScheduledTest {
	
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings;
    
    static {
        settings = tog.getBrightTalkApiSettings();
    }

	@isTest static void scheduleSelf() {
        BrightTalkApi api = new BrightTalkApi();

        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

        Nebula_API.NebulaApi.setMock(mock);

    	BrightTALK__Webcast__c btWebcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));

		Test.startTest();
		BrightTalkWebcastPreviewScheduled.scheduleSelf('test');
		BrightTalkWebcastPreviewScheduled.executeFuture();
		Test.stopTest();

		List<BrightTALK__Webcast__c>  webcasts = [SELECT Id, BrightTALK__Webcast_Id__c, 
				Preview_Image__c,
                Preview_Thumbnail__c, Duration_s__c, Status__c
                FROM BrightTALK__Webcast__c];

        System.assertEquals(1, webcasts.size());
        System.assertEquals(Decimal.valueOf(mock.responseParams.get('webcast_id')), webcasts[0].BrightTALK__Webcast_Id__c);
        System.assertEquals(Integer.valueOf(mock.responseParams.get('webcast_duration')), webcasts[0].Duration_s__c);
        System.assertEquals(mock.responseParams.get('webcast_thumbnail'), webcasts[0].Preview_Thumbnail__c);
        System.assertEquals(mock.responseParams.get('webcast_preview'), webcasts[0].Preview_Image__c);
        System.assertEquals(mock.responseParams.get('webcast_status'), webcasts[0].Status__c);
	}
		
}