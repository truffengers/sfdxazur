public class IBA_CashMatchingHistoryWrapper {
    public Date paidDate { get; set; }
    public c2g__codaCashMatchingHistory__c cashMatchingHistory { get; set; }
    public Id policyTransactionId { get; set; }
    public String transactionType { get; set; }

    public IBA_CashMatchingHistoryWrapper(
        Date paidDate,
        c2g__codaCashMatchingHistory__c cashMatchingHistory, 
        Id policyTransactionId,
        String transactionType) {
            this.paidDate = paidDate;
            this.cashMatchingHistory = cashMatchingHistory;
            this.policyTransactionId = policyTransactionId;
            this.transactionType = transactionType;
    }
}