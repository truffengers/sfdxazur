TT.subscriptionFormModal = (function($) {

  'use strict';

  var $modal;
  var $input;
  var $successMessage;
  var $errorMessage;
  var $closeButton;
  var nagTimeoutId;
  var settings;

  var OPEN_CLASS = 'is-open';
  var COOKIE_KEY = 'subscription-form-revealed';
  var OPENED     = 'opened';
  var CLOSED     = 'closed';

  var state = CLOSED;

  var DEFAULTS = {
    isNagging       : true,
    nagDelaySeconds : 10,
    nagAgainDays    : 14
  };



  function referenceElements() {
    $modal          = $('#js-subscription-form-modal');
    $input          = $modal.find('.c-input-send');
    $successMessage = $modal.find('.c-success');
    $errorMessage   = $modal.find('.c-error');
    $closeButton    = $modal.find('button');
  }

  function addListeners() {
    $closeButton.click(hide);
  }

  function show() {
    state = OPENED;
    clear();
    stopNagTimer();
    setNaggedCookie();
    $modal.addClass(OPEN_CLASS);
  }

  function hide() {
    state = CLOSED;
    $modal.removeClass(OPEN_CLASS);
  }

  function success() {
    $input.hide();
    $successMessage.css({opacity: 0}).show().transition({opacity: 1});
    $errorMessage.css({opacity: 0}).slideUp(function() {
      $errorMessage.text('').hide();
    });
  }

  function failure(message) {
    $errorMessage.css({opacity: 0}).text(message).slideDown().transition({opacity: 1});
  }

  function clear() {
    $input.show();
    $successMessage.hide();
    $errorMessage.css({opacity: 0}).slideUp(function() {
      $errorMessage.text('').hide();
    });
  }

  function startNagTimer() {
    if(!getPreviouslyRevealed()) {
      nagTimeoutId = window.setTimeout(nag, settings.nagDelaySeconds * 1000);
    }
  }

  function stopNagTimer() {
    if(nagTimeoutId !== undefined) {
      window.clearTimeout(nagTimeoutId);
      nagTimeoutId = undefined;
    }
  }

  function nag() {
    show();
  }

  function setNaggedCookie() {
    Cookies.set(COOKIE_KEY, true, { expires: settings.nagAgainDays });
  }

  function getPreviouslyRevealed() {
    return Cookies.get(COOKIE_KEY) !== undefined;
  }

  function init(options) {
    settings = $.extend({}, DEFAULTS, options || {});
    referenceElements();
    addListeners();
    startNagTimer();
  }



  return {
    init    : init,
    show    : show,
    hide    : hide,
    success : success,
    failure : failure,
    clear   : clear

  };

}(jQuery));
