public with sharing class IXHelper {
    @AuraEnabled
    public static Boolean checkTermsAgreed() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, ix_Terms_Agreed__c FROM User WHERE Id=:userID];
        
        if (user != null) {
        	return user.ix_Terms_Agreed__c;
        } else {
            return null;
        }
    }
    
    @AuraEnabled
    public static User agreeToTerms() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, ix_Terms_Agreed__c FROM User WHERE Id=:userID];
        
        if (user != null) {
            user.ix_Terms_Agreed__c = true;
        	update user;
            
            return user;
        } else {
            return null;
        }
    }
    
    // This method is for testing purposes only. Used to disagree to the terms, in order to test agreeing to the terms again.
    @AuraEnabled
    public static User disagreeToTerms() {
        String userID = UserInfo.getUserId();
        User user = [SELECT Id, ix_Terms_Agreed__c FROM User WHERE Id=:userID];
        
        if (user != null) {
            user.ix_Terms_Agreed__c = false;
        	update user;
            
            return user;
        } else {
            return null;
        }
    }
}