public class BrokerIqCarouselController {

	@AuraEnabled
	public static List<IQ_Content__c> getCarouselArticles() {
		return [SELECT Id, Name, RecordType.Name, Primary_Topic__c, Content_Date__c, Content_Duration__c,
						Broker_iQ_Contributor__r.Full_Name__c, Broker_iQ_Contributor__r.Portrait_Cache__c, 
						Carousel_Image_1920_Cache__c,
						Carousel_Image_1440_Cache__c,
						Carousel_Image_960_Cache__c,
						Carousel_Image_480_Cache__c
				FROM IQ_Content__c 
				WHERE Is_Carousel__c = true];
	}	
}