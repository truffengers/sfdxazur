public virtual class PortalController {

    @AuraEnabled
    public Contact thisUserAsContact {get; set;}
    @AuraEnabled
    public User thisUser {get; set;}
    @AuraEnabled
    public String userType {get; set;}

    public PortalController() {
        try {
            userType = UserInfo.getUserType();
            thisUser = PortalUserService.getCurrentUser();
            thisUserAsContact = PortalUserService.getCurrentUserAsContact();
        } catch(Exception e) {
            if(userType != 'Guest') {
                //TODO Probably this is where I return a message that the user is not a portal user and handle itwhen the user clicks on the webinar
                return;
            }
        }
    }

    @AuraEnabled
    public static PortalController getInstance() {
        return new PortalController();
    }
}