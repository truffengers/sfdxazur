public class ReCaptchaApi extends Nebula_API.NebulaApi {
	
	private static ReCaptcha__c cachedApiSettings;

	public ReCaptcha__c getApiSettings() {
		if(cachedApiSettings == null) {
			cachedApiSettings = ReCaptcha__c.getInstance();
		}
		return cachedApiSettings;
	}

	public ReCaptchaApi() {
		super('reCAPTCHA');
	}

	public Boolean verify(String response) {
        ReCaptcha__c apiSettings = getApiSettings();
		HttpRequest req = new HttpRequest();

        req.setMethod('POST');        
        req.setEndpoint('https://www.google.com/recaptcha/api/siteverify');
        req.setBody(Nebula_API.NebulaApi.urlEncode(new Map<String, String>{
        	'secret' => apiSettings.Secret_Key__c,
        	'response' => response
    	}));

        HttpResponse res = makeCallout(req);

        Map<String, Object> parsedResponse = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

        return parsedResponse.get('success') == true;
	}
}