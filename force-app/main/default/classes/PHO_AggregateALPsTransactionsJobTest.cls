/**
* @name: PHO_AggregateALPsTransactionsJobTest
* @description: Test class for PHO_AggregateALPsTransactionsJob class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 08/01/2020
* @lastChangedBy: Konrad Wlazlo   31/03/2020    PR-79 Added test method to cover scenario with exception handling
*/
@IsTest
public class PHO_AggregateALPsTransactionsJobTest {

    private static final String CARRIER_ACCOUNT = 'Carrier Test';
    private static final String ACCOUNTING_BROKER = 'Accounting Broker Test';
    private static final String POLICY_NUMBER_1 = '1';
    private static final String PRODUCT_NAME = 'Test product';

    @TestSetup
    private static void createTestData() {
        IBA_TestDataFactory.createCustomSettings();
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        Account carrier = IBA_TestDataFactory.createAccount(false, CARRIER_ACCOUNT, IBA_UtilConst.ACCOUNT_RT_CARRIER);
        Account accountingBroker = IBA_TestDataFactory.createAccount(false, ACCOUNTING_BROKER, IBA_UtilConst.ACCOUNT_RT_BROKER);
        insert new List<Account>{carrier, accountingBroker};
        Asset policy = IBA_TestDataFactory.createPolicy(false, POLICY_NUMBER_1, settings.IBA_DummyAccount__c);
        policy.IBA_AccountingBroker__c = accountingBroker.Id;
        policy.Policy_Number__c = POLICY_NUMBER_1;
        insert policy;
        IBA_PolicyTransaction__c policyTransaction = IBA_TestDataFactory.createPolicyTransaction(false, policy.Id);
        policyTransaction.IBA_EffectiveDate__c = Date.newInstance(2019, 1, 1);
        insert policyTransaction;
        IBA_TestDataFactory.createProduct(true, PRODUCT_NAME, IBA_UtilConst.PRODUCT_RT_Master);
    }

    @IsTest
    private static void testExecuteSuccess() {
        // Prepare data
        IBA_PolicyTransaction__c policyTransaction = [SELECT Id FROM IBA_PolicyTransaction__c];
        Account carrier = [SELECT Id FROM Account WHERE Name = :CARRIER_ACCOUNT];
        IBA_FFPolicyCarrierLine__c carrierLine = IBA_TestDataFactory.createPolicyCarrierLine(true, policyTransaction.Id, carrier.Id);
        IBA_PolicyTransactionLine__c transactionLine = IBA_TestDataFactory.createPolicyTransactionLine(false, policyTransaction.Id, carrierLine.Id);
        Product2 product = [SELECT Id FROM Product2];
        transactionLine.IBA_Product__c = product.Id;
        transactionLine.IBA_BrokerCommission__c = 0;
        transactionLine.IBA_CarrierFee__c = 0;
        transactionLine.IBA_ForeignTax__c = 0;
        transactionLine.IBA_GrossWrittenPremium__c = 0;
        transactionLine.IBA_InsurancePremiumTax__c = 0;
        transactionLine.IBA_MGACommission__c = 1;
        insert transactionLine;

        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_AggregateALPsTransactionsJob(1));
        Test.stopTest();


        // Verify results
        IBA_PolicyTransactionLine__c transactionLineQueried = [
                SELECT PHO_ALPsMGACommission__c
                FROM IBA_PolicyTransactionLine__c
                WHERE Id = :transactionLine.Id
        ];

        System.assertEquals(transactionLine.IBA_MGACommission__c, transactionLineQueried.PHO_ALPsMGACommission__c, 'Aggregated field should be the same as source field');
    }

    @IsTest
    private static void testExecuteNoTransactions() {
        // Prepare data
        IBA_PolicyTransaction__c policyTransaction = [SELECT IBA_EffectiveDate__c FROM IBA_PolicyTransaction__c];
        policyTransaction.IBA_EffectiveDate__c = Date.newInstance(2018, 1, 1);
        update policyTransaction;

        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_AggregateALPsTransactionsJob(1));
        Test.stopTest();


        // Verify results
        IBA_PolicyTransaction__c transactionQueried = [
                SELECT PHO_AggregationProcessed__c
                FROM IBA_PolicyTransaction__c
                WHERE Id = :policyTransaction.Id
        ];

        System.assertEquals(false, transactionQueried.PHO_AggregationProcessed__c, 'Transaction should not be processed');
    }

    @IsTest
    private static void testExecuteException() {
        // Perform test
        Test.startTest();
        PHO_AggregateALPsTransactionsJob job = new PHO_AggregateALPsTransactionsJob(1);
        // Below is not a good way to test exception in general, however currently it is not possible to provoke exception in a different way
        job.transactions = null;
        System.enqueueJob(job);
        Test.stopTest();


        // Verify results
        Integer azurLogs = [
                SELECT COUNT()
                FROM Azur_Log__c
                WHERE Class__c = 'PHO_AggregateALPsTransactionsJob'
        ];
        System.assertEquals(1, azurLogs, 'There should be one Azur Log created');
    }
}