public with sharing class BrokerIqNavTopicsProviderController {
	
	@AuraEnabled 
	public static List<ConnectAPI.ManagedTopic> getTopics() {
		return ConnectAPI.ManagedTopics.getManagedTopics(Network.getNetworkId(), ConnectApi.ManagedTopicType.Navigational).managedTopics;
	}
}