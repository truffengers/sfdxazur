public class UserHistoryService {

    private List<User> oldUsers;
    private List<User> newUsers;
    
    public UserHistoryService(List<User> oldUsers, List<User> newUsers) {
        this.oldUsers = oldUsers;
        this.newUsers = newUsers;
    }
    
    public List<User_History__c> getChangeHistories() {
        List<FieldSetMember> trackedFields = Schema.SObjectType.User.fieldSets.History_Tracking.getFields();
        
        List<User_History__c> rval = new List<User_History__c>();
        
        for(Integer i=0; i < oldUsers.size(); i++) {
            User newUser = newUsers[i];
            User oldUser = oldUsers[i];
            
            for(FieldSetMember f : trackedFields) {
                Object oldVal = oldUser.get(f.getFieldPath());
                Object newVal = newUser.get(f.getFieldPath());
                
                if(oldVal != newVal || (oldVal != null && !oldVal.equals(newVal))) {
                    User_History__c newHistory = new User_History__c(
                        User__c = newUser.Id,
                        Field_Developer_Name__c  = f.getFieldPath(),
                        Field_Label__c = f.getLabel()
                    );
                    
                    if(newVal != null) {
                        newHistory.New_Value__c = JSON.serialize(newVal);
                    }
                    if(oldVal != null) {
                        newHistory.Old_Value__c = JSON.serialize(oldVal);
                    }
                    
                    rval.add(newHistory);
                }
            }
        }
        
        return rval;
    }
    
    @future
    public static void insertFuture(String jsonUserHistories) {
        System.debug('jsonUserHistories=' + jsonUserHistories);
        List<User_History__c> userHistories = (List<User_History__c>)JSON.deserialize(jsonUserHistories, List<User_History__c>.class);
        
        insert userHistories;
    }
    
}