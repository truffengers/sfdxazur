/**
* @name: PHO_CreateOperationalData2Job
* @description: Asynchronous job to translate Phoenix Rising records into Salesforce entities. To be used for Phoenix secondary data.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 23/03/2020
* @lastChangedBy: Konrad Wlazlo   31/03/2020    PR-75 Added logic to send email with job result and create Azur Log in case of error
*/
public with sharing class PHO_CreateOperationalData2Job implements Queueable {

    @TestVisible
    private List<PHO_PhoenixStaging__c> stagingRecords;
    private Integer recordLimit;
    private Set<Id> recordIds;

    public PHO_CreateOperationalData2Job(Integer recordLimit, Set<Id> recordIds) {
        this.recordLimit = recordLimit;
        this.recordIds = recordIds;
        this.stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                ' FROM PHO_PhoenixStaging__c' +
                ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                (this.recordIds != null ? ' AND Id IN :this.recordIds' : '') +
                ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                ' LIMIT :this.recordLimit'
        );
    }

    public void execute(QueueableContext context) {
        List<String> emailAddresses = new List<String> {System.UserInfo.getUserEmail()};
        List<Azur_Log__c> customLogs = new List<Azur_Log__c>();
        LogBuilder logBuilder = new LogBuilder();

        Savepoint sp = Database.setSavepoint();
        Boolean continueProcessing = true;

        try {
            if (!this.stagingRecords.isEmpty()) {
                PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(this.stagingRecords);
            } else {
                continueProcessing = false;

                EmailUtil email = new EmailUtil(emailAddresses);
                email.plainTextBody('Phoenix Secondary Operational Data Creation process succeeded');
                email.subject('Phoenix Secondary Operational Data Creation process succeeded');
                email.sendEmail();
            }
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug(LoggingLevel.ERROR, 'PHO Exception occured. Queueable job terminated. Error message: '+e.getMessage() +'. Stack trace: '+e.getStackTraceString());
            continueProcessing = false;

            EmailUtil email = new EmailUtil(emailAddresses);
            email.plainTextBody('An error occurred during Phoenix Secondary Operational Data Creation process, please check the latest Azur logs for more details');
            email.subject('Phoenix Secondary Operational Data Creation process failed');
            email.sendEmail();
            Azur_Log__c newLog = logBuilder.createLogWithCodeInfo(e,'Phoenix Rising','PHO_CreateOperationalData2Job','execute');
            customLogs.add(newLog);
            insert customLogs;
        }

        if (continueProcessing && !Test.isRunningTest()) {
            System.enqueueJob(new PHO_CreateOperationalData2Job(this.recordLimit, this.recordIds));
        }
    }
}