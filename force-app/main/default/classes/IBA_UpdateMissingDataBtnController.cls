public with sharing class IBA_UpdateMissingDataBtnController {

    private ApexPages.StandardSetController standardSetController;

    public IBA_UpdateMissingDataBtnController(ApexPages.StandardSetController standardSetController) {
        this.standardSetController = standardSetController;
    }

    public PageReference updateMissingData() {
        update [SELECT Id, IBA_Status__c FROM IBA_IBAStaging__c WHERE IBA_Status__c = :IBA_UtilConst.STAGING_STATUS_MISSING_DATA];
        
        Schema.DescribeSObjectResult result = IBA_IBAStaging__c.SObjectType.getDescribe();
        PageReference pageRef = new PageReference('/' + result.getKeyPrefix());
        pageRef.setRedirect(true);
        return pageRef;
    }
}