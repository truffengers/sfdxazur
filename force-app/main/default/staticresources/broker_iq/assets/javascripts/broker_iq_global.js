(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = (function() {

  var $element;

  var WINDOWS_PHONE = 'windows-phone';
  var ANDROID       = 'android';
  var IOS           = 'ios';

  var mobileOS;
  var COOKIE_KEY = 'addToHomeConcealed';

  var ON     = 'on';
  var OFF    = 'off';

  var DELAY_MS = 3000;

  var isRevealed;



  function referenceElements() {
    $element = $('#js-add-to-home');
  }

  function turnListeners(onOfOff) {
    $('body')[onOfOff]('click touchend', conceal);
  }

  function detectMobileOS() {
    mobileOS = getMobileOS();
  }

  function getMobileOS() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if(/windows phone/i.test(userAgent)) {
      return WINDOWS_PHONE;
    } else if(/android/i.test(userAgent)) {
      return ANDROID;
    } else if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return IOS;
    }
  }

  function getPreviouslyConcealed() {
    return Cookies.get(COOKIE_KEY) !== undefined;
  }

  function getIsAppropriate() {
    return getIsIosOrAndroid() && !getPreviouslyConcealed();
  }

  function getIsIosOrAndroid() {
    return mobileOS === IOS || mobileOS === ANDROID;
  }

  function revealIfAppropriate() {
    if(getIsAppropriate()) { revealAfterDelay(); }
  }

  function revealAfterDelay() {
    window.setTimeout(reveal, DELAY_MS);
  }

  function reveal() {
    referenceElements();
    turnListeners(ON);
    toggleMobileOsClass();
    toggleVisibilityClass();
    isRevealed = true;
  }

  function conceal() {
    if(isRevealed) {
      isRevealed = false;
      Cookies.set(COOKIE_KEY, true, { expires: 28 });
      turnListeners(OFF);
      toggleVisibilityClass();
    }
  }

  function toggleMobileOsClass() {
    $element.toggleClass('is-' + mobileOS);
  }

  function toggleVisibilityClass() {
    $element.toggleClass('is-visible');
  }

  function init() {
    detectMobileOS();
    revealIfAppropriate();
  }



  return { init: init };

}());
},{}],2:[function(require,module,exports){
module.exports = function(yOffset, inertia) {
  this.yOffset = yOffset;
  this.inertia = inertia;
};
},{}],3:[function(require,module,exports){
(function (global){
require('ls.bgset');
require('ls.respimg');
require('ls.parent-fit');
require('lazysizes');
require('jqueryClam');

global.Cookies = require('js-cookie');

global._ = {
  throttle : require('lodash/throttle'),
  capitalize : require('lodash/capitalize')
};



jQuery.extend(TT, {
  addToHome                : require('addToHome'),
  BackgroundData           : require('BackgroundData'),
  clamShells               : require('clamShells'),
  heightAnimator           : require('heightAnimator'),
  initBrokerIq             : require('initBrokerIq'),
  initFilers               : require('initFilers'),
  mediaMatcher             : require('mediaMatcher'),
  menuVisibilityManager    : require('menuVisibilityManager'),
  parallax                 : require('parallax'),
  brokerIqParallaxElements : require('brokerIqParallaxElements'),
  scrollBroadcaster        : require('scrollBroadcaster'),
  scrollRatioCalculator    : require('scrollRatioCalculator'),
  svg4everybody            : require('svg4everybody')
});
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"BackgroundData":2,"addToHome":1,"brokerIqParallaxElements":4,"clamShells":5,"heightAnimator":6,"initBrokerIq":7,"initFilers":8,"jqueryClam":9,"js-cookie":15,"lazysizes":16,"lodash/capitalize":35,"lodash/throttle":42,"ls.bgset":17,"ls.parent-fit":18,"ls.respimg":19,"mediaMatcher":10,"menuVisibilityManager":11,"parallax":12,"scrollBroadcaster":13,"scrollRatioCalculator":14,"svg4everybody":46}],4:[function(require,module,exports){
module.exports = (function($) {

  var $scrollable;

  return {

    init: function() {

      $scrollable = $(window);

      TT.parallax({
        $elements   : $('.c-page-strip'),
        data        : [
          new TT.BackgroundData('40%', -0.25)
        ],
        $scrollable : $scrollable
      });


      TT.parallax({
        $elements   : $('.c-bg-slice'),
        data        : [
          new TT.BackgroundData('0%', 0.1),
          new TT.BackgroundData('0%', 0.2)
        ],
        $scrollable : $scrollable
      });

      TT.parallax({
        $elements   : $('.c-banner__img'),
        data        : [ new TT.BackgroundData('0%', 0.05) ],
        $scrollable : $scrollable
      });

    }
  };

}(jQuery));
},{}],5:[function(require,module,exports){
module.exports = (function() {

  return {
    init: function() {
      $('.js-clam-shell').clam();
    }
  };

}());
},{}],6:[function(require,module,exports){
module.exports = function($container, options) {

  "use strict";

  var defaults = {
    duration : 500,
  };

  var callback;
  var startHeight;
  var settings = $.extend({}, defaults, options || {});



  function storeStartHeight() {
    startHeight = settings.reveal ? 0 : $container.height();
  }

  function getFinishHeight() {
    return settings.conceal ? 0 : $container.height();
  }

  function clearHeight() {
    $container.height('auto');
    startHeight = undefined;
  }

  function heightAnimatedHandler() {
    clearHeight();
    if(callback) {
      callback();
      callback = undefined;
    }
  }

  storeStartHeight();



  return {

    animate: function(value) {
      callback = value;
      var finishHeight = getFinishHeight();
      if(startHeight !== finishHeight) {
        $container.height(startHeight).transition({ height: finishHeight }, settings.duration, heightAnimatedHandler);
      } else {
        heightAnimatedHandler();
      }
    }

  };

};
},{}],7:[function(require,module,exports){
module.exports = function() {

  'use strict';

  if(!$.support.transition) { $.fn.transition = $.fn.animate; }

  TT.mediaMatcher.init({
    mobile  : '(max-width: 860px)',
    desktop : '(min-width: 861px)'
  });

  $('.c-select-nav').hover(function() {});

  TT.scrollBroadcaster.init({ window: window, isLogging: false });
  TT.scrollBroadcaster.enable();
  TT.menuVisibilityManager.init();
  TT.brokerIqParallaxElements.init();
  TT.clamShells.init();
  TT.initFilers();
  TT.addToHome.init();

  TT.svg4everybody();

};
},{}],8:[function(require,module,exports){
module.exports = function() {

  var ACTIVE_CLASS = 'is-active';
  var $filerLabels = $('.js-filer__label');
  var $filerInners = $('.js-filer__inner');

  $filerLabels.on('click', function(event) {
    event.preventDefault();
    var fileId = '#' + $(this).data('target-id');
    $filerLabels.removeClass(ACTIVE_CLASS);
    $filerInners.removeClass(ACTIVE_CLASS);
    $(this).addClass(ACTIVE_CLASS);
    $(fileId).addClass(ACTIVE_CLASS);
  });

};

},{}],9:[function(require,module,exports){
(function( $ ) {

  $.fn.clam = function() {

    var clams = [];

    function clam(element) {

      var ease        = $.support.transition ? 'ease-in-out' : 'swing';
      var $element    = $(element);
      var $button     = $element.find('> :first-child');
      var $body       = $element.find('> :last-child');
      var id          = $element.attr('id');
      var cookieKey   = 'clam-' + id;
      var isAnimating = false;
      var OPENED      = 'opened';
      var CLOSED      = 'closed';
      var OPEN_CLASS  = 'is-open';
      var state;



      function init() {
        state = getInitialState();
        if(state === OPENED) { open(); }
        $button.click(clickHandler);
      }

      function getInitialState() {
        return id !== undefined && Cookies.set(cookieKey) === OPENED ? OPENED : CLOSED;
      }

      function clickHandler(event) {
        event.preventDefault();
        if(isAnimating) { return; }
        state === OPENED ? close() : open();
      }

      function updateCookie() {
        Cookies.set(cookieKey, state, { expires: 365 });
      }

      function animate(finishHeight, callback) {
        $body.transition({ height: finishHeight }, ease, function() {
          isAnimating = false;
          callback();
        });
      }

      function open() {
        isAnimating = true;
        state = OPENED;
        updateCookie();
        $element.addClass(OPEN_CLASS);
        var finishHeight = $body.height();
        $body.height(0);
        animate(finishHeight, function() {
          $body.height('');
        });
      }

      function close() {
        isAnimating = true;
        state = CLOSED;
        updateCookie();
        $body.height($body.height());
        animate(0, function() {
          $element.removeClass(OPEN_CLASS);
          $body.height('');
        });
      }

      init();
    }


    $(this).each(function() { clams.push(clam(this)); });

    return this;

  };

}( jQuery ));
},{}],10:[function(require,module,exports){
module.exports = (function() {

  "use strict";

  var self = {};
  var $self = $(self);
  var mediaQueryDefinitions = {};
  var mediaQueryLists = [];
  var mediaName;



  function buildLookupAndDefineConstants(definitions) {
    $.each(definitions, function(name, query) {
      mediaQueryDefinitions[window.matchMedia(query).media] = name;
      self[name.toUpperCase()] = name;
      self['getIs' + _.capitalize(name)] = function() {
        return mediaName === name;
      };
    });
  }

  function defineMediaQueries() {
    $.each(mediaQueryDefinitions, defineMediaQuery);
  }

  function defineMediaQuery(media) {
    var mediaQueryList = window.matchMedia(media);
    mediaQueryList.addListener(mediaChangeHandler);
    mediaQueryLists.push(mediaQueryList);
  }

  function mediaChangeHandler(mediaQueryList) {
    if(mediaQueryList.matches) {
      mediaName = getNameFromMedia(mediaQueryList.media);
      dispatchMediaMatchEvent();
    }
  }

  function dispatchMediaMatchEvent() {
    $self.triggerHandler('mediaChange', [mediaName]);
  }

  function findMatch() {
    for(var i=0; i<mediaQueryLists.length; i++) {
      if(mediaQueryLists[i].matches) {
        return getNameFromMedia(mediaQueryLists[i].media);
      }
    }
  }

  function getNameFromMedia(media) {
    return mediaQueryDefinitions[media];
  }



  $.extend(self, {

    init: function(definitions) {
      buildLookupAndDefineConstants(definitions);
      defineMediaQueries();
      mediaName = findMatch();
    },

    getMediaName: function() {
      return mediaName;
    }

  });



  return self;


}());
},{}],11:[function(require,module,exports){
module.exports = (function($) {

  var $header;
  var previousPosition           = 0;
  var isShowing                  = true;
  var HIDE_CLASS                 = 'is-hidden';
  var HIDE_SHOW_OFFSET           = 15;
  var headerHeight;
  var disableIds                 = [];


  function referenceElements() {
    $header = $('#js-header');
  }

  function addListeners() {
    $(TT.scrollBroadcaster).on('scroll', scrollHandler);
    $(TT.mediaMatcher).on('mediaChange', mediaChangeHandler);
  }

  function isHovering() {
    return $header.is(':hover');
  }

  function hide() {
    $header.addClass(HIDE_CLASS);
    isShowing = false;
  }

  function show() {
    $header.removeClass(HIDE_CLASS);
    isShowing = true;
  }

  function getIsEnabled() {
    return disableIds.length === 0;
  }

  function scrollHandler(event, data) {

    if(!getIsEnabled()) { return; }

    var currentPosition = data.topTrigger;
    var difference      = Math.abs(currentPosition - previousPosition);
    var isScrollingDown = currentPosition > previousPosition;
    var isScrollingFast = difference >= HIDE_SHOW_OFFSET;
    var isAtTop         = currentPosition <= headerHeight;

    function shouldShow() {
      return !isShowing && (isScrollingFast || isAtTop) && !isScrollingDown;
    }

    function shouldHide() {
      return isShowing && !isAtTop && isScrollingFast && isScrollingDown && !isHovering();
    }

    if(shouldHide()) {
      hide();
    } else if(shouldShow()) {
      show();
    }

    previousPosition = currentPosition;
  }

  function mediaChangeHandler() {
    recordHeaderHeight();
  }

  function recordHeaderHeight() {
    headerHeight = $header.height();
  }



  return {

    init: function() {
      referenceElements();
      addListeners();
      recordHeaderHeight();
    },

    disable: function(id) {
      disableIds.push(id);
    },

    enable: function(idToRemove) {
      disableIds = disableIds.filter(function(id) { return id !== idToRemove; });
    }
  };

}(jQuery));
},{}],12:[function(require,module,exports){
module.exports = function(params) {

  var $elements   = params.$elements;
  var data        = params.data;
  var $scrollable = params.$scrollable;

  var scrollRatioCalculator;



  function init() {
    addListeners();
    scrollRatioCalculator = TT.scrollRatioCalculator($scrollable);
    move();
    reveal();
  }

  function addListeners() {
    $scrollable.scroll(_.throttle(scrollHandler, 10));
    $scrollable.resize(_.throttle(resizeHandler, 10));
  }

  function resizeHandler() {
    scrollRatioCalculator.recalculateHeight();
    move();
  }

  function scrollHandler() {
    move();
  }

  function percentageToPixels(yOffset) {
    return (parseFloat(yOffset) * scrollRatioCalculator.getTotalHeight()) / 100;
  }

  function calculatePosition(yOffset, inertia) {
    return Math.round(percentageToPixels(yOffset) + (scrollRatioCalculator.getRatio() * scrollRatioCalculator.getTotalHeight() * inertia)) + 'px';
  }

  function getPositions() {
    return $.map(data, function(e) {
      return calculatePosition(e.yOffset, e.inertia);
    });
  }

  function move() {
    var positions = getPositions();
    $.each($elements, function(i, element) {
      $(element).css({transform: 'translate3d(0,' + positions[i] + ',0)'});
    });
  }

  function reveal() {
    $elements.addClass('is-positioned');
  }


  init();



  return {

    teardown: function() {
      $scrollable.unbind('scroll resize');
      scrollRatioCalculator.teardown();
      data                  = null;
      $scrollable           = null;
      scrollRatioCalculator = null;
    }

  };

};
},{}],13:[function(require,module,exports){
module.exports = (function($) {

  "use strict";

  var $window;
  var $document;
  var lastTop = 0;
  var lastHeight;
  var self = { UP: 'up', DOWN: 'down' };
  var $self = $(self);
  var scrollThrottleFunction;
  var resizeThrottleFunction;
  var settings;
  var isEnabled = false;
  var eventData;

  var ON  = 'on';
  var OFF = 'off';

  var defaults = {
    scrollInterval : 30,
    resizeInterval : 1000,
    window         : window,
    isLogging      : false
  };



  function setDefaults(options) {
    settings = $.extend({}, defaults, options || {});
  }

  function referenceElements() {
    $window    = $(settings.window);
    $document  = $(document);
  }

  function turnScrollListener(onOrOff) {
    $window[onOrOff]('scroll', scrollThrottleFunction);
  }

  function turnResizeListener(onOrOff) {
    $window[onOrOff]('scroll', resizeThrottleFunction);
  }

  function initThrottleFunctions() {
    scrollThrottleFunction = _.throttle(scrollHandler, settings.scrollInterval, { trailing: false });
    resizeThrottleFunction = _.throttle(resizeHandler, settings.resizeInterval, { trailing: false });
  }

  function getHeight() {
    var innerHeight = settings.window.innerHeight;
    return innerHeight === undefined ? settings.window.clientHeight : innerHeight;
  }

  function getTop() {
    var top = settings.window.scrollTop;
    return top === undefined ? window.pageYOffset : top;
  }

  function getDirection(top) {
    return top >= lastTop ? self.DOWN : self.UP;
  }

  function dispatchScrollEvent() {
    log();
    $self.triggerHandler('scroll', [eventData]);
  }

  function log() {
    if(settings.isLogging) { console.info(eventData); }
  }

  function storeScrollEventData(direction, topTrigger, bottomTrigger, triggerY, height) {
    eventData = {
      direction     : direction,
      topTrigger    : topTrigger,
      bottomTrigger : bottomTrigger,
      triggerY      : triggerY,
      height        : height
    };
  }

  function scrollHandler(event, options) {
    var skipDispatching = options && options.skipDispatching === true ? true : false;
    var top             = getTop();
    var direction       = getDirection(top);
    var height          = getHeight();
    var topTrigger      = top;
    var bottomTrigger   = top + height;
    var triggerY        = direction === self.UP ? top : top + height;
    storeScrollEventData(direction, topTrigger, bottomTrigger, triggerY, height);
    if(!skipDispatching && isEnabled) { dispatchScrollEvent(); }
    lastTop = top;
  }



  // Height change recalculations

  function resizeHandler() {
    var height = $document.height();
    if(height !== lastHeight) {
      lastHeight = height;
      dispatchHeightChangeEvent();
    }
  }

  function dispatchHeightChangeEvent() {
    $self.triggerHandler('heightChange', [lastHeight]);
  }


  $.extend(self, {

    init: function(options) {
      setDefaults(options);
      referenceElements();
      initThrottleFunctions();
      turnScrollListener(ON);
    },

    enable: function() {
      isEnabled = true;
      turnResizeListener(ON);
      scrollHandler();
    },

    disable: function() {
      isEnabled = false;
      turnResizeListener(OFF);
    },

    lastScrollEventData: function() {
      if(!eventData) { scrollHandler(null, { skipDispatching: true }); }
      return eventData;
    }

  });



  return self;

}(jQuery));
},{}],14:[function(require,module,exports){
module.exports = function($element) {

  var visibleHeight;
  var totalHeight;
  var element = $element[0];



  function recalculateHeight() {
    visibleHeight = $element.height();
    totalHeight   = element === window ? document.documentElement.scrollHeight : element.scrollHeight;
  }

  function getTotalHeight() {
    return totalHeight;
  }

  function getScrollLimit() {
    return totalHeight - visibleHeight;
  }

  function getRatio() {
    return $element.scrollTop() / getScrollLimit();
  }

  function teardown() {
    $element = null;
    element  = null;
  }

  recalculateHeight();


  return {
    recalculateHeight : recalculateHeight,
    getRatio          : getRatio,
    getTotalHeight    : getTotalHeight,
    teardown          : teardown
  };

};
},{}],15:[function(require,module,exports){
/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;(function (factory) {
  var registeredInModuleLoader = false;
  if (typeof define === 'function' && define.amd) {
    define(factory);
    registeredInModuleLoader = true;
  }
  if (typeof exports === 'object') {
    module.exports = factory();
    registeredInModuleLoader = true;
  }
  if (!registeredInModuleLoader) {
    var OldCookies = window.Cookies;
    var api = window.Cookies = factory();
    api.noConflict = function () {
      window.Cookies = OldCookies;
      return api;
    };
  }
}(function () {
  function extend () {
    var i = 0;
    var result = {};
    for (; i < arguments.length; i++) {
      var attributes = arguments[ i ];
      for (var key in attributes) {
        result[key] = attributes[key];
      }
    }
    return result;
  }

  function init (converter) {
    function api (key, value, attributes) {
      var result;
      if (typeof document === 'undefined') {
        return;
      }

      // Write

      if (arguments.length > 1) {
        attributes = extend({
          path: '/'
        }, api.defaults, attributes);

        if (typeof attributes.expires === 'number') {
          var expires = new Date();
          expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
          attributes.expires = expires;
        }

        try {
          result = JSON.stringify(value);
          if (/^[\{\[]/.test(result)) {
            value = result;
          }
        } catch (e) {}

        if (!converter.write) {
          value = encodeURIComponent(String(value))
            .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
        } else {
          value = converter.write(value, key);
        }

        key = encodeURIComponent(String(key));
        key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
        key = key.replace(/[\(\)]/g, escape);

        return (document.cookie = [
          key, '=', value,
          attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
          attributes.path ? '; path=' + attributes.path : '',
          attributes.domain ? '; domain=' + attributes.domain : '',
          attributes.secure ? '; secure' : ''
        ].join(''));
      }

      // Read

      if (!key) {
        result = {};
      }

      // To prevent the for loop in the first place assign an empty array
      // in case there are no cookies at all. Also prevents odd result when
      // calling "get()"
      var cookies = document.cookie ? document.cookie.split('; ') : [];
      var rdecode = /(%[0-9A-Z]{2})+/g;
      var i = 0;

      for (; i < cookies.length; i++) {
        var parts = cookies[i].split('=');
        var cookie = parts.slice(1).join('=');

        if (cookie.charAt(0) === '"') {
          cookie = cookie.slice(1, -1);
        }

        try {
          var name = parts[0].replace(rdecode, decodeURIComponent);
          cookie = converter.read ?
            converter.read(cookie, name) : converter(cookie, name) ||
            cookie.replace(rdecode, decodeURIComponent);

          if (this.json) {
            try {
              cookie = JSON.parse(cookie);
            } catch (e) {}
          }

          if (key === name) {
            result = cookie;
            break;
          }

          if (!key) {
            result[name] = cookie;
          }
        } catch (e) {}
      }

      return result;
    }

    api.set = api;
    api.get = function (key) {
      return api.call(api, key);
    };
    api.getJSON = function () {
      return api.apply({
        json: true
      }, [].slice.call(arguments));
    };
    api.defaults = {};

    api.remove = function (key, attributes) {
      api(key, '', extend(attributes, {
        expires: -1
      }));
    };

    api.withConverter = init;

    return api;
  }

  return init(function () {});
}));

},{}],16:[function(require,module,exports){
(function(window, factory) {
  var lazySizes = factory(window, window.document);
  window.lazySizes = lazySizes;
  if(typeof module == 'object' && module.exports){
    module.exports = lazySizes;
  }
}(window, function l(window, document) {
  'use strict';
  /*jshint eqnull:true */
  if(!document.getElementsByClassName){return;}

  var lazySizesConfig;

  var docElem = document.documentElement;

  var Date = window.Date;

  var supportPicture = window.HTMLPictureElement;

  var _addEventListener = 'addEventListener';

  var _getAttribute = 'getAttribute';

  var addEventListener = window[_addEventListener];

  var setTimeout = window.setTimeout;

  var requestAnimationFrame = window.requestAnimationFrame || setTimeout;

  var requestIdleCallback = window.requestIdleCallback;

  var regPicture = /^picture$/i;

  var loadEvents = ['load', 'error', 'lazyincluded', '_lazyloaded'];

  var regClassCache = {};

  var forEach = Array.prototype.forEach;

  var hasClass = function(ele, cls) {
    if(!regClassCache[cls]){
      regClassCache[cls] = new RegExp('(\\s|^)'+cls+'(\\s|$)');
    }
    return regClassCache[cls].test(ele[_getAttribute]('class') || '') && regClassCache[cls];
  };

  var addClass = function(ele, cls) {
    if (!hasClass(ele, cls)){
      ele.setAttribute('class', (ele[_getAttribute]('class') || '').trim() + ' ' + cls);
    }
  };

  var removeClass = function(ele, cls) {
    var reg;
    if ((reg = hasClass(ele,cls))) {
      ele.setAttribute('class', (ele[_getAttribute]('class') || '').replace(reg, ' '));
    }
  };

  var addRemoveLoadEvents = function(dom, fn, add){
    var action = add ? _addEventListener : 'removeEventListener';
    if(add){
      addRemoveLoadEvents(dom, fn);
    }
    loadEvents.forEach(function(evt){
      dom[action](evt, fn);
    });
  };

  var triggerEvent = function(elem, name, detail, noBubbles, noCancelable){
    var event = document.createEvent('CustomEvent');

    event.initCustomEvent(name, !noBubbles, !noCancelable, detail || {});

    elem.dispatchEvent(event);
    return event;
  };

  var updatePolyfill = function (el, full){
    var polyfill;
    if( !supportPicture && ( polyfill = (window.picturefill || lazySizesConfig.pf) ) ){
      polyfill({reevaluate: true, elements: [el]});
    } else if(full && full.src){
      el.src = full.src;
    }
  };

  var getCSS = function (elem, style){
    return (getComputedStyle(elem, null) || {})[style];
  };

  var getWidth = function(elem, parent, width){
    width = width || elem.offsetWidth;

    while(width < lazySizesConfig.minSize && parent && !elem._lazysizesWidth){
      width =  parent.offsetWidth;
      parent = parent.parentNode;
    }

    return width;
  };

  var rAF = (function(){
    var running, waiting;
    var fns = [];

    var run = function(){
      var fn;
      running = true;
      waiting = false;
      while(fns.length){
        fn = fns.shift();
        fn[0].apply(fn[1], fn[2]);
      }
      running = false;
    };

    var rafBatch = function(fn){
      if(running){
        fn.apply(this, arguments);
      } else {
        fns.push([fn, this, arguments]);

        if(!waiting){
          waiting = true;
          (document.hidden ? setTimeout : requestAnimationFrame)(run);
        }
      }
    };

    rafBatch._lsFlush = run;

    return rafBatch;
  })();

  var rAFIt = function(fn, simple){
    return simple ?
      function() {
        rAF(fn);
      } :
      function(){
        var that = this;
        var args = arguments;
        rAF(function(){
          fn.apply(that, args);
        });
      }
    ;
  };

  var throttle = function(fn){
    var running;
    var lastTime = 0;
    var gDelay = 125;
    var RIC_DEFAULT_TIMEOUT = 666;
    var rICTimeout = RIC_DEFAULT_TIMEOUT;
    var run = function(){
      running = false;
      lastTime = Date.now();
      fn();
    };
    var idleCallback = requestIdleCallback ?
      function(){
        requestIdleCallback(run, {timeout: rICTimeout});
        if(rICTimeout !== RIC_DEFAULT_TIMEOUT){
          rICTimeout = RIC_DEFAULT_TIMEOUT;
        }
      }:
      rAFIt(function(){
        setTimeout(run);
      }, true)
    ;

    return function(isPriority){
      var delay;
      if((isPriority = isPriority === true)){
        rICTimeout = 44;
      }

      if(running){
        return;
      }

      running =  true;

      delay = gDelay - (Date.now() - lastTime);

      if(delay < 0){
        delay = 0;
      }

      if(isPriority || (delay < 9 && requestIdleCallback)){
        idleCallback();
      } else {
        setTimeout(idleCallback, delay);
      }
    };
  };

  //based on http://modernjavascript.blogspot.de/2013/08/building-better-debounce.html
  var debounce = function(func) {
    var timeout, timestamp;
    var wait = 99;
    var run = function(){
      timeout = null;
      func();
    };
    var later = function() {
      var last = Date.now() - timestamp;

      if (last < wait) {
        setTimeout(later, wait - last);
      } else {
        (requestIdleCallback || run)(run);
      }
    };

    return function() {
      timestamp = Date.now();

      if (!timeout) {
        timeout = setTimeout(later, wait);
      }
    };
  };


  var loader = (function(){
    var lazyloadElems, preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;

    var eLvW, elvH, eLtop, eLleft, eLright, eLbottom;

    var defaultExpand, preloadExpand, hFac;

    var regImg = /^img$/i;
    var regIframe = /^iframe$/i;

    var supportScroll = ('onscroll' in window) && !(/glebot/.test(navigator.userAgent));

    var shrinkExpand = 0;
    var currentExpand = 0;

    var isLoading = 0;
    var lowRuns = -1;

    var resetPreloading = function(e){
      isLoading--;
      if(e && e.target){
        addRemoveLoadEvents(e.target, resetPreloading);
      }

      if(!e || isLoading < 0 || !e.target){
        isLoading = 0;
      }
    };

    var isNestedVisible = function(elem, elemExpand){
      var outerRect;
      var parent = elem;
      var visible = getCSS(document.body, 'visibility') == 'hidden' || getCSS(elem, 'visibility') != 'hidden';

      eLtop -= elemExpand;
      eLbottom += elemExpand;
      eLleft -= elemExpand;
      eLright += elemExpand;

      while(visible && (parent = parent.offsetParent) && parent != document.body && parent != docElem){
        visible = ((getCSS(parent, 'opacity') || 1) > 0);

        if(visible && getCSS(parent, 'overflow') != 'visible'){
          outerRect = parent.getBoundingClientRect();
          visible = eLright > outerRect.left &&
            eLleft < outerRect.right &&
            eLbottom > outerRect.top - 1 &&
            eLtop < outerRect.bottom + 1
          ;
        }
      }

      return visible;
    };

    var checkElements = function() {
      var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal, beforeExpandVal;

      if((loadMode = lazySizesConfig.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)){

        i = 0;

        lowRuns++;

        if(preloadExpand == null){
          if(!('expand' in lazySizesConfig)){
            lazySizesConfig.expand = docElem.clientHeight > 500 && docElem.clientWidth > 500 ? 500 : 370;
          }

          defaultExpand = lazySizesConfig.expand;
          preloadExpand = defaultExpand * lazySizesConfig.expFactor;
        }

        if(currentExpand < preloadExpand && isLoading < 1 && lowRuns > 2 && loadMode > 2 && !document.hidden){
          currentExpand = preloadExpand;
          lowRuns = 0;
        } else if(loadMode > 1 && lowRuns > 1 && isLoading < 6){
          currentExpand = defaultExpand;
        } else {
          currentExpand = shrinkExpand;
        }

        for(; i < eLlen; i++){

          if(!lazyloadElems[i] || lazyloadElems[i]._lazyRace){continue;}

          if(!supportScroll){unveilElement(lazyloadElems[i]);continue;}

          if(!(elemExpandVal = lazyloadElems[i][_getAttribute]('data-expand')) || !(elemExpand = elemExpandVal * 1)){
            elemExpand = currentExpand;
          }

          if(beforeExpandVal !== elemExpand){
            eLvW = innerWidth + (elemExpand * hFac);
            elvH = innerHeight + elemExpand;
            elemNegativeExpand = elemExpand * -1;
            beforeExpandVal = elemExpand;
          }

          rect = lazyloadElems[i].getBoundingClientRect();

          if ((eLbottom = rect.bottom) >= elemNegativeExpand &&
            (eLtop = rect.top) <= elvH &&
            (eLright = rect.right) >= elemNegativeExpand * hFac &&
            (eLleft = rect.left) <= eLvW &&
            (eLbottom || eLright || eLleft || eLtop) &&
            ((isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4)) || isNestedVisible(lazyloadElems[i], elemExpand))){
            unveilElement(lazyloadElems[i]);
            loadedSomething = true;
            if(isLoading > 9){break;}
          } else if(!loadedSomething && isCompleted && !autoLoadElem &&
            isLoading < 4 && lowRuns < 4 && loadMode > 2 &&
            (preloadElems[0] || lazySizesConfig.preloadAfterLoad) &&
            (preloadElems[0] || (!elemExpandVal && ((eLbottom || eLright || eLleft || eLtop) || lazyloadElems[i][_getAttribute](lazySizesConfig.sizesAttr) != 'auto')))){
            autoLoadElem = preloadElems[0] || lazyloadElems[i];
          }
        }

        if(autoLoadElem && !loadedSomething){
          unveilElement(autoLoadElem);
        }
      }
    };

    var throttledCheckElements = throttle(checkElements);

    var switchLoadingClass = function(e){
      addClass(e.target, lazySizesConfig.loadedClass);
      removeClass(e.target, lazySizesConfig.loadingClass);
      addRemoveLoadEvents(e.target, rafSwitchLoadingClass);
    };
    var rafedSwitchLoadingClass = rAFIt(switchLoadingClass);
    var rafSwitchLoadingClass = function(e){
      rafedSwitchLoadingClass({target: e.target});
    };

    var changeIframeSrc = function(elem, src){
      try {
        elem.contentWindow.location.replace(src);
      } catch(e){
        elem.src = src;
      }
    };

    var handleSources = function(source){
      var customMedia, parent;

      var sourceSrcset = source[_getAttribute](lazySizesConfig.srcsetAttr);

      if( (customMedia = lazySizesConfig.customMedia[source[_getAttribute]('data-media') || source[_getAttribute]('media')]) ){
        source.setAttribute('media', customMedia);
      }

      if(sourceSrcset){
        source.setAttribute('srcset', sourceSrcset);
      }

      //https://bugzilla.mozilla.org/show_bug.cgi?id=1170572
      if(customMedia){
        parent = source.parentNode;
        parent.insertBefore(source.cloneNode(), source);
        parent.removeChild(source);
      }
    };

    var lazyUnveil = rAFIt(function (elem, detail, isAuto, sizes, isImg){
      var src, srcset, parent, isPicture, event, firesLoad;

      if(!(event = triggerEvent(elem, 'lazybeforeunveil', detail)).defaultPrevented){

        if(sizes){
          if(isAuto){
            addClass(elem, lazySizesConfig.autosizesClass);
          } else {
            elem.setAttribute('sizes', sizes);
          }
        }

        srcset = elem[_getAttribute](lazySizesConfig.srcsetAttr);
        src = elem[_getAttribute](lazySizesConfig.srcAttr);

        if(isImg) {
          parent = elem.parentNode;
          isPicture = parent && regPicture.test(parent.nodeName || '');
        }

        firesLoad = detail.firesLoad || (('src' in elem) && (srcset || src || isPicture));

        event = {target: elem};

        if(firesLoad){
          addRemoveLoadEvents(elem, resetPreloading, true);
          clearTimeout(resetPreloadingTimer);
          resetPreloadingTimer = setTimeout(resetPreloading, 2500);

          addClass(elem, lazySizesConfig.loadingClass);
          addRemoveLoadEvents(elem, rafSwitchLoadingClass, true);
        }

        if(isPicture){
          forEach.call(parent.getElementsByTagName('source'), handleSources);
        }

        if(srcset){
          elem.setAttribute('srcset', srcset);
        } else if(src && !isPicture){
          if(regIframe.test(elem.nodeName)){
            changeIframeSrc(elem, src);
          } else {
            elem.src = src;
          }
        }

        if(srcset || isPicture){
          updatePolyfill(elem, {src: src});
        }
      }

      rAF(function(){
        if(elem._lazyRace){
          delete elem._lazyRace;
        }
        removeClass(elem, lazySizesConfig.lazyClass);

        if( !firesLoad || elem.complete ){
          if(firesLoad){
            resetPreloading(event);
          } else {
            isLoading--;
          }
          switchLoadingClass(event);
        }
      });
    });

    var unveilElement = function (elem){
      var detail;

      var isImg = regImg.test(elem.nodeName);

      //allow using sizes="auto", but don't use. it's invalid. Use data-sizes="auto" or a valid value for sizes instead (i.e.: sizes="80vw")
      var sizes = isImg && (elem[_getAttribute](lazySizesConfig.sizesAttr) || elem[_getAttribute]('sizes'));
      var isAuto = sizes == 'auto';

      if( (isAuto || !isCompleted) && isImg && (elem.src || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesConfig.errorClass)){return;}

      detail = triggerEvent(elem, 'lazyunveilread').detail;

      if(isAuto){
         autoSizer.updateElem(elem, true, elem.offsetWidth);
      }

      elem._lazyRace = true;
      isLoading++;

      lazyUnveil(elem, detail, isAuto, sizes, isImg);
    };

    var onload = function(){
      if(isCompleted){return;}
      if(Date.now() - started < 999){
        setTimeout(onload, 999);
        return;
      }
      var afterScroll = debounce(function(){
        lazySizesConfig.loadMode = 3;
        throttledCheckElements();
      });

      isCompleted = true;

      lazySizesConfig.loadMode = 3;

      throttledCheckElements();

      addEventListener('scroll', function(){
        if(lazySizesConfig.loadMode == 3){
          lazySizesConfig.loadMode = 2;
        }
        afterScroll();
      }, true);
    };

    return {
      _: function(){
        started = Date.now();

        lazyloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass);
        preloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass + ' ' + lazySizesConfig.preloadClass);
        hFac = lazySizesConfig.hFac;

        addEventListener('scroll', throttledCheckElements, true);

        addEventListener('resize', throttledCheckElements, true);

        if(window.MutationObserver){
          new MutationObserver( throttledCheckElements ).observe( docElem, {childList: true, subtree: true, attributes: true} );
        } else {
          docElem[_addEventListener]('DOMNodeInserted', throttledCheckElements, true);
          docElem[_addEventListener]('DOMAttrModified', throttledCheckElements, true);
          setInterval(throttledCheckElements, 999);
        }

        addEventListener('hashchange', throttledCheckElements, true);

        //, 'fullscreenchange'
        ['focus', 'mouseover', 'click', 'load', 'transitionend', 'animationend', 'webkitAnimationEnd'].forEach(function(name){
          document[_addEventListener](name, throttledCheckElements, true);
        });

        if((/d$|^c/.test(document.readyState))){
          onload();
        } else {
          addEventListener('load', onload);
          document[_addEventListener]('DOMContentLoaded', throttledCheckElements);
          setTimeout(onload, 20000);
        }

        if(lazyloadElems.length){
          checkElements();
        } else {
          throttledCheckElements();
        }
      },
      checkElems: throttledCheckElements,
      unveil: unveilElement
    };
  })();


  var autoSizer = (function(){
    var autosizesElems;

    var sizeElement = rAFIt(function(elem, parent, event, width){
      var sources, i, len;
      elem._lazysizesWidth = width;
      width += 'px';

      elem.setAttribute('sizes', width);

      if(regPicture.test(parent.nodeName || '')){
        sources = parent.getElementsByTagName('source');
        for(i = 0, len = sources.length; i < len; i++){
          sources[i].setAttribute('sizes', width);
        }
      }

      if(!event.detail.dataAttr){
        updatePolyfill(elem, event.detail);
      }
    });
    var getSizeElement = function (elem, dataAttr, width){
      var event;
      var parent = elem.parentNode;

      if(parent){
        width = getWidth(elem, parent, width);
        event = triggerEvent(elem, 'lazybeforesizes', {width: width, dataAttr: !!dataAttr});

        if(!event.defaultPrevented){
          width = event.detail.width;

          if(width && width !== elem._lazysizesWidth){
            sizeElement(elem, parent, event, width);
          }
        }
      }
    };

    var updateElementsSizes = function(){
      var i;
      var len = autosizesElems.length;
      if(len){
        i = 0;

        for(; i < len; i++){
          getSizeElement(autosizesElems[i]);
        }
      }
    };

    var debouncedUpdateElementsSizes = debounce(updateElementsSizes);

    return {
      _: function(){
        autosizesElems = document.getElementsByClassName(lazySizesConfig.autosizesClass);
        addEventListener('resize', debouncedUpdateElementsSizes);
      },
      checkElems: debouncedUpdateElementsSizes,
      updateElem: getSizeElement
    };
  })();

  var init = function(){
    if(!init.i){
      init.i = true;
      autoSizer._();
      loader._();
    }
  };

  (function(){
    var prop;

    var lazySizesDefaults = {
      lazyClass: 'lazyload',
      loadedClass: 'lazyloaded',
      loadingClass: 'lazyloading',
      preloadClass: 'lazypreload',
      errorClass: 'lazyerror',
      //strictClass: 'lazystrict',
      autosizesClass: 'lazyautosizes',
      srcAttr: 'data-src',
      srcsetAttr: 'data-srcset',
      sizesAttr: 'data-sizes',
      //preloadAfterLoad: false,
      minSize: 40,
      customMedia: {},
      init: true,
      expFactor: 1.5,
      hFac: 0.8,
      loadMode: 2
    };

    lazySizesConfig = window.lazySizesConfig || window.lazysizesConfig || {};

    for(prop in lazySizesDefaults){
      if(!(prop in lazySizesConfig)){
        lazySizesConfig[prop] = lazySizesDefaults[prop];
      }
    }

    window.lazySizesConfig = lazySizesConfig;

    setTimeout(function(){
      if(lazySizesConfig.init){
        init();
      }
    });
  })();

  return {
    cfg: lazySizesConfig,
    autoSizer: autoSizer,
    loader: loader,
    init: init,
    uP: updatePolyfill,
    aC: addClass,
    rC: removeClass,
    hC: hasClass,
    fire: triggerEvent,
    gW: getWidth,
    rAF: rAF,
  };
}
));

},{}],17:[function(require,module,exports){
(function(){
  'use strict';
  if(!window.addEventListener){return;}

  var regWhite = /\s+/g;
  var regSplitSet = /\s*\|\s+|\s+\|\s*/g;
  var regSource = /^(.+?)(?:\s+\[\s*(.+?)\s*\])?$/;
  var regBgUrlEscape = /\(|\)|'/;
  var allowedBackgroundSize = {contain: 1, cover: 1};
  var proxyWidth = function(elem){
    var width = lazySizes.gW(elem, elem.parentNode);

    if(!elem._lazysizesWidth || width > elem._lazysizesWidth){
      elem._lazysizesWidth = width;
    }
    return elem._lazysizesWidth;
  };
  var getBgSize = function(elem){
    var bgSize;

    bgSize = (getComputedStyle(elem) || {getPropertyValue: function(){}}).getPropertyValue('background-size');

    if(!allowedBackgroundSize[bgSize] && allowedBackgroundSize[elem.style.backgroundSize]){
      bgSize = elem.style.backgroundSize;
    }

    return bgSize;
  };
  var createPicture = function(sets, elem, img){
    var picture = document.createElement('picture');
    var sizes = elem.getAttribute(lazySizesConfig.sizesAttr);
    var ratio = elem.getAttribute('data-ratio');
    var optimumx = elem.getAttribute('data-optimumx');

    if(elem._lazybgset && elem._lazybgset.parentNode == elem){
      elem.removeChild(elem._lazybgset);
    }

    Object.defineProperty(img, '_lazybgset', {
      value: elem,
      writable: true
    });
    Object.defineProperty(elem, '_lazybgset', {
      value: picture,
      writable: true
    });

    sets = sets.replace(regWhite, ' ').split(regSplitSet);

    picture.style.display = 'none';
    img.className = lazySizesConfig.lazyClass;

    if(sets.length == 1 && !sizes){
      sizes = 'auto';
    }

    sets.forEach(function(set){
      var source = document.createElement('source');

      if(sizes && sizes != 'auto'){
        source.setAttribute('sizes', sizes);
      }

      if(set.match(regSource)){
        source.setAttribute(lazySizesConfig.srcsetAttr, RegExp.$1);
        if(RegExp.$2){
          source.setAttribute('media', lazySizesConfig.customMedia[RegExp.$2] || RegExp.$2);
        }
      }
      picture.appendChild(source);
    });

    if(sizes){
      img.setAttribute(lazySizesConfig.sizesAttr, sizes);
      elem.removeAttribute(lazySizesConfig.sizesAttr);
      elem.removeAttribute('sizes');
    }
    if(optimumx){
      img.setAttribute('data-optimumx', optimumx);
    }
    if(ratio) {
      img.setAttribute('data-ratio', ratio);
    }

    picture.appendChild(img);

    elem.appendChild(picture);
  };

  var proxyLoad = function(e){
    if(!e.target._lazybgset){return;}

    var image = e.target;
    var elem = image._lazybgset;
    var bg = image.currentSrc || image.src;

    if(bg){
      elem.style.backgroundImage = 'url(' + (regBgUrlEscape.test(bg) ? JSON.stringify(bg) : bg ) + ')';
    }

    if(image._lazybgsetLoading){
      lazySizes.fire(elem, '_lazyloaded', {}, false, true);
      delete image._lazybgsetLoading;
    }
  };

  addEventListener('lazybeforeunveil', function(e){
    var set, image, elem;

    if(e.defaultPrevented || !(set = e.target.getAttribute('data-bgset'))){return;}

    elem = e.target;
    image = document.createElement('img');

    image.alt = '';

    image._lazybgsetLoading = true;
    e.detail.firesLoad = true;

    createPicture(set, elem, image);

    setTimeout(function(){
      lazySizes.loader.unveil(image);

      lazySizes.rAF(function(){
        lazySizes.fire(image, '_lazyloaded', {}, true, true);
        if(image.complete) {
          proxyLoad({target: image});
        }
      });
    });

  });

  document.addEventListener('load', proxyLoad, true);

  window.addEventListener('lazybeforesizes', function(e){
    if(e.target._lazybgset && e.detail.dataAttr){
      var elem = e.target._lazybgset;
      var bgSize = getBgSize(elem);

      if(allowedBackgroundSize[bgSize]){
        e.target._lazysizesParentFit = bgSize;

        lazySizes.rAF(function(){
          e.target.setAttribute('data-parent-fit', bgSize);
          if(e.target._lazysizesParentFit){
            delete e.target._lazysizesParentFit;
          }
        });
      }
    }
  }, true);

  document.documentElement.addEventListener('lazybeforesizes', function(e){
    if(e.defaultPrevented || !e.target._lazybgset){return;}
    e.detail.width = proxyWidth(e.target._lazybgset);
  });
})();

},{}],18:[function(require,module,exports){
(function(window, document){
  'use strict';

  if(!window.addEventListener){return;}

  var regDescriptors = /\s+(\d+)(w|h)\s+(\d+)(w|h)/;
  var regCssFit = /parent-fit["']*\s*:\s*["']*(contain|cover|width)/;
  var regCssObject = /parent-container["']*\s*:\s*["']*(.+?)(?=(\s|$|,|'|"|;))/;
  var regPicture = /^picture$/i;

  var getCSS = function (elem){
    return (getComputedStyle(elem, null) || {});
  };

  var parentFit = {

    getParent: function(element, parentSel){
      var parent = element;
      var parentNode = element.parentNode;

      if((!parentSel || parentSel == 'prev') && parentNode && regPicture.test(parentNode.nodeName || '')){
        parentNode = parentNode.parentNode;
      }

      if(parentSel != 'self'){
        if(parentSel == 'prev'){
          parent = element.previousElementSibling;
        } else if(parentSel && (parentNode.closest || window.jQuery)){
          parent = (parentNode.closest ?
              parentNode.closest(parentSel) :
              jQuery(parentNode).closest(parentSel)[0]) ||
            parentNode
          ;
        } else {
          parent = parentNode;
        }
      }

      return parent;
    },

    getFit: function(element){
      var tmpMatch, parentObj;
      var css = getCSS(element);
      var content = css.content || css.fontFamily;
      var obj = {
        fit: element._lazysizesParentFit || element.getAttribute('data-parent-fit')
      };

      if(!obj.fit && content && (tmpMatch = content.match(regCssFit))){
        obj.fit = tmpMatch[1];
      }

      if(obj.fit){
        parentObj = element._lazysizesParentContainer || element.getAttribute('data-parent-container');

        if(!parentObj && content && (tmpMatch = content.match(regCssObject))){
          parentObj = tmpMatch[1];
        }

        obj.parent = parentFit.getParent(element, parentObj);


      } else {
        obj.fit = css.objectFit;
      }

      return obj;
    },

    getImageRatio: function(element){
      var i, srcset, media, ratio;
      var parent = element.parentNode;
      var elements = parent && regPicture.test(parent.nodeName || '') ?
          parent.querySelectorAll('source, img') :
          [element]
        ;

      for(i = 0; i < elements.length; i++){
        element = elements[i];
        srcset = element.getAttribute(lazySizesConfig.srcsetAttr) || element.getAttribute('srcset') || element.getAttribute('data-pfsrcset') || element.getAttribute('data-risrcset') || '';
        media = element.getAttribute('media');
        media = lazySizesConfig.customMedia[element.getAttribute('data-media') || media] || media;

        if(srcset && (!media || (window.matchMedia && matchMedia(media) || {}).matches )){
          ratio = parseFloat(element.getAttribute('data-aspectratio'));

          if(!ratio && srcset.match(regDescriptors)){
            if(RegExp.$2 == 'w'){
              ratio = RegExp.$1 / RegExp.$3;
            } else {
              ratio = RegExp.$3 / RegExp.$1;
            }
          }
          break;
        }
      }

      return ratio;
    },

    calculateSize: function(element, width){
      var displayRatio, height, imageRatio, retWidth;
      var fitObj = this.getFit(element);
      var fit = fitObj.fit;
      var fitElem = fitObj.parent;

      if(fit != 'width' && ((fit != 'contain' && fit != 'cover') || !(imageRatio = this.getImageRatio(element)))){
        return width;
      }

      if(fitElem){
        width = fitElem.clientWidth;
      } else {
        fitElem = element;
      }

      retWidth = width;

      if(fit == 'width'){
        retWidth = width;
      } else {
        height = fitElem.clientHeight;

        if(height > 40 && (displayRatio =  width / height) && ((fit == 'cover' && displayRatio < imageRatio) || (fit == 'contain' && displayRatio > imageRatio))){
          retWidth = width * (imageRatio / displayRatio);
        }
      }

      return retWidth;
    }
  };

  var extend = function(){
    if(window.lazySizes){
      if(!lazySizes.parentFit){
        lazySizes.parentFit = parentFit;
      }
      window.removeEventListener('lazyunveilread', extend, true);
    }
  };

  window.addEventListener('lazyunveilread', extend, true);

  document.addEventListener('lazybeforesizes', function(e){
    if(e.defaultPrevented){return;}
    var element = e.target;
    e.detail.width = parentFit.calculateSize(element, e.detail.width);
  });

  setTimeout(extend);

})(window, document);

},{}],19:[function(require,module,exports){
(function(window, document, undefined){
  /*jshint eqnull:true */
  'use strict';
  var polyfill;
  var config = (window.lazySizes && lazySizes.cfg) || window.lazySizesConfig;
  var img = document.createElement('img');
  var supportSrcset = ('sizes' in img) && ('srcset' in img);
  var regHDesc = /\s+\d+h/g;
  var fixEdgeHDescriptor = (function(){
    var regDescriptors = /\s+(\d+)(w|h)\s+(\d+)(w|h)/;
    var forEach = Array.prototype.forEach;

    return function(edgeMatch){
      var img = document.createElement('img');
      var removeHDescriptors = function(source){
        var ratio;
        var srcset = source.getAttribute(lazySizesConfig.srcsetAttr);
        if(srcset){
          if(srcset.match(regDescriptors)){
            if(RegExp.$2 == 'w'){
              ratio = RegExp.$1 / RegExp.$3;
            } else {
              ratio = RegExp.$3 / RegExp.$1;
            }

            if(ratio){
              source.setAttribute('data-aspectratio', ratio);
            }
          }
          source.setAttribute(lazySizesConfig.srcsetAttr, srcset.replace(regHDesc, ''));
        }
      };
      var handler = function(e){
        var picture = e.target.parentNode;

        if(picture && picture.nodeName == 'PICTURE'){
          forEach.call(picture.getElementsByTagName('source'), removeHDescriptors);
        }
        removeHDescriptors(e.target);
      };

      var test = function(){
        if(!!img.currentSrc){
          document.removeEventListener('lazybeforeunveil', handler);
        }
      };

      if(edgeMatch[1]){
        document.addEventListener('lazybeforeunveil', handler);

        if(true || edgeMatch[1] > 14){
          img.onload = test;
          img.onerror = test;

          img.srcset = 'data:,a 1w 1h';

          if(img.complete){
            test();
          }
        }
      }
    };
  })();


  if(!config){
    config = {};
    window.lazySizesConfig = config;
  }

  if(!config.supportsType){
    config.supportsType = function(type/*, elem*/){
      return !type;
    };
  }

  if(window.picturefill || config.pf){return;}

  if(window.HTMLPictureElement && supportSrcset){

    if(document.msElementsFromPoint){
      fixEdgeHDescriptor(navigator.userAgent.match(/Edge\/(\d+)/));
    }

    config.pf = function(){};
    return;
  }

  config.pf = function(options){
    var i, len;
    if(window.picturefill){return;}
    for(i = 0, len = options.elements.length; i < len; i++){
      polyfill(options.elements[i]);
    }
  };

  // partial polyfill
  polyfill = (function(){
    var ascendingSort = function( a, b ) {
      return a.w - b.w;
    };
    var regPxLength = /^\s*\d+px\s*$/;
    var reduceCandidate = function (srces) {
      var lowerCandidate, bonusFactor;
      var len = srces.length;
      var candidate = srces[len -1];
      var i = 0;

      for(i; i < len;i++){
        candidate = srces[i];
        candidate.d = candidate.w / srces.w;

        if(candidate.d >= srces.d){
          if(!candidate.cached && (lowerCandidate = srces[i - 1]) &&
            lowerCandidate.d > srces.d - (0.13 * Math.pow(srces.d, 2.2))){

            bonusFactor = Math.pow(lowerCandidate.d - 0.6, 1.6);

            if(lowerCandidate.cached) {
              lowerCandidate.d += 0.15 * bonusFactor;
            }

            if(lowerCandidate.d + ((candidate.d - srces.d) * bonusFactor) > srces.d){
              candidate = lowerCandidate;
            }
          }
          break;
        }
      }
      return candidate;
    };

    var parseWsrcset = (function(){
      var candidates;
      var regWCandidates = /(([^,\s].[^\s]+)\s+(\d+)w)/g;
      var regMultiple = /\s/;
      var addCandidate = function(match, candidate, url, wDescriptor){
        candidates.push({
          c: candidate,
          u: url,
          w: wDescriptor * 1
        });
      };

      return function(input){
        candidates = [];
        input = input.trim();
        input
          .replace(regHDesc, '')
          .replace(regWCandidates, addCandidate)
        ;

        if(!candidates.length && input && !regMultiple.test(input)){
          candidates.push({
            c: input,
            u: input,
            w: 99
          });
        }

        return candidates;
      };
    })();

    var runMatchMedia = function(){
      if(runMatchMedia.init){return;}

      runMatchMedia.init = true;
      addEventListener('resize', (function(){
        var timer;
        var matchMediaElems = document.getElementsByClassName('lazymatchmedia');
        var run = function(){
          var i, len;
          for(i = 0, len = matchMediaElems.length; i < len; i++){
            polyfill(matchMediaElems[i]);
          }
        };

        return function(){
          clearTimeout(timer);
          timer = setTimeout(run, 66);
        };
      })());
    };

    var createSrcset = function(elem, isImage){
      var parsedSet;
      var srcSet = elem.getAttribute('srcset') || elem.getAttribute(config.srcsetAttr);

      if(!srcSet && isImage){
        srcSet = !elem._lazypolyfill ?
          (elem.getAttribute(config.srcAttr) || elem.getAttribute('src')) :
          elem._lazypolyfill._set
        ;
      }

      if(!elem._lazypolyfill || elem._lazypolyfill._set != srcSet){

        parsedSet = parseWsrcset( srcSet || '' );
        if(isImage && elem.parentNode){
          parsedSet.isPicture = elem.parentNode.nodeName.toUpperCase() == 'PICTURE';

          if(parsedSet.isPicture){
            if(window.matchMedia){
              lazySizes.aC(elem, 'lazymatchmedia');
              runMatchMedia();
            }
          }
        }

        parsedSet._set = srcSet;
        Object.defineProperty(elem, '_lazypolyfill', {
          value: parsedSet,
          writable: true
        });
      }
    };

    var getX = function(elem){
      var dpr = window.devicePixelRatio || 1;
      var optimum = lazySizes.getX && lazySizes.getX(elem);
      return Math.min(optimum || dpr, 2.5, dpr);
    };

    var matchesMedia = function(media){
      if(window.matchMedia){
        matchesMedia = function(media){
          return !media || (matchMedia(media) || {}).matches;
        };
      } else {
        return !media;
      }

      return matchesMedia(media);
    };

    var getCandidate = function(elem){
      var sources, i, len, media, source, srces, src, width;

      source = elem;
      createSrcset(source, true);
      srces = source._lazypolyfill;

      if(srces.isPicture){
        for(i = 0, sources = elem.parentNode.getElementsByTagName('source'), len = sources.length; i < len; i++){
          if( config.supportsType(sources[i].getAttribute('type'), elem) && matchesMedia( sources[i].getAttribute('media')) ){
            source = sources[i];
            createSrcset(source);
            srces = source._lazypolyfill;
            break;
          }
        }
      }

      if(srces.length > 1){
        width = source.getAttribute('sizes') || '';
        width = regPxLength.test(width) && parseInt(width, 10) || lazySizes.gW(elem, elem.parentNode);
        srces.d = getX(elem);
        if(!srces.src || !srces.w || srces.w < width){
          srces.w = width;
          src = reduceCandidate(srces.sort(ascendingSort));
          srces.src = src;
        } else {
          src = srces.src;
        }
      } else {
        src = srces[0];
      }

      return src;
    };

    var p = function(elem){
      if(supportSrcset && elem.parentNode && elem.parentNode.nodeName.toUpperCase() != 'PICTURE'){return;}
      var candidate = getCandidate(elem);

      if(candidate && candidate.u && elem._lazypolyfill.cur != candidate.u){
        elem._lazypolyfill.cur = candidate.u;
        candidate.cached = true;
        elem.setAttribute(config.srcAttr, candidate.u);
        elem.setAttribute('src', candidate.u);
      }
    };

    p.parse = parseWsrcset;

    return p;
  })();

  if(config.loadedClass && config.loadingClass){
    (function(){
      var sels = [];
      ['img[sizes$="px"][srcset].', 'picture > img:not([srcset]).'].forEach(function(sel){
        sels.push(sel + config.loadedClass);
        sels.push(sel + config.loadingClass);
      });
      config.pf({
        elements: document.querySelectorAll(sels.join(', '))
      });
    })();

  }
})(window, document);

/**
 * Some versions of iOS (8.1-) do load the first candidate of a srcset candidate list, if width descriptors with the sizes attribute is used.
 * This tiny extension prevents this wasted download by creating a picture structure around the image.
 * Note: This extension is already included in the ls.respimg.js file.
 *
 * Usage:
 *
 * <img
 *  class="lazyload"
 *  data-sizes="auto"
 *  data-srcset="small.jpg 640px,
 *    medium.jpg 980w,
 *    large.jpg 1280w"
 *  />
 */

(function(document){
  'use strict';
  var regPicture;
  var img = document.createElement('img');

  if(('srcset' in img) && !('sizes' in img) && !window.HTMLPictureElement){
    regPicture = /^picture$/i;
    document.addEventListener('lazybeforeunveil', function(e){
      var elem, parent, srcset, sizes, isPicture;
      var picture, source;
      if(e.defaultPrevented ||
        lazySizesConfig.noIOSFix ||
        !(elem = e.target) ||
        !(srcset = elem.getAttribute(lazySizesConfig.srcsetAttr)) ||
        !(parent = elem.parentNode) ||
        (
          !(isPicture = regPicture.test(parent.nodeName || '')) &&
          !(sizes = elem.getAttribute('sizes') || elem.getAttribute(lazySizesConfig.sizesAttr))
        )
      ){return;}

      picture = isPicture ? parent : document.createElement('picture');

      if(!elem._lazyImgSrc){
        Object.defineProperty(elem, '_lazyImgSrc', {
          value: document.createElement('source'),
          writable: true
        });
      }
      source = elem._lazyImgSrc;

      if(sizes){
        source.setAttribute('sizes', sizes);
      }

      source.setAttribute(lazySizesConfig.srcsetAttr, srcset);
      elem.setAttribute('data-pfsrcset', srcset);
      elem.removeAttribute(lazySizesConfig.srcsetAttr);

      if(!isPicture){
        parent.insertBefore(picture, elem);
        picture.appendChild(elem);
      }
      picture.insertBefore(source, elem);
    });
  }
})(document);

},{}],20:[function(require,module,exports){
var root = require('./_root');

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;

},{"./_root":32}],21:[function(require,module,exports){
/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;

},{}],22:[function(require,module,exports){
/**
 * Converts an ASCII `string` to an array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the converted array.
 */
function asciiToArray(string) {
  return string.split('');
}

module.exports = asciiToArray;

},{}],23:[function(require,module,exports){
var Symbol = require('./_Symbol'),
    getRawTag = require('./_getRawTag'),
    objectToString = require('./_objectToString');

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  value = Object(value);
  return (symToStringTag && symToStringTag in value)
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;

},{"./_Symbol":20,"./_getRawTag":29,"./_objectToString":31}],24:[function(require,module,exports){
/**
 * The base implementation of `_.slice` without an iteratee call guard.
 *
 * @private
 * @param {Array} array The array to slice.
 * @param {number} [start=0] The start position.
 * @param {number} [end=array.length] The end position.
 * @returns {Array} Returns the slice of `array`.
 */
function baseSlice(array, start, end) {
  var index = -1,
      length = array.length;

  if (start < 0) {
    start = -start > length ? 0 : (length + start);
  }
  end = end > length ? length : end;
  if (end < 0) {
    end += length;
  }
  length = start > end ? 0 : ((end - start) >>> 0);
  start >>>= 0;

  var result = Array(length);
  while (++index < length) {
    result[index] = array[index + start];
  }
  return result;
}

module.exports = baseSlice;

},{}],25:[function(require,module,exports){
var Symbol = require('./_Symbol'),
    arrayMap = require('./_arrayMap'),
    isArray = require('./isArray'),
    isSymbol = require('./isSymbol');

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = baseToString;

},{"./_Symbol":20,"./_arrayMap":21,"./isArray":37,"./isSymbol":40}],26:[function(require,module,exports){
var baseSlice = require('./_baseSlice');

/**
 * Casts `array` to a slice if it's needed.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {number} start The start position.
 * @param {number} [end=array.length] The end position.
 * @returns {Array} Returns the cast slice.
 */
function castSlice(array, start, end) {
  var length = array.length;
  end = end === undefined ? length : end;
  return (!start && end >= length) ? array : baseSlice(array, start, end);
}

module.exports = castSlice;

},{"./_baseSlice":24}],27:[function(require,module,exports){
var castSlice = require('./_castSlice'),
    hasUnicode = require('./_hasUnicode'),
    stringToArray = require('./_stringToArray'),
    toString = require('./toString');

/**
 * Creates a function like `_.lowerFirst`.
 *
 * @private
 * @param {string} methodName The name of the `String` case method to use.
 * @returns {Function} Returns the new case function.
 */
function createCaseFirst(methodName) {
  return function(string) {
    string = toString(string);

    var strSymbols = hasUnicode(string)
      ? stringToArray(string)
      : undefined;

    var chr = strSymbols
      ? strSymbols[0]
      : string.charAt(0);

    var trailing = strSymbols
      ? castSlice(strSymbols, 1).join('')
      : string.slice(1);

    return chr[methodName]() + trailing;
  };
}

module.exports = createCaseFirst;

},{"./_castSlice":26,"./_hasUnicode":30,"./_stringToArray":33,"./toString":44}],28:[function(require,module,exports){
(function (global){
/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],29:[function(require,module,exports){
var Symbol = require('./_Symbol');

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;

},{"./_Symbol":20}],30:[function(require,module,exports){
/** Used to compose unicode character classes. */
var rsAstralRange = '\\ud800-\\udfff',
    rsComboMarksRange = '\\u0300-\\u036f',
    reComboHalfMarksRange = '\\ufe20-\\ufe2f',
    rsComboSymbolsRange = '\\u20d0-\\u20ff',
    rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
    rsVarRange = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsZWJ = '\\u200d';

/** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

/**
 * Checks if `string` contains Unicode symbols.
 *
 * @private
 * @param {string} string The string to inspect.
 * @returns {boolean} Returns `true` if a symbol is found, else `false`.
 */
function hasUnicode(string) {
  return reHasUnicode.test(string);
}

module.exports = hasUnicode;

},{}],31:[function(require,module,exports){
/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;

},{}],32:[function(require,module,exports){
var freeGlobal = require('./_freeGlobal');

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;

},{"./_freeGlobal":28}],33:[function(require,module,exports){
var asciiToArray = require('./_asciiToArray'),
    hasUnicode = require('./_hasUnicode'),
    unicodeToArray = require('./_unicodeToArray');

/**
 * Converts `string` to an array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the converted array.
 */
function stringToArray(string) {
  return hasUnicode(string)
    ? unicodeToArray(string)
    : asciiToArray(string);
}

module.exports = stringToArray;

},{"./_asciiToArray":22,"./_hasUnicode":30,"./_unicodeToArray":34}],34:[function(require,module,exports){
/** Used to compose unicode character classes. */
var rsAstralRange = '\\ud800-\\udfff',
    rsComboMarksRange = '\\u0300-\\u036f',
    reComboHalfMarksRange = '\\ufe20-\\ufe2f',
    rsComboSymbolsRange = '\\u20d0-\\u20ff',
    rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
    rsVarRange = '\\ufe0e\\ufe0f';

/** Used to compose unicode capture groups. */
var rsAstral = '[' + rsAstralRange + ']',
    rsCombo = '[' + rsComboRange + ']',
    rsFitz = '\\ud83c[\\udffb-\\udfff]',
    rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
    rsNonAstral = '[^' + rsAstralRange + ']',
    rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
    rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
    rsZWJ = '\\u200d';

/** Used to compose unicode regexes. */
var reOptMod = rsModifier + '?',
    rsOptVar = '[' + rsVarRange + ']?',
    rsOptJoin = '(?:' + rsZWJ + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
    rsSeq = rsOptVar + reOptMod + rsOptJoin,
    rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

/** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

/**
 * Converts a Unicode `string` to an array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the converted array.
 */
function unicodeToArray(string) {
  return string.match(reUnicode) || [];
}

module.exports = unicodeToArray;

},{}],35:[function(require,module,exports){
var toString = require('./toString'),
    upperFirst = require('./upperFirst');

/**
 * Converts the first character of `string` to upper case and the remaining
 * to lower case.
 *
 * @static
 * @memberOf _
 * @since 3.0.0
 * @category String
 * @param {string} [string=''] The string to capitalize.
 * @returns {string} Returns the capitalized string.
 * @example
 *
 * _.capitalize('FRED');
 * // => 'Fred'
 */
function capitalize(string) {
  return upperFirst(toString(string).toLowerCase());
}

module.exports = capitalize;

},{"./toString":44,"./upperFirst":45}],36:[function(require,module,exports){
var isObject = require('./isObject'),
    now = require('./now'),
    toNumber = require('./toNumber');

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        result = wait - timeSinceLastCall;

    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

module.exports = debounce;

},{"./isObject":38,"./now":41,"./toNumber":43}],37:[function(require,module,exports){
/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

module.exports = isArray;

},{}],38:[function(require,module,exports){
/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;

},{}],39:[function(require,module,exports){
/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;

},{}],40:[function(require,module,exports){
var baseGetTag = require('./_baseGetTag'),
    isObjectLike = require('./isObjectLike');

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;

},{"./_baseGetTag":23,"./isObjectLike":39}],41:[function(require,module,exports){
var root = require('./_root');

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

module.exports = now;

},{"./_root":32}],42:[function(require,module,exports){
var debounce = require('./debounce'),
    isObject = require('./isObject');

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a throttled function that only invokes `func` at most once per
 * every `wait` milliseconds. The throttled function comes with a `cancel`
 * method to cancel delayed `func` invocations and a `flush` method to
 * immediately invoke them. Provide `options` to indicate whether `func`
 * should be invoked on the leading and/or trailing edge of the `wait`
 * timeout. The `func` is invoked with the last arguments provided to the
 * throttled function. Subsequent calls to the throttled function return the
 * result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the throttled function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.throttle` and `_.debounce`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to throttle.
 * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=true]
 *  Specify invoking on the leading edge of the timeout.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new throttled function.
 * @example
 *
 * // Avoid excessively updating the position while scrolling.
 * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
 *
 * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
 * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
 * jQuery(element).on('click', throttled);
 *
 * // Cancel the trailing throttled invocation.
 * jQuery(window).on('popstate', throttled.cancel);
 */
function throttle(func, wait, options) {
  var leading = true,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  if (isObject(options)) {
    leading = 'leading' in options ? !!options.leading : leading;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }
  return debounce(func, wait, {
    'leading': leading,
    'maxWait': wait,
    'trailing': trailing
  });
}

module.exports = throttle;

},{"./debounce":36,"./isObject":38}],43:[function(require,module,exports){
var isObject = require('./isObject'),
    isSymbol = require('./isSymbol');

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;

},{"./isObject":38,"./isSymbol":40}],44:[function(require,module,exports){
var baseToString = require('./_baseToString');

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

module.exports = toString;

},{"./_baseToString":25}],45:[function(require,module,exports){
var createCaseFirst = require('./_createCaseFirst');

/**
 * Converts the first character of `string` to upper case.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category String
 * @param {string} [string=''] The string to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.upperFirst('fred');
 * // => 'Fred'
 *
 * _.upperFirst('FRED');
 * // => 'FRED'
 */
var upperFirst = createCaseFirst('toUpperCase');

module.exports = upperFirst;

},{"./_createCaseFirst":27}],46:[function(require,module,exports){
!function(root, factory) {
    "function" == typeof define && define.amd ? // AMD. Register as an anonymous module unless amdModuleId is set
    define([], function() {
        return root.svg4everybody = factory();
    }) : "object" == typeof exports ? // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory() : root.svg4everybody = factory();
}(this, function() {
    /*! svg4everybody v2.1.0 | github.com/jonathantneal/svg4everybody */
    function embed(svg, target) {
        // if the target exists
        if (target) {
            // create a document fragment to hold the contents of the target
            var fragment = document.createDocumentFragment(), viewBox = !svg.getAttribute("viewBox") && target.getAttribute("viewBox");
            // conditionally set the viewBox on the svg
            viewBox && svg.setAttribute("viewBox", viewBox);
            // copy the contents of the clone into the fragment
            for (// clone the target
            var clone = target.cloneNode(!0); clone.childNodes.length; ) {
                fragment.appendChild(clone.firstChild);
            }
            // append the fragment into the svg
            svg.appendChild(fragment);
        }
    }
    function loadreadystatechange(xhr) {
        // listen to changes in the request
        xhr.onreadystatechange = function() {
            // if the request is ready
            if (4 === xhr.readyState) {
                // get the cached html document
                var cachedDocument = xhr._cachedDocument;
                // ensure the cached html document based on the xhr response
                cachedDocument || (cachedDocument = xhr._cachedDocument = document.implementation.createHTMLDocument(""), 
                cachedDocument.body.innerHTML = xhr.responseText, xhr._cachedTarget = {}), // clear the xhr embeds list and embed each item
                xhr._embeds.splice(0).map(function(item) {
                    // get the cached target
                    var target = xhr._cachedTarget[item.id];
                    // ensure the cached target
                    target || (target = xhr._cachedTarget[item.id] = cachedDocument.getElementById(item.id)), 
                    // embed the target into the svg
                    embed(item.svg, target);
                });
            }
        }, // test the ready state change immediately
        xhr.onreadystatechange();
    }
    function svg4everybody(rawopts) {
        function oninterval() {
            // while the index exists in the live <use> collection
            for (// get the cached <use> index
            var index = 0; index < uses.length; ) {
                // get the current <use>
                var use = uses[index], svg = use.parentNode;
                if (svg && /svg/i.test(svg.nodeName)) {
                    var src = use.getAttribute("xlink:href") || use.getAttribute("href");
                    if (polyfill && (!opts.validate || opts.validate(src, svg, use))) {
                        // remove the <use> element
                        svg.removeChild(use);
                        // parse the src and get the url and id
                        var srcSplit = src.split("#"), url = srcSplit.shift(), id = srcSplit.join("#");
                        // if the link is external
                        if (url.length) {
                            // get the cached xhr request
                            var xhr = requests[url];
                            // ensure the xhr request exists
                            xhr || (xhr = requests[url] = new XMLHttpRequest(), xhr.open("GET", url), xhr.send(), 
                            xhr._embeds = []), // add the svg and id as an item to the xhr embeds list
                            xhr._embeds.push({
                                svg: svg,
                                id: id
                            }), // prepare the xhr ready state change event
                            loadreadystatechange(xhr);
                        } else {
                            // embed the local id into the svg
                            embed(svg, document.getElementById(id));
                        }
                    }
                } else {
                    // increase the index when the previous value was not "valid"
                    ++index;
                }
            }
            // continue the interval
            requestAnimationFrame(oninterval, 67);
        }
        var polyfill, opts = Object(rawopts), newerIEUA = /\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/, webkitUA = /\bAppleWebKit\/(\d+)\b/, olderEdgeUA = /\bEdge\/12\.(\d+)\b/;
        polyfill = "polyfill" in opts ? opts.polyfill : newerIEUA.test(navigator.userAgent) || (navigator.userAgent.match(olderEdgeUA) || [])[1] < 10547 || (navigator.userAgent.match(webkitUA) || [])[1] < 537;
        // create xhr requests object
        var requests = {}, requestAnimationFrame = window.requestAnimationFrame || setTimeout, uses = document.getElementsByTagName("use");
        // conditionally start the interval if the polyfill is active
        polyfill && oninterval();
    }
    return svg4everybody;
});
},{}]},{},[3]);
