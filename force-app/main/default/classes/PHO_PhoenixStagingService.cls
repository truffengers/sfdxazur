/**
* @name: PHO_PhoenixStagingService
* @description: This class contains business logic to derive product Phoenix fields.
* @author: Unknown
* @lastChangedBy: Antigoni D'Mello  08/10/2020  PR-112: Added 'assignDRCode' method to update DR Codes
* @lastChangedBy: Antigoni D'Mello  21/10/2020  PR-112: Added check on Phoenix product code
* @lastChangedBy: Antigoni D'Mello  18/10/2021  PR-113: Changed Carrier DR Code implementation
*/
public with sharing class PHO_PhoenixStagingService {

    public static void assignDRCode(List<PHO_PhoenixStaging__c> newPHOStg) {
        // Map of product combination of Phoenix product code and Phoenix section code to Carrier DR Code
        Map<String, String> productCodesMap = new Map<String, String>();

        List<Product2> allProducts = [SELECT Id, Name, PHO_PhoenixProductCode__c, PHO_PhoenixRiskCode__c, PHO_PhoenixSectionCode__c,
                                            CarrierDRCode__c, vlocity_ins__CarrierId__c
                                        FROM Product2
                                        WHERE IBA_ClassofBusiness__c != 'Azur Emerging Wealth'
                                        AND IBA_ClassofBusiness__c != ''
                                        LIMIT 500];

        for(Product2 product: allProducts) {
            String productCodesComb = product.PHO_PhoenixProductCode__c+product.PHO_PhoenixSectionCode__c;
            if(productCodesMap.get(productCodesComb) == null) {
                productCodesMap.put(product.PHO_PhoenixProductCode__c+product.PHO_PhoenixSectionCode__c, product.CarrierDRCode__c);
            }
        }
        for(PHO_PhoenixStaging__c newPhoenixStaging: newPHOStg) {
            String phoenixCodesComb = newPhoenixStaging.PHO_ProductCode__c + newPhoenixStaging.PHO_SectionCode__c;
            newPhoenixStaging.PHO_CarrierDRCode__c = productCodesMap.get(phoenixCodesComb);
        }
    }

    public static void phoToSFFieldMappingOnInsert(List<SObject> newPHOStgList) {
        phoToSFFieldMapping((List<PHO_PhoenixStaging__c>) newPHOStgList);
    }

    public static void phoToSFFieldMappingOnUpdate(Map<Id, SObject> newPHOStgMap, Map<Id, SObject> oldPHOStgMap) {
        List<PHO_PhoenixStaging__c> newPHOStgList = (List<PHO_PhoenixStaging__c>) newPHOStgMap.values();
        Map<Id, PHO_PhoenixStaging__c> oldPHOStgMapCasted = (Map<Id, PHO_PhoenixStaging__c>) oldPHOStgMap;
        List<PHO_PhoenixStaging__c> updatedPHOStgList = new List<PHO_PhoenixStaging__c>();

        // choose only the records for which relevant fields changed
        for (PHO_PhoenixStaging__c newPHOStgRecord : newPHOStgList) {
            PHO_PhoenixStaging__c oldPHOStgRecord = oldPHOStgMapCasted.get(newPHOStgRecord.Id);
            if (oldPHOStgRecord.PHO_AccountingBroker__c == null
                    || oldPHOStgRecord.PHO_CarrierAccount__c == null
                    || oldPHOStgRecord.PHO_MGAAccount__c == null
                    || oldPHOStgRecord.PHO_ProducingBroker__c == null
                    || oldPHOStgRecord.PHO_Product__c == null) {
                updatedPHOStgList.add(newPHOStgRecord);
            } else if (newPHOStgRecord.PHO_AlternateReference__c != oldPHOStgRecord.PHO_AlternateReference__c) {
                updatedPHOStgList.add(newPHOStgRecord);
            } else if (newPHOStgRecord.PHO_CurrencyType__c != oldPHOStgRecord.PHO_CurrencyType__c) {
                updatedPHOStgList.add(newPHOStgRecord);
            } else if (newPHOStgRecord.PHO_ProductCode__c != oldPHOStgRecord.PHO_ProductCode__c) {
                updatedPHOStgList.add(newPHOStgRecord);
            } else if (newPHOStgRecord.PHO_SectionCode__c != oldPHOStgRecord.PHO_SectionCode__c) {
                updatedPHOStgList.add(newPHOStgRecord);
            } else if (newPHOStgRecord.PHO_PaymentMethod__c != oldPHOStgRecord.PHO_PaymentMethod__c) {
                updatedPHOStgList.add(newPHOStgRecord);
            }
        }

        phoToSFFieldMapping(updatedPHOStgList);
    }

    public static void phoToSFFieldMapping(List<PHO_PhoenixStaging__c> targetPHOStgList) {
        if (!targetPHOStgList.isEmpty()) {
            Map<String, Map<String, Id>> drCodeCurrTAccIdMap = new Map<String, Map<String, Id>>();
            Map<String, Id> prodMap = new Map<String, Id>();
            IBA_IBACustomSettings__c ibaCS = IBA_IBACustomSettings__c.getInstance();
            Set<String> drCodeSet = new Set<String>();
            Set<String> currTypeSet = new Set<String>();
            Set<String> prodCodeSet = new Set<String>();
            Set<String> sectionCodeSet = new Set<String>();

            populateSets(drCodeSet, currTypeSet,prodCodeSet, sectionCodeSet, targetPHOStgList, ibaCS);
            buildDrCodeCurrTAccIdMap(drCodeCurrTAccIdMap, drCodeSet, currTypeSet);
            buildProdMap(prodMap, prodCodeSet, sectionCodeSet);
            assignFieldsVal(targetPHOStgList, drCodeCurrTAccIdMap, ibaCS, prodMap);
        }
    }

    @TestVisible
    private static void populateSets(
            Set<String> drCodeSet, Set<String> currTypeSet,
            Set<String> prodCodeSet, Set<String> sectionCodeSet,
            List<PHO_PhoenixStaging__c> targetPHOStgList, IBA_IBACustomSettings__c ibaCS) {

        if (String.isNotBlank(ibaCS.PHO_PhoenixAccBrokerAccountDRCode__c)) {
            drCodeSet.add(ibaCS.PHO_PhoenixAccBrokerAccountDRCode__c);
        }
        if (String.isNotBlank(ibaCS.PHO_PhoenixMGAAccountDRCode__c)) {
            drCodeSet.add(ibaCS.PHO_PhoenixMGAAccountDRCode__c);
        }

        for (PHO_PhoenixStaging__c phoStg : targetPHOStgList) {
            if (String.isNotBlank(phoStg.PHO_AlternateReference__c)) {
                drCodeSet.add(phoStg.PHO_AlternateReference__c);
            }
            if (String.isNotBlank(phoStg.PHO_CarrierDRCode__c)) {
                drCodeSet.add(phoStg.PHO_CarrierDRCode__c);
            }
            if (String.isNotBlank(phoStg.PHO_CurrencyType__c)) {
                currTypeSet.add(phoStg.PHO_CurrencyType__c);
            }
            if (String.isNotBlank(phoStg.PHO_ProductCode__c)) {
                prodCodeSet.add(phoStg.PHO_ProductCode__c);
            }
            if (String.isNotBlank(phoStg.PHO_SectionCode__c)) {
                sectionCodeSet.add(phoStg.PHO_SectionCode__c);
            }
        }
    }

    @TestVisible
    private static void buildDrCodeCurrTAccIdMap(Map<String, Map<String, Id>> drCodeCurrTAccIdMap, Set<String> drCodeSet, Set<String> currTypeSet){
        List<Account> accList = [
                SELECT Id, EW_DRCode__c, c2g__CODAAccountTradingCurrency__c
                FROM Account
                WHERE EW_DRCode__c IN :drCodeSet
                AND c2g__CODAAccountTradingCurrency__c IN :currTypeSet
        ];

        for (Account acc : accList) {
            if (drCodeCurrTAccIdMap.containsKey(acc.EW_DRCode__c)) {
                drCodeCurrTAccIdMap.get(acc.EW_DRCode__c).put(acc.c2g__CODAAccountTradingCurrency__c, acc.Id);
            } else {
                drCodeCurrTAccIdMap.put(
                        acc.EW_DRCode__c,
                        new Map<String, Id>{
                                acc.c2g__CODAAccountTradingCurrency__c => acc.Id
                        }
                );
            }
        }
    }

    @TestVisible
    private static void buildProdMap(Map<String, Id> prodMap, Set<String> prodCodeSet, Set<String> sectionCodeSet){
        List<Product2> prodList = [
                SELECT Id, PHO_PhoenixProductCode__c, PHO_PhoenixSectionCode__c
                FROM Product2
                WHERE PHO_PhoenixProductCode__c in : prodCodeSet AND PHO_PhoenixSectionCode__c IN :sectionCodeSet
        ];

        for(Product2 prod : prodList){
            String prodKey = prod.PHO_PhoenixProductCode__c + prod.PHO_PhoenixSectionCode__c;
            prodMap.put(prodKey, prod.Id);
        }
    }

    @TestVisible
    private static void assignFieldsVal(
            List<PHO_PhoenixStaging__c> targetPHOStgList, Map<String, Map<String, Id>> drCodeCurrTAccIdMap,
            IBA_IBACustomSettings__c ibaCS, Map<String, Id> prodMap) {

        Map<String, Id> brokerAccountIdsByCurrencyMap = drCodeCurrTAccIdMap.get(ibaCS.PHO_PhoenixAccBrokerAccountDRCode__c);
        Map<String, Id> carrierAccountIdsByCurrencyMap = new Map<String, Id>();
        Map<String, Id> mgaAccountIdsByCurrencyMap = drCodeCurrTAccIdMap.get(ibaCS.PHO_PhoenixMGAAccountDRCode__c);

        for (PHO_PhoenixStaging__c phoStg : targetPHOStgList) {
            String productKey = phoStg.PHO_ProductCode__c + phoStg.PHO_SectionCode__c;
            carrierAccountIdsByCurrencyMap = drCodeCurrTAccIdMap.get(phoStg.PHO_CarrierDRCode__c);
            try {
                Id accBroker, carrAcc, mgaAcc, prodBroker;
                if (brokerAccountIdsByCurrencyMap != null) {
                    accBroker = brokerAccountIdsByCurrencyMap.get(phoStg.PHO_CurrencyType__c);
                }
                if (carrierAccountIdsByCurrencyMap != null) {
                    carrAcc = carrierAccountIdsByCurrencyMap.get(phoStg.PHO_CurrencyType__c);
                }
                if (mgaAccountIdsByCurrencyMap != null) {
                    mgaAcc = mgaAccountIdsByCurrencyMap.get(phoStg.PHO_CurrencyType__c);
                }
                if (drCodeCurrTAccIdMap.containsKey(phoStg.PHO_AlternateReference__c)) {
                    prodBroker = drCodeCurrTAccIdMap.get(phoStg.PHO_AlternateReference__c)
                            .get(phoStg.PHO_CurrencyType__c);
                }

                phoStg.PHO_AccountingBroker__c = phoStg.PHO_PaymentMethod__c == PHO_Constants.STAGING_PAYMENT_METHOD_D
                        ? accBroker : phoStg.PHO_AccountingBroker__c;
                phoStg.PHO_CarrierAccount__c = carrAcc;
                phoStg.PHO_MGAAccount__c = mgaAcc;
                phoStg.PHO_ProducingBroker__c = prodBroker;
                phoStg.PHO_Product__c = prodMap.get(productKey);

            } catch(Exception exp) {
                phoStg.addError(exp);
            }

        }
    }

}