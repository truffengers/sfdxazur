/**
* @name: IBA_IBAStaging
* @description: Trigger for the custom object IBA_IBAStaging__c
*
*/

trigger IBA_IBAStaging on IBA_IBAStaging__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    TriggerDispatcher.Run(new IBA_IBAStagingTriggerHandler());
}