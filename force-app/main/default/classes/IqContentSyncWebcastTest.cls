@isTest
private class IqContentSyncWebcastTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();

	@isTest static void basic() {
		IQ_Content__c iqContent = tog.getWebcastIqContent();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(123);

		iqContent.BrightTALK_Webcast__c = webcast.Id;
		update iqContent;

		webcast = [SELECT Id, IQ_Content__c FROM BrightTALK__Webcast__c WHERE Id = :webcast.Id];

		System.assertEquals(iqContent.Id, webcast.IQ_Content__c);
	}
		
}