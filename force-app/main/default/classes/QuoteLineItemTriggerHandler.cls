/**
* @name: QuoteLineItemTriggerHandler
* @description: This class is handling and dispatching the relevant account trigger functionality
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 03/11/2018
*
*/

public with sharing class QuoteLineItemTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled()
    {
        return QuoteLineItemTriggerHandler.IsDisabled != null ? QuoteLineItemTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems)
    {
        EWQuoteLineItemHelper.HandleVlocityAttributes(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
    {
        EWQuoteLineItemHelper.HandleVlocityAttributes(newItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {

        AuditLogUtil.createAuditLog('QuoteLineItem', newItems.values(), null );
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

        AuditLogUtil.createAuditLog('QuoteLineItem', newItems.values(), oldItems );
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}