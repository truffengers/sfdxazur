/**
* @name: EWInsuranceClaimDataFactory
* @description: Reusable factory methods for insurance claim object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 18/12/2018
* 16/1/2018 Roy Lloyd   Amended to replace custom with Vlocity fields
*/
@isTest
public with sharing class EWInsuranceClaimDataFactory {

    public static vlocity_ins__InsuranceClaim__c createInsuranceClaim(String name,
                                                                      Date claimDate,
                                                                      String claimType,
                                                                      String lossType,
                                                                      Decimal claimValue,
                                                                      Id quoteId,
                                                                      Id policyId,
                                                                      Boolean insertRecord) {

        vlocity_ins__InsuranceClaim__c insuranceClaim = new vlocity_ins__InsuranceClaim__c();
        insuranceClaim.Name = (name == null) ? 'testInsuranceClaim' : name;
        insuranceClaim.EW_Quote__c = quoteId;
        insuranceClaim.vlocity_ins__PrimaryPolicyAssetId__c = policyId;
        insuranceClaim.vlocity_ins__LossDate__c = (claimDate == null) ? Date.today() : claimDate;
        insuranceClaim.vlocity_ins__LossType__c = (lossType == null) ? 'Property' : lossType;
        insuranceClaim.vlocity_ins__LossCause__c = (claimType == null) ? 'Fire' : claimType;
        insuranceClaim.vlocity_ins__TotalIncurred__c = (claimValue == null) ? 10000 : claimValue;

        if (insertRecord) {

            insert insuranceClaim;
        }

        return insuranceClaim;
    }
}