import { LightningElement, track } from 'lwc';
import SendBrokerRequestEmail from '@salesforce/apex/EWEmailHelper.SendBrokerRequestEmail';

export default class ew_registerInterest extends LightningElement {
    answers = {
        name: '',
        role: '',
        company: '',
        location: '',
        phone: '',
        email: ''
    }
    @track displayError;
    @track errorMessage;
    @track formSubmitted = false;

    handleInputChange(event) {
        this.answers[event.target.dataset.question] = event.target.value;
    }

    handleSubmit(e) {
        e.preventDefault();
        let errorMessage = this.formHasErrors(this.answers);
        if (errorMessage) {
            this.errorMessage = 'Looks like there is an error! ' + errorMessage;
            this.displayError = true;
            return;
        }

        SendBrokerRequestEmail(this.answers)
            .then(() => {
                this.formSubmitted = true;
            })
            .catch(() => {
                this.errorMessage = 'Sorry, there was an error. Please try again later.'
                this.displayError = true;
            });
    }

    formHasErrors(answers) {
        let errorMessage = '';
        let emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let numberRegex = /^[0-9]*$/;

        Object.keys(answers).forEach(question => {
            let capitalizedQuestion
                = question.charAt(0).toUpperCase() + question.slice(1);

            // Ensure that no answers are blank
            if (!answers[question]) {
                errorMessage += `${capitalizedQuestion} cannot be blank. `;
                return;
            }

            // Ensure that email is whatever@whatever.xxx format
            if (question === 'email'
                && !emailRegex.test(answers.email.toLowerCase())) {
                errorMessage += 'The email format is incorrect. ';
                return;
            }

            // Ensure that phone number only contains numeric characters
            if (question === 'phone'
                && !numberRegex.test(answers.phone)) {
                errorMessage += 'The phone number format is incorrect. ';
                return;
            }

            // Ensure that no answers are too short
            if (answers[question].length === 1) {
                errorMessage += `Please enter a valid ${question}. `;
            }
        });

        return errorMessage;
    }

    goBack() {
        window.history.back();
    }
}