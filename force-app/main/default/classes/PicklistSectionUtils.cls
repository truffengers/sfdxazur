public without sharing class PicklistSectionUtils {

    @TestVisible
    private static final String PS_OBJECT_API_NAME = 'Picklist_Section__c';
    private static final String DEFAULT_CSV_SEPARATOR = ',';

    public static final String BUSINESS_TYPE = 'Business_Type';
    public static final String NATIONALITY = 'Nationality';
    public static final String OCCUPATION = 'Occupation';
    public static final String TITLE = 'Title';

    public static void updateRecordsBasedOnStaticResource(String csvResourceName, String psRtDevName) {
        updateRecordsBasedOnStaticResource(csvResourceName, psRtDevName, DEFAULT_CSV_SEPARATOR, false, true);
    }

    public static void updateRecordsBasedOnStaticResource(String csvResourceName, String psRtDevName, String csvSeparator, Boolean fillScoreWithZeros, Boolean deleteOldData) {
        List<StaticResource> srDataSourceList = [SELECT Id, Body FROM StaticResource WHERE Name = :csvResourceName LIMIT 1];
        if (!srDataSourceList.isEmpty()) {
            List<String> csvDS = srDataSourceList.get(0).Body.toString().split('\n');
            updateRecords(csvDS, psRtDevName, csvSeparator, fillScoreWithZeros, deleteOldData);
        }
    }

    public static void updateRecords(List<String> csvDataLines, String psRtDevName) {
        updateRecords(csvDataLines, psRtDevName, DEFAULT_CSV_SEPARATOR, false, true);
    }

    public static void updateRecords(List<String> csvDataLines, String psRtDevName, String csvSeparator, Boolean fillScoreWithZeros, Boolean deleteOldData) {
        Id recordTypeId = getRecordTypeId(psRtDevName);
        Savepoint sp = Database.setSavepoint();

        try {
            if (deleteOldData) {
                psDataCleanUp(recordTypeId);
            }

            List<Picklist_Section__c> newPRecords = new List<Picklist_Section__c>();

            for (String csvLine : csvDataLines) {
                List<String> lineElements = csvLine.split(csvSeparator);

                Picklist_Section__c ps = new Picklist_Section__c();
                ps.RecordTypeId = recordTypeId;
                ps.Name = lineElements.get(0);

                if (lineElements.size() > 1) {
                    ps.Score__c = Decimal.valueOf(lineElements.get(1).trim());
                } else if (fillScoreWithZeros) {
                    ps.Score__c = 0;
                }

                newPRecords.add(ps);
            }

            insert newPRecords;
        } catch (Exception e) {
            Database.rollback(sp);
            System.debug('EXCEPTION // message: ' + e.getMessage());
            System.debug('EXCEPTION // stack trace: ' + e.getStackTraceString());
        }
    }

    private static Id getRecordTypeId(String rtDevName) {
        List<RecordType> rts = [
                SELECT Id, Name, DeveloperName
                FROM RecordType
                WHERE SobjectType = :PS_OBJECT_API_NAME
                AND DeveloperName = :rtDevName
                LIMIT 1
        ];

        return rts.isEmpty() ? null : rts.get(0).Id;
    }

    private static void psDataCleanUp(Id recordTypeId) {
        delete [SELECT Id FROM Picklist_Section__c WHERE RecordTypeId = :recordTypeId];
    }

}