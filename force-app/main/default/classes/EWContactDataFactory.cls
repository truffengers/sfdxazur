/**
* @name: EWContactDataFactory
* @description: Reusable factory methods for contact object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 16/04/2019
*/
@isTest
public class EWContactDataFactory {

    public static Contact createContact(String firstName,
    									String lastName,
    									Id accountId,
                                        Boolean insertRecord) {

        Contact con = new Contact();
        con.FirstName = (firstName == null) ? 'test' : firstName;
        con.LastName = (lastName == null) ? 'contact' : lastName;
        con.AccountId = accountId; 

        if (insertRecord) {
            insert con;
        }

        return con;
    }
}