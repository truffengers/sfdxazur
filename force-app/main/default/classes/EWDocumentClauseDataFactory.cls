/**
* @name: EWDocumentClauseDataFactory
* @description: Reusable factory methods for document clause object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 04/02/2019
*/
@isTest
public with sharing class EWDocumentClauseDataFactory {

	public static vlocity_ins__DocumentClause__c createDocumentClause(String name,
																	  String content,
																	  Boolean isStandard,
	    									   						  Boolean insertRecord) {

        vlocity_ins__DocumentClause__c documentClause = new vlocity_ins__DocumentClause__c();
		documentClause.Name = (name == null) ? 'testDocumentClauseName' : name;
		documentClause.vlocity_ins__ClauseContent__c = (content == null) ? 'testDocumentClauseContent' : content;
		documentClause.vlocity_ins__Category__c = isStandard ? EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD : EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED;
        
        if (insertRecord) {
			insert documentClause;
		}

        return documentClause;
    }
}