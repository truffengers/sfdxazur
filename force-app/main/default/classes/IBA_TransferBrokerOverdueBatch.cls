/**
* @name: IBA_TransferBrokerOverdueBatch
* @description: Batch transfer broker overdue value from policy transactions to related policies.
*
*/
global with sharing class IBA_TransferBrokerOverdueBatch implements Database.Batchable<SObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String active = IBA_UtilConst.POLICY_STATUS_ACTIVE;
        String query = 'SELECT Id, IBA_AccountingBrokerOverdue__c, IBA_BrokerOvedue__c,'
            + ' (SELECT Id, IBA_ActivePolicy__c, IBA_AccountingBrokerOverdue__c, IBA_BrokerOverdue__c, IBA_AccountingBrokerSalesInvoiceDueDate__c, IBA_BrokerSalesInvoiceDueDate__c'
            + ' FROM FF_Policy_Transactions__r WHERE IBA_AccountingBrokerSalesInvoiceDueDate__c = YESTERDAY OR IBA_BrokerSalesInvoiceDueDate__c = YESTERDAY)'
            + ' FROM Asset WHERE Policy_Status__c = :active';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Asset> scope) {
        for(Asset policy :scope) {
            Decimal brokerOverdue, accountingBrokerOverdue;
            for(IBA_PolicyTransaction__c policyTransaction :policy.FF_Policy_Transactions__r) {
                if(policy.Id == policyTransaction.IBA_ActivePolicy__c) {
                    if(policyTransaction.IBA_BrokerOverdue__c != null) {
                        if(brokerOverdue == null) {
                            brokerOverdue = policyTransaction.IBA_BrokerOverdue__c;
                        } else {
                            brokerOverdue += policyTransaction.IBA_BrokerOverdue__c;
                        }
                    }
                    if(policyTransaction.IBA_AccountingBrokerOverdue__c != null) {
                        if(accountingBrokerOverdue == null) {
                            accountingBrokerOverdue = policyTransaction.IBA_AccountingBrokerOverdue__c;
                        } else {
                            accountingBrokerOverdue += policyTransaction.IBA_AccountingBrokerOverdue__c;
                        }
                    }
                }
            }
            if(brokerOverdue != null) {
                policy.IBA_BrokerOvedue__c = brokerOverdue;
            }
            if(accountingBrokerOverdue != null) {
                policy.IBA_AccountingBrokerOverdue__c = accountingBrokerOverdue;
            }
        }
        update scope;
    }

    global void finish(Database.BatchableContext bc) {

    }
}