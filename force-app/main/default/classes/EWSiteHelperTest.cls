/**
 * Created by roystonlloyd on 2019-08-13.
 */
@isTest
public with sharing class EWSiteHelperTest {


    static User brokerUser;

    static testMethod void testPolicyRelatedListReturnsNull() {

        Id insuredId = [SELECT Id FROM Account WHERE RecordTypeId =:Schema.SObjectType.Account.getRecordTypeInfosByName().get(EWConstants.ACCOUNT_PERSON_RECORD_TYPE).getRecordTypeId() LIMIT 1].Id;
        System.runAs(brokerUser){
            List<Asset> policies = EWSiteHelper.policyRelatedList(insuredId);
            System.assertEquals(true, policies.isEmpty());

        }
    }


    static testMethod void testPolicyRelatedListReturnsValues() {

        Id insuredId = [SELECT Id FROM Account WHERE RecordTypeId =:Schema.SObjectType.Account.getRecordTypeInfosByName().get(EWConstants.ACCOUNT_PERSON_RECORD_TYPE).getRecordTypeId() LIMIT 1].Id;
        List<Asset> policies = EWSiteHelper.policyRelatedList(insuredId);
        System.assertEquals(false, policies.isEmpty());

    }


    // data setup
    static {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false);
        insert new List<Account>{ broker, insured};

        User systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,null,true);
        System.runAs(systemAdminUser) {
            Id partnerProfileId = [SELECT Id FROM Profile WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME].Id;
            Contact brokerContact = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser = EWUserDataFactory.createPartnerUser('brokerUser', partnerProfileId, brokerContact.Id, true);
        }

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);

        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.EW_Quote__c = quote.Id;
        insert policy;

    }

}