/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 14/10/2017
 * Description: (if required)
 */

@isTest
private class BrokerIqContributorsProviderTest {
    private static TestObjectGenerator tog = new TestObjectGenerator();

    @isTest static void testBehavior() {
        Broker_iQ_Contributor__c contributor = tog.getContributor(true);
        contributor.Featured_Contributor__c = true;
        update contributor;

        List<Broker_iQ_Contributor__c> results = BrokerIqContributorsProviderController.getContributors();

        System.assertEquals(1, results.size());
        System.assertEquals(contributor.Id, results[0].Id);
    }

    @isTest static void testSingle() {
        Broker_iQ_Contributor__c contributor = tog.getContributor(true);
        contributor.Featured_Contributor__c = true;
        update contributor;

        Broker_iQ_Contributor__c result = BrokerIqContributorsProviderController.getContributor(contributor.Id);

        System.assertEquals(contributor.Id, result.Id);
    }
    @isTest static void testSingleNoMatch() {
        System.assertEquals(null, BrokerIqContributorsProviderController.getContributor(null));
    }
}