TT.imageParallaxItem = function item($parent, isWindow, getScrollY, getWindowHeight, getScrollMax, options) {

  var $child;
  var parentHeight;
  var childHeight;
  var maxTranslation;
  var lastRatio;

  var WIDTH  = 'width';
  var HEIGHT = 'height';

  var POSITIONS = {
    width  : 'left',
    height : 'top'
  };



  function init() {
    referenceChild();
    setDimension(HEIGHT);
    setDimension(WIDTH);
    updateSize();
  }

  function setDimension(dimension) {
    if(options[dimension] === undefined) { return; }
    var css = {};
    var value = parseFloat(options[dimension]);
    css[POSITIONS[dimension]] = (-(value - 100) / 2) + '%';
    $child[dimension](value + '%');
    $child.css(css);
  }

  function referenceChild() {
    $child = $parent.children().eq(0);
  }

  function getParentY() {
    return isWindow ? $parent.offset().top : $parent.position().top;
  }

  function getRatio() {

    var ratio;
    var boundScrollY;
    var parentY;
    var scrollStart;
    var scrollFinish;

    parentY      = getParentY();
    scrollStart  = parentY - getWindowHeight();
    scrollFinish = parentY + parentHeight;
    scrollStart  = scrollStart < 0 ? 0 : scrollStart;
    scrollFinish = scrollFinish > getScrollMax() ? getScrollMax() : scrollFinish;
    boundScrollY = getScrollY() - scrollStart;
    ratio        = boundScrollY / (scrollFinish - scrollStart);
    ratio        = ratio < 0 ? 0 : ratio;
    ratio        = ratio > 1 ? 1 : ratio;

    return ratio;
  }

  function updateSize() {
    parentHeight   = $parent.height();
    childHeight    = $child[0].scrollHeight;
    maxTranslation = (childHeight - parentHeight) / 2;
  }

  function updatePosition() {
    var ratio = getRatio();
    if(ratio !== lastRatio) {
      var translation = Math.round(maxTranslation * (ratio - 0.5));
      $child.css({transform: 'translate3d(0, ' + translation + 'px, 0)'});
      if(lastRatio === undefined) { $child.addClass('is-parallaxed'); }
      lastRatio = ratio;
    }
  }

  init();

  return {
    updateSize     : updateSize,
    updatePosition : updatePosition
  };

};