import { LightningElement } from 'lwc';
import BROKER_HUB from '@salesforce/resourceUrl/BrokerHub';

export default class Ew_xmas extends LightningElement {
    santaHatUrl = BROKER_HUB + '/assets/images/xmas/santa-hat.png';

    snowLargeUrl = BROKER_HUB + '/assets/images/xmas/snow-large.png';
    snowMediumUrl = BROKER_HUB + '/assets/images/xmas/snow-medium.png';
    snowSmallUrl = BROKER_HUB + '/assets/images/xmas/snow-small.png';

    snowLargeStyle = `background-image: url('${this.snowLargeUrl}')`;
    snowMediumStyle = `background-image: url('${this.snowMediumUrl}')`;
    snowSmallStyle = `background-image: url('${this.snowSmallUrl}')`;
}