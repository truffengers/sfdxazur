/**
* @name: EWJSONHelper
* @description: Utility Class to assist with Emerging Wealth JSON
* @author: Roy Lloyd, rlloyd@azuruw.com
* @date: 19/02/2019
*
*/

public with sharing class EWJSONHelper {

    private static final Integer LONG_TEXT_MAX_SIZE = 131072;

    public static final String CLIENT_DETAILS_NODE = 'clientNode';
    public static final String PROPERTY_NODE = 'propertyNode';
    public static final String AMB_NODE = 'ambNode';
    public static final String SUBS_NODE = 'subsNode';
    public static final String OUTRA_NODE = 'outraNode';
    public static final String STATUS_NODE = 'statusNode';
    public static final String COVERAGE_NODE = 'coverageNode';
    public static final String CONFIGURE_POLICY = 'configurePolicy';
    public static final String CUSTOM_NODE = 'customNode';
    public static final String REPRICING_UI_NODE = 'repricingUserInputsNode';
    public static final String REPRICING_II_NODE = 'repricingInsuredItemsNode';
    public static final String ADDITIONAL_DATA = 'additionalData';

    public static final String YEAR_BUILD_KEY_NAME = 'year_built';
    public static final String AGE_RANGE_KEY_NAME = 'ageRange';
    public static final String RADIO_CLAIMS_HISTORY_KEY_NAME = 'radioClaimsHistory';
    public static final String CLAIMS_LIST_KEY_NAME = 'editBlockClaimsHistory';
    public static final String CLAIMS_LIST_READONLY_KEY_NAME = 'editBlockClaimsHistoryReadOnly';
    public static final String CLAIM_ID_KEY_NAME = 'claimId';


    public static String typeOfTransaction;

    public static final Map<String, String> outraFieldMapping = new Map<String, String>{
            'EW_MarketValue__c' => 'property_value',
            'EW_NumberOfBathrooms__c' => 'bathrooms',
            'EW_NumberOfBedrooms__c' => 'bedrooms',
            'EW_RebuildCost__c' => 'reinstatement_cost_higher_rate_azur',
            'EW_SquareMetres__c' => 'floor_area',
            'EW_Style__c' => 'property_style',
            'EW_Tenure__c' => 'tenure',
            'EW_Type__c' => 'property_type',
            'EW_YearBuilt__c' => YEAR_BUILD_KEY_NAME
    };

    public static final Map<String, Set<String>> userInputsFieldMapping = new Map<String, Set<String>>{
            'property_value' => new Set<String>{
                    'insuredProperty.marketValue', 'insuredProperty.propertyValue'
            },
            'bathrooms' => new Set<String>{
                    'insuredProperty.numberOfBathrooms'
            },
            'bedrooms' => new Set<String>{
                    'insuredProperty.numberOfBedrooms'
            },
            'reinstatement_cost_higher_rate_azur' => new Set<String>{
                    'insuredProperty.buildingSI', 'buildings.buildingSumInsured'
            },
            'floor_area' => new Set<String>{
                    'insuredProperty.squareMetres'
            },
            'property_style' => new Set<String>{
                    'insuredProperty.propertyStyle'
            },
            'tenure' => new Set<String>{
                    'insuredProperty.tenure'
            },
            'property_type' => new Set<String>{
                    'insuredProperty.propertyType'
            },
            YEAR_BUILD_KEY_NAME => new Set<String>{
                    'insuredProperty.yearBuilt'
            },
            'ageRange' => new Set<String>{
                    'insuredProperty.ageRange'
            }
    };

    public static final Map<String, Set<String>> insuredItemPropertyMapping = new Map<String, Set<String>>{
            'property_value' => new Set<String>{
                    'marketValue', 'propertyValue'
            },
            'bathrooms' => new Set<String>{
                    'numberOfBathrooms'
            },
            'bedrooms' => new Set<String>{
                    'insuredProperty.numberOfBedrooms'
            },
            'reinstatement_cost_higher_rate_azur' => new Set<String>{
                    'buildingSI'
            },
            'floor_area' => new Set<String>{
                    'squareMetres'
            },
            'property_style' => new Set<String>{
                    'propertyStyle'
            },
            'tenure' => new Set<String>{
                    'tenure'
            },
            'property_type' => new Set<String>{
                    'propertyType'
            },
            YEAR_BUILD_KEY_NAME => new Set<String>{
                    'yearBuilt'
            },
            'ageRange' => new Set<String>{
                    'ageRange'
            }
    };

    public static void postQuoteJSONData(Id quoteId, Map<String, Object> payload) {
        List<EW_QuoteJSONData__c> oldRecords = new List<EW_QuoteJSONData__c>([
                SELECT Id
                FROM EW_QuoteJSONData__c
                WHERE EW_Quote__c = :quoteId
        ]);

        delete oldRecords;

        EW_QuoteJSONData__c jsonDataRecord = new EW_QuoteJSONData__c();
        jsonDataRecord.EW_ClientNode__c = JSON.serializePretty(payload.get(CLIENT_DETAILS_NODE));
        jsonDataRecord.EW_PropertyNode__c = JSON.serializePretty(payload.get(PROPERTY_NODE));
        jsonDataRecord.EW_AmbNode__c = JSON.serializePretty(payload.get(AMB_NODE));
        jsonDataRecord.EW_SubsNode__c = JSON.serializePretty(payload.get(SUBS_NODE));
        jsonDataRecord.EW_OutraNode__c = JSON.serializePretty(payload.get(OUTRA_NODE));
        jsonDataRecord.EW_Quote__c = quoteId;

        String coverageNodeString = '';
        Boolean needsAttachment = false;
        fixForRemovedClaims(jsonDataRecord);

        if (payload.containsKey(COVERAGE_NODE)) {
            coverageNodeString = JSON.serialize(payload.get(COVERAGE_NODE)).remove('\n');
            while (coverageNodeString.contains('  ')) {
                coverageNodeString = coverageNodeString.replaceAll('  ', '');
            }

            if (coverageNodeString.length() < LONG_TEXT_MAX_SIZE) {
                jsonDataRecord.EW_CoverageNode__c = coverageNodeString;
            } else {
                needsAttachment = true;
            }
        }
        if (payload.containsKey(REPRICING_UI_NODE)) {
            jsonDataRecord.EW_RepricingUserInputsNode__c = JSON.serializePretty(payload.get(REPRICING_UI_NODE));
        }
        if (payload.containsKey(REPRICING_II_NODE)) {
            jsonDataRecord.EW_RepricingInsuredItemsNode__c = JSON.serializePretty(payload.get(REPRICING_II_NODE));
        }

        if (payload.containsKey(CUSTOM_NODE)) {
            jsonDataRecord.EW_CustomNode__c = JSON.serializePretty(payload.get(CUSTOM_NODE));
        }

        insert jsonDataRecord;

        if (needsAttachment) {
            Attachment coverageNode = new Attachment();
            coverageNode.Body = Blob.valueOf(coverageNodeString);
            coverageNode.Name = COVERAGE_NODE;
            coverageNode.ParentId = jsonDataRecord.Id;
            insert coverageNode;
        }
    }

    private static void fixForRemovedClaims(EW_QuoteJSONData__c jsonDataRecord) {
        Map<String, Object> propertyNode = (Map<String, Object>) JSON.deserializeUntyped(jsonDataRecord.EW_PropertyNode__c);

        if (String.valueOf(propertyNode.get(RADIO_CLAIMS_HISTORY_KEY_NAME)) == 'no') {

            Object editBlockClaimsHistory = propertyNode.get(CLAIMS_LIST_KEY_NAME);
            Set<Id> claimsToDelete = new Set<Id>();

            if (editBlockClaimsHistory instanceof Map<String, Object>) {
                Map<String, Object> claim = (Map<String, Object>) editBlockClaimsHistory;
                claimsToDelete.add(String.valueOf(claim.get(CLAIM_ID_KEY_NAME)));
            }

            if (editBlockClaimsHistory instanceof List<Object>) {
                for (Object claimObj : (List<Object>) editBlockClaimsHistory) {
                    Map<String, Object> claim = (Map<String, Object>) claimObj;
                    claimsToDelete.add(String.valueOf(claim.get(CLAIM_ID_KEY_NAME)));
                }
            }

            delete new List<vlocity_ins__InsuranceClaim__c>([
                    SELECT Id
                    FROM vlocity_ins__InsuranceClaim__c
                    WHERE Id IN :claimsToDelete
            ]);

            propertyNode.put(CLAIMS_LIST_KEY_NAME, null);
            jsonDataRecord.EW_PropertyNode__c = JSON.serializePretty(propertyNode);

        }
    }


    public static Map<String, Object> getQuoteJSONData(Map<String, Object> optionMap) {

        Id recordId = (Id) optionMap.get('recordId');

        Boolean includeCoverageNode =
                optionMap.containsKey('includeCoverageNode') ? (Boolean) optionMap.get('includeCoverageNode') : false;

        Boolean isNewRenewalQuote =
                optionMap.containsKey('isNewRenewalQuote') ? (Boolean) optionMap.get('isNewRenewalQuote') : false;

        String quoteTransactionType = optionMap.containsKey('quoteTransactionType') ? (String) optionMap.get('quoteTransactionType') : null;
        String mtaOriginatedFrom = optionMap.containsKey('mtaOriginatedFrom') ? (String) optionMap.get('mtaOriginatedFrom') : null;

        Map<String, Object> jsonResult = new Map<String, Object>();
        EW_QuoteJSONData__c quoteJSONData = getQuoteJSONDataRecord(recordId);

        List<Quote> quotes = new List<Quote>([
                SELECT Id, Status, vlocity_ins__EffectiveDate__c, EW_Excess__c, EW_BrokerCommissionPercent__c, EW_Policy__r.vlocity_ins__ExpirationDate__c
                FROM Quote
                WHERE Id = :recordId
        ]);

        Quote quote;
        if (!quotes.isEmpty()) {
            quote = quotes.get(0);

            Map<String,Object> additionalDataMap = new Map<String, Object>{
                    'currentExcess' => quote.EW_Excess__c,
                    'currentBrokerCommissionPercent' => quote.EW_BrokerCommissionPercent__c,
                    'currentEffectiveDate' => quote.vlocity_ins__EffectiveDate__c
            };

            if(isNewRenewalQuote){
                additionalDataMap.put(
                    'sourcePolicyExpirationDate', quote.EW_Policy__r.vlocity_ins__ExpirationDate__c
                );
            }

            jsonResult.put(STATUS_NODE, quote.Status);
            jsonResult.put(ADDITIONAL_DATA, (Object) additionalDataMap );
        }

        if (quoteJSONData != null) {
            if (String.isNotBlank(quoteJSONData.EW_ClientNode__c)) {
                jsonResult.put(CLIENT_DETAILS_NODE, JSON.deserializeUntyped(quoteJSONData.EW_ClientNode__c));
            }
            if (String.isNotBlank(quoteJSONData.EW_PropertyNode__c)) {

                Map<String, Object> propertyNodeMap = (Map<String, Object>) JSON.deserializeUntyped(quoteJSONData.EW_PropertyNode__c);

                // add/replace Main Residence recordId to JSON
                List<vlocity_ins__InsurableItem__c> residenceList = [
                        SELECT Id
                        FROM vlocity_ins__InsurableItem__c
                        WHERE EW_Quote__c = :recordId AND EW_CoverType__c = :EWConstants.INSURABLE_ITEM_COVER_TYPE_PROPERTY AND EW_Active__c = TRUE
                        LIMIT 1
                ];
                if (residenceList != null && !residenceList.isEmpty()) {
                    propertyNodeMap.put('recordId', residenceList[0].Id);
                }

                // on new renewal and MTA quotes, the quote recordId = the live policy quote. when editing the renewal/MTA quote, the claims should already be associated to the quote record.
                if( (isNewRenewalQuote && quote != null && quote.EW_Policy__c != null)
                        || (mtaOriginatedFrom != null && mtaOriginatedFrom == 'Policy' && quoteTransactionType == EWConstants.QUOTE_TRANSACTION_TYPE_MTA)
                ){

                    List<EWClaimsHelper.InputClaim> claims = EWClaimsHelper.getPolicyClaims( quote.EW_Policy__c );

                    // create ReadOnly version.  Need to replicate because unable to dynamically prevent Delete in the QuoteProperty Omniscript.
                    List<EWClaimsHelper.OutputClaim> readOnlyclaims = new List<EWClaimsHelper.OutputClaim>();
                    List<EWClaimsHelper.InputClaim> editableClaims = new List<EWClaimsHelper.InputClaim>();

                    for(EWClaimsHelper.InputClaim claim : claims){
                        EWClaimsHelper.OutputClaim readOnlyClaim = new EWClaimsHelper.OutputClaim();
                        readOnlyClaim.radioClaimAtCurrentPropertyRO = claim.radioClaimAtCurrentProperty;
                        readOnlyClaim.textClaimValueRO = claim.textClaimValue;
                        readOnlyClaim.claimDateTextRO = claim.claimDateText;
                        readOnlyClaim.selectClaimTypeRO = claim.selectClaimType;
                        readOnlyClaim.radioClaimStillOpenRO = claim.radioClaimStillOpen;
                        readOnlyClaim.azurClaimRO = claim.azurClaim;
                        readOnlyClaim.claimNameRO = claim.claimName;
                        readOnlyClaim.claimIdRO = claim.claimId;
                        readOnlyClaim.claimDescriptionRO = claim.claimDescription;
                        readOnlyclaims.add(readOnlyClaim);

                        claim.claimId = null;
                        editableClaims.add(claim);
                    }
                    propertyNodeMap.put(CLAIMS_LIST_READONLY_KEY_NAME, readOnlyclaims);
                    propertyNodeMap.put(CLAIMS_LIST_KEY_NAME, editableClaims);

                    if(!editableClaims.isEmpty()) propertyNodeMap.put(RADIO_CLAIMS_HISTORY_KEY_NAME, 'yes');
                }

                jsonResult.put(PROPERTY_NODE, propertyNodeMap);
            }
            if (String.isNotBlank(quoteJSONData.EW_AmbNode__c) && !isNewRenewalQuote) {
                jsonResult.put(AMB_NODE, JSON.deserializeUntyped(quoteJSONData.EW_AmbNode__c));
            }
            if (String.isNotBlank(quoteJSONData.EW_SubsNode__c) && !isNewRenewalQuote) {
                jsonResult.put(SUBS_NODE, JSON.deserializeUntyped(quoteJSONData.EW_SubsNode__c));
            }
            if (String.isNotBlank(quoteJSONData.EW_OutraNode__c) && !isNewRenewalQuote) {
                jsonResult.put(OUTRA_NODE, JSON.deserializeUntyped(quoteJSONData.EW_OutraNode__c));
            }
            if (String.isNotBlank(quoteJSONData.EW_CustomNode__c)) {
                jsonResult.put(CUSTOM_NODE, JSON.deserializeUntyped(quoteJSONData.EW_CustomNode__c));
            }

            if (includeCoverageNode) {
                Object coverageNode = getCoverageNode(quoteJSONData);
                if (coverageNode != null) {
                    jsonResult.put(COVERAGE_NODE, coverageNode);
                }
            }
        }
        return jsonResult;
    }

    public static Map<String, Object> getQuoteCoverageData(Id recordId) {
        Map<String, Object> jsonResult = new Map<String, Object>();
        EW_QuoteJSONData__c quoteJSONData = getQuoteJSONDataRecord(recordId);

        if (quoteJSONData != null) {
            Object coverageNode = getCoverageNode(quoteJSONData);
            if (coverageNode != null) {
                jsonResult.put(COVERAGE_NODE, coverageNode);
            }
        }
        return jsonResult;
    }

    public static Map<String, Object> postQuoteCoverageData(Id recordId, Map<String, Object> payload) {
        EW_QuoteJSONData__c quoteJSONData = getQuoteJSONDataRecord(recordId);

        String coverageNodeString = JSON.serialize(payload).remove('\n');
        while (coverageNodeString.contains('  ')) {
            coverageNodeString = coverageNodeString.replaceAll('  ', '');
        }


        if (coverageNodeString.length() < LONG_TEXT_MAX_SIZE) {
            quoteJSONData.EW_CoverageNode__c = coverageNodeString;
            update quoteJSONData;
        } else {
            Attachment coverageNode = new Attachment();
            coverageNode.Body = Blob.valueOf(coverageNodeString);
            coverageNode.Name = COVERAGE_NODE;
            coverageNode.ParentId = quoteJSONData.Id;
            insert coverageNode;
        }

        Map<String,Object> configurePolicy = (Map<String,Object>) ((List<Object>) payload.get(CONFIGURE_POLICY)).get(0);
        return configurePolicy;
    }

    @TestVisible
    private static EW_QuoteJSONData__c getQuoteJSONDataRecord(Id recordId) {
    
        List<EW_QuoteJSONData__c> quoteJSONDataList = new List<EW_QuoteJSONData__c>([
                SELECT Id, EW_PropertyNode__c, EW_OutraNode__c, EW_SubsNode__c, EW_AmbNode__c, EW_ClientNode__c, EW_CoverageNode__c,
                        EW_CustomNode__c, EW_RepricingInsuredItemsNode__c, EW_RepricingUserInputsNode__c, EW_Quote__r.Status, (
                        SELECT Id
                        FROM Attachments
                        WHERE Name = :COVERAGE_NODE
                        ORDER BY CreatedDate DESC
                        LIMIT 1
                )
                FROM EW_QuoteJSONData__c
                WHERE (EW_Quote__c = :recordId OR EW_Policy__c = :recordId)
                ORDER BY CreatedDate DESC
                LIMIT 1
        ]);

        if (!quoteJSONDataList.isEmpty()) {
            return quoteJSONDataList.get(0);
        } else {
            return null;
        }
    }


    private static Object getCoverageNode(EW_QuoteJSONData__c quoteJSONData) {
        Object coverageNode = null;

        if (String.isNotBlank(quoteJSONData.EW_CoverageNode__c)) {
            coverageNode = JSON.deserializeUntyped(quoteJSONData.EW_CoverageNode__c);
        } else if (!quoteJSONData.Attachments.isEmpty()) {
            /** Because of: "Error: Binary fields cannot be selected in join queries" **/
            Attachment coverageNodeAttachment = [
                    SELECT Id, Body
                    FROM Attachment
                    WHERE Id = :quoteJSONData.Attachments.get(0).Id
            ];
            coverageNode = JSON.deserializeUntyped(coverageNodeAttachment.Body.toString());
        } else {
            return null;
        }

        if(Test.isRunningTest()){
            return coverageNode;
        } else {
            return validateCoverageNodeForPCIs(coverageNode);
        }
    }

    private static Object validateCoverageNodeForPCIs(Object coverageJSONObject) {
        Map<String, Object> coverageMap = (Map<String, Object>) coverageJSONObject;
        Map<String, Object> configurePolicy = (Map<String, Object>) ((List<Object>) coverageMap.get('configurePolicy')).get(0);
        Map<String, Object> childProducts = (Map<String, Object>) configurePolicy.get('childProducts');
        List<Object> childProductsRecords = (List<Object>) childProducts.get('records');

        Map<String, Object> childProductMap = (Map<String, Object>) childProductsRecords.get(0);
        Id pciTestId = String.valueOf(childProductMap.get('pciId'));

        Map<Id, vlocity_ins__ProductChildItem__c> pciMap = new Map<Id, vlocity_ins__ProductChildItem__c>([
                SELECT Id, Name, vlocity_ins__ChildProductId__c, vlocity_ins__ParentProductId__c, vlocity_ins__ParentProductId__r.Name
                FROM vlocity_ins__ProductChildItem__c
                WHERE vlocity_ins__ParentProductId__r.Name = 'Emerging Wealth' OR Id = :pciTestId
        ]);

        Boolean allGood = pciMap.containsKey(pciTestId);

        if (allGood) {
            return coverageJSONObject;
        }

        Map<Id, Id> pciIdsMapping = new Map<Id, Id>();

        for (vlocity_ins__ProductChildItem__c pci : pciMap.values()) {
            pciIdsMapping.put(pci.vlocity_ins__ChildProductId__c, pci.Id);
        }

        for (Object childProductObject : childProductsRecords) {
            Map<String, Object> childProduct = (Map<String, Object>) childProductObject;

            Id pciId = String.valueOf(childProduct.get('pciId'));
            Id productId = String.valueOf(childProduct.get('productId'));

            childProduct.put('pciId', pciIdsMapping.get(productId));
        }

        return coverageJSONObject;
    }

    public static String updateJSONstring(String JSONstring, Map<String, Object> updateMap, Boolean isMultiplier, Integer precision) {
        // isMultiplier enables update to adjust the current value

        Map<String, Object> jsonMap = (Map<String, Object>) JSON.deserializeUntyped(JSONString);

        for (String key : updateMap.keySet()) {

            if (jsonMap.containsKey(key)) {

                if (isMultiplier) {
                    Decimal adjustment = (Decimal) updateMap.get(key);
                    Decimal originalValue = (Decimal) jsonMap.get(key);
                    Decimal newValue = originalValue * adjustment;

                    if (precision != null) {
                        newValue = newValue.setScale(precision);
                    }
                    jsonMap.put(key, newValue);

                } else {
                    jsonMap.put(key, updateMap.get(key));
                }

            }
        }
        return JSON.serialize(jsonMap);
    }

    public static void updateOutraNodeOnIIUpdate(Map<Id, Map<String, Object>> quotesWithUpdates) {
        List<EW_QuoteJSONData__c> quoteJSONData = filterQuoteJSONDataRecords(new List<EW_QuoteJSONData__c>([
                SELECT Id, EW_Quote__c, EW_OutraNode__c, EW_RepricingUserInputsNode__c, EW_CoverageNode__c, EW_RepricingInsuredItemsNode__c,
                        EW_Quote__r.vlocity_ins__EffectiveDate__c, EW_Quote__r.OpportunityId, EW_Quote__r.AccountId, (
                        SELECT Id
                        FROM Attachments
                        WHERE Name = :COVERAGE_NODE
                        ORDER BY CreatedDate DESC
                        LIMIT 1
                )
                FROM EW_QuoteJSONData__c
                WHERE EW_Quote__c IN :quotesWithUpdates.keySet()
        ]));

        for (EW_QuoteJSONData__c qJSONData : quoteJSONData) {
            Map<String, Object> updates = quotesWithUpdates.get(qJSONData.EW_Quote__c);

            Map<String, Object> outraNode = (Map<String, Object>) JSON.deserializeUntyped(qJSONData.EW_OutraNode__c);
            Map<String, Object> userInputsNode = (Map<String, Object>) JSON.deserializeUntyped(qJSONData.EW_RepricingUserInputsNode__c);
            Map<String, Object> insuredItemsNode = (Map<String, Object>) JSON.deserializeUntyped(qJSONData.EW_RepricingInsuredItemsNode__c);
            Map<String, Object> insuredItemsPropertyNode = (Map<String, Object>) insuredItemsNode.get('insuredProperty');

            for (String fieldName : updates.keySet()) {
                String jsonElementName = outraFieldMapping.get(fieldName);
                outraNode.put(jsonElementName, updates.get(fieldName));

                for (String uiElement : userInputsFieldMapping.get(jsonElementName)) {
                    userInputsNode.put(uiElement, updates.get(fieldName));
                }

                for (String uiElement : insuredItemPropertyMapping.get(jsonElementName)) {
                    insuredItemsPropertyNode.put(uiElement, updates.get(fieldName));
                }

                if (jsonElementName == YEAR_BUILD_KEY_NAME) {
                    Integer ageRange = Date.today().year() - Integer.valueOf(updates.get(fieldName));
                    for (String uiElement : userInputsFieldMapping.get(AGE_RANGE_KEY_NAME)) {
                        userInputsNode.put(uiElement, (Object) ageRange);
                    }

                    for (String uiElement : insuredItemPropertyMapping.get(AGE_RANGE_KEY_NAME)) {
                        insuredItemsPropertyNode.put(uiElement, (Object) ageRange);
                    }
                }
            }

            insuredItemsNode.put('insuredProperty', insuredItemsPropertyNode);

            qJSONData.EW_OutraNode__c = JSON.serializePretty(outraNode);
            qJSONData.EW_RepricingUserInputsNode__c = JSON.serializePretty(userInputsNode);
            qJSONData.EW_RepricingInsuredItemsNode__c = JSON.serializePretty(insuredItemsNode);
        }

        update quoteJSONData;

        for (EW_QuoteJSONData__c qJSONData : quoteJSONData) {
            updatePricing(qJSONData.Id);
        }
    }

    public static List<EW_QuoteJSONData__c> filterQuoteJSONDataRecords(List<EW_QuoteJSONData__c> inputList) {
        List<EW_QuoteJSONData__c> result = new List<EW_QuoteJSONData__c>();

        for (EW_QuoteJSONData__c record : inputList) {
            if (record.EW_OutraNode__c != null &&
                    record.EW_RepricingUserInputsNode__c != null &&
                    record.EW_RepricingInsuredItemsNode__c != null) {
                result.add(record);
            }
        }

        return result;
    }

    @future
    public static void updatePricing(Id qJSONDataId) {
        EW_QuoteJSONData__c qJSONData = [
                SELECT Id, EW_Quote__r.vlocity_ins__EffectiveDate__c, EW_RepricingInsuredItemsNode__c, EW_RepricingUserInputsNode__c,
                        EW_Quote__c, EW_Quote__r.AccountId, EW_Quote__r.OpportunityId, EW_CoverageNode__c, (SELECT Id FROM Attachments)
                FROM EW_QuoteJSONData__c
                WHERE Id = :qJSONDataId
        ];

        Map<String, Object> inputMap = getRepriceQuoteInputMap(qJSONData);
        Map<String, Object> optionsMap = new Map<String, Object>();

        if (!Test.isRunningTest()) {
            Object response = vlocity_ins.IntegrationProcedureService.runIntegrationService('EW_QuoteProductRepricing', inputMap, optionsMap);
        }
    }

    public static Map<String, Object> getRepriceQuoteInputMap(EW_QuoteJSONData__c qJSONData) {
        Map<String, Object> inputMap = new Map<String, Object>();
        String effectiveDate =
                Datetime.newInstanceGmt(
                        qJSONData.EW_Quote__r.vlocity_ins__EffectiveDate__c,
                        Time.newInstance(0, 0, 0, 0)
                ).format('yyyy-MM-dd');

        Object insuredItems = JSON.deserializeUntyped(qJSONData.EW_RepricingInsuredItemsNode__c);
        Object userInputs = JSON.deserializeUntyped(qJSONData.EW_RepricingUserInputsNode__c);
        Map<String, Object> coverageNode = (Map<String, Object>) getCoverageNode(qJSONData);

        Map<String, Object> records = new Map<String, Object>();
        records.put('records', coverageNode.get(CONFIGURE_POLICY));

        inputMap.put('userInputs', userInputs);
        inputMap.put('selectedProduct', records);

        inputMap.put('quoteId', qJSONData.EW_Quote__c);
        inputMap.put('effectiveDate', effectiveDate);
        inputMap.put('accountId', qJSONData.EW_Quote__r.AccountId);
        inputMap.put('opportunityId', qJSONData.EW_Quote__r.OpportunityId);
        inputMap.put('insuredItems', insuredItems);

        /** Useful to work with IP - copy/paste payload, suitable for testing **/
        //Attachment testPayload = new Attachment();
        //testPayload.Body = Blob.valueOf(JSON.serializePretty(inputMap));
        //testPayload.Name = 'testPayload';
        //testPayload.ParentId = qJSONData.Id;
        //insert testPayload;

        return inputMap;
    }

    public static void mapJsonFieldsToSFFields(List<SObject> items, Map<String, String> jsonToApiMapping) {
        mapJsonFieldsToSFFields(items, EWConstants.VLOCITY_ATTRIBUTE_SEL_VALS_JSON_FIELD_API_NAME, jsonToApiMapping, false);
    }

    public static void mapJsonFieldsToSFFields(List<SObject> items, String jsonFieldName, Map<String, String> jsonToApiMapping, Boolean doDml) {
        List<SObject> toUpsert = new List<SObject>();

        for (SObject obj : items) {
            Object jsonFieldValue = obj.get(jsonFieldName);
            if (jsonFieldValue != null && String.isNotBlank(String.valueOf(jsonFieldValue))) {
                Map<String, Object> jsonAttrMap = (Map<String, Object>) JSON.deserializeUntyped(String.valueOf(jsonFieldValue));

                for (String jsonFieldKey : jsonToApiMapping.keySet()) {
                    if (jsonAttrMap.containsKey(jsonFieldKey)) {
                        DisplayType fieldType = obj.getSObjectType().getDescribe().fields.getMap().get(jsonToApiMapping.get(jsonFieldKey)).getDescribe().getType();
                        if (fieldType == DisplayType.STRING) {
                            obj.put(jsonToApiMapping.get(jsonFieldKey), String.valueOf(jsonAttrMap.get(jsonFieldKey)));
                        } else {
                            obj.put(jsonToApiMapping.get(jsonFieldKey), jsonAttrMap.get(jsonFieldKey));
                        }

                    }
                }

                if (doDml) {
                    toUpsert.add(obj);
                }
            }
        }

        if (doDml) {
            List<SObject> toInsert = new List<SObject>();
            List<SObject> toUpdate = new List<SObject>();
            for (SObject obj : toUpsert) {
                if (String.isEmpty(obj.Id)) {
                    toInsert.add(obj);
                } else {
                    toUpdate.add(obj);
                }
            }
            insert toInsert;
            update toUpdate;
        }
    }


    public static void resetRecordValues(List<SObject> newItems, Map<String, String> jsonToApiMap){

        // reset the currency values in 'newItems' based on the jsonToAPImap
        for (SObject obj : newItems) {
            for (String fieldName : jsonToAPImap.values()) {
                if(!String.isBlank(fieldName)){
                    DisplayType fieldType = obj.getSObjectType().getDescribe().fields.getMap().get(fieldName).getDescribe().getType();
                    if (fieldType == DisplayType.CURRENCY) {
                        obj.put(fieldName, 0);
                    }
                }
            }
        }
    }


    /**
    * @methodName: getJsonValue
    * @returns: returns the value of a node from a JSON string
    * @author: Roy Lloyd
    * @date: 14/2/2020
    */
    public static Object getJsonValue(String jsonString, String target){
        if(!jsonString.contains(target)) return null;

        Object returnValue;

        Map<String,Object> jsObj = (Map<String,Object>) JSON.deserializeUntyped(jsonString);
        for(String key : jsObj.keySet()){
            if(key == target){
                returnValue = jsObj.get(target);
            }
        }
        return returnValue;
    }


}