/*
* @name: BrokerIqRegistrationFormHelper
* @description: BrokerIqRegistrationForm helper
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 12/01/2018
* @modifiedBy: Ignacio Sarmiento    26/03/2018         BIQ-57 - Fix the bug to redirect to "Terms and Conditions" page 
* @modifiedBy: Ignacio Sarmiento    20/04/2018 		   BIQ-04 - Add spinner and disable button logic
*/
({
    setTermsOfUseURLHelper: function(component) {
        var termOfUserPartialURL = '/terms-of-use';
        var currentUrl = window.location.href;
        var currentUrlWithoutLoginOrSelfRegister = currentUrl.replace('/login/SelfRegister','');
        var termsOfUseURL = currentUrlWithoutLoginOrSelfRegister + termOfUserPartialURL;
        component.set("v.termsOfUseURL", termsOfUseURL);
    },

	handleRegisterHelper : function(component, event, helper) {
        //First actions when you click the button
        this.firstClickButtonActions(component);
        //Get form information
        var firstName = component.find("first_name").getElement().value;
        var lastName = component.find("last_name").getElement().value;
        var jobTitle = component.find("job_title").getElement().value;
        var company = component.find("company").getElement().value;
        var email = component.find("email").getElement().value;
        var phone = component.find("phone").getElement().value;
        var opt_in = component.find("opt_in").getElement().checked;
        var countryName='';
        if(component.find("country").getElement().value==='Other')
            countryName=component.find("otherCountry").getElement().value;
        else
            countryName =component.find("country").getElement().value ;                
        //set registration variables
        var actionRegisterBrokerIq2User = component.get("c.registerBrokerIq2User");
        actionRegisterBrokerIq2User.setParams({
            "firstName": firstName,
            "lastName": lastName,
            "jobTitle" : jobTitle,
            "company" : company,
            "email" : email,
            "phone" : phone,
            "opt_in": opt_in,
            "countryName": countryName
        });     
        //Registration action
        actionRegisterBrokerIq2User.setCallback(this, function(response){
            var state = response.getState();
            var reponseReturnValue = response.getReturnValue();
            if (state === "SUCCESS" && $A.util.isEmpty(reponseReturnValue) ) {
                this.redirectToConfirmationPage();
            }else {
                component.set("v.errorMessage", reponseReturnValue);
                component.set("v.showError", true);
            }
            //Last actions when you click the button
            this.lastClickButtonActions(component);
        });
        //Enqueue method
        $A.enqueueAction(actionRegisterBrokerIq2User);
	},

    firstClickButtonActions : function(component) {
    	this.disableRegisterButton(component);
    	this.enableSpinner(component);
	},
    
    lastClickButtonActions : function(component) {
    	this.disableSpinner(component);
        this.enableRegistrerButton(component);
	},
    
    enableRegistrerButton : function(component) {
        this.setRegisterButtonDisability(component, false);
    },
    
    disableRegisterButton : function(component) {
        this.setRegisterButtonDisability(component, true);
    },
    
    setRegisterButtonDisability: function(component, disableRegistrationButton) {
        component.find("registerButton").getElement().disabled = disableRegistrationButton;
    },
    
    enableSpinner : function(component) {
        this.setSpinnerEnability(component, true);
    },
    
    disableSpinner : function(component) {
        this.setSpinnerEnability(component, false);
    },
    
    setSpinnerEnability: function(component, enableRegisterButton) {
        component.set("v.toggleSpinner", enableRegisterButton); 
    },
    
    redirectToConfirmationPage : function() {
        var pageToRedirect = 'registerconfirmationpage';
        var urlToRedirect;
        //Set url to redirect
        var currentUrl = window.location.href;
        var lastElementCurrentUrl = currentUrl.substr(currentUrl.lastIndexOf('/') + 1) + '$';
    	urlToRedirect = currentUrl.replace(new RegExp(lastElementCurrentUrl), '');        
        urlToRedirect = urlToRedirect.slice(0, urlToRedirect.lastIndexOf('/'));
        urlToRedirect = urlToRedirect.slice(0, urlToRedirect.lastIndexOf('/'));
        urlToRedirect = urlToRedirect + '/' + pageToRedirect;
        //Redirect
		window.location.href =  urlToRedirect;
    },
    handleCountrySelectHelper: function(component,event,helper) {  
        var countrValue = component.find("country").getElement().value;
        if(countrValue==='Other')
            component.set('v.showOtherText',true);
        else
            component.set('v.showOtherText',false);                        
    }
})