public without sharing class PolicyHelper {

    public static final String AIG_POLICY_NUMBER = 'AIG_Policy_Number';
    public static final String TYPE_OF_TRANSACTION_MTA = 'MTA';
    public static final String EW_POLICY_STATUS_ADJUSTED = 'Adjusted';

    public static void setFormulaNumberField(List<SObject> newItems) {
        Set<String> rtDevNames = new Set<String>();
        Map<Id, RecordTypeInfo> rtMap = Asset.getSObjectType().getDescribe().getRecordTypeInfosById();
        Set<Id> parentIds = new Set<Id>();

        for (Asset policy : (List<Asset>) newItems) {
            if (policy.ParentId == null) {
                rtDevNames.add(rtMap.get(policy.RecordTypeId).getDeveloperName());
            }{
                parentIds.add(policy.ParentId);
            }
        }

        // for assigning policy number
        List<PolicyNumberRanges__c> policyNumberRanges = new List<PolicyNumberRanges__c>([
                SELECT Id, PolicyRecordTypeDevName__c, CurrentNumber__c, LengthWithZeros__c, PolicyNamePrefix__c
                FROM PolicyNumberRanges__c
                WHERE PolicyRecordTypeDevName__c IN :rtDevNames OR PolicyRecordTypeDevName__c = :AIG_POLICY_NUMBER
                FOR UPDATE
        ]);

        Map<String, PolicyNumberRanges__c> policyFormulaNumbersByRT = new Map<String, PolicyNumberRanges__c>();
        for (PolicyNumberRanges__c pnr : policyNumberRanges) {
            policyFormulaNumbersByRT.put(pnr.PolicyRecordTypeDevName__c, pnr);
        }

        // for using parent policy number
        Map<Id, Asset> parentPolicyMap = new Map<Id, Asset>([
                SELECT Id, Name
                FROM Asset
                WHERE Id IN:parentIds
        ]);

        // update triggering Policy records
        for (Asset policy : (List<Asset>) newItems) {
            String policyRtDevName = rtMap.get(policy.RecordTypeId).getDeveloperName();

            if (policyFormulaNumbersByRT.containsKey(policyRtDevName)) {
                // assign policy number

                policy.Name = getNextSequenceNum(policyFormulaNumbersByRT.get(policyRtDevName));
            } else if (parentPolicyMap.containsKey(policy.ParentId)) {
                // use parent policy number

                List<String> parentNameSplit = parentPolicyMap.get(policy.ParentId).Name.split(' ');
                policy.Name = parentNameSplit[0] + ' V' + policy.EW_Version_Number__c;

            }

            if (policyRtDevName == EWConstants.POLICY_EW_RECORD_TYPE_DEVNAME
                    && policyFormulaNumbersByRT.containsKey(AIG_POLICY_NUMBER)
                    && policy.EW_TypeofTransaction__c != TYPE_OF_TRANSACTION_MTA
                    && policy.Status != EW_POLICY_STATUS_ADJUSTED) {
                policy.EW_AIGPolicyNumber__c = getNextSequenceNum(policyFormulaNumbersByRT.get(AIG_POLICY_NUMBER));
            }
        }

        update policyNumberRanges;
    }

    private static String getNextSequenceNum(PolicyNumberRanges__c pnr) {
        pnr.CurrentNumber__c = pnr.CurrentNumber__c + 1;

        String policyFormulaNumber = String.valueOf(pnr.CurrentNumber__c);
        while (policyFormulaNumber.length() < pnr.LengthWithZeros__c) {
            policyFormulaNumber = '0' + policyFormulaNumber;
        }

        return pnr.PolicyNamePrefix__c + policyFormulaNumber;
    }
}