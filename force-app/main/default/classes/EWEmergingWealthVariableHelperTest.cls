/**
* @name: EWEmergingWealthVariableHelperTest
* @description: Test class for the class EWEmergingWealthVariableHelper which is used for Remote Action calls from Vlocity
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 26/10/2018
*/

@isTest
public with sharing class EWEmergingWealthVariableHelperTest {

    private static testMethod void testRemoteActionAccess(){

        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        Boolean check = raHelper.invokeMethod('defaultCoverageValues', new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
        System.assertEquals(true, check);

    }

    private static testMethod void testRetrievalOfRecords(){

        setupData();

        Test.startTest();

        Map<String,Object> outputMap = new Map<String,Object>();
        outputMap.putAll(EWEmergingWealthVariableHelper.getRecords());

        Test.stopTest();

        // confirms expected value retrieved
        System.assertEquals(31, outputMap.get('legalExpenses'));

        // confirms out-dated record not retrieved
        System.assertEquals(12, outputMap.size());
        System.assertEquals(0.12, outputMap.get('iptPercent'));

    }

    private static void setupData(){

        // Create EW_EmergingWealthVariable__c Records
        Map<String,Decimal> keyValueMap = new Map<String,Decimal>{
                'iptPercent' => 0.12,
                'personalCyberPremium' => 24.21,
                'personalCyberCoverageLimit' => 50000,
                'legalExpenses' => 31,
                'legalExpensesCoverageLimit' => 100000,
                'homeEmergencyPremium' => 8.7,
                'homeEmergencyCoverageLimit' => 1000,
                'azurPercentageCommission' => 0.12,
                'defaultExcess' => 500,
                'employersLiability' => 5000000,
                'publicLiability' => 10000000,
                'otherPermanentStructures' => 200000
        };
        List<EW_EmergingWealthVariable__c> emergingWealthVariables = new List<EW_EmergingWealthVariable__c>();
        for(String key : keyValueMap.keySet()){
            emergingWealthVariables.add(new EW_EmergingWealthVariable__c(Name = key, EW_Key__c = key, EW_Value__c = keyValueMap.get(key), EW_Effective_From__c = Date.today()));
        }
        // add out-dated record
        emergingWealthVariables.add(new EW_EmergingWealthVariable__c(Name = 'iptPercent', EW_Key__c = 'iptPercent', EW_Value__c = 0.14, EW_Effective_From__c = Date.today().addDays(-7), EW_Effective_To__c = Date.today().addDays(-1) ));

        insert emergingWealthVariables;

    }


}