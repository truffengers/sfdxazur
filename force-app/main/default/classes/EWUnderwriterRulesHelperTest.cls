/**
* @name: EWUnderwriterRulesHelperTest
* @description: Test class for EWUnderwriterRulesHelper
* @author: Roy Lloyd, rlloyd@azuruw.com
* @date: 01/02/2019
*/

@isTest
private class EWUnderwriterRulesHelperTest {

    private static User underwriterUser;
    private static List<vlocity_ins__ProductRequirement__c> uwRules;
    private static Quote testQuote;
    private static Product2 product;

    static{

        Profile uwProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_AZURUNDERWRITER_NAME);
        underwriterUser = EWUserDataFactory.createUser('underWriter', uwProfile.Id, NULL, true);

        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr','Test', 'Insured', Date.newInstance(1969, 12, 29), true);

        System.runAs(underwriterUser) {
            Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            testQuote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
        }

        vlocity_ins__VlocityStateModel__c stateModel = EWProductDataFactory.createStateModel('EW Quote Approval');
        insert stateModel;

        vlocity_ins__VlocityStateModelVersion__c stateModelVersion = EWProductDataFactory.createStateModelVerion(stateModel.Id, 'EW Quote Approval', 1);
        insert stateModelVersion;

        List<vlocity_ins__VlocityStateTransition__c> transitions = new List<vlocity_ins__VlocityStateTransition__c>();
        transitions.add(EWProductDataFactory.createStateTransition('Draft>Declined', stateModelVersion.Id));
        transitions.add(EWProductDataFactory.createStateTransition('Draft>UW Referred', stateModelVersion.Id));
        insert transitions;

        product = EWProductDataFactory.createProduct(null, true);

        uwRules = new List<vlocity_ins__ProductRequirement__c>();
        uwRules.add(EWProductDataFactory.createRule(product.Id, transitions[0].Id, stateModelVersion.Id, 'insuredEntity.occupationGroup == 14' , 'The risk has been declined due to client details.'));
        uwRules.add(EWProductDataFactory.createRule(product.Id, transitions[1].Id, stateModelVersion.Id, 'insuredEntity.occupationGroup > 10' , 'The risk has declined due to client details.'));
        uwRules.add(EWProductDataFactory.createRule(product.Id, transitions[1].Id, stateModelVersion.Id, 'insuredEntity.phAge > 18 && insuredEntity.phAge < 26' , 'The risk has referred due to client details.'));
        insert uwRules;

    }


    private static void postUnderwritingJson(String inputJson, Boolean startTest){

        Map<String, Object> inputMap = (Map<String, Object>) JSON.deserializeUntyped(inputJson);

        if(startTest) Test.startTest();
        EWVlocityRemoteActionHelper raHelper = new EWVlocityRemoteActionHelper();
        raHelper.invokeMethod('postUnderwritingRules', inputMap, new Map<String, Object>(), new Map<String, Object>());
        if(startTest) Test.stopTest();

    }

    private static testMethod void testUnderwriterDeclineAndRefer(){

        String inputJson = '{"uwRules":{"declined":{"Quote":[{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>Declined","productId":"'+product.id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has been declined due to client details.","conditions":"insuredEntity.occupationGroup == 14","requirement name":"Occupation Group 14"},"actionResults":{"responseType":"JSON","error":true,"response":[]}}]},"referred":{"Quote":[{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>UW Referred","productId":"'+product.Id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has declined due to client details.","conditions":"insuredEntity.occupationGroup > 10","requirement name":"Occupation Group Over 10"},"actionResults":{"responseType":"JSON","error":true,"response":[]}},{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>UW Referred","productId":"'+product.Id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has referred due to client details.","conditions":"insuredEntity.phAge > 18 && insuredEntity.phAge < 26","requirement name":"Primary Ins Under 25"},"actionResults":{"responseType":"JSON","error":true,"response":[]}}]}}}';
        postUnderwritingJson(inputJson, true);
        System.assertEquals(3, [SELECT Count() FROM EW_Underwriting_Rule__c WHERE EW_Quote__c = :testQuote.Id]);

    }

    private static testMethod void testUnderwriterDecline(){

        String inputJson = '{"uwRules":{"declined":{"Quote":[{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>Declined","productId":"'+product.id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has been declined due to client details.","conditions":"insuredEntity.occupationGroup == 14","requirement name":"Occupation Group 14"},"actionResults":{"responseType":"JSON","error":true,"response":[]}}]}}}';
        postUnderwritingJson(inputJson, true);
        System.assertEquals(1, [SELECT Count() FROM EW_Underwriting_Rule__c WHERE EW_Quote__c = :testQuote.Id]);

    }

    private static testMethod void testUnderwriterRefer(){

        String inputJson = '{"uwRules":{"referred":{"Quote":[{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>UW Referred","productId":"'+product.Id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has declined due to client details.","conditions":"insuredEntity.occupationGroup > 10","requirement name":"Occupation Group Over 10"},"actionResults":{"responseType":"JSON","error":true,"response":[]}},{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>UW Referred","productId":"'+product.Id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has referred due to client details.","conditions":"insuredEntity.phAge > 18 && insuredEntity.phAge < 26","requirement name":"Primary Ins Under 25"},"actionResults":{"responseType":"JSON","error":true,"response":[]}}]}}}';
        postUnderwritingJson(inputJson, true);
        System.assertEquals(2, [SELECT Count() FROM EW_Underwriting_Rule__c WHERE EW_Quote__c = :testQuote.Id]);

    }

    private static testMethod void testNoUnderwriterError(){

        String inputJson = '{"uwRules":{}}';
        postUnderwritingJson(inputJson, true);
        System.assertEquals(0, [SELECT Count() FROM EW_Underwriting_Rule__c WHERE EW_Quote__c = :testQuote.Id]);

    }

    private static testMethod void testUpdateUWrules(){

        // create underwriting rule record
        String inputJson = '{"uwRules":{"referred":{"Quote":[{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>UW Referred","productId":"'+product.Id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has declined due to client details.","conditions":"insuredEntity.occupationGroup > 10","requirement name":"Occupation Group Over 10"},"actionResults":{"responseType":"JSON","error":true,"response":[]}},{"ruleDetails":{"objectId":"'+testQuote.Id+'","transitionName":"Draft>UW Referred","productId":"'+product.Id+'","action class":"DROpenImplementationClass","action method":"ReferToUnderwriting","message":"The risk has referred due to client details.","conditions":"insuredEntity.phAge > 18 && insuredEntity.phAge < 26","requirement name":"Primary Ins Under 25"},"actionResults":{"responseType":"JSON","error":true,"response":[]}}]}}}';
        postUnderwritingJson(inputJson, false);

        // confirm ready for test
        System.assertNotEquals(EWConstants.QUOTE_STATUS_UW_APPROVED, [SELECT Status FROM Quote WHERE Id = :testQuote.Id].Status);

        Test.startTest();
        System.runAs(underwriterUser) {
            testQuote.Status = EWConstants.QUOTE_STATUS_UW_APPROVED;
            update testQuote;
        }
        Test.stopTest();
        System.assertEquals(2, [SELECT Count() FROM EW_Underwriting_Rule__c WHERE EW_Quote__c = :testQuote.Id AND EW_Approved_By__c = :underwriterUser.Id]);

    }


}