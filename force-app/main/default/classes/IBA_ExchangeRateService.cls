/**
* @name: IBA_ExchangeRateService
* @description: This class contains business logic used in trigger for c2g__codaExchangeRate__c object.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 20/12/2019
*/
public with sharing class IBA_ExchangeRateService {

    private static final String COMPANY_NAME_IBA_TRUST_ACCOUNTS = 'IBA Trust Accounts';

    /**
    * @methodName: populateExchangeRateOnPolicyTransactions
    * @description: Populates Exchange Rate on matching Policy Transaction records.
    * @dateCreated: 02/01/2020
    */
    public static void populateExchangeRateOnPolicyTransactions(Set<Id> newIds) {
        List<c2g__codaExchangeRate__c> exchangeRates = [
                SELECT c2g__Rate__c,
                        c2g__StartDate__c,
                        c2g__ExchangeRateCurrency__r.Name
                FROM c2g__codaExchangeRate__c
                WHERE Id IN :newIds
                AND c2g__ExchangeRateCurrency__r.c2g__OwnerCompany__r.Name = :COMPANY_NAME_IBA_TRUST_ACCOUNTS
        ];

        Set<String> currencies = new Set<String>();

        for (c2g__codaExchangeRate__c exchangeRate : exchangeRates) {
            currencies.add(exchangeRate.c2g__ExchangeRateCurrency__r.Name);
        }

        List<IBA_PolicyTransaction__c> transactionsToUpdate = new List<IBA_PolicyTransaction__c>();
        for (IBA_PolicyTransaction__c policyTransaction : [
                SELECT IBA_TransactionCurrency__c,
                        IBA_PeriodCalc__c
                FROM IBA_PolicyTransaction__c
                WHERE IBA_ExchangeRate__c = NULL
                AND IBA_TransactionCurrency__c IN :currencies
                AND IBA_PeriodCalc__c != NULL
        ]) {
            if (policyTransaction.IBA_PeriodCalc__c.length() == 8) {
                Date matchingStartDate = Date.newInstance(Integer.valueOf(policyTransaction.IBA_PeriodCalc__c.left(4)), Integer.valueOf(policyTransaction.IBA_PeriodCalc__c.right(3)), 1);
                for (c2g__codaExchangeRate__c exchangeRate : exchangeRates) {
                    if (matchingStartDate == exchangeRate.c2g__StartDate__c
                            && policyTransaction.IBA_TransactionCurrency__c.equals(exchangeRate.c2g__ExchangeRateCurrency__r.Name)) {
                        policyTransaction.IBA_ExchangeRate__c = exchangeRate.c2g__Rate__c;
                        transactionsToUpdate.add(policyTransaction);
                        break;
                    }
                }
            }
        }

        update transactionsToUpdate;
    }

}