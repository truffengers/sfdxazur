/*
* @name: BrokerIqSharingController
* @description: controller for VF Page for getting the PDF for IQContent
* @author: Rajni Bajpai
* @date: 09/May/2021

************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1             Rajni Bajpai         06 May 2021         EW-Download IQcontent PDF in Community
************************************************************************************************
*/

public without sharing class BrokerIqSharingController {
    
    public String PDFData {get; set;} 
    public IQ_Content__c Qcontent {get; set;}     
    
    public BrokerIqSharingController(){
        String recordId = ApexPages.currentPage().getParameters().get('recordId');
        Qcontent=[select id,Content__c,Name from IQ_Content__c where id=:recordId limit 1];
        PDFData=Qcontent.Content__c;
    }
    
    @AuraEnabled
    public static Map<String,String> getSharingLinkFor(Id iqContentId) {
        Map<String,String> genericMap=new Map<String,String>();
        PageReference rval = Page.BrokerIqShare;
        rval.getParameters().put('id', iqContentId);
        genericMap.put('url',(Site.getBaseSecureUrl() + rval.getUrl()).replace('/s/', '/'));
        IQ_Content__c Qcontent=[select id,Content__c,Name,RecordType.Name from IQ_Content__c where id=:iqContentId limit 1];
        genericMap.put('Name',Qcontent.Name);
        genericMap.put('RecordTypeName',Qcontent.RecordType.Name);            
        
        return genericMap;
    }
    @AuraEnabled
    public static String getURL(Id iqContentId) {
        return (Site.getBaseSecureUrl() + '/apex/BrokerIqDownloadArticle?recordId='+iqContentId).replace('/s/', '/');
    }
}