/**
* @name: BrokerIqFurtherWebinarsController
* @description: BrokerIqFurtherWebinars apex controller
* @author: Nebula Consulting 
* @date: 14/10/2017
* @Modified: Ignacio Sarmiento	14/12/2017	Get records on behalf of 'BrokerIqIQContentUtil' class
*/
public with sharing class BrokerIqFurtherWebinarsController {
    private static final Integer LIMIT_QUERY_RECORDS = 3;
    
	public class Result {
		@AuraEnabled
		public List<IQ_Content__c> upcomingWebinars {get; set;}
		@AuraEnabled
		public List<IQ_Content__c> recentWebinars {get; set;}
	}

	@AuraEnabled
	public static Result getFurtherWebinars(String existingId) {
		Result rval = new Result();
        List<Id> iqContentIds = new List<Id>{existingId};
		rval.upcomingWebinars = BrokerIqIQContentUtil.getUpcomingWebinars(new List<Id>(), iqContentIds, LIMIT_QUERY_RECORDS);
		rval.recentWebinars = BrokerIqIQContentUtil.getRecentWebinars(new List<Id>(), iqContentIds, LIMIT_QUERY_RECORDS);
		return rval;
	}
}