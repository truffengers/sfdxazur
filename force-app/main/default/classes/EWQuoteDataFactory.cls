/**
* @name: EWQuoteDataFactory
* @description: Reusable factory methods for quote object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 05/09/2018
*/
@isTest
public class EWQuoteDataFactory {

    public static Quote createQuote(String name,
                                    Id oppId,
                                    Id brokerId,
                                    Boolean insertRecord) {

        return createQuote(name,oppId,brokerId,null,insertRecord);
    }

    public static Quote createQuote(String name,
            Id oppId,
            Id brokerId,
            Id priceBookId,
            Boolean insertRecord) {

        Id quoteRecId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get(EWConstants.QUOTE_EW_RECORD_TYPE).getRecordTypeId();
        Quote quote = new Quote();
        quote.RecordTypeId = quoteRecId;
        quote.Name = (name == null) ? 'testQuote' : name;
        quote.OpportunityId = oppId;
        quote.vlocity_ins__AgencyBrokerageId__c = brokerId;
        quote.Pricebook2Id = priceBookId;
        quote.EW_CoverageType__c = 'Buildings & Contents';
        quote.vlocity_ins__EffectiveDate__c = System.today().addDays(1);

        if (insertRecord) {
            insert quote;
        }

        return quote;
    }

    public static QuoteLineItem createQuoteLineItem(
        Id quoteId,
        Id pbeId,
        Id productId,
        Decimal unitPrice,
        Boolean insertRecord
    ){

        QuoteLineItem qli = new QuoteLineItem();
        qli.QuoteId = quoteId;
        qli.PricebookEntryId = pbeId;
        qli.Quantity = 1;
        qli.UnitPrice = unitPrice;
        qli.Product2Id = productId;

        if (insertRecord) {
            insert qli;
        }
        return qli;
    }

}