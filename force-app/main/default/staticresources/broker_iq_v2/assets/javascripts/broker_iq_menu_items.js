TT.brokerIqMenuItems = (function($) {

  'use strict';

  var $body;
  var $menu;
  var $items;
  var menu;
  var menuSelector;
  var itemClass;
  var linkClass;
  var subLinkClass;
  var tabletStart;
  var desktopStart;
  var item;
  var items;

  var ON  = 'on';
  var OFF = 'off';

  var closeTimerId;
  var openTimerId;

  var currentIndex;
  var expandedIndex;

  var OPEN_DELAY  = 200;
  var CLOSE_DELAY = 400;



  function referenceElements() {
    $body  = $('body');
    $menu  = $(menuSelector);
    $items = $menu.find('.' + itemClass);
  }

  function initItems() {
    items = {};
    $items.each(function(index, element) {
      items[index] = item($, {
        id            : index,
        isMouseInput  : getIsMousePrimaryInput(),
        enterCallback : enterCallback,
        leaveCallback : leaveCallback,
        clickCallback : clickCallback,
        touchCallback : touchCallback,
        element       : element
      });
      items[index].init({ linkClass: linkClass });
      if(items[index].getIsExpanded()) { expandedIndex = index; }
    });
  }

  function turnListeners(onOrOff) {
    $body[onOrOff]('touchstart', touchOutsideHandler);
  }

  function enterCallback(event) {
    var newIndex = event.data.id;
    if(newIndex === currentIndex) {
      clearCloseTimer();
    } else if(!event.data.isExpandable) {
      if(currentIndex !== undefined) {
        clearTimers();
        conceal();
        currentIndex = newIndex;
      }
    } else if(currentIndex === undefined) {
      currentIndex = newIndex;
      startOpenTimer();
    } else {
      clearTimers();
      conceal();
      currentIndex = newIndex;
      reveal();
    }
  }

  function leaveCallback(event) {
    clearCloseTimer();
    if(event.data.isExpandable) {
      startCloseTimer();
    } else {
      currentIndex = undefined;
    }
  }

  function getIsMousePrimaryInput() {
    return detectIt.primaryInput === 'mouse';
  }

  function getIsTablet() {
    return !getIsMousePrimaryInput() && window.matchMedia('(min-width: ' + tabletStart + 'px) and (max-width: ' +  (desktopStart) + 'px)').matches;
  }

  function getIsMobileSized() {
    return window.matchMedia('(max-width: ' + (tabletStart-1) + 'px)').matches;
  }

  function shouldPreventDefault(event) {
    return event.data.isExpandable && (getIsMobileSized() || getIsTablet()) && !$(event.target).hasClass(subLinkClass);
  }

  function contract(expandedIndex) {
    items[expandedIndex].contract();
  }

  function clickCallback(event) {
    if(shouldPreventDefault(event)) { event.preventDefault(); }
    if(!$(event.target).hasClass(linkClass)) { return; }
    var newExpandedIndex = event.data.id;
    if(newExpandedIndex === expandedIndex) {
      contract(expandedIndex);
      expandedIndex = undefined;
    } else {
      if(expandedIndex !== undefined) { contract(expandedIndex); }
      expandedIndex = newExpandedIndex;
      items[expandedIndex].expand();
    }
  }

  function touchCallback(event) {
    var newIndex = event.data.id;
    event.stopPropagation();
    if(newIndex === currentIndex && $(event.target).hasClass(linkClass)) {
      conceal();
    } else {
      if(currentIndex !== undefined) { conceal(); }
      currentIndex = newIndex;
      reveal();
    }
  }

  function touchOutsideHandler(event) {
    if(currentIndex !== undefined && !$menu.is(event.target) && $menu.has(event.target).length === 0) {
      conceal();
    }
  }

  function startCloseTimer() {
    closeTimerId = window.setTimeout(conceal, CLOSE_DELAY);
  }

  function startOpenTimer() {
    openTimerId = window.setTimeout(reveal, OPEN_DELAY);
  }

  function clearCloseTimer() {
    if(closeTimerId !== undefined) {
      window.clearTimeout(closeTimerId);
      closeTimerId = undefined;
    }
  }

  function clearOpenTimer() {
    if(openTimerId !== undefined) {
      window.clearTimeout(openTimerId);
      openTimerId = undefined;
    }
  }

  function clearTimers() {
    clearCloseTimer();
    clearOpenTimer();
  }

  function conceal() {
    items[currentIndex].conceal();
    currentIndex = undefined;
  }

  function reveal() {
    items[currentIndex].reveal();
  }

  function teardown() {
    if($body) {
      turnListeners(OFF);
      clearCloseTimer();
      clearOpenTimer();
      $.each(items, function() { this.teardown(); });
      $body         = undefined;
      $menu         = undefined;
      $items        = undefined;
      menu          = undefined;
      menuSelector  = undefined;
      itemClass     = undefined;
      linkClass     = undefined;
      subLinkClass  = undefined;
      tabletStart   = undefined;
      desktopStart  = undefined;
      detectIt      = undefined;
      item          = undefined;
      items         = undefined;
      currentIndex  = undefined;
      expandedIndex = undefined;
    }
  }

  function init(params) {
    teardown();
    menu         = params.menu;
    item         = params.item;
    menuSelector = params.menuSelector;
    itemClass    = params.itemClass;
    linkClass    = params.linkClass;
    subLinkClass = params.subLinkClass;
    tabletStart  = params.tabletStart;
    desktopStart = params.desktopStart;
    detectIt     = params.detectIt;
    referenceElements();
    initItems();
    turnListeners(ON);
  }

  return {
    init: init
  };

}(jQuery));
