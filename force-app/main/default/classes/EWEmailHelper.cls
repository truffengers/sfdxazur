/**
* @name: EWEmailHelper
* @description: Helper class for email
* @author: Steve Loftus sloftus@azuruw.com
* @date: 01/10/2018

************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1             Rajni Bajpai         24 March 2021     added method Policy
************************************************************************************************
*/
public without sharing class EWEmailHelper {
    public EWEmailHelper() {}

    public static EmailResults SendVlocityEmail(EmailOptions emailOptions) {


        EmailResults results = new EmailResults(false, null, null);

        if ((String.isBlank(emailOptions.toAddress) && String.isBlank(emailOptions.targetObjectId))
            || String.isBlank(emailOptions.templateName)) {
            return results;
        }

        List<Messaging.EmailFileAttachment> attachmentList = new  List<Messaging.EmailFileAttachment>();

        Id templateIdList = [SELECT Id
                             FROM EmailTemplate
                             WHERE DeveloperName = :emailOptions.templateName LIMIT 1].Id;

        Messaging.SingleEmailMessage semail = Messaging.renderStoredEmailTemplate(
                                                            templateIdList,
                                                            emailOptions.targetObjectId,
                                                            emailOptions.whatId);   

        String emailSubject = semail.getSubject();
        String emailHtmlBody = semail.getHtmlBody();
        Boolean saveAsActivity = emailOptions.saveAsActivity != null ? emailOptions.saveAsActivity : false;

        if (String.isNotBlank(emailOptions.toAddress)) {
            List<String> toAddressesList = new List<String>();
            toAddressesList.add(emailOptions.toAddress);
            semail.setToAddresses(toAddressesList);
        } else {
            semail.setTargetObjectId(emailOptions.targetObjectId);
        }
        semail.setSubject(emailSubject);
        semail.setHtmlBody(emailHtmlBody);
        semail.setSaveAsActivity(saveAsActivity);

        Id whatId = emailOptions.whatId;
        List<Attachment> attachments = new List<Attachment>();

        if (whatId.getSobjectType() == Schema.Quote.SObjectType) {
            // The whatId provided is a Quote Id (during Documents OS)
            List<Quote> quotes = [SELECT Id, EWQuoteDisplayNumber__c FROM Quote WHERE Id=:whatId];
            Quote quote = quotes[0];

            String displayNumberString = '%' + quote.EWQuoteDisplayNumber__c + '.pdf';
            attachments = [SELECT Id FROM Attachment WHERE ParentId = :quote.Id AND Name LIKE :displayNumberString ORDER BY CreatedDate DESC LIMIT 1];
        } else if (whatId.getSobjectType() == Schema.IBA_PolicyTransaction__c.SObjectType) {
            // The whatId provided is a Policy Transaction Id (during Policy Documents OS)
            List<IBA_PolicyTransaction__c> policyTransactions = [SELECT Id, IBA_OriginalPolicy__c FROM IBA_PolicyTransaction__c WHERE Id=:whatId];
            Id policyId = policyTransactions[0].IBA_OriginalPolicy__c;
            
            List<Asset> policies = [SELECT Id, Name FROM Asset WHERE Id=:policyId];
            Asset policy = policies[0];

            String displayNumberString = '%' + policy.Name + '.pdf';
            attachments = [SELECT Id FROM Attachment WHERE ParentId = :policy.Id AND Name LIKE :displayNumberString ORDER BY CreatedDate DESC LIMIT 1];
        } else if (whatId.getSobjectType() == Schema.Asset.SObjectType) {
            
            List<Asset> policies = [SELECT Id, Name FROM Asset WHERE Id=:whatId];
            Asset policy = policies[0];
            String displayNumberString = '%' + policy.Name + '.pdf';
            attachments = [SELECT Id FROM Attachment WHERE ParentId = :policy.Id AND Name LIKE :displayNumberString ORDER BY CreatedDate DESC LIMIT 1];
        }

        List<Id> attachmentIdList = new List<Id>();

        for (Attachment a : attachments) {
            attachmentIdList.add(a.Id);
        }

        emailOptions.osAttachmentList = attachmentIdList;

        if (emailOptions.osAttachmentList != null && !emailOptions.osAttachmentList.isEmpty()) {

            for (Attachment att : [SELECT Id, Body, ContentType, Name
                                   FROM Attachment
                                   WHERE Id IN :emailOptions.osAttachmentList]) {

                Messaging.EmailFileAttachment ea = new Messaging.EmailFileAttachment();
                ea.setBody(att.Body);
                ea.setContentType(att.ContentType);
                ea.setFileName(att.Name);
                ea.setInline(false);
                attachmentList.add(ea);
            }           

            if (!attachmentList.isEmpty()) {

                semail.setFileAttachments(attachmentList) ;
                semail.setCharset('UTF-8');
                semail.setUseSignature(true);
            }            
        }

        EW_Global_Setting__mdt globalSettings = [SELECT EW_InsuranceEmailName__c
                                                 FROM EW_Global_Setting__mdt
                                                 WHERE QualifiedApiName = 'Default'
                                                 LIMIT 1];

        if (String.isNotBlank(globalSettings.EW_InsuranceEmailName__c)) {

            for (OrgWideEmailAddress owa : [SELECT Id, DisplayName FROM OrgWideEmailAddress]) {

                if (owa.DisplayName.contains(globalSettings.EW_InsuranceEmailName__c)) {

                    semail.setOrgWideEmailAddressId(owa.Id);
                }
            }
        }
        
        List<Messaging.SendEmailResult> res = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});

        results.isSent = res.get(0).isSuccess();

        if (!results.isSent) {

            results.statusCode = res.get(0).getErrors()[0].getStatusCode().ordinal();
            results.errorMessage = res.get(0).getErrors()[0].getMessage();
        }

        return results;
    }

    public class EmailOptions {

        public Id targetObjectId;
        public String templateName;
        public List<Id> osAttachmentList;
        public Boolean saveAsActivity;
        public Id whatId;
        public String toAddress;

        public EmailOptions() {}

        public EmailOptions(
        			Id targetObjectId,
        			String templateName,
        			List<Id> osAttachmentList,
        			Boolean saveAsActivity,
        			Id whatId) {

        	this.targetObjectId = targetObjectId;
        	this.templateName = templateName;
        	this.osAttachmentList = osAttachmentList;
        	this.saveAsActivity = saveAsActivity;
        	this.whatId = whatId;
        }
    }

    public class EmailResults {

        public Boolean isSent;
        public Integer statusCode;
        public String errorMessage;

        public EmailResults() {}

        public EmailResults(
                    Boolean isSent,
                    Integer statusCode,
                    String errorMessage) {

            this.isSent = isSent;
            this.statusCode = statusCode;
            this.errorMessage = errorMessage;
        }
    }

    @AuraEnabled()
    public static EmailResults SendBrokerRequestEmail(
                            String name, 
                            String role, 
                            String company, 
                            String location, 
                            String phone, 
                            String email) {

        EmailResults results = new EmailResults(false, null, null);

        if (String.isBlank(name)
            || String.isBlank(role)
            || String.isBlank(company)
            || String.isBlank(location)
            || String.isBlank(phone)
            || String.isBlank(email)) {
            return results;
        }

        List<EW_New_Broker_Requests__c> recipientEmails = [SELECT Id, SetupOwner.Email
                                                           FROM EW_New_Broker_Requests__c];
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        for (EW_New_Broker_Requests__c recipient : recipientEmails) {

            Messaging.SingleEmailMessage oneEmail = new Messaging.SingleEmailMessage();

            oneEmail.toAddresses = new String[] { recipient.SetupOwner.Email };

            oneEmail.subject = 'Broker Hub Sign Up Form Submission: ' + name;
            oneEmail.plainTextBody = 'Name: ' + name + '\n' +
                                     'Role: ' + role + '\n' +
                                     'Company: ' + company + '\n' +
                                     'Location: ' + location + '\n' +
                                     'Phone Number: ' + phone + '\n' +
                                     'Email: ' + email;

            emails.add(oneEmail);
        }

        List<Messaging.SendEmailResult> res = Messaging.sendEmail(emails);

        System.debug(res.size());
        results.isSent = res != null && res.size() > 0 ? res.get(0).isSuccess() : false;

        if (!results.isSent) {

            results.statusCode = res.get(0).getErrors()[0].getStatusCode().ordinal();
            results.errorMessage = res.get(0).getErrors()[0].getMessage();
        }

        return results;
    }


    /**
    * @methodName: generateEmail
    * @description: utility method to create email message record
    * @author: Roy Lloyd
    * @date: 9/3/2020
    */
    public static Messaging.SingleEmailMessage generateEmail (Id targetId, List<String> listOfRecipients, Id fromId, Id whatId, Id templateId) {

        Messaging.SingleEmailMessage msg = Messaging.renderStoredEmailTemplate(
                templateId,
                targetId,
                whatId);

        msg.setTemplateId(templateId);
        msg.setToAddresses(listOfRecipients);
        msg.setSaveAsActivity(false);
        msg.setOrgWideEmailAddressId(fromId);
        msg.setTargetObjectId(targetId);

        return msg;
    }



}