/**
* @name: EWClauseAssignmentDataFactory
* @description: Reusable factory methods for clause assignment object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 05/02/2019
*/
@isTest
public with sharing class EWClauseAssignmentDataFactory {

	public static EW_ClauseAssignment__c createClauseAssignment(Id documentClauseId,
																Id quoteId,
																Id policyId,
	    									   					Boolean insertRecord) {

        EW_ClauseAssignment__c clauseAssignment = new EW_ClauseAssignment__c();
        clauseAssignment.EW_DocumentClause__c = documentClauseId;
		clauseAssignment.EW_Quote__c = quoteId;
		clauseAssignment.EW_Policy__c = policyId;
        
        if (insertRecord) {
			insert clauseAssignment;
		}

        return clauseAssignment;
    }
}