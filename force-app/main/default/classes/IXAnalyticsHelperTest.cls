@isTest
public class IXAnalyticsHelperTest {
    static testMethod void testCreateAnalyticsEvent(){
        String eventName = 'eventName';
        Integer testSize = 10;
        
        Test.startTest();
        for(integer i = 0; i < testSize; i++){
            IXAnalyticsHelper.createAnalyticsEvent(eventName + i, null, 'desc', 'eventData');
        }
        Test.stopTest();
        
		List<IX_analytics_event__c> events = [SELECT Id,Name FROM IX_analytics_event__c];
        
        for(integer i = 0; i < testSize; i++){
            IX_analytics_event__c e = events[i];
            System.assertEquals(eventName+i, e.Name);
        }
    }
    
    static testMethod void testGetAnalyticsEvents() {
        IX_analytics_event__c analyticsEvent = new IX_analytics_event__c(
            Name='Event Name',
            Session_ID__c='123abc',
            Event_Description__c='Event Description',
            Event_Data__c='Event Data'
        );
        insert analyticsEvent;
        
        Test.startTest();
        List<IX_analytics_event__c> events = IXAnalyticsHelper.getAnalyticsEvents(7);
        Test.stopTest();
        
        System.assertEquals(1, events.size());
    }
}