/**
 * Created by roystonlloyd on 2020-01-10.
 */

public with sharing class EWClauseAssignmentController {


    @AuraEnabled
    public static Map<String, Object> loadRecordData(Id id) {
        Map<String, Object> outputMap = new Map<String, Object>();
        if(id == null || id.getSobjectType() != Schema.Quote.SObjectType) return outputMap;

        Map<String, Clause> sortedClauses = new Map<String, Clause>();
        List<String> assignedClausesList = new List<String>();
        List<String> requiredClausesList = new List<String>();

        List<Clause> assignedClauses = getAssignedClauses(id);  // values
        List<Clause> availableClauses = getAvailableClauses(id);  // options

        // dedupe clauses by name, effectively overwriting new version of clauses with old versions
        for(Clause c : availableClauses){
            sortedClauses.put(c.label, c);
        }
        for(Clause c : assignedClauses){
            sortedClauses.put(c.label, c);
            assignedClausesList.add(c.value);
            if(c.required) requiredClausesList.add(c.value);
        }

        outputMap.put('options', sortedClauses.values());
        outputMap.put('values', assignedClausesList);
        outputMap.put('required', requiredClausesList);

        return outputMap;
    }

    @AuraEnabled
    public static Boolean saveSelectedAssignments(Map<String,Object> inputMap) {

        Boolean isSuccess = false;

        Savepoint sp = Database.setSavepoint();
        try {

            Id quoteId = (Id) inputMap.get('recordId');
            List<String> selectedValues = getStringList( (List<Object>) inputMap.get('selectedValues'));
            Set<String> selectedClauseIds = new Set<String>(selectedValues);

            // review existing selections, removing and adding new as appropriate
            Set<Id> currentClauseIds = new Set<Id>();
            List<EW_ClauseAssignment__c> removeClauseAssignments = new List<EW_ClauseAssignment__c>();
            List<EW_ClauseAssignment__c> upsertClauseAssignments = new List<EW_ClauseAssignment__c>();
            List<EW_ClauseAssignment__c> currentAssignments = [SELECT Id, EW_Active__c, EW_DocumentClause__c, EW_DocumentClause__r.vlocity_ins__Category__c, EW_DocumentClause__r.EW_Clause_Name__c, EW_DocumentClause__r.Name FROM EW_ClauseAssignment__c WHERE EW_Quote__c = :quoteId];
            Map<Id,Id> inactiveClauseAssignmentMap = new Map<Id,Id>();

            for (EW_ClauseAssignment__c ca : currentAssignments) {

                // check for active assignments
                if(ca.EW_Active__c){

                    // if current clause is no longer selected
                    if (!selectedClauseIds.contains(ca.EW_DocumentClause__c)){

                        if(ca.EW_DocumentClause__r.vlocity_ins__Category__c == EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED){
                            ca.EW_Active__c = false;
                            upsertClauseAssignments.add(ca);
                        }
                        else{
                            removeClauseAssignments.add(new EW_ClauseAssignment__c(Id = ca.Id));
                        }
                    }

                    // current clause still selected
                    else{
                        currentClauseIds.add(ca.EW_DocumentClause__c);
                    }

                // map the inactive assignments
                }else if(ca.EW_Active__c != true && ca.EW_DocumentClause__r.vlocity_ins__Category__c == EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED){
                    inactiveClauseAssignmentMap.put(ca.EW_DocumentClause__c, ca.id);
                }
            }

            // check through the selected clauses
            for (String selectedId : selectedClauseIds) {

                // reactivate inactive assignments
                if(inactiveClauseAssignmentMap.containsKey(selectedId)){

                    upsertClauseAssignments.add(new EW_ClauseAssignment__c(
                            Id = inactiveClauseAssignmentMap.get(selectedId),
                            EW_Active__c = true,
                            EW_Assigned_By_Underwriter__c = true
                    ));

                // create new assignment
                }else if (!currentClauseIds.contains(selectedId)){

                    upsertClauseAssignments.add(new EW_ClauseAssignment__c(
                            EW_Quote__c = quoteId,
                            EW_DocumentClause__c = selectedId,
                            EW_Active__c = true,
                            EW_Assigned_By_Underwriter__c = true
                    ));
                }

            }
            if (!removeClauseAssignments.isEmpty()) delete removeClauseAssignments;
            if (!upsertClauseAssignments.isEmpty()) upsert upsertClauseAssignments;

            isSuccess = true;

        }catch(Exception e){
            Database.rollback(sp);

            LogBuilder logsBuilder = new LogBuilder();
            Azur_Log__c log = logsBuilder.createLogWithCodeInfo(e, 'EW', 'EWClauseAssignmentController', 'saveSelectedAssignments');
            insert log;
        }
        return isSuccess;
    }


    @AuraEnabled
    public static Map<String, Object> saveNewClause(Map<String,Object> inputMap) {

        Id quoteId = (Id) inputMap.get('recordId');
        Map<String, Object> outputMap = new Map<String, Object>();
        if(quoteId == null || quoteId.getSobjectType() != Schema.Quote.SObjectType) return outputMap;

        Boolean isSuccess = false;
        Savepoint sp = Database.setSavepoint();
        ClauseAssignment ca;
        try {

            String inputJSON = JSON.serialize(inputMap);
            ca = (ClauseAssignment) JSON.deserialize(inputJSON, ClauseAssignment.class);

            vlocity_ins__DocumentClause__c docClause = new vlocity_ins__DocumentClause__c(
                    Name = ca.inputValues.name,
                    vlocity_ins__ClauseContent__c = ca.inputValues.content,
                    vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED
            );
            insert docClause;

            EW_ClauseAssignment__c newClauseAssignment = new EW_ClauseAssignment__c(
                EW_Quote__c = quoteId,
                EW_DocumentClause__c = docClause.Id,
                EW_Active__c = true,
                EW_Assigned_By_Underwriter__c = true
            );
            insert newClauseAssignment;

            ca.options.available.add(new Clause(docClause.Name, docClause.Id, false));
            ca.options.selected.add(docClause.Id);

            isSuccess = true;

        }catch(Exception e){
            Database.rollback(sp);

            LogBuilder logsBuilder = new LogBuilder();
            Azur_Log__c log = logsBuilder.createLogWithCodeInfo(e, 'EW', 'EWClauseAssignmentController', 'saveNewClause');
            insert log;
        }
        outputMap.put('newClauseSaved', isSuccess);

        if(isSuccess && ca != null){
            outputMap.put('options', ca.options.available);
            outputMap.put('values', ca.options.selected);
        }

        return outputMap;
    }


    private static List<Clause> getAssignedClauses(Id id) {

        List<Clause> clauses = new List<Clause>();
        for(vlocity_ins__DocumentClause__c dc : [
                SELECT Id, vlocity_ins__Category__c, Name
                FROM vlocity_ins__DocumentClause__c
                WHERE vlocity_ins__IsArchived__c = false AND Id IN (SELECT EW_DocumentClause__c FROM EW_ClauseAssignment__c WHERE EW_Quote__c =: id AND EW_Active__c = true)
        ]){
            clauses.add(new Clause(dc.Name, dc.Id, false));
        }
        return clauses;
    }

    @TestVisible
    private static List<Clause> getAvailableClauses(Id id) {

        List<Clause> clauses = new List<Clause>();
        Set<Id> inactiveClauseIds = new Set<Id>();
        for(EW_ClauseAssignment__c inactiveClause : [SELECT EW_DocumentClause__c FROM EW_ClauseAssignment__c WHERE EW_Quote__c =: id AND EW_Active__c = false]){
            inactiveClauseIds.add(inactiveClause.EW_DocumentClause__c);
        }

        List<vlocity_ins__DocumentClause__c> allStandardClauses = [
                SELECT Id, vlocity_ins__Category__c, Name, EW_Clause_Name__c, EW_Start_Date__c
                FROM vlocity_ins__DocumentClause__c
                WHERE (vlocity_ins__IsArchived__c = false AND vlocity_ins__Category__c = :EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD) OR Id IN :inactiveClauseIds
        ];

        for(vlocity_ins__DocumentClause__c dc : EWClauseAssignmentHelper.getAppropriateClauseVersions(id, allStandardClauses) ){
            clauses.add(new Clause(dc.Name, dc.Id, false));
        }
        return clauses;
    }

    private static List<String> getStringList(List<Object> objList){

        List<String> returnList = new List<String>();
        if(objList != null){
            for (Object o : objList) {
                returnList.add((String) o);
            }
        }
        return returnList;
    }

    public class Clause{
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String value {get; set;}
        @AuraEnabled public Boolean required {get; set;}

        public Clause(String l, String v, Boolean r){
            this.label = l;
            this.value = v;
            this.required = r;
        }
    }

    public class NewClause{
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public String content {get; set;}

    }

    @TestVisible
    private class ClauseAssignment {
        @TestVisible private NewClause inputValues;
        @TestVisible private ClauseOptions options;
    }

    @TestVisible
    private class ClauseOptions{
        @TestVisible private List<Clause> available;
        @TestVisible private List<String> selected;
    }


}