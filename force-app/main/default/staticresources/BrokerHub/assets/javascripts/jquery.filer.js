(function( $ ) {

  $.fn.filer = function() {

    var filers = [];

    function getInnerSelector(i, label) {
      return '#' + $(label || this).data('target-id');
    }

    function filer(element) {

      var ACTIVE_CLASS = 'is-active';

      var $menu          = $(element);
      var $labels        = $menu.find('.js-filer__label');
      var innerSelectors = $labels.map(getInnerSelector).toArray();
      var $inners        = $(innerSelectors.join(','));

      $labels.on('click', function(event) {
        event.preventDefault();
        var fileId = getInnerSelector(null, this);
        $labels.removeClass(ACTIVE_CLASS);
        $inners.removeClass(ACTIVE_CLASS);
        $(this).addClass(ACTIVE_CLASS);
        $(fileId).addClass(ACTIVE_CLASS);
      });

    }


    $(this).each(function() { filers.push(filer(this)); });

    return this;

  };

}( jQuery ));