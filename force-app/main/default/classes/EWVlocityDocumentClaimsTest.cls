/**
* @name: EWVlocityDocumentClaimsTest
* @description: Test class for EWVlocityDocumentClaims
* @author: Steve Loftus sloftus@azuruw.com
* @date: 19/12/2018
*/
@isTest
public with sharing class EWVlocityDocumentClaimsTest {

    private static User systemAdminUser;
    private static Account broker;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static Product2 product;
    private static vlocity_ins__InsuranceClaim__c claim;
    private static EWVlocityDocumentClaims remoteAction;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> outputMap;

    @isTest static void GetClaimsTableWithContentTest() {
        setup(true);

        Test.startTest();

        System.runAs(systemAdminUser) {
            remoteAction.invokeMethod(EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD,
            						  inputMap,
            						  outputMap,
            						  null);
        }

        Test.stopTest();

        String sectionContent = (String)outputMap.get('sectionContent');
        System.assertNotEquals(null, sectionContent, 'Should have some content');
        System.assert(sectionContent.startsWith('<p>'), 'Should start with <p>');
        System.assert(sectionContent.contains('Amount'), 'Should contain Amount');
    }
    
    @isTest static void GetClaimsTableWithoutContentTest() {
        setup(false);

        Test.startTest();

        System.runAs(systemAdminUser) {
            remoteAction.invokeMethod(EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD,
            						  inputMap,
            						  outputMap,
            						  null);
        }

        Test.stopTest();

        String sectionContent = (String)outputMap.get('sectionContent');
        System.assertNotEquals(null, sectionContent, 'Should have some content');
        System.assert(!sectionContent.contains('Amount'), 'Should not contain Amount');
    }
        
    static void setup(Boolean createClaim) {

    	remoteAction = new EWVlocityDocumentclaims();

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                                    EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                                    techteamUserRole.Id,
                                    true);
        
        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);

            insured = EWAccountDataFactory.createInsuredAccount(
                                                    'Mr',
                                                    'Test',
                                                    'Insured',
                                                    Date.newInstance(1969, 12, 29),
                                                    true);

            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);

            product = EWProductDataFactory.createProduct(null, true);

            if (createClaim) {
                claim = EWInsuranceClaimDataFactory.createInsuranceClaim(null,
                                                                         null,
                                                                         null,
                                                                         null,
                                                                         null,
                                                                         quote.Id,
                                                                         null,
                                                                         true);
            }

            Map<String, Object> contextData = new Map<String, Object>();
            contextData.put('contextId', quote.Id);

            Map<String, Object> documentData = new Map<String, Object>();
            documentData.put('contextData', contextData);

            String dataJSON = JSON.serialize(documentData);

            inputMap = new Map<String, Object>();
            inputMap.put('dataJSON', dataJSON);

            outputMap = new Map<String, Object>();
        }
    }
}