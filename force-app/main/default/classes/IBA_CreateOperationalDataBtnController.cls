public with sharing class IBA_CreateOperationalDataBtnController {

    private ApexPages.StandardSetController standardSetController;

    public IBA_CreateOperationalDataBtnController(ApexPages.StandardSetController standardSetController) {
        this.standardSetController = standardSetController;
    }

    public PageReference startOperationalDataCreationJob() {
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        List<AsyncApexJob> jobInfos = [SELECT Status, ApexClassID FROM AsyncApexJob
            WHERE (Status = 'Queued' OR Status = 'Preparing' OR Status = 'Processing') AND ApexClassID IN 
            (SELECT Id FROM ApexClass WHERE Name =:IBA_OperationalDataCreationJob.class.getName())];
        if(jobInfos.size() > 0) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Operational Data Creation job is already running.'));
            return null;
        } else if(countData() > settings.IBA_OperationalDataLimit__c) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Too many groups of staging records with the same policy number to process.'));
            return null;
        } else {
            System.enqueueJob(new IBA_OperationalDataCreationJob());
        }
        
        return new ApexPages.Action('{!List}').invoke();
    }

    private Integer countData() {
        AggregateResult[] groupedStagingRecords = [
            SELECT IBA_AIGPolicyNumber__c, count(Id) 
            FROM IBA_IBAStaging__c 
            WHERE IBA_Status__c = :IBA_UtilConst.STAGING_STATUS_READY_TO_PROCESS
            GROUP BY IBA_AIGPolicyNumber__c 
        ];
        return groupedStagingRecords.size();
    }
}