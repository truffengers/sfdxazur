global class BrokerIqContributorUpdateParent implements Nebula_Tools.AfterInsertI, Nebula_Tools.AfterUpdateI {
	global void handleAfterInsert(List<Broker_iQ_Contributor__c> newList) {
		Map<Id, Contact> contactsToUpdate = new Map<Id, Contact>();

		for(Broker_iQ_Contributor__c newContributor : newList) {
			if(contactsToUpdate.containsKey(newContributor.Contact__c)) {
				newContributor.addError(Label.BrokerIqContributorOnePerContact);
			} else {
				Contact toUpdate = new Contact(Id = newContributor.Contact__c, 
				                               Broker_iQ_Contributor__c = newContributor.Id);

				contactsToUpdate.put(toUpdate.Id, toUpdate);
			}
		}

		if(!contactsToUpdate.isEmpty()) {
			update contactsToUpdate.values();
		}
	}

	global void handleAfterUpdate(List<Broker_iQ_Contributor__c> oldList, List<Broker_iQ_Contributor__c> newList) {
		for(Integer i=0; i < newList.size(); i++) {
			Broker_iQ_Contributor__c newContributor = newList[i];

			if(oldList[i].Contact__c != newContributor.Contact__c) {
				newContributor.addError(Label.BrokerIqCannotReparentContributor);
			}
		}
	}
}