public class BrightTalkUser extends XmlRepresentableMap {
    
    public BrightTalkUser() {
        super(new List<String> {
                'realmUserId',
                'firstName',
                'lastName',
                'email',
                'timeZone',
                'companyName'},
             'user'); 
    }

    public BrightTalkUser(Contact c, User u) {
        this();

        if (u.UserType != 'Standard' && u.UserType != 'Guest') {
            data.put('firstName', c.FirstName);
            data.put('lastName', c.LastName);
            data.put('email', c.Email);
        } else if (u.UserType == 'Standard'){
            data.put('firstName', u.FirstName);
            data.put('lastName', u.LastName);
            data.put('email', u.Email);
        }
        data.put('realmUserId', c.Id);
        data.put('companyName', c.Account.Name);
        data.put('timeZone', u.TimeZoneSidKey);

    }

}