/**
* @name: PHO_ALPsTransactionAggregator
* @description: This class contains business logic to aggregate ALPs data on Policy Transactions.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 13/03/2020
*/
public class PHO_ALPsTransactionAggregator {

    private static Boolean only2019;
    private static final List<String> TRANSACTION_LINE_RELEVANT_FIELDS = new List<String> {
            'IBA_Product__c',
            'IBA_PolicyCarrierLine__r.IBA_CarrierAccount__c',
            'IBA_BrokerCommission__c',
            'IBA_CarrierFee__c',
            'IBA_ForeignTax__c',
            'IBA_GrossWrittenPremium__c',
            'IBA_InsurancePremiumTax__c',
            'IBA_MGACommission__c',
            'PHO_PHBrokerCommission__c',
            'PHO_PHCarrierFee__c',
            'PHO_PHForeignTax__c',
            'PHO_PHGrossWrittenPremium__c',
            'PHO_PHInsurancePremiumTax__c',
            'PHO_PHMGACommission__c',
            'PHO_ALPsBrokerCommission__c',
            'PHO_ALPsCarrierFee__c',
            'PHO_ALPsForeignTax__c',
            'PHO_ALPsGrossWrittenPremium__c',
            'PHO_ALPsInsurancePremiumTax__c',
            'PHO_ALPsMGACommission__c'
    };

    private static final Set<String> PT_CLASS_OF_BUSINESS_NOT_TO_AGGREGATE = new Set<String> {
            'Course of Construction',
            'Azur Emerging Wealth',
            'Classic DUA'
    };

    private static final String CONTAINS_ALPS_DATA_CONDITIONS =
            ' (PHO_PHBrokerCommission__c != NULL AND PHO_PHBrokerCommission__c != 0)' +
            ' OR (PHO_PHCarrierFee__c != NULL AND PHO_PHCarrierFee__c != 0)' +
            ' OR (PHO_PHForeignTax__c != NULL AND PHO_PHForeignTax__c != 0)' +
            ' OR (PHO_PHGrossWrittenPremium__c != NULL AND PHO_PHGrossWrittenPremium__c != 0)' +
            ' OR (PHO_PHInsurancePremiumTax__c != NULL AND PHO_PHInsurancePremiumTax__c != 0)' +
            ' OR (PHO_PHMGACommission__c != NULL AND PHO_PHMGACommission__c != 0)';


    static {
        only2019 = IBA_IBACustomSettings__c.getOrgDefaults().PHO_AggregateOnly2019ALPsData__c;
    }

    /**
    * @methodName: retrieveTransactionsToAggregate
    * @description: Retrieves Policy Transactions and their child Policy Transaction Lines which should be subject for aggregation.
    *               There are different criteria for retrieving 2019 data than 2020 onwards data.
    * @dateCreated: 13/03/2020
    */
    public static List<IBA_PolicyTransaction__c> retrieveTransactionsToAggregate(Integer recordLimit){
        String query =
                'SELECT PHO_ALPsAggregationSeed__c,' +
                        'PHO_PrimaryRecord__c,' +
                        ' (SELECT ' + String.join(TRANSACTION_LINE_RELEVANT_FIELDS, ',') +
                        ' FROM Policy_Transaction_Lines__r' +
                        addYearSpecificConditionsForTransactionLine() +
                        ')' +
                ' FROM IBA_PolicyTransaction__c' +
                ' WHERE PHO_AggregationProcessed__c = FALSE' +
                ' AND IBA_PolicyNumber__c != NULL' +
                ' AND IBA_ClassofBusiness__c NOT IN :PT_CLASS_OF_BUSINESS_NOT_TO_AGGREGATE' +
                addYearSpecificConditionsForTransaction() +
                ' ORDER BY IBA_PolicyNumber__c, IBA_EffectiveDate__c, IBA_AccountingBrokerID__c' +
                ' LIMIT :recordLimit';
        return Database.query(query);
    }

    /**
    * @methodName: aggregateTransactions
    * @description: Main method to perform aggregation process. Takes a list of Policy Transactions as an argument.
    * @dateCreated: 13/03/2020
    */
    public static void aggregateTransactions (List<IBA_PolicyTransaction__c> transactions) {
        // group transactions by seed
        Map<String, List<IBA_PolicyTransaction__c>> groupedTransactionsToAggregate = groupTransactionsToAggregate(transactions);
        // get already existing primary transactions
        Map<String, IBA_PolicyTransaction__c> primaryTransactionBySeed = retrievePrimaryTransactions(groupedTransactionsToAggregate.keySet());
        List<IBA_PolicyTransaction__c> transactionsToUpdate = new List<IBA_PolicyTransaction__c>();
        List<IBA_PolicyTransactionLine__c> transactionLinesToUpdate = new List<IBA_PolicyTransactionLine__c>();
        List<TransactionAndCarrierLineWrapper> newTransactionAndCarrierLines = new List<TransactionAndCarrierLineWrapper>();
        List<IBA_FFPolicyCarrierLine__c> newCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();
        // aggregate financial data
        for (String policyAggregationSeed : groupedTransactionsToAggregate.keySet()) {
            List<IBA_PolicyTransaction__c> transactionsToAggregate = groupedTransactionsToAggregate.get(policyAggregationSeed);
            IBA_PolicyTransaction__c primaryTransaction = primaryTransactionBySeed.get(policyAggregationSeed);
            if (primaryTransaction == null) {
                // make one of the transactions primary
                primaryTransaction = transactionsToAggregate.remove(0);
                primaryTransaction.PHO_PrimaryRecord__c = true;
                primaryTransaction.PHO_AggregationProcessed__c = true;
                populateInitialAggregationData(primaryTransaction);
            }
            // add primary transaction lines to list to update
            transactionsToUpdate.add(primaryTransaction);
            transactionLinesToUpdate.addAll(primaryTransaction.Policy_Transaction_Lines__r);

            Map<String, IBA_PolicyTransactionLine__c> primaryTransactionLineByKeyMap = prepareTransactionLineByKeyMap(primaryTransaction.Policy_Transaction_Lines__r);
            for (IBA_PolicyTransaction__c policyTransaction : transactionsToAggregate) {
                for (IBA_PolicyTransactionLine__c transactionLine : policyTransaction.Policy_Transaction_Lines__r) {
                    String transactionLineComparisonKey = constructComparisonKey(transactionLine);
                    IBA_PolicyTransactionLine__c primaryTransactionLine = primaryTransactionLineByKeyMap.get(transactionLineComparisonKey);
                    if (primaryTransactionLine != null) {
                        aggregateTransactionLineData(primaryTransactionLine, transactionLine);
                    } else {
                        // create new primary transaction line (and carrier line)
                        primaryTransactionLine = createPrimaryTransactionLine(primaryTransaction.Id, transactionLine.IBA_Product__c);
                        IBA_FFPolicyCarrierLine__c newCarrierLine = new IBA_FFPolicyCarrierLine__c(
                                IBA_PolicyTransaction__c = primaryTransaction.Id,
                                IBA_CarrierAccount__c = transactionLine.IBA_PolicyCarrierLine__r.IBA_CarrierAccount__c
                        );
                        aggregateTransactionLineData(primaryTransactionLine, transactionLine);
                        primaryTransactionLineByKeyMap.put(transactionLineComparisonKey, primaryTransactionLine);
                        newCarrierLines.add(newCarrierLine);
                        newTransactionAndCarrierLines.add(new TransactionAndCarrierLineWrapper(primaryTransactionLine, newCarrierLine));
                    }
                }
                // update transaction records with processed flag
                policyTransaction.PHO_AggregationProcessed__c = true;
                transactionsToUpdate.add(policyTransaction);
            }
        }

        // perform DML operations
        insert newCarrierLines;
        List<IBA_PolicyTransactionLine__c> newPrimaryTransactionLines = new List<IBA_PolicyTransactionLine__c>();
        for (TransactionAndCarrierLineWrapper wrapper : newTransactionAndCarrierLines) {
            wrapper.transactionLine.IBA_PolicyCarrierLine__c = wrapper.carrierLine.Id;
            newPrimaryTransactionLines.add(wrapper.transactionLine);
        }
        insert newPrimaryTransactionLines;
        update transactionsToUpdate;
        update transactionLinesToUpdate;
    }

    private static String addYearSpecificConditionsForTransaction() {
        String toReturn;
        if (only2019) {
            toReturn =
                    ' AND CALENDAR_YEAR(IBA_EffectiveDate__c) = 2019' +
                    ' AND PHO_PhoenixProcessed__c = FALSE';
        } else { // ASSUMPTION: all 2019 ALPs primary data has already been aggregated prior to >2019 aggregation
            toReturn =
                    ' AND PHO_AggregationProcessed__c = FALSE' +
                    ' AND ID IN (SELECT IBA_PolicyTransaction__c' +
                                ' FROM IBA_PolicyTransactionLine__c' +
                                ' WHERE '+ CONTAINS_ALPS_DATA_CONDITIONS + ')';
        }
        return toReturn;
    }

    private static String addYearSpecificConditionsForTransactionLine() {
        String toReturn;
        if (only2019) {
            toReturn = '';
        } else { // take only those PTLs which have at least one secondary data (ALPs data) field populated
            toReturn = ' WHERE ' + CONTAINS_ALPS_DATA_CONDITIONS;
        }
        return toReturn;
    }

    private static IBA_PolicyTransactionLine__c createPrimaryTransactionLine(Id primaryTransactionId, Id productId) {
        return new IBA_PolicyTransactionLine__c(
                IBA_PolicyTransaction__c = primaryTransactionId,
                IBA_Product__c = productId,
                PHO_ALPsBrokerCommission__c = 0,
                PHO_ALPsCarrierFee__c = 0,
                PHO_ALPsForeignTax__c = 0,
                PHO_ALPsGrossWrittenPremium__c = 0,
                PHO_ALPsInsurancePremiumTax__c = 0,
                PHO_ALPsMGACommission__c = 0
        );
    }

    private static String constructComparisonKey(IBA_PolicyTransactionLine__c transactionLine) {
        return String.valueOf(transactionLine.IBA_Product__c) + String.valueOf(transactionLine.IBA_PolicyCarrierLine__r.IBA_CarrierAccount__c);
    }

    private static Map<String, IBA_PolicyTransactionLine__c> prepareTransactionLineByKeyMap(List<IBA_PolicyTransactionLine__c> transactionLines) {
        Map<String, IBA_PolicyTransactionLine__c> returnMap = new Map<String, IBA_PolicyTransactionLine__c>();
        for (IBA_PolicyTransactionLine__c transactionLine : transactionLines) {
            returnMap.put(constructComparisonKey(transactionLine), transactionLine);
        }
        return returnMap;
    }

    private static Map<String, List<IBA_PolicyTransaction__c>> groupTransactionsToAggregate(List<IBA_PolicyTransaction__c> transactions) {
        Map<String, List<IBA_PolicyTransaction__c>> groupedTransactionsBySeed = new Map<String, List<IBA_PolicyTransaction__c>>();
        for (IBA_PolicyTransaction__c policyTransaction : transactions) {
            List<IBA_PolicyTransaction__c> groupedTransactions = groupedTransactionsBySeed.get(policyTransaction.PHO_ALPsAggregationSeed__c);
            if (groupedTransactions == null) {
                groupedTransactionsBySeed.put(policyTransaction.PHO_ALPsAggregationSeed__c, new List<IBA_PolicyTransaction__c>{policyTransaction});
            } else {
                groupedTransactions.add(policyTransaction);
            }
        }
        return  groupedTransactionsBySeed;
    }

    private static Map<String, IBA_PolicyTransaction__c> retrievePrimaryTransactions(Set<String> seedsSet) {
        Map<String, IBA_PolicyTransaction__c> primaryTransactionBySeed = new Map<String, IBA_PolicyTransaction__c>();
        String query =
                'SELECT PHO_ALPsAggregationSeed__c,' +
                        ' (SELECT ' + String.join(TRANSACTION_LINE_RELEVANT_FIELDS, ',') +
                        ' FROM Policy_Transaction_Lines__r)' +
                ' FROM IBA_PolicyTransaction__c' +
                ' WHERE PHO_ALPsAggregationSeed__c IN :seedsSet' +
                ' AND PHO_PrimaryRecord__c = TRUE';
        List<IBA_PolicyTransaction__c> primaryTransactions = Database.query(query);
        for (IBA_PolicyTransaction__c primaryTransaction : primaryTransactions) {
            primaryTransactionBySeed.put(primaryTransaction.PHO_ALPsAggregationSeed__c, primaryTransaction);
        }
        return primaryTransactionBySeed;
    }

    private static void populateInitialAggregationData(IBA_PolicyTransaction__c primaryTransaction) {
        if (only2019) {
            for (IBA_PolicyTransactionLine__c transactionLine : primaryTransaction.Policy_Transaction_Lines__r) {
                transactionLine.PHO_ALPsBrokerCommission__c = setZeroIfNull(transactionLine.IBA_BrokerCommission__c);
                transactionLine.PHO_ALPsCarrierFee__c = setZeroIfNull(transactionLine.IBA_CarrierFee__c);
                transactionLine.PHO_ALPsForeignTax__c = setZeroIfNull(transactionLine.IBA_ForeignTax__c);
                transactionLine.PHO_ALPsGrossWrittenPremium__c = setZeroIfNull(transactionLine.IBA_GrossWrittenPremium__c);
                transactionLine.PHO_ALPsInsurancePremiumTax__c = setZeroIfNull(transactionLine.IBA_InsurancePremiumTax__c);
                transactionLine.PHO_ALPsMGACommission__c = setZeroIfNull(transactionLine.IBA_MGACommission__c);
            }
        } else {
            for (IBA_PolicyTransactionLine__c transactionLine : primaryTransaction.Policy_Transaction_Lines__r) {
                transactionLine.PHO_ALPsBrokerCommission__c = setZeroIfNull(transactionLine.PHO_PHBrokerCommission__c);
                transactionLine.PHO_ALPsCarrierFee__c = setZeroIfNull(transactionLine.PHO_PHCarrierFee__c);
                transactionLine.PHO_ALPsForeignTax__c = setZeroIfNull(transactionLine.PHO_PHForeignTax__c);
                transactionLine.PHO_ALPsGrossWrittenPremium__c = setZeroIfNull(transactionLine.PHO_PHGrossWrittenPremium__c);
                transactionLine.PHO_ALPsInsurancePremiumTax__c = setZeroIfNull(transactionLine.PHO_PHInsurancePremiumTax__c);
                transactionLine.PHO_ALPsMGACommission__c = setZeroIfNull(transactionLine.PHO_PHMGACommission__c);
            }
        }
    }

    private static void aggregateTransactionLineData(IBA_PolicyTransactionLine__c primaryTransactionLine, IBA_PolicyTransactionLine__c transactionLine) {
        if (only2019) {
            primaryTransactionLine.PHO_ALPsBrokerCommission__c += setZeroIfNull(transactionLine.IBA_BrokerCommission__c);
            primaryTransactionLine.PHO_ALPsCarrierFee__c += setZeroIfNull(transactionLine.IBA_CarrierFee__c);
            primaryTransactionLine.PHO_ALPsForeignTax__c += setZeroIfNull(transactionLine.IBA_ForeignTax__c);
            primaryTransactionLine.PHO_ALPsGrossWrittenPremium__c += setZeroIfNull(transactionLine.IBA_GrossWrittenPremium__c);
            primaryTransactionLine.PHO_ALPsInsurancePremiumTax__c += setZeroIfNull(transactionLine.IBA_InsurancePremiumTax__c);
            primaryTransactionLine.PHO_ALPsMGACommission__c += setZeroIfNull(transactionLine.IBA_MGACommission__c);
        } else {
            primaryTransactionLine.PHO_ALPsBrokerCommission__c += setZeroIfNull(transactionLine.PHO_PHBrokerCommission__c);
            primaryTransactionLine.PHO_ALPsCarrierFee__c += setZeroIfNull(transactionLine.PHO_PHCarrierFee__c);
            primaryTransactionLine.PHO_ALPsForeignTax__c += setZeroIfNull(transactionLine.PHO_PHForeignTax__c);
            primaryTransactionLine.PHO_ALPsGrossWrittenPremium__c += setZeroIfNull(transactionLine.PHO_PHGrossWrittenPremium__c);
            primaryTransactionLine.PHO_ALPsInsurancePremiumTax__c += setZeroIfNull(transactionLine.PHO_PHInsurancePremiumTax__c);
            primaryTransactionLine.PHO_ALPsMGACommission__c += setZeroIfNull(transactionLine.PHO_PHMGACommission__c);
        }
    }

    private static Decimal setZeroIfNull(Decimal fieldValue) {
        return fieldValue != null ? fieldValue : 0;
    }

    private class TransactionAndCarrierLineWrapper {
        public IBA_PolicyTransactionLine__c transactionLine {get; set;}
        public IBA_FFPolicyCarrierLine__c carrierLine {get; set;}

        public TransactionAndCarrierLineWrapper(
                IBA_PolicyTransactionLine__c transactionLine,
                IBA_FFPolicyCarrierLine__c carrierLine
        ) {
            this.transactionLine = transactionLine;
            this.carrierLine = carrierLine;
        }
    }
}