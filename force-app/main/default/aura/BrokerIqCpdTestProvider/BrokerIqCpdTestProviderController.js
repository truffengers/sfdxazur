({
	doInit : function(component, event, helper) {
		var recordId = component.get('v.recordId');

		if(!$A.util.isEmpty(recordId)) {
			var getCpdTestAction = component.get('c.getCpdTest');

			getCpdTestAction.setParam('recordId', recordId);

			getCpdTestAction.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	            	var cpdTestRecord = response.getReturnValue();

	                component.set('v.cpdTestRecord', cpdTestRecord);
		        }
	        });
	        
	        $A.enqueueAction(getCpdTestAction);	
		}
	}
})