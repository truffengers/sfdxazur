({
	doInit : function(component, event, helper) {
		var getFurtherWebinarsAction = component.get('c.getFurtherWebinars');

        getFurtherWebinarsAction.setParam('existingId', component.get('v.recordId'));

		getFurtherWebinarsAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.upcomingWebinars', result.upcomingWebinars);
                component.set('v.recentWebinars', result.recentWebinars);
            }
        });
        
        $A.enqueueAction(getFurtherWebinarsAction);		
    }
})