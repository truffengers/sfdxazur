trigger TopicAssignmentTrigger on TopicAssignment (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  (new Nebula_Tools.MetaDataTriggerManager(TopicAssignment.sObjectType)).handle();
}