/**
* @name: IBA_PolicyTransactionTriggerHandler
* @description: This class is handling and dispatching the relevant IBA_PolicyTransaction__c trigger functionality
*
*/

public with sharing class IBA_PolicyTransactionTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_PolicyTransactionTriggerHandler.IsDisabled != null ? IBA_PolicyTransactionTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
        IBA_PolicyTransactionService.populateExchangeRate(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        IBA_PolicyTransactionService.populateExchangeRate(newItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Set<Id> activePolicyIds = new Set<Id>();
        Map<Id, IBA_PolicyTransaction__c> policyTransactionDueDateMap = new Map<Id, IBA_PolicyTransaction__c>();
        for(Id recordId :newItems.keySet()) {
            IBA_PolicyTransaction__c policyTransaction = (IBA_PolicyTransaction__c)newItems.get(recordId);
            IBA_PolicyTransaction__c policyTransactionOld = (IBA_PolicyTransaction__c)oldItems.get(recordId);
            if((policyTransaction.IBA_AccountingBrokerOverdue__c != policyTransactionOld.IBA_AccountingBrokerOverdue__c ||
                policyTransaction.IBA_BrokerOverdue__c != policyTransactionOld.IBA_BrokerOverdue__c) && policyTransaction.IBA_ActivePolicy__c != null) {
                policyTransactionDueDateMap.put(policyTransaction.Id, policyTransaction);
                activePolicyIds.add(policyTransaction.IBA_ActivePolicy__c);
            }
        }

        if(policyTransactionDueDateMap.size() > 0) {
            Set<Id> allPolicyTransactionIds = new Set<Id>();
            Map<Id, IBA_PolicyTransaction__c> allOtherPolicyTransactions = new Map<Id, IBA_PolicyTransaction__c>(
                [SELECT Id, IBA_ActivePolicy__c FROM IBA_PolicyTransaction__c WHERE IBA_ActivePolicy__c IN :activePolicyIds]);
            allPolicyTransactionIds.addAll(policyTransactionDueDateMap.keySet());
            allPolicyTransactionIds.addAll(allOtherPolicyTransactions.keySet());
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(allPolicyTransactionIds);
            transferData.updateBrokerAssetOverdue();
        }
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}