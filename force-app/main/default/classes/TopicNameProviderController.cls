/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 18/10/2017
 */

public with sharing class TopicNameProviderController {

    @AuraEnabled
    public static Topic getTopic(String topicId, String topicName) {
        List<Topic> rval;

        if(topicId != null) {
            rval = [SELECT Id, Name FROM Topic WHERE Id = :topicId];
        } else if(topicName != null) {
            rval = [SELECT Id, Name FROM Topic WHERE Name = :topicName AND NetworkId = :Network.getNetworkId()];
        }

        if(rval == null || rval.isEmpty()) {
            return null;
        } else {
            return rval[0];
        }
    }

}