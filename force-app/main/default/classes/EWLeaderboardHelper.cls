public with sharing class EWLeaderboardHelper {
    @AuraEnabled
    public static List<Quote> getLeaderboardData(){
        User currentUser = [
            SELECT Id, ContactId, Contact.Account.Id
            FROM User
            WHERE Id = :UserInfo.getUserId()
        ];

        String brokerageId = '';
        if(!String.isBlank(currentUser.ContactId)){
            brokerageId = currentUser.Contact.Account.Id;
        }

        List<Quote> quotes = [
            SELECT Id, Owner.Name
            FROM Quote
            WHERE vlocity_ins__AgencyBrokerageId__c = :brokerageId
            AND CreatedDate = THIS_MONTH
        ];

        return quotes;
    }

    @AuraEnabled
    public static List<EW_TopTip__c> getTopTips(){
        List<EW_TopTip__c> tips = [
            SELECT Name, EW_TextContent__c
            FROM EW_TopTip__c
            WHERE EW_Deactivated__c = false
        ];

        return tips;
    }

    @AuraEnabled
    public static Decimal thumbsUpClicked(String topTipId){
        EW_TopTip__c tip = [
            SELECT Id, EW_ThumbsUp__c
            FROM EW_TopTip__c
            WHERE Id=:topTipId
        ];

        if (tip != null) {
            if (tip.EW_ThumbsUp__c == null) {
                tip.EW_ThumbsUp__c = 1;
            } else {
                tip.EW_ThumbsUp__c = tip.EW_ThumbsUp__c + 1;
            }
        }

        update (SObject) tip;

        return tip.EW_ThumbsUp__c;
    }

    @AuraEnabled
    public static Decimal thumbsDownClicked(String topTipId){
        EW_TopTip__c tip = [
                SELECT Id, EW_ThumbsDown__c
                FROM EW_TopTip__c
                WHERE Id=:topTipId
        ];

        if (tip != null) {
            if (tip.EW_ThumbsDown__c == null) {
                tip.EW_ThumbsDown__c = 1;
            } else {
                tip.EW_ThumbsDown__c = tip.EW_ThumbsDown__c + 1;
            }
        }

        update (SObject) tip;

        return tip.EW_ThumbsDown__c;
    }
}