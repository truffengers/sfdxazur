/*
*Author     :    Meti Gashi
*Company    :    Azur Underwriting
*Date       :    28/06/2017
*Purpose    :    Used to call the recordType ids
*/
global with sharing class RecordTypeSelector implements vlocity_ins.VlocityOpenInterface {

    /**
    * @methodName : invokeMethod
    * @description: This method is needed to allow this apex class to interact with the omniscript
    * @dateCreated: 28/06/2017
    */
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Boolean result = true;

        try {
            if(String.isNotBlank(methodName)){

                try{
                    String objectType = String.valueOf(inputMap.get('objectType'));
                    recordTypeSearch(inputMap, outMap, objectType);
                }
                catch(Exception e) {system.debug('error is '+e.getMessage());}
            }
        }
        catch(Exception e) {
            system.debug('error is '+e.getMessage());
            result = false;
        }
        system.debug('****result is ' +result);
        return result;
    }
    public void recordTypeSearch(Map<String,Object> inputMap, Map<String,Object> outMap, string objectType) {
        List<RecordType> recordTypeList = new List<RecordType>([SELECT Id, DeveloperName, sObjectType FROM RecordType WHERE sObjectType =: objectType]);
        system.debug(recordTypeList);
        for(RecordType r : recordTypeList) {
            outMap.put(r.DeveloperName, r.Id);
        }
    }

    /**public void recordTypeSearch(Map<String,Object> inputMap, Map<String,Object> outMap, string objectType) {
        System.debug('START OF METHOD');
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //Define the base URL to be called based on the request type
        String url = 'https://azur-integrations.herokuapp.com/queryABIFor'+urlString;
        System.debug('URL before is '+url);

        url=url.replace(' ', '%20'); //Replace any blank strings in the url with %20 to avoid any call errors

        //Set the request variables
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setCompressed(false); // otherwise we hit a limit of 32000

        //Attempt to make the callout and pull the data back. Populate the returned data into the outMap so it appears in the omniscript
        try {
            res = http.send(req);
            String jsonStr = res.getBody();
            System.debug('jsonStr: '+jsonStr);

            outMap.put('TableResponse', JSON.deserializeUntyped(jsonStr));

        } catch(System.CalloutException e) {

            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
    } */

    /**
    * @methodName : glassSearch
    * @description: This method is used to call the glasses service in Heroku and pull back vehicle information
    * @dateCreated: 28/06/2017
    */
}