/**
* @name: IBA_TestDataFactory
* @description: Test Data factory for IBA project
*
*/
@isTest
public with sharing class IBA_TestDataFactory {

    public static final String SYSTEM_ADMINISTRATOR = 'System Administrator';
    public static final String SYSTEM_ADMINISTRATOR_ID = [SELECT Id FROM Profile WHERE Name = :SYSTEM_ADMINISTRATOR LIMIT 1].Id;

    public static final String IBA_ACCOUNTING_PERM = 'IBA_IBAAccounting';
    public static final String IBA_ACCOUNTANT = 'Azur IBA Accountant';
    public static final String IBA_ACCOUNTANT_ID = [SELECT Id FROM Profile WHERE Name = :IBA_ACCOUNTANT LIMIT 1].Id;

    public static void createCustomSettings() {
        Account dummy = createAccount(true, 'Dummy Account', IBA_UtilConst.ACCOUNT_RT_BROKER);
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        settings.IBA_OperationalDataLimit__c = 1000;
        settings.IBA_DummyAccount__c = dummy.Id;
        upsert settings IBA_IBACustomSettings__c.Id;
    }

    @future
    public static void createUserWithPermSets(Id profileId, Set<String> permSetApiNames) {
        User user = createUser(profileId);
        List<PermissionSetAssignment> psAssignments  = new List<PermissionSetAssignment>();
        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name IN: (permSetApiNames)]){
            psAssignments.add(new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = ps.Id ));
        }
        insert psAssignments;
    }

    public static User createUser(Id profileId) {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueOf(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomId = Integer.valueOf(Math.rint(Math.random()*1000000));
        String uniqueName = orgId + dateString + randomId;
        User user = new User(FirstName = 'ABC',
                LastName = 'XYZ',
                Email = uniqueName + '@azuruw.com.test',
                Username = uniqueName + '@azuruw.com.test',
                EmailEncodingKey = 'ISO-8859-1',
                Alias = uniqueName.substring(18, 23),
                TimeZoneSidKey = 'Europe/London',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                ProfileId = profileId
        );
        insert user;
        return user;
    }

    public static List<IBA_IBAStaging__c> createIBAStagingRecords(Boolean doDML, Integer numOfStagingRecordsToCreate) {
        List<IBA_IBAStaging__c> stagingList = new List<IBA_IBAStaging__c>();
        for (Integer i = 0; i < numOfStagingRecordsToCreate; i++) {
            IBA_IBAStaging__c staging = createIBAStagingRecord(false);
            staging.IBA_Commission__c = i;
            stagingList.add(staging);
        }
        if (doDML) {
            insert stagingList;
        }
        return stagingList;
    }

    public static IBA_IBAStaging__c createIBAStagingRecord(Boolean doDML) {
        IBA_IBAStaging__c staging = new IBA_IBAStaging__c();
        staging.IBA_AzurCommissionRate__c = 0;
        staging.IBA_AzurCommission__c = 0;
        staging.IBA_BrokerCommissionRate__c = 0;
        staging.IBA_ForeignTaxRate__c = 0;
        staging.IBA_ForeignTax__c = 0;
        staging.IBA_GrossWrittenPremium__c = 0;
        staging.IBA_InsuarncePremiumTax_Rate__c = 0;
        staging.IBA_InsuredName__c = 'Test Insured Name';
        staging.IBA_NetDuefromBroker__c = 0;
        staging.IBA_NetPayabletoAIG__c = 0;
        staging.IBA_PolicyExpiryDate__c = Date.today().addDays(3);
        staging.IBA_PolicyInceptionDate__c = Date.today().addDays(2);
        staging.IBA_PremiumEffectiveDate__c = Date.today().addDays(1);
        staging.IBA_TotalCommissionRate__c = 0;
        staging.IBA_TransactionDate__c = Datetime.now().addSeconds(5);
        staging.IBA_TransactionType__c = IBA_UtilConst.POLICY_TRANSACTION_TYPE_ENDORSEMENT;
        if(doDML) {
            insert staging;
        }
        return staging;
    }

    public static List<Account> createAccounts(Boolean doDML, Integer numOfAccountsToCreate, String recordType) {
        List<Account> accList = new List<Account>();
        for (Integer i = 0; i < numOfAccountsToCreate; i++) {
            Account acc = createAccount(false, 'Test Account ' + i, recordType);
            accList.add(acc);
        }
        if (doDML) {
            insert accList;
        }
        return accList;
    }

    public static Account createAccount(Boolean doDML, String accountName, String recordType) {
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        Account acc = new Account(
            Name = accountName,
			RecordTypeId = recordTypeId
        );
        if(doDML) {
            insert acc;
        }
        return acc;
    }

    // public static Contact createContact(Boolean doDML, Id accountId, String firstName, String lastName, String recordType) {
    //     Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
    //     Contact con = new Contact(
    //         AccountId = accountId,
    //         FirstName = firstName,
    //         LastName = lastName,
	// 		RecordTypeId = recordTypeId
    //     );
    //     if(doDML) {
    //         insert con;
    //     }
    //     return con;
    // }

    public static List<Product2> createProducts(Boolean doDML, Integer numOfProductsToCreate, String recordType) {
        List<Product2> productList = new List<Product2>();
        for (Integer i = 0; i < numOfProductsToCreate; i++) {
            Product2 product = createProduct(false, 'Test Product ' + i, recordType);
            productList.add(product);
        }
        if (doDML) {
            insert productList;
        }
        return productList;
    }

    public static Product2 createProduct(Boolean doDML, String productName, String recordType) {
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        Product2 product = new Product2(
            Name = productName,
            RecordTypeId = recordTypeId
        );
        if(doDML) {
            insert product;
        }
        return product;
    }

    public static List<Asset> createPolicies(Boolean doDML, Integer numOfPoliciesToCreate, Id accountId) {
        List<Asset> policyList = new List<Asset>();
        for (Integer i = 0; i < numOfPoliciesToCreate; i++) {
            Asset policy = createPolicy(false, 'Policy ' + i, accountId);
            policyList.add(policy);
        }
        if (doDML) {
            insert policyList;
        }
        return policyList;
    }

    public static Asset createPolicy(Boolean doDML, String policyNumber, Id accountId) {
        Id rtId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get(IBA_UtilConst.POLICY_RT_HIGH_NET_WORTH).getRecordTypeId();
        Asset policy = new Asset(
            Name = policyNumber,
            AccountId = accountId,
            RecordTypeId = rtId
        );
        if(doDML) {
            insert policy;
        }
        return policy;
    }

    public static List<vlocity_ins__AssetCoverage__c> createPolicyCoverages(Boolean doDML, Integer numOfPolicyCoveragesToCreate, Id productId, Id policyId) {
        List<vlocity_ins__AssetCoverage__c> policyCoverageList = new List<vlocity_ins__AssetCoverage__c>();
        for (Integer i = 0; i < numOfPolicyCoveragesToCreate; i++) {
            vlocity_ins__AssetCoverage__c policyCoverage = createPolicyCoverage(false, 'Product Name ' + i, productId, policyId);
            policyCoverageList.add(policyCoverage);
        }
        if (doDML) {
            insert policyCoverageList;
        }
        return policyCoverageList;
    }

    public static vlocity_ins__AssetCoverage__c createPolicyCoverage(Boolean doDML, String name, Id productId, Id policyId) {
        vlocity_ins__AssetCoverage__c policyCoverage = new vlocity_ins__AssetCoverage__c(
            vlocity_ins__Product2Id__c = productId,
            vlocity_ins__PolicyAssetId__c = policyId,
            Name = name,
            IBA_Status__c = IBA_UtilConst.POLICY_COVERAGE_STATUS_ACTIVE
        );
        if(doDML) {
            insert policyCoverage;
        }
        return policyCoverage;
    }

    public static List<IBA_PolicyTransaction__c> createPolicyTransactions(Boolean doDML, Integer numOfPolicyTransactionsToCreate, Id policyId) {
        List<IBA_PolicyTransaction__c> policyTransactionList = new List<IBA_PolicyTransaction__c>();
        for (Integer i = 0; i < numOfPolicyTransactionsToCreate; i++) {
            IBA_PolicyTransaction__c policyTransaction = createPolicyTransaction(false, policyId);
            policyTransactionList.add(policyTransaction);
        }
        if (doDML) {
            insert policyTransactionList;
        }
        return policyTransactionList;
    }

    public static IBA_PolicyTransaction__c createPolicyTransaction(Boolean doDML, Id policyId) {
        IBA_PolicyTransaction__c policyTransaction = new IBA_PolicyTransaction__c(
            IBA_OriginalPolicy__c = policyId,
            IBA_ActivePolicy__c = policyId,
            IBA_TransactionDate__c = Datetime.now(),
            IBA_EffectiveDate__c = Date.today().addDays(2),
            IBA_TransactionType__c = IBA_UtilConst.POLICY_TRANSACTION_TYPE_ENDORSEMENT
        );
        if(doDML) {
            insert policyTransaction;
        }
        return policyTransaction;
    }

    public static IBA_FFPolicyCarrierLine__c createPolicyCarrierLine(Boolean doDML, Id policyTransactionId, Id carrierAccountId) {
        IBA_FFPolicyCarrierLine__c policyCarrierLine = new IBA_FFPolicyCarrierLine__c(
            IBA_PolicyTransaction__c = policyTransactionId,
            IBA_CarrierAccount__c = carrierAccountId
        );
        if(doDML) {
            insert policyCarrierLine;
        }
        return policyCarrierLine;
    }

    public static IBA_PolicyTransactionLine__c createPolicyTransactionLine(Boolean doDML, Id policyTransactionId, Id policyCarrierLineId) {
        IBA_PolicyTransactionLine__c policyTransactionLine = new IBA_PolicyTransactionLine__c(
            IBA_PolicyTransaction__c = policyTransactionId,
            IBA_PolicyCarrierLine__c = policyCarrierLineId
        );
        if(doDML) {
            insert policyTransactionLine;
        }
        return policyTransactionLine;
    }

    public static c2g__codaAccountingCurrency__c createCurrency(Boolean doDML, String name) {
        c2g__codaAccountingCurrency__c curr = new c2g__codaAccountingCurrency__c(
            Name = name,
            c2g__DecimalPlaces__c = 2,
            c2g__Home__c = true,
            c2g__Dual__c = true
        );
        if(doDML) {
            insert curr;
        }
        return curr;
    }

    public static c2g__codaYear__c createYear(Boolean doDML, Id companyGroupId, Id companyId) {
        c2g__codaYear__c yr = new c2g__codaYear__c(
            Name = String.valueOf(Date.today().year()),
            OwnerId = companyGroupId,
            c2g__OwnerCompany__c = companyId,
            c2g__NumberOfPeriods__c = 12,
            c2g__AutomaticPeriodList__c = true,
            c2g__StartDate__c = Date.valueOf(Date.today().year() + '-01-01 00:00:00'),
            c2g__EndDate__c =  Date.valueOf(Date.today().year() + '-12-31 00:00:00'),
            c2g__PeriodCalculationBasis__c = IBA_UtilConst.YEAR_PERIOD_CALCULATION_MONTH_END
        );
        if(doDML) {
            insert yr;
        }
        return yr;
    }

    public static c2g__codaGeneralLedgerAccount__c createGeneralLedgerAccount(Boolean doDML, String name, String uniqueCode) {
        c2g__codaGeneralLedgerAccount__c gla = new c2g__codaGeneralLedgerAccount__c(
            Name = name,
            c2g__ReportingCode__c = uniqueCode,
            c2g__Type__c = IBA_UtilConst.GENERAL_LEDGER_ACCOUNT_TYPE_BALANCE,
            c2g__TrialBalance1__c = IBA_UtilConst.GENERAL_LEDGER_ACCOUNT_BALANCE1_INCOME,
            c2g__TrialBalance2__c = IBA_UtilConst.GENERAL_LEDGER_ACCOUNT_BALANCE2_GROSS,
            c2g__TrialBalance3__c = IBA_UtilConst.GENERAL_LEDGER_ACCOUNT_BALANCE3_ACCOUNTS
        );
        if(doDML) {
            insert gla;
        }
        return gla;
    }

    public static c2g__codaCompany__c createCompany(Boolean doDML, String name, String recordType) {
        Id recordTypeId = Schema.SObjectType.c2g__codaCompany__c.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        c2g__codaCompany__c company = new c2g__codaCompany__c(
            Name = name,
            RecordTypeId = recordTypeId
        );
        if(doDML) {
            insert company;
        }
        return company;
    }

    public static c2g__codaUserCompany__c createUserCompany(Boolean doDML, Id companyId, String userId) {
        c2g__codaUserCompany__c userCompany = new c2g__codaUserCompany__c(
            c2g__Company__c = companyId,
            c2g__User__c = userId
        );
        if(doDML) {
            insert userCompany;
        }
        return userCompany;
    }

    public static c2g__codaBankAccount__c createBankAccount(Boolean doDML, String accountName, String accountNumber, String bankName, String uniqueCode, Id glaId, Id currencyId) {
        c2g__codaBankAccount__c bankAccount = new c2g__codaBankAccount__c(
            c2g__AccountName__c = accountName, 
            c2g__AccountNumber__c = accountNumber, 
            c2g__BankName__c = bankName, 
            c2g__ReportingCode__c = uniqueCode,
            c2g__GeneralLedgerAccount__c = glaId,
            c2g__BankAccountCurrency__c = currencyId
        );
        if(doDML) {
            insert bankAccount;
        }
        return bankAccount;
    }

    public static c2g__codaCashEntry__c createCashEntry(Boolean doDML, Id bankAccountId, Id periodId, Id currencyId) {
        c2g__codaCashEntry__c cashEntry = new c2g__codaCashEntry__c(
            c2g__BankAccount__c = bankAccountId,
            c2g__Period__c = periodId,
            c2g__CashEntryCurrency__c = currencyId
        );
        if(doDML) {
            insert cashEntry;
        }
        return cashEntry;
    }

    public static c2g__codaCashEntryLineItem__c createCashEntryLineItem(Boolean doDML, Id accountId, Decimal cashAmount, Id cashEntryId) {
        c2g__codaCashEntryLineItem__c cashEntryLineItem = new c2g__codaCashEntryLineItem__c(
            c2g__Account__c = accountId,
            c2g__CashEntryValue__c = cashAmount,
            c2g__CashEntry__c = cashEntryId
        );
        if(doDML) {
            insert cashEntryLineItem;
        }
        return cashEntryLineItem;
    }

    public static c2g__codaCreditNote__c createSalesCreditNote(Boolean doDML, Id ownerCompanyId, Id accountId, Id periodId, Id currencyId) {
        c2g__codaCreditNote__c salesCreditNote = new c2g__codaCreditNote__c(
            c2g__CreditNoteDate__c = System.today(),
            c2g__DueDate__c = System.today().addDays(14),
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__Account__c = accountId,
            c2g__Period__c = periodId,
            c2g__CreditNoteCurrency__c = currencyId
        );
        if(doDML) {
            insert salesCreditNote;
        }
        return salesCreditNote;
    }

    public static c2g__codaCreditNoteLineItem__c createSalesCreditNoteLineItem(Boolean doDML, Decimal unitPrice, Id ownerCompanyId, Id salesCreditNoteId, Id productId) {
        c2g__codaCreditNoteLineItem__c cnli = new c2g__codaCreditNoteLineItem__c(
            c2g__UnitPrice__c = unitPrice,
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__CreditNote__c = salesCreditNoteId,
            c2g__Product__c = productId
        );
        if(doDML) {
            insert cnli;
        }
        return cnli;
    }

    public static c2g__codaPurchaseCreditNote__c createPurchaseCreditNote(Boolean doDML, Id ownerCompanyId, Id accountId, Id periodId, Id currencyId) {
        c2g__codaPurchaseCreditNote__c purchaseCreditNote = new c2g__codaPurchaseCreditNote__c(
            c2g__CreditNoteDate__c = System.today(),
            c2g__DueDate__c = System.today().addDays(14),
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__Account__c = accountId,
            c2g__Period__c = periodId,
            c2g__AccountInvoiceNumber__c = 'test12341290testing',
            c2g__AccountCreditNoteNumber__c = 'test12341290testing',
            c2g__CreditNoteCurrency__c = currencyId
        );
        if(doDML) {
            insert purchaseCreditNote;
        }
        return purchaseCreditNote;
    }

    public static c2g__codaPurchaseCreditNoteExpLineItem__c createPurchaseCreditNoteExpLineItem(Boolean doDML, Decimal netValue, Id ownerCompanyId, Id glaId, Id purchaseCreditNoteId) {
        c2g__codaPurchaseCreditNoteExpLineItem__c purchaseCreditNoteExpLineItem = new c2g__codaPurchaseCreditNoteExpLineItem__c(
            c2g__GeneralLedgerAccount__c = glaId,
            c2g__NetValue__c = netValue,
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__PurchaseCreditNote__c = purchaseCreditNoteId
        );
        if(doDML) {
            insert purchaseCreditNoteExpLineItem;
        }
        return purchaseCreditNoteExpLineItem;
    }

    public static c2g__codaPurchaseInvoice__c createPurchaseInvoice(Boolean doDML, Id ownerCompanyId, Id accountId, Id periodId, Id currencyId, Id salesInvoiceId) {
        c2g__codaPurchaseInvoice__c purchaseInvoice = new c2g__codaPurchaseInvoice__c(
            c2g__AccountInvoiceNumber__c = 'test12341290testing',
            c2g__InvoiceDate__c = System.today(),
            c2g__DueDate__c = System.today().addDays(14),
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__Account__c = accountId,
            c2g__Period__c = periodId,
            c2g__InvoiceCurrency__c = currencyId,
            IBA_BrokerSalesInvoice__c = salesInvoiceId
        );
        if(doDML) {
            insert purchaseInvoice;
        }
        return purchaseInvoice;
    }

    public static c2g__codaPurchaseInvoiceExpenseLineItem__c createPurchaseInvoiceExpenseLineItem(Boolean doDML, Decimal netValue, Id ownerCompanyId, Id glaId, Id purchaseInvoiceId) {
        c2g__codaPurchaseInvoiceExpenseLineItem__c pieli = new c2g__codaPurchaseInvoiceExpenseLineItem__c(
            c2g__GeneralLedgerAccount__c = glaId,
            c2g__NetValue__c = netValue,
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__PurchaseInvoice__c = purchaseInvoiceId
        );
        if(doDML) {
            insert pieli;
        }
        return pieli;
    }

    public static c2g__codaJournal__c createJournal(Boolean doDML, Id ownerCompanyId, Id ptId, Id periodId, Id currencyId) {
        c2g__codaJournal__c journal = new c2g__codaJournal__c(
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__JournalCurrency__c = currencyId,
            c2g__Period__c = periodId,
            IBA_PolicyTransaction__c = ptId
        );
        if(doDML) {
            insert journal;
        }
        return journal;
    }

    public static c2g__codaJournalLineItem__c createJournalLineItem(Boolean doDML, Integer lineNumber, Decimal value, Id glaId, Id journalId, String type, Id accId) {
        c2g__codaJournalLineItem__c journalLineItem = new c2g__codaJournalLineItem__c(
            c2g__LineNumber__c = lineNumber,
            c2g__UnitOfWork__c = 12,
            c2g__Value__c = value,
            c2g__GeneralLedgerAccount__c = glaId,
            c2g__LineType__c = type,
            c2g__Journal__c = journalId,
            c2g__Account__c = accId
        );
        if(doDML) {
            insert journalLineItem;
        }
        return journalLineItem;
    }

    public static c2g__codaInvoice__c createInvoice(Boolean doDML, Id ownerCompanyId, Id accountId, Id periodId, Id currencyId) {
        c2g__codaInvoice__c invoice = new c2g__codaInvoice__c(
            c2g__InvoiceDate__c = System.today(),
            c2g__DueDate__c = System.today().addDays(14),
            c2g__Account__c = accountId,
            c2g__OwnerCompany__c = ownerCompanyId,
            c2g__Period__c = periodId,
            c2g__InvoiceCurrency__c = currencyId
        );
        if(doDML) {
            insert invoice;
        }
        return invoice;
    }

    public static c2g__codaInvoiceLineItem__c createInvoiceLineItem(Boolean doDML, Integer lineNumber, Decimal unitPrice, Id invoiceId, Id productId) {
        c2g__codaInvoiceLineItem__c invoiceLineItem = new c2g__codaInvoiceLineItem__c(
            c2g__LineNumber__c = lineNumber,
            c2g__Quantity__c = 1.0,
            c2g__UnitPrice__c = unitPrice,
            c2g__Product__c = productId,
            c2g__Invoice__c = invoiceId
        );
        if(doDML) {
            insert invoiceLineItem;
        }
        return invoiceLineItem;
    }

    // too avoid SOQL query limits from managed package
    @future
    public static void postPurchaseCreditNotes(Set<Id> purchaseCreditNoteIds) {
        List<c2g.CODAAPICommon.Reference> purchaseCreditRefs = new List<c2g.CODAAPICommon.Reference>();
        for(Id purchaseCreditNoteId :purchaseCreditNoteIds) {
            purchaseCreditRefs.add(c2g.CODAAPICommon.getRef(purchaseCreditNoteId, null));
        }
        c2g.CODAAPIPurchaseCreditNote_7_0.BulkPostPurchaseCreditNote(null, purchaseCreditRefs);
    }

    // too avoid SOQL query limits from managed package
    @future
    public static void postSalesCreditNotes(Set<Id> salesCreditNoteIds) {
        List<c2g.CODAAPICommon.Reference> salesCreditRefs = new List<c2g.CODAAPICommon.Reference>();
        for(Id salesCreditNoteId :salesCreditNoteIds) {
            salesCreditRefs.add(c2g.CODAAPICommon.getRef(salesCreditNoteId, null));
        }
        c2g.CODAAPISalesCreditNote_10_0.BulkPostCreditNote(null, salesCreditRefs);
    }

    // too avoid SOQL query limits from managed package
    @future
    public static void postPurchaseInvoices(Set<Id> purchaseInvoiceIds) {
        List<c2g.CODAAPICommon.Reference> purchaseInvoiceRefs = new List<c2g.CODAAPICommon.Reference>();
        for(Id purchaseInvoiceId :purchaseInvoiceIds) {
            purchaseInvoiceRefs.add(c2g.CODAAPICommon.getRef(purchaseInvoiceId, null));
        }
        c2g.CODAAPIPurchaseInvoice_9_0.BulkPostPurchaseInvoice(null, purchaseInvoiceRefs);
    }

    // too avoid SOQL query limits from managed package
    @future
    public static void postJournals(Set<Id> journalIds) {
        List<c2g.CODAAPICommon.Reference> journalRefs = new List<c2g.CODAAPICommon.Reference>();
        for(Id journalId :journalIds) {
            journalRefs.add(c2g.CODAAPICommon.getRef(journalId, null));
        }
        c2g.CODAAPIJournal_12_0.BulkPostJournal(null, journalRefs);
    }

    // too avoid SOQL query limits from managed package
    @future
    public static void postCashEntries(Set<Id> cashEntryIds) {
        List<c2g.CODAAPICommon.Reference> cashEntryRefs = new List<c2g.CODAAPICommon.Reference>();
        for(Id cashEntryId :cashEntryIds) {
            cashEntryRefs.add(c2g.CODAAPICommon.getRef(cashEntryId, null));
        }
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(null, cashEntryRefs);
    }
    // too avoid SOQL query limits from managed package
    @future
    public static void postInvoices(Set<Id> invoiceIds) {
        List<c2g.CODAAPICommon.Reference> invoiceRefs = new List<c2g.CODAAPICommon.Reference>();
        for(Id invoiceId :invoiceIds) {
            invoiceRefs.add(c2g.CODAAPICommon.getRef(invoiceId, null));
        }
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(null, invoiceRefs);
    }

    // too avoid SOQL query limits from managed package
    @future
    public static void matchCash(Id accId, Id periodId, Set<Id> transactionLinesToMatchIds) {
        List<c2g__codaTransactionLineItem__c> transactionLinesToMatch = [SELECT Id, c2g__AccountValue__c, c2g__Transaction__c, 
            IBA_InvoicePaidFlowFired__c, c2g__MatchingStatus__c, c2g__LineType__c, c2g__GeneralLedgerAccount__c
            FROM c2g__codaTransactionLineItem__c WHERE Id IN :transactionLinesToMatchIds];

        c2g.CODAAPICommon_8_0.Context context = new c2g.CODAAPICommon_8_0.Context();
        c2g.CODAAPICashMatchingTypes_8_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_8_0.Configuration();
        configuration.Account = c2g.CODAAPICommon.getRef(accId, null);
        configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_8_0.enumMatchingCurrencyMode.Account; 
        configuration.MatchingDate = System.today();
        configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(periodId, null);
        List<c2g.CODAAPICashMatchingTypes_8_0.Item> items = new List<c2g.CODAAPICashMatchingTypes_8_0.Item>();
        for(c2g__codaTransactionLineItem__c transactionLine : transactionLinesToMatch) {
            c2g.CODAAPICashMatchingTypes_8_0.Item item = new c2g.CODAAPICashMatchingTypes_8_0.Item();
            item.TransactionLineItem = c2g.CODAAPICommon.getRef(transactionLine.Id, null);
            item.Paid = transactionLine.c2g__AccountValue__c;
            item.Discount = 0;
            item.WriteOff = 0;
            items.add(item);
        }
        c2g.CODAAPICashMatchingTypes_8_0.Analysis analisysInfo = new c2g.CODAAPICashMatchingTypes_8_0.Analysis();
        c2g.CODAAPICommon.Reference matchReference = c2g.CODAAPICashMatching_8_0.Match(context, configuration, items, analisysInfo);
    }

    public static FinancialForceSetup setupFinancialForce(String companyName, String curr) {
        FinancialForceSetup ffs = new FinancialForceSetup();
        System.runAs(new User(Id=UserInfo.getUserId())) {
            ffs.company = IBA_TestDataFactory.createCompany(true, companyName, IBA_UtilConst.COMPANY_RT_VAT);
            c2g.CODACompanyWebService.createQueue(ffs.company.Id, curr, ffs.company.Name);
            c2g.CODAYearWebService.calculatePeriods(null); // Workaround to bug in company API's, safe to remain once fixed
            c2g.CODACompanyWebService.activateCompany(ffs.company.Id, curr, ffs.company.Name);
            IBA_TestDataFactory.createUserCompany(true, ffs.company.Id, UserInfo.getUserId());
            String queueName = 'FF ' + ffs.company.Name;
            ffs.companyGroup = [SELECT Id FROM Group WHERE Name = :queueName AND Type = 'Queue' LIMIT 1];
            insert new GroupMember(GroupId = ffs.companyGroup.Id, UseroRGroupId = UserInfo.getUserId());
        }
        return ffs;
    }

    public class FinancialForceSetup {
        public Group companyGroup;
        public c2g__codaCompany__c company;
    }
}