/**
* @name: BrokerIqLoginHelper.js
* @description: BrokerIqLogin helper
* @author: Nebula Consulting
* @date: 14/10/2017
* @Modified: Ignacio Sarmiento	02/01/2018	Set 'startUrl' to redirect to current record when you log in
* @Modified: Ignacio Sarmiento	29/01/2018	BiQ-3: Set methods to redirect to home page. Remove 'getStartUrl' function
* @Modified: Ignacio Sarmiento	31/01/2018	BiQ-3: Update 'http' value to 'https'

*/
({
    qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    },

    mustBeRedirectedToLoginPage: function(component) {
        var partialUrl = window.location.pathname;
        var redirect = component.get("v.redirectToLogin");
        return redirect && !partialUrl.includes("/login/");
    },
    
    redirectToLoginPage: function() {
        var loginPageUrl = this.getLoginPageUrl();
        window.location.replace(loginPageUrl);
    },
    
    getLoginPageUrl: function() {
        var result = "https://"+ window.location.hostname;
        if(this.isSandbox()){
            result += '/brokeriq21';
        }
        result += '/s/login/';
        return result;
    },
    
    isSandbox: function(url){
        var productionUrl = 'broker-iq.azuruw.com';
        var currentUrl = window.location.href;
        return !currentUrl.includes(productionUrl);
    }, 
    
    handleLogin: function (component, event, helpler) {
        var username = component.find("username").get('v.value');
        var password = component.find("password").get('v.value');
        var action = component.get("c.login");
        var startUrl = component.get("v.startUrl");
        
        action.setParams({username:username, password:password, startUrl:startUrl});
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set("v.errorMessage",rtnValue);
                component.set("v.showError",true);
            }
        });
        $A.enqueueAction(action);
    },

    getIsUsernamePasswordEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    getIsSelfRegistrationEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsSelfRegistrationEnabled");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isSelfRegistrationEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    getCommunityForgotPasswordUrl : function (component, event, helpler) {
        var action = component.get("c.getForgotPasswordUrl");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communityForgotPasswordUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    getCommunitySelfRegisterUrl : function (component, event, helpler) {
        var action = component.get("c.getSelfRegistrationUrl");
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communitySelfRegisterUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    }
})