@isTest
public class EWCustomReportSchedulerTest {

    @isTest
    private static void testBehaviour_scheduleAndSendEmail() {
        String cronExp = '0 0 23 * * ?';
        EWCustomReportScheduler reportScheduler = new EWCustomReportScheduler();

        Test.startTest();
        System.schedule('testScheduledApex', cronExp, reportScheduler);

        reportScheduler.sendEmail(new Map<String, String>{
                'att' => 'content'
        }, new List<String>{
                'test@example.com'
        });

        Integer i = Limits.getEmailInvocations();
        Test.stopTest();

        System.assertNotEquals(0, i);
    }
}