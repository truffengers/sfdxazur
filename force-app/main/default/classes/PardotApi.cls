public class PardotApi extends Nebula_API.NebulaApi {

    public static String apiKey;
    public static String userKey;
    
    public static String pardotBase = 'https://pi.pardot.com/api';
    private String pardotVersion;
    private Pardot_API_Login__c apiLogin;

    public PardotApi() {
        super('Pardot');
        apiLogin = Pardot_API_Login__c.getInstance();

        pardotVersion = '/version/' + apiLogin.API_Version__c; 
    }
        
    public void login() {
        login(apiLogin);
    }
    public void login(Pardot_API_Login__c creds) {
        
        if(creds == null || creds.User_Key__c == null || creds.Email__c == null || creds.Password__c == null) {
            throw new Nebula_API.NebulaApiException('No Pardot_API_Login__c Custom Setting found, cannot log into API');
        }
        userKey = creds.User_Key__c;
        pardotVersion = '/version/' + creds.API_Version__c;

        String thisUri = pardotBase + '/login' + pardotVersion + '?email=' + EncodingUtil.urlEncode(creds.Email__c, 'UTF-8') + '&password=' + EncodingUtil.urlEncode(creds.Password__c, 'UTF-8') + '&user_key=' + EncodingUtil.urlEncode(creds.User_Key__c, 'UTF-8') + '&format=json';
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setTimeout(60*1000);
        
        req.setEndpoint(thisUri);        
        HttpResponse res = makeCallout(req);
        PardotResponse parsedResponse;
        
        try {
            parsedResponse = (PardotResponse)JSON.deserialize(res.getBody().replaceAll('"@attributes"', '"attributes"'), PardotResponse.class);
            if(parsedResponse.attributes.stat == 'ok' && parsedResponse.api_key != null) {
                apiKey = parsedResponse.api_key;
            } else {
                throw new Nebula_API.NebulaApiException('Unexpected response during Pardot login: ' + res.getBody());
            }
        } catch(Exception e) {
            throw new Nebula_API.NebulaApiException('Unexpected response during Pardot login: ' + res.getBody());
        }
    }
    public String callPardot(String uri, String op) {
        return callPardot(uri, op, null);
    }
    public String callPardot(String uri, String op, String rawBody) {
        return callPardot(uri, op, false, rawBody);
    }
    public String callPardot(String uri, String op, boolean isRetry, String rawBody) {
        if(apiKey == null || userKey == null) {
            login();
        }
        
        uri += (uri.indexOf('?') > 0 ? '&' : '?') + 'api_key=' + apiKey + '&user_key=' + userKey + '&format=json';
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setTimeout(119*1000);
        req.setEndpoint(uri);
        if(rawBody != null) {
            req.setBody(rawBody);
        }
        System.debug('Calling Pardot for ' + op + ': ' + uri);
        System.debug('Body ' + rawBody);
        HttpResponse res = makeCallout(req);
        
        Map<String, Object> parsedBody = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        
        Map<String, Object> responseAttributes = (Map<String, Object>)parsedBody.get('@attributes');
        if(responseAttributes == null) {
            throw new Nebula_API.NebulaApiException('Error with ' + op + '. Unable to parse response: ' + res.getBody());
        } else {
            String statusValue = (String)responseAttributes.get('stat');
            if(statusValue != 'ok') {
                integer errCode = (integer)responseAttributes.get('err_code');
                if(errCode == 1) {
                    if(!isRetry) {
                        apiKey = null;
                        userKey = null;
                        System.debug('Pardot authentication rejected. Logging in again...');
                        return callPardot(uri, op, true, rawBody);
                    } else {
                        throw new Nebula_API.NebulaApiException('Pardot authentication rejected twice. Aborting ' + op + ' : ' + res.getBody());
                    }
                } else {
                    throw new Nebula_API.NebulaApiException('Error with ' + op + ' : ' + res.getBody());
                }
            }
            
        }
        return res.getBody();
        
    }

    public Map<String, Object> getVisitorInfo(String visitorId) {
    	String resultBody = callPardot(pardotBase + '/visitor' + pardotVersion + '/do/read/id/' + visitorId,
										'getVisitorInfo');

    	Map<String, Object> parsedResult = (Map<String, Object>)JSON.deserializeUntyped(resultBody);
    	if(parsedResult.containsKey('visitor')) {
    		return (Map<String, Object>)parsedResult.get('visitor');
    	} else {
    		return null;
    	}
    }
}