trigger CpdAssessmentSubmitted on CPD_Assessment__c (before update, after update) {

    Set<Id> submittedAssessmentIds = new Set<Id>();
    
    for(Integer i=0; i < Trigger.size; i++) {
        CPD_Assessment__c oldAssessment = Trigger.old[i];
        CPD_Assessment__c newAssessment = Trigger.new[i];
        
        if(oldAssessment.Status__c != newAssessment.Status__c && newAssessment.Status__c == 'Submitted') {
            if(Trigger.isBefore) {
                newAssessment.Date_Time_Taken__c = DateTime.now();
            }else {
	            submittedAssessmentIds.add(newAssessment.Id);
            }
        }
    }
    
    if(submittedAssessmentIds.isEmpty()) {
        return;
    }
    
    List<CPD_Assessment_Answer__c> answers =
        [SELECT Id, Answer__c, CPD_Test_Question__r.Answer__c
         FROM CPD_Assessment_Answer__c
         WHERE CPD_Assessment__c IN :submittedAssessmentIds];
    
    for(CPD_Assessment_Answer__c answer : answers) {
        answer.Correct__c = answer.Answer__c == answer.CPD_Test_Question__r.Answer__c ? 'true' : 'false';
    }
    
    if(!answers.isEmpty()) {
        update answers;
    }
}