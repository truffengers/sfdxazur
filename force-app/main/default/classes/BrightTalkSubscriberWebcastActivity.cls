public class BrightTalkSubscriberWebcastActivity extends XmlRepresentableMap {

    public BrightTALK__Webcast__c webcast;

    public BrightTalkSubscriberWebcastActivity() {
        super(new List<String>{
            'lastUpdated',
            'totalViewings',
            'webcast.id',
            'user->realmUserId'
                },
             'subscriberWebcastActivity',
              new Map<String, StringToObject>{
                  'lastUpdated' => new DateTimeParser(),
                  'totalViewings' => new IntegerParser()
                  });
        setSortOnField('lastUpdated');
    }
    public BrightTalkSubscriberWebcastActivity(Integer totalViewings, DateTime lastUpdated, String webcastId, String userRealmId) {
        this();
        data.put('totalViewings', totalViewings);
        data.put('lastUpdated', lastUpdated);
        data.put('webcast.id', webcastId);
        data.put('user->realmUserId', userRealmId);
    }

    public Integer getTotalViewings() {
        return (Integer)data.get('totalViewings');
    }
    
    public DateTime getLastUpdated() {
        return (DateTime)data.get('lastUpdated');
    }
    
    public String getWebcastId() {
        return (String)data.get('webcast.id');
    }
    
    public String getUserRealmId() {
        return (String)data.get('user->realmUserId');
    }
}