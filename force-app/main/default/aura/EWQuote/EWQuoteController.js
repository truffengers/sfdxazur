({
    doInit: function (component, event, helper) {
        helper.doInit(component);
    },
    loadData: function (component, event, helper) {
        helper.getRecords(component);
    }
})