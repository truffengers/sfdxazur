/**
  * Author: Azur Group
  * Created Date: 5-Mar-2021 
  * Description: Created for updating Budget record, called from Process Builder
 */

public with sharing class AZUpdateBudgetHelper {
    @InvocableMethod
    public static void updateBudgetLatestVersionFlag (List<AZ_Budget__c> budget) {
        Id budgetId         = budget[0].Id;
        String budgetYear   = budget[0].AZ_BudgetYear__c;
        Id productId        = budget[0].AZ_Product__c;
        List<AZ_Budget__c> budgetToUpdate = new List<AZ_Budget__c>();
        
        
        try{        
            List<AZ_Budget__c> budgetList = [SELECT Id, AZ_LatestVersion__c, AZ_LockBudget__c FROM AZ_Budget__c 
                                                WHERE AZ_Product__c = :productId AND AZ_BudgetYear__c = :budgetYear];
            
            
            For (AZ_Budget__c loopBudget : budgetList){
                if(loopBudget.Id == budgetId){
                    loopBudget.AZ_LatestVersion__c = true;
                    //loopBudget.AZ_LockBudget__c = true;
                    budgetToUpdate.add(loopBudget);                    
                }
                else if(loopBudget.AZ_LatestVersion__c){
                    loopBudget.AZ_LatestVersion__c = false;
                    budgetToUpdate.add(loopBudget);
                }    
                
            }  
            if(budgetToUpdate != null){
                 update budgetToUpdate;
            } 
            

         } catch (Exception e) {
            LogBuilder logBuilder = new LogBuilder();
            
            Azur_Log__c log = logBuilder.createGenericLog(e);
            log.Class__c = AZIntermediationPipelineHelper.class.getName();
            log.Method__c = 'updateBudgetLatestVersionFlag';
            log.Log_message__c = log.Log_message__c;
            log.Log_message__c += '\n\nln: ' + e.getLineNumber();
            log.Log_message__c += '\n\nst: ' + e.getStackTraceString();
            
            insert log;

        }                     
          
    } 
}