/**
* @name: PHO_PhoenixStagingTranslatorSecondarTest
* @description: This class contains test methods for PHO_PhoenixStagingTranslatorSecondary class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 14/01/2020
* @lastChangedBy: Antigoni D'Mello  26/01/2021  PR-113: Changed carrier DR Code and added Carrier in products
* @lastChangedBy: Antigoni D'Mello  14/01/2021  PR-129: Added fixed commission rate in products
* @lastChangedBy: Antigoni D'Mello  14/04/2021  PR-135: Added assertion for transaction line Azur DD Fee
*/
@IsTest
public class PHO_PhoenixStagingTranslatorSecondarTest {

    private static final String CURRENCY_GBP = 'GBP';

    @TestSetup
    private static void createTestData() {
        String brokerDrCode = 'BAD';
        String carrierDrCode = 'CR0008';
        String mgaDrCode = 'MAD';

        Account dummyAccount = TestDataFactory.createBrokerageAccount('Dummy Account');

        Account producingBrokerAccount1 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAccount_DR8059');
        producingBrokerAccount1.EW_DRCode__c = 'DR8059';
        producingBrokerAccount1.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;
        Account producingBrokerAccount2 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAccount_DR8522');
        producingBrokerAccount2.EW_DRCode__c = 'DR8522';
        producingBrokerAccount2.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account accountingBrokerAccount = TestDataFactory.createBrokerageAccount('TestAccountingBrokerAccount');
        accountingBrokerAccount.EW_DRCode__c = brokerDrCode;
        accountingBrokerAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account carrierAccount = TestDataFactory.createCarrierAccount('TestCarrierAccount');
        carrierAccount.EW_DRCode__c = carrierDrCode;
        carrierAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account mgaAccount = TestDataFactory.createBrokerageAccount('TestMGAAccount');
        mgaAccount.EW_DRCode__c = mgaDrCode;
        mgaAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        insert new List<Account>{
                dummyAccount,
                producingBrokerAccount1,
                producingBrokerAccount2,
                accountingBrokerAccount,
                carrierAccount,
                mgaAccount
        };

        IBA_IBACustomSettings__c settings = new IBA_IBACustomSettings__c (
                PHO_PhoenixAccBrokerAccountDRCode__c = brokerDrCode,
                PHO_PhoenixMGAAccountDRCode__c = mgaDrCode,
                PHO_FixedHomeBrokerCommission__c = 36.7,
                PHO_FixedMotorBrokerCommission__c = 26.45,
                IBA_DummyAccount__c = dummyAccount.Id
        );
        insert settings;

        carrierAccount = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Carrier'];

        Product2 p1 = new Product2(Name = 'Motor Comprehensive', PHO_PhoenixProductCode__c = 'MOT01',  PHO_PhoenixSectionCode__c = 'MT01',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor', IBA_FixedBrokerCommissionRate__c = 26.45);
        Product2 p2 = new Product2(Name = 'Motor Uninsured Loss Recovery', PHO_PhoenixProductCode__c = 'MOT01', PHO_PhoenixSectionCode__c = 'MT02',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor', IBA_FixedBrokerCommissionRate__c = 26.45);
        Product2 p3 = new Product2(Name = 'Motor Breakdown', PHO_PhoenixProductCode__c = 'MOT01', PHO_PhoenixSectionCode__c = 'MT03',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor', IBA_FixedBrokerCommissionRate__c = 26.45);
        insert new List<Product2>{p1,p2,p3};

        List<PHO_PhoenixStaging__c> stagingRecords = Test.loadData(PHO_PhoenixStaging__c.SObjectType, 'PHO_PhoenixStagingTestDataSet1');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsSingleStagingRecord() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                        ' LIMIT 1');
        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT PHO_PHMGACommission__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(1, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'This Policy should be active');
        System.assertEquals(1, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 1 transaction record assigned as it\'s an active Policy');
        System.assertEquals(1, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(1, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(1, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        System.assertEquals(1, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        System.assertEquals(stagingRecords[0].PHO_MGACommission__c, transactionLines[0].PHO_PHMGACommission__c, 'Transaction Line should contain financial data from staging record');

        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsAllNewRecords() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c');
        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                        IBA_ActivePolicy__c,
                (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT PHO_PHMGACommission__c,
                        PHO_PolicyTransactionLineSeedSecondary__c,
                        IBA_AzurDDFee__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(3, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'This Policy should be active');
        System.assertNotEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[1].Policy_Status__c, 'This Policy should not be active');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[2].Policy_Status__c, 'This Policy should be active');
        System.assert(policies[1].Name.endsWith('V1'), 'This Policy should be a version 1 snapshot policy');
        System.assertEquals(policies[0].Id, policies[1].IBA_ActivePolicy__c, 'Snapshot Policy should be assigned to active Policy');
        System.assertEquals(2, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 2 transaction records assigned as it\'s an active Policy');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(5, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        Map<String, Decimal> MGACommissionByTransactionLineSeed = new Map<String, Decimal>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            Decimal aggregatedCommission = MGACommissionByTransactionLineSeed.get(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c);
            if (aggregatedCommission != null) {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c, aggregatedCommission + stagingRecord.PHO_MGACommission__c);
            } else {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c, stagingRecord.PHO_MGACommission__c);
            }
        }
        System.assertEquals(5, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        for (IBA_PolicyTransactionLine__c transactionLine : transactionLines) {
            System.assertEquals(MGACommissionByTransactionLineSeed.get(transactionLine.PHO_PolicyTransactionLineSeedSecondary__c), transactionLine.PHO_PHMGACommission__c, 'Transaction Line should contain financial data aggregated from all staging records with the same transaction line seed');
            System.assertEquals(0, transactionLine.IBA_AzurDDFee__c, 'Transaction Line Azur DD Fee should be zero');
        }

        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    @IsTest
    private static void testTranslateToSalesforceObjectsUpdateRecords() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c');
        List<PHO_PhoenixStaging__c> stagingRecords1 = new List<PHO_PhoenixStaging__c>();
        List<PHO_PhoenixStaging__c> stagingRecords2 = new List<PHO_PhoenixStaging__c>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            if ('1504005871'.equals(stagingRecord.PHO_AzurPolicyNumber__c)
                    && stagingRecord.PHO_EffectiveDate__c.date() == Date.newInstance(2019, 1, 2)) {
                stagingRecords1.add(stagingRecord);
            } else {
                stagingRecords2.add(stagingRecord);
            }
        }
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecords1);

        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecords2);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                        IBA_ActivePolicy__c,
                (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT PHO_PHMGACommission__c,
                        PHO_PolicyTransactionLineSeedSecondary__c,
                        IBA_AzurDDFee__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(3, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'This Policy should be active');
        System.assertNotEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[1].Policy_Status__c, 'This Policy should not be active');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[2].Policy_Status__c, 'This Policy should be active');
        System.assert(policies[1].Name.endsWith('V1'), 'This Policy should be a version 1 snapshot policy');
        System.assertEquals(policies[0].Id, policies[1].IBA_ActivePolicy__c, 'Snapshot Policy should be assigned to active Policy');
        System.assertEquals(2, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 2 transaction records assigned as it\'s an active Policy');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(5, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(3, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        Map<String, Decimal> MGACommissionByTransactionLineSeed = new Map<String, Decimal>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            Decimal aggregatedCommission = MGACommissionByTransactionLineSeed.get(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c);
            if (aggregatedCommission != null) {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c, aggregatedCommission + stagingRecord.PHO_MGACommission__c);
            } else {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c, stagingRecord.PHO_MGACommission__c);
            }
        }
        System.assertEquals(5, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        for (IBA_PolicyTransactionLine__c transactionLine : transactionLines) {
            System.assertEquals(MGACommissionByTransactionLineSeed.get(transactionLine.PHO_PolicyTransactionLineSeedSecondary__c), transactionLine.PHO_PHMGACommission__c, 'Transaction Line should contain financial data aggregated from all staging records with the same transaction line seed');
            System.assertEquals(0, transactionLine.IBA_AzurDDFee__c, 'Transaction Line Azur DD Fee should be zero');
        }

        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    // test the case when primary transaction with matching seed already exists,
    // so we should only mark existing operational records as phoenix processed and aggregate financial data
    @IsTest
    private static void testTranslateToSalesforceObjectsExistingALPsData() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                        ' LIMIT 1');
        // prepare ALPs policy based on phoenix staging record
        Asset alpsPolicy = new Asset(
                AccountId = PHO_Constants.DUMMY_ACCOUNT_ID,
                IBA_AccountingBroker__c = stagingRecords[0].PHO_AccountingBroker__c,
                Policy_Number__c = stagingRecords[0].PHO_AzurPolicyNumber__c,
                Carrier__c = stagingRecords[0].PHO_CarrierAccount__c,
                vlocity_ins__EffectiveDate__c = stagingRecords[0].PHO_EffectiveDate__c.date(),
                IBA_ProducingBrokerAccount__c = stagingRecords[0].PHO_ProducingBroker__c,
                EW_Version_Number__c = 1,
                Name = stagingRecords[0].PHO_AzurPolicyNumber__c,
                Policy_Status__c = PHO_Constants.POLICY_STATUS_ACTIVE,
                PHO_PhoenixProcessed__c = false,
                RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get(PHO_Constants.POLICY_RT_HIGH_NET_WORTH).getRecordTypeId()
        );
        insert alpsPolicy;
        vlocity_ins__AssetCoverage__c coverage = new vlocity_ins__AssetCoverage__c (
                vlocity_ins__Product2Id__c = stagingRecords[0].PHO_Product__c,
                Name = stagingRecords[0].PHO_ProductName__c,
                vlocity_ins__PolicyAssetId__c = alpsPolicy.Id,
                IBA_Status__c = PHO_Constants.POLICY_COVERAGE_STATUS_ACTIVE,
                PHO_PhoenixProcessed__c = false
        );
        insert coverage;
        IBA_PolicyTransaction__c policyTransaction = new IBA_PolicyTransaction__c (
                IBA_OriginalPolicy__c = alpsPolicy.Id,
                IBA_ActivePolicy__c = alpsPolicy.Id,
                IBA_EffectiveDate__c = stagingRecords[0].PHO_EffectiveDate__c.date(),
                PHO_PhoenixProcessed__c = false,
                PHO_PrimaryRecord__c = true
        );
        insert policyTransaction;
        IBA_FFPolicyCarrierLine__c carrierLine = new IBA_FFPolicyCarrierLine__c (
                IBA_CarrierAccount__c = stagingRecords[0].PHO_CarrierAccount__c,
                IBA_PolicyTransaction__c = policyTransaction.Id,
                PHO_PhoenixProcessed__c = false
        );
        insert carrierLine;
        IBA_PolicyTransactionLine__c transactionLine = new IBA_PolicyTransactionLine__c (
                IBA_Product__c = stagingRecords[0].PHO_Product__c,
                IBA_PolicyTransaction__c = policyTransaction.Id,
                IBA_PolicyCarrierLine__c = carrierLine.Id,
                PHO_PhoenixProcessed__c = false
        );
        insert transactionLine;

        // Perform test
        Test.startTest();
        // process the same staging record in translator so that it should match previously created policy (and all related records)
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT PHO_PhoenixProcessed__c,
                (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<vlocity_ins__AssetCoverage__c> coverages = [
                SELECT PHO_PhoenixProcessed__c
                FROM vlocity_ins__AssetCoverage__c
        ];
        List<IBA_PolicyTransaction__c> transactions = [
                SELECT PHO_PhoenixProcessed__c
                FROM IBA_PolicyTransaction__c
        ];
        List<IBA_FFPolicyCarrierLine__c> carrierLines = [
                SELECT PHO_PhoenixProcessed__c
                FROM IBA_FFPolicyCarrierLine__c
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT PHO_PHMGACommission__c,
                        PHO_PhoenixProcessed__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY Name
        ];

        System.assertEquals(1, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(1, policies[0].FF_Policy_Transactions__r.size(), 'This Policy should have 1 transaction record assigned as it\'s an active Policy');
        System.assertEquals(1, transactions.size(), 'Wrong number of Policy Transaction records created');

        System.assertEquals(1, coverages.size(), 'Wrong number of Policy Coverage records created');

        System.assertEquals(1, carrierLines.size(), 'Wrong number of Policy Carrier Line records created');

        System.assertEquals(1, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        // We created fake ALPs transacton line based on this staging record before, so after processing this staging record in translator, all financial data from it should be added again to already existing transaction line
        System.assertEquals(stagingRecords[0].PHO_MGACommission__c, transactionLines[0].PHO_PHMGACommission__c, 'Transaction Line should contain financial data from the same staging record');

        System.assert(policies[0].PHO_PhoenixProcessed__c, 'Policy should become phoenix processed');
        System.assert(coverages[0].PHO_PhoenixProcessed__c, 'Coverage should become phoenix processed');
        System.assert(transactions[0].PHO_PhoenixProcessed__c, 'Transaction should become phoenix processed');
        System.assert(carrierLines[0].PHO_PhoenixProcessed__c, 'Carrier line should become phoenix processed');
        System.assert(transactionLines[0].PHO_PhoenixProcessed__c, 'Transaction line should become phoenix processed');
        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All staging records should be processed');
    }

    // test that no snapshot should be created when staging record effective date is older than currently active transaction
    @IsTest
    private static void testTranslateToSalesforceObjectsOlderEffectiveDate() {
        // Prepare data
        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c');
        List<PHO_PhoenixStaging__c> stagingRecordsOlder = new List<PHO_PhoenixStaging__c>();
        List<PHO_PhoenixStaging__c> stagingRecordsNewer = new List<PHO_PhoenixStaging__c>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            if ('1504005871'.equals(stagingRecord.PHO_AzurPolicyNumber__c)) {
                if (stagingRecord.PHO_EffectiveDate__c.date() == Date.newInstance(2019, 1, 2)) {
                    stagingRecordsOlder.add(stagingRecord);
                } else {
                    stagingRecordsNewer.add(stagingRecord);
                }
            }
        }
        // create transaction with newer effective date
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecordsNewer);

        // Perform test
        Test.startTest();
        // process staging records with older effective date
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecordsOlder);
        Test.stopTest();

        // Verify results
        List<Asset> policies = [
                SELECT Name,
                        Policy_Status__c,
                        IBA_ActivePolicy__c,
                (SELECT Id FROM FF_Policy_Transactions__r)
                FROM Asset
                ORDER BY Name
        ];
        List<IBA_PolicyTransactionLine__c> transactionLines = [
                SELECT PHO_PHMGACommission__c,
                        PHO_PolicyTransactionLineSeedSecondary__c,
                        IBA_AzurDDFee__c
                FROM IBA_PolicyTransactionLine__c
                ORDER BY IBA_PolicyTransaction__r.IBA_OriginalPolicy__r.Name
        ];

        System.assertEquals(2, policies.size(), 'Wrong number of Policy records created');
        System.assertEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[0].Policy_Status__c, 'Newer Policy should be active');
        System.assertNotEquals(PHO_Constants.POLICY_STATUS_ACTIVE, policies[1].Policy_Status__c, 'Older Policy should not be active');
        System.assert(policies[1].Name.endsWith('V1'), 'This Policy should be a version 1 snapshot policy');
        System.assertEquals(policies[0].Id, policies[1].IBA_ActivePolicy__c, 'Older Policy should be assigned to active Policy');
        System.assertEquals(2, policies[0].FF_Policy_Transactions__r.size(), 'Newer Policy should have 2 transaction records assigned as it\'s an active Policy');

        System.assertEquals(2, [SELECT COUNT() FROM IBA_PolicyTransaction__c], 'Wrong number of Policy Transaction records created');

        System.assertEquals(2, [SELECT COUNT() FROM vlocity_ins__AssetCoverage__c], 'Wrong number of Policy Coverage records created');

        System.assertEquals(2, [SELECT COUNT() FROM IBA_FFPolicyCarrierLine__c], 'Wrong number of Policy Carrier Line records created');

        Map<String, Decimal> MGACommissionByTransactionLineSeed = new Map<String, Decimal>();
        for (PHO_PhoenixStaging__c stagingRecord : stagingRecords) {
            Decimal aggregatedCommission = MGACommissionByTransactionLineSeed.get(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c);
            if (aggregatedCommission != null) {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c, aggregatedCommission + stagingRecord.PHO_MGACommission__c);
            } else {
                MGACommissionByTransactionLineSeed.put(stagingRecord.PHO_PolicyTransactionLineSeedSecondary__c, stagingRecord.PHO_MGACommission__c);
            }
        }
        System.assertEquals(2, transactionLines.size(), 'Wrong number of Policy Transaction Line records created');
        for (IBA_PolicyTransactionLine__c transactionLine : transactionLines) {
            System.assertEquals(MGACommissionByTransactionLineSeed.get(transactionLine.PHO_PolicyTransactionLineSeedSecondary__c), transactionLine.PHO_PHMGACommission__c, 'Transaction Line should contain financial data aggregated from all staging records with the same transaction line seed');
            System.assertEquals(0, transactionLine.IBA_AzurDDFee__c, 'Transaction Line Azur DD Fee should be zero');
        }

        System.assertEquals(stagingRecordsOlder.size() + stagingRecordsNewer.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All selected staging records should be processed');
    }

    @IsTest
    private static void volumeTestTranslateToSalesforceObjects() {
        Integer recordLimit = 200;
        // Prepare data
        Product2 p1 = new Product2(Name = 'MOT02_MT01', PHO_PhoenixProductCode__c = 'MOT02',  PHO_PhoenixSectionCode__c = 'MT01');
        Product2 p2 = new Product2(Name = 'MOT02_MT02', PHO_PhoenixProductCode__c = 'MOT02', PHO_PhoenixSectionCode__c = 'MT02');
        Product2 p3 = new Product2(Name = 'MOT02_MT03', PHO_PhoenixProductCode__c = 'MOT02', PHO_PhoenixSectionCode__c = 'MT03');
        Product2 p4 = new Product2(Name = 'MOT02_MT04', PHO_PhoenixProductCode__c = 'MOT02', PHO_PhoenixSectionCode__c = 'MT04');
        Product2 p5 = new Product2(Name = 'PCG01_PC01', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC01');
        Product2 p6 = new Product2(Name = 'PCG01_PC02', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC02');
        Product2 p7 = new Product2(Name = 'PCG01_PC03', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC03');
        Product2 p8 = new Product2(Name = 'PCG01_PC04', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC04');
        Product2 p9 = new Product2(Name = 'PCG01_PC05', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC05');
        Product2 p10 = new Product2(Name = 'PCG01_PC07', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC07');
        Product2 p11 = new Product2(Name = 'PCG01_PC08', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC08');
        Product2 p12 = new Product2(Name = 'PCG01_PC09', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC09');
        Product2 p13 = new Product2(Name = 'PCG01_PC10', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC10');
        Product2 p14 = new Product2(Name = 'PCG01_PC11', PHO_PhoenixProductCode__c = 'PCG01', PHO_PhoenixSectionCode__c = 'PC11');
        insert new List<Product2>{p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};

        Test.loadData(PHO_PhoenixStaging__c.SObjectType, 'PHO_PhoenixStagingTestDataSet2');

        List<PHO_PhoenixStaging__c> stagingRecords = Database.query(
                'SELECT ' + String.join(PHO_Constants.STAGING_RELEVANT_FIELDS_SECONDARY, ',') +
                        ' FROM PHO_PhoenixStaging__c' +
                        ' WHERE PHO_Status__c = \''+PHO_Constants.STAGING_STATUS_READY+'\'' +
                        ' ORDER BY PHO_AzurPolicyNumber__c, PHO_EffectiveDate__c, PHO_EventNumber__c' +
                        ' LIMIT :recordLimit');
        // Perform test
        Test.startTest();
        PHO_PhoenixStagingTranslatorSecondary.translateToSalesforceObjects(stagingRecords);
        Test.stopTest();

        // Verify results
        System.assertEquals(stagingRecords.size(), [SELECT COUNT() FROM PHO_PhoenixStaging__c WHERE PHO_RecordProcessed__c = TRUE], 'All selected staging records should be processed');
    }
}