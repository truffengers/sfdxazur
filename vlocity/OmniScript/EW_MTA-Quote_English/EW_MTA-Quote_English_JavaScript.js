let observer = new MutationObserver(e => {
	const hasId = e[0].target.attributes.id;
	const hasIdAndAriaHidden = hasId && e[0].target.attributes['aria-hidden'];

    const isNotRight = e[0].target.attributes.class
        && e[0].target.attributes.class.value.includes('somethingNotRight');

	if (!hasIdAndAriaHidden && !isNotRight) {
		return;
	}

	const isHidingPropDetails = e[0].target.attributes.id.value === 'propertyDetails'
		&& e[0].target.attributes['aria-hidden'].value === 'true';

	const isShowingUWRefer = e[0].target.attributes.id.value === 'UnderwritingRefer'
		&& e[0].target.attributes['aria-hidden'].value === 'false';

	const isShowingDeclined = e[0].target.attributes.id.value === 'UnderwritingDecline'
		&& e[0].target.attributes['aria-hidden'].value === 'false';

	const isShowingPostcodeDeclined = e[0].target.attributes.id.value === 'UnderwritingDeclinedExcludedPostcode';

	const isShowingDuplicatePropertyDeclined = e[0].target.attributes.id.value === 'UnderwritingDeclinedDuplicateProperty';

	const isShowingCoverage = e[0].target.attributes.id.value === 'coverage'
		&& e[0].target.attributes['aria-hidden'].value === 'false';

	const contentElement = parent.document.getElementsByClassName('contentRegion')[0];
	const loadingElement = parent.document.getElementsByTagName('c-ew_loading')[0];

	if (isHidingPropDetails) {
		window.scrollTo(0, 0);
		contentElement.hidden = true;
		loadingElement.hidden = false;
		loadingElement.scrollIntoView();
	} else if (isShowingUWRefer
		|| isShowingDeclined
		|| isShowingCoverage
		|| isShowingPostcodeDeclined
		|| isShowingDuplicatePropertyDeclined
		|| isNotRight) {
		contentElement.hidden = false;
		loadingElement.hidden = true;
	}
});

[...document.getElementsByTagName('*')].forEach(node => {
	if (!window.chrome) {
		return;
	}

	observer.observe(node, {
		childList: true,
		attributes: true,
		characterData: true,
		subtree: true
	});
});

document.addEventListener("visibilitychange", function() {
	const contentElement = parent.document.getElementsByClassName('contentRegion')[0];
	const loadingElement = parent.document.getElementsByTagName('c-ew_loading')[0];

	const referElement = document.getElementById('UnderwritingRefer');
	const declineElement = document.getElementById('UnderwritingDecline');
	const postcodeDeclineElement = document.getElementById('UnderwritingDeclinedExcludedPostcode');
	const duplicatePropertyDeclineElement = document.getElementById('UnderwritingDeclinedDuplicateProperty');
	const coverageElement = document.getElementById('coverage');
	const errorElement = document.getElementsByClassName('somethingNotRight');

	const showingRefer = referElement
		&& referElement.getAttribute('aria-hidden') === 'false';

	const showingDecline = declineElement
		&& declineElement.getAttribute('aria-hidden') === 'false';

	const showingPostcodeDeclined = postcodeDeclineElement;

	const showingDuplicatePropertyDeclined = duplicatePropertyDeclineElement;

	const showingCoverage = coverageElement
		&& coverageElement.getAttribute('aria-hidden') === 'false';

	const showingError = !!errorElement.length;

	const hideLoadingBar = showingRefer
		|| showingDecline
		|| showingPostcodeDeclined
		|| showingDuplicatePropertyDeclined
		|| showingCoverage
		|| showingError;

	if (document.visibilityState === 'visible' && hideLoadingBar) {
		contentElement.hidden = false;
		loadingElement.hidden = true;
	}
});