@IsTest
private class EWLightningForgotPasswordControllerTest {
    static testMethod void testBehavior() {
        System.assertEquals(Label.Site.invalid_email, EWLightningForgotPasswordController.forgotPassword('fakeUser', 'http://a.com'));
        System.assertEquals(EWLightningForgotPasswordController.INVALID_USERNAME, EWLightningForgotPasswordController.forgotPassword('example@example.com', 'http://abc.com'));


        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        User systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        User brokerUser;

        System.runAs(systemAdminUser) {
            insert new EWFeatureSwitches__c(InsuredDuplication__c = true);
            Id partnerProfileId = [SELECT Id FROM Profile WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME].Id;

            Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
            Contact brokerContact1 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser = EWUserDataFactory.createPartnerUser('User1', partnerProfileId, brokerContact1.Id, false);
            brokerUser.Username = 'testUserNameBroker@test.com';
            insert brokerUser;
        }

        System.assertEquals(null, EWLightningForgotPasswordController.forgotPassword('testUserNameBroker@test.com', 'http://example.com'));
        System.assertEquals('Argument 1 cannot be null', EWLightningForgotPasswordController.forgotPassword('testUserNameBroker@test.com', null));
        System.assertEquals(null, EWLightningForgotPasswordController.forgotPassword('testUserNameBroker@test.com', ''));
    }
}