@isTest
private class BrokerIqContributorPortraitLinkTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();
	private static Contact testContact;
	private static ContentVersion cv;
	private static Broker_iQ_Contributor__c contributor;
	static {
		testContact = tog.getContactNoInsert();
		testContact.Is_Broker_iQ_Contributor__c = true;
		insert testContact;
		contributor = [SELECT Id FROM Broker_iQ_Contributor__c WHERE Contact__c = :testContact.Id];
		cv = tog.getContentVersion('foo.txt', Blob.valueOf('foo'));
	}

	@isTest static void basic() {

		contributor.Portrait__c = cv.ContentDocumentId;

		update contributor;
		contributor = [SELECT Id, Portrait_URL__c, Portrait__c 
				FROM Broker_iQ_Contributor__c
				WHERE Id = :contributor.Id];

		List<ContentDistribution> distribution = [SELECT Id 
													FROM ContentDistribution 
													WHERE ContentDocumentId = :contributor.Portrait__c];

		System.assertEquals(1, distribution.size());
		System.assertNotEquals(null, contributor.Portrait_URL__c);
	}
}