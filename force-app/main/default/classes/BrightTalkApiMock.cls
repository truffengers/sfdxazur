public class BrightTalkApiMock extends Nebula_Api.NebulaApiMock {

    public BrightTALK_API_Settings__c settings;

    public Map<String, String> responseParams;

    public Boolean returnEmpty = false;
    public Boolean includeNextLink = false;

    public BrightTalkApiMock(BrightTALK_API_Settings__c settings, integer statusCode) {
        super(statusCode);
        init(settings);
    }

    public BrightTalkApiMock(BrightTALK_API_Settings__c settings, boolean throwException) {
        super(throwException);
        init(settings);
    }

    private void init(BrightTALK_API_Settings__c settings) {
        this.settings = settings;

        responseParams = new Map<String, String> {
            'webcast_id' => '98840',
            'realm_id' => settings.Realm_Id__c,
            'realm_user_id' => '0030Y000001qztsQAA',
            'total_viewings' => '68',
            'webcast_status' => 'recorded',
            'webcast_preview' => 'https://www.stage.brighttalk.net/communication/98840/slide1_001.png',
            'webcast_thumbnail' => 'https://www.stage.brighttalk.net/communication/98840/tn1_1.png',
            'webcast_duration' => '129',
            'last_updated' => '2016-11-02T14:12:36Z'
        };
    }
    
    private static String getRegexedField(String fieldName) {
        return '\\{\\!' + fieldName + '\\}';
    }

    public String getMergedResponse(String resourceName) {
        StaticResource response = [SELECT Id, Body FROM StaticResource WHERE Name = :resourceName];
        String rval;
        if(response != null) {
            rval = response.Body.toString();

            for(String key : responseParams.keySet()) {
                rval = rval.replaceAll(getRegexedField(key), responseParams.get(key));
            }
        }
        return rval;
    }

    public override HttpResponse respond(HTTPRequest req, HTTPResponse rval) {
        
        String endpoint = req.getEndpoint();
        String body = '';
        StaticResource response;

        if(endpoint.contains('/subscribers_webcast_activity')) {
            if(returnEmpty) {
                body = '<?xml version="1.0" encoding="UTF-8"?>';
            } else {
                body = getMergedResponse('subscribers_webcast_activity_response');
            }

            if(includeNextLink) {
                body += '<link rel="next" href="https://api.brighttalk.com/v1/channel/14433/subscribers_webcast_activity?cursor=49504523-1489406173" />';
                includeNextLink = false;
            }
        } else if(endpoint.endsWith('/feed')) {
            body = getMergedResponse('feed_response');
        }


        if(body != null) {
            rval.setBody(body);
        }    
        
        return rval;
    }    
}