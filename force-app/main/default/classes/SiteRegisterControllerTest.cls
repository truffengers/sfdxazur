/**
* @name: SiteRegisterControllerTest
* @description: Class containing tests for SiteRegisterController
* @author: Azur Group
* @date: 2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
*/
@IsTest public with sharing class SiteRegisterControllerTest {
    @IsTest
    static void testRegistration() {
        SiteRegisterController controller = new SiteRegisterController();
        controller.username = 'test@force.com';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
        System.assert(controller.registerUser() == null);  
    }
}