/**
* @name: SiteProviderControllerTest
* @description: Test class for SiteProviderControllerTest
* @date:01 /11/2017
* @modifiedBy: Antigoni D'Mello	21/12/2017	AZ-77: Added private method getBaseSecureUrl
*/
@IsTest
private class SiteProviderControllerTest
{
    /*
        The purpose of this test class and test method is solely code coverage,
        for the test class 'SiteProviderController'. There isn't a test setup method,
        as there isn't any data to setup.
    */
    @isTest
    public static void itShouldReturnSiteMetadata()
    {
        Test.startTest();

        system.assertEquals(null, SiteProviderController.getSiteMetadata().adminEmail);
        system.assertNotEquals(null, SiteProviderController.getSiteMetadata().pathPrefix);
        system.assertNotEquals(null, SiteProviderController.getSiteMetadata().isSandbox);

        Test.stopTest();
    }
}