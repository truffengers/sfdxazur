({
    setupCarousel : function(component) {
        var carouselArticles = component.get('v.carouselArticles');
        if(carouselArticles.length > 0) {
            TT.slidesData = [];
            TT.postsData = [];
            
            carouselArticles.forEach(function(thisArticle) {
                var postToPush;
                var dataToPush;
                if(thisArticle.Primary_Topic__c && thisArticle.RecordType && thisArticle.Broker_iQ_Contributor__r) {
                    var thisBgSet = '';
                    for(var field in thisArticle) {
                        var matchResult = /Carousel_Image_([\d]+)_Cache__c/.exec(field);

                        if(matchResult) {
                            var urlVal = thisArticle[field];

                            if(urlVal) {
                                thisBgSet += urlVal + ' ' + matchResult[1] + 'w, ';
                            }
                        }
                    }
                    if(thisBgSet.length > 0) {
                        thisBgSet.substring(0, thisBgSet.length - ', '.length);

                        dataToPush = {
                            "bgset":thisBgSet,
                            "sizes":"auto",
                            "parent-fit":"cover"
                        };


                        postToPush = {
                            topic: thisArticle.Primary_Topic__c,
                            contentType: ' ' + thisArticle.RecordType.Name,
                            title: thisArticle.Name,
                            subtitle: new Date(thisArticle.Content_Date__c).toLocaleDateString('en-GB', {day: 'numeric', month: 'long', year: 'numeric'}) + ' ⋅ ' + thisArticle.Content_Duration__c,
                            callback: $A.getCallback(function() {
                                var navEvt = $A.get("e.force:navigateToSObject");
                                navEvt.setParams({
                                    "recordId": thisArticle.Id
                                });
                                navEvt.fire();
                            }),
                            contributor: {
                                name: thisArticle.Broker_iQ_Contributor__r.Full_Name__c,
                                image: thisArticle.Broker_iQ_Contributor__r.Portrait_Cache__c
                            }
                        };
                    }
                }

                if(!$A.util.isEmpty(postToPush)) {
                    TT.postsData.push(postToPush);
                    TT.slidesData.push(dataToPush);
                }
// Removed for production
//                else {
//                    console.log('Dropped article from Carousel due to missing data');
//                    console.log(thisArticle);
//                    console.log(dataToPush);
//                }
            });
            
            TT.svgSpritePath = $A.get('$Resource.broker_iq_v2') + '/assets/images/sprite.svg';
            
            var carouselHome = TT.carouselHome($, lazySizes, Hammer, TT, {
                carouselId    : 'js-carousel',
                svgSpritePath : TT.svgSpritePath,
                postsData     : TT.postsData,
                slidesData    : TT.slidesData,
                automatic     : true,
                advancerShowDuration : 5000
            });
            
            carouselHome.init();
            if(TT && TT.imageParallax) {
                TT.imageParallax.register($('#js-carousel'), {height: '150'});
            }
            component.set('v.jsInitialised', true);
            
        }		
    }
})