/**
 * Created by roystonlloyd on 2019-05-24.
 */

public with sharing class EWClauseAssignmentTriggerHandler implements ITriggerHandler {

    public static Boolean IsDisabled;

    public Boolean IsDisabled()
    {
        return EWClauseAssignmentTriggerHandler.IsDisabled != null ? EWClauseAssignmentTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems){}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        new EWClauseAssignmentHelper().refreshClauses(newItems.values(),null);
        new EWClauseAssignmentHelper().updateRemovedFlag(newItems,null);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        new EWClauseAssignmentHelper().refreshClauses(newItems.values(), oldItems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
        new EWClauseAssignmentHelper().refreshClauses(oldItems.values(),null);
        new EWClauseAssignmentHelper().updateRemovedFlag(null, oldItems);
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
        new EWClauseAssignmentHelper().refreshClauses(oldItems.values(),null);
        new EWClauseAssignmentHelper().updateRemovedFlag(oldItems,null);
    }

}