({
	filterClicked : function(component, event, helper) {
		var srcComponent = event.getSource();
		component.set('v.selectedTag', srcComponent.get('v.filterWrapper'));
	},
	resetClicked : function(component, event, helper) {
		component.set('v.selectedTag', null);
	}
})