/**
* @name: EWOpportunityDataFactory
* @description: Reusable factory methods for opportunity object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 05/09/2018
* @ 06/02/2019 Roy Lloyd, added createOpportunityForJointInsured
*/
@isTest
public class EWOpportunityDataFactory {

    public static Opportunity createOpportunityForJointInsured(String name,
            Id insuredId,
            Id jointId,
            Id brokerId) {

        vlocity_ins__Party__c primaryParty = new vlocity_ins__Party__c(
                vlocity_ins__AccountId__c = jointId,
                vlocity_ins__PartyEntityType__c = 'Household'
        );
        insert primaryParty;

        vlocity_ins__Household__c house = new vlocity_ins__Household__c(
                name ='householdTest',
                vlocity_ins__PrimaryAccountId__c = insuredId,
                vlocity_ins__PartyId__c = primaryParty.Id
        );
        insert house;
        primaryParty.vlocity_ins__HouseholdId__c = house.Id;
        update primaryParty;


        List<vlocity_ins__Party__c> parties = new List<vlocity_ins__Party__c>{
            new vlocity_ins__Party__c(
                vlocity_ins__HouseholdId__c = house.Id,
                vlocity_ins__AccountId__c = jointId,
                vlocity_ins__PartyEntityType__c = 'Account'
            ),
            new vlocity_ins__Party__c(
                vlocity_ins__HouseholdId__c = house.Id,
                vlocity_ins__AccountId__c = insuredId,
                vlocity_ins__PartyEntityType__c = 'Account'
            ),
            new vlocity_ins__Party__c(
                vlocity_ins__HouseholdId__c = house.Id,
                vlocity_ins__PartyEntityType__c = 'Household'
            )
        };
        insert parties;

        Opportunity o = createOpportunity(name,insuredId, brokerId, true);


        vlocity_ins__Application__c app = new vlocity_ins__Application__c(
                vlocity_ins__AccountId__c = insuredId,
                vlocity_ins__PrimaryPartyId__c = primaryParty.Id,
                vlocity_ins__OpportunityId__c = o.Id
        );
        insert app;

        return o;
    }


    public static Opportunity createOpportunity(String name,
                                                Id insuredId,
                                                Id brokerId,
                                                Boolean insertRecord) {

        Id oppRecId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(EWConstants.OPPORTUNITY_EW_RECORD_TYPE).getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.Name = (name == null) ? 'testOpportunity' : name;
        opp.AccountId = insuredId;
        opp.vlocity_ins__AgencyBrokerageId__c = brokerId;
        opp.CloseDate = Date.today().addYears(1);
        opp.StageName = 'Open';
        opp.RecordTypeId = oppRecId;

        if (insertRecord) {
            insert opp;
        }

        return opp;
    }
}