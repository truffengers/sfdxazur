/**
* @name: IBA_JournalTrigger
* @description: Trigger for the managed object c2g__codaJournal__c
*
*/

trigger IBA_JournalTrigger on c2g__codaJournal__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_JournalTriggerHandler());
}