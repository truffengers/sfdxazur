TT.stickySocial = (function($, _) {

  'use strict';

  var element;
  var $window;
  var $element;
  var $parent;
  var fixAt;
  var concealAt;

  var FREE      = 'free';
  var STUCK     = 'stuck';
  var CONCEALED = 'concealed';

  var state = FREE;

  var STUCK_CLASS     = 'is-sticky';
  var CONCEALED_CLASS = 'is-concealed';

  var SCROLL_WAIT_DURATION = 1;
  var RESIZE_WAIT_DURATION = 1000;



  function init(params) {
    element = document.getElementById(params.id);
    if(element) {
      referenceElements(element);
      addListeners();
      setBounds();
    }
  }

  function referenceElements() {
    $window  = $(window);
    $element = $(element);
    $parent  = $element.parent();
  }

  function addListeners() {
    $window.scroll(_.throttle(scrollHandler, SCROLL_WAIT_DURATION, { trailing: false }));
    $window.resize(_.throttle(resizeHandler, RESIZE_WAIT_DURATION, { trailing: false }));
  }

  function scrollHandler() {
    var scrollTop = $window.scrollTop();
    if(getShouldStick(scrollTop)) {
      state = STUCK;
      $element.addClass(STUCK_CLASS);
      $element.removeClass(CONCEALED_CLASS);
    } else if(getShouldFree(scrollTop)) {
      state = FREE;
      $element.removeClass(STUCK_CLASS);
      $element.removeClass(CONCEALED_CLASS);
    } else if(getShouldConceal(scrollTop)) {
      state = CONCEALED;
      $element.removeClass(STUCK_CLASS);
      $element.addClass(CONCEALED_CLASS);
    }
  }

  function setBounds() {
    fixAt     = $parent.offset().top;
    concealAt = fixAt + $parent.height() - element.scrollHeight - parseInt($element.css('margin-top'), 10);
  }

  function getShouldStick(scrollTop) {
    return state !== STUCK && scrollTop >= fixAt && scrollTop < concealAt;
  }

  function getShouldFree(scrollTop) {
    return state !== FREE && scrollTop < fixAt;
  }

  function getShouldConceal(scrollTop) {
    return state !== CONCEALED && scrollTop >= concealAt;
  }

  function resizeHandler() {
    setBounds();
    scrollHandler();
  }



  return {
    init          : init,
    resizeHandler : resizeHandler
  };

}(jQuery, _));