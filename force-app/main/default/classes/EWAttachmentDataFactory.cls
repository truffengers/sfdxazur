/**
* @name: EWAttachmentDataFactory
* @description: Reusable factory methods for Attachment object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 24/09/2018
*/
@isTest
public class EWAttachmentDataFactory {

	public static Attachment createAttachment(String name,
                                          	  Id parentId,
                                          	  Boolean insertRecord) {

        Attachment attach = new Attachment();   	
    	attach.Name = (name == null) ? 'testAttachment' : name;
    	Blob bodyBlob = Blob.valueOf(attach.Name);
    	attach.body = bodyBlob;
        attach.parentId = parentId;

       	if (insertRecord) {
          insert attach;
        }

       	return attach;
    }
}