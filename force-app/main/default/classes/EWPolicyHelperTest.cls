@IsTest
private class EWPolicyHelperTest {

    static testMethod void testPolicyActivationOnCoverages() {
        Asset policy = [
                SELECT Id, Policy_Status__c, (
                        SELECT Id, IBA_Status__c
                        FROM vlocity_ins__PolicyCoverages__r
                )
                FROM Asset
                LIMIT 1
        ];

        System.assertNotEquals(EWPolicyHelper.POLICY_AND_COVERAGE_ACTIVE_STATUS, policy.Policy_Status__c);

        for (vlocity_ins__AssetCoverage__c coverage : policy.vlocity_ins__PolicyCoverages__r) {
            System.assertNotEquals(EWPolicyHelper.POLICY_AND_COVERAGE_ACTIVE_STATUS, coverage.IBA_Status__c);
        }

        policy.Policy_Status__c = EWPolicyHelper.POLICY_AND_COVERAGE_ACTIVE_STATUS;
        update policy;

        List<vlocity_ins__AssetCoverage__c> coverages = new List<vlocity_ins__AssetCoverage__c>([
                SELECT Id, IBA_Status__c
                FROM vlocity_ins__AssetCoverage__c
                WHERE vlocity_ins__PolicyAssetId__c = :policy.Id
        ]);

        for (vlocity_ins__AssetCoverage__c coverage : coverages) {
            System.assertEquals(EWPolicyHelper.POLICY_AND_COVERAGE_ACTIVE_STATUS, coverage.IBA_Status__c);
        }
    }

    static testMethod void testPolicyQuoteJSONDataLinking() {
        //Policy was inserted in the setup - trigger should link EW_QuoteJSONData__c on AfterInsert action.

        Asset policy = [
                SELECT Id, (
                        SELECT Id
                        FROM EW_QuoteJSONData__r
                )
                FROM Asset
                LIMIT 1
        ];

        System.assertNotEquals(0, policy.EW_QuoteJSONData__r.size());
    }

    static testMethod void testPolicyPremiumFix() {
        // before update is triggered by after insert but we need to trigger it here too as this code needs to go ahead of EWH-530

        Asset policy = [
                SELECT Status
                FROM Asset
                LIMIT 1
        ];

        Test.startTest();
        policy.Status = 'Issued';
        update policy;
        Test.stopTest();

        policy = [
                SELECT vlocity_ins__StandardPremium__c, vlocity_ins__AnnualPremium__c, vlocity_ins__TotalPremiumForTerm__c
                FROM Asset
                LIMIT 1
        ];

        System.assertEquals(325.64346, policy.vlocity_ins__StandardPremium__c, 'Standard Premium should be 325.64346');
        System.assertEquals(325.64346, policy.vlocity_ins__AnnualPremium__c, 'Annual Premium should be 325.64346');
        System.assertEquals(325.64346, policy.vlocity_ins__TotalPremiumForTerm__c, 'Total Premium For Term should be 325.64346');
    }

    static testMethod void testPolicyFormulaNumberFieldCS() {
        PolicyNumberRanges__c pnr = new PolicyNumberRanges__c();
        pnr.PolicyRecordTypeDevName__c = 'EW_Policy';
        pnr.CurrentNumber__c = 1;
        pnr.LengthWithZeros__c = 7;
        pnr.Name = 'EW_Policy';
        pnr.PolicyNamePrefix__c = '88-EWA-P-';
        insert pnr;

        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );
        insert new List<Account>{
                broker, insured
        };

        Product2 product1 = EWProductDataFactory.createProduct('Home Emergency', false);
        Product2 product2 = EWProductDataFactory.createProduct('Contents', false);
        insert new List<Product2>{
                product1, product2
        };

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
        insert new PricebookEntry (Product2Id = product1.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
        insert new PricebookEntry (Product2Id = product2.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
        insert new EW_QuoteJSONData__c(EW_Quote__c = quote.Id);

        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        insert policy;

        policy = [SELECT Id, Name FROM Asset WHERE ID = :policy.Id];

        System.assert(policy.Name.contains('0000002'));
    }

    static testMethod void testPolicySnapshot() {
        Asset policy = [SELECT Id FROM Asset LIMIT 1];
        Quote quote = [SELECT Id FROM Quote LIMIT 1];

        insert new List<vlocity_ins__AssetInsuredItem__c>{
                new vlocity_ins__AssetInsuredItem__c(Name = EWQuoteHelper.MAIN_RESIDENCE, vlocity_ins__PolicyAssetId__c = policy.Id, EW_AddressId__c = '123abc'),
                new vlocity_ins__AssetInsuredItem__c(Name = EWQuoteHelper.MAIN_RESIDENCE, vlocity_ins__PolicyAssetId__c = policy.Id, EW_AddressId__c = '123abc')
        };

        insert new List<IBA_PolicyTransaction__c>{
                new IBA_PolicyTransaction__c(IBA_ActivePolicy__c = policy.Id, IBA_OriginalPolicy__c = policy.Id),
                new IBA_PolicyTransaction__c(IBA_ActivePolicy__c = policy.Id, IBA_OriginalPolicy__c = policy.Id)
        };

        insert new List<vlocity_ins__InsuranceClaim__c>{
                new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id),
                new vlocity_ins__InsuranceClaim__c(vlocity_ins__PrimaryPolicyAssetId__c = policy.Id)
        };

        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> inputMap = new Map<String, Object>{
                'policyId' => policy.Id,
                'quoteId' => quote.id
        };

        EWVlocityRemoteActionHelper.createPolicySnapshot(inputMap, outputMap, new Map<String, Object>{'withClaims' => true});

        Asset clonedPolicy = [
                SELECT Id, Policy_Status__c, (
                        SELECT Id
                        FROM vlocity_ins__PolicyCoverages__r
                ), (
                        SELECT Id
                        FROM Policy_Transactions__r
                ), (
                        SELECT Id
                        FROM vlocity_ins__PrimaryClaims__r
                ), (
                        SELECT Id
                        FROM vlocity_ins__InsuredItems__r
                )
                FROM Asset
                WHERE IBA_ActivePolicy__c = :policy.Id
                LIMIT 1
        ];

        System.assert(outputMap.containsKey('snapshotCreated'));
        System.assert((Boolean) outputMap.get('snapshotCreated'));
        System.assertNotEquals(null, clonedPolicy);
        System.assertEquals(EWConstants.EW_POLICY_STATUS_NON_ACTIVE, clonedPolicy.Policy_Status__c);

        System.assert(!clonedPolicy.vlocity_ins__PolicyCoverages__r.isEmpty());
        System.assert(!clonedPolicy.vlocity_ins__InsuredItems__r.isEmpty());
        System.assert(!clonedPolicy.Policy_Transactions__r.isEmpty());
        System.assert(!clonedPolicy.vlocity_ins__PrimaryClaims__r.isEmpty());
    }

    static testMethod void testNotesClone() {
        Asset policy = [SELECT Id FROM Asset LIMIT 1];
        Quote quote = [SELECT Id FROM Quote LIMIT 1];

        quote.EW_Policy__c = policy.Id;
        quote.Status = EWConstants.QUOTE_STATUS_QUOTED;
        update quote;

        insert new List<Note>{
                new Note(Title = '88-EWA', Body = 'BodyBodyBodyBody', ParentId = quote.Id),
                new Note(Title = 'Note', Body = 'BodyBodyBodyBody', ParentId = quote.Id)
        };

        EWPolicyHelper.copyNotesFromQuote(quote.Id, policy.Id);

        List<Note> policyNotes = new List<Note>([
                SELECT Id
                FROM Note
                WHERE ParentId = :policy.Id
        ]);

        System.assertEquals(1, policyNotes.size());
    }

    static testMethod void testFilterForMTAdPolicies(){

        String newCoverageType = 'updated Coverage Type';

        Asset policy = [SELECT Id, EW_CoverageType__c FROM Asset LIMIT 1];
        Quote quote = [SELECT Id, Name, OpportunityId FROM Quote LIMIT 1];
        Quote quote2 = quote.clone(false,false,false,false);
        quote2.EW_CoverageType__c = newCoverageType;
        insert quote2;

        System.assertNotEquals(newCoverageType, policy.EW_CoverageType__c);

        Test.startTest();
        policy.EW_Quote__c = quote2.Id;
        update policy;
        Test.stopTest(); // fires async processes

        System.assertEquals(newCoverageType, [SELECT EW_CoverageType__c FROM Asset WHERE Id = :policy.Id].EW_CoverageType__c);
    }


    static testMethod void testReversePolicyOnNewBusinessWithTransactionCanx(){
        // with the Policy Transaction cancelled.

        Asset policy = [SELECT Id FROM Asset Limit 1];
        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        policy.EW_TypeofTransaction__c = EWConstants.POLICY_TRANSACTION_TYPE_NB;
        policy.EW_FixedExpense__c = 10;
        policy.EW_FixedExpenseExIPT__c = 8;
        update policy;

        reversePolicyTest(policy.Id, EWConstants.EW_POLICY_STATUS_REVERSAL, true);
    }

    static testMethod void testReversePolicyOnNewBusinessWithReversalTransaction(){
        // with a reversal Policy Transaction

        Asset policy = [SELECT Id FROM Asset Limit 1];
        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        policy.EW_TypeofTransaction__c = EWConstants.POLICY_TRANSACTION_TYPE_NB;
        policy.EW_FixedExpense__c = 10;
        policy.EW_FixedExpenseExIPT__c = 8;
        update policy;

        reversePolicyTest(policy.Id, EWConstants.EW_POLICY_STATUS_REVERSAL, false);
    }

    static testMethod void testReversePolicyOnMtaWithTransactionCanx(){
        // with the Policy Transaction cancelled.

        Asset policy = [SELECT Id, EW_Quote__c FROM Asset LIMIT 1];

        EWVlocityRemoteActionHelper.createPolicySnapshot(
                new Map<String, Object>{'policyId' => policy.Id, 'quoteId' => policy.EW_Quote__c},
                new Map<String, Object>(),
                new Map<String, Object>()
        );

        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        policy.EW_TypeofTransaction__c = EWConstants.POLICY_TRANSACTION_TYPE_MTA;
        update policy;

        System.debug('policy = ' + policy);

        reversePolicyTest(policy.Id, EWConstants.EW_POLICY_STATUS_ISSUED, true);
    }

    static testMethod void testReversePolicyOnMtaWithReversalTransaction(){
        // with a reversal Policy Transaction

        Asset policy = [SELECT Id, EW_Quote__c FROM Asset LIMIT 1];

        EWVlocityRemoteActionHelper.createPolicySnapshot(
                new Map<String, Object>{'policyId' => policy.Id, 'quoteId' => policy.EW_Quote__c},
                new Map<String, Object>(),
                new Map<String, Object>()
        );

        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        policy.EW_TypeofTransaction__c = EWConstants.POLICY_TRANSACTION_TYPE_MTA;
        update policy;

        reversePolicyTest(policy.Id, EWConstants.EW_POLICY_STATUS_ISSUED, false);
    }


    static void reversePolicyTest(Id reversingPolicyId, String newStatus, Boolean cancelTransaction){
        
        Set<String> transactionTypes = new Set<String>();
        Decimal transactionValue = 0;

        if(!cancelTransaction){
            // update transaction record to 'Process Complete' so a reversal transaction will be needed
            List<IBA_PolicyTransaction__c> transactions = [SELECT Id, IBA_Status__c FROM IBA_PolicyTransaction__c WHERE IBA_ActivePolicy__c = :reversingPolicyId];
            for(IBA_PolicyTransaction__c pt : transactions){
                pt.IBA_Migration__c = true;
                pt.IBA_MigrationJournalCreated__c = true;
            }
            update transactions;
        }

        Test.startTest();
        new EWVlocityRemoteActionHelper().invokeMethod('reverseRecord', new Map<String,Object>(), new Map<String,Object>(), new Map<String,Object>{'recordId' => reversingPolicyId});
        Test.stopTest();

        List<Asset> checkPolicy = [SELECT Id,Status, ParentId, (SELECT Id, IBA_TransactionType__c, IBA_TotalGWP__c, IBA_Status__c FROM FF_Policy_Transactions__r) FROM Asset WHERE Id = :reversingPolicyId OR ParentId = :reversingPolicyId];
        System.assertEquals(2, checkPolicy.size());
        Integer policyCount =0;
        for(Asset p : checkPolicy){
	
            if(p.Status == EWConstants.EW_POLICY_STATUS_REVERSED && p.ParentId == reversingPolicyId) policyCount++;
            if(p.Status == newStatus && p.Id == reversingPolicyId) policyCount++;
            for(IBA_PolicyTransaction__c pt : p.FF_Policy_Transactions__r){
                transactionTypes.add(pt.IBA_TransactionType__c);
                transactionValue += pt.IBA_TotalGWP__c;
            }
        }
        System.assertEquals(2, policyCount);
        System.assertEquals(1, transactionTypes.size());
        
    }

    @IsTest
    static void createFFCancellationRecordsTest(){
        Asset testPolicy = [SELECT Id, EW_TypeofTransaction__c, Status FROM Asset LIMIT 1];
        testPolicy.EW_TypeofTransaction__c = EWConstants.POLICY_TRANSACTION_STATUS_CANCELLATION;
        testPolicy.Status = EWConstants.EW_POLICY_STATUS_PENDING_CANCELLATION;
        update testPolicy;

        IBA_TestDataFactory.createPolicyTransaction(true, testPolicy.Id);

        List<IBA_PolicyTransaction__c> policyTransactionBeforeFF = [
                SELECT Id, CreatedDate
                FROM IBA_PolicyTransaction__c
                WHERE IBA_ActivePolicy__c = :testPolicy.Id
                OR IBA_OriginalPolicy__c = :testPolicy.Id];

        test.startTest();
        EWPolicyHelper.createFFCancellationRecords(testPolicy.Id);
        test.stopTest();

        List<IBA_PolicyTransaction__c> policyTransactionAfterFF = [
                SELECT Id, CreatedDate
                FROM IBA_PolicyTransaction__c
                WHERE IBA_ActivePolicy__c = :testPolicy.Id
                OR IBA_OriginalPolicy__c = :testPolicy.Id];

        //System.assert(policyTransactionBeforeFF.size() < policyTransactionAfterFF.size());
    }

    @IsTest
    static void createFFCancellationRecordsExceptionTest(){
        String expPolicyId = 'malformedId';
        test.startTest();
        try{
            EWPolicyHelper.createFFCancellationRecords(expPolicyId);
        }catch (Exception exp){
            System.assert(exp != null);
        }
        test.stopTest();
    }


/**
    * @methodName: secureBrokerOwnershipTest
    * @description: Test method for EWPolicyHelper.secureBrokerOwnership method
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    * @dateCreated: 07/02/2020
    */
	@IsTest
    static void secureBrokerOwnershipTest(){
        
        Test.startTest();
        Account testBroker = [SELECT Id FROM Account WHERE Name='testBroker' LIMIT 1];
        Account testInsured = [SELECT Id FROM Account WHERE Name!='testBroker' LIMIT 1];
        Opportunity testOppty = [SELECT Id FROM Opportunity LIMIT 1];
        Quote testQuote = [SELECT Id FROM Quote LIMIT 1];
        Asset testParentPolicy = [SELECT Id, Status, OwnerId FROM Asset LIMIT 1];
        String brokerProfile = EWConstants.PROFILE_EW_PARTNER_NAME;
        User brokerUser = [SELECT Id FROM User WHERE Profile.Name = :brokerProfile AND IsActive = true LIMIT 1];  	    
        Asset childPolicy = EWPolicyDataFactory.createPolicy(null, testOppty.Id, testBroker.Id, testInsured.Id, false);
        childPolicy.IBA_InsuredName__c = 'insName';
        childPolicy.Status = 'Draft';
        childPolicy.EW_Quote__c = testQuote.Id;
        childPolicy.ParentId = testParentPolicy.Id;
        childPolicy.OwnerId = brokerUser.Id;
        childPolicy.vlocity_ins__StandardPremium__c = 123;
        childPolicy.vlocity_ins__AnnualPremium__c = 456;
        childPolicy.vlocity_ins__TotalPremiumForTerm__c = 789;
        childPolicy.EW_FixedExpense__c = 40;
        childPolicy.IBA_InsurancePremiumTaxRate__c = 0;
        childPolicy.IBA_MGACommissionRate__c = 0;
        childPolicy.IBA_BrokerCommissionRate__c = 0;
        insert childPolicy;
        testParentPolicy.Status = 'Issued';
        update testParentPolicy;
        Test.stopTest();
        Asset parentPolicyAfterUpdate = [SELECT Id, OwnerId FROM Asset WHERE Status='Issued' LIMIT 1];
        system.assertEquals(parentPolicyAfterUpdate.OwnerId,brokerUser.Id);
    }
    /**
    * @methodName: updateEmailTemplateFlagPositiveTest
    * @description: Test method for EWPolicyHelper.updateEmailTemplateFlagToTrue method positive scenario
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    * @dateCreated: 27/03/2020
    */
    @isTest
    static void updateEmailTemplateFlagPositiveTest(){
    
    	Asset testPolicyBefore = [SELECT Id FROM Asset LIMIT 1];
        Test.startTest();
        EWPolicyHelper.updateEmailTemplateFlagToTrue(testPolicyBefore.Id);
        Test.stopTest();
        Asset testPolicyAfter = [SELECT Id, EW_IsStandardTemplateForRenewal__c FROM Asset LIMIT 1];
        system.assert(testPolicyAfter.EW_IsStandardTemplateForRenewal__c); 
    }
    /**
    * @methodName: updateEmailTemplateFlagNegativeTest
    * @description: Test method for EWPolicyHelper.updateEmailTemplateFlagToTrue method negative scenario
    * @author: Aleksejs Jedamenko ajedamenko@azuruw.com
    * @dateCreated: 27/03/2020
    */
    @isTest
    static void updateEmailTemplateFlagNegativeTest(){
        
        Test.startTest();
        EWPolicyHelper.updateEmailTemplateFlagToTrue(null);
        Test.stopTest();
        Asset testPolicyAfter = [SELECT Id, EW_IsStandardTemplateForRenewal__c FROM Asset LIMIT 1];
        system.assert(!testPolicyAfter.EW_IsStandardTemplateForRenewal__c); 
        List<Azur_Log__c> testLogs = [SELECT Id, Method__c FROM Azur_Log__c];
        system.assertEquals(1, testLogs.size());
    }   
    @testSetup
    static void setup() {
        EWQuoteHelperTest.setupCarrierAccountsAndCoverages();

        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );
        insert new List<Account>{
                broker, insured
        };

        Product2 product1 = EWProductDataFactory.createProduct('Home Emergency', false);
        Product2 product2 = EWProductDataFactory.createProduct('Contents', false);
        Product2 product3 = EWProductDataFactory.createProduct('Emerging Wealth', false);
        insert new List<Product2>{
                product1, product2, product3
        };

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
        List<PricebookEntry> pbes = new List<PricebookEntry>{
            new PricebookEntry (Product2Id = product1.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true),
            new PricebookEntry (Product2Id = product2.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true),
            new PricebookEntry (Product2Id = product3.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true)
        };
        insert pbes;
        insert new EW_QuoteJSONData__c(EW_Quote__c = quote.Id);

        List<QuoteLineItem> qlis = new List<QuoteLineItem>{
            EWQuoteDataFactory.createQuoteLineItem(quote.id, pbes[0].Id, pbes[0].Product2Id, 100, false),
            EWQuoteDataFactory.createQuoteLineItem(quote.id, pbes[1].Id, pbes[1].Product2Id, 100, false),
            EWQuoteDataFactory.createQuoteLineItem(quote.id, pbes[2].Id, pbes[2].Product2Id, 100, false)
        };
        qlis[0].EW_Transactional_PremiumGrossExIPT__c = 100;
        qlis[1].EW_Transactional_PremiumGrossExIPT__c = 100;
        qlis[2].EW_Transactional_PremiumGrossExIPT__c = 100;
        insert qlis;

        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.IBA_InsuredName__c = 'insName';
        policy.Status = 'Draft';
        policy.EW_Quote__c = quote.Id;
        policy.vlocity_ins__StandardPremium__c = 123;
        policy.vlocity_ins__AnnualPremium__c = 456;
        policy.vlocity_ins__TotalPremiumForTerm__c = 789;
        policy.EW_FixedExpense__c = 40;
        policy.IBA_InsurancePremiumTaxRate__c = 0;
        policy.IBA_MGACommissionRate__c = 0;
        policy.IBA_BrokerCommissionRate__c = 0;
        policy.EW_IsStandardTemplateForRenewal__c = false;
        insert policy;

        quote.EW_Policy__c = policy.Id;
        update quote;

        vlocity_ins__AssetCoverage__c coverage1 = new vlocity_ins__AssetCoverage__c();
        coverage1.vlocity_ins__PolicyAssetId__c = policy.Id;
        coverage1.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverage1.vlocity_ins__Product2Id__c = product1.Id;
        coverage1.CurrencyIsoCode = 'GBP';

        vlocity_ins__AssetCoverage__c coverage2 = new vlocity_ins__AssetCoverage__c();
        coverage2.vlocity_ins__PolicyAssetId__c = policy.Id;
        coverage2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverage2.vlocity_ins__Product2Id__c = product2.Id;
        coverage2.CurrencyIsoCode = 'GBP';

        insert new List<vlocity_ins__AssetCoverage__c>{
                coverage1, coverage2
        };

    }

}