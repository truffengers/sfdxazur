/**
* @name: EWVlocityDocumentClauses
* @description: Helper class for the Clauses Section
* @author: Steve Loftus sloftus@azuruw.com
* @date: 04/02/2019
*/
global with sharing class EWVlocityDocumentClauses implements vlocity_ins.VlocityOpenInterface {

    private Integer maxLogSize = 32767;
    private static String CLASS_NAME = EWVlocityDocumentClauses.class.getName();
    private static String METHOD_INVOKE_METHOD = 'invokeMethod';
    private static String VLOCITY_DESCRIPTION = 'Vlocity Document Remote Action call';

    private static String BORDER_STYLE_BASE = 'border: .5px solid black;border-collapse: collapse;';
    private static String FONT_STYLE_BASE = 'font-size: 10pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String FONT_STYLE_HEADING = 'font-size: 12pt; font-family: \'Trebuchet MS\', geneva, sans-serif;';
    private static String ROW_STYLE_BASE = 'height: 20px;';
    private static String QUOTE_PREFIX = '0Q0';
    private static String NONE = 'None';

    public EWVlocityDocumentClauses() {

    }

    public Boolean invokeMethod(
            String methodName,
            Map<String, Object> inputMap,
            Map<String, Object> outputMap,
            Map<String, Object> optionMap) {

        Boolean result = true;

        Azur_Log__c traceLog = new Azur_Log__c();

        traceLog.Class__c = CLASS_NAME;
        traceLog.Method__c = METHOD_INVOKE_METHOD;
        traceLog.Log_description__c = VLOCITY_DESCRIPTION;

        string logMessage = 'methodName:' + '\n' +
                methodName + '\n\n' +
                'inputMap:' + '\n' +
                JSON.serializePretty(inputMap) + '\n\n' +
                'optionMap:' + '\n' +
                JSON.serializePretty(optionMap);

        if (logMessage.length() > maxLogSize) {
            traceLog.Log_message__c = logMessage.substring(0, maxLogSize);
        } else {
            traceLog.Log_message__c = logMessage;
        }

        try {
            if (methodName == EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD) {
                buildDocumentSectionContent(inputMap, outputMap, optionMap);
            } else {
                result = false;
            }

        } catch (Exception e) {
            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(e);
            log.Class__c = CLASS_NAME;
            log.Method__c = METHOD_INVOKE_METHOD;
            insert log;

            result = false;

        } finally {
            String additionalLogMessage = traceLog.Log_message__c + '\n\n' + 'outputMap:' + '\n' + JSON.serializePretty(outputMap);

            if (additionalLogMessage.length() > maxLogSize) {
                traceLog.Log_message__c = additionalLogMessage.substring(0, maxLogSize);
            } else {
                traceLog.Log_message__c = additionalLogMessage;
            }

            insert traceLog;
        }
        return result;
    }

    private void buildDocumentSectionContent(
            Map<String, Object> inputMap,
            Map<String, Object> outputMap,
            Map<String, Object> optionMap) {

        String dataJSON = (String) inputMap.get('dataJSON');
        Map<String, Object> documentData = (Map<String, Object>) JSON.deserializeUntyped(dataJSON);
        Map<String, Object> contextData = (Map<String, Object>) documentData.get('contextData');

        Id contextId = (Id) contextData.get('contextId');

        String query = 'SELECT EW_DocumentClause__r.EW_Clause_Name__c, EW_DocumentClause__r.Name, EW_DocumentClause__r.vlocity_ins__ClauseContent__c, EW_DocumentClause__r.vlocity_ins__Category__c';
        query += ' FROM EW_ClauseAssignment__c';
        query += ' WHERE ' + ((((String)contextId).startsWith(QUOTE_PREFIX)) ? 'EW_Quote__c' : 'EW_Policy__c');
        query += ' =: contextId';
        query += ' AND EW_Active__c = True';

        List<EW_ClauseAssignment__c> standardClauseList = new List<EW_ClauseAssignment__c>();
        List<EW_ClauseAssignment__c> additionalClauseList = new List<EW_ClauseAssignment__c>();

        for (EW_ClauseAssignment__c clause : Database.query(query)) {
            if (clause.EW_DocumentClause__r.vlocity_ins__Category__c == EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED) {
                standardClauseList.add(clause);
            } else {
                additionalClauseList.add(clause);
            }
        }

        String content = '';

        // standard clauses
        content += buildClausesContent(standardClauseList);

        // additional
        content += buildClausesContent(additionalClauseList);

        if (String.isEmpty(content)) {
            content += '<p><span style=\"' + FONT_STYLE_BASE + '\">';
            content += '<viawrapper>' + NONE + '</viawrapper>';
            content += '</span></p>';
        }

        outputMap.put('sectionContent', content);
    }

    private String buildClausesContent(List<EW_ClauseAssignment__c> clauseList) {
        String clauseContent = '';

        if (!clauseList.isEmpty()) {

            clauseContent += '<table style=\"' + FONT_STYLE_BASE + 'width: 100%\">';
            clauseContent += '<tbody>';

            for (EW_ClauseAssignment__c clause : clauseList) {

                clauseContent += '<tr style=\"' + ROW_STYLE_BASE + '\">';
                clauseContent += '<td style=\"' + ROW_STYLE_BASE + 'width: 100%; white-space:pre-wrap; word-wrap:break-word;\">';               
                clauseContent += '<span style=\"' + FONT_STYLE_BASE + '\"><strong>';
                clauseContent += '<viawrapper>' + clause.EW_DocumentClause__r.EW_Clause_Name__c + '</viawrapper>';
                clauseContent += '</strong></span></td>';
                clauseContent += '</tr>';
                clauseContent += '<tr style=\"' + ROW_STYLE_BASE + '\">';
                clauseContent += '<td style=\"' + ROW_STYLE_BASE + 'width: 100%; white-space:pre-wrap; word-wrap:break-word;\">';               
                clauseContent += '<span style=\"' + FONT_STYLE_BASE + '\">';
                clauseContent += '<viawrapper>' + clause.EW_DocumentClause__r.vlocity_ins__ClauseContent__c + '</viawrapper>';
                clauseContent += '</span></td>';
                clauseContent += '</tr>';
            }

            clauseContent += '</tbody></table>';

        }

        return clauseContent;
    }
}