({
    init: function(component) {

        // store the name of the Analytics object
        window.GoogleAnalyticsObject = 'ga';
        // check whether the Analytics object is defined
        if (!('ga' in window)) {
            // define the Analytics object
            window.ga = function() {
                // add the tasks to the queue
                window.ga.q.push(arguments);
            };
    
            // create the queue
            window.ga.q = [];
        }
        // store the current timestamp
        window.ga.l = (new Date()).getTime();
        window.ga_debug = { trace: true };
    
        var trackingid = component.get("v.trackingid");
        var userdata = component.get("v.userdata");
    
        ga('create', trackingid, { 'userId': userdata.id, 'storage': 'none' });
        ga('send', 'pageview');
	}
})