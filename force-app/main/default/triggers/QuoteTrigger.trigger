/**
* @name: QuoteTrigger
* @description: Trigger for the standard object Quote
* @author: Roy Lloyd, rlloyd@azuruw.com
* @date: 07/02/2019
*
*/

trigger QuoteTrigger on Quote (before insert, before update, before delete, after insert, after update, after delete, after undelete)
{
    TriggerDispatcher.Run(new QuoteTriggerHandler());
}