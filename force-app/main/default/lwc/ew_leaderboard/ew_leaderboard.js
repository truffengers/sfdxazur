import { LightningElement, track } from 'lwc';
import Chart from '@salesforce/resourceUrl/chartjs';
import { loadScript } from 'lightning/platformResourceLoader';
import getLeaderboardData from '@salesforce/apex/EWLeaderboardHelper.getLeaderboardData';
import getTopTips from '@salesforce/apex/EWLeaderboardHelper.getTopTips';
import thumbsUpClicked from '@salesforce/apex/EWLeaderboardHelper.thumbsUpClicked';
import thumbsDownClicked from '@salesforce/apex/EWLeaderboardHelper.thumbsDownClicked';
import checkIfBrokerNetworkBroker from '@salesforce/apex/EWAccountHelper.checkIfBrokerNetworkBroker';

export default class Ew_leaderboard extends LightningElement {
    chart;
    chartjsInitialized;

    rotateTopTips = true;
    rotationInterval;

    quotesByBroker = [];
    @track topTips = [];
    currentTopTipIndex = 0;

    createLeaderboardChart() {
        const canvas = document.createElement('canvas');
        this.template.querySelector('div.chart').appendChild(canvas);
        const ctx = canvas.getContext('2d');
        window.Chart.defaults.global.defaultFontFamily = "'FS Elliot'";

        this.chart = new window.Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: this.quotesByBroker.map(q => q.name),
                datasets: [{
                    label: 'record count',
                    data: this.quotesByBroker.map(q => q.quotes),
                    backgroundColor: '#b8e4fa'
                }]
            },
            defaults: {
                global: {
                    defaultFontSize: 24
                }
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Record Owner',
                        },
                        gridLines: {
                            zeroLineColor: 'white',
                            color: 'rgba(0, 0, 0, 0)'
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Record Count'
                        },
                        gridLines: {
                            zeroLineColor: 'black',
                            color: 'black'
                        }
                    }]
                }
            }
        });
    }

    createLeaderboardData() {
        getLeaderboardData()
            .then(data => {
                data.forEach(quote => {
                    if (!this.quotesByBroker.includes(quote.Owner.Name)) {
                        this.quotesByBroker.push(quote.Owner.Name);
                    }
                });

                this.quotesByBroker = this.quotesByBroker
                    .map(broker => {
                        const brokerQuoteNumber = data.filter(quote => {
                                return quote.Owner.Name === broker;
                            }).length;

                        return {
                            name: broker,
                            quotes: brokerQuoteNumber
                        }
                    })
                    .sort((a, b) => {
                        return a.quotes > b.quotes ? -1 : 1;
                    });

                this.createLeaderboardChart();
            })
            .catch(e => console.warn(e)); // eslint-disable-line TESTING
    }

    renderedCallback() {
        if (this.chartjsInitialized) {
            return;
        }
        this.chartjsInitialized = true;

        // Depending on if the user is a Broker Network Broker, the styling may need to be
        // changed to accommodate the special Home page design. Add class broker-network to
        // accommodate this.
        checkIfBrokerNetworkBroker()
            .then(result => {
                if (result) {
                    this.template.querySelectorAll('.leaderboard-wrapper').forEach(box => {
                        box.classList.add('broker-network');
                    });
                    this.template.querySelector('.tip-content-wrapper')
                        .setAttribute('style', 'height: auto; margin-bottom: 0');
                    this.template.querySelector('.top-tip-wrapper')
                        .setAttribute('style', 'height: auto;');
                    this.template.querySelector('.top-tip-heading')
                        .setAttribute('style', 'margin-bottom: 0;');
                }
            })
            .catch(error => console.log(error)); // eslint-disable-line

        getTopTips()
            .then(tips => {
                this.topTips = tips;

                this.topTips.forEach(tip => {
                    tip.className = tip.Name.split(' ').join('-');
                    tip.active = false;
                });

                this.topTips[0].active = true;

                this.rotationInterval = setInterval(() => { // eslint-disable-line
                    if (this.rotateTopTips) {
                        this.rotateTopTip('forward');
                    }
                }, 5000);
            })
            .catch(e => console.warn(e)); // eslint-disable-line

        loadScript(this, `${Chart}/Chart.bundle.js`)
            .then(() => {
                this.createLeaderboardData();
            })
            .catch(error => console.log(error)); // eslint-disable-line
    }

    arrowClick({ target }) {
        this.rotateTopTips = false;
        this.rotateTopTip(target.dataset.direction);
    }

    rotateTopTip(direction) {
        this.topTips.map(tip => {
            tip.active = false;
            return null;
        });

        if (direction === 'forward') {
            if (this.currentTopTipIndex === this.topTips.length - 1) {
                this.currentTopTipIndex = 0;
            } else {
                this.currentTopTipIndex++;
            }
        } else {
            if (this.currentTopTipIndex === 0) {
                this.currentTopTipIndex = this.topTips.length - 1;
            } else {
                this.currentTopTipIndex--;
            }
        }

        this.topTips[this.currentTopTipIndex].active = true;
    }

    thumbsUpClick() {
        thumbsUpClicked({ topTipId: this.topTips[this.currentTopTipIndex].Id })
            .then(() => {
                this.topTips[this.currentTopTipIndex].feedbackSubmitted = true;
            })
            .catch(e => console.warn(e)); // eslint-disable-line
    }

    thumbsDownClick() {
        thumbsDownClicked({ topTipId: this.topTips[this.currentTopTipIndex].Id })
            .then(() => {
                this.topTips[this.currentTopTipIndex].feedbackSubmitted = true;
            })
            .catch(e => console.warn(e)); // eslint-disable-line
    }
}