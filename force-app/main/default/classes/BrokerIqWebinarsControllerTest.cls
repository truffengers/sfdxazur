@isTest
private class BrokerIqWebinarsControllerTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings = tog.getBrightTalkApiSettings();

	private static PageReference pageRef;
	private static Map<String, String> params;

	static {
		pageRef = Page.BrokerIqWebinars;
		Test.setCurrentPage(pageRef);
		params = pageRef.getParameters();
	}

	@isTest static void noParams() {
        Test.startTest();
		BrokerIqWebinarsController controller = new BrokerIqWebinarsController();
		Test.stopTest();

		System.assertEquals(null, controller.featuredWebcast);
	}

    private static BrightTALK__Channel__c getChannel() {
		BrightTALK__Channel__c channel = tog.getBtChannel();
		channel.BrightTALK__Channel_Id__c = settings.Channel_Id__c;
		update channel;
        
		return channel;        
    }
    
	@isTest static void withFeaturedWebcast() {

		IQ_Content__c iqContent = tog.getWebcastIqContent();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(123);
		webcast.BrightTALK__Start_Date__c = Date.today().addDays(-1);
		webcast.Status__c = 'recorded';
		webcast.IQ_Content__c = iqContent.Id;
		update webcast;
				
		BrightTALK__Channel__c channel = getChannel();
		channel.Featured_Webcast__c = webcast.Id;
		update channel;

		CPD_Test__c aTest = tog.getCpdTest();
		aTest.Status__c = 'Published';
		update aTest;

        Test.startTest();
		BrokerIqWebinarsController controller = new BrokerIqWebinarsController();
		Test.stopTest();

		System.assertEquals(webcast.Id, controller.featuredWebcast.Id);
		System.assertEquals(1, controller.recentWebcasts.size());
		System.assertEquals(webcast.Id, controller.recentWebcasts[0].Id);
		System.assertEquals(iqContent.Id, controller.featuredIqContent.Id);
	}
	@isTest static void withId() {
        getChannel();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(123);
		webcast.BrightTALK__Start_Date__c = Date.today().addDays(-1);
		webcast.Status__c = 'upcoming';
		update webcast;
        
        params.put('id', webcast.Id);
        
        Test.startTest();
		BrokerIqWebinarsController controller = new BrokerIqWebinarsController();
		Test.stopTest();

		System.assertEquals(webcast.Id, controller.featuredWebcast.Id);
	}
	@isTest static void withTitle() {
        getChannel();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(123);
		webcast.BrightTALK__Start_Date__c = Date.today();
        webcast.BrightTALK__Name__c = 'A title';
		webcast.Status__c = 'upcoming';
		update webcast;
        
        params.put('title', webcast.BrightTALK__Name__c);
        
        Test.startTest();
		BrokerIqWebinarsController controller = new BrokerIqWebinarsController();
		Test.stopTest();

		System.assertEquals(webcast.Id, controller.featuredWebcast.Id);
	}

	@isTest static void maybeGotoLogin() {
        Test.startTest();
		BrokerIqWebinarsController controller = new BrokerIqWebinarsController();
		Test.stopTest();

		System.assertEquals(null, controller.maybeGotoLogin());
	}
	@isTest static void maybeGotoLoginGuest() {

		List<User> guestUser = [SELECT Id FROM User WHERE IsActive = true AND UserType = 'Guest'];

		if(!guestUser.isEmpty()) {
	        Test.startTest();
			BrokerIqWebinarsController controller = new BrokerIqWebinarsController();
			Test.stopTest();

			System.runAs(guestUser[0]) {
				System.assertEquals(Page.BrokerIqLogin.getUrl() + '?startURL=' + EncodingUtil.urlEncode(ApexPages.currentPage().getUrl(), 'UTF-8'), 
                                    controller.maybeGotoLogin().getUrl());
			}
		}
	}
}