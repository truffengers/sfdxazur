/**
* @name: PHO_DeltaTransactionsJobTest
* @description: Test class for the handling of Phoenix 2019 Data
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 30/03/2020
* @lastChangedBy: Antigoni D'Mello  17/08/2020  Added NetPayable to Carrier to reflect PR-94 Carrier Line bug changes
*/

@IsTest
private class PHO_DeltaTransactionsJobTest {

    private static final String CURRENCY_GBP = 'GBP';
    private static final String COVERAGE_SPEC_RECORDTYPE = 'CoverageSpec';

    @TestSetup
    static void createTestData() {
        // Person Account, Azur account, Carrier account
        List<Account> allAccounts = new List<Account>();
        Account primaryHolder = TestDataFactory.createPersonAccount('Mrs', 'Delta', 'PersonTest');
        primaryHolder.Relationship__c = 'Primary Policyholder';
        primaryHolder.PersonMailingCity = 'London';
        primaryHolder.PersonMailingCountry = 'United Kingdom';
        primaryHolder.PersonMailingStreet = '16 Hollybush Lane';
        primaryHolder.PersonMailingPostalCode = 'AL7 4JN';
        allAccounts.add(primaryHolder);
        allAccounts.add(IBA_TestDataFactory.createAccount(false, 'Azur TestAccount', 'Business_Account'));
        allAccounts.add(IBA_TestDataFactory.createAccount(false, 'Carrier TestAccount', 'Carrier'));
        insert allAccounts;

        // Products
        List<Product2> products = new List<Product2>();
        Product2 personalCyber = IBA_TestDataFactory.createProduct(false, 'Cyber', COVERAGE_SPEC_RECORDTYPE);
        personalCyber.IsActive = true;
        personalCyber.ProductCode = 'CYB';
        personalCyber.CurrencyIsoCode = CURRENCY_GBP;

        Product2 buildings = IBA_TestDataFactory.createProduct(false, 'Buildings', COVERAGE_SPEC_RECORDTYPE);
        buildings.IsActive = true;
        buildings.ProductCode = 'buildings';
        buildings.CurrencyIsoCode = CURRENCY_GBP;

        Product2 contents = IBA_TestDataFactory.createProduct(false, 'Contents', COVERAGE_SPEC_RECORDTYPE);
        contents.IsActive = true;
        contents.ProductCode = 'contents';
        contents.CurrencyIsoCode = CURRENCY_GBP;

        Product2 jewelleryWatches = IBA_TestDataFactory.createProduct(false, 'Jewellery And Watches', COVERAGE_SPEC_RECORDTYPE);
        jewelleryWatches.IsActive = true;
        jewelleryWatches.ProductCode = 'jewelleryAndWatches';
        jewelleryWatches.CurrencyIsoCode = CURRENCY_GBP;

        Product2 legalExpenses = IBA_TestDataFactory.createProduct(false, 'Legal Expenses', COVERAGE_SPEC_RECORDTYPE);
        legalExpenses.IsActive = true;
        legalExpenses.ProductCode = 'legalExpenses';
        legalExpenses.Family = 'Emerging Wealth';
        legalExpenses.CurrencyIsoCode = CURRENCY_GBP;

        products.add(personalCyber);
        products.add(buildings);
        products.add(contents);
        products.add(jewelleryWatches);
        products.add(legalExpenses);
        insert products;

        // Original policy & policy record type
        Asset originalPolicy = IBA_TestDataFactory.createPolicy(false, '88-EWA-P-0001173', allAccounts.get(0).Id);
        originalPolicy.Status = 'Issued';
        originalPolicy.Policy_Status__c = 'Active';
        originalPolicy.Currency_Type__c = CURRENCY_GBP;
        originalPolicy.Azur_Account__c = allAccounts.get(1).Id;
        originalPolicy.vlocity_ins__InceptionDate__c = System.today();
        originalPolicy.vlocity_ins__EffectiveDate__c = System.today().addDays(1);
        originalPolicy.vlocity_ins__ExpirationDate__c = System.today().addYears(1);
        originalPolicy.IBA_BrokerCommissionRate__c = 25.00;
        originalPolicy.IBA_MGACommissionRate__c = 12.00;
        originalPolicy.IBA_InsurancePremiumTaxRate__c = 12.00;
        originalPolicy.IBA_TotalCommissionRate__c = 37.00;
        insert originalPolicy;

        // Insured item
        vlocity_ins__AssetInsuredItem__c insuredItem = new vlocity_ins__AssetInsuredItem__c();
        insuredItem.vlocity_ins__PolicyAssetId__c = originalPolicy.Id;
        insuredItem.Name = 'Main residence';
        insuredItem.vlocity_ins__Style__c = 'Detached';
        insuredItem.vlocity_ins__Type__c = 'House';
        insuredItem.EW_CoverType__c = 'Property';
        insuredItem.RecordTypeId = Schema.SObjectType.vlocity_ins__AssetInsuredItem__c.getRecordTypeInfosByDeveloperName().get('EW_Property').getRecordTypeId();
        insuredItem.vlocity_ins__City__c = 'Guildford';
        insuredItem.EW_BuildingNumber__c = '8';
        insuredItem.vlocity_ins__Address1__c = 'GUILDOWN AVENUE';
        insuredItem.vlocity_ins__Country__c = 'ENG';
        insert insuredItem;

        // Policy coverages
        List<vlocity_ins__AssetCoverage__c> policyCoverages = new List<vlocity_ins__AssetCoverage__c>();
        policyCoverages.add(IBA_TestDataFactory.createPolicyCoverage(false, 'Cyber', products.get(0).Id, originalPolicy.Id));
        policyCoverages.add(IBA_TestDataFactory.createPolicyCoverage(false, 'Buildings', products.get(1).Id, originalPolicy.Id));
        policyCoverages.add(IBA_TestDataFactory.createPolicyCoverage(false, 'Contents', products.get(2).Id, originalPolicy.Id));
        policyCoverages.add(IBA_TestDataFactory.createPolicyCoverage(false, 'JewelleryAndWatches', products.get(3).Id, originalPolicy.Id));
        policyCoverages.add(IBA_TestDataFactory.createPolicyCoverage(false, 'LegalExpenses', products.get(4).Id, originalPolicy.Id));

        // Two related Policy Transactions
        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(false, 2, originalPolicy.Id);
        policyTransactions.get(0).PHO_2019Delta__c = true;
        policyTransactions.get(1).PHO_2019Delta__c = true;
        insert policyTransactions;

        // Two Policy Carrier Lines
        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();
        IBA_FFPolicyCarrierLine__c carrierLine1 = IBA_TestDataFactory.createPolicyCarrierLine(false, policyTransactions.get(0).Id, allAccounts.get(2).Id);
        IBA_FFPolicyCarrierLine__c carrierLine2 = IBA_TestDataFactory.createPolicyCarrierLine(false, policyTransactions.get(1).Id, allAccounts.get(2).Id);
        policyCarrierLines.add(carrierLine1);
        policyCarrierLines.add(carrierLine2);
        insert policyCarrierLines;

        // Two Policy Transaction Lines
        List<IBA_PolicyTransactionLine__c> policyTransactionLines = new List<IBA_PolicyTransactionLine__c>();
        IBA_PolicyTransactionLine__c transactionLine1 = IBA_TestDataFactory.createPolicyTransactionLine(false, policyTransactions.get(0).Id, policyCarrierLines.get(0).Id);
        IBA_PolicyTransactionLine__c transactionLine2 = IBA_TestDataFactory.createPolicyTransactionLine(false, policyTransactions.get(1).Id, policyCarrierLines.get(1).Id);
        transactionLine1.PHO_PHBrokerCommission__c = 100.20;
        transactionLine1.PHO_ALPsBrokerCommission__c = 50.50;
        transactionLine1.PHO_PHCarrierFee__c = 80.4;
        transactionLine1.PHO_ALPsCarrierFee__c = 50.89;
        transactionLine1.PHO_PHForeignTax__c = 110.31;
        transactionLine1.PHO_ALPsForeignTax__c = 78.30;
        transactionLine1.PHO_PHGrossWrittenPremium__c = 1500.45;
        transactionLine1.PHO_ALPsGrossWrittenPremium__c = 541.00;
        transactionLine1.PHO_PHInsurancePremiumTax__c = 465.45;
        transactionLine1.PHO_ALPsInsurancePremiumTax__c = 231.00;
        transactionLine1.IBA_NetPayabletoCarrier__c = 0.10;

        transactionLine2.PHO_PHBrokerCommission__c = 100.20;
        transactionLine2.PHO_ALPsBrokerCommission__c = 50.50;
        transactionLine2.PHO_PHCarrierFee__c = 80.4;
        transactionLine2.PHO_ALPsCarrierFee__c = 50.89;
        transactionLine2.PHO_PHForeignTax__c = 110.31;
        transactionLine2.PHO_ALPsForeignTax__c = 78.30;
        transactionLine2.PHO_PHGrossWrittenPremium__c = 1500.45;
        transactionLine2.PHO_ALPsGrossWrittenPremium__c = 541.00;
        transactionLine2.PHO_PHInsurancePremiumTax__c = 465.45;
        transactionLine2.PHO_ALPsInsurancePremiumTax__c = 231.00;
        transactionLine2.IBA_NetPayabletoCarrier__c = 0.10;
        policyTransactionLines.add(transactionLine1);
        policyTransactionLines.add(transactionLine2);
        insert policyTransactionLines;
    }

    @IsTest
    public static void testSuccessfulCreationOfData() {
        Test.startTest();

        // Run the batch job
        Database.executeBatch(new PHO_DeltaTransactionsJob());

        Test.stopTest();

        // Assert that new records were created, with all the correct relationships and information
        List<Azur_Log__c> azurLogs = [SELECT Id FROM Azur_Log__c WHERE Class__c = 'PHO_DeltaTransactionsJob'];
        List<IBA_PolicyTransactionLine__c> allTransactionLines = [SELECT Id, Name FROM IBA_PolicyTransactionLine__c LIMIT 10];
        List<IBA_FFPolicyCarrierLine__c> allCarrierLines = [SELECT Id, Name FROM IBA_FFPolicyCarrierLine__c LIMIT 10];
        List<IBA_PolicyTransaction__c> allTransactions = [SELECT Id, PHO_2019Delta__c, PHO_DeltaProcessed__c, PHO_DeltaTransaction__c, Name
                                                            FROM IBA_PolicyTransaction__c
                                                            LIMIT 10];

        System.assertEquals(0, azurLogs.size(), 'No logs should have been created');
        System.assertEquals(4, allTransactions.size(), 'Total transactions should be 4');
        System.assertEquals(4, allTransactionLines.size(), 'Total Policy Transaction Lines should be 4');
        System.assertEquals(4, allCarrierLines.size(), 'Total Policy Carrier Lines should be 4');
        System.assertEquals(true, allTransactions.get(0).PHO_DeltaProcessed__c, 'Transaction should be marked as Delta processed');
        System.assertEquals(true, allTransactions.get(1).PHO_DeltaProcessed__c, 'Transaction should be marked as Delta processed');
        System.assertEquals(true, allTransactions.get(2).PHO_DeltaTransaction__c, 'Transaction should be marked as Delta transaction');
        System.assertEquals(true, allTransactions.get(3).PHO_DeltaTransaction__c, 'Transaction should be marked as Delta transaction');
    }

    @IsTest
    public static void volumeTest() {
        // Perform a basic volume test to prove that the batch can handle the maximum size of 200 records
        // And it will produce the new 200 policy transactions
        List<IBA_PolicyTransaction__c> originalPolicyTransactions = [SELECT Id, Name, IBA_ActivePolicy__c,PHO_2019Delta__c FROM IBA_PolicyTransaction__c LIMIT 10];
        List<IBA_PolicyTransaction__c> bulkPolicyTransactions = new List<IBA_PolicyTransaction__c>();
        for(Integer i=0; i<198; i++) {
            IBA_PolicyTransaction__c newTransaction = originalPolicyTransactions.get(0).clone();
            bulkPolicyTransactions.add(newTransaction);
        }
        insert bulkPolicyTransactions;

        Test.startTest();

        Database.executeBatch(new PHO_DeltaTransactionsJob());

        Test.stopTest();

        List<IBA_PolicyTransaction__c> newPolicyTransactions = [SELECT Id FROM IBA_PolicyTransaction__c WHERE PHO_DeltaTransaction__c = true];

        System.assertEquals(200, newPolicyTransactions.size(), 'The new delta policy transactions should be 200');
    }
}