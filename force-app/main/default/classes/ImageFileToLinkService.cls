/*
	Takes the id of a set of ContentDocuments and turns them into
	publically shareable URLs by creating a Content Delivery (if one does
	not already exist), then putting the URL of the delivery back into a 
	specified field
*/
public  class ImageFileToLinkService {
    public class ImageFieldPair {
        public Schema.sObjectField imageField;
        public Schema.sObjectField imageUrlField;

        public ImageFieldPair(Schema.sObjectField imageField, Schema.sObjectField imageUrlField) {
            this.imageField = imageField;
            this.imageUrlField = imageUrlField;
        }
    }

    private List<ImageFieldPair> imageFieldPairs;
    private Set<Id> contentDocumentIds;
    private Map<Id, sObject> interestingObjects;
    private Map<Id, List<ImageFieldPair>> interestingObjectIdToFields;

	public ImageFileToLinkService(List<ImageFieldPair> imageFieldPairs) {
		this.imageFieldPairs = imageFieldPairs;
		this.contentDocumentIds = new Set<Id>();
		this.interestingObjects = new Map<Id, sObject>();
		this.interestingObjectIdToFields = new Map<Id, List<ImageFieldPair>>();
	}

	public void addPotentialObject(sObject oldObject, sObject newObject) {
        for(ImageFieldPair thisFieldPair : imageFieldPairs) {
            try {
                Id oldContentDocumentId = (Id)oldObject.get(thisFieldPair.imageField);
                Id newContentDocumentId = (Id)newObject.get(thisFieldPair.imageField);

                if(newContentDocumentId != null && newContentDocumentId.getSobjectType() != ContentDocument.sObjectType) {
                    newObject.addError(String.format(
                        Label.BrokerIqCarouselIdWrongType,
                        new List<String>{
                            thisFieldPair.imageField.getDescribe().getLabel(),
                            ContentDocument.sObjectType.getDescribe().getKeyPrefix()
                        }
                        ));
                } else {
                    if(oldContentDocumentId != newContentDocumentId) {
                        if(newContentDocumentId != null) {
                            contentDocumentIds.add(newContentDocumentId);
                        }
                        interestingObjects.put(newObject.Id, newObject);
                        List<ImageFieldPair> fields = interestingObjectIdToFields.get(newObject.Id);

                        if(fields == null) {
                            fields = new List<ImageFieldPair>();
                            
                            interestingObjectIdToFields.put(newObject.Id, fields);
                        }
                        fields.add(thisFieldPair);
                    }
                }
            } catch(Exception e) {
                if(e.getMessage().containsIgnoreCase('invalid id')) {
                    newObject.addError(String.format(
                        Label.BrokerIqCarouselIdError,
                        new List<String> {
                                        thisFieldPair.imageField.getDescribe().getLabel(),
                                        (String)newObject.get(thisFieldPair.imageField) 
                        } ));
                } else {
                    throw e;
                }
            }
        }
	}

	public Boolean isEmpty() {
		return interestingObjects.isEmpty();
	}

	public void updateUrls() {
		loadAndUpsertContentDistributions();
		doUpdateUrls();
	}

	private Nebula_Tools.sObjectIndex contentDocumentIdToDistribution;
	
	private void loadAndUpsertContentDistributions() {
        Nebula_Tools.sObjectIndex contentDocumentIdToNewestVersion = new Nebula_Tools.sObjectIndex(
            'ContentDocumentId',
            [SELECT Id, ContentDocumentId
            FROM ContentVersion 
            WHERE ContentDocumentId IN :contentDocumentIds
            AND IsLatest = true]);

        contentDocumentIdToDistribution = new Nebula_Tools.sObjectIndex(
            'ContentDocumentId',
            [SELECT Id, ContentDownloadUrl, ContentDocumentId
            FROM ContentDistribution 
            WHERE ContentDocumentId IN :contentDocumentIds]);

        List<ContentDistribution> distributionsToInsert = new List<ContentDistribution>();

        for(Id thisContentDocumentId : contentDocumentIds) {
            ContentDistribution toMaybeInsert = new ContentDistribution(ContentVersionId  = contentDocumentIdToNewestVersion.get(thisContentDocumentId).Id, 
                                                                        Name='External Link',
                                                                        PreferencesNotifyOnVisit = false);

            if(contentDocumentIdToDistribution.get(thisContentDocumentId) == null) {
                distributionsToInsert.add(toMaybeInsert);
            }
        }

        if(!distributionsToInsert.isEmpty()) {
            insert distributionsToInsert;

            distributionsToInsert = [SELECT Id, ContentDownloadUrl, ContentDocumentId
                                    FROM ContentDistribution 
                                    WHERE Id IN :(new Map<Id, ContentDistribution>(distributionsToInsert)).keySet()];

            contentDocumentIdToDistribution.putAll(distributionsToInsert);
        }		
	}

	private void doUpdateUrls() {
        for(sObject thisObject : interestingObjects.values()) {
            for(ImageFieldPair thisFieldPair : interestingObjectIdToFields.get(thisObject.Id)) {
                ContentDistribution thisDistribution = (ContentDistribution)contentDocumentIdToDistribution.get((Id)thisObject.get(thisFieldPair.imageField));
                if(thisDistribution != null) {
                    String thisDownloadUrl = thisDistribution.ContentDownloadUrl;
//                    thisDownloadUrl = thisDownloadUrl.replaceAll('[\\w\\-\\.]+\\.content\\.force\\.com', Label.CloudFront_URL);
                    thisObject.put(thisFieldPair.imageUrlField, thisDownloadUrl);
                } else {
                    thisObject.put(thisFieldPair.imageUrlField, null);
                }

            }
        }

	}
}