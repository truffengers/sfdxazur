/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 14/10/2017
 * Description: (if required)
 * Modified: Ignacio Sarmiento	18/12/2017	Add 'isRenderCPDButton' attribute and functionality
 */
@isTest
private class BrokerIqContentControllerTest {
    private static TestObjectGenerator tog = new TestObjectGenerator();

    //Profile contants
    private static final String PROFILE_NAME_PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS = BrokerIqTestDataFactory.PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS;
    //Object name constants
    private static final String USER_SYSTEMADMIN_LASTNAME = 'Testing last name system admin';
    private static final String USER_BROKERIQ_COMMUNITY_USER_LASTNAME = 'Testing last name community user';
    private static final String CONTACT_LASTNAME = 'Testing last name contact'; 
    private static final String CATEGORY_NAME = 'Test class compliance'; 
    private static final String CONTRIBUTOR_NAME = 'Test class compliance'; 
    private static final String BT_CHANNEL_NAME = 'test class Azur underwriting'; 
    //Variables
    private static User systemAdmin;
    private static User biqCommUser;
    private static Category__c category;
    private static Broker_iQ_Contributor__c biqContributor;
    private static BrightTALK__Channel__c btChannel;
    private static IQ_Content__c webinar;

    
    @testSetup
    public static void setup() {
        //Insert system administrator
        systemAdmin = BrokerIqTestDataFactory.createSystemAdmistratorUser(USER_SYSTEMADMIN_LASTNAME);
        insert systemAdmin;
        
        System.runAs(systemAdmin) {
            //Insert Biq Commmuty user 
        	Profile BrokerIqCommunityPlusProfile = [SELECT Id FROM Profile WHERE Name =: PROFILE_NAME_PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS LIMIT 1];
        	biqCommUser = BrokerIqTestDataFactory.createPortalUser(USER_BROKERIQ_COMMUNITY_USER_LASTNAME, BrokerIqCommunityPlusProfile.Id);
        	insert biqCommUser;
       
            //Insert category
            category = BrokerIqTestDataFactory.createCategory(CATEGORY_NAME);
            insert category;
            
            //Insert biq contributor
            Account account = BrokerIqTestDataFactory.createBusinessAccount('Test class business account');
            insert account;
            Contact contact = BrokerIqTestDataFactory.createBroker(account.Id, CONTACT_LASTNAME);
            insert contact;
            biqContributor = BrokerIqTestDataFactory.createBrokerIQContributor(CONTRIBUTOR_NAME, contact.Id);
			insert biqContributor;
            
            //Insert channel
            btChannel = BrokerIqTestDataFactory.createBrightTalkChannel(BT_CHANNEL_NAME);
			insert btChannel;
        } 
    }
    
    private static void getUsers(){
    	systemAdmin = [SELECT Id FROM User WHERE LastName =: USER_SYSTEMADMIN_LASTNAME AND isActive = true LIMIT 1];
        biqCommUser = [SELECT Id, ContactId FROM User WHERE LastName =: USER_BROKERIQ_COMMUNITY_USER_LASTNAME AND isActive = true LIMIT 1];
    }
    
    private static void getSetupRecords(){
        category = [SELECT Id FROM Category__c WHERE Name =: CATEGORY_NAME  LIMIT 1];
        biqContributor = [SELECT Id FROM Broker_iQ_Contributor__c WHERE Name =: CONTRIBUTOR_NAME  LIMIT 1];
        btChannel = [SELECT Id FROM BrightTALK__Channel__c WHERE Name =: BT_CHANNEL_NAME  LIMIT 1];
    }
   
    testMethod static void testBehavior() {
        IQ_Content__c iqc = tog.getWebcastIqContent();
        update iqc;

        IQ_Content__c iqc2  = BrokerIqContentController.getIqContentInformation(iqc.Id).iqContent;

        System.assertEquals(iqc.Id, iqc2.Id);
    }  
    
    testMethod static void testIsRenderCPDButtonProperty(){
        getUsers();
        String webinarName = 'Test class webinar';
        DateTime startDateRecentWebinar = Datetime.now().addHours(-1); //Set recent webinar
        Boolean isCorrectAnswer = true;	//Set correct answer
        
        System.runAs(systemAdmin) {
            	Test.startTest();
                    //Initialize variables
                    getSetupRecords();
    
                    //Insert webinar 
                    webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, true);
                    insert webinar;
                    IQ_Content__Share iqContentShare = new IQ_Content__Share();
                    iqContentShare.AccessLevel = 'Edit';
                    iqContentShare.ParentId = webinar.id;
                    iqContentShare.UserOrGroupId  = biqCommUser.Id;
                    insert iqContentShare;
                
                    //Insert webcast
                    BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateRecentWebinar);
                    insert btWebcast;   
                
                    //Create CPD test data
                    CPD_Test__c cpdTest = BrokerIqTestDataFactory.createCPDTest('Test class CPD Test', webinar.Id);
                    insert cpdTest;
                    CPD_Assessment__c cpdTestAssesstment = BrokerIqTestDataFactory.createCPDAssessment(cpdTest.Id, biqCommUser.contactId);
                    insert cpdTestAssesstment;
                    CPD_Test_Question__c cpdTestQuestion = BrokerIqTestDataFactory.createCPDTestQuestion('Test class question', cpdTest.Id);
                    insert cpdTestQuestion;
                    CPD_Assessment_Answer__c cpdAssessmentAnswer = BrokerIqTestDataFactory.createCPDAnswer(cpdTestAssesstment.Id, cpdTestQuestion.Id, isCorrectAnswer);
                    insert cpdAssessmentAnswer; 
            
                    //Update cpd test status 
            		cpdTest.Status__c = 'Published';
                    update cpdTest;
            	Test.stopTest();
        } 
        
        System.runAs(biqCommUser) {
            //Test 
            BrokerIqContentController.Result result = BrokerIqContentController.getIqContentInformation(webinar.Id);
            
         	//Assess results
            System.assertEquals(webinar.Id, result.iqContent.Id);
            System.assertEquals(false, result.isRenderCPDButton);
        } 

    }
    
     testMethod static void testNoIsRenderCPDButtonPropertyIfNoPassTestAnswers(){
        getUsers();
        String webinarName = 'Test class webinar';
        DateTime startDateRecentWebinar = Datetime.now().addHours(-1); //Set recent webinar
        Boolean isCorrectAnswer = false;	//Set correct answer
        
        System.runAs(systemAdmin) {
            	Test.startTest();
                    //Initialize variables
                    getSetupRecords();
    
                    //Insert webinar 
                    webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, true);
                    insert webinar;
                    IQ_Content__Share iqContentShare = new IQ_Content__Share();
                    iqContentShare.AccessLevel = 'Edit';
                    iqContentShare.ParentId = webinar.id;
                    iqContentShare.UserOrGroupId  = biqCommUser.Id;
                    insert iqContentShare;
                
                    //Insert webcast
                    BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateRecentWebinar);
                    insert btWebcast;   
                
                    //Create CPD test data
                    CPD_Test__c cpdTest = BrokerIqTestDataFactory.createCPDTest('Test class CPD Test', webinar.Id);
                    insert cpdTest;
                    CPD_Assessment__c cpdTestAssesstment = BrokerIqTestDataFactory.createCPDAssessment(cpdTest.Id, biqCommUser.contactId);
                    insert cpdTestAssesstment;
                    CPD_Test_Question__c cpdTestQuestion = BrokerIqTestDataFactory.createCPDTestQuestion('Test class question', cpdTest.Id);
                    insert cpdTestQuestion;
                    CPD_Assessment_Answer__c cpdAssessmentAnswer = BrokerIqTestDataFactory.createCPDAnswer(cpdTestAssesstment.Id, cpdTestQuestion.Id, isCorrectAnswer);
            		insert cpdAssessmentAnswer; 
                    
                    //Update cpd test status 
                    cpdTest.Status__c = 'Published';
                    update cpdTest;
            	Test.stopTest();
        } 
        
        System.runAs(biqCommUser) {
            //Test 
            BrokerIqContentController.Result result = BrokerIqContentController.getIqContentInformation(webinar.Id);
            
         	//Assess results
            System.assertEquals(webinar.Id, result.iqContent.Id);
            System.assertEquals(true, result.isRenderCPDButton);
        } 

    }   
    
    testMethod static void testNoIsRenderCPDButtonPropertyIfNoRecentWebinar(){
        getUsers();
        String webinarName = 'Test class webinar';
        DateTime startDateRecentWebinar = Datetime.now().addHours(1); //Set NO recent webinar
        Boolean isCorrectAnswer = true;	//Set correct answer
        
        System.runAs(systemAdmin) {
            	Test.startTest();
                    //Initialize variables
                    getSetupRecords();
    
                    //Insert webinar 
                    webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, true);
                    insert webinar;
                    IQ_Content__Share iqContentShare = new IQ_Content__Share();
                    iqContentShare.AccessLevel = 'Edit';
                    iqContentShare.ParentId = webinar.id;
                    iqContentShare.UserOrGroupId  = biqCommUser.Id;
                    insert iqContentShare;
                
                    //Insert webcast
                    BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateRecentWebinar);
                    insert btWebcast;   
                
                    //Create CPD test data
                    CPD_Test__c cpdTest = BrokerIqTestDataFactory.createCPDTest('Test class CPD Test', webinar.Id);
                    insert cpdTest;
                    CPD_Assessment__c cpdTestAssesstment = BrokerIqTestDataFactory.createCPDAssessment(cpdTest.Id, biqCommUser.contactId);
                    insert cpdTestAssesstment;
                    CPD_Test_Question__c cpdTestQuestion = BrokerIqTestDataFactory.createCPDTestQuestion('Test class question', cpdTest.Id);
                    insert cpdTestQuestion;
                    CPD_Assessment_Answer__c cpdAssessmentAnswer = BrokerIqTestDataFactory.createCPDAnswer(cpdTestAssesstment.Id, cpdTestQuestion.Id, isCorrectAnswer);
            		insert cpdAssessmentAnswer; 
                    
                    //Update cpd test status 
                    cpdTest.Status__c = 'Published';
                    update cpdTest;
            	Test.stopTest();
        } 
        
        System.runAs(biqCommUser) {
            //Test 
            BrokerIqContentController.Result result = BrokerIqContentController.getIqContentInformation(webinar.Id);
            
         	//Assess results
            System.assertEquals(webinar.Id, result.iqContent.Id);
            System.assertEquals(false, result.isRenderCPDButton);
        } 

    }

    testMethod static void testNoIsRenderCPDButtonPropertyIfNoRelatedCPDTests(){
        getUsers();
        String webinarName = 'Test class webinar';
        DateTime startDateRecentWebinar = Datetime.now().addHours(-1); //Set recent webinar
        Boolean isCorrectAnswer = true;	//Set correct answer
        
        System.runAs(systemAdmin) {
            	Test.startTest();
                    //Initialize variables
                    getSetupRecords();
    
                    //Insert webinar 
                    webinar = BrokerIqTestDataFactory.createWebinar(webinarName, category.Id, biqContributor.Id, true);
                    insert webinar;
                    IQ_Content__Share iqContentShare = new IQ_Content__Share();
                    iqContentShare.AccessLevel = 'Edit';
                    iqContentShare.ParentId = webinar.id;
                    iqContentShare.UserOrGroupId  = biqCommUser.Id;
                    insert iqContentShare;
                
                    //Insert webcast
                    BrightTALK__Webcast__c  btWebcast = BrokerIqTestDataFactory.createBrightTalkWebcast('Test class webcast', webinar.Id, btChannel.Id, startDateRecentWebinar);
                    insert btWebcast;   
            	Test.stopTest();
        } 
        
        System.runAs(biqCommUser) {
            //Test 
            BrokerIqContentController.Result result = BrokerIqContentController.getIqContentInformation(webinar.Id);
            
         	//Assess results
            System.assertEquals(webinar.Id, result.iqContent.Id);
            System.assertEquals(false, result.isRenderCPDButton);
        } 
    } 
    
}