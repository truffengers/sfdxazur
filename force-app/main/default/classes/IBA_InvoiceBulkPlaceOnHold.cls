global with sharing class IBA_InvoiceBulkPlaceOnHold {
    @InvocableMethod
    public static void bulkPlaceOnHold(List<c2g__codaPurchaseInvoice__c> invoices) {
        List<c2g.CODAAPICommon.Reference> invoiceReferences = new List<c2g.CODAAPICommon.Reference>();
        for(c2g__codaPurchaseInvoice__c inv :invoices) {
            c2g.CODAAPICommon.Reference ref = c2g.CODAAPICommon.getRef(null, inv.Name);
            invoiceReferences.add(ref);
        }
        c2g.CODAAPICommon_9_0.Context context = new c2g.CODAAPICommon_9_0.Context();
        c2g.CODAAPIPurchaseInvoice_9_0.BulkPlaceOnHold(context, invoiceReferences);
    }
}