global class IqContentCarouselImages implements Nebula_Tools.BeforeUpdateI {

    private static List<ImageFileToLinkService.ImageFieldPair> carouselImageFields = new List<ImageFileToLinkService.ImageFieldPair> 
        {
            new ImageFileToLinkService.ImageFieldPair(IQ_Content__c.Carousel_Image_1920__c, IQ_Content__c.Carousel_Image_1920_URL__c),
            new ImageFileToLinkService.ImageFieldPair(IQ_Content__c.Carousel_Image_1440__c, IQ_Content__c.Carousel_Image_1440_URL__c),
            new ImageFileToLinkService.ImageFieldPair(IQ_Content__c.Carousel_Image_960__c, IQ_Content__c.Carousel_Image_960_URL__c),
            new ImageFileToLinkService.ImageFieldPair(IQ_Content__c.Carousel_Image_480__c, IQ_Content__c.Carousel_Image_480_URL__c),
            new ImageFileToLinkService.ImageFieldPair(IQ_Content__c.Article_Preview_Image__c, IQ_Content__c.Article_Preview_Image_URL__c)
        };

    global void handleBeforeUpdate(List<IQ_Content__c> oldList, List<IQ_Content__c> newList) {
        
        ImageFileToLinkService imageFileService = new ImageFileToLinkService(carouselImageFields);

        for(Integer i=0; i < newList.size(); i++) {
            imageFileService.addPotentialObject(oldList[i], newList[i]);
        }

        if(!imageFileService.isEmpty()) {
            imageFileService.updateUrls();
        }
    }

}