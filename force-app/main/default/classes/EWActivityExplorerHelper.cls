/*
Name:            EWActivityExplorerHelper
Description:     This class will support the EWActivityExplorerController in structuring and sorting the data returned
Created By:      Rajni Bajpai 
Created On:      18 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1  
************************************************************************************************
*/

global class EWActivityExplorerHelper implements Comparable {
    
    DateTime createdOn;
    String title;
    String body;
    String activityId;
    String entryType;
    String createdBy;
    String preview;
    String additionalInfo;
	
    public EWActivityExplorerHelper (DateTime createdDateInput, String titleInput, String bodyInput, String activityIdInput, String entryTypeInput, String createdByInput, String previewInput, String additionalInfoInput) {
        createdOn = createdDateInput;
        title = titleInput;
        body = bodyInput;
        activityId = activityIdInput;
        entryType = entryTypeInput;
        createdBy = createdByInput;
        preview = previewInput;
        additionalInfo = additionalInfoInput;
    }
    
    global Integer compareTo(Object compareTo) {
        EWActivityExplorerHelper compareToEmp = (EWActivityExplorerHelper)compareTo;
        if (createdOn < compareToEmp.createdOn) return +1;
        if (createdOn > compareToEmp.createdOn) return -1;
        return 0;        
    }
}