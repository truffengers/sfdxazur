({
	doInit : function(component, event, helper) {
		var getTopicsAction = component.get('c.getTopics');
		
		getTopicsAction.setStorable();
		
		getTopicsAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.navigationalTopics', response.getReturnValue());
	        }
        });
        
        $A.enqueueAction(getTopicsAction);		
  	}
})