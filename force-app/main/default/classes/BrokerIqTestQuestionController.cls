public with sharing class BrokerIqTestQuestionController {

	@AuraEnabled
	public static CPD_Assessment__c getAssessment(Id cpdTestId) {
		Contact currentUserAsContact = PortalUserService.getCurrentUserAsContact();
		CPD_Assessment__c thisAssessment;
		List<CPD_Assessment__c> existingAssessments = 
            [SELECT Id, Status__c
             FROM CPD_Assessment__c
             WHERE CPD_Test__c = :cpdTestId
             AND Contact__c = :currentUserAsContact.Id
             AND Status__c = 'In Progress'];
        if(existingAssessments.isEmpty()) {
            thisAssessment = new CPD_Assessment__c(Contact__c = currentUserAsContact.Id,
                                                   CPD_Test__c = cpdTestId,
                                                   Status__c = 'In Progress');
            insert thisAssessment; 
        } else {
            thisAssessment = existingAssessments[0];
        }

        thisAssessment = [SELECT Id, (SELECT Id, Answer__c, CPD_Test_Question__c
    												FROM CDP_Answers__r)
										FROM CPD_Assessment__c
										WHERE Id = :thisAssessment.Id
    									];

        return thisAssessment;
	}

	@AuraEnabled
	public static void saveAnswer(CPD_Assessment_Answer__c currentAnswer) {
		update currentAnswer;
	}

	@AuraEnabled
	public static void submitCpdAssessment(CPD_Assessment__c cpdAssessment) {
		cpdAssessment.Status__c = 'Submitted';

		update cpdAssessment;
	}
	
}