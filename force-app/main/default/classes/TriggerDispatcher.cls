/**
* @name: TriggerDispatcher
* @description: This class is making sure that each relevant method
*               of the trigger handler is called depending on the current trigger context.
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 03/11/2018
*
*/
public class TriggerDispatcher {
    /*
        Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
        This method will fire the appropriate methods on the handler depending on the trigger context.
    */
    public static void Run(ITriggerHandler handler) {
        // Check to see if the trigger has been disabled. If it has, return
        if (handler.IsDisabled())
            return;

        // Detect the current trigger context and fire the relevant methods on the trigger handler:
        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                handler.BeforeInsert(Trigger.new);
            }
            if (Trigger.isUpdate) {
                handler.BeforeUpdate(Trigger.newMap, Trigger.oldMap);
            }
            if (Trigger.isDelete) {
                handler.BeforeDelete(Trigger.oldMap);
            }
        }
        if (Trigger.isAfter) {
            if (Trigger.isInsert)
                handler.AfterInsert(Trigger.newMap);

            if (Trigger.isUpdate)
                handler.AfterUpdate(Trigger.newMap, Trigger.oldMap);

            if (Trigger.IsDelete)
                handler.AfterDelete(Trigger.oldMap);

            if (Trigger.isUndelete)
                handler.AfterUndelete(Trigger.newMap);
        }
    }
}