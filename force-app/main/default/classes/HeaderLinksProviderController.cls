public class HeaderLinksProviderController {

    @AuraEnabled
    public static Map<String, String> getHeaderLinks() {
        Map<String, String> results = new Map<String, String>();

        for (Azur_Community_Header_Link__mdt headerLink : getHeaderLinksMetadata()) {
            results.put(headerLink.DeveloperName, headerLink.URL__c);
        }

        return results;
    }

    private static List<Azur_Community_Header_Link__mdt> getHeaderLinksMetadata() {
        List<Azur_Community_Header_Link__mdt> communityHeaderLinks = new List<Azur_Community_Header_Link__mdt>([
                SELECT Id, DeveloperName, Label, URL__c
                FROM Azur_Community_Header_Link__mdt
        ]);

        return communityHeaderLinks;
    }
}