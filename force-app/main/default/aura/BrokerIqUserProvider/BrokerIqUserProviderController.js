({
	doInit : function(component, event, helper) {
		var getInstanceAction = component.get('c.getInstance');
		
		getInstanceAction.setStorable();

		getInstanceAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	var portalController = response.getReturnValue();
                component.set('v.userType', portalController.userType);
                component.set('v.thisUser', portalController.thisUser);
                component.set('v.thisUserAsContact', portalController.thisUserAsContact);
            }
        });
        
        $A.enqueueAction(getInstanceAction);
	}
})