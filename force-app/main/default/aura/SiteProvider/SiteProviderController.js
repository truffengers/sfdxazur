({
    doInit : function(component, event, helper) {
        var getSiteMetadataAction = component.get('c.getSiteMetadata');

        getSiteMetadataAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var siteMetadata = response.getReturnValue();
                component.set('v.siteMetadata', siteMetadata);
            }
        });
        
        $A.enqueueAction(getSiteMetadataAction);   
    }
})