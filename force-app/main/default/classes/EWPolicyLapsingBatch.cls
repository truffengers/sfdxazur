/**
* @name: EWPolicyLapsingBatch
* @description: Batch changes status to 'Lapsed' in policy records, where related renewal quote wasn't taken up (but only if policy expiration date <= today)   
* @author: Aleksejs Jedamenko ajedamenko@azuruw.com
* @date: 06/03/2020
*
*/
public class EWPolicyLapsingBatch implements Database.Batchable<SObject>{

    /**
    * @methodName: start
    * @description: Batch class start method, compose a query locator which fetches all NTU renewal quote records, which related policies should be lapsed
    * @dateCreated: 06/03/2020 v2: 03/06/20120 - new logic to process manually not taken up reversed quotes 
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    */
    public Database.QueryLocator start(Database.BatchableContext bc) {

        String quoteStatus = EWConstants.QUOTE_STATUS_NTU;
        String quoteTransactionType = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        String issuedPolicyStatus = EWConstants.EW_POLICY_STATUS_ISSUED;
        String queryString = 'SELECT Id, Status, EW_Source_Policy__c, EW_Source_Policy__r.IBA_ActivePolicy__c FROM Quote WHERE Status = :quoteStatus AND EW_TypeofTransaction__c = :quoteTransactionType AND ((EW_Source_Policy__r.vlocity_ins__ExpirationDate__c < TODAY AND EW_Source_Policy__r.Status = :issuedPolicyStatus) OR (EW_Source_Policy__r.IBA_ActivePolicy__r.vlocity_ins__ExpirationDate__c < TODAY AND EW_Source_Policy__r.IBA_ActivePolicy__r.Status = :issuedPolicyStatus))';
        return Database.getQueryLocator(queryString);
    }

    /**
    * @methodName: execute
    * @description: This method will change source policies of quotes fetched by query locator to 'Lapsed'
    * @dateCreated: 06/03/2020 v2: 03/06/20120 - new logic to process manually not taken up reversed quotes 
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    */
    public void execute(Database.BatchableContext bc, List<Quote> scope) {
        
        try {

            Set<Id> ntuQuoteIds = new Set<Id>(); 
            for (Quote ntuQuoteWithExpiredPolicy : scope) {

                if (ntuQuoteWithExpiredPolicy.EW_Source_Policy__r.IBA_ActivePolicy__c==null) {

                    ntuQuoteIds.add(ntuQuoteWithExpiredPolicy.EW_Source_Policy__c);
                } else {

                    ntuQuoteIds.add(ntuQuoteWithExpiredPolicy.EW_Source_Policy__r.IBA_ActivePolicy__c);
                }
            }
            List<Asset> policiesToBeLapsed = [SELECT Id, Status FROM Asset WHERE Id in :ntuQuoteIds];
            for (Asset policyToBeLapsed : policiesToBeLapsed) {
                
                policyToBeLapsed.Status = EWConstants.EW_POLICY_STATUS_LAPSED;
            }
            if (policiesToBeLapsed.size()>0) {
                update policiesToBeLapsed;
            }
        } catch (Exception ex) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = 'EWPolicyLapsingBatch';
            log.Method__c = 'execute';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWPolicyLapsingBatch.execute'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
            insert log;
            System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            throw ex;
        }
        
    }

    /**
    * @methodName: finish
    * @description: Sends a message regarding batch job results to users mentioned in ApexEmailNotification object
    * @dateCreated: 06/03/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface 
    */
    public void finish(Database.BatchableContext bc) {

        try {

            AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, TotalJobItems FROM AsyncApexJob where Id =:BC.getJobId()]; 
            List<ApexEmailNotification> emailNotifications = [SELECT ID, User.Email FROM ApexEmailNotification];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> toAddresses = new List<String> ();   
            for (ApexEmailNotification emailNotification : emailNotifications) {

                toAddresses.add(emailNotification.User.Email);
            }
            mail.setToAddresses(toAddresses);
            mail.setSubject('Policy Lapsing Batch ' + a.Status);
            mail.setPlainTextBody('Number of processed batches: ' + a.TotalJobItems +
           '\n  Number of failed batches: '+ a.NumberOfErrors + '\n\n Detailed information about failures can be found in Azur Log records');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception ex) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = 'EWPolicyLapsingBatch';
            log.Method__c = 'finish';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWPolicyLapsingBatch.finish'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
            System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            insert log;
            throw ex;
        }
    }

}