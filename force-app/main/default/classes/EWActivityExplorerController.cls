/*
Name:            EWActivityExplorerController
Description:     This Batch class will retrieve data for use in ew_activityExplorer lwc component.
Created By:      Meti Gashi 
Created On:      18 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai        19 May 2021         EWH-2121 Quote/Policy Activity Viewer   
************************************************************************************************
*/

public class EWActivityExplorerController {
    
    @AuraEnabled(cacheable=true)
    public static string getActivityHistory(Id recordId){
        
        Id productId;
        Set<Id> recordIds = new Set<Id>();
        Set<Id> quoteIds = new Set<Id>();
        Set<Id> assetIds = new Set<Id>();
        Set<Id> contentDocumentIds = new Set<Id>();
        Map<Id,ContentVersion> CVMap=new Map<Id,ContentVersion>();
        List<EWActivityExplorerHelper> outputElements = new List<EWActivityExplorerHelper>();
        
        //Check the object of the recordId and then pull the accountId
        String sObjectName = string.valueOf(recordId.getSObjectType());
        if(sObjectName == 'Quote'){
            Quote q = [SELECT AccountId, vlocity_ins__PrimaryProductId__c FROM Quote WHERE Id = :recordId LIMIT 1];
            productId = q.vlocity_ins__PrimaryProductId__c;
            recordId = q.AccountId;
        } else if(sObjectName == 'Asset') {
            Asset a = [SELECT AccountId, Product2Id FROM Asset WHERE Id = :recordId LIMIT 1];
            productId = a.Product2Id;
            recordId = a.AccountId;
        } else if(sObjectName == 'Account') {
            recordId = recordId;
        }
        
        //Get all quotes for this client
        String quoteQueryString = 'SELECT Id, AccountId FROM Quote WHERE AccountId = \''+recordId+ '\'';
        if(productId != null){
           quoteQueryString = quoteQueryString + ' AND vlocity_ins__PrimaryProductId__c = \''+ String.valueOf(productId).substring(0, 15) + '\'';
            
        }
        for (Quote q : Database.query(quoteQueryString)){
            quoteIds.add(q.Id);
            recordIds.add(q.Id);
        }
        
        //Get all policies for this client
        String policyQueryString = 'SELECT Id, AccountId FROM Asset WHERE AccountId = \''+recordId+'\'';
        if(productId != null){
            policyQueryString = policyQueryString + ' AND Product2Id = \''+ productId +'\'';
        }
        for (Asset a : Database.query(policyQueryString)){
            assetIds.add(a.Id);
            recordIds.add(a.Id);
        }
        
        //Get all cases linked to these quotes
        for (Case c : [SELECT Id, Subject, Description, EW_Quote__c, CreatedDate, CreatedBy.Name, Status FROM Case WHERE EW_Quote__c = :quoteIds OR Asset__c = :assetIds]){
            EWActivityExplorerHelper record = new EWActivityExplorerHelper(c.CreatedDate, c.Subject, c.Description, c.Id , 'Case', c.CreatedBy.Name, c.Status,'STATUS: '+c.Status);
            outputElements.add(record);
            recordIds.add(c.Id);
        }
        
        //Get all related notes and files for quotes and policies
        for (ContentDocumentLink cdl : [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :recordIds]){
            contentDocumentIds.add(cdl.ContentDocumentId);
        }
        
        if(contentDocumentIds.size() > 0){
            List<ContentDocument> cds = new List<ContentDocument>([SELECT Id, Title, FileType, CreatedDate, CreatedBy.Name 
                                                                   FROM ContentDocument 
                                                                   WHERE Id IN :contentDocumentIds]);
            
            List<contentVersion> cvList=new List<ContentVersion>([SELECT id,TextPreview,ContentDocumentId,IsLatest 
                                                                  FROM ContentVersion 
                                                                  WHERE IsLatest=true AND ContentDocumentId in:contentDocumentIds]);
            
            for(contentVersion cv:cvList){
                CVMap.put(cv.ContentDocumentId,cv);
            }
            
            if(cds.size() > 0){
                for (ContentDocument cd : cds){
                    String contentType = cd.FileType == 'SNOTE' ? 'Note' : cd.FileType;
                    EWActivityExplorerHelper record = new EWActivityExplorerHelper(cd.CreatedDate, cd.Title, CVMap.get(cd.Id).TextPreview, cd.Id, contentType, cd.CreatedBy.Name, CVMap.get(cd.Id).TextPreview, '');
                    outputElements.add(record);
                }     
            }
        }
        
        //Get all emails attached quotes and policies
        for (EmailMessage em : [SELECT Id, TextBody,HtmlBody, Subject, CreatedDate, CreatedBy.Name, FromAddress, ToAddress, Incoming FROM EmailMessage WHERE RelatedToId IN :recordIds]){
            //EWActivityExplorerHelper record = new EWActivityExplorerHelper(em.CreatedDate, em.Subject, em.HtmlBody, em.Id , 'Email', em.CreatedBy.Name, 'FROM: '+em.FromAddress + ' | TO: '+em.ToAddress);
            EWActivityExplorerHelper record = new EWActivityExplorerHelper(em.CreatedDate, em.Subject, em.HtmlBody, em.Id , 'Email', em.CreatedBy.Name, em.Incoming == true ? 'Inbound' : 'Outbound', 'FROM: '+em.FromAddress + ' | TO: '+em.ToAddress);
            outputElements.add(record);
        }
        
        //Get all attachments for quotes and policies
        for (Attachment att : [SELECT Id, Name, CreatedDate, CreatedBy.Name, ParentId FROM attachment WHERE ParentId IN :recordIds]){
            EWActivityExplorerHelper record = new EWActivityExplorerHelper(att.CreatedDate, att.Name, '', att.Id , 'Attachment', att.CreatedBy.Name, '','');
            outputElements.add(record);
        }
        
        //Sort the output by date
        outputElements.sort();
        
        return JSON.serialize(outputElements);
    }
}