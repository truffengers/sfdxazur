/**
* @name: EWTransferPolicy
* @description: Class for transfer Policy from EW/Transfer OS
* @author: Bharati Pal bpal@azuruw.com
* @date: 23/12/2020
*/
global with sharing class EWTransferPolicy{
    
    /**
	* @methodName: transferPolicy
	* @description: Used in EW_Transfer OS, to transfer the Policy, Quote and Opportunity to new Brokerage.
	* @author: Bharati Pal
	* @dateCreated: 23/12/2020
	*/
    public static String transferPolicy( Id policyId, String newBrokerAccountId, String newBrokerUserId,String oldBrokerAccountId ){
        
        Asset policy = [SELECT OwnerId, AccountId, EW_BrokerageBeforeTransfer__c, IBA_ProducingBrokerAccount__c, EW_Quote__c FROM Asset WHERE Id =: policyId LIMIT 1];
        Account customer = [SELECT OwnerId, EW_BrokerAccount__c, EW_PrimaryInsured__c, isPersonAccount FROM Account WHERE Id =: policy.AccountId];
        Account newBrokerAccount = [Select Id,EW_IsFinancialAccount__c,EW_FinancialAccount__c,isPersonAccount FROM Account WHERE Id =:newBrokerAccountId];
        List<Account> jointInsured = [Select OwnerId, EW_BrokerAccount__c,isPersonAccount from Account where EW_PrimaryInsured__c =: customer.Id LIMIT 1];
        
        Quote quote = new Quote();
        List<Opportunity> opportunities = new List<Opportunity>(); 
        
        String message = '';
        
        If(policy.EW_Quote__c != null){
            quote = [SELECT Id,EW_Policy__c,OwnerId,OpportunityId FROM Quote WHERE Id =: policy.EW_Quote__c];
            opportunities = [Select Id, OwnerId, vlocity_ins__AgencyBrokerageId__c from Opportunity WHERE AccountId =: customer.Id];
            
        }
        
        try{
            policy.OwnerId = newBrokerUserId;
            policy.EW_BrokerageBeforeTransfer__c = oldBrokerAccountId;
            If (newBrokerAccount.EW_IsFinancialAccount__c)
            {
                policy.IBA_ProducingBrokerAccount__c = newBrokerAccountId;
            }else 
            {
                policy.IBA_ProducingBrokerAccount__c = newBrokerAccount.EW_FinancialAccount__c;
            }
            update policy;
            
            customer.OwnerId = newBrokerUserId;
            customer.EW_BrokerAccount__c = newBrokerAccountId;
            update customer;
            
            If(quote != null){                
                
                for (Opportunity opp: opportunities)
                {
                    opp.OwnerId = newBrokerUserId;
                    If (opp.vlocity_ins__AgencyBrokerageId__c!=null)
                    {
                        opp.vlocity_ins__AgencyBrokerageId__c = newBrokerAccountId;
                    }
                }
                update opportunities;
                
                quote.vlocity_ins__AgencyBrokerageId__c = newBrokerAccountId;
                update quote;
            }
            
            EWAccountHelper.createBrokerShares(new Map<Id, SObject>{customer.Id => customer});
            
            // Joined Insured
            
            if(jointInsured.size()> 0) {
                jointInsured[0].OwnerId = newBrokerUserId;
                update jointInsured;
                EWAccountHelper.createBrokerShares(new Map<Id, SObject>{jointInsured[0].Id => jointInsured[0]});	
            }
            
            message ='Success';  
            return message;
        }catch(Exception ee){
            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ee);
            log.Class__c = EWTransferPolicy.class.getName();
            log.Method__c = 'transferPolicy';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = transferPolicy'
                    + '\n\n ln: ' + ee.getLineNumber()
                    + '\n\n st: ' + ee.getStackTraceString();
            insert log;
            message ='Failure';
            System.debug('Exception occured'+ ee); 
            return message;
        }
    }
    /**
	* @methodName: updatePolicyBrokerage
	* @description: Used in MTA Quote OS, to transfer the Policy, Quote and Opportunity to old Brokerage.
	* @author: Bharati Pal
	* @dateCreated: 23/12/2020
	*/
    public static void updatePolicyBrokerage(Id policyId){    	
        
        Asset policy = [SELECT Id, EW_Quote__c,EW_BrokerageBeforeTransfer__c FROM Asset WHERE Id =: policyId LIMIT 1];        
        Quote quote = new Quote();
        Quote newquote = new Quote();
        Opportunity opportunities = new Opportunity();
        Asset newpolicy = new Asset();
        
        If(policy.EW_Quote__c != null){
            quote = [SELECT Id,EW_Source_Policy__c, OwnerId,OpportunityId FROM Quote WHERE Id =: policy.EW_Quote__c];
            newpolicy = [SELECT OwnerId, AccountId, EW_BrokerageBeforeTransfer__c, IBA_ProducingBrokerAccount__c, EW_Quote__c FROM Asset WHERE Id =: quote.EW_Source_Policy__c LIMIT 1];
            newquote = [SELECT Id,EW_Source_Policy__c, OwnerId,OpportunityId FROM Quote WHERE Id =: newpolicy.EW_Quote__c];
            opportunities = [SELECT OwnerId FROM Opportunity WHERE Id =: quote.OpportunityId]; 
        }
        try{
            If(newpolicy.EW_BrokerageBeforeTransfer__c != null){
                newpolicy.IBA_ProducingBrokerAccount__c = newpolicy.EW_BrokerageBeforeTransfer__c;
                newquote.vlocity_ins__AgencyBrokerageId__c = newpolicy.EW_BrokerageBeforeTransfer__c;
                newpolicy.EW_BrokerageBeforeTransfer__c = null; 
                
                update newpolicy;
                update newquote;
            }
            
        }catch(Exception ee ){
            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ee);
            log.Class__c = EWTransferPolicy.class.getName();
            log.Method__c = 'updatePolicyBrokerage';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = updatePolicyBrokerage'
                    + '\n\n ln: ' + ee.getLineNumber()
                    + '\n\n st: ' + ee.getStackTraceString();
            insert log;
            system.debug('Exception Occurred'+ee);
        }
    }
    
}