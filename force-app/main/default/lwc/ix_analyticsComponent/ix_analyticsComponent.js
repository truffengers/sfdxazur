import { LightningElement, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import getAnalyticsEvents from '@salesforce/apex/IXAnalyticsHelper.getAnalyticsEvents';
import Moment from '@salesforce/resourceUrl/Moment';

export default class IxAnalyticsComponent extends LightningElement {
    @track dataLoaded = false;
    @track analyticsData;

    @track showGeneralAnalytics = true;

    @track currentNumberOfDays = 'last 7 days';

    @track totalSessions;
    @track totalBounces;
    @track bounceRate;

    @track totalSearches;
    @track totalSuccessfulSearches;
    @track totalUnsuccessfulSearches;
    @track totalIndecisiveSearches;

    @track successfulSearchPercentage;
    @track unsuccessfulSearchPercentage;
    @track indecisiveSearchPercentage;

    @track totalThumbsUp;
    @track totalThumbsDown;
    @track thumbsUpPercentage;
    @track thumbsDownPercentage;

    @track totalUniqueUsers;
    @track totalUniqueUsersNames;
    @track totalUniqueBrokerages;
    @track totalUniqueBrokeragesNames;

    @track totalSessionsByBrokerage = [];

    @track totalSearchesByTerm = [];

    @track totalDropdownSelections;

    @track successfulSearches;
    @track unsuccessfulSearches;

    @track uiEvents;

    @track feedbackForms;

    connectedCallback() {
        // By default, the last 7 days' events are displayed.
        this.fetchAnalyticsEvents(7);
    }

    fetchAnalyticsEvents(numberOfDays) {
        getAnalyticsEvents({ numberOfDays })
            .then(data => {
                const excludedUserIds = [];
                const excludedUserNames = [
                    'Test Broker', 'BrokerHub Site Guest User',
                    'Tester Brokerhub', 'Andrew Xu', 'Tom Bradley'
                ];
                // const excludedUserNames = [];
                const filteredData = data.filter(event => {
                    // Check that each analytics event contains all the
                    // required fields
                    return !excludedUserIds.includes(event.createdById)
                        && !excludedUserNames.includes(event.CreatedBy.Name);
                })
                this.analyticsData = filteredData;

                loadScript(this, `${Moment}/moment.js`)
                    .then(() => {
                        this.sessionsCalculations();
                        this.searchCalculations();
                        this.thumbsCalculations();
                        this.usersCalculations();
                        this.dropdownCalculations();
                        this.searchQueryCalculations();
                        this.uiInteractions();
                        this.dataLoaded = true;
                    })
                    .catch(error => console.error(error)); // eslint-disable-line
            })
            .catch(error => console.error(error)); // eslint-disable-line
    }

    handleButtonClick({ target }) {
        this.dataLoaded = false;
        this.fetchAnalyticsEvents(target.dataset.value);
        this.showGeneralAnalytics = true;

        if (target.dataset.value) {
            this.currentNumberOfDays = `last ${target.dataset.value} days`;
        } else {
            this.currentNumberOfDays = `all-time`
        }
    }

    handleFeedbacks() {
        this.dataLoaded = false;
        this.feedbackForms = getAnalyticsEvents()
            .then(events => {
                let iterator = 0;

                this.feedbackForms = events
                    .filter(event => {
                        return event.Name === 'Feedback Submitted';
                    })
                    .map (event => {
                        iterator++;

                        const parsedEventData = JSON.parse(event.Event_Data__c);
                        const processedEventData = [];

                        Object.keys(parsedEventData).forEach(key => {
                            processedEventData.push({
                                name: key,
                                value: parsedEventData[key]
                            })
                        })

                        return {
                            ...event,
                            key: iterator,
                            Event_Data__c: processedEventData,
                            createdDate: this.makeDateTime(event.CreatedDate)
                        }
                    })
                    .reverse();
                    
                this.dataLoaded = true;
            })
            .catch(e => console.error(e)); // eslint-disable-line
        this.showGeneralAnalytics = false;
    }

    sessionsCalculations() {
        this.totalBounces = this.analyticsData
            .filter(event => {
                return event.Name === 'Session Ended'
                    && event.Event_Description__c.includes('bounced');
            })
            .length;

        this.totalSessions = this.analyticsData
            .filter(event => event.Name === 'Session Ended')
            .length;

        this.bounceRate = this.makePercentage(
            this.totalBounces, this.totalSessions);
    }

    searchCalculations() {
        const searchEventNames = [
            'Successful Search', 'Unsuccessful Search', 'Indecisive Search'];

        const searchEvents = this.analyticsData
            .filter(event => searchEventNames.includes(event.Name));

        this.totalSearches = searchEvents.length;

        const allSearchTerms = [];

        searchEvents.forEach(event => {
            if (event.Event_Data__c) {
                const lowerCaseEventData
                    = event.Event_Data__c.toLowerCase().trim();

                return !allSearchTerms.includes(lowerCaseEventData)
                    ? allSearchTerms.push(lowerCaseEventData)
                    : null;
            }
            return null;
        });

        this.totalSearchesByTerm = [];

        allSearchTerms.forEach(term => {
            this.totalSearchesByTerm.push({
                term,
                frequency: searchEvents.filter(event => {
                    if (event.Event_Data__c) {
                        return event
                            .Event_Data__c.toLowerCase().trim() === term;
                    }
                    return false;
                }).length
            });
        });

        this.totalSearchesByTerm.sort((a, b) => {
            return a.frequency <= b.frequency ? 1 : -1;
        });

        this.totalSuccessfulSearches = this.analyticsData
            .filter(event => event.Name === 'Successful Search')
            .length;

        this.totalUnsuccessfulSearches = this.analyticsData
            .filter(event => event.Name === 'Unsuccessful Search')
            .length;

        this.totalIndecisiveSearches = this.analyticsData
            .filter(event => event.Name === 'Indecisive Search')
            .length;

        this.successfulSearchPercentage
            = this.makePercentage(
                this.totalSuccessfulSearches, this.totalSearches);
        this.unsuccessfulSearchPercentage
            = this.makePercentage(
                this.totalUnsuccessfulSearches, this.totalSearches);
        this.indecisiveSearchPercentage
            = this.makePercentage(
                this.totalIndecisiveSearches, this.totalSearches);
    }

    thumbsCalculations() {
        this.totalThumbsUp = this.analyticsData
            .filter(event => event.Name === 'Thumbs Up Clicked')
            .length;

        this.totalThumbsDown = this.analyticsData
            .filter(event => event.Name === 'Thumbs Down Clicked')
            .length;

        this.thumbsUpPercentage = this.makePercentage(
            this.totalThumbsUp, this.totalSearches);
        this.thumbsDownPercentage = this.makePercentage(
            this.totalThumbsDown, this.totalSearches);
    }

    usersCalculations() {
        const uniqueIds = [];
        const uniqueNames = [];
        this.analyticsData.forEach(event => {
            if (!uniqueIds.includes(event.CreatedBy.Id)) {
                uniqueIds.push(event.CreatedBy.Id);
                uniqueNames.push(event.CreatedBy.Name);
            }
        });
        this.totalUniqueUsers = uniqueIds.length;
        this.totalUniqueUsersNames = uniqueNames;

        const uniqueBrokerages = [];
        this.analyticsData.forEach(event => {
            if (event.Brokerage__c
                && !uniqueBrokerages.includes(event.Brokerage__c)) {
                    uniqueBrokerages.push(event.Brokerage__c);
                }
        })
        this.totalUniqueBrokerages = uniqueBrokerages.length;
        this.totalUniqueBrokeragesNames = uniqueBrokerages;

        uniqueBrokerages.map(brokerage => {
            const brokerageSessionIds = [];
            this.analyticsData
                .filter(event => event.Brokerage__c === brokerage)
                .forEach(event => {
                    if (!brokerageSessionIds.includes(event.Session_ID__c)) {
                        brokerageSessionIds.push(event.Session_ID__c);
                    }
                })
            return this.totalSessionsByBrokerage.push({
                name: brokerage,
                totalSessions: brokerageSessionIds.length
            });
        });
    }

    dropdownCalculations() {
        this.totalDropdownSelections = this.analyticsData
            .filter(event => {
                return event.Name === 'Dropdown Item Selected';
            })
            .length;
    }

    searchQueryCalculations() {
        let successfulIterator = 0;
        this.successfulSearches = this.analyticsData
            .filter(event => event.Name === 'Successful Search')
            .map(event => {
                successfulIterator++;
                return {
                    key: successfulIterator,
                    text: event.Event_Data__c,
                    dropdownSelection: this.convertCharRef(
                        event.Event_Description__c),
                    userName: event.CreatedBy.Name,
                    createdDate: this.makeDateTime(event.CreatedDate)
                };
            })
            .reverse();

        let unsuccessfulIterator = 0;
        this.unsuccessfulSearches = this.analyticsData
            .filter(event => event.Name === 'Unsuccessful Search')
            .map(event => {
                unsuccessfulIterator++;
                return {
                    key: unsuccessfulIterator,
                    text: event.Event_Data__c,
                    userName: event.CreatedBy.Name,
                    createdDate: this.makeDateTime(event.CreatedDate)
                }
            })
            .reverse();

        let indecisiveIterator = 0;
        this.indecisiveSearches = this.analyticsData
            .filter(event => event.Name === 'Indecisive Search')
            .map(event => {
                indecisiveIterator++;
                return {
                    key: indecisiveIterator,
                    text: event.Event_Data__c,
                    userName: event.CreatedBy.Name,
                    createdDate: this.makeDateTime(event.CreatedDate)
                }
            })
            .reverse();
    }

    uiInteractions() {
        let uiIterator = 0;
        this.uiEvents = this.analyticsData
            .map(event => {
                uiIterator++;
                return {
                    key: uiIterator,
                    data: event.Event_Data__c,
                    userName: event.CreatedBy.Name,
                    action: event.Name,
                    description: this.convertCharRef(
                        event.Event_Description__c),
                    createdDate: this.makeDateTime(event.CreatedDate)
                }
            })
            .reverse();
    }

    makePercentage(numerator, denominator) {
        return ((numerator / denominator) * 100)
            .toString().substring(0, 5) + '%';
    }

    makeDateTime(dateString) {
        return moment(dateString).format('HH:mm:ss DD/MM/YY'); // eslint-disable-line
    }

    convertCharRef(string) {
        if (!string) {
            return '';
        }
        return string.split('&quot;').join('"').split('&amp;').join('&');
    }
}