TT.carouselCore = function($, lazySizes, Hammer, fsmFactory, options) {

  'use strict';

  var $container;
  var $newImg;
  var $oldImg;
  var $self;
  var actions;
  var advancer;
  var currentIndex;
  var data;
  var tagName;
  var fsm;
  var imagesCount;
  var loader;
  var newIndex;
  var settings;
  var swipeManager;
  var slideClass;

  var DEFAULTS = {
    transitionDuration : 500,
    name               : 'carousel',
    logger             : null,
    imageType          : 'background',
    automatic          : false
  };



  function createAdvancer(options) {

    var DEFAULTS = {
      showDuration : 7000
    };

    var advanceTimeoutId;
    var isEnabled = false;
    var settings;


    function setDefaults() {
      settings = $.extend({}, DEFAULTS, options || {});
    }

    function setAdvanceTimeout() {
      advanceTimeoutId = window.setTimeout(advance, settings.showDuration);
    }

    function clearAdvanceTimeout() {
      window.clearTimeout(advanceTimeoutId);
    }

    function startTimer() {
      if(isEnabled) { setAdvanceTimeout(); }
    }

    function enable() {
      isEnabled = true;
    }

    function disable() {
      isEnabled = false;
      clearAdvanceTimeout();
    }

    function teardown() {
      disable();
      advanceTimeoutId = null;
      options          = null;
      settings         = null;
    }

    function init() {
      setDefaults();
      enable();
    }

    init();



    return {
      startTimer : startTimer,
      enable     : enable,
      disable    : disable,
      teardown   : teardown
    };

  }




  function createSwipeManager(Hammer, element) {

    var hammertime;

    return {

      init: function() {
        hammertime = new Hammer(element, { dragLockToAxis: true, dragBlockHorizontal: true });
        hammertime.on('swipeleft', next);
        hammertime.on('swiperight', previous);
      },

      teardown: function() {
        hammertime.destroy();
        hammertime = null;
        element    = null;
      }

    };

  }





  function createBackgroundImageLoader() {

    return {
      load: function($element) {
        var intervalId = window.setInterval(function() {
          var background = $element.css('backgroundImage');
          if(background !== 'none') {
            window.clearInterval(intervalId);
            $element.triggerHandler('really.loaded');
          }
        }, 25);
      }
    };

  }




  function createInlineImageLoader() {

    var $img;

    function loadHandler() {
      $img.triggerHandler('really.loaded');
    }

    function lazybeforeunveilHandler() {
      $img.on('load', loadHandler);
    }

    return {
      load: function(value) {
        $img = value;
        $img.on('lazybeforeunveil', lazybeforeunveilHandler);
      }
    };

  }





  function init(index) {
    currentIndex = index || 0;
    newIndex     = 0;
    data         = options.data;
    $container   = options.slidesContainerSelector.jquery === undefined ? $(options.slidesContainerSelector) : options.slidesContainerSelector;
    slideClass   = options.slideClass;
    delete options.container;
    delete options.data;
    delete options.slideClass;
    settings = $.extend({}, DEFAULTS, options);
    initFsm(index);
  }

  function removeOldImage() {
    if($oldImg) {
      $oldImg.remove();
      $oldImg = undefined;
    }
  }

  function revealedHandler() {
    removeOldImage();
    if(advancer) { advancer.startTimer(); }
    settings.revealed(currentIndex, $newImg);
    fsm.trigger('revealed');
  }

  function checkIfPositionChanged() {
    if(newIndex !== currentIndex) {
      currentIndex = newIndex;
      fsm.trigger('positionChanged');
    }
  }

  function select(index) {
    newIndex = index;
    settings.changed(newIndex, imagesCount);
    if(fsm.get_state() === 'showing') {
      checkIfPositionChanged();
    }
  }

  function manualSelect(index) {
    disableAdvancer();
    select(index);
  }

  function shuffle(direction) {
    select((newIndex + direction + imagesCount) % imagesCount);
  }

  function previous() {
    disableAdvancer();
    shuffle(-1);
  }

  function next() {
    disableAdvancer();
    shuffle(1);
  }

  function advance() {
    shuffle(1);
  }

  function getIndex() {
    return currentIndex;
  }

  function teardown() {
    if($newImg) {
      $newImg.unbind();
      $newImg.remove();
      $newImg = null;
    }
    if(advancer) { advancer.teardown(); }
    if(swipeManager) { swipeManager.teardown(); }
    removeOldImage();
    fsm        = { trigger: $.noop };
    $self      = null;
    actions    = null;
    $container = null;
  }

  function addLoadListener() {
    $newImg.bind('really.loaded', function() {
      $newImg.unbind('really.loaded');
      fsm.trigger('loaded');
      settings.loaded(currentIndex, $newImg);
      if(!$oldImg) {
        settings.loadedFirst();
      }
    });
  }

  function preventDrag() {
    $newImg.on('dragstart', function(event) { event.preventDefault(); });
  }

  function initAdvancer() {
    if(settings.automatic) {
      advancer = createAdvancer({ advance: advance, showDuration: settings.advancerShowDuration });
    }
  }

  function initSwipeManager() {
    swipeManager = createSwipeManager(Hammer, $container[0]);
    swipeManager.init();
  }

  function disableAdvancer() {
    if(advancer) { advancer.disable(); }
  }

  function initLoader() {
    switch(settings.imageType) {
    case 'background':
      loader = createBackgroundImageLoader();
      tagName = 'div';
      break;
    case 'image':
      loader = createInlineImageLoader();
      tagName = 'img';
      break;
    }
  }

  function generateElement(attributes) {

    function attributesHtml() {
      var string = '';
      $.each(attributes, function(key, value) {
        string += ' data-' + key + '="' + value + '"';
      });
      return string;
    }

    function classHtml() {
      return slideClass ? ' class="' + slideClass + '"' : '';
    }

    function html() {
      return '<' + tagName + attributesHtml() + classHtml() + ' />';
    }

    return $(html());
  }



  actions = {

    initialise: function() {
      imagesCount = data.length;
      if(imagesCount > 0) {
        currentIndex = currentIndex < imagesCount ? currentIndex : 0;
        initLoader();
        initAdvancer();
        initSwipeManager();
        fsm.trigger('initialised');
      } else {
        fsm.trigger('emptyDataFound');
      }
    },

    load: function() {
      $oldImg = $newImg;
      $newImg = generateElement(data[currentIndex]);
      settings.loading(currentIndex, $newImg);
      $newImg.css({zIndex: 2, opacity: 0});
      addLoadListener();
      loader.load($newImg);
      $container.append($newImg);
      preventDrag();
      lazySizes.loader.unveil($newImg[0]);
    },

    reveal: function() {
      $newImg.transition({opacity: 1}, settings.transitionDuration, revealedHandler);
    },

    checkIfPositionChanged: checkIfPositionChanged
  };



  function initFsm() {

    fsm = fsmFactory(settings.name, actions, settings.logger);

    fsm.define('initialising')
      .entry('initialise')
      .transition('initialised', 'loading')
      .transition('emptyDataFound', 'sleeping');

    fsm.define('loading')
      .entry('load')
      .transition('loaded', 'revealing');

    fsm.define('revealing')
      .entry('reveal')
      .transition('revealed', 'showing');

    fsm.define('showing')
      .entry('checkIfPositionChanged')
      .transition('positionChanged', 'loading');

    fsm.define('sleeping');

    fsm.begin();
  }



  return {
    init      : init,
    select    : manualSelect,
    teardown  : teardown,
    advance   : advance,
    previous  : previous,
    next      : next,
    getIndex  : getIndex
  };

};