/**
 * Created by roystonlloyd on 2019-05-28.
 */
@isTest
public with sharing class EWClauseAssignmentHelperTest {


    static List<Quote> quotes;
    static List<vlocity_ins__DocumentClause__c> documentClauses;
    static Account insured;
    static Account broker;

    public static testMethod void TestRefreshClauses() {

        setup(1);
        Quote quote = quotes[0];

        Test.startTest();

        // check no clauses
        System.assertEquals(null, [SELECT EW_Clauses__c FROM Quote WHERE Id = :quote.Id].EW_Clauses__c);

        // check insert and two clauses
        List<EW_ClauseAssignment__c> clauseAssignments = new List<EW_ClauseAssignment__c>();
        clauseAssignments.add(EWClauseAssignmentDataFactory.createClauseAssignment(documentClauses[0].Id, quote.Id, null, false));
        clauseAssignments.add(EWClauseAssignmentDataFactory.createClauseAssignment(documentClauses[1].Id, quote.Id, null, false));
        insert clauseAssignments;

        Quote checkQuote = [SELECT EW_Clauses__c FROM Quote WHERE Id = :quote.Id];
        system.debug('checkQuote ' + checkQuote);
        System.assertEquals(true, checkQuote.EW_Clauses__c.contains('Name1'));
        System.assertEquals(true, checkQuote.EW_Clauses__c.contains('Name2'));

        // check delete and one clause
        delete clauseAssignments[1];
        checkQuote = [SELECT EW_Clauses__c FROM Quote WHERE Id = :quote.Id];
        System.assertEquals(true, checkQuote.EW_Clauses__c.contains('Name1'));
        System.assertEquals(false, checkQuote.EW_Clauses__c.contains('Name2'));

        // check undelete and two clauses
        undelete clauseAssignments[1];
        checkQuote = [SELECT EW_Clauses__c FROM Quote WHERE Id = :quote.Id];
        System.assertEquals(true, checkQuote.EW_Clauses__c.contains('Name1'));
        System.assertEquals(true, checkQuote.EW_Clauses__c.contains('Name2'));

        // check update and one clause
        clauseAssignments[0].EW_Quote__c = null;
        update clauseAssignments[0];
        checkQuote = [SELECT EW_Clauses__c FROM Quote WHERE Id = :quote.Id];
        System.assertEquals(false, checkQuote.EW_Clauses__c.contains('Name1'));
        System.assertEquals(true, checkQuote.EW_Clauses__c.contains('Name2'));

        Test.stopTest();

    }


    public static testMethod void testManualClauseAssignments() {

        setup(2);

        Quote boundQuote = quotes[0];
        Quote quote = quotes[1];

        List<EW_ClauseAssignment__c> clauseAssignments = new List<EW_ClauseAssignment__c>();
        for(vlocity_ins__DocumentClause__c dc : documentClauses){
            dc.vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_RESTRICTED;

            EW_ClauseAssignment__c ca = EWClauseAssignmentDataFactory.createClauseAssignment(dc.Id, boundQuote.Id, null, false);
            ca.EW_Assigned_By_Underwriter__c = true;
            clauseAssignments.add(ca);
        }
        update documentClauses;
        insert clauseAssignments;

        Test.startTest();
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outputMap = new Map<String,Object>();
        Map<String,Object> optionMap = new Map<String,Object>{'boundQuoteId' => boundQuote.Id, 'quoteId' => quote.Id, 'transactionType' => EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL};
        new EWVlocityRemoteActionHelper().invokeMethod('manualClauseAssignments', inputMap, outputMap, optionMap);
        Test.stopTest();

        System.assertEquals(2, [SELECT Count() FROM EW_ClauseAssignment__c WHERE EW_Quote__c = :quote.Id]);
    }


    public static testMethod void testManualClauseAssignmentsWithNoIds() {

        Test.startTest();
        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> outputMap = new Map<String,Object>();
        Map<String,Object> optionMap = new Map<String,Object>();
        new EWVlocityRemoteActionHelper().invokeMethod('manualClauseAssignments', inputMap, outputMap, optionMap);
        Test.stopTest();

        System.assertEquals(true, outputMap.containsKey('Error'));
    }


    public static testMethod void testGetStandardClauses_TestMTA(){
        setup(2);

        Asset policy = EWPolicyDataFactory.createPolicy('policy1', quotes[0].OpportunityId, broker.Id, insured.Id, false);
        policy.EW_Quote__c = quotes[0].Id;
        insert policy;

        quotes[0].EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE;
        quotes[0].EW_Policy__c = policy.Id;
        quotes[1].EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_MTA;
        quotes[1].EW_Source_Policy__c = policy.Id;
        update quotes;

        List<vlocity_ins__DocumentClause__c> clauses = [SELECT Id, Name, EW_Clause_Name__c FROM vlocity_ins__DocumentClause__c ORDER BY EW_Clause_Name__c ASC];

        vlocity_ins__DocumentClause__c dc1 = clauses[0].clone(false,false,false,false);
        dc1.EW_Start_Date__c = Date.today().addDays(2);
        clauses.add(dc1);
        vlocity_ins__DocumentClause__c dc2 = clauses[0].clone(false,false,false,false);
        dc2.EW_Start_Date__c = Date.today().addDays(3);
        clauses.add(dc2);

        for(vlocity_ins__DocumentClause__c dc : clauses) {
            dc.vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD;
        }
        upsert clauses;

        Map<String,Object> retrievedObj;

        Test.startTest();
        Map<String, Object> inputMap = new Map<String, Object>{'quoteId' => quotes[1].Id};
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{'clauseNames' => new List<Object>{'Name1', 'Name2'}};
        new EWVlocityRemoteActionHelper().invokeMethod('getStandardClauses', inputMap, outputMap, optionMap);

        retrievedObj = (Map<String,Object>) outputMap.get('Name1');

        Test.stopTest();

        System.assertEquals('Name1', retrievedObj.get('EW_Clause_Name__c'));
        System.assertEquals(null, retrievedObj.get('EW_Start_Date__c'));

    }

    public static testMethod void testGetStandardClauses_TestNB1(){
        setup(1);

        quotes[0].EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE;
        update quotes;

        List<vlocity_ins__DocumentClause__c> clauses = [SELECT Id,Name FROM vlocity_ins__DocumentClause__c ORDER BY Name ASC];

        vlocity_ins__DocumentClause__c dc1 = clauses[0].clone(false,false,false,false);
        dc1.EW_Start_Date__c = Date.today().addDays(-1);
        clauses.add(dc1);
        vlocity_ins__DocumentClause__c dc2 = clauses[0].clone(false,false,false,false);
        dc2.EW_Start_Date__c = Date.today().addDays(2);
        clauses.add(dc2);

        for(vlocity_ins__DocumentClause__c dc : clauses) {
            dc.vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD;
        }
        upsert clauses;

        Map<String,Object> retrievedObj;

        Test.startTest();
        Map<String, Object> inputMap = new Map<String, Object>{'quoteId' => quotes[0].Id};
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{'clauseNames' => new List<Object>{'Name1', 'Name2'}};
        new EWVlocityRemoteActionHelper().invokeMethod('getStandardClauses', inputMap, outputMap, optionMap);

        retrievedObj = (Map<String,Object>) outputMap.get('Name1');

        Test.stopTest();

        System.assertEquals('Name1', retrievedObj.get('EW_Clause_Name__c'));
        System.assertEquals(JSON.serialize(dc1.EW_Start_Date__c).replace('"',''), retrievedObj.get('EW_Start_Date__c'));

    }

    public static testMethod void testGetStandardClauses_TestNB2(){
        setup(1);

        quotes[0].EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE;
        update quotes;

        List<vlocity_ins__DocumentClause__c> clauses = [SELECT Id,Name FROM vlocity_ins__DocumentClause__c ORDER BY Name ASC];

        vlocity_ins__DocumentClause__c dc1 = clauses[0].clone(false,false,false,false);
        dc1.EW_Start_Date__c = Date.today().addDays(-5);
        clauses.add(dc1);
        vlocity_ins__DocumentClause__c dc2 = clauses[0].clone(false,false,false,false);
        dc2.EW_Start_Date__c = Date.today().addDays(0);
        clauses.add(dc2);

        for(vlocity_ins__DocumentClause__c dc : clauses) {
            dc.vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD;
        }
        upsert clauses;

        Map<String,Object> retrievedObj;

        Test.startTest();
        Map<String, Object> inputMap = new Map<String, Object>{'quoteId' => quotes[0].Id};
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{'clauseNames' => new List<Object>{'Name1', 'Name2'}};
        new EWVlocityRemoteActionHelper().invokeMethod('getStandardClauses', inputMap, outputMap, optionMap);

        retrievedObj = (Map<String,Object>) outputMap.get('Name1');

        Test.stopTest();

        System.assertEquals('Name1', retrievedObj.get('EW_Clause_Name__c'));
        System.assertEquals(JSON.serialize(dc2.EW_Start_Date__c).replace('"',''), retrievedObj.get('EW_Start_Date__c'));

    }


    /**
    * @methodName: testUpdateRemovedFlag
    * @description: test method for the EWClauseAssignmentHelper.updateRemovedFlag method.
    * @author: Roy Lloyd
    * @date: 6/3/2020
    */
    private static testMethod void testUpdateRemovedFlag(){

        setup(2);

        Asset policy = EWPolicyDataFactory.createPolicy('policy0', quotes[0].OpportunityId, broker.Id, insured.Id, false);
        policy.EW_Quote__c = quotes[0].Id;
        insert policy;

        quotes[0].EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_BUSINESS_TYPE;
        quotes[1].EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_MTA;
        quotes[1].EW_Source_Policy__c = policy.Id;
        update quotes;

        EW_ClauseAssignment__c ca0 = EWClauseAssignmentDataFactory.createClauseAssignment(documentClauses[0].Id, quotes[0].Id, null, false);
        ca0.EW_Active__c = true;
        EW_ClauseAssignment__c ca1 = EWClauseAssignmentDataFactory.createClauseAssignment(documentClauses[0].Id, quotes[1].Id, null, false);
        ca1.EW_Active__c = true;
        insert ca0;

        // confirm pretest condition
        System.assertEquals(false,  [SELECT EW_RemovedInSuccessiveQuote__c FROM EW_ClauseAssignment__c WHERE Id =: ca0.Id].EW_RemovedInSuccessiveQuote__c);

        test.startTest();

        insert ca1;
        System.assertEquals(false,  [SELECT EW_RemovedInSuccessiveQuote__c FROM EW_ClauseAssignment__c WHERE Id =: ca0.Id].EW_RemovedInSuccessiveQuote__c);

        delete ca1;
        System.assertEquals(true,   [SELECT EW_RemovedInSuccessiveQuote__c FROM EW_ClauseAssignment__c WHERE Id =: ca0.Id].EW_RemovedInSuccessiveQuote__c);

        undelete ca1;
        System.assertEquals(false,  [SELECT EW_RemovedInSuccessiveQuote__c FROM EW_ClauseAssignment__c WHERE Id =: ca0.Id].EW_RemovedInSuccessiveQuote__c);

        test.stopTest();

    }


    private static void setup(Integer noOfQuotes){

        broker = EWAccountDataFactory.createBrokerAccount(null, true);

        insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true);

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

        quotes = new List<Quote>();
        for(Integer i=0 ; i<noOfQuotes ; i++){
            quotes.add(EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, false));
        }
        insert quotes;

        documentClauses = new List<vlocity_ins__DocumentClause__c>();
        documentClauses.add(EWDocumentClauseDataFactory.createDocumentClause('Name1', null, true, false));
        documentClauses.add(EWDocumentClauseDataFactory.createDocumentClause('Name2', null, true, false));
        insert documentClauses;
    }

}