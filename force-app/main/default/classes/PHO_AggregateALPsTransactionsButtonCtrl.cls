/**
* @name: PHO_AggregateALPsTransactionsButtonCtrl
* @description: Controller class for PHO_AggregateALPsTransactionsButton page.
*               Invokes aggregation of ALPs data on Policy Transactions.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 07/01/2020
*/
public with sharing class PHO_AggregateALPsTransactionsButtonCtrl {

    private ApexPages.StandardSetController standardSetController;
    private PageReference originalPage;

    public PHO_AggregateALPsTransactionsButtonCtrl(ApexPages.StandardSetController standardSetController) {
        this.standardSetController = standardSetController;
        this.originalPage = standardSetController.cancel();
    }

    /**
    * @methodName: aggregateALPsTransactions
    * @description: Invokes asynchronous job to aggregate ALPs financial data on Policy Transactions.
    * @dateCreated: 07/01/2020
    */
    public PageReference aggregateALPsTransactions() {

        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();
        if (settings.PHO_ALPsDataAggregationRecordsLimit__c > 0) {
            System.enqueueJob(new PHO_AggregateALPsTransactionsJob(settings.PHO_ALPsDataAggregationRecordsLimit__c.intValue()));
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to set PHO_ALPsDataAggregationRecordsLimit__c in IBA_IBACustomSettings__c custom setting to execute this action'));
            return null;
        }

        return this.originalPage;
    }

    /**
    * @methodName: goBack
    * @description: Redirects to original page.
    * @dateCreated: 07/01/2020
    */
    public PageReference goBack() {
        return this.originalPage;
    }
}