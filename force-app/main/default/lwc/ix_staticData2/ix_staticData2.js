import { CONSTANTS, STATIC_EXCLUSIONS, STATIC_CLAUSES } from 'c/ix_constants';

const STATIC_DATA_2 = {
    'Buildings, Contents, Pedal Cycles and Collections': {
        [CONSTANTS.BUILDINGS]: {
            [CONSTANTS.SUM_INSURED]: '£1,000,000.00',
            [CONSTANTS.SUBHEADING]: '<p>We will insure the property shown in your schedule against all risks of physical loss or physical damage which happens during the policy period unless an exclusion applies.</p><p>We will pay the cost of repairing or reinstating your tenants’ improvements, buildings or other permanent structures at the same location with materials and workmanship of like kind and quality. This includes the fees, costs and expenses agreed by us, which are necessarily incurred in the repair or reinstatement of the damaged buildings or other permanent structures.</p><p><strong>Unoccupied Property</strong><br/>The definition of an unoccupied property is: Not lived in for 60 consecutive days or not adequately furnished to be lived in normally.</p><p>As stated in the General Conditions, please tell us if a home on cover is going to be unoccupied or unfurnished. Losses are more likely to occur in unoccupied or unfurnished properties so we may amend the terms of your insurance.</p>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>We will not pay for the cost of replacing any undamaged item(s) or parts of items forming part of a pair, set, suite or other article of a uniform nature, colour or design when damage occurs within a clearly identifiable area or to a specific part and replacements cannot be matched other than fitted kitchens and bathroom suites.</p><p>It does not include any amount required for the excavation, replacement or stabilisation of land under or around a structure, other than the cover provided under C Buildings cover – Additional covers, 4. Land stabilisation.</p><p>Following a total loss, where we deem the building or other permanent structures to be beyond economical repair or reconstruction, any salvage shall become our property.</p>',
            [CONSTANTS.CLAUSES]: {
                [CONSTANTS.CLAUSES_NAMES.FLAT_ROOF]: STATIC_CLAUSES.FLAT_ROOF
            },
            [CONSTANTS.EXCLUSIONS]: {
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            }
        },
        [CONSTANTS.OTHER_PERMANENT_STRUCTURES]: {
            [CONSTANTS.SUM_INSURED]: '£200,000.00',
            [CONSTANTS.SUBHEADING]: '<p>We will pay the cost of repairing or reinstating your tenants’ improvements, buildings or other permanent structures at the same location with materials and workmanship of like kind and quality. This includes the fees, costs and expenses agreed by us, which are necessarily incurred in the repair or reinstatement of the damaged buildings or other permanent structures.</p><p><strong>Unoccupied Property</strong><br/>The definition of an unoccupied property is: Not lived in for 60 consecutive days or not adequately furnished to be lived in normally.</p><p>As stated in the General Conditions, please tell us if a home on cover is going to be unoccupied or unfurnished. Losses are more likely to occur in unoccupied or unfurnished properties so we may amend the terms of your insurance.</p>',
            [CONSTANTS.EXCLUSIONS]: {
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            }
        },
        [CONSTANTS.CONTENTS]: {
            [CONSTANTS.SUM_INSURED]: 'Unlimited',
            [CONSTANTS.SUBHEADING]: '<p>Your contents, pedal cycles and collections are insured against all risks of physical loss or physical damage while at your home and while temporarily removed for no longer than 60 consecutive days anywhere in the world unless an exclusion applies.</p><p><strong>Contents Away From the Home</strong><br/>Your contents are covered anywhere in the world while temporarily removed, unless they are removed for longer than 60 days or an exclusion applies.</p>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>For a covered loss to your contents, art and collectables, jewellery and watches or pedal cycles, we will at our option decide whether to:</p><ul> <li>repair the item;</li><li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul><p>We will not deduct anything for wear and tear.<p><p><strong>Payment for specified items and unspecified items:</strong></p><ol type="a"> <li> <p>Specified items</p><ol type="i"> <li> <p>Total loss</p><p>For a covered loss to an item listed in your schedule of items, we will at our option decide whether to:</p><ul> <li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul> </li><li> <p>Partial loss</p><p>If only part of the specified item is lost or damaged, we shall pay either the amount to restore the item to its condition immediately before the loss or to make up the difference between its market value before and after the loss. If after the restoration the market value of the item is less than its market value immediately before the loss, we shall pay the difference. In no event shall payment exceed the sum insured for that item.</p></li></ol> </li><li> <p>Unspecified items</p><p>We shall pay the amount required to restore or replace the property, whichever is less, without deduction for depreciation, for a covered loss to collections with unspecified cover as shown in the schedule. If after the restoration the market value of the item is less than its market value immediately prior to the loss,we shall pay the difference. We will not pay more than the single article limit as shown in your schedule</p><p>We will not pay for the cost of replacing any undamaged item(s) or parts of items forming part of a pair, set, suite or other article of a uniform nature, colour or design when damage occurs within a clearly identifiable area or to a specific part and replacements cannot be matched except:</p><ol type="i"> <li>for items of art, jewellery and watches – provided that you surrender any undamaged matching item(s) and/or parts to us and we agree to accept them, we will at our option, replace or pay the replacement cost of the complete matching set.</li><li>for all other contents – we will pay up to 50% of the cost of replacing any undamaged matching item(s) or parts of matching items.</li></ol> </li></ol><p>In no event shall payment exceed the sum insured for that item or the unspecified single article limit as shown in your schedule.</p>',
            [CONSTANTS.EXCLUSIONS]: {
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ANIMALS]: STATIC_EXCLUSIONS.ANIMALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: STATIC_EXCLUSIONS.ELECTRONIC_DATA,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE]: STATIC_EXCLUSIONS.MALICIOUS_COMPUTER_USE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL]: STATIC_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            },
            [CONSTANTS.CLAUSES]: {
                [CONSTANTS.CLAUSES_NAMES.PERSONAL_CUSTODY]: STATIC_CLAUSES.PERSONAL_CUSTODY,
                [CONSTANTS.CLAUSES_NAMES.VALUATIONS]: STATIC_CLAUSES.VALUATIONS
            }
        },
        [CONSTANTS.PEDAL_CYCLES]: {
            [CONSTANTS.SUM_INSURED]: 'Refer to schedule',
            [CONSTANTS.SUBHEADING]: '<p>Your pedal cycles are insured against all risks of physical loss or physical damage while at your home and while temporarily removed for no longer than 60 consecutive days anywhere else in the world unless an exclusion applies.</p><p><strong>Contents Away From the Home</strong><br />Your contents are covered anywhere in the world while temporarily removed, unless they are removed for longer than 60 days or an exclusion applies.</p>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>For a covered loss to your contents, art and collectables, jewellery and watches or pedal cycles, we will at our option decide whether to:</p><ul> <li>repair the item;</li><li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul><p>We will not deduct anything for wear and tear.<p><p><strong>Payment for specified items and unspecified items:</strong></p><ol type="a"> <li> <p>Specified items</p><ol type="i"> <li> <p>Total loss</p><p>For a covered loss to an item listed in your schedule of items, we will at our option decide whether to:</p><ul> <li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul> </li><li> <p>Partial loss</p><p>If only part of the specified item is lost or damaged, we shall pay either the amount to restore the item to its condition immediately before the loss or to make up the difference between its market value before and after the loss. If after the restoration the market value of the item is less than its market value immediately before the loss, we shall pay the difference. In no event shall payment exceed the sum insured for that item.</p></li></ol> </li><li> <p>Unspecified items</p><p>We shall pay the amount required to restore or replace the property, whichever is less, without deduction for depreciation, for a covered loss to collections with unspecified cover as shown in the schedule. If after the restoration the market value of the item is less than its market value immediately prior to the loss,we shall pay the difference. We will not pay more than the single article limit as shown in your schedule</p><p>We will not pay for the cost of replacing any undamaged item(s) or parts of items forming part of a pair, set, suite or other article of a uniform nature, colour or design when damage occurs within a clearly identifiable area or to a specific part and replacements cannot be matched except:</p><ol type="i"> <li>for items of art, jewellery and watches – provided that you surrender any undamaged matching item(s) and/or parts to us and we agree to accept them, we will at our option, replace or pay the replacement cost of the complete matching set.</li><li>for all other contents – we will pay up to 50% of the cost of replacing any undamaged matching item(s) or parts of matching items.</li></ol> </li></ol><p>In no event shall payment exceed the sum insured for that item or the unspecified single article limit as shown in your schedule.</p>',
            [CONSTANTS.EXCLUSIONS]: {
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNLOCKED_PEDAL_CYCLE]: STATIC_EXCLUSIONS.UNLOCKED_PEDAL_CYCLE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            }
        },
        [CONSTANTS.ART_AND_COLLECTABLES]: {
            [CONSTANTS.SUM_INSURED]: 'Refer to Schedule',
            [CONSTANTS.SUBHEADING]: '<p>Your Art and Collectables are insured against all risks of physical loss or physical damage while at your home and while temporarily removed for no longer than 60 consecutive days anywhere else in the world unless an exclusion applies.</p><p><strong>Contents Away From the Home</strong><br/>Your contents are covered anywhere in the world while temporarily removed, unless they are removed for longer than 60 days or an exclusion applies.</p>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>For a covered loss to your contents, art and collectables, jewellery and watches or pedal cycles, we will at our option decide whether to:</p><ul> <li>repair the item;</li><li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul><p>We will not deduct anything for wear and tear.<p><p><strong>Payment for specified items and unspecified items:</strong></p><ol type="a"> <li> <p>Specified items</p><ol type="i"> <li> <p>Total loss</p><p>For a covered loss to an item listed in your schedule of items, we will at our option decide whether to:</p><ul> <li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul> </li><li> <p>Partial loss</p><p>If only part of the specified item is lost or damaged, we shall pay either the amount to restore the item to its condition immediately before the loss or to make up the difference between its market value before and after the loss. If after the restoration the market value of the item is less than its market value immediately before the loss, we shall pay the difference. In no event shall payment exceed the sum insured for that item.</p></li></ol> </li><li> <p>Unspecified items</p><p>We shall pay the amount required to restore or replace the property, whichever is less, without deduction for depreciation, for a covered loss to collections with unspecified cover as shown in the schedule. If after the restoration the market value of the item is less than its market value immediately prior to the loss, we shall pay the difference. We will not pay more than the single article limit as shown in your schedule</p></li></ol>',
            [CONSTANTS.EXCLUSIONS]: {
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            },
            [CONSTANTS.CLAUSES]: {
                [CONSTANTS.CLAUSES_NAMES.VALUATIONS]: STATIC_CLAUSES.VALUATIONS
            }
        },
        [CONSTANTS.JEWELLERY_AND_WATCHES]: {
            [CONSTANTS.SUM_INSURED]: 'Refer to Schedule',
            [CONSTANTS.SUBHEADING]: '<b>Your Jewellery and Watches are insured sgainst all risks of physical loss or physical damage while at your home and while temporarily removed for no longer than 60 consecutive days anywhere else in the world unless an exclusion applies.</b>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: 'For a covered loss to your contents, art and collectables, jewellery and watches or pedal cycles, we will at our option decide whether to:<ul><li>repair the item;</li><li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality;</li><li>make a cash settlement for the value at which we could replace the item.</li><li>We will not deduct anything for wear and tear.</li></ul>Payment for specified items and unspecified items:<ul type="a"><li>a. Specified items<ul><li>Total loss<br /><br />For a covered loss to an item listed in your schedule of items, we will at our option decide whether to:<ul><li>replace the item as new, if a replacement is not available we will replace it with an item of similar quality; or</li><li>make a cash settlement for the value at which we could replace the item.</li></ul></li><li>Partial loss<br /><br />If only part of the specified item is lost or damaged, we shall pay either the amount to restore the item to its condition immediately before the loss or to make up the difference between its market value before and after the loss. If after the restoration the market value of the item is less than its market value immediately before the loss, we shall pay the difference. In no event shall payment exceed the sum insured for that item.</li></li><li>Unspecified items<br /><br />We shall pay the amount required to restore or replace the property, whichever is less, without deduction for depreciation, for a covered loss to collections with unspecified cover as shown in the schedule. If after the restoration the market value of the item is less than its market value immediately prior to the loss,we shall pay the difference. We will not pay more than the single article limit as shown in your schedule.</li></ul>',
            [CONSTANTS.EXCLUSIONS]: {
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            },
            [CONSTANTS.CLAUSES]: {
                [CONSTANTS.CLAUSES_NAMES.PERSONAL_CUSTODY]: STATIC_CLAUSES.PERSONAL_CUSTODY,
                [CONSTANTS.CLAUSES_NAMES.VALUATIONS]: STATIC_CLAUSES.VALUATIONS
            }
        },
        [CONSTANTS.BUILDINGS_ADDITIONAL_COVERS]: {
            [CONSTANTS.ADDITIONAL_COVERS.ALTERNATIVE_ACCOMMODATION]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: 'Three years / max £200,000',
                [CONSTANTS.SUBHEADING]: 'If a covered loss makes your home uninhabitable, we will also cover the reasonable costs for alternative accommodation and any ground rent you have paid or are obligated to pay up to the sum insured or timeframe shown in your policy schedule. Payment will continue for the shortest reasonable amount of time necessary to restore your home to a habitable condition. This includes accommodation for your domestic pets and horses. If you are not able to rent out your home, that you usually rent to others because of a covered loss, we will pay the rent you would have received including ground rent for the reasonable amount of time necessary to restore your home to a habitable condition, up to the sum insured shown in your policy schedule.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTRACTOR_INSURANCE]: STATIC_EXCLUSIONS.CONTRACTOR_INSURANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.TENANTS_IMPROVEMENTS]: {
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured in your schedule for tenants improvements which covers the fixtures, fittings and interior decorations which are fixed to and form part of the structure of your home against physical loss or physical damage which happens during the policy period. This cover applies where you do not own or are not responsible for insuring the buildings of your home.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTRACTOR_INSURANCE]: STATIC_EXCLUSIONS.CONTRACTOR_INSURANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.DISABILITY_COSTS]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£25,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured in your schedule in total for essential alterations to your home to allow you or a family member to live unassisted following permanent disablement as a result of either an illness or injury which first occurs during the policy period',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.FATAL_INJURY]: {
                [CONSTANTS.SUM_INSURED]: '£50,000 but not more than £5,000 for any persons under 16 years of age.',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured in your schedule in total for fatal injury by fire, lightning, aircraft, explosion or physical assault to you at your home, should you die within six months of the event. The event must be the sole or predominant attributable cause of death.'
            },
            [CONSTANTS.ADDITIONAL_COVERS.TRAUMA_COVER]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£5,000',
                [CONSTANTS.SUBHEADING]: 'Following a violent crime being committed against you or a family member in your home during the current policy period, we will pay up to the sum insured in your schedule (subject to our prior consent and approval) to either carry out necessary improvements to the security at your home or for necessary conveyancing, removal and estate agency fees if, within 90 days of the incident, you feel compelled to move house and had not already planned to do so. This additional cover will cease to be payable after 12 months from the date of the incident.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.FORCED_EVACUATION]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£1,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured in your schedule for the cost of alternative accommodation for you, your domestic pets and horses if a local authority or emergency service prohibits your home from being lived in. We will also cover any loss of rent for up to thirty (30) days if your home is rented to others.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.SALE_OF_YOUR_HOME]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: 'property specific',
                [CONSTANTS.SUBHEADING]: '<p>If you enter into a contract to sell any buildings shown in your schedule, we will cover that building, at the same terms and conditions, for the buyer from the time you exchange contracts (or in Scotland the offer to purchase) until completion of the sale. We will only do this if, and for so long as:</p><ol type="a"><li>the building is not insured by, or does not have the benefit of, any other insurance</li><li>the building is not occupied; and</li><li>the policy remains in force</li>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.TRACING_A_LEAK]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: 'Within your home - £20,000; Outside your home - £10,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for the cost to remove and replace part of your home in order to find the source of the water, oil or gas leak from any fixed domestic water or heating installation or storage tank and the subsequent repairs to the walls, floors or ceilings, driveways, paths, patios or gardens. The leak must occur during the policy period and we do not cover loss or damage to the heating or water system itself.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.LAND_STABILISATION]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£50,000',
                [CONSTANTS.SUBHEADING]: 'In the event of a covered loss to your buildings or other permanent structures we will pay up to the sum insured shown in your schedule for required stabilisation, excavation, or replacement of land under or around your buildings or other permanent structures.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS
                }
            }
        },
        [CONSTANTS.CONTENTS_ADDITIONAL_COVERS]: {
            [CONSTANTS.ADDITIONAL_COVERS.BUSINESS_EQUIPMENT]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£30,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for a covered loss to business equipment at a home listed on the schedule which happens during the policy period.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ANIMALS]: STATIC_EXCLUSIONS.ANIMALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: STATIC_EXCLUSIONS.ELECTRONIC_DATA,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL]: STATIC_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.STUDENT_CONTENTS]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£5,000',
                [CONSTANTS.SUBHEADING]: 'We will pay for physical loss or physical damage to contents which happens during the policy period up to the sum insured shown in your schedule for permanent members of your household who are in full time education while they are studying away from home.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.NO_PROOF_OF_THEFT]: STATIC_EXCLUSIONS.NO_PROOF_OF_THEFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ANIMALS]: STATIC_EXCLUSIONS.ANIMALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: STATIC_EXCLUSIONS.ELECTRONIC_DATA,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL]: STATIC_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.GUESTS_AND_DOMESTIC_EMPLOYEES_CONTENTS]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£10,000',
                [CONSTANTS.SUBHEADING]: '<p>We will pay for any loss of or damage to contents in your home belonging to guests or domestic employees permanently residing with you up to the sum insured shown in your schedule provided such contents are not otherwise insured.</p><p>The maximum amount we will pay for any one article is £500.</p>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ANIMALS]: STATIC_EXCLUSIONS.ANIMALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: STATIC_EXCLUSIONS.ELECTRONIC_DATA,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL]: STATIC_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            }
        },
        [CONSTANTS.INNER_LIMITS_TITLE]: {
            [CONSTANTS.INNER_LIMITS.GARDEN_REINSTATEMENT]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£25,000 but not more than £2,500 per plant',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured in your schedule for the reasonable and necessary cost you incur to restore your garden at your home if it is damaged during the policy period. We will only pay for losses caused by fire, lightning, earthquake, explosion or aircraft, theft, attempted theft, vandalism, riots, civil commotion or malicious acts. If rubbish and waste material has been deposited on your land at the address shown in your schedule during the policy period without your permission, we will also pay the reasonable and necessary cost of its removal.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FALLEN_TREES]: STATIC_EXCLUSIONS.FALLEN_TREES
                }
            },
            [CONSTANTS.INNER_LIMITS.LOCK_REPLACEMENT]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: 'Unlimited',
                [CONSTANTS.SUBHEADING]: 'We will pay for the cost of replacing the locks in your home if the keys to that home are lost or stolen. Your excess does not apply to this cover.'
            },
            [CONSTANTS.INNER_LIMITS.TEMPORARY_LETTING]: {
                [CONSTANTS.SUBHEADING]: 'We will pay for a covered loss whilst your home is being temporarily let for no more than 60 days in total per policy period subject to the following exclusions:<ol type="a"><li>We do not cover loss or damage caused by accidental damage; accidental damage is damage that occurs suddenly as a result of an unexpected and non-deliberate action.</li><li>We will not pay for any loss or damage caused directly or indirectly by theft or attempted theft, unless there is physical evidence of forced entry or exit from the home resulting from such theft or attempted theft.</li><li>We will not pay for any loss or damage caused directly or indirectly by commercial activity, including but not limited to filming or any other business activity.</li></ol>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ANIMALS]: STATIC_EXCLUSIONS.ANIMALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: STATIC_EXCLUSIONS.ELECTRONIC_DATA,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL]: STATIC_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.REMOVAL_OF_NESTS]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£1,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured in your schedule in total for the removal of wasp, bee, mouse, rat or cockroach nests from your main dwelling.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.NEW_FIXTURES]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£15,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for loss of or damage to new fixtures, fittings, construction materials and supplies owned by you or for which you are responsible, all kept within the boundaries of your home, whilst awaiting installation or construction.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LEFT_EXPOSED]: STATIC_EXCLUSIONS.LEFT_EXPOSED,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DURING_INSTALLATION]: STATIC_EXCLUSIONS.DURING_INSTALLATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.MONEY]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£2,500',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for money to insure you against physical loss or physical damage which happens anywhere in the world during the policy period.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.THEFT]: STATIC_EXCLUSIONS.THEFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEPRECIATION]: STATIC_EXCLUSIONS.DEPRECIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.STORAGE]: STATIC_EXCLUSIONS.STORAGE
                }
            },
            [CONSTANTS.INNER_LIMITS.QUAD_BIKES]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]:'£10,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for loss or damage to quad bikes, toy vehicles and motorcycles of under 51cc used within the grounds of your home.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSAFE_STORAGE]: STATIC_EXCLUSIONS.UNSAFE_STORAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.FOOD_SPOILAGE]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: 'Unlimited',
                [CONSTANTS.SUBHEADING]: 'We will pay for loss of food caused by spoilage due to a temperature change in a refrigerator or freezer caused by an interruption of the power supply, or due to the mechanical breakdown of refrigeration equipment at any home you live at or own.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.WINE]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£10,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for loss or damage to wine.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.NATURAL_CAUSES]: STATIC_EXCLUSIONS.NATURAL_CAUSES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.GARDEN_FURNITURE]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£10,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for loss or damage to garden furniture and outdoor items.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_WEATHER]: STATIC_EXCLUSIONS.EXTREME_WEATHER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.DEEDS_BONDS_SECURITIES]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£5,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for loss or damage to deeds, bonds, securities or other similar private documents.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: STATIC_EXCLUSIONS.ELECTRONIC_DATA,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.GARDENING_EQUIPMENT]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£20,000',
                [CONSTANTS.SUBHEADING]: 'We will pay the market value, up to the sum insured shown in your schedule for loss or damage to domestic gardening and landscaping equipment.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSAFE_STORAGE]: STATIC_EXCLUSIONS.UNSAFE_STORAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.GUNS]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£5,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for loss or damage to guns.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.UNATTENDED_VEHICLE_THEFT]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£5,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for theft or attempted theft from an unattended motor vehicle.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: STATIC_EXCLUSIONS.LAWN_MOWER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: STATIC_EXCLUSIONS.AIRCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: STATIC_EXCLUSIONS.TENANTS_CONTENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.DOMESTIC_LOSS]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£15,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for the cost of loss of domestic oil, gas, liquefied petroleum gas or metered water that has escaped from your heating or water system at a home listed in your schedule which occurs during the policy period.<br /><br />We also cover the cost of clearing up contamination or pollution of land and/or water at your home caused by any sudden, unforeseen and identifiable oil leakage from any fixed domestic oil installation at your home up to the sum insured shown in your schedule per policy period.',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: STATIC_EXCLUSIONS.WEAR_AND_TEAR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: STATIC_EXCLUSIONS.WIND_AND_STORM,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: STATIC_EXCLUSIONS.EROSION,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: STATIC_EXCLUSIONS.FROST,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: STATIC_EXCLUSIONS.RODENTS,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: STATIC_EXCLUSIONS.PET_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
                }
            },
            [CONSTANTS.INNER_LIMITS.CONTENTS_IN_STORAGE]: {
                [CONSTANTS.LIMITS_TO_SUM_INSURED]: '£20,000',
                [CONSTANTS.SUBHEADING]: 'We will pay up to the sum insured shown in your schedule for contents in storage up to 60 consecutive days (unless agreed in writing with us).',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.JEWELLERY_WATCHES_MONEY]: STATIC_EXCLUSIONS.JEWELLERY_WATCHES_MONEY
                }
            }
        },
        [CONSTANTS.ALL_PROPERTY_EXCLUSIONS]: {
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WEAR_AND_TEAR]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.WEAR_AND_TEAR
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLEANING_AND_REPAIRS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.CLEANING_AND_REPAIRS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.WIND_AND_STORM]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.WIND_AND_STORM
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ESCAPE_OF_WATER]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.ESCAPE_OF_WATER
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE_OUTDOOR]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.SUBSIDENCE_OUTDOOR
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.INFESTATION_REMOVAL]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.INFESTATION_REMOVAL
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_PLANNING]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.DEFECTIVE_PLANNING
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MATERIALS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.DEFECTIVE_MATERIALS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.DEFECTIVE_MAINTENANCE]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.DEFECTIVE_MAINTENANCE
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EROSION]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.EROSION
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNRECEIVED_GOODS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.UNRECEIVED_GOODS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNSECURE_TRANSIT]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.UNSECURE_TRANSIT
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.UNATTENDED_VEHICLE]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.UNATTENDED_VEHICLE
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.LAWN_MOWER]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.LAWN_MOWER
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.BUSINESS_USE_BUILDINGS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.BUSINESS_USE_BUILDINGS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MOTORISED_LAND_VEHICLES]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.VEHICLE_REGISTRATION]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.VEHICLE_REGISTRATION
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.AIRCRAFT]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.AIRCRAFT
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SPORTS_EQUIPMENT]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.SPORTS_EQUIPMENT
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ANIMALS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.ANIMALS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.CONTENTS_FOR_BUSINESS_USE
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.ELECTRONIC_DATA]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.ELECTRONIC_DATA
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FROST]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.FROST
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.RODENTS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.RODENTS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.PERSONAL_DATA_RETRIEVAL
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.SUBSIDENCE]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.BULGING_EXPANSION_SHRINKING
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.EXTREME_TEMPERATURE]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.EXTREME_TEMPERATURE
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.MONEY_STAMPS_COINS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.MONEY_STAMPS_COINS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.FAMILIAL_MISAPPROPRIATION
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.PET_DAMAGE]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.PET_DAMAGE
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.TENANTS_CONTENTS]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.TENANTS_CONTENTS
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.COVERED_UNDER_PART_VI]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI
            },
            [CONSTANTS.EXCLUSION_NAMES.PROPERTY_EXCLUSIONS.CLAIM_PREPARATION_FEES]: {
                [CONSTANTS.SUBHEADING]: STATIC_EXCLUSIONS.CLAIM_PREPARATION_FEES
            }
        }
    }
};

export default STATIC_DATA_2;