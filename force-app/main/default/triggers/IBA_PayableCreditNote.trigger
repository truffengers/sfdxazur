/**
* @name: IBA_PayableCreditNote
* @description: Trigger for the managed object c2g__codaPurchaseCreditNote__c
*
*/

trigger IBA_PayableCreditNote on c2g__codaPurchaseCreditNote__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_PayableCreditNoteTriggerHandler());
}