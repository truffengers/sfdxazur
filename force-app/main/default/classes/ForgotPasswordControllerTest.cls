/**
* @name: ForgotPasswordControllerTest
* @description: An apex page controller that exposes the site forgot password functionality
* @author: Azur Group
* @date: 2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
*/
@IsTest
public with sharing class ForgotPasswordControllerTest {
  	 @IsTest
     public static void testForgotPasswordController() {
    	// Instantiate a new controller with all parameters in the page
    	ForgotPasswordController controller = new ForgotPasswordController();
    	controller.username = 'test@salesforce.com';     	
    
    	System.assertEquals(controller.forgotPassword(),null); 
    }
}