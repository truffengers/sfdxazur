global class BrightTalkWebcastPreviewScheduled implements Schedulable {

    private static String jobName = 'BrightTALK Webcast Preview Links';

    global void execute(SchedulableContext sc) {
        executeFuture();
    }

    @future(callout=true)
    public static void executeFuture() {
        BrightTalkApi api = new BrightTalkApi();

        List<BrightTALK__Webcast__c> feedWebcasts = api.getWebcastsFromFeed();
        Set<Decimal> webcastIds = new Set<Decimal>();

        for(BrightTALK__Webcast__c wc : feedWebcasts) {
            webcastIds.add(wc.BrightTALK__Webcast_Id__c);
        }

        Nebula_Tools.sObjectIndex existingWebcastsById = new Nebula_Tools.sObjectIndex(
              'BrightTALK__Webcast_Id__c',
              [SELECT Id, BrightTALK__Webcast_Id__c, Preview_Image__c,
                    Preview_Thumbnail__c, Duration_s__c, Status__c
                FROM BrightTALK__Webcast__c
                WHERE BrightTALK__Webcast_Id__c IN :webcastIds]);

        Map<Id, BrightTALK__Webcast__c> toUpdate = new Map<Id, BrightTALK__Webcast__c>();

        for(BrightTALK__Webcast__c feedWebcast : feedWebcasts) {
            BrightTALK__Webcast__c existingWebcast = (BrightTALK__Webcast__c)existingWebcastsById.get(feedWebcast.BrightTALK__Webcast_Id__c);

            if(existingWebcast != null && (existingWebcast.Preview_Thumbnail__c != feedWebcast.Preview_Thumbnail__c 
                                           || existingWebcast.Preview_Image__c != feedWebcast.Preview_Image__c
                                           || existingWebcast.Duration_s__c != feedWebcast.Duration_s__c 
                                           || existingWebcast.Status__c != feedWebcast.Status__c)) {
                existingWebcast.Preview_Image__c = feedWebcast.Preview_Image__c;
                existingWebcast.Preview_Thumbnail__c = feedWebcast.Preview_Thumbnail__c;
                existingWebcast.Duration_s__c = feedWebcast.Duration_s__c;
                                               existingWebcast.Status__c = feedWebcast.Status__c;
                toUpdate.put(existingWebcast.Id, existingWebcast);
            }
        }

        if(!toUpdate.isEmpty()) {
            update toUpdate.values();
        }
    }

    public static void cancelSelf(String namePrefix) {
        List<CronTrigger> existingToCancel = [SELECT Id, CronJobDetail.Name, State 
                                              FROM CronTrigger 
                                              WHERE CronJobDetail.Name LIKE :namePrefix + jobName + '%'
                                              AND CronJobDetail.JobType = '7' 
                                              AND State != 'DELETED'];

        for(CronTrigger t : existingToCancel) {
            System.abortJob(t.Id);
        }
        
        
    }  

    public static void scheduleSelf(String namePrefix) {
        cancelSelf(namePrefix);
        
        BrightTalkWebcastPreviewScheduled btp = new BrightTalkWebcastPreviewScheduled();
        
        System.schedule(namePrefix + jobName, '0 15 * * * ?', btp);
    }        

}