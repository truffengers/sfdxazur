({
	doInit : function(component, event, helper) {
		var topicId = component.get('v.topicId');
		var topicName = component.get('v.topicName');

		if(!($A.util.isEmpty(topicId) && $A.util.isEmpty(topicName))) {
            var getTopicAction = component.get('c.getTopic');

            getTopicAction.setParams({
                topicId: topicId,
                topicName: topicName
            });

            getTopicAction.setStorable();

			getTopicAction.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	            	var topicObject = response.getReturnValue();
	                component.set('v.topicObject', topicObject);
	                if(!topicName) {
	                	component.set('v.topicName', topicObject.Name);
	                }
	            }
	        });
	        
	        $A.enqueueAction(getTopicAction);
    	}
	}
})