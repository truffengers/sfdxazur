({
	submit : function(component, event, helper) {
		event.preventDefault();
		var searchString = component.find('searchInput').getElement().value;
		if(!$A.util.isEmpty(searchString)) {
			var navEvt = $A.get('e.force:navigateToURL');
		    navEvt.setParam('url', '/global-search/' + searchString);
		    navEvt.fire();
		    if(TT.menu && TT.menu.getIsOpen()) {
	            TT.menu.openOrClose();
	        }
    	}
	},
	selectSearch : function(component, event, helper) {
		event.target.select();
	}
})