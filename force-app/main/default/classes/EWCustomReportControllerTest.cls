@isTest
private class EWCustomReportControllerTest {

    @testSetup
    private static void setup() {
        Id personAccount = Account.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(EWConstants.ACCOUNT_PERSON_RECORD_TYPE_DEVNAME).getRecordTypeId();
        String addressId = '123abc';

        Product2 productHomeEmergency = EWProductDataFactory.createProduct('Home Emergency', false);
        productHomeEmergency.ProductCode = EWConstants.EW_PROD_CODE_HOME_EMERGANCY;
        Product2 productLegalExpenses = EWProductDataFactory.createProduct('Legal Expenses', false);
        productLegalExpenses.ProductCode = EWConstants.EW_PROD_CODE_LEGAL_EXPENSES;
        Product2 productPersonalCyber = EWProductDataFactory.createProduct('Personal Cyber', false);
        productPersonalCyber.ProductCode = EWConstants.EW_PROD_CODE_PERSONAL_CYBER;

        insert new List<Product2>{
                productHomeEmergency, productLegalExpenses, productPersonalCyber
        };

        Account person = new Account(RecordTypeId = personAccount, LastName = 'Kowalski');
        insert person;

        Account acc = new Account(Name = 'AIG HSB 1 ARAG');
        insert acc;

        Asset asset = new Asset(
                Name = 'testAsset',
                Status = EWQuoteHelper.QUOTE_STATUS_ISSUED,
                vlocity_ins__ExpirationDate__c = Date.today(),
                EW_TypeofTransaction__c = 'New Business',
                AccountId = person.Id);
        insert asset;

        vlocity_ins__AssetCoverage__c coverage1 = new vlocity_ins__AssetCoverage__c();
        coverage1.Name = 'LegalExpenses';
        coverage1.vlocity_ins__PolicyAssetId__c = asset.Id;
        coverage1.vlocity_ins__AttributeSelectedValues__c = '{"personalCyberCoverageLimit":50000}';
        coverage1.vlocity_ins__Product2Id__c = productLegalExpenses.Id;
        coverage1.CurrencyIsoCode = 'GBP';

        vlocity_ins__AssetCoverage__c coverage2 = new vlocity_ins__AssetCoverage__c();
        coverage2.Name = 'PersonalCyber';
        coverage2.vlocity_ins__PolicyAssetId__c = asset.Id;
        coverage2.vlocity_ins__AttributeSelectedValues__c = '{"personalCyberCoverageLimit":50000}';
        coverage2.vlocity_ins__Product2Id__c = productPersonalCyber.Id;
        coverage2.CurrencyIsoCode = 'GBP';

        vlocity_ins__AssetCoverage__c coverage3 = new vlocity_ins__AssetCoverage__c();
        coverage3.Name = 'HomeEmergency';
        coverage3.vlocity_ins__PolicyAssetId__c = asset.Id;
        coverage3.vlocity_ins__AttributeSelectedValues__c = '{"personalCyberCoverageLimit":50000}';
        coverage3.vlocity_ins__Product2Id__c = productHomeEmergency.Id;
        coverage3.CurrencyIsoCode = 'GBP';

        insert new List<vlocity_ins__AssetCoverage__c>{
                coverage1, coverage2, coverage3
        };

        vlocity_ins__AssetInsuredItem__c insuredItem = new vlocity_ins__AssetInsuredItem__c(
                Name = EWQuoteHelper.MAIN_RESIDENCE,
                vlocity_ins__PolicyAssetId__c = asset.id,
                EW_AddressId__c = addressId);
        insert insuredItem;
        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(true, 2, asset.Id);
        IBA_FFPolicyCarrierLine__c policyCarrierLine = IBA_TestDataFactory.createPolicyCarrierLine(true, policyTransactions[0].Id, acc.Id);

        List<IBA_PolicyTransactionLine__c> policyTransactionLines = new List<IBA_PolicyTransactionLine__c>();

        for (IBA_PolicyTransaction__c pt : policyTransactions) {
            IBA_PolicyTransactionLine__c productHomeEmergencyPTL = IBA_TestDataFactory.createPolicyTransactionLine(false, pt.Id, policyCarrierLine.Id);
            IBA_PolicyTransactionLine__c productLegalExpensesPTL = IBA_TestDataFactory.createPolicyTransactionLine(false, pt.Id, policyCarrierLine.Id);
            IBA_PolicyTransactionLine__c productPersonalCyberPTL = IBA_TestDataFactory.createPolicyTransactionLine(false, pt.Id, policyCarrierLine.Id);

            productHomeEmergencyPTL.IBA_Product__c = productHomeEmergency.Id;
            productLegalExpensesPTL.IBA_Product__c = productLegalExpenses.Id;
            productPersonalCyberPTL.IBA_Product__c = productPersonalCyber.Id;

            policyTransactionLines.add(productHomeEmergencyPTL);
            policyTransactionLines.add(productLegalExpensesPTL);
            policyTransactionLines.add(productPersonalCyberPTL);
        }

        insert policyTransactionLines;
    }

    @isTest
    private static void shouldReturnEmptyTable_whenActiveReportIsNull() {
        String resultJSON = EWCustomReportController.getReportData(null, null, null, null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        System.assertEquals(true, result.containsKey('headers'), 'Expected 1 header row');
        System.assertEquals(true, result.containsKey('rows'), 'Expected 1 data row');
    }

    @isTest
    private static void shouldReturnAIGReport_whenTableNotEmpty() {
        String reportName = 'AIG Report';
        String resultJSON = EWCustomReportController.getReportData(reportName, null, null, null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        system.assertEquals(true, result.containsKey('headers'), 'Expected header row');
        system.assertEquals(true, result.containsKey('rows'), 'Expected rows');

        List<Object> headers = (List<Object>) result.get('headers');
        System.assert(headers.size() > 0);

        List<Object> rows = (List<Object>) result.get('rows');
         System.assertEquals(2, rows.size());
    }

    @isTest
    private static void shouldReturnCyberReport_whenTableNotEmpty() {
        String reportName = 'Cyber Report';
        String resultJSON = EWCustomReportController.getReportData(reportName, null, null, null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        system.assertEquals(true, result.containsKey('headers'), 'Expected header row');
        system.assertEquals(true, result.containsKey('rows'), 'Expected rows');

        List<Object> headers = (List<Object>) result.get('headers');
        System.assert(headers.size() > 0);

        List<Object> rows = (List<Object>) result.get('rows');
        System.assertEquals(1, rows.size());
    }

    @isTest
    private static void shouldReturnCyberReport_whenDatesSelected() {
        String reportName = 'Cyber Report';
        String resultJSON = EWCustomReportController.getReportData(reportName, String.valueOf(Date.today() - 1), String.valueOf(Date.today() + 2), null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        system.assertEquals(true, result.containsKey('headers'), 'Expected header row');
        system.assertEquals(true, result.containsKey('rows'), 'Expected rows');

        List<Object> headers = (List<Object>) result.get('headers');
        System.assert(headers.size() > 0);

        List<Object> rows = (List<Object>) result.get('rows');
        System.assertEquals(1, rows.size());
    }

    @isTest
    private static void shouldNotReturnRecordsCyberReport_whenDatesNotMatch() {
        String reportName = 'Cyber Report';
        String resultJSON = EWCustomReportController.getReportData(reportName, String.valueOf(Date.today() - 2), String.valueOf(Date.today() - 1), null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        system.assertEquals(true, result.containsKey('headers'), 'Expected header row');
        system.assertEquals(true, result.containsKey('rows'), 'Expected rows');

        List<Object> headers = (List<Object>) result.get('headers');
        System.assert(headers.size() > 0);

        List<Object> rows = (List<Object>) result.get('rows');
        System.assertEquals(0, rows.size());
    }

    @isTest
    private static void shouldReturnLegalExpenses_whenTableNotEmpty() {
        String reportName = 'Legal Expenses Report';
        String resultJSON = EWCustomReportController.getReportData(reportName, null, null, null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        system.assertEquals(true, result.containsKey('headers'), 'Expected header row');
        system.assertEquals(true, result.containsKey('rows'), 'Expected rows');

        List<Object> headers = (List<Object>) result.get('headers');
        System.assert(headers.size() > 0);

        List<Object> rows = (List<Object>) result.get('rows');
        System.assertEquals(1, rows.size());
    }

    @isTest
    private static void shouldReturnHomeEmergency_whenTableNotEmpty() {
        String reportName = 'Home Emergency Report';
        String resultJSON = EWCustomReportController.getReportData(reportName, null, null, null, null, false);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);

        system.assertEquals(true, result.containsKey('headers'), 'Expected header row');
        system.assertEquals(true, result.containsKey('rows'), 'Expected rows');

        List<Object> headers = (List<Object>) result.get('headers');
        System.assert(headers.size() > 0);

        List<Object> rows = (List<Object>) result.get('rows');
        System.assertEquals(1, rows.size());
    }
}