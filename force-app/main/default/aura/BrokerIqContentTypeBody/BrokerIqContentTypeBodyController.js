({
	doInit : function(component, event, helper) {
		var getSearchFilterWrapperAction = component.get('c.getSearchFilterWrapper');
		
		getSearchFilterWrapperAction.setStorable();
		getSearchFilterWrapperAction.setParam('metadataTypeId', component.get('v.recordId'));

		getSearchFilterWrapperAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.selectedContentType', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(getSearchFilterWrapperAction);				
	}
})