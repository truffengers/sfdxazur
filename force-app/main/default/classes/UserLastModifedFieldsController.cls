global class UserLastModifedFieldsController {
    
    global Id userIdParam {get; set;}
    
    global List<User_History__c> getModifiedFields() {
        List<User_History__c> rval = new List<User_History__c>();
        
        if(userIdParam != null) {
            User u = [SELECT Id, LastModifiedDate FROM User WHERE Id = :userIdParam];
            List<User_History__c> histories = [
                SELECT Field_Developer_Name__c, Field_Label__c, 
                New_Value__c, Old_Value__c, CreatedDate
                FROM User_History__c
                WHERE User__c = :u.Id 
                AND CreatedById = :u.Id
                ORDER BY CreatedDate DESC
                LIMIT :Schema.SObjectType.User.fieldSets.History_Tracking.getFields().size()];
            
            DateTime firstCreatedDate;
            for(User_History__c thisHistory : histories) {
                if(firstCreatedDate == null) {
                    firstCreatedDate = thisHistory.CreatedDate;
                }
                if(thisHistory.CreatedDate == firstCreatedDate) {
                    if(thisHistory.Field_Label__c == 'Cell') {
                        thisHistory.Field_Label__c = 'Mobile';
                    }
                    rval.add(thisHistory);
                }
            }
        }
        return rval;
    }
}