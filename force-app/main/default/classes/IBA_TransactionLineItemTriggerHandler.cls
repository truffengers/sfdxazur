/**
* @name: IBA_TransactionLineItemTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaTransactionLineItem__c trigger functionality
*
*/

public with sharing class IBA_TransactionLineItemTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_TransactionLineItemTriggerHandler.IsDisabled != null ? IBA_TransactionLineItemTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        // this method returns related invoices
        List<c2g__codaPurchaseInvoice__c> invoices = IBA_InvoiceBulkReleaseForPayment.getInvoices(newItems.keySet());
        // this job changes the status of the related invoices to Release For Payment in asynch way to avoid financial force API soql limits
        if(invoices.size() > 0) {
            System.enqueueJob(new IBA_BulkReleaseForPaymentJob(invoices));
        }
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}