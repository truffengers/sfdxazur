/**
* @name: IBA_UtilConst
* @description: Utility class for IBA project
*
*/
public with sharing class IBA_UtilConst {
    public static final Id DUMMY_ACCOUNT_ID = IBA_IBACustomSettings__c.getOrgDefaults().IBA_DummyAccount__c;
    public static final String ACCOUNT_RT_BROKER = 'AgencyBrokerage';
    public static final String ACCOUNT_RT_CARRIER = 'Carrier';
    public static final String ACCOUNT_RT_BUSINESS = 'Business_Account';

    public static final String CONTACT_RT_CONTACT = 'Contact';

    public static final String PRODUCT_RT_Master = 'Product_Master';
    public static final String PRODUCT_NAME_CLASSIC_CAR = 'PCG Azur Classic Car';
    public static final String PRODUCT_NAME_LOCAL_AUTHORITY = 'PCG Local Authorities';

    public static final String STAGING_STATUS_READY_TO_PROCESS = 'Ready to Process';
    public static final String STAGING_STATUS_MISSING_DATA = 'Missing Data';

    public static final String AIG_POLICY_SPECIAL_NUMBER1 = '1500';
    public static final String AIG_POLICY_SPECIAL_NUMBER2 = '1501';

    public static final String POLICY_RT_HIGH_NET_WORTH = 'High_Net_Worth';
    public static final String POLICY_STATUS_ACTIVE = 'Active';
    public static final String POLICY_STATUS_INACTIVE = 'Non Active';

    public static final String POLICY_COVERAGE_STATUS_ACTIVE = 'Active';
    public static final String POLICY_COVERAGE_STATUS_INACTIVE = 'Non Active';

    public static final String POLICY_TRANSACTION_TYPE_ENDORSEMENT = 'ENDORSEMENT';
    public static final String POLICY_TRANSACTION_BUSINESS_TYPE_RENEWAL = 'Renewal';

    public static final String LOG_PROJECT_NAME = 'EW';
    public static final String LOG_SUBPROJECT_NAME = 'IBA';

    public static final String PAYABLE_INVOICE_HOLD_STATUS = 'On Hold';
    public static final String PAYABLE_INVOICE_STATUS_COMPLETE = 'Complete';

    public static final String COMPANY_RT_VAT = 'VAT';

    public static final String TRANSACTION_LINE_ITEM_MATCHING_STATUS_MATCHED = 'Matched';
    public static final String TRANSACTION_LINE_ITEM_TYPE_ACCOUNT = 'Account';

    public static final String TRANSACTION_TYPE_INVOICE = 'Invoice';
    public static final String TRANSACTION_TYPE_PURCHASE_INVOICE = 'Purchase Invoice';
    public static final String TRANSACTION_TYPE_CASH = 'Cash';
    public static final String TRANSACTION_TYPE_CREDIT_NOTE = 'Credit Note';
    public static final String TRANSACTION_TYPE_PURCHASE_CREDIT_NOTE = 'Purchase Credit Note';
    public static final String TRANSACTION_TYPE_JOURNAL = 'Journal';

    public static final String CASH_MATCHING_HISTORY_ACTION_UNDO_MATCH = 'Undo Match';

    public static final String GENERAL_LEDGER_ACCOUNT_TYPE_BALANCE = 'Balance Sheet';
    public static final String GENERAL_LEDGER_ACCOUNT_BALANCE1_INCOME = 'Income Statement';
    public static final String GENERAL_LEDGER_ACCOUNT_BALANCE2_GROSS = 'Gross Profit';
    public static final String GENERAL_LEDGER_ACCOUNT_BALANCE3_ACCOUNTS = 'Accounts Receivable';

    public static final String YEAR_PERIOD_CALCULATION_MONTH_END = 'Month End';
    
    public static final String SALES_INVOICE_STATUS_COMPLETE = 'Complete';
    public static final String JOURNAL_STATUS_COMPLETE = 'Complete';
    public static final String SALES_CREDIT_NOTE_STATUS_COMPLETE = 'Complete';
    public static final String PAYABLE_CREDIT_NOTE_STATUS_COMPLETE = 'Complete';

    public static final String JOURNAL_LINE_TYPE_GLA = 'General Ledger Account';
    public static final String JOURNAL_LINE_TYPE_AC = 'Account - Customer';
}