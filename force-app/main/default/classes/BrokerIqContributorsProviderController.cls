public with sharing class BrokerIqContributorsProviderController {

	@AuraEnabled
	public static List<Broker_iQ_Contributor__c> getContributors() {
		String q = new Nebula_Tools.FieldSetUtils.QueryBuilder('Broker_iQ_Contributor__c', 'Active__c = true AND Featured_Contributor__c = true')
				.setOrderBy('Featured_Contributor_Order__c ASC NULLS LAST, Full_Name__c ASC')
				.build();

		return Database.query(q);
	}

	@AuraEnabled
	public static Broker_iQ_Contributor__c getContributor(String recordId) {
		String q = new Nebula_Tools.FieldSetUtils.QueryBuilder('Broker_iQ_Contributor__c', 'Id = :recordId')
				.build();

		List<Broker_iQ_Contributor__c> rval = Database.query(q);

		if(rval.isEmpty()) {
			return null;
		} else {
			return rval[0];
		}
	}
}