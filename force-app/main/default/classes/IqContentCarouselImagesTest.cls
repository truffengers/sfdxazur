@isTest
private class IqContentCarouselImagesTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();
	private static IQ_Content__c iqc;
	private static ContentVersion cv;

	static {
		iqc = tog.getWebcastIqContent();
		cv = tog.getContentVersion('foo.txt', Blob.valueOf('foo'));
	}

	@isTest static void basic() {

		iqc.Carousel_Image_1920__c = cv.ContentDocumentId;

		update iqc;
		iqc = [SELECT Id, Carousel_Image_1920__c, Carousel_Image_1920_URL__c 
				FROM IQ_Content__c
				WHERE Id = :iqc.Id];

		List<ContentDistribution> distribution = [SELECT Id 
													FROM ContentDistribution 
													WHERE ContentDocumentId = :iqc.Carousel_Image_1920__c];

		System.assertEquals(1, distribution.size());
		System.assertNotEquals(null, iqc.Carousel_Image_1920_URL__c);
	}
	@isTest static void stableOverUpdate() {

		iqc.Carousel_Image_1920__c = cv.ContentDocumentId;

		update iqc;
		iqc.Carousel_Image_1440__c = cv.ContentDocumentId;
		update iqc;
		iqc = [SELECT Id, Carousel_Image_1920__c, Carousel_Image_1920_URL__c 
				FROM IQ_Content__c
				WHERE Id = :iqc.Id];

		List<ContentDistribution> distribution = [SELECT Id 
													FROM ContentDistribution 
													WHERE ContentDocumentId = :iqc.Carousel_Image_1920__c];

		System.assertEquals(1, distribution.size());
		System.assertNotEquals(null, iqc.Carousel_Image_1920_URL__c);
	}
	@isTest static void twice() {
		basic();

		iqc.Carousel_Image_1920__c = null;

		update iqc;

		iqc = [SELECT Id, Carousel_Image_1920__c, Carousel_Image_1920_URL__c 
				FROM IQ_Content__c
				WHERE Id = :iqc.Id];
		
		System.assertEquals(null, iqc.Carousel_Image_1920_URL__c);

		iqc.Carousel_Image_1920__c = cv.ContentDocumentId;

		update iqc;

		List<ContentDistribution> distribution = [SELECT Id 
													FROM ContentDistribution 
													WHERE ContentDocumentId = :iqc.Carousel_Image_1920__c];

		System.assertEquals(1, distribution.size());
	}
	@isTest static void notAnId() {

		iqc.Carousel_Image_1920__c = 'foo';

		try {
			update iqc;
		} catch(Exception e) {
			System.assert(e.getMessage().contains(String.format(
                            Label.BrokerIqCarouselIdError,
                            new List<String> {
                                            IQ_Content__c.Carousel_Image_1920__c.getDescribe().getLabel(),
                                            iqc.Carousel_Image_1920__c 
                            } )),
				e.getMessage());
			return;
		}

		System.assert(false, 'Should have thrown exception');
	}
	@isTest static void wrongTypeOfId() {

		iqc.Carousel_Image_1920__c = iqc.Id;

		try {
			update iqc;
		} catch(Exception e) {
			System.assert(e.getMessage().contains(String.format(
                            Label.BrokerIqCarouselIdWrongType,
                            new List<String> {
                                            IQ_Content__c.Carousel_Image_1920__c.getDescribe().getLabel(),
                                            ContentDocument.sObjectType.getDescribe().getKeyPrefix() 
                            } )),
				e.getMessage());
			return;
		}

		System.assert(false, 'Should have thrown exception');
	}
}