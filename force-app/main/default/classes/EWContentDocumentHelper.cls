public with sharing class EWContentDocumentHelper {

    private static final String QUOTE_OBJECT_PREFIX = Quote.getSObjectType().getDescribe().getKeyPrefix();
    private static final String POLICY_OBJECT_PREFIX = vlocity_ins__Policy__c.getSObjectType().getDescribe().getKeyPrefix();
    private static final String CLAIM_OBJECT_PREFIX = vlocity_ins__InsuranceClaim__c.getSObjectType().getDescribe().getKeyPrefix();
    private static final String NOTE_DELETE_ERROR = 'You are note permitted to delete this note!';

    @TestVisible // to avoid complexity - as I cannot do DMLs on custom metadata types
    private static Boolean skipCheckForTest = false;

    public void handleOnBeforeDelete(Map<Id, SObject> oldItems) {
        if (!isUserAllowedToDeleteNotes() || skipCheckForTest) {
            Set<Id> cdIds = oldItems.keySet();

            List<ContentDocumentLink> cdLinks = new List<ContentDocumentLink>([
                    SELECT Id, ContentDocumentId, LinkedEntityId, IsDeleted
                    FROM ContentDocumentLink
                    WHERE ContentDocumentId IN :cdIds
            ]);

            for (ContentDocumentLink cdl : cdLinks) {
                String objKey = String.valueOf(cdl.LinkedEntityId).substring(0, 3);
                if (objKey.equals(QUOTE_OBJECT_PREFIX) || objKey.equals(POLICY_OBJECT_PREFIX) || objKey.equals(CLAIM_OBJECT_PREFIX)) {
                    oldItems.get(cdl.ContentDocumentId).addError(NOTE_DELETE_ERROR);
                }
            }
        }
    }

    private Boolean isUserAllowedToDeleteNotes() {
        Profile p = [
                SELECT Id, Name
                FROM Profile
                WHERE Id = :UserInfo.getProfileId()
        ];

        List<EW_Notes_Setting__mdt> settings = new List<EW_Notes_Setting__mdt>([
                SELECT Id
                FROM EW_Notes_Setting__mdt
                WHERE EW_PreventProfileFromDeletingNotes__c = :p.Name
                LIMIT 1
        ]);

        return settings.isEmpty();
    }

    public void handleOnAfterUpsert(Map<Id, SObject> newItems) {
        if (newItems != null) {
            updateContentDocumentLinksVisibilityForQuotes(newItems.keySet());
        }
    }

    /**
    * @methodName: updateContentDocumentLinksVisibilityForQuotes
    * @description: preexisting @future method updated to handle calls when already running async processes
    * @author: Roy Lloyd
    * @date: 31/3/2020
    */
    public static void updateContentDocumentLinksVisibilityForQuotes(Set<Id> contentDocumentIds) {

        Boolean runningAsync = System.isQueueable() || System.isBatch() || System.isFuture() || System.isScheduled();
        if(runningAsync){
            updateContentDocumentLinksVisibilityForQuotesNow(contentDocumentIds);
        }else{
            updateContentDocumentLinksVisibilityForQuotesAtFuture(contentDocumentIds);
        }
    }

    @TestVisible
    @Future
    public static void updateContentDocumentLinksVisibilityForQuotesAtFuture(Set<Id> contentDocumentIds) {
        updateContentDocumentLinksVisibilityForQuotesNow(contentDocumentIds);
    }

    public static void updateContentDocumentLinksVisibilityForQuotesNow(Set<Id> contentDocumentIds) {
        List<ContentDocumentLink> contentDocumentLinksToUpdate = new List<ContentDocumentLink>();
        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>([
                SELECT Id, Visibility, ShareType, LinkedEntityId
                FROM ContentDocumentLink
                WHERE ContentDocumentId IN :contentDocumentIds
        ]);

        for (ContentDocumentLink cdl : contentDocumentLinks) {
            if (String.valueOf(cdl.LinkedEntityId).startsWith('0Q0')) {
                cdl.ShareType = 'V';
                cdl.Visibility = 'AllUsers';
                contentDocumentLinksToUpdate.add(cdl);
            }
        }
        update contentDocumentLinksToUpdate;
    }

}