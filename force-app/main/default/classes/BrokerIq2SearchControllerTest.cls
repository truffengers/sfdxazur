/**
* @name: BrokerIq2SearchControllerTest
* @description: BrokerIq2SearchController test class
* @author: aidan@nebulaconsulting.co.uk
* @date: 14/10/2017
* @Modified: Ignacio Sarmiento  30/11/2017  Add testIsCIIUserWithCIIUser' and 'testIsCIIUserWithNoCIIUser' methods and CII logic
* @Modified: Ignacio Sarmiento	04/12/2017	Extend functionality isLoggedUserCIIUserAndNotGuestBrokerIq2User() to search in groups inside current CII group
* @Modified: Ignacio Sarmiento	11/12/2017	Extend functionality to biq2 guest user and remove custom labels from class properties
*/
@isTest
private class BrokerIq2SearchControllerTest 
{
    //Profile constants
    private static final String PROFILE_NAME_PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS = BrokerIqTestDataFactory.PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS;
    //User contants
    private static final String USER_SYSTEMADMIN_LASTNAME = 'Testing last name system admin';
    private static final String USER_CII_LASTNAME = 'Testing last name CII';
    private static final String USER_CII_INSIDE_A_GROUP_LASTNAME = 'Testing last name CII inside a group';
    private static final String USER_NO_CII_LASTNAME = 'Testing last name no CII';
    private static final String BROKER_IQ2_GUEST_USER_INSIDE_CII_GROUP_LASTNAME = 'Testing biq2 guest user inside CII group';
    private static final String BROKER_IQ2_GUEST_USER_NOT_INSIDE_CII_GROUP_LASTNAME = 'Testing biq2 guest user not inside CII group';
    //Other constants
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static IQ_Content__c iqc;
    private static String keyword = 'octopus';

    @testSetup
    static void setup() {
        //Insert system administrator
        User systemAdminUser = BrokerIqTestDataFactory.createSystemAdmistratorUser(USER_SYSTEMADMIN_LASTNAME);
        insert systemAdminUser;

        System.runAs(systemAdminUser) {
            //Insert users
            list<User> usersToInsert = new list<User>();
            Profile BrokerIqCommunityPlusProfile = [SELECT Id FROM Profile WHERE Name =: PROFILE_NAME_PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS LIMIT 1];
            User CIIUser = BrokerIqTestDataFactory.createPortalUser(USER_CII_LASTNAME, BrokerIqCommunityPlusProfile.Id);
            usersToInsert.add(CIIUser);
            User CIIUserInsideAGroup = BrokerIqTestDataFactory.createPortalUser(USER_CII_INSIDE_A_GROUP_LASTNAME, BrokerIqCommunityPlusProfile.Id);
            usersToInsert.add(CIIUserInsideAGroup);
            User noCIIUser = BrokerIqTestDataFactory.createPortalUser(USER_NO_CII_LASTNAME, BrokerIqCommunityPlusProfile.Id);
            usersToInsert.add(noCIIUser);
            User biq2GuestUserInsideCIIGroup = BrokerIqTestDataFactory.createBrokerIq2GuestUser(BROKER_IQ2_GUEST_USER_INSIDE_CII_GROUP_LASTNAME);
            usersToInsert.add(biq2GuestUserInsideCIIGroup);
            User biq2GuestUserNotInsideCIIGroup = BrokerIqTestDataFactory.createBrokerIq2GuestUser(BROKER_IQ2_GUEST_USER_NOT_INSIDE_CII_GROUP_LASTNAME);
            usersToInsert.add(biq2GuestUserNotInsideCIIGroup);
            insert usersToInsert;
            
            //Update Webcast Iq Content
        	iqc = tog.getWebcastIqContent();
        	iqc.Name = 'A name with an octopus ' + keyword + ' that I can search on';
	        iqc.Active__c = true;
            update iqc;
        } 
    }

    static void querySetupData() {
        iqc = [SELECT Id, Name FROM IQ_Content__c];
    }
    
    @isTest
    static void matchBySearch() {
        querySetupData();

        Test.setFixedSearchResults(new List<Id>{iqc.Id});

        BrokerIq2SearchController.Results result =
                BrokerIq2SearchController.doSearch('octopus', 0, 100, null, null, null, null);

        System.assertEquals(1, result.totalItems);
        System.assertEquals(iqc.Id, result.iqContentPage[0].Id);
    }

    @isTest
    static void matchByRecordType() {
        querySetupData();

        BrokerIq2SearchController.Results result =
                BrokerIq2SearchController.doSearch(null,
                        0, 100,
                        null,
                        Nebula_Tools.NamedSObjectCache.getRecordType('Brighttalk', 'IQ_Content__c').Id,
                        null,
                        null);

        System.assertEquals(1, result.totalItems);
        System.assertEquals(iqc.Id, result.iqContentPage[0].Id);
    }

    @isTest
    static void matchByTopic() {
        querySetupData();
        Topic aTopic = new Topic(Name = 'Test Topic');
        insert aTopic;

        insert new TopicAssignment(TopicId = aTopic.Id, EntityId = iqc.Id, NetworkId = Network.getNetworkId());

        BrokerIq2SearchController.Results result =
                BrokerIq2SearchController.doSearch(null,
                        0, 100,
                        aTopic.Id,
                        null,
                        null,
                        null);

        System.assertEquals(1, result.totalItems);
        System.assertEquals(iqc.Id, result.iqContentPage[0].Id);
    }
    @isTest
    static void matchByContributor() {
        querySetupData();

        Broker_iQ_Contributor__c contributor = tog.getContributor(true);

        iqc.Broker_iQ_Contributor__c = contributor.Id;
        update iqc;

        BrokerIq2SearchController.Results result =
                BrokerIq2SearchController.doSearch(null,
                        0, 100,
                        null,
                        null,
                        contributor.Id,
                        null);

        System.assertEquals(1, result.totalItems);
        System.assertEquals(iqc.Id, result.iqContentPage[0].Id);
    }
    @isTest
    static void recentWebinar() {
        querySetupData();
        iqc.BrightTALK_Webcast__c = tog.getBtWebcast(120).Id;
        update iqc;

        Test.setFixedSearchResults(new List<Id>{iqc.Id});

        BrokerIq2SearchController.Results result =
                BrokerIq2SearchController.doSearch('octopus', 0, 100, null, null, null, false);

        System.assertEquals(1, result.totalItems);
        System.assertEquals(iqc.Id, result.iqContentPage[0].Id);
    }

    @isTest
    static void upcomingWebinar() {
        querySetupData();
        BrightTALK__Webcast__c wc = tog.getBtWebcast(120);
        wc.BrightTALK__Start_Date__c = DateTime.now().addDays(1);
        update wc;
        iqc.BrightTALK_Webcast__c = wc.Id;
        update iqc;

        Test.setFixedSearchResults(new List<Id>{iqc.Id});

        BrokerIq2SearchController.Results result =
                BrokerIq2SearchController.doSearch('octopus', 0, 100, null, null, null, true);

        System.assertEquals(1, result.totalItems);
        System.assertEquals(iqc.Id, result.iqContentPage[0].Id);
    }
}