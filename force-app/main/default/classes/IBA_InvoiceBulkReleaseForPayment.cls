global with sharing class IBA_InvoiceBulkReleaseForPayment {
    @InvocableMethod
    public static void bulkReleaseForPayment(List<c2g__codaPurchaseInvoice__c> invoices) {
        List<c2g.CODAAPICommon.Reference> invoiceReferences = new List<c2g.CODAAPICommon.Reference>();
        for(c2g__codaPurchaseInvoice__c inv :invoices) {
            c2g.CODAAPICommon.Reference ref = c2g.CODAAPICommon.getRef(null, inv.Name);
            invoiceReferences.add(ref);
        }
        c2g.CODAAPICommon_9_0.Context context = new c2g.CODAAPICommon_9_0.Context();
        c2g.CODAAPIPurchaseInvoice_9_0.BulkReleaseForPayment(context, invoiceReferences);
    }

    public static List<c2g__codaPurchaseInvoice__c> getInvoices(Set<Id> newItemIds) {
        Set<Id> salesInvoiceIds = new Set<Id>();
        for(c2g__codaTransactionLineItem__c tli :[SELECT Id, c2g__LineType__c, c2g__Transaction__r.c2g__SalesInvoice__c, c2g__Transaction__r.c2g__TransactionType__c, c2g__MatchingStatus__c, IBA_InvoicePaidFlowFired__c 
            FROM c2g__codaTransactionLineItem__c WHERE Id IN :newItemIds]) {
            if(tli.c2g__LineType__c == IBA_UtilConst.TRANSACTION_LINE_ITEM_TYPE_ACCOUNT && 
                tli.c2g__Transaction__r.c2g__TransactionType__c == IBA_UtilConst.TRANSACTION_TYPE_INVOICE &&
                tli.c2g__MatchingStatus__c == IBA_UtilConst.TRANSACTION_LINE_ITEM_MATCHING_STATUS_MATCHED && 
                tli.IBA_InvoicePaidFlowFired__c == false) {
                    salesInvoiceIds.add(tli.c2g__Transaction__r.c2g__SalesInvoice__c);
            }
        }
        List<c2g__codaPurchaseInvoice__c> invoices = [SELECT Id, Name FROM c2g__codaPurchaseInvoice__c WHERE 
            c2g__HoldStatus__c = :IBA_UtilConst.PAYABLE_INVOICE_HOLD_STATUS AND IBA_BrokerSalesInvoice__c IN :salesInvoiceIds];
        return invoices;
    }
}