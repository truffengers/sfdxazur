/**
 * Created by tijojoy on 04/03/2019.
 */

public with sharing class EWPolicyCoverageTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled()
    {
        return EWPolicyCoverageTriggerHandler.IsDisabled != null ? EWPolicyCoverageTriggerHandler.IsDisabled : false;
    }
    public void BeforeInsert(List<SObject> newItems)
    {
        EWPolicyCoverageHelper.handleVlocityAttributes(newItems);
        EWPolicyCoverageHelper.handleDuplicatingCoverages(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
    {
        EWPolicyCoverageHelper.HandleVlocityAttributes(newItems.values());
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}