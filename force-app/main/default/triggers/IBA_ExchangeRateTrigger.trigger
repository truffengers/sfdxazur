/**
* @name: IBA_ExchangeRateTrigger
* @description: Single trigger for c2g__codaExchangeRate__c object. All logic is delegated to handler class.
* @author: Konrad Wlazlo <kwlazlo@azuruw.com>
* @date: 20/12/2020
*/
trigger IBA_ExchangeRateTrigger on c2g__codaExchangeRate__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new IBA_ExchangeRateTriggerHandler());
}