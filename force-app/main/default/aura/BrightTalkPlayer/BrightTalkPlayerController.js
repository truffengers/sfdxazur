({
    doInit : function(component, event, helper) {

        var action = component.get("c.getInstance");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var controllerRecord = response.getReturnValue();
                var config = component.get('v.controllerConfig');
                var configObj;
                if($A.util.isEmpty(config)) {
                    configObj = {};
                } else {
                    configObj = JSON.parse(config);
                }

                configObj.channelId = parseInt(controllerRecord.channelId, 10);
                configObj.realmToken = controllerRecord.realmToken;
                configObj.displayMode = "standalone";

                component.set('v.controllerConfig', JSON.stringify(configObj));
                document.getElementsByClassName("jsBrightTALKEmbedConfig")[0].innerHTML = component.get('v.controllerConfig');
                startBrightTALK();
            }
        });

        $A.enqueueAction(action);

    },
    /*
	componentInitJs : function(component, event, helper) {
        console.log('====> BT player componentInitJs begin');
	    document.getElementsByClassName("jsBrightTALKEmbedConfig")[0].innerHTML = component.get('v.controllerConfig');
	    startBrightTALK();
        console.log('====> BT player componentInitJs end');
	},
	*/
	paramsChanged : function(component, event, helper) {
   //     console.log('====> BT player paramsChanged begin');
        var config = component.get('v.controllerConfig');
        var configObj;
        if($A.util.isEmpty(config)) {
            configObj = {};
        } else {
            configObj = JSON.parse(config);
        }
        configObj.commid = component.get('v.webcastId');
        configObj.height = component.get('v.playerHeight');
        configObj.width = component.get('v.playerWidth');

        component.set('v.controllerConfig', JSON.stringify(configObj));
     //   console.log('====> BT player paramsChanged end :' + JSON.stringify(configObj));
    }
})