@isTest
public with sharing class EWBatchSchedulerTest {
    
    @testSetup
    static void setup(){
        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );

        insert new List<Account>{
                broker, insured
        };

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.Status = EWConstants.EW_POLICY_STATUS_PENDING_CANCELLATION;
        
        insert policy;
    }

    @isTest
    public static void EWPolicyCancellationBatchScheduleTest(){
        Asset expectedPolicy;
        String cronExp = '0 0 1 * * ?';
        String jobId;

        Test.startTest();
            Database.Batchable<SObject> policyBatch = new EWPolicyCancellationBatch();
            EWBatchScheduler bSch = new EWBatchScheduler(policyBatch);
            jobId = System.schedule('testJob', cronExp, bSch);
            expectedPolicy = [SELECT Status FROM Asset LIMIT 1];
        Test.stopTest();

        List<CronTrigger> ct = [SELECT Id FROM CronTrigger WHERE id = :jobId];
        System.assert(jobId != null);
        System.assert(ct.size() > 0);
    }
}