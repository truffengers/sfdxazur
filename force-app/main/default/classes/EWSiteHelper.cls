/**
 * Created by roystonlloyd on 2019-08-13.
 */
public with sharing class EWSiteHelper {
    // WITH SHARING to manage SOQL returns to Community user.

    @AuraEnabled
    public static List <Asset> policyRelatedList(id recordId) {
        return [SELECT Id, Name, Status, vlocity_ins__EffectiveDate__c, vlocity_ins__ExpirationDate__c FROM Asset WHERE AccountId = :recordId AND Status = 'Issued' ORDER BY vlocity_ins__EffectiveDate__c DESC];
    }

}