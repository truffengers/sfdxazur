/**
 * Created by tijojoy on 04/03/2019.
 */

trigger EW_PolicyCoverage on vlocity_ins__AssetCoverage__c (before insert, before update) {

    TriggerDispatcher.Run(new EWPolicyCoverageTriggerHandler());
}