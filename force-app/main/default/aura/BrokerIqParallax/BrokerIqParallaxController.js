({
	componentInitJs : function(component, event, helper) {
        var $scrollable = $(window);
        var scrollRatioCalculator = TT.scrollRatioCalculator($scrollable);
      
        TT.parallax(_, {
          $elements             : $('.c-page-strip'),
          $scrollable           : $scrollable,
          scrollRatioCalculator : scrollRatioCalculator,
          data                  : [
            { yOffset: '40%', inertia: -0.25 }
          ]
        });
      
        TT.parallax(_, {
          $elements             : $('.c-bg-slice'),
          $scrollable           : $scrollable,
          scrollRatioCalculator : scrollRatioCalculator,
          data                  : [
            { yOffset: '0%', inertia: 0.1 },
            { yOffset: '0%', inertia: 0.2 }
          ]
        });
  	}
})