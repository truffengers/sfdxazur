/**
* @name: OrgInformationUtility
* @description: Utility class to provide information around current orgs,
                such as type of Org, currencies, timezone, etc. 
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date:05/01/2018
* @modifiedBy:
*/
public with sharing class OrgInformationUtility
{
    public static Boolean isSandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
}