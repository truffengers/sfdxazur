/**
* @name: EWVlocityDataFactory
* @description: Reusable factory methods for Vlocity objects
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 06/11/2018
*/
@isTest
public class EWVlocityDataFactory {



    public static List<vlocity_ins__CalculationProcedure__c> createCalculationProcedures(Set<String> calculationProcedureNames) {

        List<vlocity_ins__CalculationProcedure__c> calculationProcedures = new List<vlocity_ins__CalculationProcedure__c>();

        for (String name : calculationProcedureNames) {
            calculationProcedures.add(new vlocity_ins__CalculationProcedure__c(Name = name));
        }
        insert calculationProcedures;

        return calculationProcedures;
    }


    public static Map<String, Id> createCalculationProcedureVersions(List<vlocity_ins__CalculationProcedure__c> calculationProcedures){

        List<vlocity_ins__CalculationProcedureVersion__c> calculationProcedureVersions = new List<vlocity_ins__CalculationProcedureVersion__c>();
        for(vlocity_ins__CalculationProcedure__c certificationProcedure : calculationProcedures){
            calculationProcedureVersions.add(new vlocity_ins__CalculationProcedureVersion__c(
                    Name = certificationProcedure.Name,
                    vlocity_ins__CalculationProcedureId__c = certificationProcedure.Id,
                    vlocity_ins__StartDateTime__c = Datetime.now().addDays(-7)
            ));
        }
        insert calculationProcedureVersions;

        Map<String, Id> versionIdMap =  new Map<String, Id>();
        for(vlocity_ins__CalculationProcedureVersion__c version : calculationProcedureVersions){
            versionIdMap.put(version.Name, version.Id);
        }
        return versionIdMap;

    }


    public static vlocity_ins__CalculationProcedureVariable__c createCalculationProcedureVariable (String name, String dataType, String defaultValue, Id versionId) {
        return new vlocity_ins__CalculationProcedureVariable__c(
                Name = name,
                vlocity_ins__Alias__c = name,
                vlocity_ins__DataType__c = dataType,
                vlocity_ins__DefaultValue__c = defaultValue,
                vlocity_ins__CalculationProcedureVersionId__c = versionId,
                vlocity_ins__IsEditable__c = true,
                vlocity_ins__IsSelected__c = true,
                vlocity_ins__UserDefined__c = true
        );
    }


    public static vlocity_ins__CalculationProcedureStep__c createProcedureStep(String name, String dataType, Id procedureVersionId, Integer sequence, Id matrixId ){
        return new vlocity_ins__CalculationProcedureStep__c(
                vlocity_ins__CalculationFormulaLong__c = matrixId == null ? name : null,
                vlocity_ins__CalculationFormulaTags__c = matrixId == null ? '[{"text":"'+name+'","alias":"'+name+'","userDefined":true,"dataType":"'+dataType+'"}]' : null,
                vlocity_ins__CalculationMatrixId__c = matrixId,
                vlocity_ins__CalculationProcedureVersionId__c = procedureVersionId,
                vlocity_ins__ConditionalInputDisplayData__c ='[]',
                vlocity_ins__Function__c = matrixId == null ? 'Calculation' : 'Matrix Lookup',
                vlocity_ins__Input__c = '[{"name":"'+name+'","dataType":"'+dataType+'"}]',
                vlocity_ins__IsConditionalStep__c = false,
                vlocity_ins__IsIncludedInResult__c = true,
                vlocity_ins__OutputJSON__c = '{"name":"'+name+'","dataType":"'+dataType+'","alias":"'+name+'"}',
                vlocity_ins__OutputMappingJSON__c = '{"'+name+'":"'+name+'"}',
                vlocity_ins__Sequence__c  = sequence
        );
    }


    public static vlocity_ins__CalculationMatrix__c createCalculationMatrix(String name){

        vlocity_ins__CalculationMatrix__c matrix = new vlocity_ins__CalculationMatrix__c(
                Name = name
        );
        insert matrix;
        return matrix;
    }


    public static vlocity_ins__CalculationMatrixVersion__c createMatrixVersion(vlocity_ins__CalculationMatrix__c matrix){

        vlocity_ins__CalculationMatrixVersion__c matrixVersion = new vlocity_ins__CalculationMatrixVersion__c(vlocity_ins__CalculationMatrixId__c = matrix.Id, vlocity_ins__VersionNumber__c = 1, vlocity_ins__StartDateTime__c = DateTime.now().addDays(-8));
        insert matrixVersion;
        return matrixVersion;
    }


    public static void activateCalculationMatrix(Id matrixVersonId){

        vlocity_ins__CalculationMatrixVersion__c version = new vlocity_ins__CalculationMatrixVersion__c(
                Id = matrixVersonId, vlocity_ins__IsEnabled__c = true);
        update version;
    }


    public static vlocity_ins__CalculationMatrixRow__c createCalculationMatrixRow (String name, String inputData, String outputData, Id matrixVersionId ){
        return new vlocity_ins__CalculationMatrixRow__c(
                Name = name,
                vlocity_ins__InputData__c = inputData,
                vlocity_ins__OutputData__c = outputData,
                vlocity_ins__CalculationMatrixVersionId__c = matrixVersionId
        );
    }


    public static void activateCalculationProcedures(Map<String,Id> versionIdMap){

        List<vlocity_ins__CalculationProcedureVersion__c> versions = new List<vlocity_ins__CalculationProcedureVersion__c>();
        for(String versionName : versionIdMap.keySet()){
            vlocity_ins__CalculationProcedureVersion__c version = new vlocity_ins__CalculationProcedureVersion__c(Id = versionIdMap.get(versionName));
            version.vlocity_ins__IsEnabled__c =true;
            version.vlocity_ins__Constants__c = '[]';
            versions.add(version);
        }
        update versions;
    }




}