/*
Name:            EWActivityExplorerControllerTest
Description:     This is the test class for EWActivityExplorerController.
Created By:      Rajni Bajpai 
Created On:      18 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               
************************************************************************************************
*/
@isTest

public class EWActivityExplorerControllerTest {
    private static Account broker;
    private static User systemAdminUser;
    private static Product2 product;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static PricebookEntry pricebookEntry;
    private static Asset policy;
    
    private static testMethod void testgetActivityHistoryQuote() {        
        setup();
        Test.startTest();
        String result= EWActivityExplorerController.getActivityHistory(quote.Id);
        System.assertEquals(result.contains('"title":"Test Case"'),true);
        System.assertEquals(result.contains('"body":"Test email body"'),true);
        System.assertEquals(result.contains('"entryType":"Attachment"'),true);        
        Test.stopTest();                   
    }
    
    private static testMethod void testgetActivityHistoryAsset() {        
        setup();
        Test.startTest();
        String result= EWActivityExplorerController.getActivityHistory(policy.Id);
        System.assertEquals(result.contains('"title":"Header_Picture1"'),true);        
        Test.stopTest();                    
    }
    private static testMethod void testgetActivityHistoryAccount() {        
        setup();
        Test.startTest();
        string result=EWActivityExplorerController.getActivityHistory(insured.Id);
        System.assertEquals(result.contains('"body":"Test email body"'),true);        
        Test.stopTest();                    
    }
    static void setup() {
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);
        
        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            techteamUserRole.Id,
            true);
        
        System.runAs(systemAdminUser) {
            
            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true);
            
            product = EWProductDataFactory.createProduct(null, true);
            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
            
            policy = new Asset(Name = 'testAsset',product2Id=product.Id, Status = 'Issued', vlocity_ins__ExpirationDate__c = quote.vlocity_ins__EffectiveDate__c.addDays(2), AccountId = insured.Id);
            insert policy;
            priceBookEntry = new PricebookEntry (Product2Id = product.id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
            insert priceBookEntry;
            
            ContentVersion content=new ContentVersion(); 
            content.Title='Header_Picture1'; 
            content.PathOnClient='/' + content.Title + '.jpg'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob; 
            content.origin = 'H';
            insert content;
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=policy.id;
            contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
            contentlink.ShareType = 'I';
            contentlink.Visibility = 'AllUsers';             
            insert contentlink;
            
            String CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EWConstants.CASE_RECORD_TYPE_COMMUNITY).getRecordTypeId();
            Case newCase = new Case(Subject='Test Case',Origin='Email',Status='New',RecordtypeId=CaseRecordTypeId,EW_Quote__c=quote.Id);
            insert newCase;
            
            //Insert emailmessage for case
            EmailMessage email = new EmailMessage();
            email.FromAddress = 'test@abc.org';
            email.Incoming = True;
            email.ToAddress= 'test@xyz.org';
            email.Subject = 'Test email';
            email.HtmlBody = 'Test email body';
            email.RelatedToId = quote.Id; 
            insert email;
            
            Attachment attach=new Attachment();   	
            attach.Name='Test';
            attach.body=Blob.valueOf('Testing Body of Attachment');
            attach.parentId=quote.id;
            insert attach;
            
        }
    }
}