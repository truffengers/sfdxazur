public class BrokerIqWebinarPreviewController {
    public BrightTALK__Webcast__c thisWebcast {get; set;}
    public String webcastJsonP {get; set {
        thisWebcast = (BrightTALK__Webcast__c)JSON.deserialize(value, BrightTALK__Webcast__c.class);
    }}
    
    public static String getQuery(String whereClause) {
        return 'SELECT Id, BrightTALK__Webcast_Id__c, BrightTALK__Name__c, '
            + 'IQ_Content__r.Category__r.Name, IQ_Content__r.Category__r.Colour__c, '
            + 'IQ_Content__r.Category__r.Icon__c, '
            + 'IQ_Content__r.Category__r.Placeholder_Image__c, '
            + 'Preview_Thumbnail__c, Preview_Image__c '
            + 'FROM BrightTALK__Webcast__c '
            + 'WHERE ' + whereClause;
    }

    public static String getSearch(String findClause, String whereClause) {
        return 'FIND \'' + findClause + '\' '
            + 'RETURNING BrightTALK__Webcast__c(Id, BrightTALK__Webcast_Id__c, BrightTALK__Name__c, '
            + 'IQ_Content__r.Category__r.Name, IQ_Content__r.Category__r.Colour__c, '
            + 'IQ_Content__r.Category__r.Icon__c, '
            + 'IQ_Content__r.Category__r.Placeholder_Image__c, '
            + 'Preview_Thumbnail__c, Preview_Image__c '
            + ' WHERE ' + whereClause + ')';
    }
    
    public static List<String> webcastsToListOfJson(List<BrightTALK__Webcast__c> webcasts) {
        List<String> rval = new List<String>();
        for(BrightTALK__Webcast__c wc : webcasts) {
            rval.add(JSON.serialize(wc));
        }
        return rval;
    }
}