/**
* @name: EWAccountHelper
* @description: Helper class for the account object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 03/09/2018
* @lastChangedBy Steve Loftus sloftus@azuruw.com 11/09/2018
* @lastChangedBy Steve Loftus sloftus@azuruw.com 12/09/2018
************************************************************************************************
Sr.No.          ChangedBy           Date                 Desc
************************************************************************************************
1             Rajni Bajpai         29 Apr 2021           EWH-1983 Sharing between Offices
************************************************************************************************
*/
public with sharing class EWAccountHelper {
    public EWAccountHelper() {

    }

    public static void CheckForDuplicateAccount(DuplicateAccountDetails details) {

        EWFeatureSwitches__c featureSwitches = EWFeatureSwitches__c.getInstance();

        details.isDuplicate = false;
        details.hasLiveQuote = false;
        details.hasLivePolicy = false;

        DuplicateAccountFlags daf = checkAccount(
                details.brokerAccountId,
                details.firstName,
                details.lastName,
                details.birthDateTime
        );

        details.isBlocked = daf.isBlocked;

        if (featureSwitches.InsuredDuplication__c) {
            details.isDuplicate = daf.isDuplicate;
            details.hasLiveQuote = daf.hasLiveQuote;
            details.hasLivePolicy = daf.hasLivePolicy;
        }
    }

    private static DuplicateAccountFlags checkAccount(Id brokerAccountId, String firstName, String lastName, Datetime birthDateTime) {
        DuplicateAccountFlags daf = new DuplicateAccountFlags();
        Set<Id> insuredAccountIdSet = new Set<Id>();

        List<Account> possiblyDuplicateOrBlockedAccounts = EWDuplicateBlockHelper.queryAccounts(firstName, lastName, birthDateTime);

        for (Account account : possiblyDuplicateOrBlockedAccounts) {
            if (account.EW_IsBlocked__c) {
                daf.isBlocked = true;
            }

            if (account.EW_BrokerAccount__c == brokerAccountId) {
                daf.isDuplicate = true;
            }

            insuredAccountIdSet.add(account.Id);
        }

        if (!insuredAccountIdSet.isEmpty()) {
            daf.insuredAccountIdSet = insuredAccountIdSet;

            daf.hasLiveQuote = [
                    SELECT COUNT()
                    FROM Quote
                    WHERE (Status = :EWConstants.QUOTE_STATUS_QUOTED
                        OR Status = :EWConstants.QUOTE_STATUS_UW_REFERRED
                        OR Status = :EWConstants.QUOTE_STATUS_UW_APPROVED
                        OR Status = :EWConstants.QUOTE_STATUS_APPROVED
                        OR Status = :EWConstants.QUOTE_STATUS_BROKER_REFERRED)
                    AND AccountId IN :insuredAccountIdSet
                    AND vlocity_ins__AgencyBrokerageId__c = :brokerAccountId
            ] > 0;

            daf.hasLivePolicy = [
                    SELECT COUNT()
                    FROM Asset
                    WHERE Status = :EWConstants.EW_POLICY_STATUS_ISSUED
                    AND vlocity_ins__ExpirationDate__c > TODAY
                    AND AccountId IN :insuredAccountIdSet
            ] > 0;
        }

        return daf;
    }

    private static DuplicateAccountFlags checkAccount(InsuredAccountsDetails details, Boolean isPrime) {
        if (isPrime) {
            return checkAccount(
                    details.brokerAccountId,
                    details.primeInsFirstName,
                    details.primeInsLastName,
                    details.primeInsBirthDateTime
            );
        } else {
            return checkAccount(
                    details.brokerAccountId,
                    details.jointInsFirstName,
                    details.jointInsLastName,
                    details.jointInsBirthDateTime
            );
        }
    }

    public static void updateInsuredAccountsData(InsuredAccountsDetails details) {
        details.primeInsIsDuplicate = false;
        details.primeInsHasLiveQuote = false;
        details.primeInsHasLivePolicy = false;

        details.jointInsIsDuplicate = false;
        details.jointInsHasLiveQuote = false;
        details.jointInsHasLivePolicy = false;

        details.jointInsLastName = (details.hasJointPolicyHolder.toLowerCase() != 'no') ? details.jointInsLastName : '';

        Account currentPrimeIns = new Account();
        Account currentJointIns = new Account();
        Boolean hasJointInsNow = false;

        Map<String, Object> setupData = EWQuoteLightningController.getSetupData(details.recordId);

        currentPrimeIns = [
                SELECT Id, Salutation, FirstName, LastName, PersonBirthdate, EW_BrokerAccount__c, PersonContactId
                FROM Account
                WHERE Id = :(String) setupData.get('accountId')
        ];

        Quote quote = (Quote) setupData.get('quote');

        currentJointIns = (Account) setupData.get('jointInsured');
        hasJointInsNow = String.isNotBlank(currentJointIns.Id);

        Boolean primeRemoved = false;
        Boolean primeUpdated = false;
        Boolean jointRemoved = false;
        Boolean jointUpdated = false;

        /** check primary ins diff **/
        if (String.isBlank(details.primeInsLastName)) {
            // primary ins removed
            primeRemoved = true;
        } else if (details.primeInsChanged(currentPrimeIns)) {
            // primary ins changed
            DuplicateAccountFlags daf = checkAccount(details, true);

            details.primeInsIsDuplicate = daf.isDuplicate;
            details.primeInsHasLiveQuote = daf.hasLiveQuote;
            details.primeInsHasLivePolicy = daf.hasLivePolicy;

            updateAccount(details, currentPrimeIns, true);

            if (details.primeInsIsDuplicate) {
                currentPrimeIns.Id = (Id) daf.insuredAccountIdSet.iterator().next();
            }

            updateRelatedRecords(details.recordId, currentPrimeIns);

            primeUpdated = true;
        }

        Boolean hasJointInsNowAndWasChangedButNotRemoved = (String.isNotBlank(details.jointInsLastName) && hasJointInsNow && details.jointInsChanged(currentJointIns));
        Boolean noJointInsNowAndQasAdded = (!hasJointInsNow && String.isNotBlank(details.jointInsLastName));

        /** check if joint added, changed or removed **/
        if (hasJointInsNowAndWasChangedButNotRemoved || noJointInsNowAndQasAdded) {
            // joint ins changed
            DuplicateAccountFlags daf = checkAccount(details, false);

            details.jointInsIsDuplicate = daf.isDuplicate;
            details.jointInsHasLiveQuote = daf.hasLiveQuote;
            details.jointInsHasLivePolicy = daf.hasLivePolicy;

            updateAccount(details, currentJointIns, false);

            if (details.jointInsIsDuplicate) {
                currentJointIns.Id = (Id) daf.insuredAccountIdSet.iterator().next();
            } else if (String.isBlank(currentJointIns.Id)) {
                currentJointIns.RecordTypeId = Account.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
                insert currentJointIns;
            }

            if (primeRemoved) {
                updateRelatedRecords(details.recordId, currentJointIns);
                updateHousehold(details, quote, currentJointIns, null);
            } else {
                updateHousehold(details, quote, currentPrimeIns, currentJointIns);
            }

            jointUpdated = true;
        } else if (hasJointInsNow && (String.isBlank(details.jointInsLastName) || details.hasJointPolicyHolder.toLowerCase() == 'no')) {
            // joint ins removed
            currentJointIns.EW_PrimaryInsured__c = null;
            update currentJointIns;

            updateHousehold(details, quote, currentPrimeIns, null);
            jointRemoved = true;
        } else if (primeRemoved) {
            // joint the same, prime removed
            updateRelatedRecords(details.recordId, currentJointIns);
            updateHousehold(details, quote, currentJointIns, null);
        } else if (primeUpdated) {
            // joint the same, prime updated
            updateHousehold(details, quote, currentPrimeIns, currentJointIns);
        }

        if (primeRemoved) {
            details.primeInsFirstName = details.jointInsFirstName;
            details.primeInsLastName = details.jointInsLastName;
            details.primeInsSalutation = details.jointInsSalutation;
            details.primeInsBirthDateTime = details.jointInsBirthDateTime;
            details.primeInsHasChanged = details.jointInsHasChanged;
            details.primeInsHasLiveQuote = details.jointInsHasLiveQuote;
            details.primeInsHasLivePolicy = details.jointInsHasLivePolicy;
            details.primeInsIsDuplicate = details.jointInsIsDuplicate;
            details.hasJointPolicyHolder = 'no';

            details.jointInsId = null;
            details.jointInsFirstName = null;
            details.jointInsLastName = null;
            details.jointInsSalutation = null;
            details.jointInsBirthDateTime = null;
            details.jointInsHasChanged = false;
            details.jointInsHasLiveQuote = false;
            details.jointInsHasLivePolicy = false;
            details.jointInsIsDuplicate = false;
        }

        details.primeInsHasChanged = (primeRemoved || primeUpdated);
        details.jointInsHasChanged = (jointRemoved || jointUpdated);
        details.primeInsId = primeRemoved ? currentJointIns.Id : currentPrimeIns.Id;
        details.jointInsId = (!primeRemoved && String.isNotBlank(details.jointInsLastName)) ? currentJointIns.Id : null;
    }

    private static void updateAccount(InsuredAccountsDetails details, Account targetAccount, Boolean isPrime) {
        if (isPrime) {
            targetAccount.EW_BrokerAccount__c = details.brokerAccountId;
            targetAccount.FirstName = details.primeInsFirstName;
            targetAccount.LastName = details.primeInsLastName;
            targetAccount.Salutation = details.primeInsSalutation;
            targetAccount.PersonBirthdate = details.primeInsBirthDateTime.date();
        } else {
            targetAccount.EW_BrokerAccount__c = details.brokerAccountId;
            targetAccount.FirstName = details.jointInsFirstName;
            targetAccount.LastName = details.jointInsLastName;
            targetAccount.Salutation = details.jointInsSalutation;
            targetAccount.PersonBirthdate = details.jointInsBirthDateTime.date();
        }
    }

    private static void updateRelatedRecords(Id quoteId, Account sourceAccount) {
        Quote quote = [SELECT Id, Name, OpportunityId, AccountId FROM Quote WHERE Id = :quoteId];
        quote.Name = sourceAccount.LastName + '_' + sourceAccount.FirstName;

        Opportunity opp = [SELECT Id, Name, AccountId FROM Opportunity WHERE Id = :quote.OpportunityId];
        opp.AccountId = sourceAccount.Id;
        opp.Name = sourceAccount.LastName + '-EW-' + Datetime.now().format('dd-MM-YYYY');

        update quote;
        update opp;
    }

    public static void updateHousehold(InsuredAccountsDetails details, Quote quote, Account currentPrimeIns, Account currentJointIns) {
        if (currentJointIns == null) {
            currentJointIns = new Account(FirstName = '', LastName = '');
        } else if (String.isNotBlank(currentJointIns.Id)) {
            currentJointIns = [
                    SELECT Id, Salutation, FirstName, LastName, PersonBirthdate, EW_BrokerAccount__c, PersonContactId
                    FROM Account
                    WHERE Id = :currentJointIns.Id
                    LIMIT 1
            ];
        }

        List<vlocity_ins__Application__c> applications = new List<vlocity_ins__Application__c>([
                SELECT Id, vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
                FROM vlocity_ins__Application__c
                WHERE vlocity_ins__OpportunityId__c = :quote.OpportunityId
        ]);

        vlocity_ins__Application__c application = applications.get(0);

        List<vlocity_ins__Party__c> parties = new List<vlocity_ins__Party__c>([
                SELECT Id, Name, vlocity_ins__HouseholdId__c, vlocity_ins__AccountId__c, vlocity_ins__ContactId__c, vlocity_ins__PartyEntityType__c, vlocity_ins__IsPersonAccount__c, vlocity_ins__PartyEntityId__c
                FROM vlocity_ins__Party__c
                WHERE vlocity_ins__HouseholdId__c = :application.vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c AND Id != :application.vlocity_ins__PrimaryPartyId__c
        ]);

        vlocity_ins__Household__c household = [
                SELECT Id, Name
                FROM vlocity_ins__Household__c
                WHERE Id = :application.vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
                LIMIT 1
        ];

        vlocity_ins__Party__c householdParty = null;
        vlocity_ins__Party__c jointParty = null;

        for (vlocity_ins__Party__c p : parties) {
            if (p.vlocity_ins__PartyEntityType__c == 'Household') {
                householdParty = p;
            } else {
                jointParty = p;
            }
        }

        if (currentJointIns == null || String.isEmpty(currentJointIns.Id)) {
            // there is only prime ins
            String idShort = ((String) jointParty.Id).substring(0, 15);
            jointParty.Name = idShort;
            jointParty.vlocity_ins__AccountId__c = null;
            jointParty.vlocity_ins__PartyEntityId__c = null;
            jointParty.vlocity_ins__PartyEntityType__c = null;
            jointParty.vlocity_ins__IsPersonAccount__c = false;
            jointParty.vlocity_ins__ContactId__c = null;
        } else {
            jointParty.Name = details.jointInsFirstName + ' ' + details.jointInsLastName;
            jointParty.vlocity_ins__AccountId__c = currentJointIns.Id;
            jointParty.vlocity_ins__PartyEntityId__c = currentJointIns.Id;
            jointParty.vlocity_ins__PartyEntityType__c = 'Account';
            jointParty.vlocity_ins__IsPersonAccount__c = true;
            jointParty.vlocity_ins__ContactId__c = currentJointIns.PersonContactId;
        }

        householdParty.Name = details.primeInsLastName + ' and ' + details.jointInsLastName;
        household.Name = details.primeInsLastName + ' and ' + details.jointInsLastName;

        update parties;
        update household;
    }

    public static void createBrokerShares(Map<Id, SObject> newAccounts) {
        EWAccountShareToBroker.shareClientAccount(newAccounts);       
        Set<Id> userIdSet = new Set<Id>();

        // get the users from the new insured accounts
        for (SObject obj : newAccounts.values()) {

            Account account = (Account) obj;

            if (account.IsPersonAccount) {
                userIdSet.add(account.OwnerId);
            }
        }

        if (!userIdSet.isEmpty()) {

            Map<Id, Id> userRoleIdByUserIdMap = new Map<Id, Id>();

            for (User user : [
                    SELECT UserRoleId
                    FROM User
                    WHERE IsActive = TRUE
                    AND (UserType = 'Partner' OR UserType = 'PowerPartner')
                    AND Id IN :userIdSet
            ]) {

                userRoleIdByUserIdMap.put(user.Id, user.UserRoleId);
            }

            if (!userRoleIdByUserIdMap.isEmpty()) {

                Map<Id, UserRole> userRoleByUserRoleIdMap = new Map<Id, UserRole>();

                for (UserRole userRole : [
                        SELECT Id, DeveloperName
                        FROM UserRole
                        WHERE Id IN :userRoleIdByUserIdMap.values()
                ]) {

                    userRoleByUserRoleIdMap.put(userRole.Id, userRole);
                }

                if (!userRoleByUserRoleIdMap.isEmpty()) {

                    Map<Id, Id> groupIdByUserRoleIdMap = new Map<Id, Id>();

                    for (Group userGroup : [
                            SELECT Id, RelatedId, DeveloperName
                            FROM Group
                            WHERE Type = 'RoleAndSubordinates'
                            AND RelatedId IN :userRoleByUserRoleIdMap.keySet()
                    ]) {

                        if (userGroup.DeveloperName == userRoleByUserRoleIdMap.get(userGroup.RelatedId).DeveloperName) {
                            groupIdByUserRoleIdMap.put(userGroup.RelatedId, userGroup.Id);
                        }
                    }

                    if (!groupIdByUserRoleIdMap.isEmpty()) {

                        List<AccountShare> accountShareList = new List<AccountShare>();

                        for (SObject obj : newAccounts.values()) {

                            Account account = (Account) obj;

                            if (userRoleIdByUserIdMap.containsKey(account.OwnerId)) {

                                Id userRoleId = userRoleIdByUserIdMap.get(account.OwnerId);

                                if (groupIdByUserRoleIdMap.containsKey(userRoleId)) {

                                    AccountShare accountShare = new AccountShare();
                                    accountShare.AccountId = account.Id;
                                    accountShare.UserOrGroupId = groupIdByUserRoleIdMap.get(userRoleId);
                                    accountShare.AccountAccessLevel = 'Edit';
                                    accountShare.OpportunityAccessLevel = 'Edit';

                                    accountShareList.add(accountShare);
                                }
                            }
                        }

                        if (!accountShareList.isEmpty()) {

                            AccountShareUtil util = new AccountShareUtil();
                            util.insertShares(accountShareList);
                        }
                    }
                }
            }
        }
    }

    public static void createNewMTAQuoteRelatedRecords(String policyId, String ownerId, Map<String, Object> outputMap) {
        Quote quote = [
                SELECT Id, OpportunityId, AccountId, Opportunity.Agency_Brokerage_Account__c, Opportunity.RecordTypeId,
                        Opportunity.Name, Opportunity.Amount, Opportunity.StageName, Opportunity.Probability, Opportunity.AccountId
                FROM Quote
                WHERE EW_Policy__c = :policyId
                LIMIT 1
        ];

        List<vlocity_ins__Application__c> applications = new List<vlocity_ins__Application__c>([
                SELECT Id, vlocity_ins__Product2Id__c, vlocity_ins__OpportunityId__c, vlocity_ins__AccountId__c,
                        vlocity_ins__PrimaryPartyId__c, vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
                FROM vlocity_ins__Application__c
                WHERE vlocity_ins__OpportunityId__c = :quote.OpportunityId
        ]);

        vlocity_ins__Application__c application = applications.get(0);

        Map<Id, vlocity_ins__Party__c> parties = new Map<Id, vlocity_ins__Party__c>([
                SELECT Id, Name, vlocity_ins__HouseholdId__c, vlocity_ins__AccountId__c, vlocity_ins__ContactId__c,
                        vlocity_ins__PartyEntityType__c, vlocity_ins__IsPersonAccount__c, vlocity_ins__PartyEntityId__c
                FROM vlocity_ins__Party__c
                WHERE vlocity_ins__HouseholdId__c = :application.vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
        ]);

        vlocity_ins__Household__c household = [
                SELECT Id, Name, vlocity_ins__Country__c, vlocity_ins__PostalCode__c, vlocity_ins__City__c,
                        vlocity_ins__PrimaryPartyId__c, vlocity_ins__Address1__c, vlocity_ins__PrimaryAccountId__c, RecordTypeId
                FROM vlocity_ins__Household__c
                WHERE Id = :application.vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
                LIMIT 1
        ];

        Opportunity newOpp = new Opportunity();
        newOpp.OwnerId = ownerId;
        newOpp.Name = quote.Opportunity.Name;
        newOpp.AccountId = quote.Opportunity.AccountId;
        newOpp.RecordTypeId = quote.Opportunity.RecordTypeId;
        newOpp.Amount = quote.Opportunity.Amount;
        newOpp.StageName = quote.Opportunity.StageName;
        newOpp.Probability = quote.Opportunity.Probability;
        newOpp.Agency_Brokerage_Account__c = quote.Opportunity.Agency_Brokerage_Account__c; // ?
        newOpp.CloseDate = Date.today().addDays(30);
        insert newOpp;

        vlocity_ins__Household__c newHousehold = household.clone();
        insert newHousehold;

        vlocity_ins__Party__c newPrimaryParty = parties.get(application.vlocity_ins__PrimaryPartyId__c).clone();
        newPrimaryParty.vlocity_ins__HouseholdId__c = newHousehold.Id;
        parties.remove(application.vlocity_ins__PrimaryPartyId__c);

        List<vlocity_ins__Party__c> newParties = new List<vlocity_ins__Party__c>{
                newPrimaryParty
        };
        for (vlocity_ins__Party__c party : parties.values()) {
            vlocity_ins__Party__c clonedParty = party.clone();
            clonedParty.vlocity_ins__HouseholdId__c = newHousehold.Id;
            newParties.add(clonedParty);
        }
        insert newParties;

        newHousehold.vlocity_ins__PrimaryPartyId__c = newPrimaryParty.Id;
        update newHousehold;

        vlocity_ins__Application__c newApplication = application.clone();
        newApplication.vlocity_ins__OpportunityId__c = newOpp.Id;
        newApplication.vlocity_ins__PrimaryPartyId__c = newPrimaryParty.Id;
        insert newApplication;

        outputMap.put('result', new Map<String, Object>{
                'newOpportunityId' => newOpp.Id,
                'accountId' => newOpp.AccountId
        });
    }

    public without sharing class AccountShareUtil {
        public void insertShares(List<AccountShare> accountShareList) {
            insert accountShareList;
        }
    }

    public class DuplicateAccountFlags {
        public Boolean isDuplicate;
        public Boolean isBlocked;
        public Boolean hasLiveQuote;
        public Boolean hasLivePolicy;
        public Set<Id> insuredAccountIdSet;

        public DuplicateAccountFlags() {
            this.isDuplicate = false;
            this.isBlocked = false;
            this.hasLiveQuote = false;
            this.hasLivePolicy = false;
            this.insuredAccountIdSet = new Set<Id>();
        }
    }

    public class DuplicateAccountDetails {
        public Id brokerAccountId;
        public String firstName;
        public String lastName;
        public Datetime birthDateTime;
        public Boolean isDuplicate;
        public Boolean hasLiveQuote;
        public Boolean hasLivePolicy;
        public Boolean isBlocked;

        public DuplicateAccountDetails(
                Id brokerAccountId,
                String firstName,
                String lastName,
                Datetime birthDateTime,
                Boolean isDuplicate,
                Boolean hasLiveQuote,
                Boolean hasLivePolicy,
                Boolean isBlocked) {

            this.brokerAccountId = brokerAccountId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDateTime = birthDateTime;
            this.isDuplicate = isDuplicate;
            this.hasLiveQuote = hasLiveQuote;
            this.hasLivePolicy = hasLivePolicy;
            this.isBlocked = isBlocked;
        }
    }

    public class InsuredAccountsDetails {
        public Id brokerAccountId;
        public Id recordId;

        public Id primeInsId;
        public Id jointInsId;
        public Boolean primeInsHasChanged = false;
        public Boolean jointInsHasChanged = false;
        public String hasJointPolicyHolder = 'no';

        public String primeInsSalutation;
        public String primeInsFirstName;
        public String primeInsLastName;
        public Datetime primeInsBirthDateTime;
        public Boolean primeInsIsDuplicate = false;
        public Boolean primeInsHasLiveQuote = false;
        public Boolean primeInsHasLivePolicy = false;

        public String jointInsSalutation;
        public String jointInsFirstName;
        public String jointInsLastName;
        public Datetime jointInsBirthDateTime;
        public Boolean jointInsIsDuplicate = false;
        public Boolean jointInsHasLiveQuote = false;
        public Boolean jointInsHasLivePolicy = false;

        public Boolean primeInsChanged(Account primeIns) {
            Boolean isCHanged = primeIns.FirstName != this.primeInsFirstName
                    || primeIns.LastName != this.primeInsLastName
                    || primeIns.Salutation != this.primeInsSalutation
                    || primeIns.EW_BrokerAccount__c != this.brokerAccountId
                    || (primeInsBirthDateTime.date().daysBetween(primeIns.PersonBirthdate) != 0);

            return isCHanged;
        }

        public Boolean jointInsChanged(Account jointIns) {
            Boolean isCHanged = jointIns.FirstName != this.jointInsFirstName
                    || jointIns.LastName != this.jointInsLastName
                    || jointIns.Salutation != this.jointInsSalutation
                    || jointIns.EW_BrokerAccount__c != this.brokerAccountId
                    || (jointInsBirthDateTime.date().daysBetween(jointIns.PersonBirthdate) != 0);

            return isCHanged;
        }
    }

    @AuraEnabled
    public static Boolean checkIfBrokerNetworkBroker() {
        Id currentUserId = UserInfo.getUserId();
        Id currentAccountId = [SELECT ContactId,
                Contact.AccountId
        FROM User
        WHERE Id = :currentUserId].Contact.AccountId;

        if (currentAccountId == NULL) {
            return false;
        }

        Boolean showBrokerNetworkContent = [SELECT Name, EW_isBrokerNetworkBroker__c
        FROM Account
        WHERE Id = :currentAccountId].EW_isBrokerNetworkBroker__c;

        return showBrokerNetworkContent;
    }
}