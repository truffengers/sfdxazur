(function( $ ) {

  $.fn.clam = function() {

    var clams = [];

    function clam(element) {

      var ease        = $.support.transition ? 'ease-in-out' : 'swing';
      var $element    = $(element);
      var $button     = $element.find('> :first-child');
      var $body       = $element.find('> :last-child');
      var id          = $element.attr('id');
      var cookieKey   = 'clam-' + id;
      var isAnimating = false;
      var OPENED      = 'opened';
      var CLOSED      = 'closed';
      var OPEN_CLASS  = 'is-open';
      var state;



      function init() {
        state = getInitialState();
        if(state === OPENED) { open(); }
        $button.click(clickHandler);
      }

      function getInitialState() {
        return id !== undefined && Cookies.set(cookieKey) === OPENED ? OPENED : CLOSED;
      }

      function clickHandler(event) {
        event.preventDefault();
        if(isAnimating) { return; }
        state === OPENED ? close() : open();
      }

      function updateCookie() {
        Cookies.set(cookieKey, state, { expires: 365 });
      }

      function animate(finishHeight, callback) {
        $body.transition({ height: finishHeight }, ease, function() {
          isAnimating = false;
          callback();
        });
      }

      function open() {
        isAnimating = true;
        state = OPENED;
        updateCookie();
        $element.addClass(OPEN_CLASS);
        var finishHeight = $body.height();
        $body.height(0);
        animate(finishHeight, function() {
          $body.height('');
        });
      }

      function close() {
        isAnimating = true;
        state = CLOSED;
        updateCookie();
        $body.height($body.height());
        animate(0, function() {
          $element.removeClass(OPEN_CLASS);
          $body.height('');
        });
      }

      init();
    }


    $(this).each(function() { clams.push(clam(this)); });

    return this;

  };

}( jQuery ));