/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 14/10/2017
 */

@isTest
private class BrokerIqCarouselControllerTest {

    private static TestObjectGenerator tog = new TestObjectGenerator();

    @isTest static void testBehavior() {
        System.debug([SELECT Id, Name, NetworkId FROM Topic WHERE Name = 'Insurance' AND NetworkId = null]);
        ContentVersion cv = tog.getContentVersion('foo.txt', Blob.valueOf('foo'));
        IQ_Content__c iqc = tog.getWebcastIqContent();
        iqc.Active__c = true;
        iqc.Is_Carousel__c = true;
        iqc.Broker_iQ_Contributor__c = tog.getContributor(true).Id;
        iqc.Carousel_Image_480__c = cv.ContentDocumentId;
        iqc.Primary_Topic__c = 'Insurance';
        update iqc;

        List<IQ_Content__c> carouselArticles = BrokerIqCarouselController.getCarouselArticles();

        System.assertEquals(1, carouselArticles.size());
        System.assertEquals(iqc.Id, carouselArticles[0].Id);
    }
}