({
	showServerErrors : function(component, event, helper) {
		var params = event.getParam('arguments');
		var messages = component.get('v.messages');
		if(!$A.util.isEmpty(params.errors)) {
			params.errors.forEach(function(thisError) {

                if(!$A.util.isEmpty(thisError.message)) {
                    messages.push({message: thisError.message, alertType: 'Error'});
                }

                if(!$A.util.isEmpty(thisError.pageErrors)) {
                	thisError.pageErrors.forEach(function(thisPageError) {
                		messages.push({message: thisPageError.message, alertType: 'Error'});
                	});
                }

                if (!$A.util.isEmpty(thisError.fieldErrors)) {
                    for (var fieldErrorKey in thisError.fieldErrors) {
                        var theseFieldErrors = thisError.fieldErrors[fieldErrorKey];
                        theseFieldErrors.forEach(function(thisFieldError) {
                        	messages.push({message: thisFieldError.message, alertType: 'Error'});
                        });
                    }
                }

			});
		}
		component.set('v.messages', messages);
	},
	showAlert : function(component, event, helper) {
		var params = event.getParam('arguments');
		if(!$A.util.isEmpty(params.message) && !$A.util.isEmpty(params.alertType)) {
		    var messages = component.get('v.messages');
		    messages.push({message: params.message, alertType: params.alertType});
		    component.set('v.messages', messages);
        }
    },
    clear : function(component, event, helper) {
        component.set('v.messages', []);
    }
})