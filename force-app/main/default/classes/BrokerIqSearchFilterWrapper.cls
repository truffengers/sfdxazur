public class BrokerIqSearchFilterWrapper {
	@AuraEnabled public String  label {get; set;}
	@AuraEnabled public String  value {get; set;}
    @AuraEnabled public Integer count {get; set;}
    
    public BrokerIqSearchFilterWrapper(String label, String value, Integer count) {
        this.label = label;
        this.value = value;
        this.count = count;
    }
}