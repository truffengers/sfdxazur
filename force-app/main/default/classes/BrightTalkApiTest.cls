@istest
public class BrightTalkApiTest {

    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings;
    
    static {
        settings = tog.getBrightTalkApiSettings();
    }
    
    @istest
    public static void getRealmToken() {
        Contact c = tog.getUserContact();
        User u = [SELECT Id, TimeZoneSidKey, UserType, FirstName, LastName, Email FROM User WHERE Id = :UserInfo.getUserId()];
        
        BrightTalkApi api = new BrightTalkApi();
        
        String realmToken = api.getRealmToken(c, u);
        List<String> realmTokenSplit = realmToken.split(':');
        
        System.assertEquals(settings.Realm_ID__c, realmTokenSplit[0]);
        System.assertEquals(api.getEncryptedUserToken(c, u), EncodingUtil.base64Decode(realmTokenSplit[1]));
    }
    
    @istest
    public static void getEncryptedUserToken() {
        Contact c = tog.getUserContact();
        User u = [SELECT Id, TimeZoneSidKey, UserType, FirstName, LastName, Email FROM User WHERE Id = :UserInfo.getUserId()];
        
        BrightTalkApi api = new BrightTalkApi();
        
        Blob encryptedUserToken = api.getEncryptedUserToken(c, u);

        System.assertEquals(api.getUserDocument(c, u), Crypto.decrypt('AES128', 
                       EncodingUtil.convertFromHex(settings.Realm_Encryption_Key__c), 
                       EncodingUtil.convertFromHex(settings.Realm_Initialisation_Vector__c),
                       encryptedUserToken).toString());
    }
    
    @istest
    public static void getUserDocument() {
        Contact c = tog.getUserContact();
        User u = [SELECT Id, TimeZoneSidKey, UserType, FirstName, LastName, Email FROM User WHERE Id = :UserInfo.getUserId()];
        
        BrightTalkApi api = new BrightTalkApi();
        
        String userDoc = api.getUserDocument(c, u);

        System.assertEquals('<?xml version="1.0" encoding="utf-8"?><user>'
                            + '<realmUserId>' + c.Id + '</realmUserId>'
                            + '<firstName>' + c.FirstName + '</firstName>'
                            + '<lastName>' + c.LastName + '</lastName>'
                            + '<email>' + (u.Email == null ? '' : u.Email) + '</email>'
                            + '<timeZone>' + u.TimeZoneSidKey + '</timeZone>'
                            + '<companyName>' + (c.Account.Name == null ? '' : c.Account.Name) + '</companyName></user>', userDoc);
    }

    @istest
    public static void getActivities() {
        BrightTalkApi api = new BrightTalkApi();

        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
        List<BrightTalkSubscriberWebcastActivity> activities = api.getActivities(null);
        Test.stopTest();

        System.assertEquals(8, activities.size());
        System.assertEquals(mock.responseParams.get('webcast_id'), activities[7].getWebcastId());
        System.assertEquals(mock.responseParams.get('realm_user_id'), activities[7].getUserRealmId());
        System.assertEquals(Integer.valueOf(mock.responseParams.get('total_viewings')), activities[7].getTotalViewings());
        DateTime lastUpdated = (DateTime)(new XmlRepresentableMap.DateTimeParser()).convert(mock.responseParams.get('last_updated'));
        System.assertEquals(lastUpdated,  activities[7].getLastUpdated());
    }
    
    @istest
    public static void getWebcastsFromFeed() {
        BrightTalkApi api = new BrightTalkApi();

        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

        Nebula_API.NebulaApi.setMock(mock);

        Test.startTest();
        List<BrightTALK__Webcast__c> webcasts = api.getWebcastsFromFeed();
        Test.stopTest();

        System.assertEquals(4, webcasts.size());
        System.assertEquals(Decimal.valueOf(mock.responseParams.get('webcast_id')), webcasts[1].BrightTALK__Webcast_Id__c);
        System.assertEquals(Integer.valueOf(mock.responseParams.get('webcast_duration')), webcasts[1].Duration_s__c);
        System.assertEquals(mock.responseParams.get('webcast_thumbnail'), webcasts[1].Preview_Thumbnail__c);
        System.assertEquals(mock.responseParams.get('webcast_preview'), webcasts[1].Preview_Image__c);
        System.assertEquals(mock.responseParams.get('webcast_status'), webcasts[1].Status__c);
    }
}