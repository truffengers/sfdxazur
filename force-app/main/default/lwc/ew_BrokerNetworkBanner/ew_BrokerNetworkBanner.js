/**
 * Created by andrewxu on 22/05/2020.
 */

import {LightningElement, track} from 'lwc';
import BrokerNetworkBanner from '@salesforce/resourceUrl/BrokerNetworkBanner';

export default class EwBrokerNetworkBanner extends LightningElement {
    @track imgUrl = BrokerNetworkBanner;
}