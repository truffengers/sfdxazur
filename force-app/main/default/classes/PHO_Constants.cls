/**
* @name: PHO_Constants
* @description: This class contains constants values for Phoenix Rising project related functionalities.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 18/12/2019
* @lastChangedBy: Antigoni D'Mello      06/04/2020    PR-27: Added relevant Delta fields
* @lastChangedBy: Antigoni D'Mello      12/08/2020    PR-94: Added new Carrier Line field
* @lastChangedBy: Antigoni D'Mello      25/08/2020    PR-107: Added more phoenix staging fields
* @lastChangedBy: Antigoni D'Mello      21/01/2021    PR-113: Added extra phoenix staging field
*/
public class PHO_Constants {

    // general
    public static final Id DUMMY_ACCOUNT_ID = IBA_IBACustomSettings__c.getOrgDefaults().IBA_DummyAccount__c;

    // Policy (Asset)
    public static final String POLICY_STATUS_ACTIVE = 'Active';
    public static final String POLICY_STATUS_INACTIVE = 'Non Active';
    public static final String POLICY_ORIGIN_PHOENIX = 'Phoenix';
    public static final String POLICY_RT_HIGH_NET_WORTH = 'High_Net_Worth';

    // Policy Coverage (vlocity_ins__AssetCoverage__c)
    public static final String POLICY_COVERAGE_STATUS_ACTIVE = 'Active';
    public static final String POLICY_COVERAGE_STATUS_INACTIVE = 'Non Active';

    // PHO_PhoenixStaging__c
    public static final String STAGING_STATUS_READY = 'Ready to Process';
    public static final String STAGING_PAYMENT_METHOD_D = 'D';

    public static final List<String> STAGING_RELEVANT_FIELDS;

    public static final List<String> STAGING_RELEVANT_FIELDS_SECONDARY;

    public static final List<String> STAGING_RELEVANT_FIELDS_COMMON = new List<String>{
            'PHO_AzurPolicyNumber__c',
            'PHO_EffectiveDate__c',
            'PHO_AccountingBroker__c',
            'PHO_MGACommissionRate__c',
            'PHO_BrokerCommissionRate__c',
            'PHO_CarrierAccount__c',
            'PHO_ForeignTaxRate__c',
            'PHO_InsurancePremiumTaxRate__c',
            'PHO_InsuredName__c',
            'PHO_MGAAccount__c',
            'PHO_CurrencyType__c',
            'PHO_ExpiryDate__c',
            'PHO_InceptionDate__c',
            'PHO_ProducingBroker__c',
            'PHO_Commission__c',
            'PHO_EventNumber__c',
            'PHO_Product__c',
            'PHO_ProductName__c',
            'PHO_BusinessType__c',
            'PHO_ClassofBusiness__c',
            'PHO_EntryDate__c',
            'PHO_MGACommission__c',
            'PHO_BrokerCommission__c',
            'PHO_ForeignTax__c',
            'PHO_Premium__c',
            'PHO_InsurancePremiumTax__c',
            'PHO_CarrierFee__c',
            'PHO_Policy__c',
            'PHO_PolicyCoverage__c',
            'PHO_PolicyTransaction__c',
            'PHO_PolicyTransactionLine__c',
            'PHO_Product__r.IBA_AIGProduct__c',
            'PHO_Product__r.IBA_ClassofBusiness__c',
            'PHO_TransactionType__c',
            'PHO_PaymentReference__c',
            'PHO_PolicyTransactionType__c',
            'PHO_Period__c',
            'PHO_ReversalType__c',
            'PHO_AccountingCompanyName__c'
    };

    public static final Set<String> STAGING_WINDBACK_ENTRY_TYPES = new Set<String>{
            'RS',
            'WB',
            'LP'
    };

    public static final Set<String> PT_WINDBACK_STATUS_SET = new Set<String>{
            'Pending',
            'No Data Processed'
    };

    static {
        STAGING_RELEVANT_FIELDS = new List<String> {
                'PHO_PolicySeed__c',
                'PHO_PolicyCoverageSeed__c',
                'PHO_PolicyTransactionSeed__c',
                'PHO_PolicyCarrierLineSeed__c',
                'PHO_PolicyTransactionLineSeed__c',
                'PHO_EntryType__c',
                'Name'
        };
        STAGING_RELEVANT_FIELDS.addAll(STAGING_RELEVANT_FIELDS_COMMON);

        STAGING_RELEVANT_FIELDS_SECONDARY  = new List<String> {
                'PHO_PolicySeedSecondary__c',
                'PHO_PolicyCoverageSeedSecondary__c',
                'PHO_PolicyTransactionSeedSecondary__c',
                'PHO_PolicyCarrierLineSeedSecondary__c',
                'PHO_PolicyTransactionLineSeedSecondary__c',
                'Name'
        };
        STAGING_RELEVANT_FIELDS_SECONDARY.addAll(STAGING_RELEVANT_FIELDS_COMMON);
    }

    public static final List<String> DELTA_POLICY_TRANSACTION_FIELDS = new List<String> {
            'IBA_OriginalPolicy__c',
            'IBA_AIGClassofBusiness__c',
            'IBA_AccountingBroker__c',
            'IBA_ActivePolicy__c',
            'IBA_PolicyNumber__c',
            'IBA_BusinessType__c',
            'IBA_AIGPolicyNumber__c',
            'IBA_TransactionDate__c',
            'IBA_EffectiveDate__c',
            'IBA_TransactionCurrency__c',
            'IBA_ClassofBusiness__c',
            'PHO_PolicyTransactionType__c',
            'IBA_TransactionType__c',
            'IBA_InsuredName__c',
            'Reversal_Type__c',
            'PHO_Total2019DeltaGWP__c',
            'PHO_DeltaProcessed__c'
    };

    public static final List<String> DELTA_POLICY_TRANSACTION_LINE_FIELDS = new List<String> {
            'IBA_Product__c',
            'IBA_PolicyCarrierLine__c',
            'IBA_PolicyTransaction__c',
            'PHO_2019DeltaMGA__c',
            'PHO_2019DeltaBrokerCommission__c',
            'PHO_2019DeltaCarrierFee__c',
            'PHO_2019DeltaForeignTax__c',
            'PHO_2019DeltaGrossWritten__c',
            'PHO_2019DeltaInsurancePremium__c',
            'PHO_OngoingDeltaMGA__c',
            'PHO_OngoingDeltaBrokerCommission__c',
            'PHO_OngoingDeltaCarrierFee__c',
            'PHO_OngoingDeltaForeignTax__c',
            'PHO_OngoingDeltaGrossWritten__c',
            'PHO_OngoingDeltaInsurancePremium__c'
    };

    public static final List<String> DELTA_POLICY_CARRIER_LINE_FIELDS = new List<String> {
            'IBA_CarrierAccount__c',
            'IBA_PolicyTransaction__c',
            'IBA_OwedtoCarrier__c'
    };
}