/**
* @name: IBA_ExchangeRateTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaExchangeRate__c trigger functionality.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 03/01/2020
*/
public with sharing class IBA_ExchangeRateTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_ExchangeRateTriggerHandler.IsDisabled != null ? IBA_ExchangeRateTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        IBA_ExchangeRateService.populateExchangeRateOnPolicyTransactions(newItems.keySet());
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}