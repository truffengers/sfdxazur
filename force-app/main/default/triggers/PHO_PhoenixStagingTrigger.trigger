/**
* @name: PHO_PhoenixStagingTrigger
* @description: Trigger for the custom object PHO_PhoenixStaging__c
*
*/

trigger PHO_PhoenixStagingTrigger on PHO_PhoenixStaging__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new PHO_PhoenixStagingTriggerHandler());
}