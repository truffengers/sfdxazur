import { LightningElement, api, track } from 'lwc';

export default class Ix_SearchBar extends LightningElement {
    @track displaySearchResults = [];
    @track currentlySelectedIndex = null;
    @track displaySearchInstruction = !this.displaySearchResults.length;

    @api searchBoxValue;
    @api handleSearchInputChange;
    @api handleOptionClick;
    @api
    get searchResults() {
        return this._searchResults;
    }
    // Sort the search results before they are rendered into the dropdown
    set searchResults(value) {
        this.displaySearchResults = this.sortByIndexOf(
            this.searchBoxValue,
            value
        ).map(searchResultItem => {
            // Create a string for the mini breadcrumbs for display. Currently
            // passing the full breadcrumbs objects into this file, but only
            // using the _name in anticipation of having to have them be
            // interactive later.
            let _miniBreadcrumbsArray = [];
            searchResultItem.miniBreadcrumbs.forEach(breadcrumbLayer => {
                _miniBreadcrumbsArray.push(breadcrumbLayer._name);
            });

            return {
                ...searchResultItem,
                _miniBreadcrumbsArray
            }
        });
    }

    renderedCallback() {
        this.displaySearchInstruction = !this.displaySearchResults.length;
    }

    connectedCallback() {
        window.addEventListener('keydown', (event) => {
            if (!this.displaySearchResults.length) {
                return;
            }

            /*
            Implement logic on key presses for selecting options from the
            options menu
            */
            switch(event.code) {
                case 'ArrowUp':
                    if (this.currentlySelectedIndex === null) {
                        this.currentlySelectedIndex
                            = this.displaySearchResults.length - 1;
                    } else if (this.currentlySelectedIndex > 0) {
                        this.currentlySelectedIndex--;
                    } else {
                        this.currentlySelectedIndex 
                            = this.displaySearchResults.length - 1;
                    }
                    break;
                case 'ArrowDown':
                    if (this.currentlySelectedIndex === null) {
                        this.currentlySelectedIndex = 0;
                    } else if (this.currentlySelectedIndex
                        < this.displaySearchResults.length-1) {
                        this.currentlySelectedIndex++;
                    } else {
                        this.currentlySelectedIndex = 0;
                    }
                    break;
                case 'Enter':
                    this.enterEvent();
                    break;
                default:
                    break;
            }

            if (this.currentlySelectedIndex !== null) {
                this.displaySearchResults
                    = this.setCurrentlySelectedDropdownItem(
                    this.displaySearchResults,
                    this.currentlySelectedIndex
                )
            }
        });
    }

    /*
    When the enter key is pressed after the user has used the up and down arrows
    to select an option.
    */
    enterEvent() {
        if (this.currentlySelectedIndex == null) {
            return;
        }
        const itemId = this.displaySearchResults[this.currentlySelectedIndex]
            ._id;
        const enterEvent = new CustomEvent('optionclick', {
            detail: {
                value: itemId
            }
        });
        this.dispatchEvent(enterEvent);
        this.currentlySelectedIndex = null;
    }

    /*
    Change the currently selected item to the one indicated by the selectedIndex
    */
    setCurrentlySelectedDropdownItem(searchResults, selectedIndex) {
        const results = searchResults.map(item => {
            return { ...item, _currentlySelected: false };
        });
        if (selectedIndex + 1 <= results.length) {
            results[selectedIndex]._currentlySelected = true;
        }
        return results;
    }
    
    // Dispatch an event to the parent on change of the query in the search box
    dispatchInputChangeEvent(event) {
        this.searchBoxValue = event.target.value;
        const inputChangeEvent = new CustomEvent('searchinputchange', {
            detail: {
                value: this.searchBoxValue
            }
        });
        this.dispatchEvent(inputChangeEvent);
    }

    // Dispatch an event to the parent on click of an option in the dropdown
    dispatchOptionClickEvent(event) {
        const optionClickEvent = new CustomEvent('optionclick', {
            detail: {
                value: event.target.dataset.value
            }
        });
        this.dispatchEvent(optionClickEvent);
    }

    handleRefresh() {
        const refreshClickEvent = new CustomEvent('refresh');
        this.dispatchEvent(refreshClickEvent);
    }

    /*
    Input: 'abc', [{ _name: 'xyz' }, { _name: 'abc' }, { _name: 'qrwxyzabc' }]
    Output: [{ _name: 'abc' }, { _name: 'xyzabc' }, { _name: 'qrwxyzabc' }]

    The _name attribute of each object in the array must include the query as a
    substring ignoring case.
    */
    sortByIndexOf(query, paramArray) {
        /*
        Somehow, if trying to operate on a proxy, this error is returned:
        "['set' on proxy: trap returned falsish for property '0']"
        */
        return JSON.parse(JSON.stringify(paramArray)).sort((a, b) => {
            const [i , j] = [a._name.toLowerCase(), b._name.toLowerCase()];
            if (i.indexOf(query.trim()) >= 0 && j.indexOf(query.trim()) >= 0) {
                return i.indexOf(query.trim()) > j.indexOf(query.trim())
                    ? 1 : -1;
            } else if (i.indexOf(query.trim()) >= 0) {
                return -1;
            }
            return 1;
        });
    }

    get boxPopulated() {
        return this.searchBoxValue ? 'box-populated' : '';
    }
}