public with sharing class BrokerIqHistoriesController extends PortalController {

    public class CpdData {
        @AuraEnabled public BrightTALK__Webcast__c webcast;
        @AuraEnabled public CPD_Assessment__c  assessment;
        @AuraEnabled public CPD_Test__c test;
        
        public CpdData(BrightTALK__Webcast__c webcast, CPD_Assessment__c  assessment, CPD_Test__c test) {
            this.webcast = webcast;
            this.assessment = assessment;
            this.test = test;
        }
    }
    
    public class TopicData {
        @AuraEnabled public ConnectAPI.ManagedTopic primaryTopic;
        @AuraEnabled public String cpdHoursCompleted;
        @AuraEnabled public Integer cpdCompletedInMinutes;
        @AuraEnabled public List<CpdData> historyData;
        
        public TopicData(ConnectAPI.ManagedTopic primaryTopic) {
            this.primaryTopic = primaryTopic;
            this.historyData = new List<CpdData>();
            this.cpdCompletedInMinutes = 0;
        }
    }
    
    @AuraEnabled
    public static Map<Integer, List<TopicData>> getYearToPrimaryTopicToData() {
    	BrokerIqHistoriesController controller = new BrokerIqHistoriesController();

    	return controller.yearToPrimaryTopicToData;
    }

    public Map<Integer, List<TopicData>> yearToPrimaryTopicToData;

    // Can't query these in a unit test, so inject mocks here
    @TestVisible
    private static List<ConnectAPI.ManagedTopic> navigationalTopicMocks;

    private List<ConnectAPI.ManagedTopic> navigationalTopics;

    private TopicData lazyGetTopicData(Integer year, BrightTalkSubscriberWebcastActivity thisWebcastActivity) {
        List<TopicData> primaryTopicToData = yearToPrimaryTopicToData.get(year);

        if(primaryTopicToData == null) {
            primaryTopicToData = new List<TopicData>();

            for(ConnectAPI.ManagedTopic thisTopic : navigationalTopics) {
                primaryTopicToData.add(new TopicData(thisTopic));
            }

            yearToPrimaryTopicToData.put(year, primaryTopicToData);
        }

        TopicData rval = null;

        for(TopicData thisTopicData : primaryTopicToData) {
            if(thisTopicData.primaryTopic.topic.name.equalsIgnoreCase(thisWebcastActivity.webcast.IQ_Content__r.Primary_Topic__c)) {
                rval = thisTopicData;
            }
        }

        return rval;
    }

    public BrokerIqHistoriesController() {

        yearToPrimaryTopicToData = new Map<Integer,List<TopicData>>();
        if(Test.isRunningTest()) {
            navigationalTopics = navigationalTopicMocks;
        } else {
            navigationalTopics = BrokerIqNavTopicsProviderController.getTopics();
        }

        List<BrightTalkSubscriberWebcastActivity> thisUserWebcasts = new List<BrightTalkSubscriberWebcastActivity>();
        Set<Decimal> webcastIds = getUserWebcastIds(thisUserWebcasts);

        List<BrightTalkSubscriberWebcastActivity> viewedWebcasts = getViewedWebcasts(webcastIds, thisUserWebcasts);

        Nebula_Tools.sObjectIndex assessmentsByWebcast = new Nebula_Tools.sObjectIndex(
            'CPD_Test__r.IQ_Content__r.BrightTALK_Webcast__c',
            [SELECT CPD_Test__r.IQ_Content__r.BrightTALK_Webcast__c, Passed__c, Date_Time_Taken__c,
             CPD_Test__r.IQ_Content__r.CPD_Hours_In_Words__c, CPD_Test__r.IQ_Content__r.CPD_Time_minutes__c,
                    CPD_Test__r.IQ_Content__r.CPD_Hours_With_Colon__c, LastModifiedDate
             FROM CPD_Assessment__c
             WHERE Contact__c  = :thisUserAsContact.Id
             ORDER BY Passed__c DESC]
        );

        Nebula_Tools.sObjectIndex testsByWebcast = new Nebula_Tools.sObjectIndex(
                'BrightTALK_Webcast__c',
                [SELECT Id,
                BrightTALK_Webcast__c, BrightTALK_Webcast__r.BrightTALK__Webcast_Id__c,
                (select id, Status__c from Tests__r where Status__c = 'Published')
                FROM IQ_Content__c
                WHERE BrightTALK_Webcast__r.BrightTALK__Webcast_Id__c IN :webcastIds
                AND BrightTALK_Webcast__r.BrightTALK__Start_Date__c <= :DateTime.now()
                and CPD_Time_minutes__c != 0 ]
        );

        for(BrightTalkSubscriberWebcastActivity thisWebcastActivity : viewedWebcasts) {
            Integer thisYear = thisWebcastActivity.getLastUpdated().year();
            CPD_Assessment__c thisAssessment = (CPD_Assessment__c)assessmentsByWebcast.get(thisWebcastActivity.webcast.Id);
            IQ_Content__c thisContent = (IQ_Content__c)testsByWebcast.get(thisWebcastActivity.webcast.Id);
            CPD_Test__c thisTest = thisContent.Tests__r.isEmpty()? null : thisContent.Tests__r.get(0);
            if(thisAssessment != null) {
                thisYear = Math.max(
                        thisYear,
                        thisAssessment.Date_Time_Taken__c == null ? thisAssessment.LastModifiedDate.year() :  thisAssessment.Date_Time_Taken__c.year());
            }

            TopicData thisData = lazyGetTopicData(thisYear, thisWebcastActivity);
            if(thisData != null) {
                if (thisAssessment != null && thisAssessment.Passed__c && thisAssessment.CPD_Test__r.IQ_Content__r.CPD_Time_minutes__c != null) {
                    thisData.cpdCompletedInMinutes += (Integer) thisAssessment.CPD_Test__r.IQ_Content__r.CPD_Time_minutes__c;
                }
                thisData.historyData.add(new CpdData(thisWebcastActivity.webcast, thisAssessment, thisTest));
            } else {
                System.debug('[BrokerIqHistoriesController] No match for ' + thisWebcastActivity.webcast.Id);
            }
        }
        
        for(Integer thisYear : yearToPrimaryTopicToData.keySet()) {
            for (TopicData thisData : yearToPrimaryTopicToData.get(thisYear)) {
                IQ_Content__c formatter = new IQ_Content__c(CPD_Time_minutes__c = thisData.cpdCompletedInMinutes);
                formatter.recalculateFormulas();
                thisData.cpdHoursCompleted = formatter.CPD_Hours_With_Colon__c;
            }
        }
    }


    private Set<Decimal> getUserWebcastIds(List<BrightTalkSubscriberWebcastActivity> thisUserWebcasts){

        BrightTalkApi api = new BrightTalkApi();
        BrightTALK_API_Settings__c settings = api.getSettings();

        List<BrightTalkSubscriberWebcastActivity> sfWebcasts = api.getActivitiesFromSf(thisUserAsContact.Id);

        DateTime apiSince = null;
        if(!sfWebcasts.isEmpty()) {
            apiSince = sfWebcasts[sfWebcasts.size()-1].getLastUpdated();
        }
        List<BrightTalkSubscriberWebcastActivity> allWebcasts = api.getActivities(apiSince);
        allWebcasts.addAll(sfWebcasts);

        Set<Decimal> webcastIds = new Set<Decimal>();

        for(Integer i = allWebcasts.size() - 1; i >= 0; i--) {
            BrightTalkSubscriberWebcastActivity wc = allWebcasts[i];
            Id thisUserId = wc.getUserRealmId();
            if(thisUserId == thisUserAsContact.Id) {
                webcastIds.add(Decimal.ValueOf(wc.getWebcastId()));
                thisUserWebcasts.add(wc);
            }
        }
        return webcastIds;
    }

    private List<BrightTalkSubscriberWebcastActivity> getViewedWebcasts(Set<Decimal> webcastIds, List<BrightTalkSubscriberWebcastActivity> thisUserWebcasts) {

        Nebula_Tools.sObjectIndex webcastsById = new Nebula_Tools.sObjectIndex(
            'BrightTALK__Webcast_Id__c',
            [SELECT Id, BrightTALK__Webcast_Id__c, BrightTALK__Name__c,
             IQ_Content__r.Primary_Topic__c, IQ_Content__c
             FROM BrightTALK__Webcast__c
             WHERE BrightTALK__Webcast_Id__c IN :webcastIds
             AND BrightTALK__Start_Date__c <= :DateTime.now()
                    AND IQ_Content__c != null
                    AND IQ_Content__r.CPD_Time_minutes__c != 0
            ]);

        List<BrightTalkSubscriberWebcastActivity> rval = new List<BrightTalkSubscriberWebcastActivity>();
        for(BrightTalkSubscriberWebcastActivity wc : thisUserWebcasts) {
            BrightTALK__Webcast__c thisDatabaseWc = (BrightTALK__Webcast__c)webcastsById.get(wc.getWebcastId());
            if(thisDatabaseWc != null) {
                wc.webcast = thisDatabaseWc;
                rval.add(wc);
            }
        }
		return rval;        
    }
    
}