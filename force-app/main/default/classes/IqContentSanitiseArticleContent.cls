global class IqContentSanitiseArticleContent implements Nebula_Tools.AfterInsertI, Nebula_Tools.AfterUpdateI {

	global void handleAfterInsert(List<IQ_Content__c> newList) {
		handleAfterUpdate(null, newList);
	}

	global void handleAfterUpdate(List<IQ_Content__c> oldList, List<IQ_Content__c> newList) {
		List<IQ_Content__c> toUpdate = new List<IQ_Content__c>();

		for(Integer i=0; i < newList.size(); i++) {
			IQ_Content__c newContent = newList[i];
			if(newContent.Content__c != null && (oldList == null || newContent.Content__c != oldList[i].Content__c)) {
				// Remove all attributes from tags, other than <a> tags
				String sanitisedContent = newContent.Content__c.replaceAll('<(([Aa]\\w+)|(([^Aa\\W>])\\w*)) [^>]+>', '<$1>');
				if(sanitisedContent != newContent.Content__c) {
					toUpdate.add(new IQ_Content__c(Id = newContent.Id, Content__c = sanitisedContent));
				}
			}
		}

		if(!toUpdate.isEmpty()) {
			update toUpdate;
		}

	}

}