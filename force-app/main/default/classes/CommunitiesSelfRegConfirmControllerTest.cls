/**
* @name: CommunitiesSelfRegConfirmControllerTest
* @description: An apex page controller that takes the user to the right start page based on credentials
*               or lack thereof
* @author: Azur Group
* @date: 2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
*/
@IsTest public with sharing class CommunitiesSelfRegConfirmControllerTest {
    @IsTest
    public static void testCommunitiesSelfRegConfirmController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesSelfRegConfirmController controller = new CommunitiesSelfRegConfirmController();
      }    
}