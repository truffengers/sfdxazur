/*
* @name: BrokerIqRegistrationFormController
* @description: BrokerIqRegistrationForm apex controller
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 14/01/2018
* @modifiedBy: Ignacio Sarmiento    16/01/2018  Update validation messages
* @modifiedBy: Ignacio Sarmiento    23/04/2018  BIQ-4 - Add functionality if an account already exists, add the contact to the existing account.
                                                        Extend letter limit in the company name
* @modifiedBy: Rajni Bajpai         30/03/2021 		EWH-2063- added Country field
*/
public without sharing class BrokerIqRegistrationFormController {
    //Constant
    private static final Integer LIMIT_LETTERS_FISTNAME = 15;
    private static final Integer LIMIT_LETTERS_LASTNAME = 30;
    private static final Integer LIMIT_LETTERS_JOBTITLE = 20;
    private static final Integer LIMIT_LETTERS_COMPANY = 40;
    private static final Integer LIMIT_LETTERS_PHONENUMBER = 14;
    //Propeties
    private static String firstName;
    private static String lastName;
    private static String jobTitle;
    private static String company;
    private static String email;
    private static String phone;
    private static String country;
    private static Account existingCompany;

    @AuraEnabled
    public static String registerBrokerIq2User(String firstName, String lastName, String jobTitle, String company, String email, String phone, Boolean opt_in, string countryName){
        String errors;
        try{
            errors = validateClassProperties(firstName, lastName, jobTitle, company, email, phone, opt_in,countryName);
            if(String.isBlank(errors)){
                setClassProperties(firstName, lastName, jobTitle, company, email, phone,countryName);
                errors = insertBrokerIqUser();
            }
        }catch(Exception e){
            errors = e.getMessage();
            return errors;
        }
        return errors;
    }
    
    private static String validateClassProperties(String firstName, String lastName, String jobTitle, String company, String email, String phone, Boolean opt_in,String countryName){
        String errors;
        if (isNotValidFirstName(firstName)){
            errors = getValidationErrorMessageForString('First Name', LIMIT_LETTERS_FISTNAME);
        }else if(isNotValidLastName(lastName)){
            errors = getValidationErrorMessageForString('Last Name', LIMIT_LETTERS_LASTNAME);
        }else if(isNotValidJobTitle(jobTitle)){
            errors = getValidationErrorMessageForString('Job Title', LIMIT_LETTERS_JOBTITLE);
        }else if(isNotValidCompany(company)){
            errors = getValidationErrorMessageForString('Company', LIMIT_LETTERS_COMPANY);
        }else if(isnotValidCountry(countryName)){
            errors = getValidationErrorMessageForCountryName();
        }else if(isNotValidEmail(email)){
            errors = getValidationErrorMessageForEmail();
        } else if(isExistingEmail(email)){
            errors = getValidationErrorMessageForExistingEmail();
        }else if(isNotValidPhoneNumber(phone)){
            errors = getValidationErrorMessageForPhoneNumber(LIMIT_LETTERS_PHONENUMBER);
        }else if(isOptInUnchecked(opt_in)){
            errors = getValidationErrorMessageForOptIn();
        }
        return errors;
    }

    private static Boolean isNotValidFirstName(String firstName){
        return isStringNullOrLengthHigherThan(firstName, LIMIT_LETTERS_FISTNAME);
    }

    private static Boolean isNotValidLastName(String lastName){
        return isStringNullOrLengthHigherThan(lastName, LIMIT_LETTERS_LASTNAME);
    }

    private static Boolean isNotValidJobTitle(String jobTitle){
        return isStringNullOrLengthHigherThan(jobTitle, LIMIT_LETTERS_JOBTITLE);
    }

    private static Boolean isNotValidCompany(String company){
        return isStringNullOrLengthHigherThan(company, LIMIT_LETTERS_COMPANY);
    }

    private static Boolean isNotValidPhoneNumber(String phoneNumber){
        return isStringNullOrLengthHigherThan(phoneNumber, LIMIT_LETTERS_PHONENUMBER) || !phoneNumber.isNumeric();
    }

    private static Boolean isStringNullOrLengthHigherThan(String stringParam, Integer maxStringLenght){
        return String.isBlank(stringParam) || stringParam.length() > maxStringLenght;
    }

    public static Boolean isNotValidEmail(String email) {
        Boolean result = false;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        if (!MyMatcher.matches()){
            result = true;
        }
        return result;
    }

    private static Boolean isnotValidCountry(String countryName){
        Boolean result=false;
        if(countryName=='Please Select' || String.isBlank(countryName))
            result=true;
        
        return result;
    }
    public static Boolean isExistingEmail(String email) {
        return [SELECT count() FROM User WHERE Email =: email] > 0;
    }

    public static Boolean isOptInUnchecked(Boolean opt_in) {
        return opt_in == false;
    }

    private static String getValidationErrorMessageForString(String fieldName, Integer maxStringLenght){
        return 'Whoops! Looks like there is an error! Please enter a ' + fieldName + ' with less than ' + maxStringLenght + ' letters';
    }

    private static String getValidationErrorMessageForPhoneNumber(Integer maxPhoneNumberLenght){
        return 'Whoops! Looks like there is an error! Please enter a phone number in a numeric format and less than ' + maxPhoneNumberLenght + ' numbers';
    }

    private static String getValidationErrorMessageForEmail(){
        return 'Whoops! Looks like there is an error! the email format is incorrect';
    }

    private static String getValidationErrorMessageForExistingEmail(){
        return 'Sorry, this email already exists. Please reset your password or use a different email address.<br/><br/>If you have a Broker Hub account, please contact help@azuruw.com and we will provide you with access to Broker iQ!';
    }

    private static String getValidationErrorMessageForOptIn(){
        return 'Please confirm Terms and Conditions';
    }

    private static String getValidationErrorMessageForCountryName(){
        return 'Whoops! Looks like there is an error! Please select a country or enter the other Country Name';
    }
    
    private static void setClassProperties(String firstNameParam, String lastNameParam, String jobTitleParam, String companyParam, String emailParam, String phoneParam, string countryName){
        firstName = firstNameParam;
        lastName = lastNameParam;
        jobTitle = jobTitleParam;
        company = companyParam;
        email = emailParam;
        phone = phoneParam;
        existingCompany = getExistingCompanyWithCurrentCompanyName();
        country=countryName;
    }

    private static Account getExistingCompanyWithCurrentCompanyName(){
        list<Account> AccountQuery = [SELECT Id FROM Account WHERE Name =: getCompanyName() AND isPersonAccount = false LIMIT 1];
        return AccountQuery.size() > 0? AccountQuery[0]: new Account();
    }

    private static String getCompanyName(){
        return company;
    }

    private static String insertBrokerIqUser(){
        String result;
        Savepoint sp = Database.setSavepoint();
        try{
            //Set Agency/Brokerage Account
            Account agencyBrokerageAccount;
            if(isExistingCompany()){
                agencyBrokerageAccount = existingCompany;
            }else{
                //Insert Agency/Brokerage Account
                agencyBrokerageAccount = createAgencyBrokerageAccount();
                Database.insert(agencyBrokerageAccount, true);
            }

            //Insert contact related to the account
            Contact contactToInsert = createContact(agencyBrokerageAccount.Id);
            Database.insert(contactToInsert, true);
        }catch(Exception e){
            Database.rollback(sp);
            result = e.getMessage();
            return result;
        }
        return result;
    }

    private static Boolean isExistingCompany(){
        return existingCompany.Id != null;
    }

    private static Account createAgencyBrokerageAccount(){
        Account result = new Account(
            Name = getCompanyName(),
            RecordTypeId = getAgencyBrokerageRecordTypeId(),
            BillingCountry=country
        );
        return result;
    }

    private static Id getAgencyBrokerageRecordTypeId(){
        return  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency/Brokerage').getRecordTypeId();
    }

    private static Contact createContact(Id accountId){
        Contact result = new Contact(
                AccountId = accountId,
                FirstName = firstName,
                LastName = lastName,
                Title = jobTitle,
                Email = email,
                Phone = phone,
                Is_BIQ_self_registration__c = true
        );
        return result;
    }
}