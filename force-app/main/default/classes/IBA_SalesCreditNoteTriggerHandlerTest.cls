/**
* @name: IBA_SalesCreditNoteTriggerHandlerTest
* @description: Test class for IBA_SalesCreditNoteTriggerHandler
*
*/
@IsTest
private class IBA_SalesCreditNoteTriggerHandlerTest {

    private static final String COMPANY_NAME = 'ApexTestCompany';
    private static final String CURRENCY_USD = 'USD';
    private static final Decimal MONEY = 100.50;
    private static final Decimal MONEY2 = 80.01;

    @testSetup
    static void createTestRecords() {
        IBA_TestDataFactory.FinancialForceSetup ffs = IBA_TestDataFactory.setupFinancialForce(COMPANY_NAME, CURRENCY_USD);
        Group companyGroup = ffs.companyGroup;
        c2g__codaCompany__c company = ffs.company;
    
        c2g__codaYear__c yr = IBA_TestDataFactory.createYear(true, companyGroup.Id, company.Id);
        c2g.CODAYearWebService.calculatePeriods(yr.Id);

        c2g__codaAccountingCurrency__c curr = IBA_TestDataFactory.createCurrency(true, CURRENCY_USD);

        c2g__codaGeneralLedgerAccount__c gla = IBA_TestDataFactory.createGeneralLedgerAccount(true, 'Brochures', 'uniqueCaseInsensitive24!');

        Product2 product = IBA_TestDataFactory.createProduct(false, 'Product 1', IBA_UtilConst.PRODUCT_RT_Master);
        product.IBA_AIGProduct__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
        product.IBA_AIGMinorLine__c = 'PCG Test Liability';
        product.IBA_AIGPolicyCodeNumber__c = '000000test';
        product.IsActive = true;
        product.c2g__CODASalesRevenueAccount__c = gla.Id;
        insert product;

        Account acc = IBA_TestDataFactory.createAccount(false, 'testAccOnHold', IBA_UtilConst.ACCOUNT_RT_BROKER);
        acc.c2g__CODAAccountTradingCurrency__c = CURRENCY_USD;
        acc.c2g__CODAAccountsPayableControl__c = gla.Id;
        acc.c2g__CODAAccountsReceivableControl__c = gla.Id;
        acc.c2g__CODAVATStatus__c = 'Home';
        acc.c2g__CODATaxCalculationMethod__c = 'Gross';
        acc.c2g__CODABaseDate1__c = 'Invoice Date';
        acc.c2g__CODADaysOffset1__c = 0;
        acc.c2g__CODADescription1__c = 'Test';
        acc.c2g__CODADiscount1__c = 0;
        insert acc;

        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c WHERE 
            c2g__YearName__c = :yr.Id AND c2g__PeriodNumber__c != '000' AND 
            c2g__EndDate__c > TODAY LIMIT 1];

        c2g__codaBankAccount__c bank = IBA_TestDataFactory.createBankAccount(false, 'Trust Account USD', '123456789', 'Santander', 'uniqueCaseInsensitive24!', gla.Id, curr.Id);
        insert bank;

        List<c2g__codaInvoice__c> invoicesToInsert = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c invoice = IBA_TestDataFactory.createInvoice(false, company.Id, acc.Id, period.Id, curr.Id);
        c2g__codaInvoice__c invoice2 = IBA_TestDataFactory.createInvoice(false, company.Id, acc.Id, period.Id, curr.Id);

        Asset policy = IBA_TestDataFactory.createPolicy(false, 'Policy 1', acc.Id);
        policy.IBA_ProducingBrokerAccount__c = acc.Id;
        policy.IBA_AccountingBroker__c = acc.Id;
        policy.Azur_Account__c = acc.Id;
        insert policy;

        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(true, 2, policy.Id);
        invoice.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        invoice2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        invoicesToInsert.add(invoice);
        invoicesToInsert.add(invoice2);
        insert invoicesToInsert;

        List<c2g__codaInvoiceLineItem__c> invoiceLineItemsToInsert = new List<c2g__codaInvoiceLineItem__c>();
        c2g__codaInvoiceLineItem__c invoiceLineItem = IBA_TestDataFactory.createInvoiceLineItem(false, 15, MONEY, invoice.Id, product.Id);
        c2g__codaInvoiceLineItem__c invoiceLineItem2 = IBA_TestDataFactory.createInvoiceLineItem(false, 15, MONEY2, invoice2.Id, product.Id);

        invoiceLineItemsToInsert.add(invoiceLineItem);
        invoiceLineItemsToInsert.add(invoiceLineItem2);
        insert invoiceLineItemsToInsert;

        List<c2g__codaCreditNote__c> salesCreditNotesToInsert = new List<c2g__codaCreditNote__c>();
        c2g__codaCreditNote__c salesCreditNote = IBA_TestDataFactory.createSalesCreditNote(false, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c);
        salesCreditNote.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        c2g__codaCreditNote__c salesCreditNote2 = IBA_TestDataFactory.createSalesCreditNote(false, 
        invoice2.c2g__OwnerCompany__c, 
        invoice2.c2g__Account__c, 
        invoice2.c2g__Period__c, 
        invoice2.c2g__InvoiceCurrency__c);
        salesCreditNote2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        salesCreditNotesToInsert.add(salesCreditNote);
        salesCreditNotesToInsert.add(salesCreditNote2);
        insert salesCreditNotesToInsert;

        List<c2g__codaCreditNoteLineItem__c> creditNoteLineItemToInsert = new List<c2g__codaCreditNoteLineItem__c>();
        c2g__codaCreditNoteLineItem__c creditNoteLineItem = IBA_TestDataFactory.createSalesCreditNoteLineItem(false,
            MONEY,
            invoice.c2g__OwnerCompany__c,
            salesCreditNote.Id,
            product.Id);

        c2g__codaCreditNoteLineItem__c creditNoteLineItem2 = IBA_TestDataFactory.createSalesCreditNoteLineItem(false,
            MONEY2,
            invoice2.c2g__OwnerCompany__c,
            salesCreditNote2.Id,
            product.Id);

        creditNoteLineItemToInsert.add(creditNoteLineItem);
        creditNoteLineItemToInsert.add(creditNoteLineItem2);
        insert creditNoteLineItemToInsert;

        Integer i = 0;
        for(IBA_PolicyTransaction__c policyTransaction :policyTransactions) {
            if(i == 0) {
                policyTransaction.IBA_AccountingBrokerSIN__c = invoice.Id;
                policyTransaction.IBA_AccountingBrokerSCN__c = salesCreditNote.Id;
            } else if(i == 1) {
                policyTransaction.IBA_BrokerSalesInvoice__c = invoice2.Id;
                policyTransaction.IBA_BrokerSalesCreditNote__c = salesCreditNote2.Id;
            }
            i++;
        }
        update policyTransactions;
    }

    @isTest
    static void afterUpdateTest() {
        Set<Id> salesCreditNoteIds = new Set<Id>();

        for(c2g__codaCreditNote__c salesCreditNote : [SELECT Id FROM c2g__codaCreditNote__c]) {
            salesCreditNoteIds.add(salesCreditNote.Id);
        }

        Test.startTest();
        IBA_TestDataFactory.postSalesCreditNotes(salesCreditNoteIds);
        Test.stopTest();
        IBA_PolicyTransaction__c accBrokerPT = new IBA_PolicyTransaction__c();
        IBA_PolicyTransaction__c brokerPT = new IBA_PolicyTransaction__c();
        for(IBA_PolicyTransaction__c policyTransaction :[SELECT Id, IBA_AccountingBrokerOutstanding__c, IBA_BrokerOutstanding__c,
            IBA_AccountingBrokerOverdue__c, IBA_BrokerOverdue__c, IBA_AccountingBrokerSalesInvoiceDueDate__c, IBA_BrokerSalesInvoiceDueDate__c FROM IBA_PolicyTransaction__c]) {
                if(policyTransaction.IBA_BrokerOverdue__c != null) {
                    brokerPT = policyTransaction;
                } else if(policyTransaction.IBA_AccountingBrokerOverdue__c != null) {
                    accBrokerPT = policyTransaction;
                }
        }

        System.assertEquals(-MONEY, accBrokerPT.IBA_AccountingBrokerOutstanding__c);
        System.assertEquals(-MONEY2, brokerPT.IBA_BrokerOutstanding__c);
        System.assertEquals(System.today().addDays(14), accBrokerPT.IBA_AccountingBrokerSalesInvoiceDueDate__c);
        System.assertEquals(System.today().addDays(14), brokerPT.IBA_BrokerSalesInvoiceDueDate__c);
    }
}