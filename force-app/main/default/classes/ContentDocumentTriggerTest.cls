@isTest
public with sharing class ContentDocumentTriggerTest {

    public static void setUpData(Boolean skipTheLink) {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
        );
        insert contentVersionInsert;

        ContentVersion contentVersionSelect = [
                SELECT Id, Title, ContentDocumentId
                FROM ContentVersion
                WHERE Id = :contentVersionInsert.Id
                LIMIT 1
        ];
        if (!skipTheLink) {
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = quote.Id;
            cdl.ContentDocumentId = contentVersionSelect.ContentDocumentId;
            cdl.shareType = 'V';
            insert cdl;
        }
    }

    @isTest
    public static void testBehaviour_updateContentDocumentLinksVisibilityForQuotes() {
        setUpData(true);
        ContentDocument doc = [
                SELECT Id
                FROM ContentDocument
                LIMIT 1
        ];

        Quote quote = [SELECT Id FROM Quote LIMIT 1];

        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = quote.Id;
        cdl.ContentDocumentId = doc.Id;
        cdl.ShareType = 'V';
        cdl.Visibility = 'InternalUsers';
        insert cdl;

        Test.startTest();
        EWContentDocumentHelper.updateContentDocumentLinksVisibilityForQuotes(new Set<Id>{
                doc.Id
        });
        Test.stopTest();

        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>([
                SELECT Id, LinkedEntityId, ContentDocumentId, Visibility, ShareType
                FROM ContentDocumentLink
                WHERE LinkedEntityId = :quote.Id
                AND Visibility = 'AllUsers'
        ]);

        System.assertNotEquals(0, contentDocumentLinks.size());
    }

    @isTest
    public static void testBehaviour_expectedException() {
        setUpData(false);
        Boolean expectedErrorOccurred = false;

        List<ContentDocument> docs = new List<ContentDocument>([
                SELECT Id
                FROM ContentDocument
                LIMIT 1
        ]);

        try {
            if (!docs.isEmpty()) {
                EWContentDocumentHelper.skipCheckForTest = true;
                delete docs;
            } else {
                System.assert(false);
            }
        } catch (Exception e) {
            expectedErrorOccurred = true;
        }

        System.assert(expectedErrorOccurred);
    }

    @isTest
    public static void testAddCoverage() {
        setUpData(false);
        ContentDocumentTriggerHandler handler = new ContentDocumentTriggerHandler();
        handler.BeforeInsert(null);
        handler.BeforeUpdate(null, null);
        handler.AfterInsert(null);
        handler.AfterUpdate(null, null);
        handler.AfterDelete(null);
        handler.AfterUndelete(null);
    }
}