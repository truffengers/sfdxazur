public class EWCustomReportWrapper {

    public static Map<String, Set<String>> headersByReportName = new Map<String, Set<String>>{
            'AIG Report' => new Set<String>{
                    //'Transaction Type',
                    'Product Name',
                    'Azur policy number',
                    'AIG policy number',
                    'Policy Status',
                    'Type of Transaction',
                    'Policyholder title',
                    'Policyholder Firstname',
                    'Policyholder Middlename',
                    'Policyholder Lastname',
                    'Policyholder DOB',
                    'Policyholder occupation',
                    'Policyholder nationality',
                    'Address Line 1',
                    'Address Line 2',
                    'Address Line 3',
                    'Address Line 4',
                    'City',
                    'County',
                    'Postcode',
                    'Financial Broker Account',
                    'Financial account DR code',
                    'Broker commission percent',
                    'Broker commission amount',
                    'Azur commission percent',
                    'Azur commission amount',
                    'Effective date (start date of policy or MTA)',
                    'Expiry date (or cancellation date)',
                    'Currency',
                    'IPT percent',
                    'Coverage type',
                    'Policy excess',
                    'Building sum insured',
                    'Buildings premium ex IPT',
                    'Tenants Improvements sum insured',
                    'Tenants Improvements premium ex IPT',
                    'Contents sum insured',
                    'Contents premium ex IPT',
                    'Art & Collectables sum insured',
                    'Art and Collectables premium ex IPT',
                    'Jewellery and watches SI',
                    'Jewellery and watches premium ex IPT',
                    'Public liability sum insured',
                    'Public libaility premium',
                    'Employers liability sum insured',
                    'Pedal cycles sum insured',
                    'Pedal cycle sum insured ex IPT',
                    'Fixed expense premium',
                    'Total premium ex IPT ',
                    'IPT amount',
                    'Total premium inc IPT'
            },
            'Cyber Report' => new Set<String>{
                    'Policy Name',
                    'Transaction Name',
                    'Status',
                    'Type of Transaction',
                    'Salutation',
                    'First Name',
                    'Middle Name',
                    'Last Name',
                    'Building number',
                    'Building name',
                    'Flat name',
                    'Person Street',
                    'Person City',
                    'Person Country',
                    'Person Postal Code',
                    //'Last Transaction Type',
                    'Inception Date',
                    'Effective Date',
                    'Expiration Date',
                    'Commission Rate',
                    'Broker Commission Rate',
                    'Insurance Premium Tax Rate',
                    'Personal Coverage Limit',
                    'Premium Net',
                    'Personal Premium Gross Ex IPT',
                    'Personal IPT Total',
                    'Owed to Carrier'
            },
            'Legal Expenses Report' => new Set<String>{
                    'Policy Name',
                    'Transaction Name',
                    'Status',
                    'Type of Transaction',
                    'Inception Date',
                    'Expiration Date',
                    'First Name',
                    'Middle Name',
                    'Last Name',
                    'Birth Date',
                    'Building number',
                    'Building name',
                    'Flat name',
                    'Person Street',
                    'Person City',
                    'Person Country',
                    'Person Postal Code',
                    'Commission Rate',
                    'Broker Commission Rate',
                    'Insurance Premium Tax Rate',
                    'Premium Gross Ex IPT',
                    'Premium Net',
                    'IPT Total',
                    'Owed to Carrier'
            },
            'Home Emergency Report' => new Set<String>{
                    'Policy Name',
                    'Transaction Name',
                    'Status',
                    'Type of Transaction',
                    'Inception Date',
                    'Expiration Date',
                    'First Name',
                    'Middle Name',
                    'Last Name',
                    'Birth Date',
                    'Building number',
                    'Building name',
                    'Flat name',
                    'Person Street',
                    'Person City',
                    'Person Country',
                    'Person Postal Code',
                    'Commission Rate',
                    'Broker Commission Rate',
                    'Insurance Premium Tax Rate',
                    'Premium Gross Ex IPT',
                    'Premium Net',
                    'IPT Total',
                    'Owed to Carrier'
            }
    };

    private static String convertDateToString(Date dateToConvert) {
        if (dateToConvert != null) {
            return Datetime.newInstanceGmt(dateToConvert, Time.newInstance(0, 0, 0, 0)).format('dd/MM/yyy');
        }
        return '';
    }

    private static String convertDecimalToPercentString(Decimal fieldValue) {
        if (fieldValue != null) {
            return fieldValue.setScale(2).toPlainString() + '%';
        }
        return '';
    }

    private static String convertDecimal(Decimal fieldValue) {
        if (fieldValue != null) {
            return fieldValue.setScale(2).toPlainString();
        }
        return '';
    }

    public interface RowDataWrapper {
        List<RowCellWrapper> getRowValues(Boolean addStyle);
        Boolean invalidRow();
    }

    public class NoDataRowDataWrapper implements RowDataWrapper {
        public List<RowCellWrapper> getRowValues(Boolean addStyle) {
            List<RowCellWrapper> result = new List<RowCellWrapper>();

            result.add(new RowCellWrapper('Please select report'));
            return result;
        }

        public Boolean invalidRow() {
            return false;
        }
    }

    public class CyberReportRowDataWrapper implements RowDataWrapper {
        private Asset policy;
        private IBA_FFPolicyCarrierLine__c carrierLine;
        private IBA_PolicyTransactionLine__c policyTransactionLine;
        private IBA_PolicyTransaction__c policyTransaction;
        private QuoteLineItem lineItem;

        public CyberReportRowDataWrapper(
                Asset policy, IBA_PolicyTransactionLine__c policyTransactionLine,
                IBA_PolicyTransaction__c trans, QuoteLineItem lineItem) {
            this.policy = policy;
            this.policyTransactionLine = policyTransactionLine;
            this.policyTransaction = trans;
            this.lineItem = lineItem;
        }

        public List<RowCellWrapper> getRowValues(Boolean addStyle) {
            List<RowCellWrapper> result = new List<RowCellWrapper>();

            // Policy Name
            result.add(new RowCellWrapper(this.policy.Name.split(' ').get(0)));
            //Transaction Name
            result.add(new RowCellWrapper(this.policyTransaction.Name));
            // Status
            result.add(new RowCellWrapper(this.policy.Status));
            // Type of Transaction
            result.add(new RowCellWrapper(this.policy.EW_TypeofTransaction__c));
            //Salutation
            result.add(new RowCellWrapper(this.policy.Account.Salutation));
            // First Name
            result.add(new RowCellWrapper(this.policy.Account.FirstName));
            // Middle Name
            result.add(new RowCellWrapper(this.policy.Account.vlocity_ins__MiddleName__pc));
            // Last Name
            result.add(new RowCellWrapper(this.policy.Account.LastName));
            //Building number
            result.add(new RowCellWrapper(this.policy.Account.EW_BuildingNumber__pc));
            //Building name
            result.add(new RowCellWrapper(this.policy.Account.EW_BuildingName__pc));
            //Flat name
            result.add(new RowCellWrapper(this.policy.Account.EW_FlatName__pc));
            //Person Street
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingStreet));
            //Person City
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingCity));
            //Person Country
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingCountry));
            //Person Postal Code
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingPostalCode));
            //Last Transaction Type
            //result.add(new RowCellWrapper(this.policy.EW_TypeofTransaction__c));
            //Inception Date
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__InceptionDate__c)));
            //Effective Date
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__EffectiveDate__c)));
            //Expiration Date
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__ExpirationDate__c)));
            //MGACommissionRate__c
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_MGACommissionRate__c)));
            //BrokerCommissionRate__c
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_BrokerCommissionRate__c)));
            //IBA_InsurancePremiumTaxRate__c
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_InsurancePremiumTaxRate__c)));
            //Personal Cyber Coverage Limit
            result.add(new RowCellWrapper(convertDecimal(this.policy.vlocity_ins__PolicyCoverages__r.get(0).EW_SpecifiedItemValue__c)));
            if (this.lineItem != null) {
                //Premium
                result.add(new RowCellWrapper(convertDecimal(this.lineItem.EW_Transactional_PremiumNet__c)));
                //Premium Gross Ex IPT
                result.add(new RowCellWrapper(convertDecimal(this.lineItem.EW_Transactional_PremiumGrossExIPT__c)));
                //IPT Total
                result.add(new RowCellWrapper(convertDecimal(this.lineItem.EW_Transactional_IPT__c)));
            } else {
                //Premium Gross Ex IPT
                result.add(new RowCellWrapper(''));
                //Premium
                result.add(new RowCellWrapper(''));
                //IPT Total
                result.add(new RowCellWrapper(''));
            }
            //Owed to Carrier
            result.add(new RowCellWrapper(convertDecimal(this.policyTransactionLine.IBA_NetPayabletoCarrier__c)));
            return result;
        }

        public Boolean invalidRow() {
            return policy == null;
        }
    }

    public class ARAGReportRowDataWrapper implements RowDataWrapper {
        private Asset policy;
        private IBA_PolicyTransactionLine__c policyTransactionLine;
        private IBA_PolicyTransaction__c policyTransaction;
        private QuoteLineItem lineItem;


        public ARAGReportRowDataWrapper(Asset policy, IBA_PolicyTransactionLine__c policyTransactionLine,
                IBA_PolicyTransaction__c trans, QuoteLineItem lineItem) {
            this.policy = policy;
            this.policyTransactionLine = policyTransactionLine;
            this.policyTransaction = trans;
            this.lineItem = lineItem;
        }

        public List<RowCellWrapper> getRowValues(Boolean addStyle) {
            List<RowCellWrapper> result = new List<RowCellWrapper>();

            // Policy Name
            result.add(new RowCellWrapper(this.policy.Name.split(' ').get(0)));
            //Transaction Name
            result.add(new RowCellWrapper(this.policyTransaction.Name));
            // Status
            result.add(new RowCellWrapper(this.policy.Status));
            // Type of Transaction
            result.add(new RowCellWrapper(this.policy.EW_TypeofTransaction__c));
            //Inception Date
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__InceptionDate__c)));
            // Expiration Date
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__ExpirationDate__c)));
            // First Name
            result.add(new RowCellWrapper(this.policy.Account.FirstName));
            // Middle Name
            result.add(new RowCellWrapper(this.policy.Account.vlocity_ins__MiddleName__pc));
            // Last Name
            result.add(new RowCellWrapper(this.policy.Account.LastName));
            //Birth Date
            result.add(new RowCellWrapper(convertDateToString(this.policy.Account.PersonBirthdate)));
            //Building number
            result.add(new RowCellWrapper(this.policy.Account.EW_BuildingNumber__pc));
            //Building name
            result.add(new RowCellWrapper(this.policy.Account.EW_BuildingName__pc));
            //Flat name
            result.add(new RowCellWrapper(this.policy.Account.EW_FlatName__pc));
            //Person Street
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingStreet));
            //Person City
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingCity));
            //Person Country
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingCountry));
            //Person Postal Code
            result.add(new RowCellWrapper(this.policy.Account.PersonMailingPostalCode));
            //Last Transaction Type
            //result.add(new RowCellWrapper(this.policy.EW_TypeofTransaction__c));
            //MGACommissionRate__c
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_MGACommissionRate__c)));
            //BrokerCommissionRate__c
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_BrokerCommissionRate__c)));
            //IBA_InsurancePremiumTaxRate__c
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_InsurancePremiumTaxRate__c)));
            if (this.lineItem != null) {
                //Premium Gross Ex IPT
                result.add(new RowCellWrapper(convertDecimal(this.lineItem.EW_Transactional_PremiumGrossExIPT__c)));
                //Premium
                result.add(new RowCellWrapper(convertDecimal(this.lineItem.EW_Transactional_PremiumNet__c)));
                //IPT Total
                result.add(new RowCellWrapper(convertDecimal(this.lineItem.EW_Transactional_IPT__c)));
            } else {
                //Premium Gross Ex IPT
                result.add(new RowCellWrapper(''));
                //Premium
                result.add(new RowCellWrapper(''));
                //IPT Total
                result.add(new RowCellWrapper(''));
            }
            //Owed to Carrier
            result.add(new RowCellWrapper(convertDecimal(this.policyTransactionLine.IBA_NetPayabletoCarrier__c)));

            return result;
        }

        public Boolean invalidRow() {
            return policy == null;
        }
    }

    public class AIGReportRowDataWrapper implements RowDataWrapper {
        public Asset policy { get; set; }
        public Account personAccount { get; set; }
        private IBA_FFPolicyCarrierLine__c carrierLine { get; set; }

        private vlocity_ins__AssetCoverage__c coverageBuildings;
        private vlocity_ins__AssetCoverage__c coverageContents;
        private vlocity_ins__AssetCoverage__c coverageArtAndCollectables;
        private vlocity_ins__AssetCoverage__c coverageJewellery;
        private vlocity_ins__AssetCoverage__c coveragePublicLiability;
        private vlocity_ins__AssetCoverage__c coverageEmployersLiability;
        private vlocity_ins__AssetCoverage__c coveragePedalCycles;
        private vlocity_ins__AssetCoverage__c coverageTenantsImprovements;

        private QuoteLineItem qliBuildings;
        private QuoteLineItem qliContents;
        private QuoteLineItem qliArtAndCollectables;
        private QuoteLineItem qliJewellery;
        private QuoteLineItem qliPublicLiability;
        private QuoteLineItem qliEmployersLiability;
        private QuoteLineItem qliPedalCycles;
        private QuoteLineItem qliTenantsImprovements;

        public AIGReportRowDataWrapper(Asset policy, IBA_FFPolicyCarrierLine__c carrierLine, Account personAccount, List<QuoteLineItem> quoteLineItems) {
            this.policy = policy;
            this.personAccount = personAccount;
            this.carrierLine = carrierLine;

            for (vlocity_ins__AssetCoverage__c coverage : policy.vlocity_ins__PolicyCoverages__r) {
                String coverageProdCode = coverage.vlocity_ins__Product2Id__r.ProductCode;

                if (coverageProdCode == EWConstants.EW_PROD_CODE_BUILDINGS) {
                    this.coverageBuildings = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_CONTENTS) {
                    this.coverageContents = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_ART) {
                    this.coverageArtAndCollectables = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_JEWELLERY) {
                    this.coverageJewellery = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_PUBLIC_LIABILITY) {
                    this.coveragePublicLiability = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_EMPLOYERS_LIABILITY) {
                    this.coverageEmployersLiability = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_PEDAL_CYCLE) {
                    this.coveragePedalCycles = coverage;
                    continue;
                } else if (coverageProdCode == EWConstants.EW_PROD_CODE_TENANTS) {
                    this.coverageTenantsImprovements = coverage;
                    continue;
                }
            }

            if (quoteLineItems != null && !quoteLineItems.isEmpty()) {
                for (QuoteLineItem qli : quoteLineItems) {
                    String qliProdCode = qli.Product2.ProductCode;

                    if (qliProdCode == EWConstants.EW_PROD_CODE_BUILDINGS) {
                        this.qliBuildings = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_CONTENTS) {
                        this.qliContents = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_ART) {
                        this.qliArtAndCollectables = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_JEWELLERY) {
                        this.qliJewellery = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_PUBLIC_LIABILITY) {
                        this.qliPublicLiability = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_EMPLOYERS_LIABILITY) {
                        this.qliEmployersLiability = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_PEDAL_CYCLE) {
                        this.qliPedalCycles = qli;
                        continue;
                    } else if (qliProdCode == EWConstants.EW_PROD_CODE_TENANTS) {
                        this.qliTenantsImprovements = qli;
                        continue;
                    }
                }
            }
        }

        public List<RowCellWrapper> getRowValues(Boolean addStyle) {
            List<RowCellWrapper> result = new List<RowCellWrapper>();

            // Product Name
            result.add(new RowCellWrapper(this.policy.IBA_AIGProductName__c));
            // Azur policy number
            result.add(new RowCellWrapper(this.policy.Name.split(' ').get(0)));
            // AIG policy number
            result.add(new RowCellWrapper(this.policy.EW_AIGPolicyNumber__c));
            // Policy Status
            result.add(new RowCellWrapper(this.policy.Status));
            // Type of Transaction
            result.add(new RowCellWrapper(this.policy.EW_TypeofTransaction__c));
            // Policyholder title
            result.add(new RowCellWrapper(this.personAccount.Salutation));
            // Policyholder Firstname
            result.add(new RowCellWrapper(this.personAccount.FirstName));
            // Policyholder Middlename
            result.add(new RowCellWrapper(this.personAccount.vlocity_ins__MiddleName__pc));
            // Policyholder Lastname
            result.add(new RowCellWrapper(this.personAccount.LastName));
            // Policyholder DOB
            result.add(new RowCellWrapper(convertDateToString(this.personAccount.PersonBirthdate)));
            // Policyholder occupation
            result.add(new RowCellWrapper(this.personAccount.EW_Occupation__pr.Name));
            // Policyholder nationality
            result.add(new RowCellWrapper(this.personAccount.EW_Nationality__pr.Name));
            // Address Line 1
            result.add(new RowCellWrapper(this.personAccount.EW_BuildingNumber__pc));
            // Address Line 2
            result.add(new RowCellWrapper(this.personAccount.EW_BuildingName__pc));
            // Address Line 3
            result.add(new RowCellWrapper(this.personAccount.EW_FlatName__pc));
            // Address Line 4
            result.add(new RowCellWrapper(this.personAccount.PersonMailingStreet));
            // City
            result.add(new RowCellWrapper(this.personAccount.PersonMailingCity));
            // County
            result.add(new RowCellWrapper(this.personAccount.PersonMailingCountry));
            // Postcode
            result.add(new RowCellWrapper(this.personAccount.PersonMailingPostalCode));
            // Financial Broker Account
            result.add(new RowCellWrapper(String.valueOf(this.policy.IBA_ProducingBrokerAccount__r.Name)));
            // Financial account DR code
            result.add(new RowCellWrapper(this.policy.IBA_ProducingBrokerAccount__r.EW_DRCode__c));
            // Broker commission percent
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_BrokerCommissionRate__c)));
            // Broker commission amount
            result.add(new RowCellWrapper(String.valueOf(this.carrierLine.IBA_CarrierBrokerCommission__c)));
            // Azur commission percent
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.EW_Quote__r.EW_AzurCommissionPercent__c)));
            // Azur commission amount
            result.add(new RowCellWrapper(String.valueOf(this.carrierLine.IBA_CarrierMGACommission__c)));
            // Effective date (start date of policy or MTA)
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__EffectiveDate__c)));
            // Expiry date (or cancellation date)
            result.add(new RowCellWrapper(convertDateToString(this.policy.vlocity_ins__ExpirationDate__c)));
            // Currency
            result.add(new RowCellWrapper(this.policy.CurrencySymbol__c));
            // IPT percent
            result.add(new RowCellWrapper(convertDecimalToPercentString(this.policy.IBA_InsurancePremiumTaxRate__c)));
            // Coverage type
            result.add(new RowCellWrapper(this.policy.EW_CoverageType__c));
            // Policy excess
            result.add(new RowCellWrapper(String.valueOf(this.policy.EW_Excess__c)));
            // Building sum insured
            result.add(new RowCellWrapper(this.policy.vlocity_ins__InsuredItems__r.isEmpty() || this.policy.EW_CoverageType__c == 'Contents Only'
                    ? '0.00' : String.valueOf(this.policy.vlocity_ins__InsuredItems__r.get(0).EW_RebuildCost__c)));
            // Buildings premium ex IPT
            result.add(new RowCellWrapper(
                    this.qliBuildings == null ? '' : String.valueOf(this.qliBuildings.EW_Transactional_PremiumGrossExIPT__c))
            );
            // Tenants Improvements sum insured
            result.add(new RowCellWrapper(
                    this.coverageTenantsImprovements == null ? '' : String.valueOf(this.coverageTenantsImprovements.EW_SpecifiedItemValue__c))
            );
            // Tenants Improvements premium ex IPT
            result.add(new RowCellWrapper(
                    this.qliTenantsImprovements == null ? '' : String.valueOf(this.qliTenantsImprovements.EW_Transactional_PremiumGrossExIPT__c))
            );
            // Contents sum insured
            result.add(new RowCellWrapper(this.policy.vlocity_ins__InsuredItems__r.isEmpty() || this.policy.vlocity_ins__InsuredItems__r.get(0).EW_RebuildCost__c == null
                    ? '0.00' : String.valueOf(this.policy.vlocity_ins__InsuredItems__r.get(0).EW_RebuildCost__c * 0.2)));
            // Contents premium ex IPT
            result.add(new RowCellWrapper(
                    this.qliContents == null ? '' : String.valueOf(this.qliContents.EW_Transactional_PremiumGrossExIPT__c))
            );
            // Art & Collectables sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coverageArtAndCollectables.EW_SpecifiedItemValue__c)));
            // Art and Collectables premium ex IPT
            result.add(new RowCellWrapper(
                    this.qliArtAndCollectables == null ? '' : String.valueOf(this.qliArtAndCollectables.EW_Transactional_PremiumGrossExIPT__c)
            ));
            // Jewellery and watches SI
            result.add(new RowCellWrapper(String.valueOf(this.coverageJewellery.EW_SpecifiedItemValue__c)));
            // Jewellery and watches premium ex IPT
            result.add(new RowCellWrapper(
                    this.qliJewellery == null ? '' : String.valueOf(this.qliJewellery.EW_Transactional_PremiumGrossExIPT__c))
            );
            // Public liability sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coveragePublicLiability.EW_SpecifiedItemValue__c)));
            // Public libaility premium
            result.add(new RowCellWrapper(
                    this.qliPublicLiability == null ? '' : String.valueOf(this.qliPublicLiability.EW_Transactional_PremiumGrossExIPT__c))
            );
            // Employers liability sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coverageEmployersLiability.EW_SpecifiedItemValue__c)));
            // Pedal cycles sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coveragePedalCycles.EW_SpecifiedItemValue__c)));
            // Pedal cycle sum insured ex IPT
            result.add(new RowCellWrapper(
                    this.qliPedalCycles == null ? '' : String.valueOf(this.qliPedalCycles.EW_Transactional_PremiumGrossExIPT__c))
            );
            // Fixed expense premium
            result.add(new RowCellWrapper(String.valueOf(this.policy.EW_FixedExpenseExIPT__c)));
            // Total premium ex IPT
            result.add(new RowCellWrapper(this.carrierLine == null ? '0.00' : String.valueOf(this.carrierLine.IBA_CarrierGWP__c)));
            // IPT amount
            result.add(new RowCellWrapper(this.carrierLine == null ? '0.00' : String.valueOf(this.carrierLine.IBA_CarrierIPT__c)));
            // Total premium inc IPT
            result.add(new RowCellWrapper(this.carrierLine == null ? '0.00' : String.valueOf(this.carrierLine.IBA_CarrierGWP__c + this.carrierLine.IBA_CarrierIPT__c)));

            return result;
        }

        public Boolean invalidRow() {
            return policy == null;
        }
    }

    public class RowCellWrapper {
        public String val { get; set; }

        public RowCellWrapper(String val) {
            this.val = (val != null) ? val : '';
        }
    }
}