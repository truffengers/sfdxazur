/**
* @name: IBA_PayableInvoiceTriggerHandlerTest
* @description: Test class for IBA_PayableInvoiceTriggerHandler
*
*/
@IsTest
private class IBA_PayableInvoiceTriggerHandlerTest {

    private static final String COMPANY_NAME = 'ApexTestCompany';
    private static final String CURRENCY_USD = 'USD';
    private static final Decimal MONEY = 100.50;
    private static final Decimal MONEY2 = 80.01;

    @testSetup
    static void createTestRecords() {
        IBA_TestDataFactory.FinancialForceSetup ffs = IBA_TestDataFactory.setupFinancialForce(COMPANY_NAME, CURRENCY_USD);
        Group companyGroup = ffs.companyGroup;
        c2g__codaCompany__c company = ffs.company;
    
        c2g__codaYear__c yr = IBA_TestDataFactory.createYear(true, companyGroup.Id, company.Id);
        c2g.CODAYearWebService.calculatePeriods(yr.Id);

        c2g__codaAccountingCurrency__c curr = IBA_TestDataFactory.createCurrency(true, CURRENCY_USD);

        c2g__codaGeneralLedgerAccount__c gla = IBA_TestDataFactory.createGeneralLedgerAccount(true, 'Brochures', 'uniqueCaseInsensitive24!');

        Product2 product = IBA_TestDataFactory.createProduct(false, 'Product 1', IBA_UtilConst.PRODUCT_RT_Master);
        product.IBA_AIGProduct__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
        product.IBA_AIGMinorLine__c = 'PCG Test Liability';
        product.IBA_AIGPolicyCodeNumber__c = '000000test';
        product.IsActive = true;
        product.c2g__CODASalesRevenueAccount__c = gla.Id;
        insert product;

        Account acc = IBA_TestDataFactory.createAccount(false, 'testAccOnHold', IBA_UtilConst.ACCOUNT_RT_BROKER);
        acc.c2g__CODAAccountTradingCurrency__c = CURRENCY_USD;
        acc.c2g__CODAAccountsPayableControl__c = gla.Id;
        acc.c2g__CODAAccountsReceivableControl__c = gla.Id;
        acc.c2g__CODAVATStatus__c = 'Home';
        acc.c2g__CODATaxCalculationMethod__c = 'Gross';
        acc.c2g__CODABaseDate1__c = 'Invoice Date';
        acc.c2g__CODADaysOffset1__c = 0;
        acc.c2g__CODADescription1__c = 'Test';
        acc.c2g__CODADiscount1__c = 0;
        insert acc;

        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c WHERE 
            c2g__YearName__c = :yr.Id AND c2g__PeriodNumber__c != '000' AND 
            c2g__EndDate__c > TODAY LIMIT 1];

        c2g__codaBankAccount__c bank = IBA_TestDataFactory.createBankAccount(false, 'Trust Account USD', '123456789', 'Santander', 'uniqueCaseInsensitive24!', gla.Id, curr.Id);
        insert bank;

        List<c2g__codaInvoice__c> invoicesToInsert = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c invoice = IBA_TestDataFactory.createInvoice(false, company.Id, acc.Id, period.Id, curr.Id);
        c2g__codaInvoice__c invoice2 = IBA_TestDataFactory.createInvoice(false, company.Id, acc.Id, period.Id, curr.Id);

        Asset policy = IBA_TestDataFactory.createPolicy(false, 'Policy 1', acc.Id);
        policy.IBA_ProducingBrokerAccount__c = acc.Id;
        policy.IBA_AccountingBroker__c = acc.Id;
        policy.Azur_Account__c = acc.Id;
        insert policy;

        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(true, 2, policy.Id);
        invoice.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        invoice2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        invoicesToInsert.add(invoice);
        invoicesToInsert.add(invoice2);
        insert invoicesToInsert;

        List<c2g__codaInvoiceLineItem__c> invoiceLineItemsToInsert = new List<c2g__codaInvoiceLineItem__c>();
        c2g__codaInvoiceLineItem__c invoiceLineItem = IBA_TestDataFactory.createInvoiceLineItem(false, 15, MONEY, invoice.Id, product.Id);
        c2g__codaInvoiceLineItem__c invoiceLineItem2 = IBA_TestDataFactory.createInvoiceLineItem(false, 15, MONEY2, invoice2.Id, product.Id);

        invoiceLineItemsToInsert.add(invoiceLineItem);
        invoiceLineItemsToInsert.add(invoiceLineItem2);
        insert invoiceLineItemsToInsert;


        List<c2g__codaPurchaseInvoice__c> purchaseInvoiceToInsert = new List<c2g__codaPurchaseInvoice__c>(); 
        c2g__codaPurchaseInvoice__c purchaseInvoice = IBA_TestDataFactory.createPurchaseInvoice(false, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c, 
        invoice.Id);
        purchaseInvoice.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        c2g__codaPurchaseInvoice__c purchaseInvoice2 = IBA_TestDataFactory.createPurchaseInvoice(false, 
        invoice2.c2g__OwnerCompany__c, 
        invoice2.c2g__Account__c, 
        invoice2.c2g__Period__c, 
        invoice2.c2g__InvoiceCurrency__c, 
        invoice2.Id);
        purchaseInvoice2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        purchaseInvoiceToInsert.add(purchaseInvoice);
        purchaseInvoiceToInsert.add(purchaseInvoice2);
        insert purchaseInvoiceToInsert;

        List<c2g__codaPurchaseInvoiceExpenseLineItem__c> purchaseInvoiceExpenseLineItemList = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();
        c2g__codaPurchaseInvoiceExpenseLineItem__c purchaseInvoiceExpenseLineItem = IBA_TestDataFactory.createPurchaseInvoiceExpenseLineItem(false,
            MONEY,
            invoice.c2g__OwnerCompany__c, 
            gla.Id, 
            purchaseInvoice.Id);
        c2g__codaPurchaseInvoiceExpenseLineItem__c purchaseInvoiceExpenseLineItem2 = IBA_TestDataFactory.createPurchaseInvoiceExpenseLineItem(false,
            MONEY2,
            invoice2.c2g__OwnerCompany__c, 
            gla.Id, 
            purchaseInvoice2.Id);

        purchaseInvoiceExpenseLineItemList.add(purchaseInvoiceExpenseLineItem);
        purchaseInvoiceExpenseLineItemList.add(purchaseInvoiceExpenseLineItem2);
        insert purchaseInvoiceExpenseLineItemList;


        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();

        IBA_FFPolicyCarrierLine__c policyCarrierLine = IBA_TestDataFactory.createPolicyCarrierLine(false, policyTransactions[0].Id, acc.Id);
        policyCarrierLine.IBA_CarrierPayableInvoice__c = purchaseInvoice.Id;
        policyCarrierLines.add(policyCarrierLine);

        insert policyCarrierLines;

        Integer i = 0;
        for(IBA_PolicyTransaction__c policyTransaction :policyTransactions) {
            if(i == 0) {
                policyTransaction.IBA_AccountingBrokerSIN__c = invoice.Id;
            } else if(i == 1) {
                policyTransaction.IBA_MGAPayableInvoice__c = purchaseInvoice2.Id;
                policyTransaction.IBA_BrokerSalesInvoice__c = invoice2.Id;
            }
            i++;
        }
        update policyTransactions;
    }

    @isTest
    static void afterUpdateTest() {
        Set<Id> payableInvoiceIds = new Set<Id>();

        for(c2g__codaPurchaseInvoice__c payableInvoice : [SELECT Id FROM c2g__codaPurchaseInvoice__c]) {
            payableInvoiceIds.add(payableInvoice.Id);
        }

        Test.startTest();
        IBA_TestDataFactory.postPurchaseInvoices(payableInvoiceIds);
        Test.stopTest();
        IBA_PolicyTransaction__c carrierPT = new IBA_PolicyTransaction__c();
        IBA_PolicyTransaction__c mgaPT = new IBA_PolicyTransaction__c();
        for(IBA_PolicyTransaction__c policyTransaction :[SELECT Id, IBA_OwedtoCarrier__c, IBA_OwedtoMGA__c, IBA_MGAPayableInvoice__c FROM IBA_PolicyTransaction__c]) {
            if(policyTransaction.IBA_MGAPayableInvoice__c != null) {
                mgaPT = policyTransaction;
            } else {
                carrierPT = policyTransaction;
            }
        }
        
        System.assertEquals(MONEY2, mgaPT.IBA_OwedtoMGA__c);
        System.assertEquals(MONEY, carrierPT.IBA_OwedtoCarrier__c);
    }
}