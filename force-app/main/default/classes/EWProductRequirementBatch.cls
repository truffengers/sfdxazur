/*
Name:            EWProductRequirementBatch
Description:     This Batch class will execute the Integration Procedures
Created By:      Rajni Bajpai 
Created On:      15 March 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          15Mar21            EWH-2032 
************************************************************************************************
*/

public with sharing class EWProductRequirementBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    public Set<String> prProductNames; 
    public Set<String> prProductRequirementNames;
    public Map<String, Map<String, vlocity_ins__ProductRequirement__c>> prReqByProductIdAndReqName; 
    public EWProductRequirementBatch(){}
    
    public EWProductRequirementBatch(Set<String> prProductNames, Set<String> prProductRequirementNames, Map<String, Map<String, vlocity_ins__ProductRequirement__c>> prReqByProductIdAndReqName ){
        this.prProductNames = prProductNames ;  
        this.prProductRequirementNames=prProductRequirementNames;
        this.prReqByProductIdAndReqName=prReqByProductIdAndReqName;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        String queryString = 'SELECT Id, EW_ProductName__c, EW_ProductRequirementName__c, EW_Product_Requirement__c';
        queryString+= ' FROM EW_Underwriting_Rule__c ';
        queryString+='WHERE EW_Product_Requirement__c = NULL AND (EW_ProductName__c IN :prProductNames AND EW_ProductRequirementName__c IN :prProductRequirementNames)';
         if (Test.isRunningTest()) {
            queryString += ' LIMIT 200';
        }
        return Database.getQueryLocator(queryString);
    }
    public void execute(Database.BatchableContext bc, List<EW_Underwriting_Rule__c> scope) {
        
        for (EW_Underwriting_Rule__c uwRule : scope) {
            if (prReqByProductIdAndReqName.containsKey(uwRule.EW_ProductName__c) && prReqByProductIdAndReqName.get(uwRule.EW_ProductName__c).containsKey(uwRule.EW_ProductRequirementName__c)) {
                Id newPrReqId = prReqByProductIdAndReqName.get(uwRule.EW_ProductName__c).get(uwRule.EW_ProductRequirementName__c).Id;
                uwRule.EW_Product_Requirement__c = newPrReqId;
            }
        }
        update scope;        
    }
    public void finish(Database.BatchableContext bc) {

    }    
}