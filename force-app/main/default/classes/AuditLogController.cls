public with sharing class AuditLogController {
	
    @AuraEnabled
    public static List<AuditLog__c> getAuditLogs(String sobjectType, 
                                                 String sobjectRecordId) {
        return [ SELECT Id, changedByUser__c, changedByUserName__c, fieldAPIName__c,
                 		fieldLabel__c, newValue__c, previousValue__c, 
               			sObjectRecordId__c, sObjectType__c, timestamp__c 
                   FROM AuditLog__c
                  WHERE sObjectType__c = :sobjectType AND 
               			sObjectRecordId__c = :sobjectRecordId 
               ORDER BY timestamp__c DESC NULLS LAST ];
       
    }
}