TT.carouselHome = function($, lazySizes, Hammer, TT, settings) {

  'use strict';

  var currentIndex;
  var carousel;
  var carouselId;
  var postsData;
  var svgSpritePath;
  var slidesData;
  var automatic;
  var advancerShowDuration;

  var $carousel;
  var $container;
  var $topicIconUse;
  var $topic;
  var $contentType;
  var $title;
  var $subtitle;
  var $contributor;
  var $contributorImage;
  var $contributorName;
  var $readMoreButton;
  var $links;

  var DETAILS_VISIBILITY_CLASS = 'is-visible';



  function referenceElements() {
    $container        = $('#' + carouselId + ' .c-carousel__details');
    $topicIconUse     = $container.find('.js-carousel-topic-icon-use');
    $topic            = $container.find('.js-carousel-topic');
    $contentType      = $container.find('.js-carousel-content-type');
    $title            = $container.find('.js-carousel-title');
    $subtitle         = $container.find('.js-carousel-subtitle');
    $contributor      = $container.find('.js-carousel-contributor');
    $contributorImage = $container.find('.js-carousel-contributor-image');
    $contributorName  = $container.find('.js-carousel-contributor-name');
    $readMoreButton   = $container.find('.js-carousel-read-more-button');
    $links            = $readMoreButton.add($title).add($contributorName);
  }

  function initVars() {
    carouselId           = settings.carouselId;
    postsData            = settings.postsData;
    svgSpritePath        = settings.svgSpritePath;
    slidesData           = settings.slidesData;
    automatic            = settings.automatic;
    advancerShowDuration = settings.advancerShowDuration;
  }

  function initCarousel() {

    carousel = TT.carousel($, lazySizes, Hammer, TT, {
      arrows: {
        containerSelector: '#' + carouselId + ' .c-carousel__arrows',
        continuous: true
      },
      pagination: {
        listSelector : '#' + carouselId + ' .c-carousel__pagination'
      },
      core: {
        slidesContainerSelector : '#' + carouselId + ' .c-carousel__slides',
        slideClass              : 'c-carousel__slide',
        data                    : slidesData,
        automatic               : automatic,
        advancerShowDuration    : advancerShowDuration
      }
    });

    $carousel = $(carousel);
    $carousel.bind('loaded', loadedHandler);
    $carousel.bind('revealed', revealedHandler);
    carousel.init();
  }

  function getSlug(string) {
    return string.toLowerCase().replace(' ', '-');
  }

  function getTopicSlug() {
    return getSlug(postsData[currentIndex].topic);
  }

  function getReadMoreButtonClasses() {
    var topic = getTopicSlug();
    return 'u-bg-' + topic + ' u-border-' + topic;
  }

  function getTitleClasses() {
    var topic = getTopicSlug();
    return 'u-hover-' + topic;
  }

  function getContributorNameClasses() {
    var topic = getTopicSlug();
    return 'u-hover-' + topic;
  }

  function getIconPath() {
    return svgSpritePath.replace('.svg', '.svg#icon-' + getTopicSlug());
  }

  function loadedHandler(event, index) {
    if(currentIndex !== undefined) {
      $readMoreButton.removeClass(getReadMoreButtonClasses());
      $title.removeClass(getTitleClasses());
      $contributorName.removeClass(getContributorNameClasses());
    }
    currentIndex = index;
    $container.removeClass(DETAILS_VISIBILITY_CLASS);
  }

  function revealedHandler() {

    var data = postsData[currentIndex];

    $topicIconUse[0].setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', getIconPath());
    $topic.text(data.topic);
    $contentType.text(data.contentType);
    $title.text(data.title);
    $subtitle.text(data.subtitle);
    $contributorImage.attr('src', data.contributor.image);
    $contributorName.text(data.contributor.name);
    $readMoreButton.addClass(getReadMoreButtonClasses());
    $title.addClass(getTitleClasses());
    $contributorName.addClass(getContributorNameClasses());

    $links.off('click');
    $links.on('click', function(event) {
      event.preventDefault();
      data.callback();
    });

    $container.addClass(DETAILS_VISIBILITY_CLASS);
  }



  return {

    init: function() {
      if(document.getElementById(settings.carouselId)) {
        initVars();
        referenceElements();
        initCarousel();
      }
    },

    teardown: function() {
      if(carousel) {
        carousel.teardown();
        carousel = null;
        $carousel.unbind('loaded', loadedHandler);
      }
    }

  };


};
