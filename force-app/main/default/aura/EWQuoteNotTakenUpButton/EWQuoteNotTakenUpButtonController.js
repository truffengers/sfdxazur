({
    doInit: function (component, event, helper) {
        const recordId = component.get('v.recordId');
        let action = component.get("c.isCommunity");

        action.setCallback(this, function (response) {
            const isCommunity = response.getReturnValue();
            if (isCommunity) {
                let urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    'url': '/quote-not-taken-up?recordId=' + recordId
                });
                urlEvent.fire();
            } else {
                component.set('v.isCommunity', false);
            }
        });
        $A.enqueueAction(action);
    }
});