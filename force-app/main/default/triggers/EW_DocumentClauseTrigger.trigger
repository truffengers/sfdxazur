/**
 * Created by roystonlloyd on 2020-01-30.
 */

trigger EW_DocumentClauseTrigger on vlocity_ins__DocumentClause__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerDispatcher.Run(new EWDocumentClauseTriggerHandler());
}