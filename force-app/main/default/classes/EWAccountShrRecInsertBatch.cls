/*
Name:            EWAccountShrRecInsertBatch
Description:     This Batch class will insert or delete Account share records called via trigger.
Created By:      Rajni Bajpai 
Created On:      02 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          02May21            EWH-1983 Share Account ID
************************************************************************************************
*/
public class EWAccountShrRecInsertBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    public Set<Id> accountSIds; 
    public EWAccountShrRecInsertBatch(){}
    
    public EWAccountShrRecInsertBatch(Set<Id> accountSIds){
        this.accountSIds = accountSIds ;  
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        String queryString = 'select id,name,EW_ParentAccount__c,EW_SharedWithAccount__c';
        queryString+= ' FROM EW_AccountSharing__c  ';
        queryString+='WHERE Id IN :accountSIds';
        if (Test.isRunningTest()) {
            queryString += ' LIMIT 200';
        }
        return Database.getQueryLocator(queryString);
    }
    public void execute(Database.BatchableContext bc, List<EW_AccountSharing__c> scope) {
        
        List<AccountShare> accShareNewList=new List<AccountShare>();
        Set<Id> parentAccIds=new Set<Id>();
        Set<Id> sharedAccIds=new Set<Id>();
        Map<Id,Id> accountIdsMap=new Map<Id,Id>();
        Map<Id,Id> quoteAccIdsMap=new Map<Id,Id>();
        for(EW_AccountSharing__c accShr:scope){
            parentAccIds.add(accShr.EW_ParentAccount__c);
            accountIdsMap.put(accShr.EW_ParentAccount__c,accShr.EW_SharedWithAccount__c);
            sharedAccIds.add(accShr.EW_SharedWithAccount__c);            
        }            
        for(Quote qt:[select id, vlocity_ins__AgencyBrokerageId__c ,AccountId from Quote where vlocity_ins__AgencyBrokerageId__c in: parentAccIds]) {
            quoteAccIdsMap.put(qt.AccountId,qt.vlocity_ins__AgencyBrokerageId__c);
        } 
        List<UserRole> roles=[select name,Id,PortalAccountId from UserRole where PortalAccountId in:sharedAccIds];
        Map<Id,Id> roleAccMap=new Map<Id,Id>();
        for(UserRole rl:roles){
            roleAccMap.put(rl.PortalAccountId,rl.Id); 
        }
        Map<String,String> groupMap=new Map<String,String>();
        
        for(group grp:[select Id,RelatedId from group where type='Role' and RelatedId in:roles]){
            
            groupMap.put(grp.RelatedId,grp.Id);
        }
        for(Id acc:quoteAccIdsMap.keyset()){
            AccountShare accShareNew= new AccountShare();
            accShareNew.AccountId=acc;
            accShareNew.UserOrGroupId=groupMap.get(roleAccMap.get(accountIdsMap.get(quoteAccIdsMap.get(acc))));
            accShareNew.AccountAccessLevel='Edit';
            accShareNew.OpportunityAccessLevel='Read';
            accShareNewList.add(accShareNew);           
        }
        
        if(!accShareNewList.isEmpty()){
            try{
                insert accShareNewList;
            }
            catch(Exception ex){
                LogBuilder logBuilder = new LogBuilder();
                Azur_Log__c log = logBuilder.createGenericLog(ex);
                log.Class__c = 'EWAccountShrRecInsertBatch';
                log.Method__c = 'finish';
                log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWAccountShrRecInsertBatch.execute'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
                insert log;
                throw ex;
            }
        }        
    }
    public void finish(Database.BatchableContext bc) {
        
    } 
    
}