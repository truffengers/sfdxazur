TT.scrollRatioCalculator = function($element) {

  var visibleHeight;
  var totalHeight;
  var element = $element[0];



  function recalculateHeight() {
    visibleHeight = $element.height();
    totalHeight   = element === window ? document.documentElement.scrollHeight : element.scrollHeight;
  }

  function getTotalHeight() {
    return totalHeight;
  }

  function getScrollLimit() {
    return totalHeight - visibleHeight;
  }

  function getRatio() {
    return getScrollLimit() === 0 ? 0 : $element.scrollTop() / getScrollLimit();
  }

  function teardown() {
    $element = null;
    element  = null;
  }

  recalculateHeight();


  return {
    recalculateHeight : recalculateHeight,
    getRatio          : getRatio,
    getTotalHeight    : getTotalHeight,
    teardown          : teardown
  };

};