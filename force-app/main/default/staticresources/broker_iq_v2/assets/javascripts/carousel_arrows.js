TT.carouselArrows = function($, options) {

  'use strict';

  var $container;
  var $previous;
  var $next;
  var $links;
  var enabled = { previous: true, next: true };
  var settings;
  var count;
  var ON = 'on';
  var OFF = 'off';

  var DEFAULTS = {
    continuous      : true,
    hiddenSelector  : 'is-hidden',
    ghostedSelector : 'is-ghosted',
    visibleClass    : 'is-visible'
  };



  function referenceElements() {
    $container = settings.containerSelector.jquery === undefined ? $(settings.containerSelector) : settings.containerSelector;
    $links     = $container.find('a');
    $previous  = $links.eq(0);
    $next      = $links.eq(1);
  }

  function turnListeners(onOrOff) {
    $previous[onOrOff]('click', previousClickHandler);
    $next[onOrOff]('click', nextClickHandler);
  }

  function previousClickHandler(event) {
    event.preventDefault();
    if(enabled.previous) { settings.previous(); }
  }

  function nextClickHandler(event) {
    event.preventDefault();
    if(enabled.next) { settings.next(); }
  }

  function setDefaults() {
    settings = $.extend({}, DEFAULTS, options);
  }

  function addOrRemoveClass($element, isAdding, selector) {
    $element[isAdding ? 'addClass' : 'removeClass'](selector);
  }

  function setHidden(isHidden) {
    addOrRemoveClass($container, isHidden, settings.hiddenSelector);
  }

  function setGhostingFor($element, isGhosted) {
    addOrRemoveClass($element, isGhosted, settings.ghostedSelector);
  }

  function setEnabledFor(type, $element, isEnabled) {
    enabled[type] = isEnabled;
    setGhostingFor($element, !isEnabled);
  }

  function setEnabled(index, length) {
    if(!settings.continuous) {
      setEnabledFor('previous', $previous, index !== 0);
      setEnabledFor('next',     $next,     index + 1 !== length);
    }
  }

  function reveal() {
    $container.addClass(settings.visibleClass);
  }

  function init(value) {
    count = value;
    if(count) {
      setDefaults();
      referenceElements();
      setEnabled(0, count);
      turnListeners(ON);
      reveal();
    }
  }

  function update(index, length) {
    setHidden(length < 2);
    setEnabled(index, length);
  }

  function teardown() {
    if(count) {
      turnListeners(OFF);
      $container = null;
      $previous  = null;
      $next      = null;
      $links     = null;
      options    = null;
      settings   = null;
      count      = null;
    }
  }



  return {
    init     : init,
    update   : update,
    teardown : teardown
  };

};