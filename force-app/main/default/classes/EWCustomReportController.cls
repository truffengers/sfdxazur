public class EWCustomReportController {

    private static final Id personAccount = Account.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(EWConstants.ACCOUNT_PERSON_RECORD_TYPE_DEVNAME).getRecordTypeId();
    private static final Id ewPolicy = Asset.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(EWConstants.POLICY_EW_RECORD_TYPE_DEVNAME).getRecordTypeId();

    private static Datetime createdStartDateTime;
    private static Datetime createdEndDateTime;
    private static Date effectiveStartDate;
    private static Date effectiveEndDate;

    private static List<String> typeOfTransactions = new List<String>{
            'New Business',
            'Cancellation',
            'Renewal'
    };

    private static Set<String> productCodesAigReport = new Set<String>{
            EWConstants.EW_PROD_CODE_BUILDINGS,
            EWConstants.EW_PROD_CODE_CONTENTS,
            EWConstants.EW_PROD_CODE_ART,
            EWConstants.EW_PROD_CODE_JEWELLERY,
            EWConstants.EW_PROD_CODE_PUBLIC_LIABILITY,
            EWConstants.EW_PROD_CODE_EMPLOYERS_LIABILITY,
            EWConstants.EW_PROD_CODE_PEDAL_CYCLE,
            EWConstants.EW_PROD_CODE_TENANTS
    };

    @AuraEnabled
    public static String getReportData(String activeReport, String createdStart, String createdEnd,
            String effectiveStart, String effectiveEnd, Boolean asCsv) {

        if (asCsv == null) {
            asCsv = false;
        }

        String headersCsv = '';

        List<EWCustomReportWrapper.RowCellWrapper> headerRow = new List<EWCustomReportWrapper.RowCellWrapper>();
        List<List<EWCustomReportWrapper.RowCellWrapper>> rows = new List<List<EWCustomReportWrapper.RowCellWrapper>>();

        if (EWCustomReportWrapper.headersByReportName.containsKey(activeReport)) {
            for (String header : EWCustomReportWrapper.headersByReportName.get(activeReport)) {
                headerRow.add(new EWCustomReportWrapper.RowCellWrapper(header));
                headersCsv += '\"' + header + '\",';
            }
            headersCsv = headersCsv.removeEnd(',');
        } else {
            headerRow.add(new EWCustomReportWrapper.RowCellWrapper(''));
        }

        List<EWCustomReportWrapper.RowDataWrapper> items = initializeData(activeReport, createdStart, createdEnd, effectiveStart, effectiveEnd);

        Boolean addStyle = false;
        for (EWCustomReportWrapper.RowDataWrapper row : items) {
            if (row.invalidRow()) {
                continue;
            }

            List<EWCustomReportWrapper.RowCellWrapper> rowLine = row.getRowValues(addStyle);
            rows.add(rowLine);
            addStyle = !addStyle;
        }

        if (asCsv) {
            String csvData = '';
            csvData += headersCsv + '\n';

            for (EWCustomReportWrapper.RowDataWrapper row : items) {
                List<EWCustomReportWrapper.RowCellWrapper> rowLine = row.getRowValues(addStyle);
                for (EWCustomReportWrapper.RowCellWrapper rowItem : rowLine) {
                    csvData += rowItem.val + ',';
                }
                csvData = csvData.removeEnd(',') + '\n';
            }

            return csvData;

        } else {
            Map<String, Object> result = new Map<String, Object>();
            result.put('headers', headerRow);
            result.put('rows', rows);

            String jsonStr = JSON.serialize(result);

            return jsonStr;
        }
    }

    private static List<EWCustomReportWrapper.RowDataWrapper> initializeData(String activeReport, String createdStart, String createdEnd,
            String effectiveStart, String effectiveEnd) {

        String additionalParams = parseSOQLParams(createdStart, createdEnd, effectiveStart, effectiveEnd);

        if (activeReport == 'AIG Report') {
            return initAIGReport(additionalParams);
        } else if (activeReport == 'Cyber Report') {
            return initCyberReport(additionalParams);
        } else if (activeReport == 'Legal Expenses Report') {
            return initARAGReport(additionalParams, EWConstants.EW_PROD_CODE_LEGAL_EXPENSES);
        } else if (activeReport == 'Home Emergency Report') {
            return initARAGReport(additionalParams, EWConstants.EW_PROD_CODE_HOME_EMERGANCY);
        }
        return initNoDataContent();
    }

    private static String parseSOQLParams(String createdStart, String createdEnd, String effectiveStart, String effectiveEnd) {
        String result = '';

        if (String.isNotBlank(createdStart)) {
            Date createdStartDate = Date.valueOf(createdStart);
            createdStartDateTime = Datetime.newInstanceGmt(createdStartDate, Time.newInstance(0, 0, 0, 0));
            result += ' AND CreatedDate >= :createdStartDateTime';
        }
        if (String.isNotBlank(createdEnd)) {
            Date createdEndDate = Date.valueOf(createdEnd);
            createdEndDateTime = Datetime.newInstanceGmt(createdEndDate, Time.newInstance(0, 0, 0, 0));
            result += ' AND CreatedDate <= :createdEndDateTime';
        }
        if (String.isNotBlank(effectiveStart)) {
            effectiveStartDate = Date.valueOf(effectiveStart);
            result += ' AND vlocity_ins__EffectiveDate__c >= :effectiveStartDate';
        }
        if (String.isNotBlank(effectiveEnd)) {
            effectiveEndDate = Date.valueOf(effectiveEnd);
            result += ' AND vlocity_ins__EffectiveDate__c <= :effectiveEndDate';
        }

        return result;
    }

    private static List<EWCustomReportWrapper.RowDataWrapper> initAIGReport(String additionalParams) {
        List<EWCustomReportWrapper.RowDataWrapper> result = new List<EWCustomReportWrapper.AIGReportRowDataWrapper>();

        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>([
                SELECT IBA_PolicyTransaction__c, IBA_PolicyTransaction__r.IBA_OriginalPolicy__c, IBA_OwedtoCarrier__c,
                        IBA_CarrierIPT__c, IBA_CarrierGWP__c, IBA_CarrierBrokerCommission__c, IBA_CarrierMGACommission__c
                FROM IBA_FFPolicyCarrierLine__c
                WHERE IBA_CarrierAccount__r.Name LIKE '%AIG%' AND IBA_PolicyTransaction__r.IBA_OriginalPolicy__r.RecordType.DeveloperName = 'EW_Policy'
        ]);

        Map<String, IBA_FFPolicyCarrierLine__c> transactionIdToCarrierLine = new Map<String, IBA_FFPolicyCarrierLine__c>();
        Set<String> assetsWithProperTransactions = new Set<String>();
        for (IBA_FFPolicyCarrierLine__c pc : policyCarrierLines) {
            if (pc.IBA_PolicyTransaction__c != null) {
                transactionIdToCarrierLine.put(pc.IBA_PolicyTransaction__c, pc);
                assetsWithProperTransactions.add(pc.IBA_PolicyTransaction__r.IBA_OriginalPolicy__c);
            }
        }

        List<IBA_PolicyTransaction__c> policyTransactions = new List<IBA_PolicyTransaction__c>([
                SELECT Id, IBA_OriginalPolicy__c, IBA_TotalGWP__c
                FROM IBA_PolicyTransaction__c
                WHERE IBA_OriginalPolicy__r.RecordType.DeveloperName = 'EW_Policy' AND IBA_TotalGWP__c = 0
        ]);

        Set<String> assetsWithZeroGWPTransactions = new Set<String>();

        for (IBA_PolicyTransaction__c pt : policyTransactions) {
            assetsWithZeroGWPTransactions.add(pt.IBA_OriginalPolicy__c);
        }

        assetsWithProperTransactions.addAll(assetsWithZeroGWPTransactions);

        String query = 'SELECT Id, IBA_AIGProductName__c, Name, EW_AIGPolicyNumber__c, vlocity_ins__EffectiveDate__c, vlocity_ins__ExpirationDate__c, Brokerage_DR_Code__c,'
                + '  EW_Payment_Method__c, IBA_BrokerCommissionRate__c, AccountId, EW_CoverageType__c, EW_Excess__c, EW_TypeofTransaction__c, '
                + '  IBA_ProducingBrokerAccount__r.EW_DRCode__c, CurrencySymbol__c, IBA_InsurancePremiumTaxRate__c, EW_Quote__r.EW_AzurCommissionPercent__c, '
                + '  EW_FixedExpense__c, EW_FixedExpenseExIPT__c, IBA_ProducingBrokerAccount__r.Name, Status, EW_Quote__c,'
                + '  (SELECT Id, IBA_TotalBrokerCommission__c, IBA_TotalMGACommission__c, IBA_TotalInsurancePremiumTax__c'
                + '    FROM Policy_Transactions__r),'
                + '  (SELECT EW_RebuildCost__c FROM vlocity_ins__InsuredItems__r WHERE Name = \'Main residence\' LIMIT 1),'
                + '  (SELECT Id, Name, EW_PremiumGrossExIPT__c, EW_SpecifiedItemValue__c, vlocity_ins__Product2Id__c, vlocity_ins__Product2Id__r.ProductCode '
                + '    FROM vlocity_ins__PolicyCoverages__r)'
                + ' FROM Asset'
                + ' WHERE Id IN :assetsWithProperTransactions AND RecordTypeId = :ewPolicy '
                + ' AND Status != \'Draft\' '
                + additionalParams;

        Map<Id, sObject> policies = new Map<Id, sObject>(Database.query(query));
        Set<Id> accountIds = new Set<Id>();
        Set<Id> policyIds = new Set<Id>();

        for (sObject policyObj : policies.values()) {
            accountIds.add(((Asset) policyObj).AccountId);
            policyIds.add(policyObj.Id);
        }

        // In some cases, Asset.fixedExpenseGrossExIPT is randomly not populated. In those cases,
        // query for IBA_GrossWrittenPremium__c inside the Policy Transaction Line and use that instead.
        String policyTransactionsWithGWPQuery = 'SELECT IBA_OriginalPolicy__c, '
                + '(SELECT IBA_GrossWrittenPremium__c FROM Policy_Transaction_Lines__r WHERE IBA_Product__r.Name = \'Fixed Expense\') '
                + 'FROM IBA_PolicyTransaction__c WHERE IBA_OriginalPolicy__c IN :policyIds';

        List<IBA_PolicyTransaction__c> policyTransactionsWithGWP = Database.query(policyTransactionsWithGWPQuery);
        Map<Id, Double> GWPsByPolicy = new Map<Id, Double>();

        for (IBA_PolicyTransaction__c pt : policyTransactionsWithGWP) {
            if (!pt.Policy_Transaction_Lines__r.isEmpty()) {
                GWPsByPolicy.put(pt.IBA_OriginalPolicy__c, pt.Policy_Transaction_Lines__r[0].IBA_GrossWrittenPremium__c);
            }
        }

        Map<Id, Account> accounts = new Map<Id, Account>([
                SELECT Id, Salutation, Name, FirstName, LastName, Building_name__c, EW_BuildingName__pc, BillingStreet,
                        EW_BuildingNumber__pc, BillingCity, BillingCountry, BillingPostalCode, EW_BrokerAccount__c,
                        EW_BrokerAccount__r.Name, EW_BrokerAccount__r.EW_DRCode__c, vlocity_ins__MiddleName__pc,
                        PersonBirthdate, EW_Occupation__pr.Name, EW_Nationality__pr.Name, EW_FlatName__pc, PersonMailingStreet,
                        PersonMailingCity, PersonMailingCountry, PersonMailingPostalCode
                FROM Account
                WHERE Id IN :accountIds AND RecordTypeId = :personAccount
        ]);

        Map<Id, Map<String, QuoteLineItem>> lineItemsByPolicyId = fetchQuoteItemsByPolicyIds(policies.values(), productCodesAigReport);

        for (sObject policyObj : policies.values()) {
            Asset policy = (Asset) policyObj;

            // Use policy.EW_FixedExpenseExIPT__c, if there is one. Otherwise, use IBA_GrossWrittenPremium__c from
            // the IP.
            policy.EW_FixedExpenseExIPT__c = GWPsByPolicy.get(policy.Id) != null ? GWPsByPolicy.get(policy.Id) : 0;

            for (IBA_PolicyTransaction__c trans : policy.Policy_Transactions__r) {
                IBA_FFPolicyCarrierLine__c carrierLine;
                if (transactionIdToCarrierLine.containsKey(trans.Id)) {
                    carrierLine = transactionIdToCarrierLine.get(trans.Id);
                }

                List<QuoteLineItem> lineItems = new List<QuoteLineItem>();
                if (lineItemsByPolicyId.containsKey(policy.Id)) {
                    lineItems = lineItemsByPolicyId.get(policy.Id).values();
                }

                result.add(new EWCustomReportWrapper.AIGReportRowDataWrapper(policy, carrierLine, accounts.get(policy.AccountId), lineItems));
            }
        }

        return result;
    }

    private static List<EWCustomReportWrapper.RowDataWrapper> initCyberReport(String additionalParams) {
        String productCode = EWConstants.EW_PROD_CODE_PERSONAL_CYBER;
        List<EWCustomReportWrapper.RowDataWrapper> result = new List<EWCustomReportWrapper.CyberReportRowDataWrapper>();

        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>([
                SELECT IBA_PolicyTransaction__c, IBA_PolicyTransaction__r.IBA_OriginalPolicy__c, IBA_OwedtoCarrier__c
                FROM IBA_FFPolicyCarrierLine__c
                WHERE IBA_CarrierAccount__r.Name LIKE '%HSB%'
        ]);

        Set<String> policyTransactionIds = new Set<String>();
        Set<String> assetsWithProperTransactions = new Set<String>();
        for (IBA_FFPolicyCarrierLine__c pc : policyCarrierLines) {
            if (pc.IBA_PolicyTransaction__c != null) {
                policyTransactionIds.add(pc.IBA_PolicyTransaction__c);
                assetsWithProperTransactions.add(pc.IBA_PolicyTransaction__r.IBA_OriginalPolicy__c);
            }
        }

        Map<Id, IBA_PolicyTransaction__c> transactionRecordsWithLines = fetchTransactionRecordsWithSpecificLine(policyTransactionIds, productCode);

        String query = 'SELECT Id, CreatedDate, Name, Status, Account.FirstName, Account.vlocity_ins__MiddleName__pc, Account.LastName,'
                + ' Account.EW_BuildingNumber__pc, Account.EW_BuildingName__pc, Account.EW_FlatName__pc, EW_TypeofTransaction__c, '
                + ' Account.PersonMailingStreet, Account.PersonMailingCity, Account.PersonMailingCountry,'
                + ' Account.PersonMailingPostalCode, IBA_MGACommissionRate__c, IBA_BrokerCommissionRate__c,'
                + ' IBA_InsurancePremiumTaxRate__c, Account.Salutation, Policy_Status__c, EW_Quote__c,'
                + ' vlocity_ins__EffectiveDate__c, vlocity_ins__ExpirationDate__c, vlocity_ins__InceptionDate__c,'
                + ' (SELECT Id, Name FROM Policy_Transactions__r WHERE Id IN :policyTransactionIds),'
                + ' (SELECT EW_PremiumGrossExIPT__c, EW_SpecifiedItemValue__c, EW_PremiumNet__c, EW_IPTTotal__c ' +
                +'        FROM vlocity_ins__PolicyCoverages__r WHERE vlocity_ins__Product2Id__r.ProductCode = \'personalCyber\')'
                + ' FROM Asset'
                + ' WHERE Id IN :assetsWithProperTransactions '
                + '    AND RecordType.DeveloperName = \'EW_Policy\' '
                + '    AND EW_TypeofTransaction__c IN :typeOfTransactions '
                + additionalParams;

        List<Asset> relatedAssets = (List<Asset>) Database.query(query);

        Map<Id, Map<String, QuoteLineItem>> lineItemsByPolicyId = fetchQuoteItemsByPolicyIds(relatedAssets, new Set<String>{
                productCode
        });
        for (Asset policy : relatedAssets) {
            if (!policy.vlocity_ins__PolicyCoverages__r.isEmpty()) {
                for (IBA_PolicyTransaction__c trans : policy.Policy_Transactions__r) {
                    IBA_PolicyTransactionLine__c policyTransactionLine = transactionRecordsWithLines.get(trans.Id).Policy_Transaction_Lines__r.get(0);
                    QuoteLineItem lineItem = null;
                    if (lineItemsByPolicyId.containsKey(policy.Id)) {
                        lineItem = lineItemsByPolicyId.get(policy.Id).get(productCode);
                    }

                    result.add(
                            new EWCustomReportWrapper.CyberReportRowDataWrapper(
                                    policy, policyTransactionLine, trans, lineItem
                            )
                    );
                }
            }
        }

        return result;
    }

    private static List<EWCustomReportWrapper.RowDataWrapper> initARAGReport(String additionalParams, String productCode) {
        List<EWCustomReportWrapper.RowDataWrapper> result = new List<EWCustomReportWrapper.ARAGReportRowDataWrapper>();

        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>([
                SELECT IBA_PolicyTransaction__c, IBA_PolicyTransaction__r.IBA_OriginalPolicy__c, IBA_OwedtoCarrier__c
                FROM IBA_FFPolicyCarrierLine__c
                WHERE IBA_CarrierAccount__r.Name LIKE '%ARAG%'
        ]);

        Set<String> policyTransactionIds = new Set<String>();

        Set<String> assetsWithProperTransactions = new Set<String>();
        for (IBA_FFPolicyCarrierLine__c pc : policyCarrierLines) {
            if (pc.IBA_PolicyTransaction__c != null) {
                policyTransactionIds.add(pc.IBA_PolicyTransaction__c);
                assetsWithProperTransactions.add(pc.IBA_PolicyTransaction__r.IBA_OriginalPolicy__c);
            }
        }

        Map<Id, IBA_PolicyTransaction__c> transactionRecordsWithLines = fetchTransactionRecordsWithSpecificLine(policyTransactionIds, productCode);

        Set<String> coverageAssetIds = fetchCoverageAssetIds(productCode);

        String query = 'SELECT Id, Name, Status, Policy_Status__c, vlocity_ins__InceptionDate__c, vlocity_ins__ExpirationDate__c,'
                + ' Account.Salutation, Account.FirstName, Account.vlocity_ins__MiddleName__pc, Account.LastName,'
                + ' Account.EW_BuildingNumber__pc, Account.EW_BuildingName__pc, Account.EW_FlatName__pc, EW_TypeofTransaction__c, '
                + ' Account.PersonMailingStreet, Account.PersonMailingCity, Account.PersonMailingCountry,'
                + ' Account.PersonMailingPostalCode, Account.PersonBirthdate, IBA_MGACommissionRate__c, IBA_BrokerCommissionRate__c,'
                + ' IBA_InsurancePremiumTaxRate__c, IBA_OwedtoCarrier__c, EW_Quote__c,'
                + ' (SELECT Id, Name FROM Policy_Transactions__r WHERE Id IN :policyTransactionIds),'
                + ' (SELECT EW_PremiumGrossExIPT__c, EW_PremiumNet__c, EW_IPTTotal__c ' +
                +'        FROM vlocity_ins__PolicyCoverages__r WHERE vlocity_ins__Product2Id__r.ProductCode = :productCode)'
                + ' FROM Asset'
                + ' WHERE Id IN :coverageAssetIds AND Id IN :assetsWithProperTransactions '
                + '    AND RecordType.DeveloperName = \'EW_Policy\' '
                + '    AND EW_TypeofTransaction__c IN :typeOfTransactions '
                + additionalParams;

        List<Asset> relatedAssets = (List<Asset>) Database.query(query);

        Map<Id, Map<String, QuoteLineItem>> lineItemsByPolicyId = fetchQuoteItemsByPolicyIds(relatedAssets, new Set<String>{
                productCode
        });

        for (Asset policy : relatedAssets) {
            for (IBA_PolicyTransaction__c trans : policy.Policy_Transactions__r) {
                IBA_PolicyTransactionLine__c policyTransactionLine = transactionRecordsWithLines.get(trans.Id).Policy_Transaction_Lines__r.get(0);
                QuoteLineItem lineItem = null;
                if (lineItemsByPolicyId.containsKey(policy.Id)) {
                    lineItem = lineItemsByPolicyId.get(policy.Id).get(productCode);
                }
                result.add(new EWCustomReportWrapper.ARAGReportRowDataWrapper(policy, policyTransactionLine, trans, lineItem));
            }
        }

        return result;
    }

    private static Map<Id, Map<String, QuoteLineItem>> fetchQuoteItemsByPolicyIds(List<Asset> relatedAssets, Set<String> productCodes) {
        Set<Id> quoteIds = new Set<Id>();
        for (Asset policy : relatedAssets) {
            quoteIds.add(policy.EW_Quote__c);
        }
        List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>([
                SELECT Id, EW_Transactional_PremiumGrossExIPT__c, EW_Transactional_PremiumNet__c,
                        EW_Transactional_IPT__c, Quote.EW_Policy__c, Product2.Name, Product2.ProductCode
                FROM QuoteLineItem
                WHERE Product2.ProductCode IN :productCodes AND QuoteId IN :quoteIds
                AND Quote.EW_Policy__c != NULL AND EW_Transactional_PremiumGrossExIPT__c != NULL
        ]);

        Map<Id, Map<String, QuoteLineItem>> result = new Map<Id, Map<String, QuoteLineItem>>();
        for (QuoteLineItem qli : quoteLineItems) {
            Id policyId = qli.Quote.EW_Policy__c;
            if (result.containsKey(policyId)) {
                result.get(policyId).put(qli.Product2.ProductCode, qli);
            } else {
                result.put(policyId, new Map<String, QuoteLineItem>{
                        qli.Product2.ProductCode => qli
                });
            }
        }
        return result;
    }

    private static List<EWCustomReportWrapper.RowDataWrapper> initNoDataContent() {
        List<EWCustomReportWrapper.RowDataWrapper> result = new List<EWCustomReportWrapper.NoDataRowDataWrapper>();

        result.add(new EWCustomReportWrapper.NoDataRowDataWrapper());
        return result;
    }

    private static Map<Id, IBA_PolicyTransaction__c> fetchTransactionRecordsWithSpecificLine(Set<String> policyTransactionIds, String productCode) {
        Map<Id, IBA_PolicyTransaction__c> transactionRecordsWithLines = new Map<Id, IBA_PolicyTransaction__c>([
                SELECT Id, (
                        SELECT Id, IBA_NetPayabletoCarrier__c
                        FROM Policy_Transaction_Lines__r
                        WHERE IBA_Product__r.ProductCode = :productCode
                        LIMIT 1
                )
                FROM IBA_PolicyTransaction__c
                WHERE Id IN :policyTransactionIds
        ]);

        return transactionRecordsWithLines;
    }

    private static Set<String> fetchCoverageAssetIds(String expectedProductCode) {
        List<vlocity_ins__AssetCoverage__c> coverages = new List<vlocity_ins__AssetCoverage__c>([
                SELECT vlocity_ins__PolicyAssetId__c
                FROM vlocity_ins__AssetCoverage__c
                WHERE vlocity_ins__Product2Id__r.ProductCode = :expectedProductCode
        ]);

        Set<String> result = new Set<String>();
        for (vlocity_ins__AssetCoverage__c pc : coverages) {
            result.add(pc.vlocity_ins__PolicyAssetId__c);
        }

        return result;
    }
}