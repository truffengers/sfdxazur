/**
* @name: EWAccountDataFactory
* @description: Reusable factory methods for account object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 05/09/2018
*
* @lastChangedBy: Antigoni D'Mello 01/11/2018 EWH-153 Replacing old Brokerage Code field
*/
@isTest
public class EWAccountDataFactory {

    public static Account createBrokerAccount(String name,
                                              Boolean insertRecord) {

        Id brokerRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EWConstants.ACCOUNT_BROKER_RECORD_TYPE).getRecordTypeId();
        Account acc = new Account();
        acc.name = (name == null) ? 'testBroker' : name;
        acc.EW_DRCode__c = 'DR' + Datetime.now().getTime();
        acc.BillingStreet = '1 ApexTest BillingStreet';
        acc.BillingCity = 'London';
        acc.BillingPostalCode = 'AP1 2TE';
        acc.BillingCountry = 'United Kingdom';
        acc.RecordTypeId = brokerRecId; 
        acc.EW_MaximumCommission__c = 15;
        acc.EW_MinimumCommission__c = 5;
        acc.EW_DefaultCommission__c = 10;
        if (insertRecord) {
            insert acc;
        }

        return acc;
    }

    public static Account createInsuredAccount(String salutation,
                                               String firstName,
                                               String lastName,
                                               Date birthDate,
                                               Boolean insertRecord) {

        Id insuredRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EWConstants.ACCOUNT_PERSON_RECORD_TYPE).getRecordTypeId();
        Account acc = new Account();
        acc.Salutation = salutation;
        acc.FirstName = firstName;
        acc.LastName = lastName;
        acc.PersonBirthdate = birthDate;
        acc.Marital_Status__c = 'Married';
        acc.BillingStreet = '1 ApexTest InsuredPerson';
        acc.BillingCity = 'London';
        acc.BillingPostalCode = 'AP2 1TE';
        acc.BillingCountry = 'United Kingdom';
        acc.PersonEmail = 'person.test@testEmail.com';
        acc.gender__pc = 'Male';
        acc.RecordTypeId = insuredRecId;

        if (insertRecord) {
            insert acc;
        }

        return acc;
    }
}