/**
* @name: EWUnderwriterRulesHelper
* @description: Helper class for Underwriter Rules
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 30/1/2019
*/

global with sharing class EWUnderwriterRulesHelper {

    private static Set<String> UNDERWRITING_OUTCOME_SET = new Set<String>{'declined', 'referred'};

    public static Map<String,Object> postUWrules (Map<String,Object> rulesMap){

        Map<String,Object> outputMap = new Map<String,Object>();
        Boolean isSuccess = false;

        // parse the Omniscript data
        UwRules uwRules = parseUWrules(rulesMap);

        // get the application UW rules
        List<vlocity_ins__ProductRequirement__c> productRules = [SELECT Id, Name, vlocity_ins__Product2Id__r.Name, vlocity_ins__Conditions__c,vlocity_ins__Message__c,vlocity_ins__Product2Id__c,vlocity_ins__StateTransitionName__c FROM vlocity_ins__ProductRequirement__c WHERE vlocity_ins__IsActive__c = true];
        if(productRules.isEmpty()){
            return new Map<String,Object>{'success' => isSuccess};
        }

        // get the rules from the uwRules
        List<Rule> triggeredUWrules = new List<Rule>();
        if(uwRules.declined != null && uwRules.declined.rules != null){
            triggeredUWrules.addAll(uwRules.declined.rules);
            outputMap.putAll(new Map<String,Object>{'declinedRuleMessages' => getMessages(uwRules.declined.rules)});
        }
        if(uwRules.referred != null && uwRules.referred.rules != null){
            triggeredUWrules.addAll(uwRules.referred.rules);
            outputMap.putAll(new Map<String,Object>{'referredRuleMessages' => getMessages(uwRules.referred.rules)});
        }

        // create the Underwriting Rules for the Quote record
        List<EW_Underwriting_Rule__c> newUWrules = new List<EW_Underwriting_Rule__c>();
        for(Rule triggeredRule : triggeredUWrules){

            if(triggeredRule.ruleDetails != null){
                RuleDetails uwRule = triggeredRule.ruleDetails;

                if(String.isNotBlank(uwRule.objectId)) {

                    for (vlocity_ins__ProductRequirement__c productRule : productRules) {

                        if (uwRule.productId == productRule.vlocity_ins__Product2Id__c &&
                            uwRule.transitionName == productRule.vlocity_ins__StateTransitionName__c &&
                            uwRule.conditions == productRule.vlocity_ins__Conditions__c &&
                            uwRule.message == productRule.vlocity_ins__Message__c
                        ) {
                                EW_Underwriting_Rule__c newRule = new EW_Underwriting_Rule__c(
                                    EW_Product_Requirement__c = productRule.Id,
                                    EW_Quote__c = uwRule.objectId,
                                    EW_ProductRequirementName__c = productRule.Name,
                                    EW_ProductName__c = productRule.vlocity_ins__Product2Id__r.Name);
                            newUWrules.add(newRule);
                        }
                    }
                }
            }
        }

        List<Database.SaveResult> srList = Database.insert(newUWrules, false);
        for (Database.SaveResult sr : srList) {
            isSuccess = sr.isSuccess();
        }

        outputMap.put('success', isSuccess);
        return outputMap;
    }

    /**
    *  v2: 10/08/2020 ajedamenko@azuruw.com - usedConditions added to prevent rules duplication
    */
    private static UwRules parseUWrules (Map<String,Object> rulesMap){

        Map<String,Object> uwRuleMap = new Map<String,Object>();
        for(String uwOutcome : UNDERWRITING_OUTCOME_SET){

            if(rulesMap.containsKey(uwOutcome)){

                Map<String,Object> outcomeMap = (Map<String,Object>) rulesMap.get(uwOutcome);
                List<Rule> allRules = new List<Rule>();
                Set<String> usedConditions = new Set<String>();

                for(String productCode : outcomeMap.keySet()){
                    String rulesMapJson = JSON.serialize(outcomeMap.get(productCode));
                    List<Rule> rules = (List<Rule>) JSON.deserialize(rulesMapJson, List<Rule> .class);
                    for (Rule rule : rules) {

                        if (!usedConditions.contains(rule.ruleDetails.conditions)) {

                            allRules.add(rule);
                            usedConditions.add(rule.ruleDetails.conditions);
                        }
                    }
                }

                if(!allRules.isEmpty()){
                    Outcome outcome = new Outcome();
                    outcome.rules = allRules;

                    uwRuleMap.put(uwOutcome, outcome);
                }
            }
        }
        return (uwRules) JSON.deserialize(JSON.serialize(uwRuleMap), uwRules .class);
    }

    private static List<Map<String,Object>> getMessages(List<Rule> rules){
        List<Map<String,Object>> messages = new List<Map<String,Object>>();

        // Vlocity returning duplicate failure messages, so dedupe underwriter messages first
        Set<String> dedupedMessages = new Set<String>();
        for(Rule r : rules){
            dedupedMessages.add(r.ruleDetails.message);
        }

        for(String s : dedupedMessages){
            Map<String,Object> message = new Map<String,Object>{'message' => (Object) s};
            messages.add(message);
        }
        return messages;
    }


    public void updateUWrules(Map<Id, SObject> triggerNewMap, Map<Id, SObject> triggerOldMap){
        // called by QuoteTriggerHandler
        // updates the child Underwriting Rules when a quote is updated to UW Approved

        Set<Id> approvedQuoteIds = new Set<Id>();

        for(String id : triggerNewMap.keySet()){

            Quote newQuote = (Quote) triggerNewMap.get(Id);
            Quote oldQuote = (Quote) triggerOldMap.get(Id);

            if(newQuote.Status == EWConstants.QUOTE_STATUS_UW_APPROVED && newQuote.Status != oldQuote.Status){
                approvedQuoteIds.add(newQuote.Id);
            }
        }

        if(!approvedQuoteIds.isEmpty()){

            DateTime now = DateTime.now();
            List<EW_Underwriting_Rule__c> uwRules = [SELECT Id FROM EW_Underwriting_Rule__c WHERE EW_Quote__c in: approvedQuoteIds AND EW_Approved_By__c = NULL];

            for(EW_Underwriting_Rule__c uwRule : uwRules){
                uwRule.EW_Approved_By__c = UserInfo.getUserId();
                uwRule.EW_Approval_Date__c = now;
            }

            update uwRules;
        }

    }


    public class UwRules{
        public Outcome declined;
        public Outcome referred;
    }

    public class Outcome{
        public List<Rule> rules;

        public Outcome(){}
    }

    public class Rule{
        public RuleDetails ruleDetails;
    }

    public class RuleDetails{
        public String objectId;
        public String transitionName;
        public String productId;
        public String message;
        public String conditions;

    }


}