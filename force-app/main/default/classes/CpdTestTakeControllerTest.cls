/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 19/10/2017
 */

@isTest
private class CpdTestTakeControllerTest {
    private static TestObjectGenerator tog = new TestObjectGenerator();

    private static PageReference pageRef;
    private static Map<String, String> params;

    static {
        tog.getUserContact();

        pageRef = Page.CpdTestTake;
        Test.setCurrentPage(pageRef);
        params = pageRef.getParameters();
    }

    @isTest static void testBehavior() {
        IQ_Content__c iqc = tog.getWebcastIqContent();
        CPD_Test__c cpdTest = tog.getCpdTest();
        cpdTest.Status__c = 'Published';
        CPD_Test_Question__c cpdTestQuestion = tog.getCpdTestQuestion();
        update cpdTest;
        CPD_Assessment__c ass = tog.getCpdAssessment();

        params.put('iqContentId', iqc.Id);

        CpdTestTakeController controller = new CpdTestTakeController();

        System.assertEquals(cpdTest.Id, controller.theTest.Id);

        controller.getAssessment();

        System.assertEquals(ass.Id, controller.thisAssessment.Id);

        controller.currentAnswer = '1';

        controller.next();

        System.assertEquals('Submitted', controller.thisAssessment.Status__c);
    }

    @isTest static void createAssessment() {
        IQ_Content__c iqc = tog.getWebcastIqContent();
        CPD_Test__c cpdTest = tog.getCpdTest();
        cpdTest.Status__c = 'Published';
        CPD_Test_Question__c cpdTestQuestion = tog.getCpdTestQuestion();
        update cpdTest;

        params.put('iqContentId', iqc.Id);

        CpdTestTakeController controller = new CpdTestTakeController();

        System.assertEquals(cpdTest.Id, controller.theTest.Id);

        controller.getAssessment();

        System.assertNotEquals(null, controller.thisAssessment.Id);
    }
}