TT.carousel = function($, lazySizes, Hammer, TT, settings) {

  var self = {};
  var $self = $(self);
  var carouselCore;
  var pagination;
  var arrows;
  var initd;
  var $container;
  var $slidesContainer;
  var count;



  function setCount() {
    count = settings.core.data.length;
  }

  function initCarouselCore() {
    carouselCore = TT.carouselCore($, lazySizes, Hammer, TT.fsm, $.extend(settings.core, {
      loading     : carouselLoadingHandler,
      loaded      : carouselLoadedHandler,
      loadedFirst : carouselLoadedFirstHandler,
      changed     : carouselChangedHandler,
      revealed    : carouselRevealedHandler
    }));
    carouselCore.init();
  }

  function initPagination() {
    if(settings.pagination) {
      pagination = TT.carouselPagination($, settings.pagination);
      pagination.init({ callback: dotSelectHandler, count: count });
    }
  }

  function initArrows() {
    if(settings.arrows) {
      arrows = TT.carouselArrows($, $.extend(settings.arrows, {
        previous : carouselCore.previous,
        next     : carouselCore.next
      }));
      arrows.init(count);
    }
  }

  function carouselLoadingHandler(index, $slide) {

  }

  function carouselLoadedHandler(index, $slide) {
    $self.triggerHandler('loaded', [index, $slide]);
    if(pagination) { pagination.update(index); }
  }

  function carouselLoadedFirstHandler() {
  }

  function carouselRevealedHandler(index, $slide) {
    $self.triggerHandler('revealed', [index, $slide]);
  }

  function carouselChangedHandler(index, length) {
    if(arrows) { arrows.update(index, length); }
  }

  function dotSelectHandler(index) {
    carouselCore.select(index);
  }

  function terminate() {
    carouselCore.terminate();
    if(arrows) { arrows.terminate(); }
    if(pagination) { pagination.terminate(); }
    carouselCore     = undefined;
    pagination       = undefined;
    arrows           = undefined;
    $container       = undefined;
    $slidesContainer = undefined;
    initd            = false;
  }



  $.extend(self, {

    init: function() {
      if(initd) { terminate(); }
      setCount();
      initCarouselCore();
      initPagination();
      initArrows();
      initd = true;
    }

  });


  return self;

};