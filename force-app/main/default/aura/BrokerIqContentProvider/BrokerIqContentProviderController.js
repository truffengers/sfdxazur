/**
* @name: BrokerIqContentProviderController.js
* @description: BrokerIqContentProvider javascript controller
* @author: Nebula Consulting 
* @date: 14/10/2017
* @Modified: Ignacio Sarmiento	18/12/2017	Add 'isRenderCPDButton' attribute and functionality
*/
({
    doInit : function(component, event, helper) {
        var recordId = component.get('v.recordId');

        if(!$A.util.isEmpty(recordId)) {
            var getIqContentAction = component.get('c.getIqContentInformation');

            getIqContentAction.setParam('recordId', recordId);

            getIqContentAction.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    var iqContentRecord = result.iqContent;
                    var isRenderCPDButton = result.isRenderCPDButton;

                    var bgset = '';
                    for(var field in iqContentRecord) {
                        var matchResult = /Carousel_Image_([\d]+)_Cache__c/.exec(field);

                        if(matchResult) {
                            var urlVal = iqContentRecord[field];

                            if(urlVal) {
                                bgset += urlVal + ' ' + matchResult[1] + 'w, ';
                            }
                        }
                    }
                    if(bgset.length > 0) {
                        bgset.substring(0, bgset.length - ', '.length);
                    }

                    component.set('v.bgset', bgset);
                    component.set('v.iqContentRecord', iqContentRecord);
                    component.set('v.isRenderCPDButton', isRenderCPDButton);
                }
            });
            
            $A.enqueueAction(getIqContentAction);   
        }
    }
})