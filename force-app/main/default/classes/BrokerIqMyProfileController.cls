public class BrokerIqMyProfileController extends PortalController{
    
    @AuraEnabled
    public static User getUser() {
        return new BrokerIqMyProfileController().thisUser;
    }

    @AuraEnabled
    public static User doSave(User userToUpdate, String currentPassword, String newPassword, String verifyNewPassword) {
        
        BrokerIqMyProfileController controller = new BrokerIqMyProfileController();

        if((currentPassword == null || currentPassword == '') && !(Test.isRunningTest() && forceCorrectPassword)) {
            throw new AuraHandledException(Label.Password_Required);
        }
        PageReference loginResult;
        try {
            loginResult = Site.login(userToUpdate.username, currentPassword, null);
        } catch(SecurityException e) {
            throw new AuraHandledException(Label.Current_password_incorrect);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        if(loginResult == null && !(Test.isRunningTest() && forceCorrectPassword)) {
            throw new AuraHandledException(Label.Current_password_incorrect);
        } else {
            if(newPassword != null && newPassword != '') {
                Site.changePassword(newPassword, verifyNewPassword, currentPassword);
            }
            
            update userToUpdate;
            // Don't mess around with the username for internal users
            try {
                if(controller.userType != 'Standard') {
                    userToUpdate.Username = userToUpdate.Email;
                }
                update userToUpdate;
            } catch(Exception e) {
                // We try to keep username/email the same, but
                // don't worry if we cannot
            }
            sendEmailNotification();                
        }
        return userToUpdate;
    }

    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}
    public String currentPassword {get; set;}
    public static Boolean forceCorrectPassword = false;
    
    public BrokerIqMyProfileController() {        
    }
    
    private void stayInEditMode() {
        ApexPages.currentPage().getParameters().put('edit', 'true');
    }
    
    public void save() {
        if((currentPassword == null || currentPassword == '') && !(Test.isRunningTest() && forceCorrectPassword)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Password_Required));
            stayInEditMode();
            return;
        }
        PageReference loginResult = Site.login(thisUser.username, currentPassword, null);
        if(loginResult == null && !(Test.isRunningTest() && forceCorrectPassword)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Current_password_incorrect));
            stayInEditMode();
        } else {
            try {
                if(newPassword != null && newPassword != '') {
                    Site.changePassword(newPassword, verifyNewPassword, currentPassword);
                }
                
                update thisUser;
                // Don't mess around with the username for internal users
                try {
                    if(userType != 'Standard') {
                        thisUser.Username = thisUser.Email;
                    }
                    update thisUser;
                } catch(Exception e) {
                    // We try to keep username/email the same, but
                    // don't worry if we cannot
                }
                sendEmailNotification();                
            } catch(Exception e) {
                ApexPages.addMessages(e);
	            stayInEditMode();
            }
        }
    }
    
    private static void sendEmailNotification() {
        List<EmailTemplate> template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Broker_iQ_User_Updated'];
        if(!template.isEmpty()) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new List<String>{ UserInfo.getUserId() };
            message.templateId = template[0].Id;
            message.targetObjectId = UserInfo.getUserId();
            message.saveAsActivity = false;
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {message});
        }
    }
}