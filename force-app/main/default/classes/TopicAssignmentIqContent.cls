global class TopicAssignmentIqContent implements Nebula_Tools.AfterInsertI, Nebula_Tools.AfterDeleteI {
	
	global void handleAfterInsert(List<TopicAssignment> newList) {
		
		List<TopicAssignment> interestingTopicAssignments = new List<TopicAssignment>();
		Set<Id> topicIds = new Set<Id>();
		for(TopicAssignment thisTopicAssignment : newList) {
			System.debug(thisTopicAssignment);
			if(thisTopicAssignment.EntityType == 'IQ_Content' && thisTopicAssignment.NetworkId == null) {
				interestingTopicAssignments.add(thisTopicAssignment);
				topicIds.add(thisTopicAssignment.TopicId);
			}
		}

		if(interestingTopicAssignments.isEmpty()) {
			return;
		}

		Map<Id, Topic> internalTopics = new Map<Id, Topic>([SELECT Id, Name FROM Topic WHERE Id IN :topicIds]);
		Set<String> topicNames = new Set<String>();
		for(Topic t : internalTopics.values()) {
			topicNames.add(t.Name);
		}

		List<Network> networks = [SELECT Id FROM Network];
		Nebula_Tools.sObjectIndex topicsByNetworkIdAndName = new Nebula_Tools.sObjectIndex.Builder()
			.setIndexFields(new List<String>{'NetworkId', 'Name'})
			.setData([SELECT Id, NetworkId, Name 
			  FROM Topic 
			  WHERE NetworkId != null
			  AND Name IN :topicNames])
			.setIsCaseInsensitive(true)
			.build();

		List<Topic> topicsToInsert = new List<Topic>();

		for(Topic internalTopic : internalTopics.values()) {
			for(Network thisNetwork : networks) {
				Topic topicToMaybeInsert = new Topic(Name = internalTopic.Name, 
				                                          NetworkId = thisNetwork.Id);
				if(topicsByNetworkIdAndName.get(topicToMaybeInsert) == null) {
					topicsToInsert.add(topicToMaybeInsert);
					topicsByNetworkIdAndName.put(topicToMaybeInsert);
				}
			}
		}

		if(!topicsToInsert.isEmpty()) {
			insert topicsToInsert;
		}

		// SF seems to just quietly prune duplicates, so we insert everything
		List<TopicAssignment> topicAssignmentsToInsert = new List<TopicAssignment>();

		for(TopicAssignment thisTopicAssignment : interestingTopicAssignments) {
			for(Network thisNetwork : networks) {
				Topic searchTopic = new Topic(Name = internalTopics.get(thisTopicAssignment.TopicId).Name, 
    	                                      NetworkId = thisNetwork.Id);

				topicAssignmentsToInsert.add(new TopicAssignment(EntityId = thisTopicAssignment.EntityId,
				                                             TopicId = topicsByNetworkIdAndName.get(searchTopic).Id,
				                                             NetworkId = thisNetwork.Id));
			}
		}

		insert topicAssignmentsToInsert;
	}

	global void handleAfterDelete(List<TopicAssignment> oldList) {
		Set<Id> entityIds = new Set<Id>();
		Set<String> topicIds = new Set<String>();
		List<TopicAssignment> interestingTopicAssignments = new List<TopicAssignment>();

		for(TopicAssignment thisTopicAssignment : oldList) {
			if(thisTopicAssignment.EntityType == 'IQ_Content') {
				entityIds.add(thisTopicAssignment.EntityId);
				topicIds.add(thisTopicAssignment.TopicId);
				interestingTopicAssignments.add(thisTopicAssignment);
			}
		}

		if(interestingTopicAssignments.isEmpty()) {
			return;
		}

		Map<Id, Topic> topics = new Map<Id, Topic>([SELECT Id, Name FROM Topic WHERE Id IN :topicIds]);
		Set<String> topicNames = new Set<String>();
		for(Topic t : topics.values()) {
			topicNames.add(t.Name);
		}


		Nebula_Tools.sObjectIndex topicAssignmentsByEntityIdAndName = new Nebula_Tools.sObjectIndex.Builder()
                .setIndexFields(new List<String>{'EntityId', 'Topic.Name'})
                .setData([SELECT Id, EntityId, Topic.Name
                FROM TopicAssignment
                WHERE Topic.Name IN :topicNames
                AND EntityId IN :entityIds])
                .setIsCaseInsensitive(true)
                .build();

		List<TopicAssignment> toDelete = new List<TopicAssignment>();

		for(TopicAssignment thisTopicAssignment : interestingTopicAssignments) {
			toDelete.addAll((List<TopicAssignment>)topicAssignmentsByEntityIdAndName
                .getAll(new TopicAssignment(EntityId = thisTopicAssignment.EntityId,
                            Topic = new Topic(Name = topics.get(thisTopicAssignment.TopicId).Name)))
				);
		}

		if(!toDelete.isEmpty()) {
			delete toDelete;
		}
	}
}