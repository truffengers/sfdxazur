public with sharing class IXAnalyticsHelper {
    @AuraEnabled
    public static void createAnalyticsEvent(String eventName, String sessionID, String eventDescription, String eventData) {
        User currentUser = [
                SELECT Id, ContactId, Contact.Account.Name
                FROM User
                WHERE Id = :UserInfo.getUserId()
        ];
        
        String brokerageName = '';
        if(!String.isBlank(currentUser.ContactId)){
			brokerageName = currentUser.Contact.Account.Name;
        }

        IX_analytics_event__c analyticsEvent = new IX_analytics_event__c(
                Name = eventName,
                Session_ID__c = sessionID,
                Event_Description__c = eventDescription,
                Event_Data__c = eventData,
                Brokerage__c = brokerageName
        );
        insert analyticsEvent;
    }

    @AuraEnabled
    public static List<IX_analytics_event__c> getAnalyticsEvents(Integer numberOfDays) {
        Date nDaysAgo = Date.parse('01/01/1970');
        if (numberOfDays != null) {
            nDaysAgo = Date.today().addDays(-numberOfDays);
        }

        List<IX_analytics_event__c> analyticsEvents = new List<IX_analytics_event__c>([
                SELECT Id, Name, User_ID__c, Session_ID__c, Event_Data__c, Event_Description__c, CreatedDate,
                        User_Name__c, CreatedBy.Name, CreatedBy.ContactId, Brokerage__c
                FROM IX_analytics_event__c
                WHERE CreatedDate > :nDaysAgo
                ORDER BY CreatedDate ASC
        ]);

        return analyticsEvents;
    }
}