/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 18/10/2017
 */
({
    loadContributor : function(component, event, helper) {
		var getContributorAction = component.get('c.getContributor');

		getContributorAction.setStorable();
		getContributorAction.setParam('recordId', component.get('v.recordId'));

		getContributorAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var contributor = response.getReturnValue();
                component.set('v.contributor', contributor);
                component.set('v.contributorSearchFilter',  {value: contributor.Id, label: contributor.Full_Name__c});
            }
        });

        $A.enqueueAction(getContributorAction);
    }
})