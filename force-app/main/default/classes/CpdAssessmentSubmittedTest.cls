@istest
public class CpdAssessmentSubmittedTest {
    
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static CPD_Test__c cpdTest;
    private static CPD_Test_Question__c cpdTestQuestion;
    private static CPD_Assessment__c ass;
    private static CPD_Assessment_Answer__c ans;
    
    static {
        cpdTest = tog.getCpdTest();
        cpdTestQuestion = tog.getCpdTestQuestion();
        cpdTestQuestion.Answer__c = '1';
        
        ass = tog.getCpdAssessment();
        ans = [SELECT Id, Correct__c
               FROM CPD_Assessment_Answer__c
               WHERE CPD_Assessment__c = :ass.Id
               AND CPD_Test_Question__c = :cpdTestQuestion.Id];

        System.assertEquals(null, ans.Correct__c);        
    }
    
    @istest
    public static void correctAnswer() {
        ans.Answer__c = '1';
        update ans;
        
        ass.Status__c = 'Submitted';
        update ass;
        
        ans = [SELECT Id, Correct__c FROM CPD_Assessment_Answer__c WHERE Id = :ans.Id];
        
        System.assertEquals('true', ans.Correct__c);
    }

    @istest
    public static void incorrectAnswer() {
        ans.Answer__c = '2';
        update ans;
        
        ass.Status__c = 'Submitted';
        update ass;
        
        ans = [SELECT Id, Correct__c FROM CPD_Assessment_Answer__c WHERE Id = :ans.Id];
        
        System.assertEquals('false', ans.Correct__c);
    }
}