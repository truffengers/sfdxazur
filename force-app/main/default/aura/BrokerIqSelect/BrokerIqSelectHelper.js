({
    valueChanged : function(component) {
        var element = component.getElement();
        var newValue = component.get('v.value');
        if(element && element.value !== newValue) {
            element.value = newValue;
        }
    }
})