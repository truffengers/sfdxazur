/**
* @name: LightningSelfRegisterControllerTest
* @description: This class should be replaced with standard Salesforce test class - written now to increase our code coverages
* @author: Rahma Belghouti rbelghouti@azuruw.com
* @date: 10/07/2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
*/
@IsTest
public with sharing class LightningSelfRegisterControllerTest { 

    @isTest
    static void testIsValidPassword(){
        System.assertEquals(true, LightningSelfRegisterController.isValidPassword('psw','psw'));  
        System.assertEquals(false, LightningSelfRegisterController.isValidPassword('psw1','psw2')); 
    }
    
    @IsTest 
    static void testSiteAsContainerEnabled() { 
        /*This code below is suggested by Salesforce but it is not working*/
        //String networkId = Network.getNetworkId(); 
        //System.assertNotEquals(null, LightningSelfRegisterController.siteAsContainerEnabled(networkId)); 
        System.assertNotEquals(null, LightningSelfRegisterController.siteAsContainerEnabled('testDomai')); 

    } 
  
    @isTest
    static void testValidatePassword(){
        User user = [SELECT Id, username, firstname, lastname, email FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        LightningSelfRegisterController.validatePassword(user, 'sales@testing', 'sales@testing');
    }
    
     
    @isTest
    static void testSiteRegister(){
        TestString testString = new TestString();
        testString.IdName = '123';
        System.assertNotEquals(null, LightningSelfRegisterController.selfRegister('tester', 'tester', 'test@testname.com', 'sales@testing', 'sales@testing', 'test', null, JSON.serialize(new List<TestString>{testString}), null, true));
        System.assertNotEquals(null, LightningSelfRegisterController.selfRegister('tester', null, 'test@testname.com', 'sales@testing', 'sales@testing', 'test', null, null, null, true));
        System.assertNotEquals(null, LightningSelfRegisterController.selfRegister('tester', 'tester', null, 'sales@testing', 'sales@testing', 'test', null, null, null, true));

    }
    
    @isTest
    static void testGetExtraFields(){
        TestString testString = new TestString();
        testString.IdName = '123';
        System.assertEquals(1, LightningSelfRegisterController.getExtraFields(JSON.serialize(new List<TestString>{testString})).size());
    }
    
    public class TestString{
        public String IdName;
    }
}