({
    doInit: function (component) {
    },

    getRecords: function (component) {
        let actionSetup = component.get("c.getSetupData");
        actionSetup.setParam('recordId', component.get('v.recordId'));
        actionSetup.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                const returnValue = response.getReturnValue();

                component.set("v.quote", returnValue.quote);
                component.set("v.jointInsured", returnValue.jointInsured);
                component.set("v.accountId", returnValue.accountId);

            } else if (response.getState() === "ERROR") {
                console.log("Errors", response.getError());
            }
        });
        $A.enqueueAction(actionSetup);
    }
});