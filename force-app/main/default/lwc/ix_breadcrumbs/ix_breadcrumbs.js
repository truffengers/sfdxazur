import { LightningElement, api } from 'lwc';

export default class Ix_Breadcrumbs extends LightningElement {
    @api breadcrumbsArray;

    breadcrumbClick(event) {
        if (event.target.dataset.id
        !== this.breadcrumbsArray[this.breadcrumbsArray.length - 1]._id) {
            const breadcrumbClickEvent = new CustomEvent('breadcrumbclick', {
                detail: {
                    value: event.target.dataset.id
                }
            });
            this.dispatchEvent(breadcrumbClickEvent);
        }
    }

    get margin() {
        return this.breadcrumbsArray.length
            ? 'breadcrumbs-wrapper margin-bottom'
            : 'breadcrumbs-wrapper';
    }
}