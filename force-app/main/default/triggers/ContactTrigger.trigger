trigger ContactTrigger on Contact (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
  (new Nebula_Tools.MetaDataTriggerManager(Contact.sObjectType)).handle();
}