/**
* @name: IBA_TransactionLineItemTriggerTest
* @description: Test class for IBA_TransactionLineItemTriggerHandler
*
*/

@isTest
private class IBA_TransactionLineItemTriggerTest {
    
    private static final String COMPANY_NAME = 'ApexTestCompany';
    private static final String CURRENCY_USD = 'USD';

    @testSetup
    static void createTestRecords() {
        User accountantUser = IBA_TestDataFactory.createUser(IBA_TestDataFactory.IBA_ACCOUNTANT_ID);
        IBA_TestDataFactory.FinancialForceSetup ffs = IBA_TestDataFactory.setupFinancialForce(COMPANY_NAME, CURRENCY_USD);
        Group companyGroup = ffs.companyGroup;
        c2g__codaCompany__c company = ffs.company;
        
        c2g__codaYear__c yr = IBA_TestDataFactory.createYear(true, companyGroup.Id, company.Id);
        c2g.CODAYearWebService.calculatePeriods(yr.Id);

        c2g__codaAccountingCurrency__c curr = IBA_TestDataFactory.createCurrency(true, CURRENCY_USD);

        c2g__codaGeneralLedgerAccount__c gla = IBA_TestDataFactory.createGeneralLedgerAccount(true, 'Brochures', 'uniqueCaseInsensitive24!');

        Product2 product = IBA_TestDataFactory.createProduct(false, 'Product 1', IBA_UtilConst.PRODUCT_RT_Master);
        product.IBA_AIGProduct__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
        product.IBA_AIGMinorLine__c = 'PCG Test Liability';
        product.IBA_AIGPolicyCodeNumber__c = '000000test';
        product.IsActive = true;
        product.c2g__CODASalesRevenueAccount__c = gla.Id;
        insert product;

        Account acc = IBA_TestDataFactory.createAccount(false, 'testAccOnHold', IBA_UtilConst.ACCOUNT_RT_BROKER);
        acc.c2g__CODAAccountTradingCurrency__c = CURRENCY_USD;
        acc.c2g__CODAAccountsPayableControl__c = gla.Id;
        acc.c2g__CODAAccountsReceivableControl__c = gla.Id;
        acc.c2g__CODAVATStatus__c = 'Home';
        acc.c2g__CODATaxCalculationMethod__c = 'Gross';
        acc.c2g__CODABaseDate1__c = 'Invoice Date';
        acc.c2g__CODADaysOffset1__c = 0;
        acc.c2g__CODADescription1__c = 'Test';
        acc.c2g__CODADiscount1__c = 0;
        insert acc;

        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c WHERE 
            c2g__YearName__c = :yr.Id AND c2g__PeriodNumber__c != '000' AND 
            c2g__EndDate__c > TODAY LIMIT 1];

        c2g__codaBankAccount__c bank = IBA_TestDataFactory.createBankAccount(false, 'Trust Account USD', '123456789', 'Santander', 'uniqueCaseInsensitive24!', gla.Id, curr.Id);
        insert bank;

        c2g__codaInvoice__c invoice = IBA_TestDataFactory.createInvoice(true, company.Id, acc.Id, period.Id, curr.Id);
        c2g__codaCashEntry__c cashEntry = IBA_TestDataFactory.createCashEntry(true, bank.Id, period.Id, curr.Id);

        IBA_TestDataFactory.createInvoiceLineItem(true, 15, 408.01, invoice.Id, product.Id);
        IBA_TestDataFactory.createCashEntryLineItem(true, acc.Id, 408.01, cashEntry.Id);

        List<c2g.CODAAPICommon.Reference> invoiceRefs = new List<c2g.CODAAPICommon.Reference>();
        invoiceRefs.add(c2g.CODAAPICommon.getRef(invoice.Id, null));
        c2g.CODAAPISalesInvoice_10_0.BulkPostInvoice(null, invoiceRefs);
    }

    @isTest
    static void bulkReleaseForPaymentAfterUpdateTest() {
        List<c2g__codaTransactionLineItem__c> transactionLinesToMatch = [SELECT Id, c2g__AccountValue__c, c2g__Transaction__c, 
            IBA_InvoicePaidFlowFired__c, c2g__MatchingStatus__c, c2g__LineType__c, c2g__GeneralLedgerAccount__c
            FROM c2g__codaTransactionLineItem__c 
            WHERE c2g__LineType__c = :IBA_UtilConst.TRANSACTION_LINE_ITEM_TYPE_ACCOUNT];

        Test.startTest();
        c2g__codaCashEntry__c cashEntry = [SELECT Id FROM c2g__codaCashEntry__c LIMIT 1];
        List<c2g.CODAAPICommon.Reference> cashEntryRefs = new List<c2g.CODAAPICommon.Reference>();
        cashEntryRefs.add(c2g.CODAAPICommon.getRef(cashEntry.Id, null));
        c2g.CODAAPICashEntry_7_0.BulkPostCashEntry(null, cashEntryRefs);
        
        c2g__codaInvoice__c invoice = [SELECT Id, c2g__Account__c, c2g__OwnerCompany__c, c2g__Period__c, c2g__InvoiceCurrency__c FROM c2g__codaInvoice__c LIMIT 1];

        c2g__codaPurchaseInvoice__c purchaseInvoice = IBA_TestDataFactory.createPurchaseInvoice(true, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c, 
        invoice.Id);
        Id purchaseInvoiceId = purchaseInvoice.Id;

        IBA_TestDataFactory.createPurchaseInvoiceExpenseLineItem(true, 
        1,
        invoice.c2g__OwnerCompany__c, 
        transactionLinesToMatch[0].c2g__GeneralLedgerAccount__c, 
        purchaseInvoice.Id);

        List<c2g.CODAAPICommon.Reference> purchaseRefs = new List<c2g.CODAAPICommon.Reference>();
        purchaseRefs.add(c2g.CODAAPICommon.getRef(purchaseInvoice.Id, null));
        c2g.CODAAPIPurchaseInvoice_9_0.BulkPostPurchaseInvoice(null, purchaseRefs); 

        List<c2g__codaPurchaseInvoice__c> insertedPurchaseInvoice = [SELECT Id, Name, c2g__HoldStatus__c FROM c2g__codaPurchaseInvoice__c];

        System.enqueueJob(new IBA_BulkReleaseForPaymentJob(insertedPurchaseInvoice));
        Test.stopTest();

        c2g__codaPurchaseInvoice__c updatedInvoice = [SELECT Id, Name, c2g__HoldStatus__c FROM c2g__codaPurchaseInvoice__c WHERE Id = :purchaseInvoiceId];

        System.assertEquals(IBA_UtilConst.PAYABLE_INVOICE_HOLD_STATUS, insertedPurchaseInvoice[0].c2g__HoldStatus__c);
        System.assertEquals(null, updatedInvoice.c2g__HoldStatus__c);
        
    }
}