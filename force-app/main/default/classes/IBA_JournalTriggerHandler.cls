/**
* @name: IBA_JournalTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaJournal__c trigger functionality
*
*/

public with sharing class IBA_JournalTriggerHandler implements ITriggerHandler {
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_JournalTriggerHandler.IsDisabled != null ? IBA_JournalTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, c2g__codaJournal__c> policyTransactionMap = new Map<Id, c2g__codaJournal__c>();
        Map<Id, List<c2g__codaJournalLineItem__c>> policyTransactionJournalLineItemsMap = new Map<Id, List<c2g__codaJournalLineItem__c>>();
        List<c2g__codaJournalLineItem__c> journalLineItemList = [SELECT Id, c2g__Value__c, c2g__Account__c, c2g__Journal__c FROM c2g__codaJournalLineItem__c WHERE c2g__Journal__c IN :newItems.keySet()];
        for(Id recordId :newItems.keySet()) {
            c2g__codaJournal__c journal = (c2g__codaJournal__c)newItems.get(recordId);
            c2g__codaJournal__c journalOld = (c2g__codaJournal__c)oldItems.get(recordId);
            if(journal.IBA_PolicyTransaction__c != null && journal.c2g__JournalStatus__c == IBA_UtilConst.JOURNAL_STATUS_COMPLETE 
                && journalOld.c2g__JournalStatus__c != IBA_UtilConst.JOURNAL_STATUS_COMPLETE) {
                    policyTransactionMap.put(journal.IBA_PolicyTransaction__c, journal);
                    for(c2g__codaJournalLineItem__c journalLineItem :journalLineItemList) {
                        if(journalLineItem.c2g__Journal__c == journal.Id) {
                            List<c2g__codaJournalLineItem__c> journalLineItems = policyTransactionJournalLineItemsMap.get(journal.IBA_PolicyTransaction__c);
                            if(journalLineItems == null) {
                                journalLineItems = new List<c2g__codaJournalLineItem__c>();
                                policyTransactionJournalLineItemsMap.put(journal.IBA_PolicyTransaction__c, journalLineItems);
                            }
                            journalLineItems.add(journalLineItem);
                        }
                    }
            }
        }

        if(policyTransactionJournalLineItemsMap.size() > 0) {
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(policyTransactionMap.keySet());
            transferData.subtractJournalLineItem(policyTransactionJournalLineItemsMap)
                .addJournalLineItem(policyTransactionJournalLineItemsMap)
                .transfer();
        }
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}