import { LightningElement, api, track } from 'lwc';
import Confetti from '@salesforce/resourceUrl/confetti';
import { loadScript } from 'lightning/platformResourceLoader';
import { analyticsEvent } from 'c/ix_utils';

export default class Ix_AnswerFeedback extends LightningElement {
    @api searchQuery;
    @api clearSearchQuery;
    @api resetParentAll;
    @api itemData;

    @track showTypeformArrow;

    connectedCallback() {
        loadScript(this, `${Confetti}/confetti.js`)
            .catch(error => console.log(error)); // eslint-disable-line
    }

    handleAnswerFound() {
        this.dispatchFeedbackClickedEvent();
        let eventDescription;
        if (this.itemData && this.searchQuery) {
            eventDescription = `Thumbs up clicked for "${this.itemData._name}"
                after searching "${this.searchQuery}"`
        } else if (this.itemData) {
            eventDescription = `The user cleared the search box, then clicked
                thumbs up for "${this.itemData._name}"`
        } else if (this.searchQuery) {
            eventDescription = `The user searched "${this.searchQuery}" but did
                not click on an option in the dropdown before clicking thumbs
                up.`
        } else {
            eventDescription = `The user clicked thumbs up without searching
                anything.`
        }

        analyticsEvent({
            eventName: 'Thumbs Up Clicked',
            eventDescription
        })
            .catch(() => null);

        confetti({ // eslint-disable-line
            // Create confetti. Note to self: You can change the color of
            // confetti
            spread: 115,
            particleCount: 75,
            startVelocity: 70,
            origin: { x: 0.5, y: 1 },
            // colors: ['#B8E4FA', '#3EB5D5', '#FFFFFF', '#222930']
        });
    }

    handleAnswerNotFound() {
        this.dispatchFeedbackClickedEvent();
        let eventDescription;
        if (this.itemData && this.searchQuery) {
            eventDescription = `Thumbs down clicked for "${this.itemData._name}"
                after searching "${this.searchQuery}"`
        } else if (this.itemData) {
            eventDescription = `The user cleared the search box, then clicked
                thumbs down for "${this.itemData._name}"`
        } else if (this.searchQuery) {
            eventDescription = `The user searched "${this.searchQuery}" but did
                not click on an option in the dropdown before clicking thumbs
                down.`
        } else {
            eventDescription = `The user clicked thumbs down without searching
                anything.`
        }

        analyticsEvent({
            eventName: 'Thumbs Down Clicked',
            eventDescription
        })
            .catch(() => null);

        this.showTypeformArrow = true;

        setTimeout(() => {
            this.showTypeformArrow = false;
        }, 5000);
    }

    handleCloseTypeformArrow() {
        this.showTypeformArrow = false;
    }

    handleSearchAgain() {
        this.clearSearchQuery();
        this.resetAll();
        this.resetParentAll();
    }

    // This method lets the searchBox component know about whether or not there
    // was any interaction with this component
    dispatchFeedbackClickedEvent() {
        const feedbackClickedEvent = new CustomEvent('feedbackclick');
        this.dispatchEvent(feedbackClickedEvent);
    }

    resetAll() {
        this.feedbackSubmitted = false;
        this.showFeedbackBox = false;
    }
}