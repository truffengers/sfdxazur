({
	doInit : function(component, event, helper) {

        var getBrokerIqCategoryMetadataAction = component.get('c.getRecordsAllFields');

		getBrokerIqCategoryMetadataAction.setParams({
			typeName: "Broker_iQ_Category__mdt",
			whereClause: "Id != null"
		});
		
		getBrokerIqCategoryMetadataAction.setStorable();

		getBrokerIqCategoryMetadataAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	var metadata = response.getReturnValue();
            	var metadataByTopicName = {};

            	for (var i = metadata.length - 1; i >= 0; i--) {
            		metadataByTopicName[metadata[i].MasterLabel.toLowerCase()] = metadata[i];
            	}

                component.set('v.metadataByTopicName', metadataByTopicName);
	        }
        });
        
        $A.enqueueAction(getBrokerIqCategoryMetadataAction);
        
	},
	topicNameChanged : function(component, event, helper) {
		var metadataByTopicName = component.get('v.metadataByTopicName');
		if(metadataByTopicName) {
			var topicName = component.get('v.topicName');
			if(!$A.util.isEmpty(topicName)) {
				var categoryMetadata = metadataByTopicName[topicName.toLowerCase()];
				if(categoryMetadata) {
					component.set('v.categoryMetadata', categoryMetadata);
				}
			}
		}
	}
})