public class EWAIGReportController {

    private static final Id personAccount = Account.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(EWConstants.ACCOUNT_PERSON_RECORD_TYPE_DEVNAME).getRecordTypeId();
    private static final Id ewPolicy = Asset.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(EWConstants.POLICY_EW_RECORD_TYPE_DEVNAME).getRecordTypeId();

    private static Set<String> headers = new Set<String>{
            //'Transaction Type',
            'AIG policy number',
            'Policy Status',
            'Policyholder title',
            'Policyholder Firstname',
            'Policyholder Middlename',
            'Policyholder Lastname',
            'Policyholder DOB',
            'Policyholder occupation',
            'Policyholder nationality',
            'Address Line 1',
            'Address Line 2',
            'Address Line 3',
            'Address Line 4',
            'City',
            'County',
            'Postcode',
            'Financial Broker Account',
            'Financial account DR code',
            'Broker commission percent',
            'Effective date (start date of policy or MTA)',
            'Expiry date (or cancellation date)',
            'Currency',
            'IPT percent',
            'Coverage type',
            'Outra building sum insured',
            'Buildings premium ex IPT',
            'Policy excess',
            'Contents premium ex IPT',
            'Art & Collectables sum insured',
            'Art and Collectables premium ex IPT',
            'Jewellery and watches SI',
            'Jewellery and watches premium ex IPT',
            'Public liability sum insured',
            'Public libaility premium',
            'Employers liability sum insured',
            'Pedal cycles sum insured',
            'Pedal cycle sum insured ex IPT',
            'Fixed expense premium'
    };

    @AuraEnabled
    public static String getReportData() {
        Map<String, Object> result = new Map<String, Object>();
        List<RowDataWrapper> items = initializeData();

        List<RowCellWrapper> headerRow = new List<RowCellWrapper>();
        List<List<RowCellWrapper>> rows = new List<List<RowCellWrapper>>();

        for (String header : headers) {
            headerRow.add(new RowCellWrapper(header));
        }

        Boolean addStyle = false;
        for (RowDataWrapper row : items) {
            if (row.policy.vlocity_ins__InsuredItems__r.isEmpty()) {
                continue;
            }
            List<RowCellWrapper> rowLine = row.getRowValues(addStyle);
            rows.add(rowLine);
            addStyle = !addStyle;
        }

        result.put('headers', headerRow);
        result.put('rows', rows);

        String headerRowStr = JSON.serialize(headerRow);
        String jsonStr = JSON.serialize(result);

        return jsonStr;
    }


    private static List<RowDataWrapper> initializeData() {
        List<RowDataWrapper> result = new List<RowDataWrapper>();

        Map<Id, Account> accounts = new Map<Id, Account>([
                SELECT Id, Salutation, Name, FirstName, LastName, Building_name__c, EW_BuildingName__pc, BillingStreet,
                        EW_BuildingNumber__pc, BillingCity, BillingCountry, BillingPostalCode, EW_BrokerAccount__c,
                        EW_BrokerAccount__r.Name, EW_BrokerAccount__r.EW_DRCode__c, vlocity_ins__MiddleName__pc,
                        PersonBirthdate, EW_Occupation__pr.Name, EW_Nationality__pr.Name, EW_FlatName__pc, PersonMailingStreet,
                        PersonMailingCity, PersonMailingCountry, PersonMailingPostalCode
                FROM Account
                WHERE RecordTypeId = :personAccount
        ]);

        // TODO - Add after MTA -> EW_TypeofTransaction__c
        Map<Id, Asset> policies = new Map<Id, Asset>([
                SELECT Id, Name, vlocity_ins__EffectiveDate__c, vlocity_ins__ExpirationDate__c, Brokerage_DR_Code__c,
                        EW_Payment_Method__c, IBA_BrokerCommissionRate__c, AccountId, EW_CoverageType__c, EW_Excess__c,
                        IBA_ProducingBrokerAccount__r.EW_DRCode__c, CurrencySymbol__c, IBA_InsurancePremiumTaxRate__c,
                        EW_FixedExpense__c, IBA_ProducingBrokerAccount__r.Name, Status, (
                        SELECT Id, IBA_TotalBrokerCommission__c, IBA_TotalMGACommission__c, IBA_TotalInsurancePremiumTax__c
                        FROM FF_Policy_Transactions__r
                ), (
                        SELECT EW_RebuildCost__c
                        FROM vlocity_ins__InsuredItems__r
                        WHERE Name = 'Main residence'
                        LIMIT 1
                ), (
                        SELECT Id, Name, EW_PremiumGrossExIPT__c, EW_SpecifiedItemValue__c
                        FROM vlocity_ins__PolicyCoverages__r
                )
                FROM Asset
                WHERE AccountId IN :accounts.keySet() AND RecordTypeId = :ewPolicy
        ]);

        for (Asset policy : policies.values()) {
            result.add(new RowDataWrapper(policy, accounts.get(policy.AccountId)));
        }

        return result;
    }

    class RowDataWrapper {
        public Asset policy { get; set; }
        public Account personAccount { get; set; }

        private vlocity_ins__AssetCoverage__c coverageBuildings;
        private vlocity_ins__AssetCoverage__c coverageContents;
        private vlocity_ins__AssetCoverage__c coverageArtAndCollectables;
        private vlocity_ins__AssetCoverage__c coverageJewellery;
        private vlocity_ins__AssetCoverage__c coveragePublicLiability;
        private vlocity_ins__AssetCoverage__c coverageEmployersLiability;
        private vlocity_ins__AssetCoverage__c coveragePedalCycles;

        public RowDataWrapper(Asset policy, Account personAccount) {
            this.policy = policy;
            this.personAccount = personAccount;

            for (vlocity_ins__AssetCoverage__c coverage : policy.vlocity_ins__PolicyCoverages__r) {
                switch on coverage.Name {
                    when 'Buildings' {
                        this.coverageBuildings = coverage;
                    }
                    when 'Contents' {
                        this.coverageContents = coverage;
                    }
                    when 'ArtandCollectables' {
                        this.coverageArtAndCollectables = coverage;
                    }
                    when 'JewelleryandWatches' {
                        this.coverageJewellery = coverage;
                    }
                    when 'PublicLiability' {
                        this.coveragePublicLiability = coverage;
                    }
                    when 'EmployersLiability' {
                        this.coverageEmployersLiability = coverage;
                    }
                    when 'PedalCycles' {
                        this.coveragePedalCycles = coverage;
                    }
                }
            }
        }

        public List<RowCellWrapper> getRowValues(Boolean addStyle) {
            List<RowCellWrapper> result = new List<RowCellWrapper>();

            // Transaction Type
            //result.add(new RowCellWrapper(this.policy.EW_TypeofTransaction__c));
            // AIG policy number
            result.add(new RowCellWrapper(this.policy.Name));
            // Policy Status
            result.add(new RowCellWrapper(this.policy.Status));
            // Policyholder title
            result.add(new RowCellWrapper(this.personAccount.Salutation));
            // Policyholder Firstname
            result.add(new RowCellWrapper(this.personAccount.FirstName));
            // Policyholder Middlename
            result.add(new RowCellWrapper(this.personAccount.vlocity_ins__MiddleName__pc));
            // Policyholder Lastname
            result.add(new RowCellWrapper(this.personAccount.LastName));
            // Policyholder DOB
            result.add(new RowCellWrapper(String.valueOf(this.personAccount.PersonBirthdate)));
            // Policyholder occupation
            result.add(new RowCellWrapper(this.personAccount.EW_Occupation__pr.Name));
            // Policyholder nationality
            result.add(new RowCellWrapper(this.personAccount.EW_Nationality__pr.Name));
            // Address Line 1
            result.add(new RowCellWrapper(this.personAccount.EW_BuildingNumber__pc));
            // Address Line 2
            result.add(new RowCellWrapper(this.personAccount.EW_BuildingName__pc));
            // Address Line 3
            result.add(new RowCellWrapper(this.personAccount.EW_FlatName__pc));
            // Address Line 4
            result.add(new RowCellWrapper(this.personAccount.PersonMailingStreet));
            // City
            result.add(new RowCellWrapper(this.personAccount.PersonMailingCity));
            // County
            result.add(new RowCellWrapper(this.personAccount.PersonMailingCountry));
            // Postcode
            result.add(new RowCellWrapper(this.personAccount.PersonMailingPostalCode));
            // Financial Broker Account
            result.add(new RowCellWrapper(String.valueOf(this.policy.IBA_ProducingBrokerAccount__r.Name)));
            // Financial account DR code
            result.add(new RowCellWrapper(this.policy.IBA_ProducingBrokerAccount__r.EW_DRCode__c));
            // Broker commission percent
            result.add(new RowCellWrapper(String.valueOf(this.policy.IBA_BrokerCommissionRate__c)));
            // Effective date (start date of policy or MTA)
            result.add(new RowCellWrapper(String.valueOf(this.policy.vlocity_ins__EffectiveDate__c)));
            // Expiry date (or cancellation date)
            result.add(new RowCellWrapper(String.valueOf(this.policy.vlocity_ins__ExpirationDate__c)));
            // Currency
            result.add(new RowCellWrapper(this.policy.CurrencySymbol__c));
            // IPT percent
            result.add(new RowCellWrapper(String.valueOf(this.policy.IBA_InsurancePremiumTaxRate__c)));
            // Coverage type
            result.add(new RowCellWrapper(this.policy.EW_CoverageType__c));
            // Outra building sum insured
            result.add(new RowCellWrapper(String.valueOf(this.policy.vlocity_ins__InsuredItems__r.get(0).EW_RebuildCost__c)));
            // Buildings premium ex IPT
            result.add(new RowCellWrapper(String.valueOf(this.coverageBuildings.EW_PremiumGrossExIPT__c)));
            // Policy excess
            result.add(new RowCellWrapper(String.valueOf(this.policy.EW_Excess__c)));
            // Contents premium ex IPT
            result.add(new RowCellWrapper(String.valueOf(this.coverageContents.EW_PremiumGrossExIPT__c)));
            // Art & Collectables sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coverageArtAndCollectables.EW_SpecifiedItemValue__c)));
            // Art and Collectables premium ex IPT
            result.add(new RowCellWrapper(String.valueOf(this.coverageArtAndCollectables.EW_PremiumGrossExIPT__c)));
            // Jewellery and watches SI
            result.add(new RowCellWrapper(String.valueOf(this.coverageJewellery.EW_SpecifiedItemValue__c)));
            // Jewellery and watches premium ex IPT
            result.add(new RowCellWrapper(String.valueOf(this.coverageJewellery.EW_PremiumGrossExIPT__c)));
            // Public liability sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coveragePublicLiability.EW_SpecifiedItemValue__c)));
            // Public libaility premium
            result.add(new RowCellWrapper(String.valueOf(this.coveragePublicLiability.EW_PremiumGrossExIPT__c)));
            // Employers liability sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coverageEmployersLiability.EW_SpecifiedItemValue__c)));
            // Pedal cycles sum insured
            result.add(new RowCellWrapper(String.valueOf(this.coveragePedalCycles.EW_SpecifiedItemValue__c)));
            // Pedal cycle sum insured ex IPT
            result.add(new RowCellWrapper(String.valueOf(this.coveragePedalCycles.EW_PremiumGrossExIPT__c)));
            // Fixed expense premium
            result.add(new RowCellWrapper(String.valueOf(this.policy.EW_FixedExpense__c)));

            for (RowCellWrapper cell : result) {
                cell.cssClass = 'responsiveHide';
            }

            return result;
        }
    }

    class RowCellWrapper {
        public String val { get; set; }
        public String cssClass { get; set; }

        public RowCellWrapper(String val) {
            this.val = (val != null) ? val : '';
            this.cssClass = '';
        }

        public RowCellWrapper(String val, String cssClass) {
            this.val = (val != null) ? val : '';
            this.cssClass = (cssClass != null) ? cssClass : '';
        }
    }

}