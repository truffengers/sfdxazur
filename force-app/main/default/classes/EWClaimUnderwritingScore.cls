/**
* @name: EWClaimUnderwritingScore
* @description: Class for EWClaimUnderwritingScore
* @author: Demiro Massessi dmassessi@azuruw.com
* @date: 09/10/2018
*/

global class EWClaimUnderwritingScore implements vlocity_ins.VlocityOpenInterface {
    public Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        System.debug('Called EWClaimUnderwritingScore class');
                
        if (methodName.equals('getScore')) {
            getScore(input, output, options);
        }
        return true;
    }
    
    public static void getScore(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        List<sObject> claims = [SELECT Name, Claim_Less_Than_5000__c, Claim_Greater_Than_5000__c, Claim_Greater_Than_30000__c FROM Claim_Underwriting_Score__c];
        
        try{
            System.debug('Map!!!!!!');
            Map<String, Object> lookupMap1 = (Map<String, Object>)input.get('propertyDetails');
            Map<String, Object> lookupMap2 = (Map<String, Object>)lookupMap1.get('editBlockClaimsHistory');
            System.debug(lookupMap2);
            
            Integer i = 0;
            if(lookupMap2 != null) {
                i = calculateScore(lookupMap2, claims);
            }
            
            // System.debug('The total score is ' + i);
            output.put('claimsUnderwritingTotalScore', i);
        } catch(Exception e) {
            // System.debug('The following exception has occurred: ' + e.getMessage());
            System.debug('List!!!!!!');
            Map<String, Object> lookupMap1 = (Map<String, Object>)input.get('propertyDetails');
            List<Object> lookupMap2 = (List<Object>)lookupMap1.get('editBlockClaimsHistory');
            System.debug(lookupMap2);
            
            Integer i = 0;
            if(lookupMap2 != null){
                for(Object claim : lookupMap2){
                	i += calculateScore((Map<String, Object>)claim, claims);
            	}
            }
            
            // System.debug('The total score is ' + i);
            output.put('claimsUnderwritingTotalScore', i);
        }
	}

    
    public static Integer calculateScore(Map<String, Object> lookupMap2, List<sObject> claims) {
        String claimType;
        Decimal claimValue;
        String claimStillOpen;
        String claimAtCurrentAddress;
        Integer claimScore;

        Set<String> keys = lookupMap2.keySet();
        	for(String key : keys){

                if(key == 'selectClaimType'){
                	claimType = (String)lookupMap2.get(key);
            	}

                else if(key == 'textClaimValue'){
               	    claimValue = (Decimal)lookupMap2.get(key);
            	}

                else if(key == 'radioClaimStillOpen'){
                	claimStillOpen = (String)lookupMap2.get(key);
            	}

                else if(key == 'radioClaimAtCurrentProperty'){
                	claimAtCurrentAddress = (String)lookupMap2.get(key);
            	}
        	}
            
            if(claimAtCurrentAddress == 'No') {
            	claimType = claimType + ' - Previous address';
        	}
            
            for(sObject claim : claims){
            	if(claim.get('Name') == claimType){
                	if(claimValue < 5000) {
                    	//System.debug('less than 5000');
                    	claimScore = integer.valueOf((Decimal)claim.get('Claim_Less_Than_5000__c'));
                	} else if(claimValue >= 5000 && claimValue < 30000) {
                    	//System.debug('grater than 5000');
                    	claimScore = integer.valueOf((Decimal)claim.get('Claim_Greater_Than_5000__c'));
                	} else if(claimValue >= 30000) {
                    	//System.debug('grater than 30000');
                    	claimScore = integer.valueOf((Decimal)claim.get('Claim_Greater_Than_30000__c'));
                    } else {
                        claimScore = 0;
                    }
            	}
        	}
        return claimScore == null ? 0 : claimScore;
    }
}