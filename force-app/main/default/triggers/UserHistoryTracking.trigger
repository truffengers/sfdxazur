trigger UserHistoryTracking on User (after update) {

    UserHistoryService uhs = new UserHistoryService(Trigger.old, Trigger.new);
    
    List<User_History__c> histories = uhs.getChangeHistories();
    
    if(!histories.isEmpty()) {
        UserHistoryService.insertFuture(JSON.serialize(histories));
    }
}