/**
* @name: IBA_PolicyTransactionTriggerHandlerTest
* @description: Test class for IBA_PolicyTransactionTriggerHandler
*
*/
@IsTest
private class IBA_PolicyTransactionTriggerHandlerTest {

    private static final String COMPANY_NAME = 'ApexTestCompany';
    private static final String CURRENCY_USD = 'USD';

    private static final Integer NUMBER_OF_POLICY_TRANSACTIONS = 5;
    private static final Integer AMOUNT = 100;

    @testSetup
    static void createTestRecords() {
        User accountantUser = IBA_TestDataFactory.createUser(IBA_TestDataFactory.IBA_ACCOUNTANT_ID);
        IBA_TestDataFactory.FinancialForceSetup ffs = IBA_TestDataFactory.setupFinancialForce(COMPANY_NAME, CURRENCY_USD);
        Group companyGroup = ffs.companyGroup;
        c2g__codaCompany__c company = ffs.company;
    
        c2g__codaYear__c yr = IBA_TestDataFactory.createYear(true, companyGroup.Id, company.Id);
        c2g.CODAYearWebService.calculatePeriods(yr.Id);

        c2g__codaAccountingCurrency__c curr = IBA_TestDataFactory.createCurrency(true, CURRENCY_USD);

        c2g__codaGeneralLedgerAccount__c gla = IBA_TestDataFactory.createGeneralLedgerAccount(true, 'Brochures', 'uniqueCaseInsensitive24!');

        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c WHERE 
            c2g__YearName__c = :yr.Id AND c2g__PeriodNumber__c != '000' AND 
            c2g__EndDate__c > TODAY LIMIT 1];

        Account acc = IBA_TestDataFactory.createAccount(true, 'testAccOnHold', IBA_UtilConst.ACCOUNT_RT_BROKER);

        c2g__codaInvoice__c invoice = IBA_TestDataFactory.createInvoice(true, company.Id, acc.Id, period.Id, curr.Id);

        Asset policy = IBA_TestDataFactory.createPolicy(false, 'Policy1234', acc.Id);
        policy.Policy_Status__c = IBA_UtilConst.POLICY_STATUS_ACTIVE;
        insert policy;
        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(false, NUMBER_OF_POLICY_TRANSACTIONS, policy.Id);
        
        for(IBA_PolicyTransaction__c pt :policyTransactions) {
            pt.IBA_BrokerSalesInvoice__c = invoice.Id;
            pt.IBA_AccountingBrokerSIN__c = invoice.Id;
            pt.IBA_BrokerOutstanding__c = AMOUNT;
            pt.IBA_AccountingBrokerOutstanding__c = AMOUNT;
        }
        insert policyTransactions;
    }

    @isTest
    static void executeTest() {
        User accountantUser = [SELECT Id FROM User WHERE LastName = 'XYZ'];
        List<IBA_PolicyTransaction__c> policyTransactions = [SELECT Id, IBA_AccountingBrokerSalesInvoiceDueDate__c, IBA_BrokerSalesInvoiceDueDate__c FROM IBA_PolicyTransaction__c];
        System.runAs(accountantUser) {
            Test.startTest();
            for(IBA_PolicyTransaction__c pt :policyTransactions) {
                pt.IBA_AccountingBrokerSalesInvoiceDueDate__c = System.today().addDays(-1);
                pt.IBA_BrokerSalesInvoiceDueDate__c = System.today().addDays(-1);
            }
            update policyTransactions;
            Test.stopTest();
        }

        Asset policy = [SELECT Id, IBA_BrokerOvedue__c, IBA_AccountingBrokerOverdue__c FROM Asset LIMIT 1];
        System.assertEquals(NUMBER_OF_POLICY_TRANSACTIONS * AMOUNT, policy.IBA_BrokerOvedue__c);
        System.assertEquals(NUMBER_OF_POLICY_TRANSACTIONS * AMOUNT, policy.IBA_AccountingBrokerOverdue__c);
    }

}