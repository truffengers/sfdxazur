global class ContactBrokerIqContributor implements Nebula_Tools.AfterInsertI, Nebula_Tools.AfterUpdateI {
	
	global void handleAfterInsert(List<Contact> newList) {
		handleAfterUpdate(null, newList);
	}

	global void handleAfterUpdate(List<Contact> oldList, List<Contact> newList) {
		List<Broker_iQ_Contributor__c> toInsert = new List<Broker_iQ_Contributor__c>();

		for(Integer i=0; i < newList.size(); i++) {
			Contact newContact = newList[i];

			if(newContact.Is_Broker_iQ_Contributor__c 
			   && newContact.Broker_iQ_Contributor__c == null
			   && (oldList == null || !oldList[i].Is_Broker_iQ_Contributor__c )) {
				toInsert.add(new Broker_iQ_Contributor__c(Contact__c = newContact.Id,
				                                          Name = newContact.FirstName + ' ' + newContact.LastName));
			}
		}

		if(!toInsert.isEmpty()) {
			insert toInsert;
		}
	}
}