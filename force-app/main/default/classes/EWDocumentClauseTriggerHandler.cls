/**
 * Created by roystonlloyd on 2020-01-30.
 */

public with sharing class EWDocumentClauseTriggerHandler implements ITriggerHandler {

public static Boolean IsDisabled;

public Boolean IsDisabled()
{
return EWDocumentClauseTriggerHandler.IsDisabled != null ? EWDocumentClauseTriggerHandler.IsDisabled : false;
}

public void BeforeInsert(List<SObject> newItems){
setName( (List<vlocity_ins__DocumentClause__c>) newItems);
}

public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){
setName( (List<vlocity_ins__DocumentClause__c>) newItems.values());
}

public void BeforeDelete(Map<Id, SObject> oldItems) {}

public void AfterInsert(Map<Id, SObject> newItems) {}

public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

public void AfterDelete(Map<Id, SObject> oldItems) {}

public void AfterUndelete(Map<Id, SObject> oldItems) {}


private void setName(List<vlocity_ins__DocumentClause__c> triggerNew){
    // sets the version within the Name to accommodate unique name required by Vlocity
    Integer maxNameLength = 80;

    for(vlocity_ins__DocumentClause__c dc : triggerNew){
        Integer versionIndex = dc.Name.indexOf('[v');
        dc.EW_Version_Number__c = dc.EW_Version_Number__c == null ? 1 : dc.EW_Version_Number__c;
        String versionString = '[v' + dc.EW_Version_Number__c + ']';
        String nameWithoutVersion = versionIndex <0 ? dc.Name : dc.Name.left(versionIndex);
        Integer fullNameLength = (nameWithoutVersion + versionString).length();
        dc.Name = fullNameLength <= maxNameLength
            ? nameWithoutVersion + versionString
            : nameWithoutVersion.left(maxNameLength - (fullNameLength - maxNameLength)) + versionString;
    }

    }


}