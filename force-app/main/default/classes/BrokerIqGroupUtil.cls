/**
* @name: BrokerIqGroupUtil
* @description: Group utility class
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 30/11/2017
* @Modified: Ignacio Sarmiento	04/12/2017	Extend functionality isLoggedUserCIIUserAndNotGuestBrokerIq2User() to search in groups inside current CII group
* @Modified: Ignacio Sarmiento	07/12/2017	Add 'isLoggedUserCIIAndNotGuestBrokerIq2User' and 'isLoggedUserBrokerIq2GuestUser' method
*/
public without sharing class BrokerIqGroupUtil 
{
    public static final String CII_PUBLIC_GROUP_DEVNAME = 'BiQ_CII_Users';
    public static final String BROKERIQ_21_GUEST_USER_PROFILE_NAME = 'Broker Iq 2.1 Profile';
    private static final Integer DEPTH_LIMIT_IN_CII_SEARCH = 3;

    public static boolean isLoggedUserCIIUserAndNotGuestBrokerIq2User(){
        return !isLoggedUserBrokerIq2GuestUser() && isLoggedUserCIIUser();
    }

    private static boolean isLoggedUserBrokerIq2GuestUser(){
        return [SELECT count() FROM User WHERE Id =: UserInfo.getUserId() AND Profile.Name =: BROKERIQ_21_GUEST_USER_PROFILE_NAME] > 0;
    }
    
    private static boolean isLoggedUserCIIUser(){
        //Initialize valiables
		Boolean result = false;
        Integer currentGroupDepth = 0;
        set<Id> userAndGroupMemberIdsFromGroups = new set<Id>();
        set<Id> usersFromUserRoles = new set<Id>();	

            //Get first search
            set<Id> userAndGroupIdsToSearch = new set<Id>();
            userAndGroupIdsToSearch.add(getCIIGroupId()); 
            
            //Check if logged user is CII user
            while(!result && !userAndGroupIdsToSearch.isEmpty() && currentGroupDepth < DEPTH_LIMIT_IN_CII_SEARCH){
                
                userAndGroupMemberIdsFromGroups = getUserAndGroupMemberIdsFromGroups(userAndGroupIdsToSearch);
                usersFromUserRoles = getUsersFromUserRolesGroupIds(userAndGroupIdsToSearch);
                boolean isLoggedUserInCurrentGroup = userAndGroupMemberIdsFromGroups.contains(UserInfo.getUserId()) ||
                                                     (!usersFromUserRoles.isEmpty() && usersFromUserRoles.contains(UserInfo.getUserId()));
                if(isLoggedUserInCurrentGroup){
                    result = true;
                }else{
                    userAndGroupIdsToSearch = new set<Id>(userAndGroupMemberIdsFromGroups);
                }
                currentGroupDepth++;
            }
        return result;
    }
    
    private static Id getCIIGroupId(){
        return [SELECT Id FROM Group WHERE DeveloperName =: CII_PUBLIC_GROUP_DEVNAME].Id;
    }
    
    private static set<Id> getUserAndGroupMemberIdsFromGroups(set<Id> groupIdsToSearch){
        set<Id> result = new set<Id>();
        list <GroupMember> groupMembers = [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId =: groupIdsToSearch];
        for(GroupMember gm: groupMembers){
            result.add(gm.UserOrGroupId);
    	}
        return result;
    }
    
    private static set<Id> getUsersFromUserRolesGroupIds(set<Id> userRoleGroupIds){
        set<Id> result = new set<Id>();
        set<Id> userRoleIds = new set<Id>();

        list<Group> userRoleGroups = [SELECT Id, RelatedId FROM Group WHERE Id IN: userRoleGroupIds];
        for(Group userRoleGroup: userRoleGroups){
            userRoleIds.add(userRoleGroup.RelatedId);
        }
        
        if(!userRoleIds.isEmpty()){
            map <Id, User> userMap = new map <Id, User>([SELECT Id FROM User WHERE UserRoleId IN: UserRoleIds]);
            result = userMap.keyset();
        }
        
        return result;
    }
    
    private static set<Id> getGroupsFromIdSet(set<Id> idsToSearch){
        set<Id> result = new set<Id>();
        for(Id currentId :idsToSearch){
            if(isAPublicGroup(currentId)){
                result.add(currentId);
            }
        }
        return result;
    }
    
    private static boolean isAPublicGroup(id idToCheck){
        return idToCheck.getSObjectType() == Schema.Group.getSObjectType();
    }

}