@IsTest
private class EWQuoteEditButtonControllerTest {

    static testMethod void testPostQuoteJSONBehavior_QuoteEdit() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        Map<String, Object> payload = EWQuoteEditButtonController.loadQuoteSetupData(quote.Id);

        System.assertNotEquals(null, payload);
        System.assert(payload.containsKey('isCommunity'));
        System.assert(payload.containsKey('quoteStatus'));
        System.assertEquals('Draft', payload.get('quoteStatus'));
    }
    static testMethod void testPostQuoteJSONBehavior_QuoteBindEmail() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        Map<String, Object> payload = EWQuoteEditButtonController.loadBindSetupData(quote.Id);

        System.assertNotEquals(null, payload);
        System.assert(payload.containsKey('hasSavedJsonData'));
        System.assert(Limits.getEmailInvocations() > 0);
    }

    static testMethod void testSaveQuoteCoverageSliders() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        Decimal excess = 19;
        Decimal bpc = 1000;
        Decimal total = 3333;

        EWQuoteEditButtonController.saveQuoteCoverageSliders(quote.Id, bpc, excess, total);

        quote = [
                SELECT Id, EW_BrokerCommissionPercent__c, EW_Excess__c, vlocity_ins__StandardPremium__c
                FROM Quote
                WHERE Id = :quote.Id
        ];

        System.assertEquals(bpc, quote.EW_BrokerCommissionPercent__c);
        System.assertEquals(excess, quote.EW_Excess__c);
        System.assertEquals(total, quote.vlocity_ins__StandardPremium__c);
    }
}