/* jshint expr: true */


TT.sharing = function($, options) {

  "use strict";

  var defaults = {
    templates    : {},
    container    : 'body',
    selector     : '.js-social a',
    isProduction : true,
    callback     : $.noop
  };

  var settings = $.extend({}, defaults, options);

  var remoteUrls = $.extend({
    'Facebook'  : 'http://www.facebook.com/sharer/sharer.php?u=URL&t=TITLE',
    'Twitter'   : 'https://twitter.com/share?url=URL&text=TITLE',
    'Tumblr'    : 'http://www.tumblr.com/share/link?url=URL&description=TITLE',
    'Pinterest' : 'http://pinterest.com/pin/create/link/?url=URL&description=TITLE',
    'Google+'   : 'https://plusone.google.com/_/+1/confirm?url=URL',
    'LinkedIn'  : 'https://www.linkedin.com/shareArticle?mini=true&url=URL&title=TITLE'
  }, settings.templates);



  function remoteUrl(type) {
    var url = remoteUrls[type];
    $.each(getInterpolations(), function(key, value) {
      url = url.replace(key.toUpperCase(), encodeURIComponent(value));
    });
    return url;
  }

  function getInterpolations() {
    return {
      title : document.title,
      url   : window.location.href
    };
  }

  function openPopup(url) {
    window.open(url, 'social', 'width=550,height=450,menubar=no,toolbar=no');
  }

  function alertUrl(id, url) {
    if(settings.logger) {
      settings.logger.log(['Sharing with ', id, ': "', url, '"'].join(''));
    }
  }

  function visitUrl(url) {
    url.match(/^mailto/) ? window.location.href = url : openPopup(url);
  }

  function clickHandler(event) {
    event.preventDefault();
    var a    = event.currentTarget;
    var type = $(a).data('social');
    var url  = remoteUrl(type);
    settings.isProduction ? visitUrl(url) : alertUrl(type, url);
    settings.callback(a, type);
  }

  function init() {
    $(settings.container).on('click', settings.selector, clickHandler);
  }



  init();

};