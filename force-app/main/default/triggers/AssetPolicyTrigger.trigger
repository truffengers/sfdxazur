trigger AssetPolicyTrigger on Asset (before insert, before update, after insert, after update) {
    TriggerDispatcher.Run(new AssetPolicyTriggerHandler());
}