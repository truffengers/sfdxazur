trigger InsurableItemTrigger on vlocity_ins__InsurableItem__c (after insert, after update) {
    TriggerDispatcher.Run(new InsurableItemTriggerHandler());
}