/**
* @name: EWInsurableItemHelper
* @description: Helper class for InsurableItem
* @author: Steve Loftus sloftus@azuruw.com
* @date: 23/10/2018
*/
public with sharing class EWInsurableItemHelper {

    public static final String INSURABLE_ITEM_SPECIFIED_ITEM_RT_DEV_NAME = 'EW_SpecifiedItem';
    public static final String SPECIFIED_ITEM_LIST_JSON_KEY = 'editBlockSpecifiedItems';
    public static final String SPECIFIED_ITEM_ID_JSON_KEY = 'itemId';
    private static final Set<String> OUTRA_UPDATE_REJECT_QUOTE_STATUSES = new Set<String>{
            'Quoted', 'Bound'
    };

    public EWInsurableItemHelper() {
    }


    public static PropertyItemResults PostPropertyItem(PropertyItemDetails propertyItemDetails) {

        PropertyItemResults propertyResults = new PropertyItemResults();

        vlocity_ins__InsurableItem__c propertyItem = getPropertyItem(propertyItemDetails, new vlocity_ins__InsurableItem__c());

        // get populated fields as a set for change checking and check for changes
        Set<String> propertyItemFields = getPopulatedFields(propertyItem);

        if(propertyItemDetails.recordId != null){

            List<vlocity_ins__InsurableItem__c> existingItems = getExistingItems(new Set<Id>{propertyItemDetails.recordId});

            // check for property item changes
            List<String> changedFields = new List<String>();
            for(vlocity_ins__InsurableItem__c existingItem : existingItems){

                // iterate through the fields to check for changes
                for(String field : propertyItemFields){
                    if(propertyItem.get(field) != existingItem.get(field)) {
                        changedFields.add(field);
                    }
                }
            }
            System.debug('Changed property fields = ' + changedFields);

            // return if nothing has changed
            if(changedFields.isEmpty()){
                propertyResults.recordId = propertyItemDetails.recordId;
                return propertyResults;
            }


            // clone so we keep an audit history
            String guid = EWInsurableItemHelper.generateGuid();
            propertyItem = EWInsurableItemHelper.CloneItems(new Map<Id, String>{
                    propertyItemDetails.recordId => guid
            }, existingItems, propertyItemDetails.quoteId).get(guid);
            // enrich propertyItem
            propertyItem = getPropertyItem(propertyItemDetails, propertyItem);

        }

        propertyItem.EW_Active__c = true;
        propertyItem.EW_CoverType__c = EWConstants.INSURABLE_ITEM_COVER_TYPE_PROPERTY;
        upsert propertyItem;

        propertyResults.recordId = propertyItem.Id;
        return propertyResults;
    }


    public static void enrichSpecifiedItems(SpecifiedItemsDetails specifiedItemDetails,
            List<SpecifiedItem> specifiedItemList) {

        // replaced PostSpecifiedItems which cloned and retained previous previous Specified Items
        // as the DML of specified items is already performed by the Quote OS

        List<vlocity_ins__InsurableItem__c> items = new List<vlocity_ins__InsurableItem__c>();

        if(specifiedItemList != null){
            for (SpecifiedItem item : specifiedItemList) {

                if(item.itemId != null){

                    vlocity_ins__InsurableItem__c specifiedItem = new vlocity_ins__InsurableItem__c(Id = item.itemId);
                    specifiedItem.Name = String.isEmpty(item.textItemName) ? item.textItemNameJewellery : item.textItemName;
                    specifiedItem.EW_Broker__c = specifiedItemDetails.brokerAccountId;
                    specifiedItem.EW_Quote__c = specifiedItemDetails.quoteId;
                    specifiedItem.EW_CoverType__c = EWConstants.INSURABLE_ITEM_COVER_TYPE_SPECIFIED_ITEM;
                    specifiedItem.vlocity_ins__AccountId__c = specifiedItemDetails.insuredAccountId;
                    specifiedItem.vlocity_ins__SpecProduct2Id__c = specifiedItemDetails.masterProductId;
                    specifiedItem.vlocity_ins__Description__c = String.isNotEmpty(item.textAreaDescription) ? item.textAreaDescription : item.textAreaDescriptionJewellery;

                    items.add(specifiedItem);

                }
            }
        }

        update items;

    }


    private static Set<String> getPopulatedFields(sObject s){

        String objectJson = JSON.serialize(s);
        Map<String,Object> objectMap = (Map<String,Object>) JSON.deserializeUntyped(objectJson);
        objectMap.remove('attributes');
        return objectMap.keySet();
    }



    private static vlocity_ins__InsurableItem__c getPropertyItem (PropertyItemDetails propertyItemDetails, vlocity_ins__InsurableItem__c propertyItem){

        propertyItem.Name = propertyItemDetails.name;
        propertyItem.RecordTypeId = propertyItemDetails.recordTypeId;
        propertyItem.vlocity_ins__AccountId__c = propertyItemDetails.insuredAccountId;
        propertyItem.vlocity_ins__SpecProduct2Id__c = propertyItemDetails.masterProductId;
        propertyItem.EW_Address1__c = propertyItemDetails.street;
        propertyItem.EW_Broker__c = propertyItemDetails.brokerAccountId;
        propertyItem.EW_BuildingName__c = propertyItemDetails.buildingName;
        propertyItem.EW_BuildingNumber__c = propertyItemDetails.buildingNumber;
        propertyItem.EW_BusinessUse__c = propertyItemDetails.businessUse;
        propertyItem.EW_City__c = propertyItemDetails.city;
        propertyItem.EW_Country__c = propertyItemDetails.country;
        propertyItem.EW_WaterScore__c = propertyItemDetails.waterScore;
        propertyItem.EW_FireScore__c = propertyItemDetails.fireScore;
        propertyItem.EW_FlatName__c = propertyItemDetails.flatName;
        propertyItem.EW_FlatRoofPercentage__c = propertyItemDetails.flatRoofPercentage;
        propertyItem.EW_FloodScore__c = propertyItemDetails.floodScore;
        propertyItem.EW_Geolocation__latitude__s = propertyItemDetails.latitude;
        propertyItem.EW_Geolocation__longitude__s = propertyItemDetails.longitude;
        propertyItem.EW_GoodStateOfRepair__c = EWInsurableItemHelper.forceBoolean(propertyItemDetails.goodStateOfRepair);
        propertyItem.EW_HasASafe__c = EWInsurableItemHelper.forceBoolean(propertyItemDetails.hasASafe);
        propertyItem.EW_ListedStatus__c = propertyItemDetails.listedStatus;
        propertyItem.EW_MinimumSecurity__c = EWInsurableItemHelper.forceBoolean(propertyItemDetails.minimumSecurity);
        propertyItem.EW_NoSignOfSubsidence__c = EWInsurableItemHelper.forceBoolean(propertyItemDetails.noSignOfSubsidence);
        propertyItem.EW_NotPlanningWorks__c = EWInsurableItemHelper.forceBoolean(propertyItemDetails.notPlanningWorks);
        propertyItem.EW_NotPreviouslyFlooded__c = EWInsurableItemHelper.forceBoolean(propertyItemDetails.notPreviouslyFlooded);
        propertyItem.EW_NumberOfBathrooms__c = propertyItemDetails.numberOfBathrooms;
        propertyItem.EW_NumberOfBedrooms__c = propertyItemDetails.numberOfBedrooms;
        propertyItem.EW_OpenToThePublic__c = propertyItemDetails.openToThePublic;
        propertyItem.EW_PostalCode__c = propertyItemDetails.postalCode;
        propertyItem.EW_PreviousInsuranceReason__c = propertyItemDetails.previousInsuranceReason;
        propertyItem.EW_Quote__c = propertyItemDetails.quoteId;
        propertyItem.EW_RebuildCost__c = propertyItemDetails.rebuildCost;
        propertyItem.EW_RoofConstruction__c = propertyItemDetails.roofConstruction;
        propertyItem.EW_SquareMetres__c = propertyItemDetails.squareMetres;
        propertyItem.EW_Style__c = propertyItemDetails.propertyStyle;
        propertyItem.EW_SubsidenceScore__c = propertyItemDetails.subsidenceScore;
        propertyItem.EW_SumInsuredBuildings__c = propertyItemDetails.sumInsuredBuildings;
        propertyItem.EW_SumInsuredTenantsImprovements__c = propertyItemDetails.sumInsuredTenantsImprovements;
        propertyItem.EW_Tenure__c = propertyItemDetails.tenure;
        propertyItem.EW_TheftScore__c = propertyItemDetails.theftScore;
        propertyItem.EW_TotalArtAndCollectablesInsured__c = propertyItemDetails.totalArtAndCollectablesInsured;
        propertyItem.EW_TotalJewelleryAndWatchesInsured__c = propertyItemDetails.totalJewelleryAndWatchesInsured;
        propertyItem.EW_TotalPedalCyclesInsured__c = propertyItemDetails.totalPedalCyclesInsured;
        propertyItem.EW_Type__c = propertyItemDetails.propertyType;
        propertyItem.EW_TypeOfAlarm__c = propertyItemDetails.typeOfAlarm;
        propertyItem.EW_WallConstruction__c = propertyItemDetails.wallConstruction;
        propertyItem.EW_WindScore__c = propertyItemDetails.windScore;
        propertyItem.EW_YearBuilt__c = propertyItemDetails.yearBuilt;
        propertyItem.EW_UPRN__c = propertyItemDetails.uprn;
        propertyItem.EW_AddressId__c = propertyItemDetails.addressId;
        propertyItem.EW_RebuildConfidenceScore__c = propertyItemDetails.rebuildConfidenceScore;
        propertyItem.EW_MarketValue__c = propertyItemDetails.marketValue;
        //Added by Rajni bajpai - EWH-2053
        propertyItem.EW_PostTown__c = propertyItemDetails.postTown;
        return propertyItem;
    }


    private static List<vlocity_ins__InsurableItem__c> getExistingItems(Set<Id> ids) {
        return [
            SELECT
                    EW_Active__c, EW_Address1__c, EW_Address2__c, EW_AddressId__c, EW_Application__c, EW_Broker__c,
                    EW_BuildingName__c, EW_BuildingNumber__c, EW_BusinessUse__c, EW_City__c, EW_Country__c,
                    EW_CoverType__c, EW_FireScore__c, EW_FlatName__c, EW_FlatRoofPercentage__c,
                    EW_FloodScore__c, EW_Geolocation__c, EW_GoodStateOfRepair__c, EW_HasASafe__c, EW_Image__c,
                    EW_ListedStatus__c, EW_Make__c, EW_MarketValue__c, EW_MinimumSecurity__c, EW_Model__c,
                    EW_ModelYear__c, EW_NoSignOfSubsidence__c, EW_NotPlanningWorks__c, EW_NotPreviouslyFlooded__c,
                    EW_Not_Used_For_Business__c, EW_NumberOfBathrooms__c, EW_NumberOfBedrooms__c,
                    EW_OpenToThePublic__c, EW_ParentItem__c, EW_PostalCode__c, EW_PreviousInstance__c,
                    EW_PreviousInsuranceReason__c, EW_Quote__c, EW_RebuildCost__c, EW_RebuildConfidenceScore__c,
                    EW_RoofConstruction__c, EW_SquareMetres__c, EW_Standard_Construction__c, EW_State__c,
                    EW_Style__c, EW_SubsidenceScore__c, EW_SumInsuredBuildings__c,
                    EW_SumInsuredTenantsImprovements__c, EW_Tenure__c, EW_TheftScore__c,
                    EW_TotalArtAndCollectablesInsured__c, EW_TotalJewelleryAndWatchesInsured__c,
                    EW_TotalPedalCyclesInsured__c, EW_TotalTenantsImprovementsInsured__c, EW_Type__c,
                    EW_TypeOfAlarm__c, EW_UPRN__c, EW_WallConstruction__c, EW_WaterScore__c, EW_WindScore__c,
                    EW_YearBuilt__c, Id, RecordTypeId, Name, EW_Geolocation__latitude__s, EW_Geolocation__longitude__s,
                    vlocity_ins__AccountId__c, vlocity_ins__AttributeSelectedValues__c, vlocity_ins__Description__c,
            vlocity_ins__LeadId__c, vlocity_ins__SpecProduct2Id__c, EW_PostTown__c
            FROM vlocity_ins__InsurableItem__c
            WHERE Id = :ids
            AND EW_Active__c = true
        ];
    }



    private static Boolean forceBoolean(Boolean field) {
        return (field == null) ? false : field;
    }

    private static String generateGuid() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8) + '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }

    public static Map<String, vlocity_ins__InsurableItem__c> CloneItems(Map<Id, String> guidByItemIdMap, List<vlocity_ins__InsurableItem__c> existingItems, Id quoteId) {

        Map<String, vlocity_ins__InsurableItem__c> resultMap = new Map<String, vlocity_ins__InsurableItem__c>();
        Map<Id, vlocity_ins__InsurableItem__c> existingItemByIdMap = new Map<Id, vlocity_ins__InsurableItem__c>();

        Savepoint sp = Database.setSavepoint();

        try {

            for (vlocity_ins__InsurableItem__c existingItem : existingItems) {

                // if the quote id is null then return the existing item
                if (existingItem.EW_Quote__c == null) {

                    resultMap.put(guidByItemIdMap.get(existingItem.Id), existingItem);

                // otherwise clone it and return the new one
                // this is for specified items created in the os which need linking ot the quote etc.
                } else {

                    existingItemByIdMap.put(existingItem.Id, existingItem);
                }
            }

            if (!existingItemByIdMap.isEmpty()) {

                for (vlocity_ins__InsurableItem__c newItem : existingItemByIdMap.values().deepClone(true)) {

                    Id existingItemId = newItem.Id;

                    newItem.Id = null;
                    newItem.EW_PreviousInstance__c = existingItemId;
                    resultMap.put(guidByItemIdMap.get(existingItemId), newItem);

                    if(quoteId == existingItemByIdMap.get(existingItemId).EW_Quote__c ) existingItemByIdMap.get(existingItemId).EW_Active__c = false;
                }

                update existingItemByIdMap.values();
            }

        } catch (Exception e) {

            Database.rollback(sp);

            LogBuilder logsBuilder = new LogBuilder();
            Azur_Log__c log = logsBuilder.createLogWithCodeInfo(e, 'EW',  'EWInsurableItemHelper', 'CloneItem');
            insert log;         
        }
    
        return resultMap;
    }

    public void checkForOutraChanges(Map<Id, SObject> oldItems, Map<Id, SObject> newItems) {
        Set<Id> validOutraQuoteIds = getValidOutraQuoteIds(newItems);
        Set<String> fieldsToControl = EWJSONHelper.outraFieldMapping.keySet();

        Map<Id, Map<String, Object>> quotesToUpdate = new Map<Id, Map<String, Object>>();
        Set<Id> propertyItems = filterForPropertyItems(newItems);

        for (Id iiId : propertyItems) {

            Boolean hasUpdates = false;
            vlocity_ins__InsurableItem__c oldII = (vlocity_ins__InsurableItem__c) oldItems.get(iiId);
            vlocity_ins__InsurableItem__c newII = (vlocity_ins__InsurableItem__c) newItems.get(iiId);

            if (!validOutraQuoteIds.contains(newII.EW_Quote__c)) {
                if (!(oldII.EW_Active__c && !newII.EW_Active__c)) {
                    newII.Name.addError('You cannot update a Property item when it belongs to a Quote whose status is Quoted or Bound.');
                    continue;
                }
            }

            for (String fieldName : fieldsToControl) {
                if (oldII.get(fieldName) != newII.get(fieldName)) {
                    hasUpdates = true;
                    break;
                }
            }

            if (hasUpdates) {
                Id quoteId = newII.EW_Quote__c;
                Map<String, Object> updates = new Map<String, Object>();

                for (String fieldName : fieldsToControl) {
                    updates.put(fieldName, newII.get(fieldName));
                }
                quotesToUpdate.put(quoteId, updates);
            }
        }

        if (!quotesToUpdate.isEmpty()) {
            EWJSONHelper.updateOutraNodeOnIIUpdate(quotesToUpdate);
        }

    }

    private Set<Id> filterForPropertyItems(Map<Id, SObject> items) {
        Set<Id> result = new Set<Id>();
        Id propertyItemRT = vlocity_ins__InsurableItem__c.getSObjectType().getDescribe()
                .getRecordTypeInfosByDeveloperName().get(EWConstants.INSURABLE_ITEM_RT_DEV_NAME_PROPERTY).getRecordTypeId();

        for (Id iiId : items.keySet()) {
            vlocity_ins__InsurableItem__c insurableItem = (vlocity_ins__InsurableItem__c) items.get(iiId);
            if (insurableItem.RecordTypeId == propertyItemRT) {
                result.add(iiId);
            }
        }

        return result;
    }

    private Set<Id> getValidOutraQuoteIds(Map<Id, SObject> newItems) {
        Set<Id> quoteIds = new Set<Id>();
        for (Id iiId : newItems.keySet()) {
            vlocity_ins__InsurableItem__c newII = (vlocity_ins__InsurableItem__c) newItems.get(iiId);
            quoteIds.add(newII.EW_Quote__c);
        }

        Map<Id, Quote> quotes = new Map<Id, Quote>([
                SELECT Id
                FROM Quote
                WHERE Status NOT IN :OUTRA_UPDATE_REJECT_QUOTE_STATUSES
                AND Id IN :quoteIds
        ]);

        return quotes.keySet();
    }

    public void setSpecifiedItemsQuantities(List<SObject> items) {
        Set<Id> quotesToRecalculateIds = new Set<Id>();
        Id specifiedItemRT = vlocity_ins__InsurableItem__c.getSObjectType().getDescribe()
                .getRecordTypeInfosByDeveloperName().get(EWConstants.INSURABLE_ITEM_RT_DEV_NAME_SPECIFIED_ITEM).getRecordTypeId();

        for (vlocity_ins__InsurableItem__c ii : (List<vlocity_ins__InsurableItem__c>) items) {
            if (ii.RecordTypeId == specifiedItemRT) {
                quotesToRecalculateIds.add(ii.EW_Quote__c);
            }
        }

        List<Quote> quotesToUpdate = new List<Quote>([
                SELECT Id, (
                        SELECT Id, EW_Type__c
                        FROM Insurable_Items__r
                        WHERE RecordTypeId = :specifiedItemRT AND EW_Type__c IN :EWConstants.SPECIFIED_ITEM_TYPE_PRODUCT_NAME_MAPPING.keySet()
                ), (
                        SELECT Id, Quantity, Product2.Name
                        FROM QuoteLineItems
                        WHERE Product2.Name IN :EWConstants.SPECIFIED_ITEM_TYPE_PRODUCT_NAME_MAPPING.values()
                )
                FROM Quote
        ]);

        List<QuoteLineItem> quoteLineItemsToUpdate = new List<QuoteLineItem>();

        for (Quote q : quotesToUpdate) {
            Map<String, Integer> quantityMapping = new Map<String, Integer>();
            if (q.Insurable_Items__r.isEmpty() || q.QuoteLineItems.isEmpty()) {
                continue;
            }

            for (String type : EWConstants.SPECIFIED_ITEM_TYPE_PRODUCT_NAME_MAPPING.keySet()) {
                quantityMapping.put(type, 0);
            }

            for (vlocity_ins__InsurableItem__c insurableItem : q.Insurable_Items__r) {
                quantityMapping.put(insurableItem.EW_Type__c, quantityMapping.get(insurableItem.EW_Type__c) + 1);
            }

            for (QuoteLineItem qli : q.QuoteLineItems) {
                String productName = qli.Product2.Name;
                for (String type : EWConstants.SPECIFIED_ITEM_TYPE_PRODUCT_NAME_MAPPING.keySet()) {
                    if (EWConstants.SPECIFIED_ITEM_TYPE_PRODUCT_NAME_MAPPING.get(type) == productName) {
                        Integer quantity = quantityMapping.get(type);
                        if (quantity > 0) {
                            qli.Quantity = (Decimal) quantity;
                            quoteLineItemsToUpdate.add(qli);
                        }
                        break;
                    }
                }
            }
        }

        update quoteLineItemsToUpdate;
    }

    public void updateQuoteJSONDataAfterSpecifiedItemDelete(Map<Id, vlocity_ins__InsurableItem__c> deletedIIs) {
        Set<Id> quoteIds = new Set<Id>();

        Id specifiedItemRTId = vlocity_ins__InsurableItem__c.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(INSURABLE_ITEM_SPECIFIED_ITEM_RT_DEV_NAME).getRecordTypeId();
        Map<Id, vlocity_ins__InsurableItem__c> deletedSpecifiedItems = new Map<Id, vlocity_ins__InsurableItem__c>();

        for (vlocity_ins__InsurableItem__c insurableItem : deletedIIs.values()) {
            if (insurableItem.RecordTypeId == specifiedItemRTId) {
                quoteIds.add(insurableItem.EW_Quote__c);
                deletedSpecifiedItems.put(insurableItem.Id, insurableItem);
            }
        }

        List<EW_QuoteJSONData__c> quoteJSONData = new List<EW_QuoteJSONData__c>([
                SELECT Id, EW_PropertyNode__c
                FROM EW_QuoteJSONData__c
                WHERE EW_Quote__c IN :quoteIds
        ]);

        for (EW_QuoteJSONData__c jsonData : quoteJSONData) {
            Map<String, Object> propertyNodeMap = (Map<String, Object>) JSON.deserializeUntyped(jsonData.EW_PropertyNode__c);
            Object editBlockSpecifiedItems = propertyNodeMap.get(SPECIFIED_ITEM_LIST_JSON_KEY);

            // for just one specifiedItem - it's an object right away & object == Map<key, value>
            if (editBlockSpecifiedItems instanceof Map<String, Object>) {
                Map<String, Object> specifiedItem = (Map<String, Object>) editBlockSpecifiedItems;
                if (deletedSpecifiedItems.containsKey(String.valueOf(specifiedItem.get(SPECIFIED_ITEM_ID_JSON_KEY)))) {
                    propertyNodeMap.put(SPECIFIED_ITEM_LIST_JSON_KEY, null);
                }
            }
            // for just more than one specifiedItem - it's a list of objects, so List<Map<key, value>>
            if (editBlockSpecifiedItems instanceof List<Object>) {
                List<Object> specifiedItemListOld = (List<Object>) editBlockSpecifiedItems;
                List<Object> specifiedItemListNew = new List<Object>();
                for (Object specifiedItemObj : specifiedItemListOld) {
                    Map<String, Object> specifiedItem = (Map<String, Object>) specifiedItemObj;
                    if (!deletedSpecifiedItems.containsKey(String.valueOf(specifiedItem.get(SPECIFIED_ITEM_ID_JSON_KEY)))) {
                        specifiedItemListNew.add(specifiedItemObj);
                    }
                }

                if (specifiedItemListNew.size() == 1) {
                    propertyNodeMap.put(SPECIFIED_ITEM_LIST_JSON_KEY, specifiedItemListNew.get(0));
                } else {
                    propertyNodeMap.put(SPECIFIED_ITEM_LIST_JSON_KEY, specifiedItemListNew);
                }
            }

            jsonData.EW_PropertyNode__c = JSON.serializePretty(propertyNodeMap);
        }

        update quoteJSONData;
    }


    /**
    * @methodName: checkMainResidenceUPRN
    * @description: method called in the renewal process from the Quote omniscript (via the EWVlocityRemoteActionHelper) to check the UPRN response from Outra matches the address we have in the bound quote.
    *   This is because Ambiental (the original provider of the UPRN through the Postcode typeahead box), has during 2019 updated and fixed their UPRN database so its possible our records have
    *   the wrong UPRN value.  If the output is different, we will refer the renewal quote to the UW.  (EWH-1337 refers)
    * @params: boundQuoteId, propertyData (the 'outraAPIResponse' node in the Omniscript JSON), outputMap for response
    * @author: Roy Lloyd
    * @dateCreated: 27/2/2020
    */
    public static void checkMainResidenceUPRN(Id boundQuoteId, Map<String,Object> propertyData, Map<String,Object> outputMap){
        if(boundQuoteId == null || propertyData == null) return;

        Map<String,Object> outraAPIResponse = (Map<String,Object>) propertyData.get('outraAPIResponse');
        Map<String,Object> ambAPIResponse = (Map<String,Object>) propertyData.get('ambAPIResponse');

        String queryString = 'SELECT Id,EW_PostalCode__c FROM vlocity_ins__InsurableItem__c WHERE EW_Quote__c = :boundQuoteId AND EW_Active__c = true ORDER BY CreatedDate DESC LIMIT 1';
        vlocity_ins__InsurableItem__c boundQuoteMainResidence = Database.query(queryString);

        Boolean isRight = boundQuoteMainResidence.get('EW_PostalCode__c') == (String) outraAPIResponse.get('postcode') && boundQuoteMainResidence.get('EW_PostalCode__c') == (String) ambAPIResponse.get('postcode');
        outputMap.put('renewalUPRNnotRight', !isRight);

    }




    public class SpecifiedItemsDetails {
        public Id brokerAccountId;
        public Id quoteId;
        public Id insuredAccountId;
        public Id masterProductId;
        public Integer itemCount;
    }

    public class SpecifiedItem {
        public String globalKey;
        public Id itemId;
        public Id recordTypeId;
        public String selectItemType;
        public String textItemName;
        public String textItemNameJewellery;
        public Decimal textItemValue;
        public String textAreaDescription;
        public String textAreaDescriptionJewellery;
        public List<FileItem> imageItemUpload;

        public SpecifiedItem() {

            this.imageItemUpload = new List<FileItem>();
        }
    }

    public class FileItem {
        public String filename;
        public Integer size;
        public String data;
        public Id contentId;
    }

    public class PropertyItemDetails {
        public Id recordId;
        public String name;
        public Id brokerAccountId;
        public Id quoteId;
        public Id insuredAccountId;
        public Id masterProductId;
        public Id recordTypeId;
        public String buildingName;
        public String buildingNumber;
        public String flatName;
        public String street;
        public String city;
        public String country;
        public String postalCode;
        public Decimal latitude;
        public Decimal longitude;
        public Decimal floodScore;
        public Decimal fireScore;
        public Decimal subsidenceScore;
        public Decimal theftScore;
        public Decimal waterScore;
        public Decimal windScore;
        public Integer numberOfBathrooms;
        public Integer numberOfBedrooms;
        public Decimal squareMetres;
        public String propertyType;
        public String propertyStyle;
        public String tenure;
        public String previousInsuranceReason;
        public String wallConstruction;
        public String roofConstruction;
        public String flatRoofPercentage;
        public String listedStatus;
        public Boolean minimumSecurity;
        public Boolean goodStateOfRepair;
        public Boolean notPreviouslyFlooded;
        public Boolean noSignOfSubsidence;
        public Boolean notPlanningWorks;
        public String openToThePublic;
        public String businessUse;
        public Boolean hasASafe;
        public String typeOfAlarm;
        public String yearBuilt;
        public Decimal sumInsuredBuildings;
        public Decimal sumInsuredTenantsImprovements;
        public Decimal rebuildCost;
        public Decimal totalJewelleryAndWatchesInsured;
        public Decimal totalArtAndCollectablesInsured;
        public Decimal totalPedalCyclesInsured;
        public String uprn;
        public String addressId;
        public Decimal rebuildConfidenceScore;
        public Decimal marketValue;
        public String postTown;
        public PropertyItemDetails() {
        }
    }

    public class PropertyItemResults {

        public Id recordId;

        public PropertyItemResults() {
        }
    }
}