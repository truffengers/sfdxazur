/**
* @name: EWRenewalQuoteReminderBatch
* @description: Re-send the Renewal Quote email 7 days before the Effective Date of the Renewal Quote to remind brokers of upcoming renewal
* @author: Andrew Xu axu@azuruw.com
* @date: 11/06/2020
*
*/

public class EWRenewalQuoteReminderBatch implements Database.Batchable<SObject>{

    /**
    * @methodName: start
    * @description: Compose a query locator fetching all Renewal Quotes where the Effective Date is 7 days from today
    * @dateCreated: 11/06/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface
    */
    public Database.QueryLocator start(Database.BatchableContext bc) {

        String quoteTransactionType = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        String queryString = 'SELECT Id, OwnerId, EWQuoteDisplayNumber__c FROM Quote WHERE EW_TypeofTransaction__c = :quoteTransactionType AND vlocity_ins__EffectiveDate__c = NEXT_N_DAYS:7 AND vlocity_ins__EffectiveDate__c > NEXT_N_DAYS:6 AND Status = \'Quoted\'';
        return Database.getQueryLocator(queryString);
    }

    /**
    * @methodName: execute
    * @description: Send the Renewal Quote to all owners of quotes fetched using the query
    * @dateCreated: 11/06/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface
    */
    public void execute(Database.BatchableContext bc, List<Quote> quotes) {
        try {

            for (Quote quote : quotes) {
                EWEmailHelper.EmailOptions emailOptions = new EWEmailHelper.EmailOptions();

                emailOptions.templateName = EWConstants.EMAIL_TEMPLATE_NAME_BROKERRENEWALQUOTEREMINDER;
                emailOptions.targetObjectId = quote.OwnerId;
                emailOptions.whatId = quote.Id;

                String displayNumberString = '%' + quote.EWQuoteDisplayNumber__c + '.pdf';
                List<Attachment> attachments = [SELECT Id FROM Attachment WHERE ParentId = :quote.Id AND Name LIKE :displayNumberString ORDER BY CreatedDate DESC LIMIT 1];
                List<Id> attachmentIdList = new List<Id>();

                for (Attachment a : attachments) {
                    attachmentIdList.add(a.Id);
                }

                emailOptions.osAttachmentList = new List<Id>(attachmentIdList);
                EWEmailHelper.SendVlocityEmail(emailOptions);
            }
        } catch (Exception ex) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = 'EWRenewalQuoteReminderBatch';
            log.Method__c = 'execute';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWRenewalQuoteReminderBatch.execute'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
            insert log;
            System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            throw ex;
        }
    }

    /**
    * @methodName: finish
    * @description: Sends a message regarding batch job results to users mentioned in ApexEmailNotification object
    * @dateCreated: 11/06/2020
    * @parameters: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface
    * @returns: Parameters and return values are not described, because they are inherited from Database.Batchable<SObject> interface
    */
    public void finish(Database.BatchableContext bc) {
        try {

            AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, TotalJobItems FROM AsyncApexJob where Id =:BC.getJobId()];
            List<ApexEmailNotification> emailNotifications = [SELECT ID, User.Email FROM ApexEmailNotification];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> toAddresses = new List<String> ();
            for (ApexEmailNotification emailNotification : emailNotifications) {

                toAddresses.add(emailNotification.User.Email);
            }
            mail.setToAddresses(toAddresses);
            mail.setSubject('Renewal Quote Reminder Email Batch ' + a.Status);
            mail.setPlainTextBody('Number of processed batches: ' + a.TotalJobItems +
                    '\n  Number of failed batches: '+ a.NumberOfErrors + '\n\n Detailed information about failures can be found in Azur Log records');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (Exception ex) {

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = 'EWPolicyLapsingBatch';
            log.Method__c = 'finish';
            log.Log_message__c = log.Log_message__c
                    + '\n\n method name = EWPolicyLapsingBatch.finish'
                    + '\n\n ln: ' + ex.getLineNumber()
                    + '\n\n st: ' + ex.getStackTraceString();
            System.debug('DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            insert log;
            throw ex;
        }
    }

}