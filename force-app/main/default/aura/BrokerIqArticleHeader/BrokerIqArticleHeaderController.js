({
    onArticleHeaderInit : function(component) {
        if (TT && TT.imageParallax) {
            TT.imageParallax.register($('#js-content-header-with-image'), { height: '125' });
        }
    },
    onRender : function(component) {
        component.set(
            'v.bgImage',
            // Arbitrarily choosing a more-or-less suitable image size from the
            // content record
            component.get('v.iqContentRecord').Carousel_Image_1440_URL__c
        );
    }
})