({
    
	doOnRender : function(cmp) {
    
        // listen for clicked links
        var ewQuotePage = '/new-quote';
        var forEach = Array.prototype.forEach;
        var links = document.getElementsByTagName('a');
        forEach.call(links, function (link) {
            link.onclick = function (e) {
                var currentPage = window.location.href+'';
                if(currentPage.includes(ewQuotePage)){
                    var check = confirm($A.get("$Label.c.EWLeavingPageError"));
                    if(!check){
                        e.preventDefault();
                        e.stopPropagation();
                    }            
				}
			}
        });
	}
    
})