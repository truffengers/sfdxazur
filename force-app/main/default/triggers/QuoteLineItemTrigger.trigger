/**
 * Created by llyrjones on 04/12/2018.
 */

trigger QuoteLineItemTrigger on QuoteLineItem (before insert, before update) {

    TriggerDispatcher.Run(new QuoteLineItemTriggerHandler());

}