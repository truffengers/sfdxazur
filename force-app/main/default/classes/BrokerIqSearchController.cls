public class BrokerIqSearchController {
    
    public class Tab {
        public List<String> webcastsJson {get; set;}
        public Integer selectedItemsPerPage {get; set;}     
        public BrokerIqSearchController parent;
        
        public Integer pageNumber {get; set;}
        public Integer totalRecords {get; set;}
        public Integer firstRecord {get; set;}
        public Integer lastRecord {get; set;}
        
        public Integer nextPage {get; set;}
        
        public List<Integer> pages {get; set;}
        
        public Integer tabNumber {get; set;}
        
        public String tabId {get; set;}
        
        private String startDateComparator;
        private String startDateOrder;

        public Tab(BrokerIqSearchController parent, Integer tabNumber, String startDateComparator, String startDateOrder, String tabId) {
            this.parent = parent;
            this.tabNumber = tabNumber;
            this.startDateComparator = startDateComparator;
            this.startDateOrder = startDateOrder;
            this.tabId = tabId;
            
            selectedItemsPerPage = 6;
            pageNumber = 1;
            updateResults();
        }
        
        public void updateResults() {
            DateTime now = DateTime.now();
            Decimal channelId = parent.channelId;
            String categoryId = parent.categoryId;
            List<BrightTALK__Webcast__c> webcastsResult;
            if(parent.q != '') {
                String searchQuery = BrokerIqWebinarPreviewController.getSearch(
                    parent.q, 'BrightTALK__Channel__r.BrightTALK__Channel_Id__c  = :channelId '
                    + 'AND BrightTALK__Start_Date__c ' + startDateComparator + ' :now '
                    + 'AND Status__c != null AND IQ_Content__c != null '
                    +  (categoryId != null ? 'AND IQ_Content__r.Category__c = :categoryId ' : '')
                    + 'ORDER BY BrightTALK__Start_Date__c ' + startDateOrder);
                
                List<List<sObject>> webcastsResultAllObjects = Search.query(searchQuery);
                webcastsResult = webcastsResultAllObjects[0];
            } else {
                webcastsResult = Database.query(BrokerIqWebinarPreviewController.getQuery(
                    'BrightTALK__Channel__r.BrightTALK__Channel_Id__c  = :channelId '
                    + 'AND BrightTALK__Start_Date__c ' + startDateComparator + ' :now '
                    + 'AND Status__c != null AND IQ_Content__c != null '
                    +  (categoryId != null ? 'AND IQ_Content__r.Category__c = :categoryId ' : '')
                    + 'ORDER BY BrightTALK__Start_Date__c ' + startDateOrder + ' LIMIT 100'                	
                ));
            }
            totalRecords = webcastsResult.size();
            pages = new List<Integer>();
            for(Integer i=0; i*selectedItemsPerPage < totalRecords; i++) {
                pages.add(i+1);
            }
            List<BrightTALK__Webcast__c> webcastsResultsToShow = new List<BrightTALK__Webcast__c>();
            firstRecord = (pageNumber-1)*selectedItemsPerPage+1;
            lastRecord = Math.min(pageNumber*selectedItemsPerPage, totalRecords);
            
            for(Integer i = firstRecord-1; i < lastRecord; i++) {
               webcastsResultsToShow.add(webcastsResult[i]);
            }
            webcastsJson = BrokerIqWebinarPreviewController.webcastsToListOfJson(webcastsResultsToShow);
        }
        
        public void toPage() {
            if(nextPage != null && nextPage <= pages.size() && nextPage >= 1) {
				pageNumber = nextPage;                
            }
            updateResults();
        }
    }
    
    public String currentQuery {get; set;}
    public String activeTabId {get; set;}
    
    public List<SelectOption> itemsPerPageOptions {get; set;}
    public List<Tab> tabs {get; set;}
    public Integer thisTabNumber {get; set;}
    
    private String q;
    private Decimal channelId;
    private String categoryId;
    public Category__c category {get; set;}
    
    public BrokerIqSearchController() {
        Map<String, String> params = ApexPages.currentPage().getParameters();
        activeTabId = params.get('activeTab');
        if(activeTabId == null || activeTabId == ''){
            activeTabId = 'recent';
        }
        q = params.get('q');
        if(q == null) {
            q = '';
        }
        categoryId = params.get('categoryId');
        if(categoryId != null) {
            categoryId = String.escapeSingleQuotes(categoryId);
            category = [SELECT Id, Icon__c, Colour__c, Name FROM Category__c WHERE Id = :categoryId];
        }
        currentQuery = EncodingUtil.urlDecode(q, 'UTF-8');
        q = String.escapeSingleQuotes(q);

        BrightTalkApi api = new BrightTalkApi();
        BrightTALK_API_Settings__c settings = api.getSettings();
        channelId = settings.Channel_Id__c;
        
        itemsPerPageOptions = new List<SelectOption>{
            new SelectOption('6', '6'),
                new SelectOption('12', '12'),
                new SelectOption('24', '24')
                };
                
        tabs = new List<Tab>{new Tab(this, 0, '<', 'DESC', 'recent'), new Tab(this, 1, '>', 'ASC', 'upcoming')};
        
    }
    
    public void toPage() {
        tabs[thisTabNumber].toPage();
    }
    
    public void itemsPerPageChanged() {
        tabs[thisTabNumber].updateResults();
    }
}