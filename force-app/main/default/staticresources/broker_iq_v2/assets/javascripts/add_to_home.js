TT.addToHome = (function($) {

  var $element;

  var WINDOWS_PHONE = 'windows-phone';
  var ANDROID       = 'android';
  var IOS           = 'ios';

  var mobileOS;
  var COOKIE_KEY = 'add-to-home-concealed';

  var ON     = 'on';
  var OFF    = 'off';

  var DELAY_MS = 3000;

  var isRevealed;

  var selector;



  function referenceElements() {
    $element = $(selector);
  }

  function turnListeners(onOfOff) {
    $('body')[onOfOff]('click touchend', conceal);
  }

  function detectMobileOS() {
    mobileOS = getMobileOS();
  }

  function getMobileOS() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if(/windows phone/i.test(userAgent)) {
      return WINDOWS_PHONE;
    } else if(/android/i.test(userAgent)) {
      return ANDROID;
    } else if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return IOS;
    }
  }

  function getPreviouslyConcealed() {
    return Cookies.get(COOKIE_KEY) !== undefined;
  }

  function getIsAppropriate() {
    return getIsIosOrAndroid() && !getPreviouslyConcealed();
  }

  function getIsIosOrAndroid() {
    return mobileOS === IOS || mobileOS === ANDROID;
  }

  function revealIfAppropriate() {
    if(getIsAppropriate()) { revealAfterDelay(); }
  }

  function revealAfterDelay() {
    window.setTimeout(reveal, DELAY_MS);
  }

  function reveal() {
    referenceElements();
    turnListeners(ON);
    toggleMobileOsClass();
    toggleVisibilityClass();
    isRevealed = true;
  }

  function conceal() {
    if(isRevealed) {
      isRevealed = false;
      Cookies.set(COOKIE_KEY, true, { expires: 28 });
      turnListeners(OFF);
      toggleVisibilityClass();
    }
  }

  function toggleMobileOsClass() {
    $element.toggleClass('is-' + mobileOS);
  }

  function toggleVisibilityClass() {
    $element.toggleClass('is-visible');
  }

  function init(value) {
    selector = value;
    detectMobileOS();
    revealIfAppropriate();
  }



  return { init: init };

}(jQuery));