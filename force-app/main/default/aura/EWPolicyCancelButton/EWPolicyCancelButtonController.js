({
    doInit: function (component, event, helper) {
        const recordId = component.get('v.recordId');
        let action = component.get("c.checkIfCommunity");

        action.setCallback(this, function (response) {
            const responseJson = response.getReturnValue();
            let redirectToRecord = false;

            if (responseJson && responseJson.isCommunity) {

                let urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    'url': '/policy-cancellation?recordId=' + recordId
                });
                urlEvent.fire();

            } else {
                redirectToRecord = true;
            }

            if (redirectToRecord) {
                var sObjectEvent = $A.get("e.force:navigateToSObject");
                sObjectEvent.setParams({
                    "recordId": recordId
                });
                sObjectEvent.fire();
            }

        });
        $A.enqueueAction(action);
    }
});