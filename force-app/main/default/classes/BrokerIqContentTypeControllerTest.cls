/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 14/10/2017
 * Description: (if required)
 */

@isTest
private class BrokerIqContentTypeControllerTest {
    @isTest static void testBehavior() {
        Broker_iQ_Record_Type__mdt metadata = [SELECT Id, DeveloperName
        FROM Broker_iQ_Record_Type__mdt
        LIMIT 1];

        BrokerIqSearchFilterWrapper wrapper = BrokerIqContentTypeController.getSearchFilterWrapper(metadata.Id);

        System.assertNotEquals(null, wrapper);
    }
}