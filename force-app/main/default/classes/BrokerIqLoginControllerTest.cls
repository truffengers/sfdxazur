@isTest
private class BrokerIqLoginControllerTest {
	
	@isTest static void basic() {
		BrokerIqLoginController controller = new BrokerIqLoginController();
		controller.username = 'un';
		controller.password = 'pw';
		controller.login();

		// Nothing we can assert about this 
	}

    @isTest static void forgotPasswordFail() {
		BrokerIqLoginController controller = new BrokerIqLoginController();
		controller.username = 'un';

        controller.forgotPassword();
        
        List<ApexPages.Message> messages = ApexPages.getMessages();
        
        System.assertEquals(1, messages.size(), messages);
        System.assertEquals(Label.Password_Reset_Fail, messages[0].getSummary(), messages);
        
        // NB Site.forgotPassword() will always fail in a test, so no test
        // for success
	}
	@isTest static void setPassword() {
		BrokerIqLoginController controller = new BrokerIqLoginController();
		controller.newPassword = 'pw';
		controller.verifyNewPassword = 'pw';
		controller.setPassword();

		// Nothing we can assert about this 
	}
	
	
}