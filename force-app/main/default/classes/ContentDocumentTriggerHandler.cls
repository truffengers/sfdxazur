public with sharing class ContentDocumentTriggerHandler implements ITriggerHandler {

    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return ContentDocumentTriggerHandler.IsDisabled != null ? ContentDocumentTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
        new EWContentDocumentHelper().handleOnBeforeDelete(oldItems);
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
        new EWContentDocumentHelper().handleOnAfterUpsert(newItems);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        new EWContentDocumentHelper().handleOnAfterUpsert(newItems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterUndelete(Map<Id, SObject> newItems) {
    }
}