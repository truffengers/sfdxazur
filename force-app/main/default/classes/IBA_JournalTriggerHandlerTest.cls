/**
* @name: IBA_JournalTriggerHandlerTest
* @description: Test class for IBA_JournalTriggerHandler
*
*/
@IsTest
private class IBA_JournalTriggerHandlerTest {

    private static final String COMPANY_NAME = 'ApexTestCompany';
    private static final String CURRENCY_USD = 'USD';

    @testSetup
    static void createTestRecords() {
        IBA_TestDataFactory.FinancialForceSetup ffs = IBA_TestDataFactory.setupFinancialForce(COMPANY_NAME, CURRENCY_USD);
        Group companyGroup = ffs.companyGroup;
        c2g__codaCompany__c company = ffs.company;
    
        c2g__codaYear__c yr = IBA_TestDataFactory.createYear(true, companyGroup.Id, company.Id);
        c2g.CODAYearWebService.calculatePeriods(yr.Id);

        c2g__codaAccountingCurrency__c curr = IBA_TestDataFactory.createCurrency(true, CURRENCY_USD);

        c2g__codaGeneralLedgerAccount__c gla = IBA_TestDataFactory.createGeneralLedgerAccount(true, 'Brochures', 'uniqueCaseInsensitive24!');

        Product2 product = IBA_TestDataFactory.createProduct(false, 'Product 1', IBA_UtilConst.PRODUCT_RT_Master);
        product.IBA_AIGProduct__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
        product.IBA_AIGMinorLine__c = 'PCG Test Liability';
        product.IBA_AIGPolicyCodeNumber__c = '000000test';
        product.IsActive = true;
        product.c2g__CODASalesRevenueAccount__c = gla.Id;
        insert product;

        Account acc = IBA_TestDataFactory.createAccount(false, 'testAccOnHold', IBA_UtilConst.ACCOUNT_RT_BROKER);
        acc.c2g__CODAAccountTradingCurrency__c = CURRENCY_USD;
        acc.c2g__CODAAccountsPayableControl__c = gla.Id;
        acc.c2g__CODAAccountsReceivableControl__c = gla.Id;
        acc.c2g__CODAVATStatus__c = 'Home';
        acc.c2g__CODATaxCalculationMethod__c = 'Gross';
        acc.c2g__CODABaseDate1__c = 'Invoice Date';
        acc.c2g__CODADaysOffset1__c = 0;
        acc.c2g__CODADescription1__c = 'Test';
        acc.c2g__CODADiscount1__c = 0;
        insert acc;

        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c WHERE 
            c2g__YearName__c = :yr.Id AND c2g__PeriodNumber__c != '000' AND 
            c2g__EndDate__c > TODAY LIMIT 1];

        c2g__codaBankAccount__c bank = IBA_TestDataFactory.createBankAccount(false, 'Trust Account USD', '123456789', 'Santander', 'uniqueCaseInsensitive24!', gla.Id, curr.Id);
        insert bank;

        Test.startTest();
        Asset policy = IBA_TestDataFactory.createPolicy(false, 'Policy 1', acc.Id);
        policy.IBA_ProducingBrokerAccount__c = acc.Id;
        policy.IBA_AccountingBroker__c = acc.Id;
        policy.Azur_Account__c = acc.Id;
        insert policy;

        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(true, 2, policy.Id);

        c2g__codaJournal__c journal = IBA_TestDataFactory.createJournal(true,
            company.Id, 
            policyTransactions[0].Id, 
            period.Id, 
            curr.Id);
        c2g__codaJournalLineItem__c journalLineItem = IBA_TestDataFactory.createJournalLineItem(true,
            1,
            0, 
            gla.Id, 
            journal.Id,
            IBA_UtilConst.JOURNAL_LINE_TYPE_AC,
            acc.Id);


        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();

        IBA_FFPolicyCarrierLine__c policyCarrierLine = IBA_TestDataFactory.createPolicyCarrierLine(false, policyTransactions[0].Id, acc.Id);
        policyCarrierLines.add(policyCarrierLine);
        insert policyCarrierLines;
    
        Test.stopTest();

    }

    @isTest
    static void afterUpdateTest() {
        List<c2g__codaJournal__c> journalList = [SELECT Id, IBA_PolicyTransaction__c, c2g__JournalStatus__c FROM c2g__codaJournal__c];

        Set<Id> journalIds = new Set<Id>();
        for(c2g__codaJournal__c journal :journalList) {
            journalIds.add(journal.Id);
        }
        
        Test.startTest();
        IBA_TestDataFactory.postJournals(journalIds);
        Test.stopTest();
        IBA_PolicyTransaction__c carrierMGAPT = new IBA_PolicyTransaction__c();
        for(IBA_PolicyTransaction__c policyTransaction :[SELECT Id, IBA_OwedtoCarrier__c, IBA_OwedtoMGA__c, IBA_MGAPayableInvoice__c FROM IBA_PolicyTransaction__c]) {
            if(policyTransaction.IBA_OwedtoMGA__c != null && policyTransaction.IBA_OwedtoCarrier__c != null) {
                carrierMGAPT = policyTransaction;
            }
        }
        
        System.assertEquals(0, carrierMGAPT.IBA_OwedtoMGA__c);
        System.assertEquals(0, carrierMGAPT.IBA_OwedtoCarrier__c);
        
    }
}