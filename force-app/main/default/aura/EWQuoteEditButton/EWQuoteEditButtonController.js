({
    doInit: function (component, event, helper) {
        const recordId = component.get('v.recordId');
        let action = component.get("c.loadQuoteSetupData");
        action.setParam('quoteId', recordId);

        action.setCallback(this, function (response) {
            const responseJson = response.getReturnValue();
            let redirectToRecord = false;

            if (responseJson && responseJson.quoteStatus) {
                component.set('v.quoteStatus', responseJson.quoteStatus);
            }

            if (responseJson.isCommunity) {
                
                let urlEvent = $A.get("e.force:navigateToURL");
                
                let urlPath; 
                switch(responseJson.quoteTransactionType) {
                    case 'MTA':
                        urlPath = '/policy-mta?recordId=' + recordId;
                        break;
                    case 'Cancellation':
                        urlPath = '/policy-cancellation?recordId=' + recordId;
                        break;
                    default:
                        urlPath = '/quote-edit?recordId=' + recordId;
                }
                
                urlEvent.setParams({
                    'url': urlPath
                });
                urlEvent.fire();

            } else {
                redirectToRecord = true;
            }

            if (redirectToRecord) {
                var sObjectEvent = $A.get("e.force:navigateToSObject");
                sObjectEvent.setParams({
                    "recordId": recordId,
                });
                sObjectEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
});