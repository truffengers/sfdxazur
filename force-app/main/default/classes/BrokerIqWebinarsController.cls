public class BrokerIqWebinarsController extends PortalController {
    
    public BrightTALK__Webcast__c featuredWebcast {get; set;}
    public String brightTalkEndpoint {get; set;}
    public IQ_Content__c featuredIqContent {get; set;}
    
    public List<BrightTALK__Webcast__c> upcomingWebcasts {get; set;}
    public List<BrightTALK__Webcast__c> recentWebcasts {get; set;}
    
    public List<String> upcomingWebcastsJson {get; set;}
    public List<String> recentWebcastsJson {get; set;}
    
    public Boolean hasPassedTest {get; set;}
    
    public BrokerIqWebinarsController() {
        BrightTalkApi api = new BrightTalkApi();
        BrightTALK_API_Settings__c settings = api.getSettings();
        Decimal channelId = settings.Channel_Id__c;
        brightTalkEndpoint = settings.Endpoint__c;
        DateTime now = DateTime.now();
        
        List<BrightTALK__Channel__c> thisChannell = [SELECT Id, Featured_Webcast__c 
                                              FROM BrightTALK__Channel__c
                                              WHERE 
                                              BrightTALK__Channel_Id__c = :channelId];

        if(thisChannell.isEmpty()) {
          return;
        }

        BrightTALK__Channel__c thisChannel = thisChannell[0];
        Id thisChannelId = thisChannel.Id;
        try {
            Map<String, String> params = ApexPages.currentPage().getParameters();
            String whereClause;
            String idParam = params.get('id');
            DateTime today = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
            DateTime tomorrow = today.addDays(1);
            String titleParam = params.get('title');
            if(titleParam != null) {
                whereClause = 'BrightTALK__Name__c = :titleParam AND BrightTALK__Start_Date__c >= :today AND BrightTALK__Start_Date__c < :tomorrow';
            } else {
                whereClause = 'Id = :idParam';
            }
            featuredWebcast = (BrightTALK__Webcast__c)Database.query('SELECT Id, BrightTALK__Webcast_Id__c, BrightTALK__Name__c, '
                               + 'BrightTALK__Presenter__c, BrightTALK__Start_Date__c, '
                               + 'BrightTALK__Description__c, Duration_s__c '
                               + 'FROM BrightTALK__Webcast__c '
                               + 'WHERE ' + whereClause);
        } catch(Exception e) {
            // In case the id param is not valid, just use the featured one
            featuredWebcast = [SELECT Id, BrightTALK__Webcast_Id__c, BrightTALK__Name__c,
                               BrightTALK__Presenter__c, BrightTALK__Start_Date__c,
                               BrightTALK__Description__c, Duration_s__c
                               FROM BrightTALK__Webcast__c
                               WHERE Id = :thisChannel.Featured_Webcast__c];           
        }
        upcomingWebcasts = Database.query(BrokerIqWebinarPreviewController.getQuery(
            'BrightTALK__Channel__c = :thisChannelId '
            + 'AND BrightTALK__Start_Date__c > :now '
            + 'AND Status__c != null AND IQ_Content__c != null '
            + 'ORDER BY BrightTALK__Start_Date__c ASC '
            + 'LIMIT 3'));
        
        upcomingWebcastsJson = BrokerIqWebinarPreviewController.webcastsToListOfJson(upcomingWebcasts);
        
        recentWebcasts = Database.query(BrokerIqWebinarPreviewController.getQuery(
            'BrightTALK__Channel__c = :thisChannelId '
            + 'AND BrightTALK__Start_Date__c < :now '
            + 'AND Status__c != null AND IQ_Content__c != null '
            + 'ORDER BY BrightTALK__Start_Date__c DESC '
            + 'LIMIT 3'));
        
        recentWebcastsJson = BrokerIqWebinarPreviewController.webcastsToListOfJson(recentWebcasts);
        
        List<CPD_Test__c> aTest = [SELECT Id, IQ_Content__c
                                   FROM CPD_Test__c
                                   WHERE Status__c = 'Published'
                                   AND IQ_Content__r.BrightTALK_Webcast__c = :featuredWebcast.Id
                                   AND IQ_Content__r.BrightTALK_Webcast__r.BrightTALK__Start_Date__c < :now];
        
        if(!aTest.isEmpty()) {
            featuredIqContent = [SELECT Id, Category__r.Colour__c, Category__r.Icon__c, Category__r.Name
                                 FROM IQ_Content__c
                                 WHERE Id = :aTest[0].IQ_Content__c];
            
            hasPassedTest = !([SELECT Id 
                               FROM CPD_Assessment__c 
                               WHERE CPD_Test__c = :aTest[0].Id 
                               AND Contact__c = :(thisUserAsContact == null ? null : thisUserAsContact.Id)
                               AND Passed__c = true].isEmpty());
        }
    }
    
    public PageReference maybeGotoLogin() {
        if(UserInfo.getUserType() == 'Guest') {
            PageReference rval = Page.BrokerIqLogin;
            rval.getParameters().put('startURL', ApexPages.currentPage().getUrl());
            return rval;
        } else {
            return null;
        }
    }
}