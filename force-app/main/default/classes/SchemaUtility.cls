/**
* @name: SchemaUtility
* @description: Utility class to retrieve useful schema information
* @author: Antigoni Tsouri atsouri@azuruw.com
* @lastChangedBy: Antigoni Tsouri 18/04/2017
* 
* 
*/
public with sharing class SchemaUtility 
{
	public static Id getRecordTypeIdByName(SObjectType objectName, String recordTypeName)
	{
		return objectName.getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
	}
}