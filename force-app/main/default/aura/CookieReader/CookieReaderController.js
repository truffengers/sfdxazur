/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 14/10/2017
 */
({
    readCookie : function(component, event, helper) {
		var params = event.getParam('arguments');
		if(!$A.util.isEmpty(params) && !$A.util.isEmpty(params.key) && typeof params.result !== 'undefined') {
		    var re = new RegExp('(?:(?:^|.*;\\s*)' + params.key + '\\s*\\=\\s*([^;]*).*$)|^.*$');
		    params.result.value = document.cookie.replace(re, "$1");
		}
    }
})