/**
* @name: BrokerIqCpdTestProviderController
* @description: test BrokerIqContentController
* @author: Nebula Consulting 
* @date: 14/10/2017
* @Modified: Ignacio Sarmiento	19/12/2017	fixing bugs
*/
public with sharing class BrokerIqCpdTestProviderController {
	
	@AuraEnabled
	public static CPD_Test__c getCpdTest(String recordId) {
		Set<String> cpdTestField = new Set<String>();
		
		List<Cpd_Test__c> results = [
			SELECT Id, Name, Pass_Score__c, Number_of_Questions__c, IQ_Content__r.CPD_Hours_In_Words__c,
                   IQ_Content__c,
                   (SELECT Question__c, Option_1__c, Option_2__c, Option_3__c, Option_4__c, Number_of_Options_to_Choose__c 
                    FROM Test_Questions__r 
                    ORDER BY Question_Order__c)
            FROM CPD_Test__c
            WHERE Id = :recordId
		];

		CPD_Test__c rval = results.isEmpty() ? null : results[0];

		if(rval != null) {
			rval.IQ_Content__r = BrokerIqContentController.getIqContentInformation(rval.IQ_Content__c).iqContent;
		}

		return rval;
	}
}