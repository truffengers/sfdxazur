/*
* @name: BrokerIqSharingControllerTest
* @description: This is the test class for apex class BrokerIqSharingController
* @author: Rajni Bajpai
* @date: 11/May/2021

************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1             Rajni Bajpai         11 May 2021         EW-Download IQcontent PDF in Community
************************************************************************************************
*/
@isTest

public class BrokerIqSharingControllerTest {
    
    //Object name constants
    private static final String USER_SYSTEMADMIN_LASTNAME = 'Testing last name system admin'; 
    private static final String CATEGORY_NAME = 'Test class compliance'; 
    private static final String CONTRIBUTOR_NAME = 'Test class compliance'; 
    private static final String BT_CHANNEL_NAME = 'test class Azur underwriting'; 
    //Variables
    private static User systemAdmin;
    private static Category__c category;
    private static Broker_iQ_Contributor__c biqContributor;
    
    @testSetup
    public static void setup() {
        //Insert system administrator
        systemAdmin = BrokerIqTestDataFactory.createSystemAdmistratorUser(USER_SYSTEMADMIN_LASTNAME);
        insert systemAdmin;
        
        System.runAs(systemAdmin) {
            //Insert category
            category = BrokerIqTestDataFactory.createCategory(CATEGORY_NAME);
            insert category;
            
            //Insert biq contributor
            Account account = BrokerIqTestDataFactory.createBusinessAccount('Test class business account');
            insert account;
            Contact contact = BrokerIqTestDataFactory.createBroker(account.Id, 'Test class contact');
            insert contact;
            biqContributor = BrokerIqTestDataFactory.createBrokerIQContributor(CONTRIBUTOR_NAME, contact.Id);
            insert biqContributor;

        } 
    }
    private static User getSystemAdmin(){
        return [SELECT Id FROM User WHERE LastName =: USER_SYSTEMADMIN_LASTNAME AND isActive = true LIMIT 1];
    }
    
    private static void getSetupRecords(){
        category = [SELECT Id FROM Category__c WHERE Name =: CATEGORY_NAME  LIMIT 1];
        biqContributor = [SELECT Id FROM Broker_iQ_Contributor__c WHERE Name =: CONTRIBUTOR_NAME  LIMIT 1];
    }
    
    testMethod static void testgetSharingLinkFor(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            //Initialize variables
            getSetupRecords();            
            //Insert webinar 
            IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar('Test class webinar', category.Id, biqContributor.Id, true);
            insert webinar;            
            //Test 
            Map<String,String> upcomingWebinarsResult = BrokerIqSharingController.getSharingLinkFor(webinar.id);
            Test.stopTest();            
            //Assess results
            System.assertEquals(true, upcomingWebinarsResult.containsKey('Name'));
            System.assertEquals(upcomingWebinarsResult.size()>0,true);
        } 
    }
    testMethod static void testgetURL(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            //Initialize variables
            getSetupRecords();            
            //Insert webinar 
            IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar('Test class webinar', category.Id, biqContributor.Id, true);
            insert webinar;            
            //Test 
            String upcomingWebinarsResult = BrokerIqSharingController.getURL(webinar.id);
            Test.stopTest();            
            //Assess results
            System.assertEquals(String.isNotBlank(upcomingWebinarsResult),true);
        } 
    }
    testMethod static void testVFPageDoc(){
        System.runAs(getSystemAdmin()) {
            Test.startTest();
            //Initialize variables
            getSetupRecords();            
            //Insert webinar 
            IQ_Content__c webinar = BrokerIqTestDataFactory.createWebinar('Test class webinar', category.Id, biqContributor.Id, true);
            insert webinar;            
            //Test 
            PageReference pageRef = Page.BrokerIqDownloadArticle;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('recordId',webinar.Id);            
            BrokerIqSharingController controller = new BrokerIqSharingController();            
            Test.stopTest();            
            //Assess results
            System.assertEquals(webinar!=null,true);
        } 
    }

}