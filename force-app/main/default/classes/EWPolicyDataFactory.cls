/**
* @name: EWPolicyDataFactory
* @description: Reusable factory methods for policy object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 05/09/2018
*/
@isTest
public class EWPolicyDataFactory {

    public static Asset createPolicy(String name,
                                     Id oppId,
                                     Id brokerId,
                                     Id insuredId,
                                     Boolean insertRecord) {

        Id policyRecId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get(EWConstants.POLICY_EW_RECORD_TYPE).getRecordTypeId();

        Asset policy = new Asset();
        policy.name = (name == null) ? 'testpolicy' : name;
        policy.Opportunity__c = oppId;
        policy.RecordTypeId = policyRecId;
        policy.AssetProvidedById = brokerId;
        policy.AccountId = insuredId;
        policy.vlocity_ins__EffectiveDate__c = Date.today();
	policy.vlocity_ins__InceptionDate__c = Date.today();
        policy.vlocity_ins__ExpirationDate__c = Date.today().addYears(1);
        policy.Status = 'Issued';

        if (insertRecord) {
            insert policy;
        }

        return policy;
    }
}