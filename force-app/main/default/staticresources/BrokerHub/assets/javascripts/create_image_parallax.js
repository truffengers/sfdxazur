TT.createImageParallax = function( jQuery, _, item, options ) {

  var $viewport;
  var items = [];
  var scrollY;
  var viewportHeight;
  var scrollHeight;
  var THROTTLE_DELAY = 10;
  var isInitd;
  var scrollMax;
  var settings;
  var scrollThrottleFunction;
  var resizeThrottleFunction;

  var ON  = 'on';
  var OFF = 'off';

  var DEFAULTS = {
    viewport: window
  };



  function init() {
    if(isInitd) { return; }
    setDefaults(options);
    $viewport = $(settings.viewport);
    storeWindowData();
    initThrottleFunctions();
    turnListeners(ON);
  }

  function initThrottleFunctions() {
    scrollThrottleFunction = _.throttle(scrollHandler, THROTTLE_DELAY);
    resizeThrottleFunction = _.throttle(resizeHandler, THROTTLE_DELAY);
  }

  function turnListeners(onOrOff) {
    $viewport[onOrOff]('scroll', scrollThrottleFunction);
    $viewport[onOrOff]('resize', resizeThrottleFunction);
  }

  function getScrollY()        { return scrollY; }
  function getViewportHeight() { return viewportHeight; }
  function getScrollMax()      { return scrollMax; }

  function register($parent, options) {
    init();
    $parent.each(function() {
      var newItem = item($(this), getIsWindow(), getScrollY, getViewportHeight, getScrollMax, options);
      items.push(newItem);
      updatePosition(null, newItem);
    });
  }

  function setDefaults(options) {
    settings = $.extend({}, DEFAULTS, options || {});
  }

  function storeWindowData() {
    scrollY        = $viewport.scrollTop();
    viewportHeight = $viewport.height();
    scrollMax      = getScrollHeight() - viewportHeight;
  }

  function getIsWindow() {
    return settings.viewport === window;
  }

  function getScrollHeight() {
    var element = getIsWindow() ? document.body : settings.viewport;
    return element.scrollHeight;
  }

  function scrollHandler() {
    storeWindowData();
    $.each(items, updatePosition);
  }

  function resizeHandler() {
    storeWindowData();
    $.each(items, updateSize);
  }

  function updateSize(i, item) {
    item.updateSize();
  }

  function updatePosition(i, item) {
    item.updatePosition();
  }

  function teardown() {
    turnListeners(OFF);
    $viewport              = undefined;
    items                  = undefined;
    scrollY                = undefined;
    viewportHeight         = undefined;
    scrollHeight           = undefined;
    isInitd                = undefined;
    scrollMax              = undefined;
    settings               = undefined;
    scrollThrottleFunction = undefined;
    resizeThrottleFunction = undefined;
  }



  return {
    register : register,
    teardown : teardown
  };

};