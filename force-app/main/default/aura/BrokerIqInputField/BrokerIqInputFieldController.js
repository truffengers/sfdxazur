({
	onKeyUp : function(component, event, helper) {
        if (event.keyCode === 13) {
        	var submitEvent = component.getEvent('submit');

        	submitEvent.fire();
		}		
	},
	onChange : function(component, event, helper) {
		component.set('v.value', event.target.value);
	},
	// See c:Link for why we do this instead of just using onclick
	componentInitJs : function(component, event, helper) {
	    var editLink = component.find('editLink');
	    if(!$A.util.isEmpty(editLink)) {
            var hammertime = new Hammer(editLink.getElement(), {});
            hammertime.on('tap', $A.getCallback(function() {
                component.set('v.editMode', true);
            }));
        }
	}
})