trigger ProductRequirementTrigger on vlocity_ins__ProductRequirement__c (after insert) {
    TriggerDispatcher.Run(new ProductRequirementTriggerHandler());
}