import { LightningElement, api } from 'lwc';
import { analyticsEvent } from 'c/ix_utils';
import { CONSTANTS, COLLAPSIBLE_CLASSES } from 'c/ix_constants';

export default class Ix_DataItem extends LightningElement {
    @api itemData;
    @api displayResultingItemById;

    // Used for analytics for accordion and link interactions
    @api currentlySelectedItem;

    @api isAccordionWithSubheading;
    @api accordionSubheading;
    @api accordionValue;

    connectedCallback() {
        if (this.itemData && this.itemData._is_parent
            && this.itemData._is_accordion_item
        ) {
            this.isAccordionWithSubheading
                = this.itemData._value.filter(a => {
                return a._name === CONSTANTS.ACCORDION_SUBHEADING;
            }).length;

            if (this.isAccordionWithSubheading) {
                this.accordionSubheading
                    = this.itemData._value.filter(item => {
                    return item._name === CONSTANTS.ACCORDION_SUBHEADING;
                })[0]._value;

                this.accordionValue
                    = this.itemData._value.filter(item => {
                    return item._name === CONSTANTS.ACCORDION_VALUE;
                })[0]._value;
            }
        }
    }

    renderedCallback() {
        // Render rich text onto the page as innerHTML
        if (this.itemData._is_subheading && this.itemData._is_rich_text) {
            this.template.querySelector('.rich-text-container').innerHTML
                = this.itemData._value;
        }
    }

    /*
    Function that is executed when a link is clicked to re-populate the
    search bar.
    */
    // Because dataItem recursively renders itself, sometimes it is executed by
    // an onclick event, where the data item id is `event.target.dataset.id`,
    // and sometimes it is executed by an onlinkclick event, where the data item
    // id is `event.detail.value`.
    dispatchLinkClick(event) {
        let dispatchLinkClickEvent;
        
        if (event.target.dataset.id) {
            dispatchLinkClickEvent = new CustomEvent('linkclick', {
                detail: {
                    value: event.target.dataset.id
                }
            });
        } else if (event.detail.value) {
            dispatchLinkClickEvent = new CustomEvent('linkclick', {
                detail: {
                    value: event.detail.value
                }
            });
        }

        analyticsEvent({
            eventName: 'Link Clicked',
            eventData: this.itemData._name,
            eventDescription: `Link to "${this.itemData._name}" was clicked for
                the item "${this.currentlySelectedItem._name}"`
        })
            .catch(() => null);

        this.dispatchEvent(dispatchLinkClickEvent);
    }

    get collapsibleClass() {
        return COLLAPSIBLE_CLASSES[this.itemData._branch_depth];
    }
}