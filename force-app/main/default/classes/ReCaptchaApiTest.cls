@istest
public class ReCaptchaApiTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();

	public static void setup(Boolean success) {
		tog.getReCaptcha();

		ReCaptchaApiMock mock = new ReCaptchaApiMock(success);
		Nebula_API.NebulaAPI.setMock(mock);
	}

	private static void runTest(Boolean success) {
		setup(success);

		ReCaptchaApi api = new ReCaptchaApi();

		Test.startTest();
		System.assertEquals(success, api.verify(''));
		Test.stopTest();
	}

	@istest
	public static void success() {
		try {
				runTest(true);
			} catch(Exception e) {
				throw e.getCause();
			}
	}
	@istest
	public static void fail() {
		runTest(false);
	}
}