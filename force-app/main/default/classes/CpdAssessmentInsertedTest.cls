@istest
public class CpdAssessmentInsertedTest {

    private static TestObjectGenerator tog = new TestObjectGenerator();
    
    @istest
    public static void basic() {
        CPD_Test__c cpdTest = tog.getCpdTest();
        CPD_Test_Question__c cpdTestQuestion = tog.getCpdTestQuestion();
        CPD_Assessment__c ass = tog.getCpdAssessment();
        
        CPD_Assessment_Answer__c ans = [SELECT Id, CPD_Test_Question__c
                                              FROM CPD_Assessment_Answer__c
                                              WHERE CPD_Assessment__c = :ass.Id];
        
        System.assertEquals(cpdTestQuestion.Id, ans.CPD_Test_Question__c);
    }
    
}