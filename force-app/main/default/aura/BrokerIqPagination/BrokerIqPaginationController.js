({
	doInit : function(component, event, helper) {
	    var itemsPerPageCookieValue = {};
        component.find('cookieReader').readCookie('itemsPerPage', itemsPerPageCookieValue);
		var itemsPerPageCookie = itemsPerPageCookieValue.value;
		if(itemsPerPageCookie) {
			component.set('v.itemsPerPage', itemsPerPageCookie);
		}

		helper.updatedBounds(component);
		helper.updatedCurrent(component);
	},
	updatedBounds : function(component, event, helper) {
		helper.updatedBounds(component);
		helper.updatedCurrent(component);
	},
	updatedCurrent : function(component, event, helper) {
		helper.updatedCurrent(component);
	},
	pageBack : function(component, event, helper) {
		var currentPage = component.get('v.currentPage');
		if(currentPage > 1) {
			currentPage = currentPage - 1;
			component.set('v.currentPage', currentPage);
			helper.updatedCurrent(component);
		}
	},
	pageForward : function(component, event, helper) {
		var currentPage = component.get('v.currentPage');
		var pages = component.get('v.pages');

		if(currentPage < pages.length) {
			currentPage = currentPage + 1;
			component.set('v.currentPage', currentPage);
			helper.updatedCurrent(component);
		}
	}
})