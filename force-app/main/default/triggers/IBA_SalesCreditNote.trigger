/**
* @name: IBA_SalesCreditNote
* @description: Trigger for the managed object c2g__codaCreditNote__c
*
*/

trigger IBA_SalesCreditNote on c2g__codaCreditNote__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
    TriggerDispatcher.Run(new IBA_SalesCreditNoteTriggerHandler());
}