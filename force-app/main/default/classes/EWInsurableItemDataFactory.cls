/**
* @name: EWInsurableItemDataFactory
* @description: Reusable factory methods for insurable item object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 23/10/2018
*/
@isTest
public with sharing class EWInsurableItemDataFactory {

    public static vlocity_ins__InsurableItem__c createInsurableItem(String name,
                                                                    Id brokerAccountId,
                                                                    Id insuredAccountId,
                                                                    Id masterProductId,
                                                                    Id quoteId,
                                                                    ID recordTypeId,
                                                                    Boolean isActive,
                                                                    Boolean insertRecord) {

        vlocity_ins__InsurableItem__c insurableItem = new vlocity_ins__InsurableItem__c();
        insurableItem.Name = (name == null) ? 'testInsurableItem' : name;
        insurableItem.EW_Quote__c = quoteId;
        insurableItem.EW_Broker__c = brokerAccountId;
        insurableItem.EW_Active__c = isActive;
        insurableItem.vlocity_ins__AccountId__c = insuredAccountId;
        insurableItem.vlocity_ins__SpecProduct2Id__c = masterProductId;
        insurableItem.RecordTypeId = (recordTypeId == null)
                                        ? EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                                                                            'vlocity_ins__InsurableItem__c',
                                                                            EWConstants.INSURABLE_ITEM_RECORD_TYPE_EW_PROPERTY)
                                        : recordTypeId;

        if (insertRecord) {

            insert insurableItem;
        }

        return insurableItem;
    }
}