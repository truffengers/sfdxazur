public class EWManualStepsHelper {

    public static final Set<String> coverageNameSet = new Set<String> {
                                                                'Art and Collectables',
                                                                'Buildings',
                                                                'Contents',
                                                                'Employers Liability',
                                                                'Home Emergency',
                                                                'Jewellery and Watches',
                                                                'Legal Expenses',
                                                                'Pedal Cycles',
                                                                'Personal Cyber',
                                                                'Public Liability',
                                                                'Tenants Improvements' };

	
    public static void postCarrierAccountsAndCoverageCustomSettings() {

        //  ****************************************
        //  Load Carrier Accounts
        //  ****************************************

        Set<String> carrierNameSet = new Set<String> { 'AIG', 'ARAG', 'HSB' };

        Map<String, Account> carrierAccountByNameMap = new Map<String, Account>();

        Id carrierAccountRecordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(
                                                                          'Account',
                                                                          EWConstants.ACCOUNT_RECORD_TYPE_CARRIER);


        for (Account carrierAccount : [SELECT Id, Name
                                       FROM Account
                                       WHERE RecordTypeId = :carrierAccountRecordTypeId]) {

            carrierAccountByNameMap.put(carrierAccount.Name, carrierAccount);
        }

        for (String carrierName : carrierNameSet) {

            if (!carrierAccountByNameMap.containsKey(carrierName)) {

                Account carrierAccount = new Account(
                                                Name = carrierName,
                                                RecordTypeId = carrierAccountRecordTypeId);

                carrierAccountByNameMap.put(carrierName, carrierAccount);
            }
        }

        upsert carrierAccountByNameMap.values();

        //  ****************************************
        //  Load Custom Settings
        //  ****************************************

        List<EW_Coverages__c> coverageList = new List<EW_Coverages__c>();

        for (String coverageName : coverageNameSet) {

            switch on coverageName {

                when 'Home Emergency', 'Legal Expenses' {
                    coverageList.add(new EW_Coverages__c(
                                        Name = coverageName,
                                        CarrierId__c = carrierAccountByNameMap.get('ARAG').Id));
                }

                when 'Personal Cyber' {
                    coverageList.add(new EW_Coverages__c(
                                        Name = coverageName,
                                        CarrierId__c = carrierAccountByNameMap.get('HSB').Id));
                }

                when else {
                    coverageList.add(new EW_Coverages__c(
                                        Name = coverageName,
                                        CarrierId__c = carrierAccountByNameMap.get('AIG').Id));
                }
            }
        }

        upsert coverageList;
    }
}