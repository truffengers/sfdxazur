({
      
    startChat: function(component, event, helper){
        var fields = [
            {
                label: 'FirstName',
                name: 'FirstName',
                value: component.get('v.firstName')
            },
            {
                label: 'LastName',
                name: 'LastName',
                value: component.get('v.lastName')
            },
            {
                label: 'UserId',
                name: 'AZ_UserId__c',
                value: component.get('v.userId'),
            }
            ,
            {
                label: 'Chatbot Token',
                name: 'AZ_ChatbotToken__c',
                value: component.get('v.token'),
            }
        ];
        if(component.find("prechatAPI").validateFields(fields).valid) {
            component.find("prechatAPI").startChat(fields);
        }
    }
});