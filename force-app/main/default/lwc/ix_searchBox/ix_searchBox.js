import { LightningElement, api, track } from 'lwc';
import { snakeToSentence, levenshteinDistance, analyticsEvent,
    prepareForMarkup, processScheduleData } from 'c/ix_utils';
import { CONSTANTS, WORD_GROUPINGS } from 'c/ix_constants';
import STATIC_DATA from 'c/ix_staticData';
import { GENERAL_DATA } from 'c/ix_generalData';

export default class Ix_SearchBox extends LightningElement {
    /*
    The processed version of the wording data we can work with
    */
    @track workingScheduleData;
    /*
    The processed version of the general data we can work with (ie. General
    Conditions, and General Exclusions)
    */
    @track workingGeneralData;
    @track searchResults = [];
    @track searchBoxValue = '';
    @track currentlySelectedItem;
    @track breadcrumbs = [];

    /*
    Variables used specifically for tracking analytics
    
    Whether or not the page has either loaded for the first time, or the user
    has just pressed refresh and not done anything.
    */
    @track searchBarUnactioned = true;
    /*
    A timeout used to track when the user has been inactive for a certain length
    of time
    */
    @track searchIdleCounter = 0;
    // Whether or not the user has clicked an option from the dropdown - for
    // determining whether or not a search should be counted as "unsuccessful".
    // This is consciously separate from `currentlySelectedItem`.
    @track optionClicked;
    // Whether or not the user clicked thumbs up or thumbs down
    @track feedbackClicked;
    // Used to know when the user cleared the search box all at once
    @track previousSearchBoxValue;

    connectedCallback() {
        this.workingScheduleData = prepareForMarkup(
            processScheduleData(STATIC_DATA)
        );

        this.workingGeneralData = prepareForMarkup(
            processScheduleData(GENERAL_DATA)
        );

        window.addEventListener('beforeunload', () => {
            if (this.searchBoxValue && !this.optionClicked) {
                const eventDescription = this.searchResults.length
                    ? `The user searched "${this.searchBoxValue}", which
                    returned results, but the user terminated the session
                    without clicking any of them`
                    : `The user searched "${this.searchBoxValue}", which
                    returned no results. The user then terminated the session`;

                analyticsEvent({
                    eventName: 'Unsuccessful Search',
                    eventData: this.searchBoxValue,
                    eventDescription
                })
                    .catch(() => null);
            }
        })

        /*
        Tracking the period of time during which the user has been inactive, for
        the purposes of knowing how long users have left a search idle for, for
        analytics purposes
        */
        window.setInterval(() => { this.searchIdleCounter++; }, 1000);
    }

    @track previousScrollPosition;

    /*
    ⚠️ HACK: Because sometimes when you open an accordion the page would
    randomly scroll, `accordionOpen` exists in localStorage, meaning an
    accordion is open, restore the scroll to the previous scroll position.
    `accordionOpen` is set in `ix-accordionItem`.
    */
    handleScroll(event) {
        if (window.localStorage.getItem('accordionOpen')) {
            event.target.scrollTop = this.previousScrollPosition;
            window.localStorage.removeItem('accordionOpen');
        } else if (window.localStorage.getItem('accordionClosed')) {
            event.target.scrollTop = this.previousScrollPosition;
            window.localStorage.removeItem('accordionClosed');
        }
        this.previousScrollPosition = event.target.scrollTop;
    }

    performSearch(paramQuery) {
        /*
        Recursively search through the names of data items and return an array
        of the relevant items.

        ⬇️ Hardcoding this in for now. No meaningful things would be searched by
        using only 2 letters. Ideally, there should be some sort of systematic
        validation and sanitisation for the search query input performing many
        checks in a separate function eventually.
        */
        if (paramQuery.length < 3) { return []; }

        // An array of strings that return the parent when searched. For
        // example, when you search "Sum Insured", return every item that has
        // a sum insured.
        const searchableByParent = [
            CONSTANTS.SUM_INSURED,
            CONSTANTS.LIMITS_TO_SUM_INSURED,
            CONSTANTS.CLAUSES,
            CONSTANTS.YOUR_COVER
        ].map(a => snakeToSentence(a).toLowerCase());

        const results = [];
        const searchWithinArray = (query, array) => {
            array.forEach(item => {
                if (Array.isArray(item._value)) {
                    // Ensure that the query is included as a substring in at
                    // least one of the items in the array searchableByParent
                    const childSearchableByParent = searchableByParent
                        .filter(a => a.includes(query)).length;

                    // Ensure that the lower case search query is a substring
                    // of the lower case name of the child
                    const childNameIncludesQuery = child => {
                        return child._name.toLowerCase().includes(
                            query.toLowerCase()
                        );
                    };

                    // Both childSearchableByParent and childNameIncludesQuery
                    // must be truthy in order to return the parent in question
                    // as a search result. Ensure the search results do not
                    // contain duplicates.
                    const returnParentAsSearchResult = item._value
                        .filter(child => {
                            return childSearchableByParent
                                && childNameIncludesQuery(child);
                        }).length

                    if (returnParentAsSearchResult
                        && !results.filter(a => {
                            return a._id === item._id;
                        }).length
                    ) {
                        results.push(item);
                    }
                    searchWithinArray(query, item._value);
                }

                /*
                Make case insensitive. Ensure the search results do not contain
                duplicates.
                */
                const queryInItemName
                    = item._name.toLowerCase().includes(query.toLowerCase());
                const itemNameInQuery
                    = query.toLowerCase().includes(item._name.toLowerCase());
                const isFuzzyMatch = this.tryFuzzyMatch(item._name, query);
                const isSynonym = this.findWordAssociation(item._name, query);
                
                if ((queryInItemName
                    || itemNameInQuery
                    || isFuzzyMatch
                    || isSynonym)
                    && !results.filter(a => {
                        return a._id === item._id;
                    }).length
                ) {
                    results.push(item);
                }
            });
        }

        searchWithinArray(paramQuery, this.workingScheduleData);
        return results
            .filter(item => !item._unsearchable)
            .map(item => {
                return {
                    ...item,
                    // Mini breadcrumbs that display within a search result
                    miniBreadcrumbs: this.findBreadcrumbsById(item._id)
                }
            })
            .sort();
    }

    tryFuzzyMatch(itemName, query) {
        /*
        TODO Fuzzy search logic sits down here. Will need to be reconsidered at
        some point, since logic needs to be finalised. Maybe have a "Do you
        mean..." option that you have to click, rather than just give the result
        in a dropdown straight up?
        */
        return itemName.toLowerCase().split(' ').filter(nameFragment => {
            if (!query || query.length <= 4) {
                return;
            } else if (query.length <= 5) {
                return levenshteinDistance(
                    nameFragment, query) <= 1;
            } else if (query.length <= 7) {
                return levenshteinDistance(
                    nameFragment, query) <= 2;
            }
            return levenshteinDistance(
                nameFragment, query) <= 3;
        }).length;
    }

    /*
    Find if there are any associated words for that query. Associated word
    groupings are hardcoded in the constants file.
    */
    findWordAssociation(itemName, query) {
        return query.split(' ').filter(queryFragment => {
            return WORD_GROUPINGS[itemName.toLowerCase()]
                && (WORD_GROUPINGS[itemName.toLowerCase()]
                    .includes(queryFragment.toLowerCase())
                || WORD_GROUPINGS[itemName.toLowerCase()].filter(synonym => {
                    return query.toLowerCase().includes(synonym); 
                }).length);
        }).length;
    }

    findItemById(id) {
        /*
        Recursively search through the data and return the first (and only)
        item with the matching id.
        */
        let numId;

        /*
        Ensure the id is a number
        */
        try { numId = parseInt(id, 10); }
        catch(error) { return null; }

        let result = {}; 
        const iterateOverArray = array => {
            return array.forEach(item => {
                const isParent = Array.isArray(item._value);
                if (item._id === numId) {
                    result = item;
                } else if (isParent) {
                    iterateOverArray(item._value);
                }
                return null;
            })
        }
        iterateOverArray(this.workingScheduleData);
        return result;
    }

    @api
    findItemByName(name, data) {
        /*
        Recursively search through the data and return the first (and not
        necessarily only) item where item._name is equal to name. `data` must
        be an array of objects.
        */
        let result = {};
        const iterateOverArray = array => {
            return array.forEach(item => {
                const isParent = Array.isArray(item._value);
                if (item._name === name) {
                    result = item;
                } else if (isParent) {
                    iterateOverArray(item._value);
                }
                return null;
            })
        }
        iterateOverArray(data);
        return result;
    }

    findParentForItemById(id) {
        /*
        Given the ID for an item, return the item it is a child of. If it does
        not have a parent, return null.
        */
        let numId;
        try { numId = parseInt(id, 10); }
        catch(error) { return null; }

        let parentItem;
        const recurseOverArray = array => {
            array.forEach((item) => {
                if (item._is_parent)
                    if (item._value.find(a => a._id === numId)) {
                        parentItem = item;
                    } else {
                        recurseOverArray(item._value);
                    }
                });
        }

        recurseOverArray(this.workingScheduleData);
        return parentItem ? parentItem : null;
    }

    findBreadcrumbsById(id) {
        /*
        Returns an array of items used for displaying breadcrumbs. Essentially
        an array of the names of the parent, then the parent of the parent, etc.
        */
        let breadcrumbs = [];
        let findingParent = true;
        let currentItem = this.findParentForItemById(id);
        while (findingParent) {
            if (!currentItem) {
                findingParent = false;
            } else {
                breadcrumbs.push(currentItem);
                currentItem = this.findParentForItemById(currentItem._id);
            }
        }

        breadcrumbs = breadcrumbs.reverse();

        if (this.currentlySelectedItem) {
            breadcrumbs.push(this.currentlySelectedItem);
        }

        return breadcrumbs;
    }

    breadcrumbClick(event) {
        const itemName = this.findItemById(event.detail.value)._name;
        analyticsEvent({
            eventName: 'Breadcrumb Clicked',
            eventData: itemName,
            eventDescription: `"${itemName}" breadcrumb clicked`
        })
            .catch(() => null);

        this.displayResultingItemById(event.detail.value);
    }

    handleBackBarClick() {
        const currentItemName = this.currentlySelectedItem._name;
        const targetItem = this.breadcrumbs[this.breadcrumbs.length - 2];

        const targetItemId = targetItem._id;
        const targetItemName = targetItem._name;

        analyticsEvent({
            eventName: 'Back Bar Clicked',
            eventData: targetItemName,
            eventDescription: `Back Bar clicked while browsing
                "${currentItemName}", taking the user to "${targetItemName}".`
        })
            .catch(() => null);

        this.displayResultingItemById(targetItemId);
    }

    get displayBackBar() { return this.breadcrumbs.length - 1 };

    searchbarChange() {
        const searchbarChangeEvent = new CustomEvent('searchbarchange');
        this.dispatchEvent(searchbarChangeEvent);
    }

    handleSearchInputChange(input) {
        /*
        Executed each time when a new letter is typed or deleted in the search
        bar, or if the user clicks the button for 'Back to search results'.
        */
        if (this.searchBarUnactioned) {
            this.searchBarUnactioned = false;
        }
        if (this.currentlySelectedItem) {
            this.currentlySelectedItem = null;
        }

        let searchQuery;
        if (input && input.detail && input.detail.value) {
            // The event is triggered when the content of the search input is
            // changed
            searchQuery = input.detail.value;
        } else if (typeof input === String) {
            // The event is triggered by the user clicking 'Back to search
            // results'
            searchQuery = input;
        } else {
            searchQuery = '';
        }

        // Arbitrarily, after 3 seconds of idleness followed by tweaking the
        // search term, the event will treated as an "indecisive search"
        if (this.searchIdleCounter > 2 && !this.optionClicked
            && this.searchBoxValue) {
            const eventDescription = this.searchResults.length
                ? `"${this.searchBoxValue}" was searched and returned results.
                The user was then idle for ${this.searchIdleCounter} seconds
                before tweaking the search query.`
                : `"${this.searchBoxValue}" was searched and returned no
                results. The user was then idle for ${this.searchIdleCounter}
                seconds before tweaking the search query.`

            analyticsEvent({
                eventName: 'Indecisive Search',
                eventData: this.searchBoxValue,
                eventDescription
            })
                .catch(() => null);
        }

        this.optionClicked = false;

        this.searchIdleCounter = 0;
        this.previousSearchBoxValue = this.searchBoxValue;

        // To check if the user typed at least 2 letters, then cleared the
        // search box all at once. If so, it is counted as an unsuccessful
        // search.
        if (this.searchBoxValue.length > 1
            && !searchQuery
            && !this.optionClicked) {
            const eventDescription = this.searchResults.length
                ? `"${this.searchBoxValue}" was searched and returned
                results. The user then cleared the search bar.`
                : `"${this.searchBoxValue}" was searched and returned no
                results. The user then cleared the search bar.`

            analyticsEvent({
                eventName: 'Unsuccessful Search',
                eventData: this.searchBoxValue,
                eventDescription
            })
                .catch(() => null);
        }

        this.searchbarChange();
        this.searchBoxValue = searchQuery;
        this.searchResults = this.performSearch(searchQuery);
    }

    handleOptionClick(event) {
        /*
        Executed when the user chooses an option from the dropdown menu
        */
        const itemName = this.findItemById(event.detail.value)._name;
        this.optionClicked = true;
        /*
        There are two analytics events here, which are fired simultaneously.
        This is because we want to track both the "Successful Search" and the
        "Dropdown Click" as two separate analytics events.
        */
        analyticsEvent({
            eventName: 'Successful Search',
            eventData: this.searchBoxValue,
            eventDescription: `The user searched "${this.searchBoxValue}" and
                clicked on "${itemName}" from the dropdown.`
        })
            .catch(() => null);

        analyticsEvent({
            eventName: 'Dropdown Item Selected',
            eventData: itemName,
            eventDescription: `"${itemName}" selected after searching
                "${this.searchBoxValue}"`
        })
            .catch(() => null);

        this.displayResultingItemById(event.detail.value);
    }

    displayResultingItemById(id) {
        const itemInData = this.findItemById(id);

        /*
        `currentlySelectedItem` is the item that is currently displayed
        */
        this.currentlySelectedItem = itemInData;
        this.breadcrumbs = this.findBreadcrumbsById(id);
        this.searchResults = [];
        this.forceRerender();
    }

    handleRefresh() {
        analyticsEvent({
            eventName: 'Search Bar Refreshed'
        })
            .catch(() => null);

        if (this.searchBoxValue && !this.optionClicked) {
            const eventDescription = this.searchResults.length
                ? `The user searched "${this.searchBoxValue}", which returned
                results, but the user refreshed the search without clicking
                any.`
                : `The user searched "${this.searchBoxValue}", which returned
                no results. The user then refreshed the search.`;

            analyticsEvent({
                eventName: 'Unsuccessful Search',
                eventData: this.searchBoxValue,
                eventDescription
            })
                .catch(() => null);
        }

        this.searchResults = [];
        this.searchBoxValue = '';
        this.currentlySelectedItem = null;
        this.searchBarUnactioned = true;
        this.feedbackClicked = false;
        this.breadcrumbs = [];
    }

    handleFeedbackClicked() {
        this.feedbackClicked = true;
    }

    handleBackToSearchClick() {
        this.handleSearchInputChange({
            detail: {
                value: this.searchBoxValue
            }
        });

        analyticsEvent({
            eventName: '"Back to Search" Clicked',
            eventDescription: `The user clicked back to the search results for
                "${this.searchBoxValue}"`
        })
            .catch(() => null);
    }

    forceRerender() {
        /*
        Completely disgusting - forcibly rerender due to LWC not rerendering on
        deep changes in objects 🙄
        */
        this.workingScheduleData = this.workingScheduleData;
        this.currentlySelectedItem = this.currentlySelectedItem;
    }
}