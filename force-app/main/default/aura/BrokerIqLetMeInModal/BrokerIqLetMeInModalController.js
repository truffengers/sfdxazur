({
	doInit : function(component, event, helper) {
		window.addEventListener(
            "message", 
            function(messageEvent){
				if (messageEvent.origin !== "https://go.pardot.com") {
					return;
				}
				if(messageEvent.data.visitorId) {
                    helper.receivedVisitorIdCallback(component, messageEvent.data.visitorId);
		    	} else if(messageEvent.data.contentSignUpComplete) {
	    			setTimeout(function() {TT.letMeInModal.hide()}, 2000);
		    	}
			},
			false
		);
	},
	componentInitJs : function(component, event, helper) {
		var userType = component.get('v.userType');
		var cookieValueResult = {};
		component.find('cookieReader').readCookie('letMeInComplete', cookieValueResult);
		var cookieValue = cookieValueResult.value;

		if(cookieValue !== 'true' && userType === 'Guest') {
			TT.letMeInModal.init({
	          nagDelaySeconds : 3,
	          src             : '//go.pardot.com/l/194892/2017-06-20/5z7kz'
	        });
        } else {
            document.cookie = 'letMeInComplete=true';
        }
	},
	needsNagChanged : function(component, event, helper) {
		if(component.get('v.needsNag') && !component.get('v.showingNag')) {
    		component.set('v.showingNag', true);
      		TT.letMeInModal.startTimer();
		}
	}
})