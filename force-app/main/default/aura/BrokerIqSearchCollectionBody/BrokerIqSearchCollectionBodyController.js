({
	doInit : function(component, event, helper) {
		var searchStringLongEnough = component.get('v.searchString').length >= 2;
		component.set('v.searchStringLongEnough', searchStringLongEnough);
	}
})