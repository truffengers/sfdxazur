@isTest
private class BrokerIqMyViewingHistoryControllerTest {
	
    private static TestObjectGenerator tog = new TestObjectGenerator();
    private static BrightTALK_API_Settings__c settings;
    
    static {
        settings = tog.getBrightTalkApiSettings();
    }

	@isTest static void noData() {
        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

		tog.getUserContact();
		PageReference pageRef = Page.BrokerIqMyViewingHistory;
		Test.setCurrentPage(pageRef);


        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
		BrokerIqMyViewingHistoryController controller = new BrokerIqMyViewingHistoryController();
		Test.stopTest();
	}

	@isTest static void withWebcastInSf() {
        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

		Contact thisUserContact = tog.getUserContact();

		mock.responseParams.put('realm_user_id', thisUserContact.Id);
		mock.returnEmpty = true;
		Category__c category = tog.getCategory();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
		IQ_Content__c iqc = tog.getWebcastIqContent();
		iqc.BrightTALK_Webcast__c = webcast.Id;
		update iqc;

		BrightTALK__Webcast_Activity__c sfActivity = tog.getWebcastActivity(
            (DateTime)JSON.deserialize('"' + mock.responseParams.get('last_updated') + '"', Datetime.class), 
            webcast);

		PageReference pageRef = Page.BrokerIqMyViewingHistory;
		Test.setCurrentPage(pageRef);


        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
		BrokerIqMyViewingHistoryController controller = new BrokerIqMyViewingHistoryController();
		Test.stopTest();

		System.assertEquals(1, controller.categoryData.size());
		System.assertEquals(category.Id, controller.categoryData[0].theCategory.Id);
		System.assertEquals(1, controller.categoryData[0].historyPairs.size());
		System.assertEquals(webcast.Id, controller.categoryData[0].historyPairs[0].webcast.Id);
	}

	@isTest static void withWebcast() {
        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

		Contact thisUserContact = tog.getUserContact();
		mock.responseParams.put('realm_user_id', thisUserContact.Id);
		Category__c category = tog.getCategory();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
		
		PageReference pageRef = Page.BrokerIqMyViewingHistory;
		Test.setCurrentPage(pageRef);


        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
		BrokerIqMyViewingHistoryController controller = new BrokerIqMyViewingHistoryController();
		Test.stopTest();

		System.assertEquals(1, controller.categoryData.size());
		System.assertEquals(category.Id, controller.categoryData[0].theCategory.Id);
		System.assertEquals(0, controller.categoryData[0].historyPairs.size());
	}

	@isTest static void withWebcastPagination() {
        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

		Contact thisUserContact = tog.getUserContact();
		mock.responseParams.put('realm_user_id', thisUserContact.Id);
		mock.includeNextLink = true;
		
		Category__c category = tog.getCategory();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
		
		PageReference pageRef = Page.BrokerIqMyViewingHistory;
		Test.setCurrentPage(pageRef);


        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
		BrokerIqMyViewingHistoryController controller = new BrokerIqMyViewingHistoryController();
		Test.stopTest();

		System.assertEquals(1, controller.categoryData.size());
		System.assertEquals(category.Id, controller.categoryData[0].theCategory.Id);
		System.assertEquals(0, controller.categoryData[0].historyPairs.size());
	}

	@isTest static void withIqContent() {
        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

		Contact thisUserContact = tog.getUserContact();
		mock.responseParams.put('realm_user_id', thisUserContact.Id);
		Category__c category = tog.getCategory();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
        webcast.BrightTALK__Start_Date__c = DateTime.now().addDays(-1);
        update webcast;
		IQ_Content__c iqContent = tog.getWebcastIqContent();
		iqContent.BrightTALK_Webcast__c = webcast.Id;
		update iqContent;
		
		PageReference pageRef = Page.BrokerIqMyViewingHistory;
		Test.setCurrentPage(pageRef);


        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
		BrokerIqMyViewingHistoryController controller = new BrokerIqMyViewingHistoryController();
		Test.stopTest();

		System.assertEquals(1, controller.categoryData.size());
		System.assertEquals(category.Id, controller.categoryData[0].theCategory.Id);
		System.assertEquals(1, controller.categoryData[0].historyPairs.size());
		System.assertEquals(webcast.Id, controller.categoryData[0].historyPairs[0].webcast.Id);
		System.assertEquals(null, controller.categoryData[0].historyPairs[0].assessment);
	}

	@isTest static void withAssessment() {
        BrightTalkApiMock mock = new BrightTalkApiMock(settings, 200);

		Contact thisUserContact = tog.getUserContact();
		CPD_Test_Question__c q = tog.getCpdTestQuestion();
		CPD_Assessment__c ass = tog.getCpdAssessment();
		CPD_Assessment_Answer__c ans = [SELECT Id FROM CPD_Assessment_Answer__c WHERE CPD_Assessment__c = :ass.Id];
		ans.Answer__c = q.Answer__c;
		update ans;
		ass.Status__c = 'Submitted';
		update ass;
		
		mock.responseParams.put('realm_user_id', thisUserContact.Id);
		Category__c category = tog.getCategory();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(Decimal.valueOf(mock.responseParams.get('webcast_id')));
        webcast.BrightTALK__Start_Date__c = DateTime.now().addDays(-1);
        update webcast;
		IQ_Content__c iqContent = tog.getWebcastIqContent();
		iqContent.BrightTALK_Webcast__c = webcast.Id;
		update iqContent;
		
		PageReference pageRef = Page.BrokerIqMyViewingHistory;
		Test.setCurrentPage(pageRef);


        Nebula_API.NebulaApi.setMock(mock);
        Test.startTest();
		BrokerIqMyViewingHistoryController controller = new BrokerIqMyViewingHistoryController();
		Test.stopTest();

		System.assertEquals(1, controller.categoryData.size());
		System.assertEquals(category.Id, controller.categoryData[0].theCategory.Id);
		System.assertEquals(iqContent.CPD_Time_minutes__c, controller.categoryData[0].cpdCompletedInMinutes);
		System.assertEquals(1, controller.categoryData[0].historyPairs.size());
		System.assertEquals(webcast.Id, controller.categoryData[0].historyPairs[0].webcast.Id);
		System.assertEquals(ass.Id, controller.categoryData[0].historyPairs[0].assessment.Id);
	}

}