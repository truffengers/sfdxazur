/**
* @name: QuoteTriggerHandler
* @description: This class is handling and dispatching the relevant quote trigger functionality
* @author: Roy Lloyd, rlloyd@azuruw.com
* @date: 07/02/2019
* v2: Aleksejs Jedamenko ajedamenko@azuruw.com 28/08/2020 - IsDisabled logic added
*
*/

public with sharing class QuoteTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled()
    {
        return QuoteTriggerHandler.IsDisabled != null ? QuoteTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
        if (IsDisabled() != true) {
           new EWQuoteHelper().copyAdjustmentPercentForMTA(newItems);
	}
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems)
    {
        if (IsDisabled() != true) {
            new EWQuoteHelper().quotePriceAjustment(newItems, oldItems);
        }
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {
        if (IsDisabled() != true) {
            AuditLogUtil.createAuditLog('Quote', newItems.values(), null );
        }
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {

        if (IsDisabled() != true) {
            new EWQuoteHelper().updateQLIunitPrice(newItems, oldItems);
            new EWUnderwriterRulesHelper().updateUWrules(newItems, oldItems);
            AuditLogUtil.createAuditLog('Quote', newItems.values(), oldItems );
        }

    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}

}