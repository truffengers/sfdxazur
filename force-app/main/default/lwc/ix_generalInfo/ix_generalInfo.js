import { LightningElement, api } from 'lwc';

export default class Ix_GeneralInfo extends LightningElement {
    @api itemData;

    displayResultingItem(event) {
        this.dispatchEvent(new CustomEvent('displayresultingitem', {
            detail: { value: event.detail.value }
        }))
    }
}