/**
* @name: AccountTrigger
* @description: Trigger for the standard object Account
* @author: Antigoni D'Mello atsouri@azuruw.com
* @date: 03/11/2018
*
*/

trigger AccountTrigger on Account (before insert, before update, before delete,
                                    after insert, after update, after delete, after undelete)
{
    TriggerDispatcher.Run(new AccountTriggerHandler());
}