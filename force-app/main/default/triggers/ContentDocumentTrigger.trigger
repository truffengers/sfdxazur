trigger ContentDocumentTrigger on ContentDocument (before delete, after insert, after update) {
    TriggerDispatcher.Run(new ContentDocumentTriggerHandler());
}