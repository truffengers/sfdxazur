/**
* @name: EWEmailHelperTest
* @description: Test class for EWEmailHelper
* @author: Steve Loftus sloftus@azuruw.com
* @date: 24/09/2018
*/
@isTest
private class EWEmailHelperTest {

	private static User systemAdminUser;
	private static Account broker;
	private static Account insured;
	private static Opportunity opportunity;
	private static Quote quote;
	private static EmailTemplate emailTemplate;
	private static Attachment emailAttachment;
	private static EWEmailHelper.EmailOptions emailOptions;
	private static EWEmailHelper.EmailResults emailResults;
	
	@isTest static void sendVlocityEmailSuccessTest() {
		setup();

		Test.startTest();

		emailResults = EWEmailHelper.SendVlocityEmail(emailOptions);

		Test.stopTest();

		System.assertEquals(true, emailResults.isSent, 'Email should be sent');
	}
    @isTest static void SendVlocityEmailTestAsset() {
          setupAsset();
        
        Test.startTest();
             emailResults = EWEmailHelper.SendVlocityEmail(emailOptions);
        Test.stopTest();
        
        System.assertEquals(true, emailResults.isSent, 'Email should be sent');
    }
        @isTest static void SendVlocityEmailTestPT() {
          setupAsset();
             List<IBA_PolicyTransaction__c> PTList=[select id from IBA_PolicyTransaction__c LIMIT 1];
            emailOptions = new EWEmailHelper.EmailOptions(
                systemAdminUser.Id,
                emailTemplate.DeveloperName,
                new List<Id>{emailAttachment.Id},
                false,
                PTList[0].Id);
            
        Test.startTest();
             emailResults = EWEmailHelper.SendVlocityEmail(emailOptions);
        Test.stopTest();
        
        System.assertEquals(true, emailResults.isSent, 'Email should be sent');
    }
	@isTest static void sendBrokerRequestEmailTest() {
		setup();

		Id siteProfileId = [SELECT Id FROM Profile WHERE Name =: EWConstants.PROFILE_BROKERHUB_GUEST_PROFILE_NAME].Id;
    	User siteUser = EWUserDataFactory.createUser('lname',siteprofileId,null,true);

		Test.startTest();
		
		System.runAs(siteUser) {
			emailResults = EWEmailHelper.SendBrokerRequestEmail(
				'Test',
				'Test',
				'Test',
				'Test',
				'11111111',
				'test@test.com');
		}
		Test.stopTest();

		System.assertEquals(true, emailResults.isSent, 'Email should be sent');
	}

    static void setupAsset() {
        
             UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);
        
        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            techteamUserRole.Id,
            false);
        systemAdminUser.Email = 'adminTest@test.com';
        
        insert systemAdminUser;
        System.runAs(systemAdminUser) {
            
            Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
            Asset createdPolicy1 = IBA_TestDataFactory.createPolicy(false,'Policy 1', IBA_UtilConst.DUMMY_ACCOUNT_ID);
            Contact   brokerContact = EWContactDataFactory.createContact(null, null, broker.Id, true);
            createdPolicy1.ContactId=brokerContact.Id;
            createdPolicy1.AccountId = broker.Id;
            insert createdPolicy1;
             insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true);
            
            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            
            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);
            
            emailTemplate = EWEmailDataFactory.createTextEmailTemplate(null, null, systemAdminUser.Id, true);
            
            emailAttachment = EWAttachmentDataFactory.createAttachment(null, quote.Id, true);
            
        IBA_PolicyTransaction__c createdPolicyTransaction = IBA_TestDataFactory.createPolicyTransaction(false,createdPolicy1.Id );
        insert createdPolicyTransaction;
            
            emailOptions = new EWEmailHelper.EmailOptions(
                systemAdminUser.Id,
                emailTemplate.DeveloperName,
                new List<Id>{emailAttachment.Id},
                false,
                createdPolicy1.Id);
        }  
        
    }
	
    static void setup() {

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
        							EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
        							techteamUserRole.Id,
        							true);

        System.runAs(systemAdminUser) {

			EW_New_Broker_Requests__c brokerRequest = new EW_New_Broker_Requests__c();
			brokerRequest.SetupOwnerId = systemAdminUser.Id;
			insert brokerRequest;

        	broker = EWAccountDataFactory.createBrokerAccount(null, true);

            insured = EWAccountDataFactory.createInsuredAccount(
            										'Mr',
            										'Test',
            										'Insured',
            										Date.newInstance(1969, 12, 29),
            										true);

        	opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

        	quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);

			emailTemplate = EWEmailDataFactory.createTextEmailTemplate(null, null, systemAdminUser.Id, true);

			emailAttachment = EWAttachmentDataFactory.createAttachment(null, quote.Id, true);

			emailOptions = new EWEmailHelper.EmailOptions(
												systemAdminUser.Id,
												emailTemplate.DeveloperName,
												new List<Id>{emailAttachment.Id},
												false,
												quote.Id);
        }
    }

	/**
    * @methodName: testGenerateEmail
    * @description: test for the utility method that creates an email message record
    * @author: Roy Lloyd
    * @date: 9/3/2020
    */
	private static testMethod void testGenerateEmail(){

		List<String> listOfRecipients = new List<String>{
				UserInfo.getUserEmail(), 'a.a@abcTest.com'
		};

		emailTemplate = EWEmailDataFactory.createTextEmailTemplate(null, null, UserInfo.getUserId(), true);

		Test.startTest();
		Messaging.SingleEmailMessage msg = EWEmailHelper.generateEmail(UserInfo.getUserId(), listOfRecipients, null, UserInfo.getUserId(), emailTemplate.Id);
		System.assertEquals(listOfRecipients, msg.toAddresses);

		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ msg });
		Test.stopTest();
	}


}