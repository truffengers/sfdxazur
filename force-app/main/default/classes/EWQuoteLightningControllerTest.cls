/**
* @name: EWQuoteLightningControllerTest
* @description: Test class for 'EWQuoteLightningController' which is used to provide Salesforce records to the 'EWQuote' lightning component
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 17/10/2018
* @ 04/02/2019 Roy Lloyd. Removed looking up the custom permissions as Opportunity and Premium are no longer on the component page
*/
@isTest
public class EWQuoteLightningControllerTest {

    private static List<Quote> quoteRecords = new List<Quote>();

    private static void setupData() {

        // create insured, broker and opportunity details
        Account testBroker = EWAccountDataFactory.createBrokerAccount('testBroker', true);
        Account insuredAccount = EWAccountDataFactory.createInsuredAccount('Mrs', 'Insured', 'Person', Date.newInstance(2000, 1, 1), true);
        Account jointInsuredAccount = EWAccountDataFactory.createInsuredAccount('Mrs', 'Joint', 'Person', Date.newInstance(2000, 1, 1), true);
        Opportunity testOppty = EWOpportunityDataFactory.createOpportunityForJointInsured('TestOppty', insuredAccount.Id, jointInsuredAccount.Id, testBroker.Id);

        // create Quote records
        for (Integer i = 0; i < 2; i++) {
            String quoteName = 'Test Quote ' + i;
            quoteRecords.add(EWQuoteDataFactory.createQuote(quoteName, testOppty.Id, testBroker.Id, false));
        }
        insert quoteRecords;

        // create Insurable Item records for the first quote
        List<vlocity_ins__InsurableItem__c> insurableItems = new List<vlocity_ins__InsurableItem__c>();
        for (Integer i = 0; i < 201; i++) {
            insurableItems.add(new vlocity_ins__InsurableItem__c (
                    Name = 'Insured Item ' + i,
                    EW_Broker__c = testBroker.Id,
                    EW_Quote__c = quoteRecords[0].Id
            ));
        }
        insert insurableItems;
    }

    private static testMethod void testRecordQuery() {
        setupData();
        EWQuoteLightningController controller = new EWQuoteLightningController();
        System.assertNotEquals(null, controller);

        vlocity_ins__InsurableItem__c test1 = EWQuoteLightningController.getInsurableItem(quoteRecords[0].Id);
        vlocity_ins__InsurableItem__c test2 = EWQuoteLightningController.getInsurableItem(quoteRecords[1].Id);
        vlocity_ins__InsurableItem__c test3 = EWQuoteLightningController.getInsurableItem(null);


        Account jointIns1 = EWQuoteLightningController.getJointInsured(quoteRecords[0].OpportunityId);
        Account jointIns2 = EWQuoteLightningController.getJointInsured(null);

        Map<String, Object> setupData = EWQuoteLightningController.getSetupData(quoteRecords[0].Id);

        System.assertEquals(0, EWQuoteLightningController.getSetupData(null).size());
        System.assert(setupData.containsKey('quote'));
        System.assert(setupData.containsKey('accountId'));
        System.assertNotEquals(null, test1);
        System.assertEquals(new vlocity_ins__InsurableItem__c(), test2);
        System.assertEquals(new vlocity_ins__InsurableItem__c(), test3);
        System.assertNotEquals(null, jointIns1.Id);
        System.assertEquals(null, jointIns2.Id);
    }

}