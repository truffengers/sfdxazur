/**
 * Created by roylloyd on 2019-04-25.
 */

public with sharing class EWButtonController {

    public static final String BUTTON_TITLES_KEY = 'hideButtons';
    public static final String IS_COMMUNITY_KEY = 'isCommunity';
    private static List<String> buttonsToHide = new List<String>();
    private static Set<String> ADD_CLAIMS_AVAILABLE_PROFILES = new Set<String>{
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            EWConstants.PROFILE_AZURUNDERWRITER_NAME,
            EWConstants.PROFILE_AZURCLAIMSUSER_NAME
    };


    @AuraEnabled
    public static Map<String, Object> loadRecordData(Id id) {
        Map<String, Object> result = new Map<String, Object>();

        switch on String.valueOf(Id.getSobjectType()) {
            when 'Asset' {
                hidePolicyButtons(id);
                if (!checkIfAddClaimsAvailable()) {
                    buttonsToHide.add('Add Claim');
                }
            }
            when 'Quote' {
                hideQuoteButtons(id);
            }
            when else {
            }
        }

        if(!buttonsToHide.isEmpty()){
            result.put(BUTTON_TITLES_KEY, buttonsToHide);
        }

        return result;
    }

    private static Boolean checkIfAddClaimsAvailable() {
        Profile profile = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
        return ADD_CLAIMS_AVAILABLE_PROFILES.contains(profile.Name);
    }

    @AuraEnabled
    public static Map<String,Object> checkIfCommunity(){
        return new Map<String,Object>{IS_COMMUNITY_KEY => Site.getSiteId() != null};
    }

    private static void hidePolicyButtons(Id id){

        // get record information
        Asset record = [SELECT Id, Status, Policy_Status__c FROM Asset WHERE Id = :id];

        // conditionally hide buttons on the Policy pagelayout in the Community
        if(record.Status.toLowerCase() != EWConstants.EW_POLICY_STATUS_ISSUED.toLowerCase()){
            buttonsToHide.add(EWConstants.BUTTON_TITLE_MTA);
        }

        if((record.Status.toLowerCase() == EWConstants.EW_POLICY_STATUS_PENDING_CANCELLATION.toLowerCase())
                || (record.Status.toLowerCase() == EWConstants.EW_POLICY_STATUS_CANCELLED.toLowerCase())) {
            buttonsToHide.add(EWConstants.BUTTON_TITLE_CANCEL);
        }

        // Hide buttons for older version policies that have Policy Status 'Not Active'
        if((record.Policy_Status__c != NULL)
                && (record.Policy_Status__c.toLowerCase() == EWConstants.EW_POLICY_STATUS_NON_ACTIVE.toLowerCase())) {
            buttonsToHide.add(EWConstants.BUTTON_TITLE_MTA);
            buttonsToHide.add(EWConstants.BUTTON_TITLE_CANCEL);
        }
    }

    private static void hideQuoteButtons(Id id){

        // get record information
        Quote record = [SELECT Id, Status FROM Quote WHERE Id = :id];
        String quoteStatus = record.Status != null ? record.Status.toLowerCase() : '';

        switch on quoteStatus {

            when 'draft' {
                // show NTU, Edit.  Hide Bind
                buttonsToHide.add(EWConstants.BUTTON_TITLE_BIND);
            }
            when 'quoted' {
                // show NTU, Edit, Bind
            }
            when 'approved' {
                // show NTU, Edit.  Hide Bind
                buttonsToHide.add(EWConstants.BUTTON_TITLE_BIND);
            }
            when 'uw approved' {
                // show NTU, Edit.  Hide Bind
                buttonsToHide.add(EWConstants.BUTTON_TITLE_BIND);
            }
            when else {
                // Hide NTU, Edit, Bind
                buttonsToHide.add(EWConstants.BUTTON_TITLE_NTU);
                buttonsToHide.add(EWConstants.BUTTON_TITLE_EDIT);
                buttonsToHide.add(EWConstants.BUTTON_TITLE_BIND);
            }
        }
    }



}