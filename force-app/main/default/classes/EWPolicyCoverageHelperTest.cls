/**
 * Created by tijojoy on 11/03/2019.
 */

@IsTest
private class EWPolicyCoverageHelperTest {

    private static Account broker;
    private static User systemAdminUser, brokerUser;
    private static Product2 product;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static Asset policy;
    private static PricebookEntry pricebookEntry;

    private static testMethod void testPolicyCoverageInsert() {

        setup();

        System.runAs(systemAdminUser) {

            vlocity_ins__AssetCoverage__c pCoverage = new vlocity_ins__AssetCoverage__c();

            pCoverage.vlocity_ins__PolicyAssetId__c = policy.Id;
            pCoverage.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":11000,"artSpecified":0,"artUnspecified":11500,"ArtSpecifiedInput":0, "buildingsLimitTotal": "Limited"}';
            pCoverage.vlocity_ins__Product2Id__c = product.id;
            insert pCoverage;

        }

        vlocity_ins__AssetCoverage__c pCoverageResult = [
                SELECT id, EW_AzurCommissionValue__c,
                        EW_BrokerCommissionValue__c,
                        EW_IPTTotal__c,EW_PremiumGrossExIPT__c,
                        EW_PremiumGrossIncIPT__c,
                        EW_PremiumNet__c,
                        EW_SpecifiedItemValue__c,
                        EW_SpecifiedValue__c,
                        EW_UnspecifiedValue__c,
                        EW_LimitTotal__c,
                        EW_RatePerSI__c
                FROM vlocity_ins__AssetCoverage__c
                WHERE vlocity_ins__PolicyAssetId__c = :policy.id
                LIMIT 1
        ];

        System.assertEquals(pCoverageResult.EW_AzurCommissionValue__c, 15.30233);
        System.assertEquals(pCoverageResult.EW_BrokerCommissionValue__c, 31.87985);
        System.assertEquals(pCoverageResult.EW_IPTTotal__c, 15.30233);
        System.assertEquals(pCoverageResult.EW_PremiumGrossExIPT__c, 127.5194);
        System.assertEquals(pCoverageResult.EW_PremiumGrossIncIPT__c, 142.82173);
        System.assertEquals(pCoverageResult.EW_PremiumNet__c, 80.3373);
        System.assertEquals(pCoverageResult.EW_SpecifiedItemValue__c, 11000.00);
        System.assertEquals(pCoverageResult.EW_SpecifiedValue__c, 0.00);
        System.assertEquals(pCoverageResult.EW_UnspecifiedValue__c, 11500.00);
        System.assertEquals(pCoverageResult.EW_LimitTotal__c, 'Limited');
        System.assertEquals(pCoverageResult.EW_RatePerSI__c, 0.0115926727272727272727272727272727);

    }

    private static testMethod void testPolicyCoverageUpdate() {

        setup();

        System.runAs(systemAdminUser) {

            vlocity_ins__AssetCoverage__c pCoverage = new vlocity_ins__AssetCoverage__c();

            pCoverage.vlocity_ins__PolicyAssetId__c = policy.Id;
            pCoverage.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":11000,"artSpecified":0,"artUnspecified":11500,"ArtSpecifiedInput":0, "buildingsLimitTotal": "Limited"}';
            pCoverage.vlocity_ins__Product2Id__c = product.id;

            insert pCoverage;

            pCoverage.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10110,"artSpecified":0,"artUnspecified":13400,"ArtSpecifiedInput":0, "buildingsLimitTotal": "Unlimited"}';

            update pCoverage;
        }

        vlocity_ins__AssetCoverage__c pCoverageResult = [
                SELECT EW_SpecifiedItemValue__c,
                        EW_SpecifiedValue__c,
                        EW_UnspecifiedValue__c,
                        EW_LimitTotal__c,
                        EW_RatePerSI__c
                FROM vlocity_ins__AssetCoverage__c
                WHERE vlocity_ins__PolicyAssetId__c = :policy.id
                LIMIT 1
        ];

        System.assertEquals(pCoverageResult.EW_SpecifiedItemValue__c, 10110.00);
        System.assertEquals(pCoverageResult.EW_SpecifiedValue__c, 0.00);
        System.assertEquals(pCoverageResult.EW_UnspecifiedValue__c, 13400.00);
        System.assertEquals(pCoverageResult.EW_LimitTotal__c, 'Unlimited');
        System.assertEquals(pCoverageResult.EW_RatePerSI__c, 0.0126131948565776458951533135509397);

    }

    private static testMethod void testHandleDuplicatingCoverages() {

        setup();
        Asset policy = [SELECT Id FROM Asset LIMIT 1], snapshotPolicy;

        Integer policyCoverageCount = [SELECT Count() FROM vlocity_ins__AssetCoverage__c WHERE vlocity_ins__PolicyAssetId__c = :policy.Id];
        System.assertEquals(1, policyCoverageCount);

        System.runAs(brokerUser) {
            // create duplicating policy coverage record
            vlocity_ins__AssetCoverage__c duepCoverage = IBA_TestDataFactory.createPolicyCoverage(true, 'dupeCoverage', product.Id, policy.Id);
        }

        Integer recheckPolicyCoverageCount = [SELECT Count() FROM vlocity_ins__AssetCoverage__c WHERE vlocity_ins__PolicyAssetId__c = :policy.Id];
        System.assertEquals(1, recheckPolicyCoverageCount);

    }


    static void setup() {

        Contact brokerContact;
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            brokerContact = EWContactDataFactory.createContact(null, null, broker.Id, true);

            product = EWProductDataFactory.createProduct(null, true);
            PricebookEntry priceBookEntry = new PricebookEntry (Product2Id = product.Id, Pricebook2Id = Test.getStandardPricebookId(), UnitPrice = 50, IsActive = true);
            insert priceBookEntry;

        }


        Profile brokerProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_EW_PARTNER_NAME);
        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);

        System.runAs(brokerUser) {

            insured = EWAccountDataFactory.createInsuredAccount(
                    'Mr',
                    'Test',
                    'Insured',
                    Date.newInstance(1969, 12, 29),
                    true);

            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
            policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, true);
            vlocity_ins__AssetCoverage__c ac = IBA_TestDataFactory.createPolicyCoverage(true, 'testCoverage', product.Id, policy.Id);
        }
    }
}