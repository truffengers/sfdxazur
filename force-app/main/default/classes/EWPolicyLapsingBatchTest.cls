/**
* @name: EWPolicyLapsingBatchTest
* @description: test class for EWPolicyLapsingBatch  
* @author: Aleksejs Jedamenko ajedamenko@azuruw.com
* @date: 06/03/2020
*
*/
@isTest
private class EWPolicyLapsingBatchTest {

    /**
    * @methodName: setup
    * @description: setup test values for following test methods
    * @dateCreated: 06/03/2020
    */
	@testSetup
    static void setup(){
        
        Account testBroker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account testInsured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );

        insert new List<Account>{
                testBroker, testInsured
        };

        Opportunity testOpportunity = EWOpportunityDataFactory.createOpportunity(null, testInsured.Id, testBroker.Id, true);

        Asset testExpireIssuedPolicy = EWPolicyDataFactory.createPolicy(null, testOpportunity.Id, testBroker.Id, testInsured.Id, false);
        testExpireIssuedPolicy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        testExpireIssuedPolicy.vlocity_ins__EffectiveDate__c = Date.today().addDays(-300);
        testExpireIssuedPolicy.vlocity_ins__InceptionDate__c = Date.today().addDays(-300);
        testExpireIssuedPolicy.vlocity_ins__ExpirationDate__c = Date.today().addDays(-3);
        
        Asset testIssuedPolicy = EWPolicyDataFactory.createPolicy(null, testOpportunity.Id, testBroker.Id, testInsured.Id, false);
        testIssuedPolicy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        testIssuedPolicy.vlocity_ins__EffectiveDate__c = Date.today().addDays(-300);
        testIssuedPolicy.vlocity_ins__InceptionDate__c = Date.today().addDays(-300);
        testIssuedPolicy.vlocity_ins__ExpirationDate__c = Date.today().addDays(10);
        insert new List<Asset> {testExpireIssuedPolicy, testIssuedPolicy};
        
        Quote testNTURenewalQuoteForExpired = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testNTURenewalQuoteForExpired.Status = EWConstants.QUOTE_STATUS_NTU;
        testNTURenewalQuoteForExpired.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        testNTURenewalQuoteForExpired.EW_Source_Policy__c = testExpireIssuedPolicy.Id;
        
        Quote testNTURenewalQuoteForNonExpired = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testNTURenewalQuoteForNonExpired.Status = EWConstants.QUOTE_STATUS_NTU;
        testNTURenewalQuoteForNonExpired.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        testNTURenewalQuoteForNonExpired.EW_Source_Policy__c = testIssuedPolicy.Id;
        insert new List<Quote> {testNTURenewalQuoteForExpired, testNTURenewalQuoteForNonExpired};
    }
    
     /**
    * @methodName: policyStatusUpdateTest
    * @description: test EWPolicyLapsingBatch batch. Scenario: test setup creates 2 policy records, 1 should be affected by batch and 1 shouldn't. 
    * After we check that 1 record with status Issued and 1 record with status Lapsed exist in the database
    * @dateCreated: 06/03/2020
    */
    @isTest
    private static void policyStatusUpdateTest(){
        
        Test.startTest();
        Id batchId = Database.executeBatch(new EWPolicyLapsingBatch());
        Test.stopTest();
        List<Asset> expectedLapsedPolicies = [SELECT Status FROM Asset WHERE Status = :EWConstants.EW_POLICY_STATUS_LAPSED];
        List<Asset> expectedIssuedPolicies = [SELECT Status, IBA_ActivePolicy__c FROM Asset WHERE Status = :EWConstants.EW_POLICY_STATUS_ISSUED];
        System.assertEquals(1, expectedLapsedPolicies.size());
        System.assertEquals(1, expectedIssuedPolicies.size());
    }
    
    
}