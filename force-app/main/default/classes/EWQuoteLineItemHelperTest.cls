/**
 * Created by llyrjones on 04/12/2018.
 */

@isTest
public with sharing class EWQuoteLineItemHelperTest {

    private static Account broker;
    private static User systemAdminUser;
    private static Product2 product;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static PricebookEntry pricebookEntry;
    public static final String PRIMARY_INSURED_SANCTIONS_RESULT = 'primaryInsuredSanctionsResult';
    public static final String JOINED_INSURED_SANCTIONS_RESULT = 'jointInsuredSanctionsResult';


    private static testMethod void testCreateQuoteLineItem() {

        setup();

        System.runAs(systemAdminUser) {

            QuoteLineItem qli = new QuoteLineItem();
            qli.QuoteId = quote.id;
            qli.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0, "buildingsLimitTotal": "Unlimited"}';
            qli.PricebookEntryId = priceBookEntry.id;
            qli.Quantity = 1;
            qli.UnitPrice = 0;
            qli.Product2Id = product.id;

            insert qli;
        }


        QuoteLineItem qliResult = [
                SELECT id, EW_AzurCommissionValue__c,
                        EW_BrokerCommissionValue__c,
                        EW_IPTTotal__c,EW_PremiumGrossExIPT__c,
                        EW_PremiumGrossIncIPT__c,
                        EW_PremiumNet__c,
                        EW_SpecifiedItemValue__c,
                        EW_SpecifiedValue__c,
                        EW_UnspecifiedValue__c,
                        EW_LimitTotal__c,
                        EW_RatePerSI__c
                FROM QuoteLineItem
                LIMIT 1
        ];

        System.assertEquals(qliResult.EW_AzurCommissionValue__c, 14.70844);
        System.assertEquals(qliResult.EW_BrokerCommissionValue__c, 5.88384);
        System.assertEquals(qliResult.EW_IPTTotal__c, 2.94192);
        System.assertEquals(qliResult.EW_PremiumGrossExIPT__c, 24.51598);
        System.assertEquals(qliResult.EW_PremiumGrossIncIPT__c, 27.4579);
        System.assertEquals(qliResult.EW_PremiumNet__c, 15.69023);
        System.assertEquals(qliResult.EW_SpecifiedItemValue__c, 10000.00);
        System.assertEquals(qliResult.EW_SpecifiedValue__c, 0.00);
        System.assertEquals(qliResult.EW_UnspecifiedValue__c, 10000.00);
        System.assertEquals(qliResult.EW_LimitTotal__c, 'Unlimited');
        System.assertEquals(qliResult.EW_RatePerSI__c, 0.002451598);
    }

    private static testMethod void testUpdateQuoteLineItem() {

        setup();

        System.runAs(systemAdminUser) {

            QuoteLineItem qli = new QuoteLineItem();
            qli.QuoteId = quote.id;
            qli.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0, "buildingsLimitTotal": "Unlimited"}';
            qli.PricebookEntryId = priceBookEntry.id;
            qli.Quantity = 1;
            qli.UnitPrice = 0;
            qli.Product2Id = product.id;

            insert qli;

            qli.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":14.70844, "brokerCommissionValue":5.88384,"premiumGrossExIPT":24.51598,"premiumNet":15.69023,"premiumGrossIncIPT":27.4579,"iptTotal":2.94192,"artSpec":0,"artValue":12000,"artSpecified":0,"artUnspecified":14000,"ArtSpecifiedInput":100, "buildingsLimitTotal": "Limited"}';

            update qli;
        }

        QuoteLineItem qliResult = [
                SELECT EW_SpecifiedItemValue__c,
                        EW_SpecifiedValue__c,
                        EW_UnspecifiedValue__c,
                        EW_LimitTotal__c,
                        EW_RatePerSI__c
                FROM QuoteLineItem
                LIMIT 1
        ];

        System.assertEquals(qliResult.EW_SpecifiedItemValue__c, 12000.00);
        System.assertEquals(qliResult.EW_SpecifiedValue__c, 0.00);
        System.assertEquals(qliResult.EW_UnspecifiedValue__c, 14000.00);
        System.assertEquals(qliResult.EW_LimitTotal__c, 'Limited');
        System.assertEquals(qliResult.EW_RatePerSI__c, 0.00204299833333333333333333333333333);
    }

    private static testMethod void testUpdateAttributeSelectedValues() {

        setup();


        System.runAs(systemAdminUser) {

            product.Name = 'Insured Entity';
            update product;

            String oldInput = '{"clientDetails":{"primaryInsuredSanctionsResult": "Passed","jointInsuredSanctionsResult": "Failed"}}';
            String input = '{"clientDetails":{"primaryInsuredSanctionsResult": "Failed","jointInsuredSanctionsResult": "Passed"}}';
            Boolean runMethod = EWVlocityRemoteActionHelperTest.testRemoteActionMethod('updateAttributeSelectedValues');

            Map<String, Object> inputMap = (Map<String, Object>) JSON.deserializeUntyped(input);
            Map<String, Object> optionMap = new Map<String, Object>();
            optionMap.put('quoteId', quote.id);

            QuoteLineItem qli = new QuoteLineItem();

            qli.QuoteId = quote.id;
            qli.PricebookEntryId = priceBookEntry.id;
            qli.Quantity = 1;
            qli.UnitPrice = 0;
            qli.Product2Id = product.id;
            qli.vlocity_ins__AttributeSelectedValues__c = oldInput;
            insert qli;

            EWVlocityRemoteActionHelper.updateAttributeSelectedValues(inputMap, new Map<String, Object>(), optionMap);
        }

        List<QuoteLineItem> qliResult = [
                SELECT id, vlocity_ins__AttributeSelectedValues__c
                FROM QuoteLineItem
                WHERE Quote.Id = :quote.Id
                LIMIT 1
        ];


        String JSONString = qliResult[0].vlocity_ins__AttributeSelectedValues__c;

        Map<String, Object> att = (Map<String, Object>) JSON.deserializeUntyped(JSONString);
        Map<String, Object> attValues = (Map<String, Object>) att.get('clientDetails');

        System.AssertEquals('Passed', (String) attValues.get(PRIMARY_INSURED_SANCTIONS_RESULT));
        System.AssertEquals('Failed', (String) attValues.get(JOINED_INSURED_SANCTIONS_RESULT));

    }


    static void setup() {

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            insured = EWAccountDataFactory.createInsuredAccount(
                    'Mr',
                    'Test',
                    'Insured',
                    Date.newInstance(1969, 12, 29),
                    true);

            product = EWProductDataFactory.createProduct(null, true);
            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);

            priceBookEntry = new PricebookEntry (Product2Id = product.id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
            insert priceBookEntry;

        }

    }
}