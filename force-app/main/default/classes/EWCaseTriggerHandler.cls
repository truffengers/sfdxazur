/*
Name:            EWCaseTriggerHandler
Description:     This Class is called by Case Trigger
Created By:      Bharati Pal 
Created On:      05 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1              	Bharati Pal         05-May-21           
************************************************************************************************
*/

public with sharing class EWCaseTriggerHandler implements ITriggerHandler {
    
    public static Boolean IsDisabled;
    
    public Boolean IsDisabled(){
        return EWCaseTriggerHandler.IsDisabled != null ? EWCaseTriggerHandler.IsDisabled : false;
    }
    
    public void BeforeInsert(List<SObject> newItems) {
        if (IsDisabled() != true) {
            IsDisabled = true;
            EWCaseTriggerHelper.handlecases(newItems);
        }

    }
    
    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems){}
    
    public void BeforeDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterInsert(Map<Id, SObject> newItems) {}
    
    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}
    
    public void AfterDelete(Map<Id, SObject> oldItems) {}
    
    public void AfterUndelete(Map<Id, SObject> oldItems) {}
    
}