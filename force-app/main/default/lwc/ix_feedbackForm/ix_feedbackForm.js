import { LightningElement, track } from 'lwc';
import { analyticsEvent } from 'c/ix_utils';
import { loadScript } from 'lightning/platformResourceLoader';
import Confetti from '@salesforce/resourceUrl/confetti';

export default class Ix_feedbackForm extends LightningElement {
    @track ratingArray = [];

    @track questions = [
        {
            shortName: 'Overall Experience Rating',
            heading: 'Please rate your overall experience of SmartDocs on a scale from 1-10.',
            subheading: '1 = terrible experience, 10 = amazing experience',
            required: true,
            typeRating: true
        },
        {
            shortName: 'Company',
            heading: 'What\'s your company?',
            subheading: 'This is optional if you would prefer not to say, but we do like to know who is giving feedback (hello brownie points!)',
            typeInput: true
        },
        {
            shortName: 'Ask Underwriter Question',
            heading: 'Do you have a question for an underwriter?',
            subheading: 'If you do we\'ll collect the question and give you a call back, if not we\'ll skip that part!',
            required: true,
            typeRadio: true,
            radioOptions: [
                {
                    optionShortName: 'Yes',
                    optionName: 'Yes - I have a question 🙋‍',
                    currentlySelected: false
                },
                {
                    optionShortName: 'No',
                    optionName: 'No - just here to give feedback 👼🏼',
                    currentlySelected: false
                }
            ]
        },
        {
            shortName: 'Question for Underwriter',
            heading: 'What is your question?',
            subheading: 'We\'ll ask one of our dedicated Smart Home underwriters to call you back, pronto!',
            typeInput: true,
            hide: true,
            dependsOn: { 'Ask Underwriter Question': 'Yes' }
        },
        {
            shortName: 'Liked',
            heading: 'What do you like about the product?',
            typeInput: true
        },
        {
            shortName: 'Improvements',
            heading: 'What could improve?',
            typeInput: true
        },
        {
            shortName: 'Give You a Call',
            heading: 'Sometimes it is really helpful for us to give you a call to understand your feedback more - this is often because we are considering how we could improve the product.',
            subheading: 'Where it could be beneficial, do we have your permission to follow up?',
            typeRadio: true,
            radioOptions: [
                {
                    optionShortName: 'Yes',
                    optionName: 'Yes',
                    currentlySelected: false
                },
                {
                    optionShortName: 'No',
                    optionName: 'No',
                    currentlySelected: false
                }
            ]
        }
    ]

    formAnswers = {};

    @track preventSubmit;
    @track formSubmitted = false;
    @track submitFailed = false;

    connectedCallback() {
        for (let i = 1; i <= 10; i++) {
            this.ratingArray.push({ glowing: false, value: i });
        }

        loadScript(this, `${Confetti}/confetti.js`)
            .catch(error => console.log(error)); // eslint-disable-line
    }

    handleSubmit (event) {
        event.preventDefault();

        let allRequiredFieldsComplete = true;
        this.questions
            .filter(question => question.required)
            .forEach(question => {
                if (!this.formAnswers[question.shortName]) {
                    allRequiredFieldsComplete = false;
                    this.questions = this.questions.map(q => {
                        return q.shortName === question.shortName
                        ? { ...q, showWarning: true}
                        : q;
                    })
                }
            })

        if (!allRequiredFieldsComplete) {
            return this.preventSubmit = true;
        }

        return analyticsEvent({
            eventName: 'Feedback Submitted',
            eventData: JSON.stringify(this.formAnswers)
        })
            .then(() => {
                confetti({ // eslint-disable-line
                    spread: 115,
                    particleCount: 75,
                    startVelocity: 70,
                    origin: { x: 0.5, y: 1 }
                });
                this.formSubmitted = true;
            })
            .catch(() => this.submitFailed = true);
    }

    handleInputChange(event) {
        const question = event.target.dataset.question;
        const answer = event.target.value || event.target.dataset.value;

        this.preventSubmit = false;

        this.formAnswers[question] = answer;

        const radioQuestions = this.questions
            .filter(q => q.typeRadio)
            .map(q => q.shortName);

        if (radioQuestions.includes(question)) {
            this.questions = this.questions.map(q => {
                if (q.shortName === question) {
                    q.radioOptions = q.radioOptions.map(option => {
                        return option.optionShortName === answer
                        ? { ...option, currentlySelected: true }
                        : { ...option, currentlySelected: false }
                    })
                }
                return { ...q, showWarning: false };
            })
        }

        this.questions = this.questions.map(q => {
            if (q.dependsOn && Object.keys(q.dependsOn).includes(question)) {
                return answer === q.dependsOn[question]
                ? { ...q, hide: false } : { ...q, hide: true };
            }
            return q;
        })

        if (question === 'Overall Experience Rating') {
            this.ratingArray = this.ratingArray.map(star => {
                return star.value <= parseInt(answer, 10)
                ? { ...star, glowing: true } : { ...star, glowing: false };
            })

            this.questions[0].showWarning = false;
        }
    }

    handleFeedbackClose() {
        if (!this.formSubmitted) {
            let confirmClose = confirm('Your feedback was not submitted. Would you like to discard your progress?');
            return confirmClose
            ? this.dispatchEvent(new CustomEvent('closefeedback'))
            : null;
        }

        return this.dispatchEvent(new CustomEvent('closefeedback'));
    }
}