/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 09/10/2017
 */
({
    setTitleAndDescription : function(component, event, helper) {
        var title = component.get('v.title');
        var description = component.get('v.description');

        if(!$A.util.isEmpty(title) && component.get('v.ready')) {
            document.title = title + ' | Azur Broker iQ';

            if(description) {
                $('meta[name=description]').remove();
                $('head').append('<meta name="description" content="' + description + '">');
            }
        }
    }
})