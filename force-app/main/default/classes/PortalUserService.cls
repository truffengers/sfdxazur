public class PortalUserService {

    private static Contact currentUserAsContact;
    private static User currentUser;
    private static Account azurAccount;

    public static User getCurrentUser() {
        if(currentUser == null) {
            Id userId = UserInfo.getUserId();
            String queryString = Nebula_Tools.FieldSetUtils.queryFieldSet('User', 'Portal_Fields', 'Id = :userId');
            currentUser = (User)Database.query(queryString)[0];
        }
		return currentUser;        
    }
    
    public static Contact getCurrentUserAsContact() {
        if(currentUserAsContact == null) {
            Id contactId = getContactIdOrGenerateNew();
            String queryString = Nebula_Tools.FieldSetUtils.queryFieldSet('Contact', 'Portal_Fields', 'Id = :contactId');

            List<sObject> contacts = Database.query(queryString);
            if (!contacts.isEmpty()) {
                currentUserAsContact = (Contact)contacts.get(0);
            }

        }
        return currentUserAsContact;
    }

    public static Id getAzurAccountId() {
        if (azurAccount == null) {
            List<Account> accs = new List<Account>([SELECT Id, Name FROM Account WHERE Name = 'Azur Group']);
            if (accs.isEmpty()) {
                insert new Account(Name = 'Azur Group');
                return getAzurAccountId();
            }
            azurAccount = accs.get(0);
        }
        return azurAccount.Id;
    }

    private static String getContactIdOrGenerateNew() {
        User thisCurrentUser = getCurrentUser();
        if (thisCurrentUser.ContactId != null) {
            return thisCurrentUser.ContactId;
        } else if (thisCurrentUser.Contact_Id__c != null) {
            return thisCurrentUser.Contact_Id__c;
        } else if (thisCurrentUser.UserType == 'Standard') {
            //this user is using community first time, we need to create 'fake' Contact for him to keep statistics in BrightTALK
            Id brightTALKRecordTypeId = Contact.getSObjectType().getDescribe().getRecordTypeInfosByName().get('BrightTALK').recordTypeId;
            Contact contact = new Contact(
                    AccountId = getAzurAccountId(),
                    FirstName = thisCurrentUser.FirstName,
                    LastName = 'BrightTALK' + thisCurrentUser.LastName,
                    Email = thisCurrentUser.Email,
                    RecordTypeId = brightTALKRecordTypeId,
                    Is_BIQ_self_registration__c = false
            );
            insert contact;
            thisCurrentUser.Contact_Id__c = contact.Id;
            update thisCurrentUser;

            return contact.Id;
        }
        return null;
    }

}