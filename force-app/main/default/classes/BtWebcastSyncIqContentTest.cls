@isTest
private class BtWebcastSyncIqContentTest {
	
	private static TestObjectGenerator tog = new TestObjectGenerator();

	@isTest static void basic() {
		IQ_Content__c iqContent = tog.getWebcastIqContent();
		BrightTALK__Webcast__c webcast = tog.getBtWebcast(123);

		webcast.IQ_Content__c = iqContent.Id;

		update webcast;

		iqContent = [SELECT Id, BrightTALK_Webcast__c FROM IQ_Content__c WHERE Id = :iqContent.Id];

		System.assertEquals(webcast.Id, iqContent.BrightTALK_Webcast__c);
	}
		
}