({	
	progressChanged : function(component, event, helper) {
		component.set('v.progress', event.getParam('value'));
	}
})