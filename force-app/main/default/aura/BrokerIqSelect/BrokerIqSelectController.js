({
	selectChanged : function(component, event, helper) {
		component.set('v.value', event.target.value);
	},
	valueChanged : function(component, event, helper) {
		helper.valueChanged(component);
	}
})