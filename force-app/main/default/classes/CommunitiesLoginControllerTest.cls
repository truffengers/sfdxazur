/**
* @name: CommunitiesLoginControllerTest
* @description: An apex page controller that exposes the site login functionality
* @author: Azur Group
* @date: 2017
* @Modified: Ignacio Sarmiento  27/04/2018  AZ-222 Remove 'SeeAllData=true'
*/
@IsTest global with sharing class CommunitiesLoginControllerTest {
    @IsTest
    global static void testCommunitiesLoginController () {
     	CommunitiesLoginController controller = new CommunitiesLoginController();
     	System.assertEquals(null, controller.forwardToAuthPage());       
    }    
}