trigger IqContentSyncWebcast on IQ_Content__c (after insert, after update) {

    Map<Id, BrightTALK__Webcast__c> webcastsToUpdate = new Map<Id, BrightTALK__Webcast__c>();
    
    for(Integer i=0; i < Trigger.size; i++) {
        IQ_Content__c newContent = Trigger.new[i];
        
        if(newContent.BrightTALK_Webcast__c != null 
           && (Trigger.isInsert || newContent.BrightTALK_Webcast__c != Trigger.old[i].BrightTALK_Webcast__c)) {
               BrightTALK__Webcast__c thisWebcast = new BrightTALK__Webcast__c(Id = newContent.BrightTALK_Webcast__c,
                                                                              IQ_Content__c = newContent.Id);
               webcastsToUpdate.put(thisWebcast.Id, thisWebcast);
           }
    }
    
    if(!webcastsToUpdate.isEmpty()) {
        List<BrightTALK__Webcast__c> existingWebcasts = [SELECT Id, IQ_Content__c 
                                               FROM BrightTALK__Webcast__c
                                               WHERE Id IN :webcastsToUpdate.keySet()];
        List<BrightTALK__Webcast__c> toReallyUpdate = new List<BrightTALK__Webcast__c>();
        
        for(BrightTALK__Webcast__c thisExistingWebcast : existingWebcasts) {
            BrightTALK__Webcast__c thisMaybeUpdate = webcastsToUpdate.get(thisExistingWebcast.Id);
            if(thisExistingWebcast.IQ_Content__c != thisMaybeUpdate.IQ_Content__c) {
                toReallyUpdate.add(thisMaybeUpdate);
            }
        }
        
        if(!toReallyUpdate.isEmpty()) {
          update toReallyUpdate;
        }
    }
}