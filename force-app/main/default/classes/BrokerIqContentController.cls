/**
* @name: BrokerIqContentController
* @description: BrokerIqContent apex controller
* @author: Nebula Consulting 
* @date: 14/10/2017
* @Modified: Ignacio Sarmiento	18/12/2017	Add 'isRenderCPDButton' attribute and functionality
*/
public with sharing class BrokerIqContentController {
    private static final Integer LIMIT_QUERY_RECORDS = 1;
    
	public class Result {
        @AuraEnabled
		public IQ_Content__c iqContent {get; set;}
        @AuraEnabled
		public Boolean isRenderCPDButton {get; set;}
	}

    @AuraEnabled
    public static Result getIqContentInformation(String recordId) {
        Result result = new Result();
        result.iqContent = getIqContentRecord(recordId);
        result.isRenderCPDButton = isRenderCPDButton(result.iqContent);
        return result;
    }

	private static IQ_Content__c getIqContentRecord(String recordId) {
		Id networkId = Network.getNetworkId();
		Set<String> iqContentFields = new Set<String>();
		iqContentFields.addAll(IQ_Content__c.sObjectType.getDescribe().fields.getMap().keySet());
		iqContentFields.addAll(new Set<String> 
		{
			'Broker_iQ_Contributor__r.Id',
			'Broker_iQ_Contributor__r.Full_Name__c',
			'Broker_iQ_Contributor__r.Portrait_Cache__c',
			'RecordType.Name',
			'RecordType.DeveloperName',
			'BrightTALK_Webcast__r.BrightTALK__Description__c',
			'BrightTALK_Webcast__r.BrightTALK__Webcast_Id__c',
			'(SELECT Id, Topic.Name, TopicId FROM TopicAssignments WHERE Topic.NetworkId = :networkId ORDER BY Topic.Name)',
			'(SELECT Id FROM Tests__r WHERE Status__c = \'Published\' ORDER BY LastModifiedDate DESC LIMIT 1)'
		});
		String q = Nebula_Tools.FieldSetUtils.queryFields('IQ_Content__c', iqContentFields, 'Id = :recordId', null);
        
		List<IQ_Content__c> iqcs = Database.query(q);

		return iqcs.isEmpty() ? null : iqcs[0];
    }
        
    private static Boolean isRenderCPDButton(IQ_Content__c iqContent){
       return isRecentWebinar(iqContent.Id) && 
           	  hasIQContentAnyCPDTest(iqContent) && 
              notPassCPDTest(iqContent);
    }

    private static Boolean isRecentWebinar(String recordId){
        List<Id> iqContentIds = new List<Id>{recordId};
        List<IQ_Content__c> recentWebinars = BrokerIqIQContentUtil.getRecentWebinars(iqContentIds, new List<Id>(), LIMIT_QUERY_RECORDS);
		return !recentWebinars.isEmpty();
    }
    
    private static Boolean hasIQContentAnyCPDTest(IQ_Content__c iqContent){
    	return iqContent != null && iqContent.Tests__r.size() > 0;
    }
    
    private static Boolean notPassCPDTest(IQ_Content__c iqContent){
        User currentUser = [SELECT Id, ContactId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        return [SELECT count() FROM CPD_Assessment__c WHERE CPD_Test__c =: iqContent.Tests__r[0].Id AND Passed__c = true AND Contact__c =: currentUser.ContactId] == 0;
    }
    
}