({
    doInit: function (component) {
        this.loadJSONReport(component);
    },

    loadJSONReport: function (component) {
        this.enableSpinner(component);
        let activeReport = component.get("v.activeReport");

        let createdStart = component.find("createdDateStart").get("v.value");
        let createdEnd = component.find("createdDateEnd").get("v.value");
        let effectiveStart = component.find("effectiveDateStart").get("v.value");
        let effectiveEnd = component.find("effectiveDateEnd").get("v.value");
        let action = component.get("c.getReportData");

        action.setParams({
            'activeReport': activeReport,
            'createdStart': createdStart,
            'createdEnd': createdEnd,
            'effectiveStart': effectiveStart,
            'effectiveEnd': effectiveEnd,
            'asCsv': false
        });

        action.setCallback(this, function (response) {
            const responseJson = response.getReturnValue();

            if (responseJson) {
                component.set("v.gridData", JSON.parse(responseJson));
            }
            this.disableSpinner(component);
        });
        $A.enqueueAction(action);
    },

    enableSpinner : function(component) {
        this.setSpinnerEnability(component, true);
    },

    disableSpinner : function(component) {
        this.setSpinnerEnability(component, false);
    },

    setSpinnerEnability: function(component, enableRegisterButton) {
        component.set("v.toggleSpinner", enableRegisterButton);
    }
});