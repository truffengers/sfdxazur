public with sharing class BrokerIqShareExtension {
	
	public String siteBaseUrl {get; set;}

	public BrokerIqShareExtension(ApexPages.StandardController controller) {
		siteBaseUrl = Site.getBaseSecureUrl() + 's/iq-content/';
	}
	
}