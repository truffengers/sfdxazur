/**
* @name: IBA_CashMatchingHistoryTrigerHandlerTest
* @description: Test class for IBA_CashMatchingHistoryTriggerHandler
*
*/
@IsTest
private class IBA_CashMatchingHistoryTrigerHandlerTest {

    private static final String COMPANY_NAME = 'ApexTestCompany';
    private static final String CURRENCY_USD = 'USD';
    private static final Decimal MONEY = 100.50;
    private static final Decimal MONEY2 = 80.01;
    private static final Decimal MONEY3 = 10.00;

    @testSetup
    static void createTestRecords() {
        IBA_TestDataFactory.FinancialForceSetup ffs = IBA_TestDataFactory.setupFinancialForce(COMPANY_NAME, CURRENCY_USD);
        Group companyGroup = ffs.companyGroup;
        c2g__codaCompany__c company = ffs.company;
    
        c2g__codaYear__c yr = IBA_TestDataFactory.createYear(true, companyGroup.Id, company.Id);
        c2g.CODAYearWebService.calculatePeriods(yr.Id);

        c2g__codaAccountingCurrency__c curr = IBA_TestDataFactory.createCurrency(true, CURRENCY_USD);

        c2g__codaGeneralLedgerAccount__c gla = IBA_TestDataFactory.createGeneralLedgerAccount(true, 'Brochures', 'uniqueCaseInsensitive24!');

        Product2 product = IBA_TestDataFactory.createProduct(false, 'Product 1', IBA_UtilConst.PRODUCT_RT_Master);
        product.IBA_AIGProduct__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
        product.IBA_AIGMinorLine__c = 'PCG Test Liability';
        product.IBA_AIGPolicyCodeNumber__c = '000000test';
        product.IsActive = true;
        product.c2g__CODASalesRevenueAccount__c = gla.Id;
        insert product;

        Account acc = IBA_TestDataFactory.createAccount(false, 'testAccOnHold', IBA_UtilConst.ACCOUNT_RT_BROKER);
        acc.c2g__CODAAccountTradingCurrency__c = CURRENCY_USD;
        acc.c2g__CODAAccountsPayableControl__c = gla.Id;
        acc.c2g__CODAAccountsReceivableControl__c = gla.Id;
        acc.c2g__CODAVATStatus__c = 'Home';
        acc.c2g__CODATaxCalculationMethod__c = 'Gross';
        acc.c2g__CODABaseDate1__c = 'Invoice Date';
        acc.c2g__CODADaysOffset1__c = 0;
        acc.c2g__CODADescription1__c = 'Test';
        acc.c2g__CODADiscount1__c = 0;
        insert acc;

        c2g__codaPeriod__c period = [SELECT Id FROM c2g__codaPeriod__c WHERE 
            c2g__YearName__c = :yr.Id AND c2g__PeriodNumber__c != '000' AND 
            c2g__EndDate__c > TODAY LIMIT 1];

        c2g__codaBankAccount__c bank = IBA_TestDataFactory.createBankAccount(false, 'Trust Account USD', '123456789', 'Santander', 'uniqueCaseInsensitive24!', gla.Id, curr.Id);
        insert bank;

        Test.startTest();
        List<c2g__codaInvoice__c> invoicesToInsert = new List<c2g__codaInvoice__c>();
        c2g__codaInvoice__c invoice = IBA_TestDataFactory.createInvoice(false, company.Id, acc.Id, period.Id, curr.Id);
        c2g__codaInvoice__c invoice2 = IBA_TestDataFactory.createInvoice(false, company.Id, acc.Id, period.Id, curr.Id);

        Asset policy = IBA_TestDataFactory.createPolicy(false, 'Policy 1', acc.Id);
        policy.IBA_ProducingBrokerAccount__c = acc.Id;
        policy.IBA_AccountingBroker__c = acc.Id;
        policy.Azur_Account__c = acc.Id;
        insert policy;

        List<IBA_PolicyTransaction__c> policyTransactions = IBA_TestDataFactory.createPolicyTransactions(true, 2, policy.Id);
        invoice.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        invoice2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        invoicesToInsert.add(invoice);
        invoicesToInsert.add(invoice2);
        insert invoicesToInsert;

        List<c2g__codaInvoiceLineItem__c> invoiceLineItemsToInsert = new List<c2g__codaInvoiceLineItem__c>();
        c2g__codaInvoiceLineItem__c invoiceLineItem = IBA_TestDataFactory.createInvoiceLineItem(false, 15, MONEY, invoice.Id, product.Id);
        c2g__codaInvoiceLineItem__c invoiceLineItem2 = IBA_TestDataFactory.createInvoiceLineItem(false, 15, MONEY2, invoice2.Id, product.Id);

        invoiceLineItemsToInsert.add(invoiceLineItem);
        invoiceLineItemsToInsert.add(invoiceLineItem2);
        insert invoiceLineItemsToInsert;

        List<c2g__codaPurchaseCreditNote__c> purchaseCreditNotesToInsert = new List<c2g__codaPurchaseCreditNote__c>();
        c2g__codaPurchaseCreditNote__c purchaseCreditNote = IBA_TestDataFactory.createPurchaseCreditNote(false, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c);
        purchaseCreditNote.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        c2g__codaPurchaseCreditNote__c purchaseCreditNote2 = IBA_TestDataFactory.createPurchaseCreditNote(false, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c);
        purchaseCreditNote2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        purchaseCreditNotesToInsert.add(purchaseCreditNote);
        purchaseCreditNotesToInsert.add(purchaseCreditNote2);
        insert purchaseCreditNotesToInsert;

        List<c2g__codaPurchaseCreditNoteExpLineItem__c> purchaseCreditNoteExpLineItemToInsert = new List<c2g__codaPurchaseCreditNoteExpLineItem__c>();
        c2g__codaPurchaseCreditNoteExpLineItem__c purchaseCreditNoteExpLineItem = IBA_TestDataFactory.createPurchaseCreditNoteExpLineItem(false,
            MONEY,
            invoice.c2g__OwnerCompany__c, 
            gla.Id, 
            purchaseCreditNote.Id);

        c2g__codaPurchaseCreditNoteExpLineItem__c purchaseCreditNoteExpLineItem2 = IBA_TestDataFactory.createPurchaseCreditNoteExpLineItem(false,
            MONEY2,
            invoice.c2g__OwnerCompany__c, 
            gla.Id, 
            purchaseCreditNote2.Id);

        purchaseCreditNoteExpLineItemToInsert.add(purchaseCreditNoteExpLineItem);
        purchaseCreditNoteExpLineItemToInsert.add(purchaseCreditNoteExpLineItem2);
        insert purchaseCreditNoteExpLineItemToInsert;

        List<c2g__codaCreditNote__c> salesCreditNotesToInsert = new List<c2g__codaCreditNote__c>();
        c2g__codaCreditNote__c salesCreditNote = IBA_TestDataFactory.createSalesCreditNote(false, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c);
        salesCreditNote.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        c2g__codaCreditNote__c salesCreditNote2 = IBA_TestDataFactory.createSalesCreditNote(false, 
        invoice2.c2g__OwnerCompany__c, 
        invoice2.c2g__Account__c, 
        invoice2.c2g__Period__c, 
        invoice2.c2g__InvoiceCurrency__c);
        salesCreditNote2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        salesCreditNotesToInsert.add(salesCreditNote);
        salesCreditNotesToInsert.add(salesCreditNote2);
        insert salesCreditNotesToInsert;

        List<c2g__codaCreditNoteLineItem__c> creditNoteLineItemToInsert = new List<c2g__codaCreditNoteLineItem__c>();
        c2g__codaCreditNoteLineItem__c creditNoteLineItem = IBA_TestDataFactory.createSalesCreditNoteLineItem(false,
            MONEY,
            invoice.c2g__OwnerCompany__c,
            salesCreditNote.Id,
            product.Id);

        c2g__codaCreditNoteLineItem__c creditNoteLineItem2 = IBA_TestDataFactory.createSalesCreditNoteLineItem(false,
            MONEY2,
            invoice2.c2g__OwnerCompany__c,
            salesCreditNote2.Id,
            product.Id);

        creditNoteLineItemToInsert.add(creditNoteLineItem);
        creditNoteLineItemToInsert.add(creditNoteLineItem2);
        insert creditNoteLineItemToInsert;

        c2g__codaJournal__c journal = IBA_TestDataFactory.createJournal(true, 
            invoice.c2g__OwnerCompany__c, 
            policyTransactions[0].Id, 
            invoice.c2g__Period__c, 
            invoice.c2g__InvoiceCurrency__c);
        c2g__codaJournalLineItem__c journalLineItem = IBA_TestDataFactory.createJournalLineItem(true,
            1,
            MONEY3, 
            gla.Id, 
            journal.Id,
            IBA_UtilConst.JOURNAL_LINE_TYPE_AC,
            acc.Id);
        Set<Id> journalIds = new Set<Id>();
        journalIds.add(journal.Id);

        List<c2g__codaPurchaseInvoice__c> purchaseInvoiceToInsert = new List<c2g__codaPurchaseInvoice__c>(); 
        c2g__codaPurchaseInvoice__c purchaseInvoice = IBA_TestDataFactory.createPurchaseInvoice(false, 
        invoice.c2g__OwnerCompany__c, 
        invoice.c2g__Account__c, 
        invoice.c2g__Period__c, 
        invoice.c2g__InvoiceCurrency__c, 
        invoice.Id);
        purchaseInvoice.IBA_PolicyTransaction__c = policyTransactions[0].Id;
        c2g__codaPurchaseInvoice__c purchaseInvoice2 = IBA_TestDataFactory.createPurchaseInvoice(false, 
        invoice2.c2g__OwnerCompany__c, 
        invoice2.c2g__Account__c, 
        invoice2.c2g__Period__c, 
        invoice2.c2g__InvoiceCurrency__c, 
        invoice2.Id);
        purchaseInvoice2.IBA_PolicyTransaction__c = policyTransactions[1].Id;

        purchaseInvoiceToInsert.add(purchaseInvoice);
        purchaseInvoiceToInsert.add(purchaseInvoice2);
        insert purchaseInvoiceToInsert;

        List<c2g__codaPurchaseInvoiceExpenseLineItem__c> purchaseInvoiceExpenseLineItemList = new List<c2g__codaPurchaseInvoiceExpenseLineItem__c>();
        c2g__codaPurchaseInvoiceExpenseLineItem__c purchaseInvoiceExpenseLineItem = IBA_TestDataFactory.createPurchaseInvoiceExpenseLineItem(false,
            MONEY,
            invoice.c2g__OwnerCompany__c, 
            gla.Id, 
            purchaseInvoice.Id);
        c2g__codaPurchaseInvoiceExpenseLineItem__c purchaseInvoiceExpenseLineItem2 = IBA_TestDataFactory.createPurchaseInvoiceExpenseLineItem(false,
            MONEY2,
            invoice2.c2g__OwnerCompany__c, 
            gla.Id, 
            purchaseInvoice2.Id);

        purchaseInvoiceExpenseLineItemList.add(purchaseInvoiceExpenseLineItem);
        purchaseInvoiceExpenseLineItemList.add(purchaseInvoiceExpenseLineItem2);
        insert purchaseInvoiceExpenseLineItemList;

        List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();

        IBA_FFPolicyCarrierLine__c policyCarrierLine = IBA_TestDataFactory.createPolicyCarrierLine(false, policyTransactions[0].Id, acc.Id);
        policyCarrierLine.IBA_CarrierPayableInvoice__c = purchaseInvoice.Id;
        IBA_FFPolicyCarrierLine__c policyCarrierLine2 = IBA_TestDataFactory.createPolicyCarrierLine(false, policyTransactions[0].Id, acc.Id);
        policyCarrierLine2.IBA_CarrierPayableCreditNote__c = purchaseCreditNote.Id;
        policyCarrierLines.add(policyCarrierLine);
        policyCarrierLines.add(policyCarrierLine2);

        insert policyCarrierLines;

        Integer i = 0;
        for(IBA_PolicyTransaction__c policyTransaction :policyTransactions) {
            if(i == 0) {
                policyTransaction.IBA_AccountingBrokerSIN__c = invoice.Id;
                policyTransaction.IBA_AccountingBrokerSCN__c = salesCreditNote.Id;
            } else if(i == 1) {
                policyTransaction.IBA_BrokerSalesInvoice__c = invoice2.Id;
                policyTransaction.IBA_MGAPayableInvoice__c = purchaseInvoice2.Id;
                policyTransaction.IBA_BrokerSalesCreditNote__c = salesCreditNote2.Id;
                policyTransaction.IBA_MGAPayableCreditNote__c = purchaseCreditNote2.Id;
            }
            i++;
        }
        update policyTransactions;
        
        Set<Id> invoiceIds = new Set<Id>();
        invoiceIds.add(invoice.Id);
        invoiceIds.add(invoice2.Id);
        IBA_TestDataFactory.postInvoices(invoiceIds);
        Set<Id> cashEntryIds = new Set<Id>();
        c2g__codaCashEntry__c cashEntry = IBA_TestDataFactory.createCashEntry(true, bank.Id, period.Id, curr.Id);
        c2g__codaCashEntry__c cashEntry2 = IBA_TestDataFactory.createCashEntry(true, bank.Id, period.Id, curr.Id);
        IBA_TestDataFactory.createCashEntryLineItem(true, acc.Id, MONEY, cashEntry.Id);
        IBA_TestDataFactory.createCashEntryLineItem(true, acc.Id, MONEY2, cashEntry2.Id);

        cashEntryIds.add(cashEntry.Id);
        cashEntryIds.add(cashEntry2.Id);
        IBA_TestDataFactory.postCashEntries(cashEntryIds);
        Test.stopTest();

    }

    @isTest
    static void afterInsertTest() {
        List<IBA_PolicyTransaction__c> ptList = [SELECT Id, IBA_AccountingBrokerSIN__c, IBA_BrokerSalesInvoice__c, IBA_AccountingBrokerSCN__c, IBA_MGAPayableInvoice__c,
            IBA_BrokerSalesCreditNote__c, IBA_MGAPayableCreditNote__c FROM IBA_PolicyTransaction__c];
        List<c2g__codaTransaction__c> transactions = new List<c2g__codaTransaction__c>();
        Integer i = 0;
        for(c2g__codaTransaction__c trans :[SELECT Id, c2g__TransactionType__c, IBA_PolicyTransaction__c FROM c2g__codaTransaction__c]) {
            if(trans.c2g__TransactionType__c == IBA_UtilConst.TRANSACTION_TYPE_CASH) {
                if(i < 2) {
                    trans.IBA_PolicyTransaction__c = ptList[0].Id;
                    i++;
                } else {
                    trans.IBA_PolicyTransaction__c = ptList[1].Id;
                }
            } else if(trans.c2g__TransactionType__c == IBA_UtilConst.TRANSACTION_TYPE_INVOICE) {
                if(i < 2) {
                    trans.IBA_PolicyTransaction__c = ptList[0].Id;
                    i++;
                } else {
                    trans.IBA_PolicyTransaction__c = ptList[1].Id;
                }
            }
            transactions.add(trans);
        }
        update transactions;

        c2g__codaInvoice__c invoice = [SELECT Id, c2g__Transaction__r.c2g__TransactionType__c, c2g__Transaction__r.IBA_PolicyTransaction__c, c2g__Account__c, c2g__Transaction__c, c2g__Period__c FROM c2g__codaInvoice__c LIMIT 1];

        Map<Id, c2g__codaTransactionLineItem__c> tliMap = new Map<Id, c2g__codaTransactionLineItem__c>();
        
        for(c2g__codaTransactionLineItem__c transactionLineItem :[SELECT Id, c2g__Transaction__r.c2g__TransactionType__c, c2g__Transaction__r.c2g__TransactionDate__c, c2g__Transaction__r.IBA_PolicyTransaction__c FROM c2g__codaTransactionLineItem__c WHERE c2g__LineType__c = :IBA_UtilConst.TRANSACTION_LINE_ITEM_TYPE_ACCOUNT]) {
            tliMap.put(transactionLineItem.Id, transactionLineItem);
        }

        Test.startTest();
        IBA_TestDataFactory.matchCash(invoice.c2g__Account__c, invoice.c2g__Period__c, tliMap.keySet());
        Test.stopTest();
        IBA_PolicyTransaction__c accBrokerPT = new IBA_PolicyTransaction__c();
        IBA_PolicyTransaction__c brokerPT = new IBA_PolicyTransaction__c();
        for(IBA_PolicyTransaction__c policyTransaction :[SELECT Id, IBA_AccountingBrokerOutstanding__c, IBA_BrokerOutstanding__c,
            IBA_BrokerSalesCreditNote__c, IBA_AccountingBrokerSCN__c, IBA_AccountingBrokerInvoicePaidDate__c, IBA_BrokerInvoicePaidDate__c, 
            IBA_OwedtoCarrier__c, IBA_OwedtoMGA__c, IBA_MGAPayableInvoice__c FROM IBA_PolicyTransaction__c]) {
                if(policyTransaction.IBA_BrokerSalesCreditNote__c != null) {
                    brokerPT = policyTransaction;
                } else if(policyTransaction.IBA_AccountingBrokerSCN__c != null) {
                    accBrokerPT = policyTransaction;
                }
        }

        System.assertEquals(20.49, accBrokerPT.IBA_AccountingBrokerOutstanding__c);
        System.assertEquals(80.01, brokerPT.IBA_BrokerOutstanding__c);

        System.assertEquals(System.today(), accBrokerPT.IBA_AccountingBrokerInvoicePaidDate__c);
        
    }
}