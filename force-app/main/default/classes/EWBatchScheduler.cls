// v2: ajedamenko@azuruw.com - 01/10/2020 - new constructor method and private property batchSize added to allow execute batch jobs with a defined batch size
global class EWBatchScheduler implements Schedulable{
    private Database.Batchable<sObject> batchToSchedule;
    private Integer batchSize;

    public EWBatchScheduler(Database.Batchable<sObject> bch) {
        this.batchToSchedule = bch;
    }

    public EWBatchScheduler(Database.Batchable<sObject> bch, Integer bchSize) {
        this.batchToSchedule = bch;
        this.batchSize = bchSize;
    }

    global void execute(SchedulableContext sc){
        if (batchSize == null) {
            
            Database.executeBatch(batchToSchedule);
        } else {

            Database.executeBatch(batchToSchedule, batchSize);
        }    
    }
}