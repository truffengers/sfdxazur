/**
* @name: RiskTriggerHandler
* @description: Handles actions from Risk trigger. It's without sharing so users can update/create child risks
* without write access to parents.
* @author: Antigoni Tsouri atsouri@azuruw.com
*
*/
public without sharing class RiskTriggerHandler
{
	public static boolean RECALCULATE_RISK = false;

	public static void handleRecords(List<Risk__c> risks, Map<Id, Risk__c> riskMap, String dmlAction)
	{
		Set<Id> parentIds = new Set<Id>();

		for(Risk__c r: risks) {
			if(dmlAction == 'insert') {
				if(r.Parent_Risk__c != null) {
					parentIds.add(r.Parent_Risk__c);
				}
			}
			if(dmlAction == 'delete') {
				if(r.Parent_Risk__c != null) {
					parentIds.add(r.Parent_Risk__c);
				}
			}
			if(dmlAction == 'update') {
				if(isParentReparented(r, riskMap) || isScoreChanged(r, riskMap) || RECALCULATE_RISK) {
					parentIds.add(r.Parent_Risk__c);
					parentIds.add(riskMap.get(r.Id).Parent_Risk__c);
				}
			}
		}
		if(parentIds.size() > 0) {
			recalculateParentValues(parentIds);
		}
	}

	private static boolean isParentReparented(Risk__c r, Map<Id, Risk__c> riskMap){
		return r.Parent_Risk__c != null
				&& r.Parent_Risk__c != riskMap.get(r.Id).Parent_Risk__c
				&& riskMap.get(r.Id).Parent_Risk__c != null;
	}

	private static boolean isScoreChanged(Risk__c r, Map<Id, Risk__c> riskMap){
		return     r.Gross_Risk_Impact__c != riskMap.get(r.Id).Gross_Risk_Impact__c
				|| r.Gross_Risk_Likelihood__c != riskMap.get(r.Id).Gross_Risk_Likelihood__c
				|| r.Net_Risk_Impact__c != riskMap.get(r.Id).Net_Risk_Impact__c
				|| r.Net_Risk_Likelihood__c != riskMap.get(r.Id).Net_Risk_Likelihood__c
				|| r.Average_Gross_Risk_Impact__c != riskMap.get(r.Id).Average_Gross_Risk_Impact__c
				|| r.Average_Gross_Risk_Likelihood__c != riskMap.get(r.Id).Average_Gross_Risk_Likelihood__c
				|| r.Average_Net_Risk_Impact__c != riskMap.get(r.Id).Average_Net_Risk_Impact__c
				|| r.Average_Net_Risk_Likelihood__c != riskMap.get(r.Id).Average_Net_Risk_Likelihood__c
				|| r.Active__c != riskMap.get(r.Id).Active__c;
	}

	public static void recalculateParentValues(Set<Id> parentRiskIds)
	{
		List<Risk__c> parentRisks = [Select Id,
		(Select Gross_Risk_Impact__c, Gross_Risk_Likelihood__c,Average_Net_Risk_Impact__c,Average_Net_Risk_Likelihood__c,
				Net_Risk_Impact__c, Net_Risk_Likelihood__c, Average_Gross_Risk_Likelihood__c,
				Average_Gross_Risk_Impact__c From Risks__r WHERE Active__c = TRUE)
		From Risk__c
		Where Id in: parentRiskIds];

		Double riskImpactSum, riskLikelihoodSum, netRiskImpactSum, netRiskLikelihoodSum;
		Integer numberOfChildren;
		// Looping through all parent risks and calculating all the scores from scratch, summarizing them from children Risks.
		for(Risk__c pRisk: parentRisks) {
			numberOfChildren = 0;
			riskImpactSum = 0;
			riskLikelihoodSum = 0;
			netRiskImpactSum = 0;
			netRiskLikelihoodSum = 0;
			for(Risk__c cRisk: pRisk.Risks__r) {
				system.debug('===> cRisk: '+cRisk);

				numberOfChildren += 1;
				riskImpactSum += cRisk.Gross_Risk_Impact__c == null ? cRisk.Average_Gross_Risk_Impact__c : cRisk.Gross_Risk_Impact__c;
				system.debug('===> riskImpactSum: '+riskImpactSum);
				riskLikelihoodSum += cRisk.Gross_Risk_Likelihood__c == null ? cRisk.Average_Gross_Risk_Likelihood__c : cRisk.Gross_Risk_Likelihood__c;
				netRiskImpactSum += cRisk.Net_Risk_Impact__c == null ? cRisk.Average_Net_Risk_Impact__c : cRisk.Net_Risk_Impact__c;
				netRiskLikelihoodSum += cRisk.Net_Risk_Likelihood__c == null ? cRisk.Average_Net_Risk_Likelihood__c : cRisk.Net_Risk_Likelihood__c;

			}
			pRisk.Average_Gross_Risk_Impact__c = (numberOfChildren > 0) ? riskImpactSum / numberOfChildren : 0;
			system.debug('===> riskImpactSum: '+pRisk.Average_Gross_Risk_Impact__c);
			pRisk.Average_Gross_Risk_Likelihood__c = (numberOfChildren > 0) ? riskLikelihoodSum / numberOfChildren : 0;
			pRisk.Average_Net_Risk_Impact__c = (numberOfChildren > 0) ? netRiskImpactSum / numberOfChildren : 0;
			pRisk.Average_Net_Risk_Likelihood__c = (numberOfChildren > 0) ? netRiskLikelihoodSum / numberOfChildren : 0;
			pRisk.Number_of_children__c = numberOfChildren;
		}
		update parentRisks;
	}

}