/**
* @name: EWVlocityRemoteActionHelperTest
* @description: Test class for EWVlocityRemoteActionHelper
* @author: Steve Loftus sloftu@azuruw.com
* @date: 04/09/2018
*/
@isTest
public class EWVlocityRemoteActionHelperTest {
	public EWVlocityRemoteActionHelperTest() {
		
	}

    @isTest
    public static void unknownMethodTest() {

        Test.startTest();

        EWVlocityRemoteActionHelper vlocityRemoteActionHelper = new EWVlocityRemoteActionHelper();
        Boolean result = vlocityRemoteActionHelper.invokeMethod('unknownMethod', null, null, null);

        Test.stopTest();

        System.assertEquals(false, result, 'method should not exist');
    }


    // generic test method of other methods
    public static Boolean testRemoteActionMethod(String methodName) {

        EWVlocityRemoteActionHelper vlocityRemoteActionHelper = new EWVlocityRemoteActionHelper();
        return vlocityRemoteActionHelper.invokeMethod(methodName, new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
    }

    private static testMethod void testGetFeatureSwitches(){

        EWFeatureSwitches__c newSetting = new EWFeatureSwitches__c(SetupOwnerId=UserInfo.getOrganizationId(), InsuredDuplication__c = false, SanctionChecking__c = true);
        insert newSetting;

        Test.startTest();
        Boolean runMethod = testRemoteActionMethod('getFeatureSwitches');
        Map<String,Object> outputMap = EWVlocityRemoteActionHelper.getFeatureSwitches(new Map<String, Object>(), new Map<String, Object>(), new Map<String, Object>());
        Test.stopTest();

        System.assertEquals(true, runMethod);
        System.assertEquals(true, outputMap.containsKey('featureSwitches'));
        EWFeatureSwitches__c checkSetting = (EWFeatureSwitches__c) outputMap.get('featureSwitches');
        System.assertEquals(newSetting.SanctionChecking__c, checkSetting.SanctionChecking__c);

    }

    private static testMethod void testDeleteStandardClauseAssignments() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        Map<String, Object> optionMap = new Map<String, Object>();
        optionMap.put('quoteId', quote.Id);

        vlocity_ins__DocumentClause__c testClause = new vlocity_ins__DocumentClause__c(Name = 'Flood Exclusion', vlocity_ins__Category__c = EWConstants.DOCUMENT_CLAUSE_CATEGORY_STANDARD);
        insert testClause;

        EW_ClauseAssignment__c clauseAssignment = new EW_ClauseAssignment__c(EW_DocumentClause__c = testClause.Id, EW_Quote__c = quote.Id);
        insert clauseAssignment;

        EWVlocityRemoteActionHelper vlocityRemoteActionHelper = new EWVlocityRemoteActionHelper();

        Test.startTest();
        Boolean result = vlocityRemoteActionHelper.invokeMethod('deleteStandardClauseAssignments', new Map<String, Object>(), new Map<String, Object>(), optionMap);
        Test.stopTest();

        System.assertEquals(true, result);
        System.assertEquals(0, [SELECT count() FROM EW_ClauseAssignment__c WHERE EW_Quote__c = :quote.Id]);
    }

    private static testMethod void testGetEffectiveDate() {

        String dateString = '2019-05-11 13:00:00';

        Map<String, Object> inputMap = (Map<String, Object>) JSON.deserializeUntyped('{"01t0D000000qQV4QAM_01t0D000000qQV4QAM":[{"quotedDate":"'+dateString+'"}]}');
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        Test.startTest();
        Boolean runMethod = testRemoteActionMethod('getEffectiveDate');
        outputMap = EWVlocityRemoteActionHelper.getEffectiveDate(inputMap , outputMap, optionMap);
        Test.stopTest();

        System.assertEquals(dateString, outputMap.get('effectiveDate'));

    }

    private static testMethod void testGetGlobals(){

        EWGlobals__c newGlobals = new EWGlobals__c();

        newGlobals.SetupOwnerId = UserInfo.getOrganizationId();
        newGlobals.ArtAndCollectablesLimit__c = 10000;
        newGlobals.Excess__c = 500;
        newGlobals.FloodDataURL__c = 'https://azur-integrations.herokuapp.com/ambId';
        newGlobals.JewelleryAndWatchesLimit__c = 10000;
        newGlobals.PedalCyclesLimit__c = 2000;
        newGlobals.PerilsScoreURL__c = 'https://azur-integrations.herokuapp.com/subsidence';
        newGlobals.PropertyDetailsURL__c = 'https://azur-integrations.herokuapp.com/outraUPRN';
        newGlobals.QuotePrefix__c = '88-EWA';
        newGlobals.SanctionsCheckURL__c = 'https://azur-integrations.herokuapp.com/sanctionCheck';
        newGlobals.PostcodeURL__c =	'https://azur-staging.herokuapp.com/ambPostcodes';
        newGlobals.PostcodePvParam__c = 'OSABS';

        insert newGlobals;

        Test.startTest();
        Boolean runMethod = testRemoteActionMethod('getGlobals');
        Map<String,Object> outputMap = EWVlocityRemoteActionHelper.getGlobals(
                                                                    new Map<String, Object>(),
                                                                    new Map<String, Object>(),
                                                                    new Map<String, Object>());
        Test.stopTest();

        System.assertEquals(true, runMethod);
        System.assertEquals(true, outputMap.containsKey('globals'));
        EWGlobals__c checkGlobals = (EWGlobals__c) outputMap.get('globals');
        System.assertEquals(newGlobals.ArtAndCollectablesLimit__c, checkGlobals.ArtAndCollectablesLimit__c);
        System.assertEquals(newGlobals.Excess__c, checkGlobals.Excess__c);
        System.assertEquals(newGlobals.FloodDataURL__c, checkGlobals.FloodDataURL__c);
        System.assertEquals(newGlobals.JewelleryAndWatchesLimit__c, checkGlobals.JewelleryAndWatchesLimit__c);
        System.assertEquals(newGlobals.PedalCyclesLimit__c, checkGlobals.PedalCyclesLimit__c);
        System.assertEquals(newGlobals.PerilsScoreURL__c, checkGlobals.PerilsScoreURL__c);
        System.assertEquals(newGlobals.PropertyDetailsURL__c, checkGlobals.PropertyDetailsURL__c);
        System.assertEquals(newGlobals.QuotePrefix__c, checkGlobals.QuotePrefix__c);
        System.assertEquals(newGlobals.SanctionsCheckURL__c, checkGlobals.SanctionsCheckURL__c);
        System.assertEquals(newGlobals.PostcodeURL__c, checkGlobals.PostcodeURL__c);
        System.assertEquals(newGlobals.PostcodePvParam__c, checkGlobals.PostcodePvParam__c);

    }

    private static testMethod void testJsonReserialize() {
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        inputMap.put('test', 'test');

        System.assert(outputMap.isEmpty());
        (new EWVlocityRemoteActionHelper()).invokeMethod('reserializeInputJson', inputMap, outputMap, optionMap);
        System.assert(!outputMap.isEmpty());
        System.assert(outputMap.containsKey('test'));
    }

    private static testMethod void testValidatePostcodeAvailability() {
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        inputMap.put('address', 'test, GU2 4HB');

        (new EWVlocityRemoteActionHelper()).invokeMethod('validatePostcodeAvailability', inputMap, outputMap, optionMap);
        System.assert(outputMap.containsKey('uw_excludedPostcode'));
        System.assert(!((Boolean) outputMap.get('uw_excludedPostcode')));

        inputMap.clear();
        outputMap.clear();

        inputMap.put('address', 'test, GY1 1BH');
        (new EWVlocityRemoteActionHelper()).invokeMethod('validatePostcodeAvailability', inputMap, outputMap, optionMap);
        System.assert(outputMap.containsKey('uw_excludedPostcode'));
        System.assert((Boolean) outputMap.get('uw_excludedPostcode'));
        System.assert(outputMap.containsKey('uw_excludedPostcodeMessage'));
    }


    private static testMethod void testUpdateQuoteRecord() {
        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);

        Quote quoteCheck = [SELECT Status FROM Quote WHERE Id = :quote.Id];
        System.assertNotEquals(EWConstants.QUOTE_STATUS_UW_APPROVED, quoteCheck.Status);

        Test.startTest();
        optionMap.put('Id',quote.Id);
        optionMap.put('Status', EWConstants.QUOTE_STATUS_UW_APPROVED);
        new EWVlocityRemoteActionHelper().invokeMethod('updateQuoteRecord', inputMap, outputMap, optionMap);
        Test.stopTest();

        quoteCheck = [SELECT Status FROM Quote WHERE Id = :quote.Id];
        System.assertEquals(EWConstants.QUOTE_STATUS_UW_APPROVED, quoteCheck.Status);

    }
    private static testMethod void testCreateContentNote(){

        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                true
        );

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);

        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{
                'contentString' => 'Hello World',
                'contentTitle' => 'Boo',
                'recordId' => quote.Id
        };

        new EWVlocityRemoteActionHelper().invokeMethod('createContentNote', inputMap, outputMap, optionMap);

        System.assertEquals(1, [SELECT Count() FROM ContentDocumentLink WHERE LinkedEntityId = :quote.Id]);

    }
    
    /**
    * @methodName: testSetConfigurePolicyGRP
    * @description: Test method for EWVlocityRemoteActionHelper.setConfigurePolicy method
    * @author: Roy Lloyd
    * @dateCreated: 18/2/2020
    */
    private static testMethod void testSetConfigurePolicyGRP(){
        // tests getRatedProducts is in the records output when capAndTrap has no records

        Object records = JSON.deserializeUntyped('[{"a":"aa", "b":"bb"}, {"c":"cc", "d":"dd"}]');
        Map<String,Object> getRatedProducts = new Map<String,Object>{'records' => records};
        Map<String,Object> capAndTrap = new Map<String,Object>();

        Map<String, Object> inputMap = new Map<String, Object>{'capAndTrap' => capAndTrap, 'getRatedProducts' => getRatedProducts};
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();
        new EWVlocityRemoteActionHelper().invokeMethod('setConfigurePolicy', inputMap, outputMap, optionMap);

        System.assertEquals(records, outputMap.get('records'));
    }

    /**
    * @methodName: testSetConfigurePolicyCAT
    * @description: Test method for EWVlocityRemoteActionHelper.setConfigurePolicy method
    * @author: Roy Lloyd
    * @dateCreated: 18/2/2020
    */
    private static testMethod void testSetConfigurePolicyCAT(){
        // tests capAndTrap is in the records output, irrespective of the GRP output

        Object records = JSON.deserializeUntyped('[{"a":"aa", "b":"bb"}, {"c":"cc", "d":"dd"}]');
        Object recordsGRP = JSON.deserializeUntyped('[{"e":"ee", "f":"ff"}]');
        Map<String,Object> getRatedProducts = new Map<String,Object>{'records' => records};
        Map<String,Object> capAndTrap = new Map<String,Object>{'getRatedProducts' => getRatedProducts};

        Map<String, Object> inputMap = new Map<String, Object>{'capAndTrap' => capAndTrap, 'getRatedProducts' => recordsGRP};
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();

        new EWVlocityRemoteActionHelper().invokeMethod('setConfigurePolicy', inputMap, outputMap, optionMap);

        System.assertEquals(records, outputMap.get('records'));
    }

    /**
    * @methodName: testPostSObject
    * @description: Test method for EWVlocityRemoteActionHelper.postSObject method
    * @author: Roy Lloyd
    * @dateCreated: 25/2/2020
    */
    private static testMethod void testPostSObject(){

        Object o= JSON.deserializeUntyped('{"JSON":"Blurb"}');

        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{
                'objectName' => 'Azur_Log__c',
                'Class__c' => 'Quote Omniscript',
                'Log_description__c' => 'Renewal',
                'Method__c' => 'getRatedProducts',
                'Log_message__c' => o,
                'abc' => 'defg'
        };

        new EWVlocityRemoteActionHelper().invokeMethod('postSObject', inputMap, outputMap, optionMap);
        System.assertEquals(1, [SELECT Count() FROM Azur_Log__c WHERE Class__c = 'Quote Omniscript']);

    }

    /**
    * @methodName: getOccupationsSetup
    * @methodName: getTypeaheadItemsSetup
    * @description: Setup method for testGetTypeaheadItemsShort and testGetTypeaheadItemsLong below
    * @author: Andrew Xu
    * @dateCreated: 27/2/2020
    */
    private static List<Picklist_Section__c> getTypeaheadItemsSetup() {
        List<Picklist_Section__c> testOccupations = new List<Picklist_Section__c>();

        Map<Integer, String> differentOccupations = new Map<Integer, String> {
            1 => 'Aromatherapist',
            2 => 'Animal Sitter',
            3 => 'Dealer',
            4 => 'Coin Dealer',
            5 => 'Scrap Metal Dealer',
            6 => 'Aromatherapeutic Oils Dealer'
        };

        for (String name : differentOccupations.values()) {
            Picklist_Section__c occupation = new Picklist_Section__c(Name = name);
            testOccupations.add(occupation);
        }

        return testOccupations;
    }

    /**
    * @methodName: testGetTypeaheadItemsShort
    * @description: One of two test methods for EWVlocityRemoteActionHelper.getTypeaheadItems method
    * @author: Andrew Xu
    * @dateCreated: 27/2/2020
    */
    private static testMethod void testGetTypeaheadItemsShort() {
        List<Picklist_Section__c> testOccupations = getTypeaheadItemsSetup();
        insert testOccupations;

        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();
        optionMap.put('searchString', 'a');
        optionMap.put('recordTypeId', '01258000000FR75AAG');
        optionMap.put('insuredType', 'primary');

        Test.startTest();
        new EWVlocityRemoteActionHelper().invokeMethod('getTypeaheadItems', inputMap, outputMap, optionMap);
        Test.stopTest();

        List<Object> results = (List<Object>) outputMap.get('OBDRresp');

        Set<String> occupationResults = new Set<String>();
        for(Object o : results){
            Map<String, Object> result = (Map<String, Object>) o;
            occupationResults.add((String) result.get('typeaheadName'));
        }

        System.assertEquals(true, occupationResults.contains('Aromatherapist'));
        System.assertEquals(true, occupationResults.contains('Animal Sitter'));
        System.assertEquals(true, occupationResults.contains('Aromatherapeutic Oils Dealer'));

        Integer numberOfResults = results.size();
        System.assertEquals(3, numberOfResults);
    }

    /**
    * @methodName: testGetTypeaheadItemsLong
    * @description: One of two test methods for EWVlocityRemoteActionHelper.getTypeaheadItems method
    * @author: Andrew Xu
    * @dateCreated: 27/2/2020
    */
    private static testMethod void testGetTypeaheadItemsLong() {
        List<Picklist_Section__c> testOccupations = getTypeaheadItemsSetup();
        insert testOccupations;

        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>();
        optionMap.put('searchString', 'Dealer');
        optionMap.put('recordTypeId', '01258000000FR75AAG');
        optionMap.put('insuredType', 'primary');

        Test.startTest();
        new EWVlocityRemoteActionHelper().invokeMethod('getTypeaheadItems', inputMap, outputMap, optionMap);
        Test.stopTest();

        List<Object> results = (List<Object>) outputMap.get('OBDRresp');

        Set<String> occupationResults = new Set<String>();
        for(Object o : results){
            Map<String, Object> result = (Map<String, Object>) o;
            occupationResults.add((String) result.get('typeaheadName'));
        }

        System.assertEquals(true, occupationResults.contains('Dealer'));
        System.assertEquals(true, occupationResults.contains('Coin Dealer'));
        System.assertEquals(true, occupationResults.contains('Scrap Metal Dealer'));
        System.assertEquals(true, occupationResults.contains('Aromatherapeutic Oils Dealer'));

        Integer numberOfResults = results.size();
        System.assertEquals(4, numberOfResults);
    }
    
    /**
    * @methodName: testSendEmail
    * @description: Test method for EWVlocityRemoteActionHelper.sendEmail method
    * @author: Aleksejs Jedamenko
    * @dateCreated: 27/03/2020
    */
    @isTest
    private static void testSendEmail() {
    	
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        User systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
        							EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
        							techteamUserRole.Id,
        							false);
		systemAdminUser.Email = 'adminTest@test.com';

		insert systemAdminUser;

        System.runAs(systemAdminUser) {

			Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
            Account insured = EWAccountDataFactory.createInsuredAccount(
                    'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
            );
            insert new List<Account>{
                    broker, insured
            };
            EW_New_Broker_Requests__c brokerRequest = new EW_New_Broker_Requests__c();
			brokerRequest.SetupOwnerId = systemAdminUser.Id;
			insert brokerRequest;
        	Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        	Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);	
            Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
            policy.EW_IsStandardTemplateForRenewal__c = false;
            insert policy;         
			EmailTemplate emailTemplate = EWEmailDataFactory.createTextEmailTemplate(null, null, systemAdminUser.Id, true);
			Attachment emailAttachment = EWAttachmentDataFactory.createAttachment(null, quote.Id, true);
			EWEmailHelper.EmailOptions  emailOptions = new EWEmailHelper.EmailOptions(
												systemAdminUser.Id,
												emailTemplate.DeveloperName,
												new List<Id>{emailAttachment.Id},
												false,
												quote.Id);
            Map<String, Object> inputMap = new Map<String, Object>();
            Map<String, Object> outputMap = new Map<String, Object>();
            Map<String, Object> optionMap = new Map<String, Object>();
            optionMap.put('osAttachmentList', new List<Id>{emailAttachment.Id});
            optionMap.put('saveAsActivity', false);
            optionMap.put('targetObjectId', systemAdminUser.Id);
            optionMap.put('templateName', emailTemplate.DeveloperName);
            optionMap.put('whatId', quote.Id);
            optionMap.put('typeOfTransaction', 'Renewal');
            optionMap.put('policyId', policy.Id);
            Test.startTest();
            new EWVlocityRemoteActionHelper().invokeMethod('sendEmail', inputMap, outputMap, optionMap);
            Test.stopTest();
         	Asset policyAfter = [SELECT Id, EW_IsStandardTemplateForRenewal__c FROM Asset LIMIT 1];
            system.debug('policyAfter value '+policyAfter);
            system.assert(policyAfter.EW_IsStandardTemplateForRenewal__c);
            system.assert(outputMap.containsKey('emailResults'));
        }
    }
    /**
    * @methodName: testCreatePDF
    * @description: Test method for EWVlocityRemoteActionHelper.createPDF, runs under @isTest(SeeAllData=true), because a creation of proper DocumentTemplate for testing is problematic   
    * @author: Aleksejs Jedamenko
    * @dateCreated: 15/04/2020
    */
    @isTest(SeeAllData=true)
    private static void testCreatePDF() {
        
        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false);
        insert new List<Account>{broker, insured};
        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);	
        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        insert policy; 
        List<vlocity_ins__Document__c> testDocs = new List<vlocity_ins__Document__c>();
        vlocity_ins__DocumentTemplate__c policyDocTemlate = [SELECT Id FROM vlocity_ins__DocumentTemplate__c WHERE Name = :EWConstants.DOCUMENT_TEMPLATE_POLICY LIMIT 1];
        vlocity_ins__DocumentTemplate__c quoteDocTemplate = [SELECT Id FROM vlocity_ins__DocumentTemplate__c WHERE Name = :EWConstants.DOCUMENT_TEMPLATE_QUOTE LIMIT 1];
        vlocity_ins__Document__c testPolicyDoc = new vlocity_ins__Document__c (vlocity_ins__DocumentCreationSource__c = 'Generate', 
                                                                              vlocity_ins__DocumentTemplateId__c = policyDocTemlate.Id,
                                                                              vlocity_ins__ObjectId__c = 'CreateDoc',
                                                                              EW_Policy__c = policy.Id,
                                                                              vlocity_ins__Status__c = 'Active');
        vlocity_ins__Document__c testQuoteDoc = new vlocity_ins__Document__c (vlocity_ins__DocumentCreationSource__c = 'Generate', 
                                                                              vlocity_ins__DocumentTemplateId__c = quoteDocTemplate.Id,
                                                                              vlocity_ins__ObjectId__c = 'CreateDoc',
                                                                              EW_Quote__c = quote.Id,
                                                                              vlocity_ins__Status__c = 'Active');
        insert new List<vlocity_ins__Document__c> {testPolicyDoc, testQuoteDoc};
        Test.startTest();
        Map<String, Object> policyFlowOptionMap = new Map<String, Object>{'docId' => testPolicyDoc.Id};
        Map<String, Object> quoteFlowOptionMap = new Map<String, Object>{'docId' => testQuoteDoc.Id};
        Map<String, Object> errorFlowOptionMap = new Map<String, Object>{'docId' => null};
        EWVlocityRemoteActionHelper.createPDF(new Map<String, Object>(),policyFlowOptionMap);
        EWVlocityRemoteActionHelper.createPDF(new Map<String, Object>(),quoteFlowOptionMap);
        EWVlocityRemoteActionHelper.createPDF(new Map<String, Object>(),errorFlowOptionMap);
        Test.stopTest();
        List<Attachment> policyAttachments = [SELECT Id FROM Attachment WHERE ParentId = :policy.Id];
       	List<Attachment> quoteAttachments = [SELECT Id FROM Attachment WHERE ParentId = :quote.Id];
        system.assertEquals(1,policyAttachments.size());
        //system.assertEquals(1,quoteAttachments.size());       
    }
}