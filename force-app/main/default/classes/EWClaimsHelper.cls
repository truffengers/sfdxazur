/**
* @name: EWClaimsHelper
* @description: Helper class for claim scoring
* @author: Roy Lloyd rlloyd@azuruw.com
* @date: 29/10/2018
* 16/1/2018 Roy Lloyd   Amended to replace custom Claim fields with Vlocity fields
*/

global with sharing class EWClaimsHelper {

    private static final Integer CLAIM_YEAR_LIMIT = 5;
    private static final Integer SCORE_PRECISION = 5; // decimal places used for scoring
    @TestVisible
    private static Map<String, InputClaim> scoredClaimMap = new Map<String, InputClaim>();
    private static Integer ignoredClaimCount;
    public static final String CLAIMS_LIST_JSON_KEY = 'editBlockClaimsHistory';
    public static final String CLAIMS_LIST_READONLY_JSON_KEY = 'editBlockClaimsHistoryReadOnly';
    public static final String CLAIM_ID_JSON_KEY = 'claimId';
    public static final String PROPERTY_DETAILS_JSON_KEY = 'propertyDetails';
    public static Boolean disableSettingAzurClaimFlag;

    public EWClaimsHelper() {
    }

    public static Map<String,Object> calculateClaimScore(Map<String,Object> inputMap) {
        // called by the EWVlocityRemoteActionHelper

        if(inputMap.isEmpty()){
            return new Map<String,Object>();
        }

        // get the editBlockClaimsHistory / editBlockClaimsHistoryReadOnly
        Map<String,Object> editBlockClaimsHistory = getClaimsHistory(inputMap);

        // calculate the scoring per claim
        Map<String,Double> calculatedScores = reviewIndividualClaims(editBlockClaimsHistory);

        // calculate the total scoring per peril
        Map<String,Object> allScores = reviewAllScores(calculatedScores);

        return allScores;
    }


    public static void postClaims(Map<String,Object> inputMap) {
        // called by the EWVlocityRemoteActionHelper

        if(inputMap.isEmpty()){
            return;
        }
        System.debug('postClaims inputMap = ' + inputMap);

        Map<String,Object> inputClaims = (Map<String,Object>) inputMap.get('claimScores');
        String quoteTransactionType = (String) inputMap.get('quoteTransactionType');
        Set<String> transactionsWithNoClaimTrigger = new Set<String>{
                EWConstants.QUOTE_TRANSACTION_TYPE_MTA,
                EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL
        };

        List<InputClaim> claims = getClaims(inputClaims, 'scoredClaims');
        String quoteId = (String) inputClaims.get('quoteId');
        System.debug('QuoteId = '+ quoteId);

        Map<String, vlocity_ins__InsuranceClaim__c> claimRecords = new Map<String, vlocity_ins__InsuranceClaim__c>();

        for (InputClaim claim : claims) {
            String mapKey = claim.claimName + claim.claimDateText + claim.textClaimValue + claim.selectClaimType;
            vlocity_ins__InsuranceClaim__c claimRecord = createClaimRecord(claim, quoteId);
            claimRecords.put(mapKey, claimRecord);
        }

        List<InputClaim> allClaims = getClaims(inputClaims, 'allClaims');
        for (InputClaim claim : allClaims) {
            if (claim != null) {
                String mapKey = claim.claimName + claim.claimDateText + claim.textClaimValue + claim.selectClaimType;
                if (!claimRecords.containsKey(mapKey)) {
                    vlocity_ins__InsuranceClaim__c claimRecord = createClaimRecord(claim, quoteId);
                    claimRecords.put(mapKey, claimRecord);
                }
            }
        }

        if (!claimRecords.isEmpty()) {
            if(transactionsWithNoClaimTrigger.contains(quoteTransactionType)) disableSettingAzurClaimFlag = true;  // to prevent the Azur Claim being ticked inappropriately (EWH-1251, March 2020)

            // upsert declared claims
            upsert claimRecords.values();
        }
    }

    /**
    * @methodName: getClaimsHistory
    * @description: retrieves the values from the Quote Omniscript JSON nodes 'editBlockClaimsHistory' & 'editBlockClaimsHistoryReadOnly'.  Read Only values are displayed in Renewal quotes.
    * @returns: merged values as a single node 'editBlockClaimsHistory'
    * @author: Roy Lloyd
    * @date: 7/2/2020
    */
    private static Map<String,Object> getClaimsHistory(Map<String,Object> inputMap){

        Map<String,Object> editBlockClaimsHistory = new Map<String,Object>();
        Map<String,Object> propertyDetails = (Map<String,Object>) inputMap.get(PROPERTY_DETAILS_JSON_KEY);

        Object readOnlyClaimsObj = propertyDetails.containsKey(CLAIMS_LIST_READONLY_JSON_KEY)
                ? propertyDetails.get(CLAIMS_LIST_READONLY_JSON_KEY)
                : null;

        if(readOnlyClaimsObj != null){

            if (readOnlyClaimsObj instanceof Map<String, Object>) {

                Map<String,Object> readOnlyClaim = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(readOnlyClaimsObj));
                editBlockClaimsHistory.put(
                        CLAIMS_LIST_JSON_KEY,
                        renameKey(readOnlyClaim , 'RO')
                );
            }

            if (readOnlyClaimsObj instanceof List<Object>) {

                List<Object> editBlockClaims = new List<Object>();
                for (Object readOnlyClaimObj : (List<Object>) readOnlyClaimsObj) {
                    Map<String,Object> readOnlyClaim = (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(readOnlyClaimObj));
                    editBlockClaims.add( (Object) renameKey (readOnlyClaim , 'RO'));
                }
                editBlockClaimsHistory.put(CLAIMS_LIST_JSON_KEY, editBlockClaims);
            }

        }else if(propertyDetails.containsKey(CLAIMS_LIST_JSON_KEY)){
            editBlockClaimsHistory.put(CLAIMS_LIST_JSON_KEY, propertyDetails.get(CLAIMS_LIST_JSON_KEY));
        }

        return editBlockClaimsHistory;
    }


    /**
    * @methodName: renameKey
    * @description: method used to rename keys in a Map<String,Object>
    * @author: Roy Lloyd
    * @date: 7/2/2020
    */
    private static Map<String,Object> renameKey (Map<String,Object> incomingMap , String removeStringFromKey ){

        Map<String,Object> returnMap = new Map<String,Object>();
        for(String key : incomingMap.keySet()) {
            String newKey = key.removeEnd(removeStringFromKey);
            returnMap.put(newKey, incomingMap.get(key));
        }
        return returnMap;
    }



    public static Map<String,Double> reviewIndividualClaims(Map<String,Object> inputMap) {

        List<InputClaim> claims = getClaims(inputMap, CLAIMS_LIST_JSON_KEY);
        List<Object> inputData = new List <Object>();
        Map<String,Double> claimScoreMap = new Map<String,Double>();
        ignoredClaimCount = 0;

        Integer index = 0;
        for(InputClaim claimRaw : claims){

            if(claimRaw != null) {
                Integer claimYear = claimRaw.claimYear == null ? null : Integer.valueOf(claimRaw.claimYear);

                if (claimYear <= CLAIM_YEAR_LIMIT) {

                    // obtain claim detail and use the calculation procedure to obtain the individual claim score
                    ClaimDetail claim = new ClaimDetail();
                    claim.otherProperty = claimRaw.radioClaimAtCurrentProperty == 'No' ? 'true' : 'false';
                    claim.claimValue = String.valueOf(claimRaw.textClaimValue);
                    claim.claimType = claimRaw.selectClaimType;
                    claim.claimTypeOtherProperties = claimRaw.selectClaimType;
                    claim.claimTypeYear = claimRaw.selectClaimType;
                    claim.year = claimRaw.claimYear;
                    claim.azurClaim = claimRaw.azurClaim == null ? false : Boolean.valueOf(claimRaw.azurClaim);
                    claim.claimName = String.valueOf(claimRaw.claimName);

                    // add key to the map of claims so we can link the single scores to the original records
                    String indexText = String.valueOf(index);
                    claim.claimkey = indexText;
                    claimRaw.claimkey = indexText;
                    scoredClaimMap.put(indexText, claimRaw);
                    index++;

                    // add claim to list for calculation procedure
                    String claimJson = JSON.serialize(claim);
                    Map<String, Object> claimMap = (Map<String, Object>) JSON.deserializeUntyped(claimJson);
                    inputData.add(claimMap);

                }
            }

        }

        // send the formatted input data to the calculation procedure
        Map<String, Object> calculationProcedureOutput = calculateSingleClaim(inputData);

        // review the output of the calculation procedure to get the peril and score
        List<Object> results = (List<Object>) calculationProcedureOutput.get('output');
        if(!results.isEmpty()){

            vlocity_ins.PricingCalculationService.CalculationProcedureResults result = (vlocity_ins.PricingCalculationService.CalculationProcedureResults) results[0];
            for (Integer i = 0; i < result.calculationResults.size(); i++) {

                Map<String, Object> resultMap = (Map<String, Object>) result.calculationResults[i];
                String claimKey = String.valueOf( Integer.valueOf( resultMap.get('claimKey')) );
                String peril = (String) resultMap.get('ClaimPeril__peril');
                Double score = (Double) resultMap.get('ClaimScore');

                // add scoring to the scoredClaimMap for retention after the quote is generated
                if( String.isNotBlank(claimKey) && scoredClaimMap.containsKey(claimKey) ){

                    InputClaim claimRaw = scoredClaimMap.get(claimKey);

                    // ignored claims are to be retained and excluded in claim count
                    if(peril == 'IgnoreClaim') {
                        ignoredClaimCount++;
                    }

                    claimRaw.claimScore = Double.valueOf( score );
                    scoredClaimMap.put(claimKey, claimRaw);
                }

                // add scoring for each peril to enable the final Claim Score calculation procedure
                score += claimScoreMap.containsKey(peril) ? claimScoreMap.get(peril) : 0;
                claimScoreMap.put(peril, score);

            }
        }

        return claimScoreMap;
    }

    private static List<InputClaim> getClaims(Map<String, Object> inputMap, String node) {

        List<InputClaim> claims = new List<InputClaim>();
        Object inputMapJson = inputMap.get(node);

        if (inputMapJson instanceof Map<String, Object>) {
            InputClaim inpClaim = (InputClaim) JSON.deserialize(JSON.serialize(inputMapJson), InputClaim.class);
            if (inpClaim != null) {
                claims.add(inpClaim);
            }
        }

        if (inputMapJson instanceof List<Object>) {
            List<Object> claimListOld = (List<Object>) inputMapJson;
            for (Object claimObj : claimListOld) {
                if (claimObj != null) {
                    InputClaim inpClaim = (InputClaim) JSON.deserialize(JSON.serialize(claimObj), InputClaim.class);
                    claims.add(inpClaim);
                }
            }
        }

        return claims;
    }

    public static Map<String,Object> calculateSingleClaim(List<Object> inputData) {

        Map<String,Object> inputMap = new Map<String,Object>();
        Map<String,Object> output = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();

        inputMap.put('UserInputList', inputData);

        options.put('configurationName', 'EmergingWealthSingleClaimCalculation');
        options.put('includeInputs', false);
        //options.put('effectiveDate','2018-10-31 23:09:10');

        return callCalculationProcedure(inputMap, output, options);
    }


    public static Map<String,Object> reviewAllScores(Map<String,Double> calculatedScores){

        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();

        Map<String, Object> inputdata = new Map<String, Object>();
        for (String key : calculatedScores.keySet()) {
            if(key != null) inputdata.put(key, String.valueOf(calculatedScores.get(key)));
        }
        inputdata.put('numberOfClaims', String.valueOf( scoredClaimMap.size() - ignoredClaimCount ));
        inputMap.put('UserInputList', inputData);

        options.put('configurationName', 'EmergingWealthClaimScores');
        options.put('includeInputs', false);
        //options.put('effectiveDate','2018-10-31 23:09:10');

        Map<String, Object> calculationProcedureOutput = callCalculationProcedure(inputMap, output, options);

        // review the output of the calculation procedure to get the final scores
        List<Object> results = (List<Object>) calculationProcedureOutput.get('output');
        if (!results.isEmpty()) {
            vlocity_ins.PricingCalculationService.CalculationProcedureResults result = (vlocity_ins.PricingCalculationService.CalculationProcedureResults) results[0];
            output = (Map<String, Object>) result.calculationResults[0];

            // amend scored claims based on this output
            Double multiClaimFactor = output.get('MultiClaimFactor__multiFactor') == null ? 1 : Double.valueOf( output.get('MultiClaimFactor__multiFactor'));
            for(String claimKey : scoredClaimMap.keySet()){
                InputClaim claimRaw = scoredClaimMap.get(claimKey);
                claimRaw.claimScore = (claimRaw.claimScore * multiClaimFactor).setScale(SCORE_PRECISION);
                scoredClaimMap.put(claimKey, claimRaw);
            }

        }

        // add scoredClaims to output
        output.put('scoredClaims', scoredClaimMap.values());

        return output;
    }

    public static Map<String,Object> callCalculationProcedure(Map<String,Object> inputMap, Map<String,Object> output, Map<String,Object> options) {
        // calls the Vlocity Calculation Procedure

        System.debug('Method: callCalculationProcedure');
        System.debug('inputMap > ' + inputMap);
        System.debug('output > ' + output);
        System.debug('options > ' + options);

        Type t = Type.forName('vlocity_ins','PricingMatrixCalculationService');
        vlocity_ins.VlocityOpenInterface vpsi = (vlocity_ins.VlocityOpenInterface) t.newInstance();
        vpsi.invokeMethod('calculate', inputMap, output, options);

        System.debug('OUTPUT = '+output);

        return output;

    }

    private static vlocity_ins__InsuranceClaim__c createClaimRecord(InputClaim claim, String quoteId) {

        Id recordTypeId = Schema.SObjectType.vlocity_ins__InsuranceClaim__c.getRecordTypeInfosByDeveloperName().get('EW_Quote').getRecordTypeId();

        vlocity_ins__InsuranceClaim__c claimRecord = new vlocity_ins__InsuranceClaim__c(
                id = claim.claimId != null ? claim.claimId : null,
                name = claim.claimName != null ? claim.claimName : null,
                vlocity_ins__LossDate__c = Date.parse(claim.claimDateText),
                vlocity_ins__ClaimStatus__c = claim.radioClaimStillOpen == 'Yes' ? 'Open' : 'Closed',
                vlocity_ins__LossType__c = claim.lossType,
                vlocity_ins__LossCause__c = claim.selectClaimType,
                vlocity_ins__TotalIncurred__c = claim.textClaimValue.setScale(2),
                vlocity_ins__Description__c = claim.claimDescriptionOther != null ? claim.claimDescriptionOther : claim.claimDescription,
                EW_Claim_from_current_address__c = claim.radioClaimAtCurrentProperty,
                EW_Claim_rating_score__c = (claim.claimScore != null) ? String.valueOf(claim.claimScore.setScale(5)) : null,
                EW_Azur_Claim__c = claim.azurClaim == null ? false : Boolean.valueOf(claim.azurClaim),
                EW_Quote__c = quoteId,
                RecordTypeId = recordTypeId
        );

        return claimRecord;
    }

    public static void updateQuoteJSONDataAfterClaimDelete(Map<Id, vlocity_ins__InsuranceClaim__c> deletedClaims) {
        Set<Id> quoteIds = new Set<Id>();
        for (vlocity_ins__InsuranceClaim__c claim : deletedClaims.values()) {
            quoteIds.add(claim.EW_Quote__c);
        }

        List<EW_QuoteJSONData__c> quoteJSONData = new List<EW_QuoteJSONData__c>([
                SELECT Id, EW_PropertyNode__c
                FROM EW_QuoteJSONData__c
                WHERE EW_Quote__c IN :quoteIds
        ]);

        for (EW_QuoteJSONData__c jsonData : quoteJSONData) {
            Map<String, Object> propertyNodeMap = (Map<String, Object>) JSON.deserializeUntyped(jsonData.EW_PropertyNode__c);
            Object editBlockClaimsHistory = propertyNodeMap.get(CLAIMS_LIST_JSON_KEY);

            // for just one claim - it's an object right away & object == Map<key, value>
            if (editBlockClaimsHistory instanceof Map<String, Object>) {
                Map<String, Object> claim = (Map<String, Object>) editBlockClaimsHistory;
                if (deletedClaims.containsKey(String.valueOf(claim.get(CLAIM_ID_JSON_KEY)))) {
                    propertyNodeMap.put(CLAIMS_LIST_JSON_KEY, null);
                }
            }
            // for just more than one claim - it's a list of objects, so List<Map<key, value>>
            if (editBlockClaimsHistory instanceof List<Object>) {
                List<Object> claimListOld = (List<Object>) editBlockClaimsHistory;
                List<Object> claimListNew = new List<Object>();
                for (Object claimObj : claimListOld) {
                    Map<String, Object> claim = (Map<String, Object>) claimObj;
                    if (!deletedClaims.containsKey(String.valueOf(claim.get(CLAIM_ID_JSON_KEY)))) {
                        claimListNew.add(claimObj);
                    }
                }

                if (claimListNew.size() == 1) {
                    propertyNodeMap.put(CLAIMS_LIST_JSON_KEY, claimListNew.get(0));
                } else {
                    propertyNodeMap.put(CLAIMS_LIST_JSON_KEY, claimListNew);
                }
            }

            jsonData.EW_PropertyNode__c = JSON.serializePretty(propertyNodeMap);
        }

        update quoteJSONData;
    }

    // called by InsuranceClaimTriggerHandler on beforeInsert events
    public static void setIfClaimsUnderAzurInsOrNot(List<vlocity_ins__InsuranceClaim__c> newItems) {
        if(disableSettingAzurClaimFlag == true) return;

        Set<Id> policyIds = new Set<Id>();
        for (vlocity_ins__InsuranceClaim__c claim : newItems) {
            if (String.isNotBlank(claim.vlocity_ins__PrimaryPolicyAssetId__c)) {
                policyIds.add(claim.vlocity_ins__PrimaryPolicyAssetId__c);
            }
        }

        Map<Id, Asset> policies = new Map<Id, Asset>([
                SELECT Id
                FROM Asset
                WHERE Id IN :policyIds
                AND RecordTypeId = :Asset.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get(EWConstants.POLICY_EW_RECORD_TYPE_DEVNAME).getRecordTypeId()
        ]);

        for (vlocity_ins__InsuranceClaim__c claim : newItems) {
            Id claimPolicyId = claim.vlocity_ins__PrimaryPolicyAssetId__c;
            if (String.isNotBlank(claimPolicyId) && policies.containsKey(claimPolicyId)) {
                claim.vlocity_ins__LossCause__c = claim.EW_Claim_Type__c;
                claim.EW_Azur_Claim__c = true;
            }
        }
    }

    public static List<InputClaim> getPolicyClaims(Id recordId){

        List<vlocity_ins__InsuranceClaim__c> claimRecords = (List<vlocity_ins__InsuranceClaim__c>)
                EWPolicyHelper.selectSObjectRecords(
                        Schema.SObjectType.vlocity_ins__InsuranceClaim__c.getName(),
                        Schema.SObjectType.vlocity_ins__InsuranceClaim__c.fields.vlocity_ins__PrimaryPolicyAssetId__c.getName(),
                        recordId,
                        true
                );

        List<InputClaim> claims = new List<InputClaim>();
        for(vlocity_ins__InsuranceClaim__c c : claimRecords){

            InputClaim claim = new InputClaim();

            claim.claimId = c.Id;
            claim.claimName = c.Name;
            claim.claimDateText = c.vlocity_ins__LossDate__c == null ? null : c.vlocity_ins__LossDate__c.format();
            claim.radioClaimStillOpen = c.vlocity_ins__ClaimStatus__c == null ? null : c.vlocity_ins__ClaimStatus__c == 'Open' ? 'Yes' : 'No';
            claim.lossType = c.vlocity_ins__LossType__c  == null ? null : c.vlocity_ins__LossType__c;
            claim.selectClaimType = c.vlocity_ins__LossCause__c == null ? null : c.vlocity_ins__LossCause__c;
            claim.textClaimValue = c.vlocity_ins__TotalIncurred__c == null ? null : c.vlocity_ins__TotalIncurred__c;
            claim.radioClaimAtCurrentProperty = c.EW_Claim_from_current_address__c == null ? null : c.EW_Claim_from_current_address__c;
            claim.claimScore = c.EW_Claim_rating_score__c == null ? null : Decimal.valueOf(c.EW_Claim_rating_score__c);
            claim.azurClaim = c.EW_Azur_Claim__c == null ? false : c.EW_Azur_Claim__c;
            claim.claimDescription = c.vlocity_ins__Description__c == null ? null : c.vlocity_ins__Description__c;
            claims.add(claim);
        }
        return claims;

    }


    /**
    * @methodName: checkForOpenRenewalQuotes
    * @description: void method to process claim records afterInsert, and to expire any open renewal quotes.
    *   Called by the class InsuranceClaimTriggerHandler. Makes asynchronous call to checkForOpenRenewalQuotes method with policyIds
    * @author: Roy Lloyd
    * @date: 9/3/2020
    */
    public static void checkForOpenRenewalQuotes(List<vlocity_ins__InsuranceClaim__c> newItems) {

        Set<Id> policyIds = new Set<Id>();
        for (vlocity_ins__InsuranceClaim__c claim : newItems) {
            policyIds.add(claim.vlocity_ins__PrimaryPolicyAssetId__c);
        }

        Boolean runningAsync = System.isQueueable() || System.isBatch() || System.isFuture() || System.isScheduled();
        if(runningAsync){
            checkForOpenRenewalQuotes(policyIds);
        }else{
            checkForOpenRenewalQuotesAtFuture(policyIds);
        }
    }


    /**
    * @methodName: checkForOpenRenewalQuotesAtFuture
    * @description: atFuture method to process claim records afterInsert, called by the above method if currently not running asynchronous process
    * @date: 9/3/2020
    */
    @future
    public static void checkForOpenRenewalQuotesAtFuture(Set<Id> policyIds) {
        checkForOpenRenewalQuotes(policyIds);
    }


    /**
    * @methodName: checkForOpenRenewalQuotes
    * @description: void method to process claim records afterInsert, and to expire any open renewal quotes.
    * @author: Roy Lloyd
    * @date: 9/3/2020
    * v2: Aleksejs Jedamenko ajedamenko@azuruw.com - EW_IsStandardTemplateForRenewal__c update logic added to send different content in renewal quote emails, if previous renewal quote was cancelled by new claim on a policy
    */
    public static void checkForOpenRenewalQuotes(Set<Id> policyIds){

        List<Quote> openRenewalQuotes = [SELECT Id FROM Quote WHERE EW_Source_Policy__c in: policyIds
            AND EW_TypeofTransaction__c = :EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL
            AND Status != :EWConstants.QUOTE_STATUS_NTU
            AND Status != :EWConstants.QUOTE_STATUS_EXPIRED ];

        for(Quote q : openRenewalQuotes){
            q.Status = EWConstants.QUOTE_STATUS_EXPIRED;
        }

        List<Asset> policiesToUpdateTemplateFlag = [SELECT Id, EW_IsStandardTemplateForRenewal__c FROM Asset WHERE Id IN: policyIds];
        for (Asset policy : policiesToUpdateTemplateFlag) {

            policy.EW_IsStandardTemplateForRenewal__c = false;
        }
     
        // email preparation to notify Underwriters of expired quotes
        OrgWideEmailAddress orgWideEmailAddy = [ SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :EWConstants.ORG_WIDE_EMAIL_NAME LIMIT 1 ];
        EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE Name = :EWConstants.EMAIL_TEMPLATE_EXPIRED_RENEWAL_QUOTES LIMIT 1];

        List<String> listOfRecipients = new List<String>();
        List<EW_Renewal_Quote_Email_Setting__mdt> recipients = [ SELECT Id, Label, EW_Email__c FROM EW_Renewal_Quote_Email_Setting__mdt];
        for(EW_Renewal_Quote_Email_Setting__mdt r : recipients){
            listOfRecipients.add(r.EW_Email__c);
        }
        List<User> usersToEmail = [SELECT Id FROM User WHERE IsActive = true AND EMAIL in: listOfRecipients];
        Id emailToId = !usersToEmail.isEmpty() ? usersToEmail[0].Id : null;

        //  update the open renewal quotes and send email notification
        try{
            update openRenewalQuotes;

            List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
            for(Quote q : openRenewalQuotes) {
                Messaging.SingleEmailMessage msg = EWEmailHelper.generateEmail(emailToId, listOfRecipients, orgWideEmailAddy.Id, q.Id, template.Id);
                emails.add(msg);
            }
            Messaging.sendEmail(emails);
            update policiesToUpdateTemplateFlag;

        }catch(Exception e){
            insert new LogBuilder().createLogWithCodeInfo(e, null, 'EWClaimsHelper', 'checkForOpenRenewalQuotes');
        }

    }

    public class InputClaim {

        public String claimId;
        public String claimName;
        public String claimKey;
        public String radioClaimAtCurrentProperty;
        public Decimal textClaimValue;
        public String claimDateText;
        public String selectClaimType;
        public String radioClaimStillOpen;
        public String claimYear;
        public String lossType;
        public Decimal claimScore;
        public Boolean azurClaim;
        public String claimDescription;
        public String claimDescriptionOther;

        public InputClaim(){}
    }

    public class OutputClaim {

        public String claimIdRO;
        public String claimNameRO;
        public String radioClaimAtCurrentPropertyRO;
        public Decimal textClaimValueRO;
        public String claimDateTextRO;
        public String selectClaimTypeRO;
        public String radioClaimStillOpenRO;
        public String claimYearRO;
        public Boolean azurClaimRO;
        public String claimDescriptionRO;

        public OutputClaim(){}
    }


    public class ClaimDetail{

        public string claimKey;
        public String claimName;
        public String otherProperty;
        public String claimValue;
        public String claimDate;
        public String claimType;
        public String claimTypeYear;
        public String claimTypeOtherProperties;
        public String claimStillOpen;
        public String year;
        public Boolean azurClaim;

    }




}