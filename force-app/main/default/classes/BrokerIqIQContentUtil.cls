/**
* @name: BrokerIqGroupUtil
* @description: IQ Content utility class
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 14/12/2017
* @Modified:
*/
public class BrokerIqIQContentUtil {

    public static List<IQ_Content__c> getUpcomingWebinars(List<Id> queryIQContentIds, List<Id> noQueryIQContentIds, Integer limitQueryRecords){
		return getWebinars(false, queryIQContentIds, noQueryIQContentIds, limitQueryRecords);
    }
    
    public static List<IQ_Content__c> getRecentWebinars(List<Id> queryIQContentIds, List<Id> noQueryIQContentIds, Integer limitQueryRecords){
		return getWebinars(true, queryIQContentIds, noQueryIQContentIds,  limitQueryRecords);
    }
    
    private static List<IQ_Content__c> getWebinars(boolean getRecentWebinars, List<Id> queryIQContentIds, List<Id> noQueryIQContentIds, Integer limitQueryRecords){
        Datetime now = Datetime.now();
        //Set where clause
        string whereClauseStartDateSymbol = getRecentWebinars? '<' : '>';
        string whereClause = 'RecordType.DeveloperName = \'Brighttalk\' AND BrightTALK_Webcast__r.BrightTALK__Start_Date__c ' + whereClauseStartDateSymbol + ' :now AND Active__c = true '; 
        if (!queryIQContentIds.isEmpty())	whereClause += 'AND Id IN: queryIQContentIds ';
        if (!noQueryIQContentIds.isEmpty())	whereClause += 'AND Id NOT IN: noQueryIQContentIds ';
        //Set query string
        String queryString = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 
                                                                      'BiQ_2_Card_Grid', 
                                                                      new Set<String>(), 
                                                                      whereClause,
                                                                      'BrightTALK_Webcast__r.BrightTALK__Start_Date__c DESC', 
                                                                      'LIMIT ' + limitQueryRecords);
        return Database.query(queryString);
    }
    
}