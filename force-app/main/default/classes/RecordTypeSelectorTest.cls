/*
* @name: RecordTypeSelectorTest
* @description: RecordTypeSelector apex test controller
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 26/01/2018
* @modifiedBy:
*/
@isTest
public class RecordTypeSelectorTest {

    testMethod static void invokeMethodTest(){
        //Set params
        String methodName = 'RecordTypeSelector';
        Map<String,Object> inputMap = new  Map<String,Object>{'objectType' => 'Account'};
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        //Execute the test
        Test.startTest();
            RecordTypeSelector recordTypeSelector = new RecordTypeSelector();
            recordTypeSelector.invokeMethod(methodName, inputMap, outMap, options);
        Test.stopTest();
    } 

}