@isTest
private class IqContentPrimaryTopicTest {
		
	private static TestObjectGenerator tog = new TestObjectGenerator();

	// We need to seeAllData to avoid clashing with duplicate names in the
	// non-test world see:
	// https://salesforce.stackexchange.com/questions/12258/pushtopic-test-duplicate-name-bug
	// https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_testing_data_access.htm#TestDataNotes
	@isTest(seeAllData=true) 
	static void basic() {
		String primaryTopic = 'Insurance';

		IQ_Content__c iqContent = tog.getWebcastIqContent();

		iqContent.Primary_Topic__c = primaryTopic;

		update iqContent;

		System.assert([SELECT Id 
		              FROM TopicAssignment 
		              WHERE Topic.Name = :primaryTopic 
		              AND EntityId = :iqContent.Id].size() > 0);

	}	
	@isTest(seeAllData=true) 
	static void createTopic() {
		String primaryTopic = 'Insurance';

		List<Topic> existingTopic = [SELECT Id FROM Topic WHERE Name = :primaryTopic AND NetworkId = null];
		if(!existingTopic.isEmpty()) {
			delete [SELECT Id FROM TopicAssignment WHERE TopicId = :existingTopic[0].Id];
			delete existingTopic;
		}
		
		IQ_Content__c iqContent = tog.getWebcastIqContent();

		iqContent.Primary_Topic__c = primaryTopic;

		update iqContent;

		System.assert([SELECT Id 
		              FROM TopicAssignment 
		              WHERE Topic.Name = :primaryTopic 
		              AND EntityId = :iqContent.Id].size() > 0);

	}	
}