/*
A file of the reusable utility functions that are generic and used across
multiple unrelated components
*/

import createAnalyticsEvent from '@salesforce/apex/IXAnalyticsHelper.createAnalyticsEvent';
import { CONSTANTS } from 'c/ix_constants';

/*
Convert snake_case to Sentence Case, making naming consistent.
*/
const snakeToSentence = (snakeCaseString) => {
    return snakeCaseString && snakeCaseString
        .split('_')
        .map(word => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ')
        // 🍝🙈
        .split(' And ')
        .join(' and ');
}

const removeItemFromArray = (array, item_name) => {
    const itemToRemove = array.find(item => item._name === item_name);
    const itemIndex = array.indexOf(itemToRemove);
    array.splice(itemIndex, 1);
    return array;
}

/*
Copied verbatim from https://gist.github.com/andrei-m/982927
*/
/*
Copyright (c) 2011 Andrei Mackenzie
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// Compute the edit distance between the two given strings
const levenshteinDistance = function(a, b){
    if (a.length == 0) return b.length; 
    if (b.length == 0) return a.length; 
  
    var matrix = [];
  
    // increment along the first column of each row
    var i;
    for (i = 0; i <= b.length; i++) {
      matrix[i] = [i];
    }
  
    // increment each column in the first row
    var j;
    for (j = 0; j <= a.length; j++) {
      matrix[0][j] = j;
    }
  
    // Fill in the rest of the matrix
    for (i = 1; i <= b.length; i++){
        for (j = 1; j <= a.length; j++){
            if (b.charAt(i-1) == a.charAt(j-1)) {
                matrix[i][j] = matrix[i-1][j-1];
            } else {
                matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                    Math.min(matrix[i][j-1] + 1, // insertion
                    matrix[i-1][j] + 1)); // deletion
            }
        }
    }
  
    return matrix[b.length][a.length];
};

const analyticsEvent = (params) => {
    return new Promise((resolve, reject) => {
        createAnalyticsEvent({
            sessionID: window.sessionStorage.getItem('sessionID'),
            ...params
        })
            .then(() => resolve())
            .catch(error => {
                reject();
                console.error(error);
            });
    })
}

const prepareForMarkup = (data) => {
    /*
    This function is going to be quite arbitrary because it's determined
    by how the CSS is going to work and stuff 🍝

    It gives each item a unique ID to make searching and stuff easier later
    */
    let iterator = 0;
    let results = data;

    // Returns classname for the CSS, depending on the depth of the current
    // item
    const getCSSClass = (depth) => {
        let greatString = '';
        switch(depth) {
            case 1:
                return 'parent';
            case 2:
                return 'child';
            case 3:
                return 'grandchild';
            default:
                for (let i = 0; i < depth - 3; i++) {
                    greatString += 'great-'
                }
                return `${greatString}grandchild`;
        }
    }

    /*
    Items with _name as these will be unsearchable.
    */
    const itemUnsearchable = [
        CONSTANTS.LIMITS_TO_SUM_INSURED,
        CONSTANTS.SUM_INSURED,
        CONSTANTS.EXCLUSIONS,
        CONSTANTS.CLAUSES,
        CONSTANTS.YOUR_COVER,
        CONSTANTS.CONDITIONS,
        CONSTANTS.MAKING_A_CLAIM,
        CONSTANTS.MAKE_A_CLAIM,
        CONSTANTS.BASIS_OF_COVER,
        CONSTANTS.ALL_HOME_EMERGENCY_EXCLUSIONS,
        CONSTANTS.ALL_LIABILITY_EXCLUSIONS,
        CONSTANTS.ALL_PERSONAL_CYBER_EXCLUSIONS
    ];

    /*
    An array of item names where we want their children to be unsearchable.
    */
    const childrenUnsearchable = [];

    /*
    The children of these items will have the property `_is_special_styling` set
    to `true`. This means that as the data is processed in `ix_contentDisplay`,
    they can have exceptional styling applied that overwrites what would
    otherwise be normal styling. For example, whilst a data item with name
    `Buildings` will usually be rendered with `_is_link`, as `true`, when such a
    data item is a child of `CONSTANTS.DEFINITIONS_TITLES.GENERAL_DEFINITIONS`,
    it is rendered as `_is_accordion` as `true`.
    */
    const childrenSpecialStyling = [
        CONSTANTS.DEFINITIONS_TITLES.GENERAL_DEFINITIONS,
        CONSTANTS.DEFINITIONS_TITLES.LEGAL_EXPENSES_DEFINITIONS,
        CONSTANTS.DEFINITIONS_TITLES.PERSONAL_CYBER_DEFINITIONS,
        CONSTANTS.DEFINITIONS_TITLES.HOME_EMERGENCY_DEFINITIONS
    ]

    const iterateOverArray = (array, currentDepth, currentUnsearchable,
            currentSpecialStyling) => {
        return array.map(item => {
            const isParent = Array.isArray(item._value);
            const depth = currentDepth ? currentDepth + 1 : 1;

            const currentItemUnsearchable = currentUnsearchable
                || itemUnsearchable.includes(item._name);

            const unsearchable = currentUnsearchable
                || currentItemUnsearchable
                ? true
                : childrenUnsearchable.includes(item._name);
            
            const childSpeciallyStyled
                = childrenSpecialStyling.includes(item._name);

            const standardItemFields = {
                ...item,
                _is_parent: isParent,
                _id: iterator,
                _depth: depth,
                _unsearchable: currentItemUnsearchable,
                _special_styling: currentSpecialStyling,
                // Adding the `displayed-item` class here for now
                _class_name: `${getCSSClass(depth)} displayed-item`,
                _name: snakeToSentence(item._name)
            }
            iterator++;

            if (isParent) {
                return {
                    ...standardItemFields,
                    _value: iterateOverArray(
                        item._value,
                        depth,
                        unsearchable,
                        childSpeciallyStyled
                    ),
                }
            }

            return standardItemFields;
        })
    }
    return iterateOverArray(results);
}

const processScheduleData = (rawInput) => {
    /*
    Convert, for instance, this:

    [
        additional_covers: [
            {
                name: 'Alternative accommodation',
                inner_limit: '£50,000'
            },
            {
                name: 'Business equipment',
                inner_limit: '£30,000'
            }
        ],
        address: '18, BARTON RD, LONDON, W14 9HD'
    ]

    To this:

    [
        {
            _name: 'Additional Covers',
            _is_parent: true,
            _value: [
                {
                    _name: 'Alternative accommodation',
                    _value: [
                        {
                            _name: 'inner_limit',
                            _value: '£50,000
                        },
                        {
                            _name: 'excess',
                            _value: '£500'
                        }
                    ]
                },
                {
                    _name: 'Business equipment',
                    _value: [
                        {
                            _name: 'inner_limit',
                            _value: '£30,000'
                        }
                    ]
                }
            ]
        },
        {
            _name: 'Address',
            _value: '18, BARTON RD, LONDON, W14 9HD'
        }
    ]

    so that every parent has children are in an array and therefore
    iterable
    */
    const parseData = data => {
        if (Array.isArray(data)) {
            // The parent is an array
            return data.map(item => {
                let value;

                // Yay loose typing?
                if (item && typeof item === 'object') {
                    // The child not null and is an array or an object
                    value = parseData(data.find(a => {
                        return a._name === item._name;
                    }));
                } else {
                    // The child is primitive
                    value = data[item._name];
                }

                return {
                    _name: item._name,
                    _value: value
                }
            })
        } else if (typeof data === 'object') {
            // The parent is an object
            let results = [];

            const itemKeys = Object.keys(data);
            itemKeys.forEach(key => {
                // Exclude any keys such as `'_name'` that are preceded by
                // an underscore and therefore should not be displayed
                if (key.match(/\b_/)) {
                    return;
                }

                let value;
                if (data[key] && typeof data[key] === 'object') {
                    // The child is not null and is an array or an object
                    value = parseData(data[key]);
                } else {
                    // The child is primitive
                    value = data[key];
                }

                results.push({
                    _name: key,
                    _value: value
                })
            })
            return results;
        }
        // The parent is primitive
        return data;
    }

    // Spreading rawInput over itself because cached items are read-only
    return parseData({ ...rawInput });
}

const findItemByName = (name, data) => {
    /*
    Recursively search through the data and return the first (and not
    necessarily only) item where item._name is equal to name. `data` must
    be an array of objects.
    */
    let result = {};
    const iterateOverArray = array => {
        return array.forEach(item => {
            const isParent = Array.isArray(item._value);
            if (item._name === name) {
                result = item;
            } else if (isParent) {
                iterateOverArray(item._value);
            }
            return null;
        })
    }
    iterateOverArray(data);
    return result;
}

export { snakeToSentence, removeItemFromArray, levenshteinDistance,
    analyticsEvent, prepareForMarkup, processScheduleData, findItemByName };