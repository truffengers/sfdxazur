/**
* @name: EWSiteProviderController
* @description: Class to provide information about the broker hub community
* @author: Bartosz Dec
* @date: 10/12/2018
* @modifiedBy:
*/
public class EWSiteProviderController {

    public class SiteMetadata {
        @AuraEnabled
        public String adminEmail = Site.getAdminEmail();
        @AuraEnabled
        public String baseSecureUrl = Site.getBaseSecureUrl();
        @AuraEnabled
        public String pathPrefix = Site.getPathPrefix();
        @AuraEnabled
        public Boolean isSandbox = OrgInformationUtility.isSandbox();
    }

    @AuraEnabled
    public static SiteMetadata getSiteMetadata() {
        return new SiteMetadata();
    }

    /**
    * Originally, the baseSecureUrl was obtained using the following ternary operator:
    *
    * `OrgInformationUtility.isSandbox() ? Site.getBaseSecureUrl() + '/BrokerHub/s' : Site.getBaseSecureUrl();`
    *
    * It now appears to be broken because the `/BrokerHub/s` being added is extra - this part of the URL is already
    * included in the baseSecureUrl itself, which has made the URL invalid and the logout link lead to an invalid page.
    *
    * A similar fix was applied for SiteProviderController for Broker iQ.
    *
    * - Andrew Xu, 10/07/2020
    *
    * Existing comment left here:
    *
    * TODO maybe some custom metadata or sth? Any class or place for links like that?
    * We could use ConnectApi.Community community = ConnectApi.Communities.getCommunity(Network.getNetworkId());
    * But then we will have to mock that not to throw an error in builder (Network id will probably be null there)
    * */
}