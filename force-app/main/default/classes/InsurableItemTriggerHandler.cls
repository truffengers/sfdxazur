public with sharing class InsurableItemTriggerHandler implements ITriggerHandler {
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return InsurableItemTriggerHandler.IsDisabled != null ? InsurableItemTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
        AuditLogUtil.createAuditLog('vlocity_ins__InsurableItem__c', newItems.values(), null);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        new EWInsurableItemHelper().checkForOutraChanges(oldItems, newItems);
        AuditLogUtil.createAuditLog('vlocity_ins__InsurableItem__c', newItems.values(), oldItems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
        new EWInsurableItemHelper().updateQuoteJSONDataAfterSpecifiedItemDelete((Map<Id, vlocity_ins__InsurableItem__c>) oldItems);
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
    }
}