TT.mediaMatcher = (function($) {

  "use strict";

  var _;
  var self = {};
  var $self = $(self);
  var mediaQueryDefinitions = {};
  var mediaQueryLists = [];
  var mediaName;



  function buildLookupAndDefineConstants(definitions) {
    $.each(definitions, function(name, query) {
      mediaQueryDefinitions[window.matchMedia(query).media] = name;
      self[name.toUpperCase()] = name;
      self['getIs' + _.capitalize(name)] = function() {
        return mediaName === name;
      };
    });
  }

  function defineMediaQueries() {
    $.each(mediaQueryDefinitions, defineMediaQuery);
  }

  function defineMediaQuery(media) {
    var mediaQueryList = window.matchMedia(media);
    mediaQueryList.addListener(mediaChangeHandler);
    mediaQueryLists.push(mediaQueryList);
  }

  function mediaChangeHandler(mediaQueryList) {
    if(mediaQueryList.matches) {
      mediaName = getNameFromMedia(mediaQueryList.media);
      dispatchMediaMatchEvent();
    }
  }

  function dispatchMediaMatchEvent() {
    $self.triggerHandler('mediaChange', [mediaName]);
  }

  function findMatch() {
    for(var i=0; i<mediaQueryLists.length; i++) {
      if(mediaQueryLists[i].matches) {
        return getNameFromMedia(mediaQueryLists[i].media);
      }
    }
  }

  function getNameFromMedia(media) {
    return mediaQueryDefinitions[media];
  }



  $.extend(self, {

    init: function(lodash, definitions) {
      _ = lodash;
      buildLookupAndDefineConstants(definitions);
      defineMediaQueries();
      mediaName = findMatch();
    },

    getMediaName: function() {
      return mediaName;
    }

  });



  return self;


}( jQuery ));