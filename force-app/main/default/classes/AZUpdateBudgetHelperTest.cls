@isTest
private class AZUpdateBudgetHelperTest {

    @TestSetup
    static void setup() {

        Product2 product1 = EWProductDataFactory.createProduct('Home Emergency', false);
        insert product1;
        
        //insert Budget
        AZ_Budget__c budgetRec = new AZ_Budget__c();
        budgetRec.AZ_Product__c = product1.Id;
        budgetRec.AZ_BudgetYear__c = '2020';
        budgetRec.AZ_Version__c = 1;
        budgetRec.AZ_AzurCommissionNewBiz__c = 15;
        budgetRec.AZ_AzurCommissionRenewal__c = 15;
        budgetRec.AZ_Baselined__c = false;
        budgetRec.AZ_BrokerCommissionNewBiz__c = 20;
        budgetRec.AZ_Status__c = 'Draft';
        insert budgetRec;

       //insert Budget record 2
        AZ_Budget__c budgetRec2 = new AZ_Budget__c();
        budgetRec2.AZ_Product__c = product1.Id;
        budgetRec2.AZ_BudgetYear__c = '2020';
        budgetRec2.AZ_Version__c = 1;
        budgetRec2.AZ_AzurCommissionNewBiz__c = 15;
        budgetRec2.AZ_AzurCommissionRenewal__c = 15;
        budgetRec2.AZ_Baselined__c = false;
        budgetRec2.AZ_BrokerCommissionNewBiz__c = 20;
        budgetRec2.AZ_Status__c = 'Draft';
        insert budgetRec2;

        //insert Budget Line Items
        AZ_BudgetLineItem__c budgetLineItem = new AZ_BudgetLineItem__c();
        budgetLineItem.AZ_Budget__c = budgetRec.Id;
        budgetLineItem.AZ_AprNewBusiness__c = 200;
        budgetLineItem.AZ_AprRenewals__c = 200;
        insert budgetLineItem;

        //insert Budget Actions
        AZ_BudgetActions__c actions = new AZ_BudgetActions__c();
        actions.AZ_Description__c = 'Test';
        actions.AZ_Due_Date__c = Date.newInstance(2021, 03, 19);
        actions.Az_Status__c = 'Open';
        actions.AZ_Budget__c = budgetRec.Id;
    }

    @IsTest
    private static void updateBudgetTest(){

       AZ_Budget__c budget = [SELECT Id FROM AZ_Budget__c LIMIT 1];
        Test.startTest();
            budget.Az_Status__c = 'Baselined';
            update budget;
        Test.stopTest();
        System.assertEquals(budget.Az_Status__c, 'Baselined');
    }
    
}