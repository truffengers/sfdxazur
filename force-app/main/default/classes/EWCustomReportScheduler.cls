global class EWCustomReportScheduler implements Schedulable {

    global void execute(SchedulableContext sc) {
        List<String> listOfRecipients = new List<String>();

        String yesterdayDate = Datetime.newInstanceGmt(
                Date.today().addDays(-1),
                Time.newInstance(0, 0, 0, 0)
        ).format('yyyy-MM-dd');

        String oneDayBeforeYesterdayDate = Datetime.newInstanceGmt(
                Date.today().addDays(-2),
                Time.newInstance(0, 0, 0, 0)
        ).format('yyyy-MM-dd');

        String csvDataString1 = EWCustomReportController.getReportData(
                'AIG Report', null, null, yesterdayDate, yesterdayDate, true
        );
        String csvDataString2 = EWCustomReportController.getReportData(
                'AIG Report', yesterdayDate, yesterdayDate, null, oneDayBeforeYesterdayDate, true
        );

        Map<String, String> attachments = new Map<String, String>{
                String.format('New_{0}.csv', new List<String>{
                        yesterdayDate
                }) => csvDataString1,
                String.format('Correction_{0}.csv', new List<String>{
                        yesterdayDate
                }) => csvDataString2
        };

        List<EW_CustomReportEmailSetting__mdt> recipients = new List<EW_CustomReportEmailSetting__mdt>([
                SELECT Id, EW_Email__c
                FROM EW_CustomReportEmailSetting__mdt
        ]);

        for (EW_CustomReportEmailSetting__mdt u : recipients) {
            listOfRecipients.add(u.EW_Email__c);
        }

        sendEmail(attachments, listOfRecipients);
    }

    public void sendEmail(Map<String, String> attachments, List<String> listOfRecipients) {

        OrgWideEmailAddress asEmail = [
                SELECT Id
                FROM OrgWideEmailAddress
                WHERE DisplayName = 'Azur Smart Home'
                LIMIT 1
        ];

        EmailTemplate template = [
                SELECT Id
                FROM EmailTemplate
                WHERE Name = 'EW_CustomReports'
                LIMIT 1
        ];

        Messaging.SingleEmailMessage msg = EWEmailHelper.generateEmail(UserInfo.getUserId(), listOfRecipients, asEmail.Id, null, template.Id);

        List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>();
        for (String csvName : attachments.keySet()) {
            Blob csvData = Blob.valueOf(attachments.get(csvName));
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(csvName);
            efa.setBody(csvData);
            fileAttachments.add(efa);
        }
        msg.setFileAttachments(fileAttachments);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{
                msg
        });

    }
}