/******
 * Main Class: AZIntermediationPipelineHelper
******/

@isTest
private class AZIntermediationPipelineHelperTest {

    @TestSetup
    static void setup() {

        EWQuoteHelperTest.setupCarrierAccountsAndCoverages();

        Account broker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account insured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );
        insert new List<Account>{
                broker, insured
        };

        Product2 product1 = EWProductDataFactory.createProduct('Home Emergency', false);
        Product2 product2 = EWProductDataFactory.createProduct('Contents', false);
        insert new List<Product2>{
                product1, product2
        };

        Opportunity opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
        Quote quoteDD = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, Test.getStandardPricebookId(), true);
        
        PricebookEntry pbe1 = new PricebookEntry (Product2Id = product1.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
        PricebookEntry pbe2 = new PricebookEntry (Product2Id = product2.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);

        insert new List<PricebookEntry>{
                pbe1, pbe2
        };
        
        QuoteLineItem qli1 = new QuoteLineItem();
        qli1.QuoteId = quote.Id;
        qli1.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"networkPercentCommission":0,"networkCommissionValue":0,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        qli1.Product2Id = product1.Id;
        qli1.PricebookEntryId = pbe1.Id;
        qli1.Quantity = 1;
        qli1.UnitPrice = pbe1.UnitPrice;
        qli1.EW_Transactional_PremiumGrossExIPT__c = pbe1.UnitPrice;

        QuoteLineItem oli2 = new QuoteLineItem();
        oli2.QuoteId = quote.Id;
        oli2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"networkPercentCommission":0,"networkCommissionValue":0,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        oli2.Product2Id = product2.Id;
        oli2.PricebookEntryId = pbe1.Id;
        oli2.Quantity = 1;
        oli2.UnitPrice = pbe1.UnitPrice;
        oli2.EW_Transactional_PremiumGrossExIPT__c = pbe1.UnitPrice;
        
        QuoteLineItem qliDD = new QuoteLineItem();
        qliDD.QuoteId = quoteDD.Id;
        qliDD.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"networkPercentCommission":0,"networkCommissionValue":0,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        qliDD.Product2Id = product1.Id;
        qliDD.PricebookEntryId = pbe1.Id;
        qliDD.Quantity = 1;
        qliDD.UnitPrice = pbe1.UnitPrice;
        qliDD.EW_Transactional_PremiumGrossExIPT__c = pbe1.UnitPrice;

        insert new List<QuoteLineItem>{
                qli1, oli2, qliDD
        };    
            
        Asset policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policy.IBA_InsuredName__c = 'insName';
        policy.Status = 'Draft';
        policy.EW_FixedExpense__c = 40;
        policy.IBA_InsurancePremiumTaxRate__c = 0;
        policy.IBA_MGACommissionRate__c = 0;
        policy.IBA_BrokerCommissionRate__c = 0;
        policy.EW_Quote__c = quote.Id;
        policy.product2Id = product1.Id;
        policy.IBA_ProducingBrokerAccount__c = broker.Id;
        policy.vlocity_ins__InceptionDate__c = Date.newInstance(2021, 01, 19);
        
        Asset policyDD = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, false);
        policyDD.IBA_InsuredName__c = 'insNameDD';
        policyDD.Status = 'Draft';
        policyDD.EW_FixedExpense__c = 40;
        policyDD.IBA_InsurancePremiumTaxRate__c = 0;
        policyDD.IBA_MGACommissionRate__c = 0;
        policyDD.IBA_BrokerCommissionRate__c = 0;
        policyDD.EW_Quote__c = quoteDD.Id;
        policyDD.product2Id = product1.Id;
        policyDD.IBA_ProducingBrokerAccount__c = broker.Id;
        policyDD.EW_Payment_Method__c = EWConstants.DIRECT_DEBIT_PAYMENT_METHOD;
        policyDD.vlocity_ins__InceptionDate__c = Date.newInstance(2021, 01, 19);
        
        insert new List<Asset>{
                policy, policyDD
        };
            
        quote.EW_Policy__c = policy.Id;
        quoteDD.EW_Policy__c = policyDD.Id;
        quoteDD.EW_DD_Interest_Percentage__c = 6;
        update new List<quote>{
                quote, quoteDD
        };
        
        vlocity_ins__AssetCoverage__c coverage1 = new vlocity_ins__AssetCoverage__c();
        coverage1.vlocity_ins__PolicyAssetId__c = policy.Id;
        coverage1.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"networkPercentCommission":0,"networkCommissionValue":0,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverage1.vlocity_ins__Product2Id__c = product1.Id;
        coverage1.CurrencyIsoCode = 'GBP';

        vlocity_ins__AssetCoverage__c coverage2 = new vlocity_ins__AssetCoverage__c();
        coverage2.vlocity_ins__PolicyAssetId__c = policy.Id;
        coverage2.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"networkPercentCommission":0,"networkCommissionValue":0,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverage2.vlocity_ins__Product2Id__c = product2.Id;
        coverage2.CurrencyIsoCode = 'GBP';
        
        vlocity_ins__AssetCoverage__c coverageDD = new vlocity_ins__AssetCoverage__c();
        coverageDD.vlocity_ins__PolicyAssetId__c = policyDD.Id;
        coverageDD.vlocity_ins__AttributeSelectedValues__c = '{"azurCommissionValue":15.30233,"brokerCommissionValue":31.87985,"networkPercentCommission":0,"networkCommissionValue":0,"premiumGrossExIPT":127.5194,"premiumNet":80.3373,"premiumGrossIncIPT":142.82173,"iptTotal":15.30233,"artSpec":0,"artValue":10000,"artSpecified":0,"artUnspecified":10000,"ArtSpecifiedInput":0}';
        coverageDD.vlocity_ins__Product2Id__c = product1.Id;
        coverageDD.CurrencyIsoCode = 'GBP';

        insert new List<vlocity_ins__AssetCoverage__c>{
                coverage1, coverage2, coverageDD
        };
       
        //insert IBA_PolicyTransaction__c
        IBA_PolicyTransaction__c policyTrans = new IBA_PolicyTransaction__c();
        policyTrans.IBA_OriginalPolicy__c = policy.Id;
        policyTrans.IBA_ActivePolicy__c = policy.Id;
        policyTrans.IBA_TransactionType__c = 'ORIGINAL PREMIUM';
        policyTrans.IBA_TransactionDate__c = Date.newInstance(2021, 03, 19);
        policyTrans.IBA_ExchangeRate__c = 60;

        IBA_PolicyTransaction__c policyTrans2 = new IBA_PolicyTransaction__c();
        policyTrans2.IBA_OriginalPolicy__c = policy.Id;
        policyTrans2.IBA_ActivePolicy__c = policy.Id;
        policyTrans2.IBA_TransactionType__c = 'ENDORSEMENT';
        policyTrans2.IBA_TransactionDate__c = Date.newInstance(2021, 01, 19);
        policyTrans2.IBA_ExchangeRate__c = 60;

        IBA_PolicyTransaction__c policyTrans3 = new IBA_PolicyTransaction__c();
        policyTrans3.IBA_OriginalPolicy__c = policy.Id;
        policyTrans3.IBA_ActivePolicy__c = policy.Id;
        policyTrans3.IBA_TransactionType__c = 'MANUAL RENEWAL';
        policyTrans3.IBA_TransactionDate__c = Date.newInstance(2021, 01, 19);
        policyTrans3.IBA_ExchangeRate__c = 60;

        insert new List<IBA_PolicyTransaction__c>{
                policyTrans, policyTrans2, policyTrans3
        };


        //insert IBA_FFPolicyCarrierLine__c
        IBA_FFPolicyCarrierLine__c carrierLine = new IBA_FFPolicyCarrierLine__c();
        carrierLine.IBA_CarrierAccount__c = broker.Id;
        carrierLine.IBA_PolicyTransaction__c = policyTrans.Id;

        IBA_FFPolicyCarrierLine__c carrierLine2 = new IBA_FFPolicyCarrierLine__c();
        carrierLine2.IBA_CarrierAccount__c = broker.Id;
        carrierLine2.IBA_PolicyTransaction__c = policyTrans2.Id;

        IBA_FFPolicyCarrierLine__c carrierLine3 = new IBA_FFPolicyCarrierLine__c();
        carrierLine3.IBA_CarrierAccount__c = broker.Id;
        carrierLine3.IBA_PolicyTransaction__c = policyTrans3.Id;

        insert new List<IBA_FFPolicyCarrierLine__c>{
                carrierLine, carrierLine2, carrierLine3
        };

        IBA_PolicyTransactionLine__c policyLine = new IBA_PolicyTransactionLine__c();
        policyLine.IBA_PolicyTransaction__c = policyTrans.Id;
        policyLine.IBA_GrossWrittenPremium__c = 100;
        policyLine.IBA_PolicyCarrierLine__c = carrierLine.Id;
        policyLine.IBA_InsurancePremiumTax__c = 10;

        IBA_PolicyTransactionLine__c policyLine2 = new IBA_PolicyTransactionLine__c();
        policyLine2.IBA_PolicyTransaction__c = policyTrans2.Id;
        policyLine2.IBA_GrossWrittenPremium__c = 200;
        policyLine2.IBA_PolicyCarrierLine__c = carrierLine2.Id;
        policyLine2.IBA_InsurancePremiumTax__c = 10;

        IBA_PolicyTransactionLine__c policyLine3 = new IBA_PolicyTransactionLine__c();
        policyLine3.IBA_PolicyTransaction__c = policyTrans3.Id;
        policyLine3.IBA_GrossWrittenPremium__c = 200;
        policyLine3.IBA_PolicyCarrierLine__c = carrierLine3.Id;
        policyLine3.IBA_InsurancePremiumTax__c = 10;

        insert new List<IBA_PolicyTransactionLine__c>{
                policyLine, policyLine2, policyLine3
        };
        
        //insert Budget
        AZ_Budget__c budgetRec = new AZ_Budget__c();
        budgetRec.AZ_Product__c = product1.Id;
        budgetRec.AZ_BudgetYear__c = '2021';
        budgetRec.AZ_Version__c = 1;
        budgetRec.AZ_AzurCommissionNewBiz__c = 15;
        budgetRec.AZ_AzurCommissionRenewal__c = 15;
        budgetRec.AZ_Baselined__c = false;
        budgetRec.AZ_BrokerCommissionNewBiz__c = 20;
        budgetRec.AZ_PremiumAdjustmentRate__c = 20;
        budgetRec.AZ_CancellationRate__c = 20;
        budgetRec.AZ_RetentionRate__c = 10;
        budgetRec.AZ_NewBusinessRate__c = 50;
        budgetRec.AZ_Status__c = 'Draft';
        budgetRec.AZ_LatestVersion__c = true;
        insert budgetRec;

        AZ_Budget__c budgetRec2 = new AZ_Budget__c();
        budgetRec2.AZ_Product__c = product1.Id;
        budgetRec2.AZ_BudgetYear__c = '2022';
        budgetRec2.AZ_Version__c = 1;
        budgetRec2.AZ_AzurCommissionNewBiz__c = 15;
        budgetRec2.AZ_AzurCommissionRenewal__c = 15;
        budgetRec2.AZ_Baselined__c = false;
        budgetRec2.AZ_BrokerCommissionNewBiz__c = 20;
        budgetRec2.AZ_PremiumAdjustmentRate__c = 20;
        budgetRec2.AZ_CancellationRate__c = 20;
        budgetRec2.AZ_RetentionRate__c = 10;
        budgetRec2.AZ_NewBusinessRate__c = 50;
        budgetRec2.AZ_Status__c = 'Draft';
        budgetRec2.AZ_LatestVersion__c = true;
        insert budgetRec2;

        //insert Budget Line Items
        AZ_BudgetLineItem__c budgetLineItem = new AZ_BudgetLineItem__c();
        budgetLineItem.AZ_Budget__c = budgetRec.Id;
        budgetLineItem.AZ_Broker__c = broker.Id;
        budgetLineItem.AZ_JanNewBusiness__c = 200;
        budgetLineItem.AZ_FebNewBusiness__c = 200;
        budgetLineItem.AZ_MarNewBusiness__c = 200;
        budgetLineItem.AZ_AprNewBusiness__c = 200;
        budgetLineItem.AZ_MayNewBusiness__c = 200;
        budgetLineItem.AZ_JunNewBusiness__c = 200;
        budgetLineItem.AZ_JulNewBusiness__c = 200;
        budgetLineItem.AZ_AugNewBusiness__c = 200;
        budgetLineItem.AZ_SepNewBusiness__c = 200;
        budgetLineItem.AZ_OctNewBusiness__c = 200;
        budgetLineItem.AZ_NovNewBusiness__c = 200;
        budgetLineItem.AZ_DecNewBusiness__c = 200;
        budgetLineItem.AZ_JanRenewals__c = 200;
        budgetLineItem.AZ_FebRenewals__c = 200;
        budgetLineItem.AZ_MarRenewals__c = 200;
        budgetLineItem.AZ_AprRenewals__c = 200;
        budgetLineItem.AZ_MayRenewals__c = 200;
        budgetLineItem.AZ_JunRenewals__c = 200;
        budgetLineItem.AZ_JulRenewals__c = 200;
        budgetLineItem.AZ_AugRenewals__c = 200;
        budgetLineItem.AZ_SepRenewals__c = 200;
        budgetLineItem.AZ_OctRenewals__c = 200;
        budgetLineItem.AZ_NovRenewals__c = 200;
        budgetLineItem.AZ_DecRenewals__c = 200;
        insert budgetLineItem;

        AZ_BudgetLineItem__c budgetLineItem2 = new AZ_BudgetLineItem__c();
        budgetLineItem2.AZ_Budget__c = budgetRec2.Id;
        budgetLineItem2.AZ_Broker__c = broker.Id;
        budgetLineItem2.AZ_JanNewBusiness__c = 200;
        budgetLineItem2.AZ_JanRenewals__c = 200;
        budgetLineItem2.AZ_AprNewBusiness__c = 200;
        budgetLineItem2.AZ_AprRenewals__c = 200;
        insert budgetLineItem2;
        
        //insert Budget Actions
        AZ_BudgetActions__c actions = new AZ_BudgetActions__c();
        actions.AZ_Description__c = 'Test';
        actions.AZ_Due_Date__c = Date.newInstance(2021, 03, 19);
        actions.Az_Status__c = 'Open';
        actions.AZ_Budget__c = budgetRec.Id;
    }

    @IsTest
    private static void createBudgetVersionTest(){

       AZ_Budget__c budget = [SELECT Id FROM AZ_Budget__c WHERE AZ_BudgetYear__c = '2021'];
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String,Object>();
        Map<String, Object> input = new Map<String, Object>();
        
        input.put('ContextId', budget.Id); 

        // Perform test
        Test.startTest();
            AZIntermediationPipelineHelper helperTest= new AZIntermediationPipelineHelper();
            helperTest.invokeMethod('createBudgetVersion',input,output,options);
        
        Test.stopTest();
        List<AZ_Budget__c> budgets = new List<AZ_Budget__c>([SELECT Id, AZ_BudgetTotal__c FROM AZ_Budget__c WHERE AZ_BudgetYear__c = '2021']);
        System.assert(!output.isEmpty());
        System.assertEquals(2,budgets.size());
        System.assertEquals(4800,budgets[0].AZ_BudgetTotal__c);

    }

    @IsTest
    private static void generateBudgetRecordsTest(){
         Asset policy = [
                SELECT Id
                FROM Asset
                LIMIT 1];
        policy.Status = EWConstants.EW_POLICY_STATUS_ISSUED;
        update policy;

        AZ_Budget__c budget = [SELECT Id FROM AZ_Budget__c WHERE AZ_BudgetYear__c = '2022'];       
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String,Object>();
        Map<String, Object> input = new Map<String, Object>();
        
        input.put('ContextId', budget.Id); 
        // Perform test
        Test.startTest();
            AZIntermediationPipelineHelper helperTest= new AZIntermediationPipelineHelper();
            helperTest.invokeMethod('generateBudgetRecords',input,output,options);
        
        Test.stopTest();
        System.assert(!output.isEmpty());
        System.assertEquals('Success',output.get('RunBudgetStatus'));
    }

}