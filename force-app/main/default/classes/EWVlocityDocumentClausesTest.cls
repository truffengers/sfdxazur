/**
* @name: EWVlocityDocumentClausesTest
* @description: Test class for EWVlocityDocumentClauses
* @author: Steve Loftus sloftus@azuruw.com
* @date: 04/02/2019
*/
@isTest
public with sharing class EWVlocityDocumentClausesTest {

    private static User systemAdminUser;
    private static Account broker;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static vlocity_ins__DocumentClause__c documentClause;
    private static EW_ClauseAssignment__c clauseAssignment;
    private static EWVlocityDocumentClauses remoteAction;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> outputMap;

    @isTest static void GetClausesTableWithContentTest() {
        setup(true);

        Test.startTest();

        System.runAs(systemAdminUser) {
            remoteAction.invokeMethod(EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD,
                                      inputMap,
                                      outputMap,
                                      null);
        }

        Test.stopTest();

        String sectionContent = (String)outputMap.get('sectionContent');
        System.assertNotEquals(null, sectionContent, 'Should have some content');
        System.assert(sectionContent.startsWith('<table'), 'Should start with <table');
        System.assert(sectionContent.contains('testDocumentClauseName'), 'Should contain testDocumentClauseName');
    }
    
    @isTest static void GetClausesTableWithoutContentTest() {
        setup(false);

        Test.startTest();

        System.runAs(systemAdminUser) {
            remoteAction.invokeMethod(EWConstants.DOCUMENT_CUSTOM_SECTION_METHOD,
                                      inputMap,
                                      outputMap,
                                      null);
        }

        Test.stopTest();

        String sectionContent = (String)outputMap.get('sectionContent');
        System.assertNotEquals(null, sectionContent, 'Should have some content');
        System.assert(sectionContent.startsWith('<p>'), 'Should start with <p>');
    }
        
    static void setup(Boolean createClause) {

        remoteAction = new EWVlocityDocumentClauses();

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                                    EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                                    techteamUserRole.Id,
                                    true);
        
        System.runAs(systemAdminUser) {

            broker = EWAccountDataFactory.createBrokerAccount(null, true);

            insured = EWAccountDataFactory.createInsuredAccount(
                                                    'Mr',
                                                    'Test',
                                                    'Insured',
                                                    Date.newInstance(1969, 12, 29),
                                                    true);

            opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

            quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);

            if (createClause) {

                documentClause = EWDocumentClauseDataFactory.createDocumentClause(null, null, true, true);

                clauseAssignment = EWClauseAssignmentDataFactory.createClauseAssignment(documentClause.Id, quote.Id, null, true);
            }

            Map<String, Object> contextData = new Map<String, Object>();
            contextData.put('contextId', quote.Id);

            Map<String, Object> documentData = new Map<String, Object>();
            documentData.put('contextData', contextData);

            String dataJSON = JSON.serialize(documentData);

            inputMap = new Map<String, Object>();
            inputMap.put('dataJSON', dataJSON);

            outputMap = new Map<String, Object>();
        }
    }
}