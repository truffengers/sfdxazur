public without sharing class EWQuoteEditButtonController {

    public static final String QUOTE_STATUS_KEY = 'quoteStatus';
    public static final String QUOTE_TRANSACTION_KEY = 'quoteTransactionType';
    public static final String IS_COMMUNITY_KEY = 'isCommunity';
    public static final String HAS_SAVED_JSON_KEY = 'hasSavedJsonData';

    @AuraEnabled
    public static Map<String, Object> loadQuoteSetupData(Id quoteId) {
        return loadSetupData(quoteId);
    }

    @AuraEnabled
    public static Map<String, Object> loadBindSetupData(Id quoteId) {
        Map<String, Object> response = loadSetupData(quoteId);
        if (!(Boolean) response.get(HAS_SAVED_JSON_KEY)) {
            sendBindOldPolicyNotificationEmail(quoteId);
        }
        return response;
    }

    @AuraEnabled
    public static Map<String, Object> loadSetupData(Id quoteId) {
        Map<String, Object> result = new Map<String, Object>();

        Quote quote = getQuote(quoteId);

        result.put(QUOTE_STATUS_KEY, quote.Status);
        result.put(QUOTE_TRANSACTION_KEY, quote.EW_TypeofTransaction__c);
        result.put(IS_COMMUNITY_KEY, Site.getSiteId() != null);
        result.put(
                HAS_SAVED_JSON_KEY,
                !(quote.Quote_JSON_Data__r.isEmpty() || String.isEmpty(quote.Quote_JSON_Data__r.get(0).EW_CustomNode__c))
        );
        return result;
    }

    @AuraEnabled
    public static void saveQuoteCoverageSliders(Id quoteId, Decimal brokerPercentageCommission, Decimal excess, Decimal premiumGrossIncIPT) {
        Quote q = getQuote(quoteId);

        q.EW_BrokerCommissionPercent__c = brokerPercentageCommission;
        q.EW_Excess__c = excess;
        q.vlocity_ins__StandardPremium__c = premiumGrossIncIPT;
        update q;
    }

    private static Quote getQuote(Id quoteId) {
        Quote quote = [
                SELECT Id, Status, EW_TypeofTransaction__c, EW_BrokerCommissionPercent__c, EW_Excess__c, vlocity_ins__StandardPremium__c, RecordType.DeveloperName, (
                        SELECT Id, EW_CustomNode__c
                        FROM Quote_JSON_Data__r
                )
                FROM Quote
                WHERE Id = :quoteId
        ];

        return quote;
    }

    private static void sendBindOldPolicyNotificationEmail(Id quoteId) {
        EW_Global_Setting__mdt globalSettings = [
                SELECT EW_BindOldQuotesEmailDevName__c, EW_QuoteErrorNotificationRecipient__c
                FROM EW_Global_Setting__mdt
                WHERE QualifiedApiName = 'Default'
                LIMIT 1
        ];

        List<User> users = new List<User>([
                SELECT Id
                FROM User
                WHERE Name = :globalSettings.EW_QuoteErrorNotificationRecipient__c
                LIMIT 1
        ]);

        Id userToEmailId = users.get(0).Id;
        EWEmailHelper.EmailResults emailResult = EWEmailHelper.SendVlocityEmail(new EWEmailHelper.EmailOptions(
                userToEmailId, globalSettings.EW_BindOldQuotesEmailDevName__c, new List<Id>(), false, quoteId
        ));
    }

}