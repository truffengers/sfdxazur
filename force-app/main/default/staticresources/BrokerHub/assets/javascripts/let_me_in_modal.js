TT.letMeInModal = (function($) {

  'use strict';

  var $modal;
  var $iFrameContainer;
  var nagTimeoutId;
  var settings;

  var OPEN_CLASS = 'is-open';

  var CLOSED = 'closed';
  var OPENED = 'opened';

  var state;

  var DEFAULTS = {
    nagDelaySeconds : 10
  };




  function referenceElements() {
    $modal           = $('#js-let-me-in-modal');
    $iFrameContainer = $modal.find('.c-modal__panel');
  }

  function getIFrame() {
    return  $('<iframe allowtransparency="true" frameborder="0" src="' + settings.src + '" style="border: 0" type="text/html" width="100%" />');
  }

  function show() {
    state = OPENED;
    stopNagTimer();
    $modal.addClass(OPEN_CLASS);
  }

  function stopNagTimer() {
    if(nagTimeoutId !== undefined) {
      window.clearTimeout(nagTimeoutId);
      nagTimeoutId = undefined;
    }
  }




  // API

  function startTimer() {
    if(state !== CLOSED) { return; }
    nagTimeoutId = window.setTimeout(show, settings.nagDelaySeconds * 1000);
  }

  function hide() {
    if(state !== OPENED) { return; }
    state = CLOSED;
    $modal.removeClass(OPEN_CLASS);
  }

  function init(options) {
    if(state !== undefined) { return; }
    settings = $.extend({}, DEFAULTS, options || {});
    var deferred = $.Deferred();
    var $iFrame = getIFrame();
    referenceElements();
    $iFrame.on('load', function() {
      $iFrame.off('load');
      state = CLOSED;
      $iFrame.iFrameResize({ checkOrigin: false });
      deferred.resolve($iFrame);
    });
    $iFrameContainer.append($iFrame);
    return deferred;
  }



  return {
    init       : init,
    startTimer : startTimer,
    hide       : hide
  };

}(jQuery));