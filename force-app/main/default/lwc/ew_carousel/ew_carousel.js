import { LightningElement, track } from 'lwc';
import getCarouselSlides from '@salesforce/apex/EWCarouselHelper.EWCarouselHelper';

export default class Ew_carousel extends LightningElement {
    interval;
    rotateSlides = false;

    @track slides;

    connectedCallback() {
        getCarouselSlides()
            .then(slides => {
                this.slides = slides.map(slide => {
                    return {
                        ...slide,
                        active: false,
                        class: `indicator indicator-${
                            slide.cloudx_cms__Title__c
                            .toLowerCase().split(' ').join('')}`,
                        activeClass: `active indicator indicator-${
                            slide.cloudx_cms__Title__c
                            .toLowerCase().split(' ').join('')}`,
                        style: `background-image: url(${
                            slide.cloudx_cms__Image_URL__c});`
                    };
                });
                this.slides[0].active = true;
            })
            .catch(error => console.error(error)); // eslint-disable-line

        if (this.rotateSlides) {
            let currentlyActive = 0;

            this.interval = setInterval(() => { // eslint-disable-line
                this.slides = this.slides.map(slide => {
                    slide.active = false;
                    return slide;
                });
    
                if (currentlyActive === 2) {
                    this.slides[0].active = true;
                    currentlyActive = 0;
                } else {
                    this.slides[currentlyActive + 1].active = true;
                    currentlyActive++;
                }
            }, 5000);
        }
    }

    indicatorClick({ target }) {
        if (this.rotateSlides) {
            clearInterval(this.interval);
        }

        this.slides = this.slides.map(slide => {
            if (slide.cloudx_cms__Title__c === target.dataset.value) {
                slide.active = true;
                return slide;
            }
            slide.active = false;
            return slide;
        });
    }
}