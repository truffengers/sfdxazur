public class BrokerIqMyViewingHistoryController extends PortalController {

    public class CpdPair {
        public BrightTALK__Webcast__c webcast {get; set;}
        public CPD_Assessment__c  assessment {get; set;}
        
        public CpdPair(BrightTALK__Webcast__c webcast, CPD_Assessment__c  assessment) {
            this.webcast = webcast;
            this.assessment = assessment;
        }
    }
    
    public class CategoryData {
        public Category__c theCategory {get; set;}
        public String cpdHoursCompleted {get; set;}
        public Integer cpdCompletedInMinutes {get; set;}
        public List<CpdPair> historyPairs {get; set;}
        
        public CategoryData(Category__c theCategory) {
            this.theCategory = theCategory;
            this.historyPairs = new List<CpdPair>();
            this.cpdCompletedInMinutes = 0;
        }
    }
    
    public List<CategoryData> categoryData {get; set;}
    
    public BrokerIqMyViewingHistoryController() {
        List<Category__c> categories = [SELECT Id, Name, Icon__c, Colour__c FROM Category__c ORDER BY Name ASC];
        categoryData = new List<CategoryData>();
        Map<Id, CategoryData> categoryIdToData = new Map<Id, CategoryData>();
        
        for(Category__c thisCategory : categories) {
            CategoryData thisData = new CategoryData(thisCategory);            
            categoryData.add(thisData);
            categoryIdToData.put(thisCategory.Id, thisData);
        }
        
        List<BrightTALK__Webcast__c> viewedWebcasts = getViewedWebcasts();
        Nebula_Tools.sObjectIndex assessmentsByWebcast = new Nebula_Tools.sObjectIndex(
            'CPD_Test__r.IQ_Content__r.BrightTALK_Webcast__c',
            [SELECT CPD_Test__r.IQ_Content__r.BrightTALK_Webcast__c, Passed__c,
             CPD_Test__r.IQ_Content__r.CPD_Hours_In_Words__c, CPD_Test__r.IQ_Content__r.CPD_Time_minutes__c 
             FROM CPD_Assessment__c
             WHERE Contact__c  = :thisUserAsContact.Id
             ORDER BY Passed__c DESC]
        );
        for(BrightTALK__Webcast__c thisWebcast : viewedWebcasts) {
            CategoryData thisData = categoryIdToData.get(thisWebcast.IQ_Content__r.Category__c);
            if(thisData != null) {
                CPD_Assessment__c thisAssessment = (CPD_Assessment__c)assessmentsByWebcast.get(thisWebcast.Id);
                
                if(thisAssessment != null && thisAssessment.Passed__c && thisAssessment.CPD_Test__r.IQ_Content__r.CPD_Time_minutes__c != null) {
                    thisData.cpdCompletedInMinutes += (Integer)thisAssessment.CPD_Test__r.IQ_Content__r.CPD_Time_minutes__c;
                }
                thisData.historyPairs.add(new CpdPair(thisWebcast, thisAssessment));
            }
        }
        
        for(CategoryData thisData : categoryData) {
            IQ_Content__c formatter = new IQ_Content__c(CPD_Time_minutes__c = thisData.cpdCompletedInMinutes);
            formatter.recalculateFormulas();
            thisData.cpdHoursCompleted = formatter.CPD_Hours_In_Words__c;
        }
    }
    
    private List<BrightTALK__Webcast__c> getViewedWebcasts() {
        BrightTalkApi api = new BrightTalkApi();
        BrightTALK_API_Settings__c settings = api.getSettings();

        List<BrightTalkSubscriberWebcastActivity> sfWebcasts = api.getActivitiesFromSf(thisUserAsContact.Id);
        DateTime apiSince = null;
        if(!sfWebcasts.isEmpty()) {
            apiSince = sfWebcasts[sfWebcasts.size()-1].getLastUpdated();
        }
        List<BrightTalkSubscriberWebcastActivity> allWebcasts = api.getActivities(apiSince);
        allWebcasts.addAll(sfWebcasts);
        List<BrightTalkSubscriberWebcastActivity> thisUserWebcasts = new List<BrightTalkSubscriberWebcastActivity>();
        Set<Decimal> webcastIds = new Set<Decimal>();
        
        for(Integer i = allWebcasts.size() - 1; i >= 0; i--) {
            BrightTalkSubscriberWebcastActivity wc = allWebcasts[i];
            Id thisUserId = wc.getUserRealmId();
            if(thisUserId == thisUserAsContact.Id) {
                webcastIds.add(Decimal.ValueOf(wc.getWebcastId()));
                thisUserWebcasts.add(wc);
            }
        }
        Nebula_Tools.sObjectIndex webcastsById = new Nebula_Tools.sObjectIndex(
            'BrightTALK__Webcast_Id__c',
            [SELECT Id, BrightTALK__Webcast_Id__c, BrightTALK__Name__c,
             IQ_Content__r.Category__c, IQ_Content__c
             FROM BrightTALK__Webcast__c
             WHERE BrightTALK__Webcast_Id__c IN :webcastIds
             AND BrightTALK__Start_Date__c <= :DateTime.now()
            ]);
        List<BrightTALK__Webcast__c> rval = new List<BrightTALK__Webcast__c>();
        for(BrightTalkSubscriberWebcastActivity wc : thisUserWebcasts) {
            BrightTALK__Webcast__c thisDatabaseWc = (BrightTALK__Webcast__c)webcastsById.get(wc.getWebcastId());
            if(thisDatabaseWc != null) {
                rval.add(thisDatabaseWc);
            }
        }
		return rval;        
    }
    
}