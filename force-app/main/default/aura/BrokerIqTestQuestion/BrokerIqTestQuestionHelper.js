({
	testOrProgressChanged : function(component) {
		var cpdTestRecord = component.get('v.cpdTestRecord');
		var progress = component.get('v.progress');

		if(!$A.util.isEmpty(cpdTestRecord) && typeof progress !== 'undefined' && progress < cpdTestRecord.Test_Questions__r.length) {
			var currentQuestion = cpdTestRecord.Test_Questions__r[progress];
			currentQuestion.options = [];
			for(var i=1; i <= 4; i++) {
				var thisOption = currentQuestion['Option_' + i + '__c'];
				if(!$A.util.isEmpty(thisOption)) {
					currentQuestion.options.push(thisOption);
				}
			}

			component.set('v.currentQuestion', currentQuestion);
			var cpdAssessment = component.get('v.cpdAssessment');

			if(!$A.util.isEmpty(cpdAssessment)) {
				this.setCurrentAnswer(component, cpdAssessment, currentQuestion);
			} else {
				this.loadCpdAssessment(component, cpdTestRecord);
			}
		}		
	},
	setCurrentAnswer : function(component, cpdAssessment, currentQuestion) {
		component.set('v.currentAnswer', 
		              cpdAssessment.CDP_Answers__r.find(function(thisAnswer) { return thisAnswer.CPD_Test_Question__c === currentQuestion.Id; }));

	},
	loadCpdAssessment: function(component, cpdTestRecord) {
		var helper = this;
		var getAssessmentAction = component.get('c.getAssessment');

		getAssessmentAction.setParam('cpdTestId', cpdTestRecord.Id);

		getAssessmentAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	var cpdAssessment = response.getReturnValue();
            	component.set('v.cpdAssessment', cpdAssessment);
            	helper.setCurrentAnswer(component, cpdAssessment, component.get('v.currentQuestion'));
            }
        });
        
        $A.enqueueAction(getAssessmentAction);

	},
	saveCurrentAnswer : function(component, progress, resolve, reject) {
		var saveAnswerAction = component.get('c.saveAnswer');

		saveAnswerAction.setParam('currentAnswer', component.get('v.currentAnswer'));

		saveAnswerAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set('v.progress', progress + 1);
            	if(resolve) {
                        resolve('saveCurrentAnswer succeeded');
                }
            } else {
            	//TODO show an error
            	if(reject) {
            		reject(Error(response.getError()));
            	}
            }
        });
        $A.enqueueAction(saveAnswerAction);
	},
	submitCpdAssessment : function(component, cpdAssessment, resolve, reject) {
		var submitCpdAssessmentAction = component.get('c.submitCpdAssessment');

		submitCpdAssessmentAction.setParam('cpdAssessment', cpdAssessment);

		submitCpdAssessmentAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
            	if(resolve) {
                        resolve('submitCpdAssessment succeeded');
                }
            } else {
            	//TODO show an error
            	if(reject) {
            		reject(Error(response.getError()));
            	}
            }
        });

        $A.enqueueAction(submitCpdAssessmentAction);
	},
	helperFunctionAsPromise : function(helperFunction) {
		var helperFunctionArgs = Array.prototype.slice.call(arguments, 1);
        return new Promise($A.getCallback(function(resolve, reject) {
            helperFunction.apply(this, helperFunctionArgs.concat([resolve, reject]));
        }));
    }
})