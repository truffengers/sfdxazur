/*
* @name: EWVlocityGetPricebookEntries
* @description: Get pricebook entry ids from a pricebook
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 06/02/2018
* @modifiedBy:
*/
global with sharing class EWVlocityPricebookEntries implements vlocity_ins.VlocityOpenInterface{
    private Map<String,Object> outputClassParam; //It is created to test the result in the apex test 
    
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Boolean result = false;
        try {
            if(String.isNotBlank(methodName)){
                setOutputPricebookEntries(options, outMap);
                setOutputClassParam(outMap);
                result = true;
            }
        }   
        catch(Exception e) {
            system.debug('error is '+ e.getMessage());
        }
        return result;
    }
    
    private void setOutputPricebookEntries(Map<String,Object> options, Map<String,Object> outMap){
        String pricebookName = getPricebookName(options);
		list<PricebookEntry> pricebookEntries = getPricebookEntries(pricebookName);
        for(PricebookEntry pricebookEntry: pricebookEntries){
            outMap.put(getOutputPricebookName(pricebookEntry.Product2.Name),  pricebookEntry.Id);
        }
    }
    
    private String getPricebookName(Map<String,Object> options){
        String result = String.valueOf(options.get('pricebookName'));
        return result;
    }
    
    private list<PricebookEntry> getPricebookEntries(String pricebookName){
        return [SELECT Id, Pricebook2Id, Product2Id, Product2.Name FROM PricebookEntry WHERE Pricebook2.Name =: pricebookName AND Pricebook2.isActive = true];
    }
    
    private string getOutputPricebookName(String productName){
        return productName + 'PBId';
    }
    
    private void setOutputClassParam(Map<String,Object> outMap){
        outputClassParam = new Map<String,Object>(outMap);
    }
    
    public Map<String,Object> getOutputClassParam(){
        return outputClassParam;
    }
}