import { LightningElement, api, track } from 'lwc';
import { analyticsEvent } from 'c/ix_utils';
import { COLLAPSIBLE_CLASSES } from 'c/ix_constants';

export default class Ix_AccordionItem extends LightningElement {
    @track accordionOpen;

    @api isRichText;
    @api hasSubheading;
    @api itemDepth;
    @api itemTitle;
    @api itemSubheading;
    @api itemValue;
    @api isParent;
    @api searchQuery;
    @api currentlySelectedItem;

    renderedCallback() {
        // Render rich text onto the page as innerHTML
        if (this.accordionOpen && !this.hasSubheading
            && !this.isParent && this.isRichText) {
            this.template.querySelector('.rich-text-container').innerHTML
                = this.itemValue;
        }
    }

    toggleAccordion() {
        this.accordionOpen = !this.accordionOpen;

        // HACK: Because sometimes when you open an accordion the page would
        // randomly scroll, set `accordionOpen` on the localStorage to know
        // when an accordion is open. `accordionOpen` is gotten in `searchBox`.
        if (this.accordionOpen) {
            window.localStorage.setItem('accordionOpen');
        } else {
            window.localStorage.setItem('accordionClosed');
        }

        if (this.currentlySelectedItem) {
            // The item is displayed as a search result
            if (this.accordionOpen) {
                analyticsEvent({
                    eventName: 'Accordion Opened',
                    eventData: this.itemTitle,
                    eventDescription: `"${this.itemTitle}" accordion was opened
                    while browsing the item
                    "${this.currentlySelectedItem._name}"`
                })
                    .catch(() => null);
            } else {
                analyticsEvent({
                    eventName: 'Accordion Closed',
                    eventData: this.itemTitle,
                    eventDescription: `"${this.itemTitle}" accordion was closed
                    while browsing the item
                    "${this.currentlySelectedItem._name}"`
                })
                    .catch(() => null);
            }
        } else {
            // The accordion was opened as part of some sort of general
            // information (eg. General Conditions or General Exclusions)
            if (this.accordionOpen) {
                analyticsEvent({
                    eventName: 'Accordion Opened',
                    eventData: this.itemTitle,
                    eventDescription: `"${this.itemTitle}" general information
                    accordion was opened.`
                })
                    .catch(() => null);
            } else {
                analyticsEvent({
                    eventName: 'Accordion Closed',
                    eventData: this.itemTitle,
                    eventDescription: `"${this.itemTitle}" general information
                    accordion was closed.`
                })
                    .catch(() => null);
            }
        }
    }

    get collapsibleClass() {
        return COLLAPSIBLE_CLASSES[this.itemDepth];
    }

    get accordionBodyClass() {
        return this.itemDepth > 2
            ? 'accordion-body indented' : 'accordion-body';
    }
}