window.shouldBreak = false;
window.alertShownAlready = false;
baseCtrl.prototype.activateOnPageLeaveAlert = function() {
    $(document).ready(function() {
        var mouseY = 0;
        var topValue = 0;
        window.parent.addEventListener("mouseout", function(e) {
                mouseY = e.clientY;
                if (!window.alertShownAlready && mouseY < topValue) {
                    window.alertShownAlready = true;
                    alert("Please make sure you have downloaded the Policy document or emailed it to yourself before leaving this page.");
                }
            },
            false);
    });
}

baseCtrl.prototype.setChecker = function(obj) {
    window.failsafeChecker = setInterval(function() {

            if (window.shouldBreak) {
                clearInterval(window.failsafeChecker);
            }

            let iframes = $('iframe');
            if (iframes !== undefined && iframes.length > 0) {
            let iframe = iframes[0];
            iframe.style.display = "inline-block"; 
            var button1 = iframe.contentWindow.document.getElementsByTagName("button")[0];
            button1.style.display = "none";
            console.log('element hidden');
            var button2 = iframe.contentWindow.document.getElementsByTagName("button")[2];
            button2.style.display = "none";
            var button3 = iframe.contentWindow.document.getElementsByTagName("button")[3];
            button3.style.display = "none";
            var button4 = iframe.contentWindow.document.getElementsByTagName("button")[4];
            button4.innerHTML = 'Done';
    
                let alreadyGeneratedButtonGroup = iframe.contentWindow.document.getElementsByClassName('objdoc-generate-doc-button-group')[0];
                let stillToGenerateButton = iframe.contentWindow.document.getElementsByClassName('objdoc-generate-doc-single-btn')[0];

                let skipThisRunNotReady = (alreadyGeneratedButtonGroup === undefined || stillToGenerateButton === undefined);

                if (!skipThisRunNotReady) {
                    let isActive_alreadyGeneratedButtonGroup = alreadyGeneratedButtonGroup.className.includes('active');
                    let isActive_stillToGenerateButton = stillToGenerateButton.className.includes('active');

                    let quoteSheet = document.getElementById('quoteSheet');
                    let quoteSheetAriaHidden = quoteSheet.getAttribute("aria-hidden");
                    let quoteSheetVisible = ((quoteSheetAriaHidden === 'false') || (quoteSheetAriaHidden === false));

                    let shouldSkip = (isActive_alreadyGeneratedButtonGroup && !isActive_stillToGenerateButton) || !quoteSheetVisible;

                    if (!shouldSkip && !window.shouldBreak) {

                        let doc = iframe.contentWindow.document;
                        let buttonElements = doc.getElementsByClassName("objdoc-generate-doc-single-btn");
                        if (buttonElements.length > 0) {
                            if (!window.shouldBreak) {
                                window.shouldBreak = true;
                                clearInterval(window.failsafeChecker);
                                buttonElements[0].click();
                            }
                        }

                        let objDocBlockPdfElements = doc.getElementsByClassName("objdoc-block-pdf");
                        if (objDocBlockPdfElements.length > 0) {
                            objDocBlockPdfElements[0].setAttribute("style", "pointer-events:none");
                        }

                        let objDocPdfButtonsElements = doc.getElementsByClassName("objdoc-generate-doc-button-group");
                        if (objDocPdfButtonsElements.length > 0) {
                            objDocPdfButtonsElements[0].setAttribute("style", "pointer-events: all !important;");
                        }
                    }
                }
            }
        },
        5000
    );
};

window.trimJSON = function(input, control, scp, actionCtrl) {
    debugger;
    var keyList = ['name', 'niceName', 'id'];
    var result = input.data;
    var newResult = [];
    if (result && angular.isArray(result)) {
        for (var i = 0; i < result.length; i++) {
            var arrayEle = {};
            for (var key in result[i]) {
                if (result[i].hasOwnProperty(key) && keyList.indexOf(key) >= 0) {
                    arrayEle[key] = result[i][key];
                }
            }
            newResult.push(arrayEle);
        }
    }
    return newResult;
}

function checkObjectNumber(retArr, typeAheadKey) {
    if (retArr && retArr.length > 0 && angular.isNumber(retArr[0][typeAheadKey])) {
        for (var i = 0; i < retArr.length; i++)
            retArr[i][typeAheadKey] = String(retArr[i][typeAheadKey]);
    }
    return retArr;
}

function dataProcessorFunc(result, control, scp, actionCtrl) {
    debugger
    var resp = angular.copy(result);
    var returnVal = [];
    if (angular.isArray(resp)) {
        for (var i = 0; i < resp.length; i++) {
            resp[i].ratedDriverName = resp[i].driverFullName;
            resp[i].ratedDriverBIPD = resp[i].driverBIPD;
            resp[i].ratedDriverMED = resp[i].driverMED;
            resp[i].ratedDriverUM = resp[i].driverUM;
            resp[i].ratedDriverCCD = resp[i].driverCCD;
            resp[i].ratedDriverCOLL = resp[i].driverCOLL;
        }
        return resp;
    } else {
        resp.ratedDriverName = resp.driverFullName;
        resp.ratedDriverBIPD = resp.driverBIPD;
        resp.ratedDriverMED = resp.driverMED;
        resp.ratedDriverUM = resp.driverUM;
        resp.ratedDriverCCD = resp.driverCCD;
        resp.ratedDriverCOLL = resp.driverCOLL;
        returnVal[0] = resp;
        return returnVal;
    }
}

baseCtrl.prototype.setIPScope = function(scp) {
    window.VlocOmniSI = scp;
    var afterSlash = '/' + window.location.href.split('.com/')[1].split('/')[0];
    if (afterSlash === 'apex') {
        afterSlash = '';
    }
    //scp.urlPrefix = window.location.origin + afterSlash;
    scp.applyCallResp({
        'urlPrefix': window.location.origin + afterSlash
    });
    //console.log('urlPrefix ', scp.urlPrefix);
 
}

window.addEventListener('message', function(event) {
    console.log('message received from iframe');
    //if (event.origin === '/apex/ObjectDocumentCreation2'){
    //debugger;
    //document.getElementById('response').innerHTML = event.data;
    if (event.data && event.data.constructor === Object && event.data.hasOwnProperty("docGenAttachmentId")) {
        window.VlocOmniSI.applyCallResp(event.data);

        if (event.data && event.data.docGenAttachmentId) {
            window.VlocOmniSI.activateButton('emailPolicy');
            window.VlocOmniSI.activateButton('viewPolicyAction');
            window.VlocOmniSI.activateButton('homeAction');
        }
        console.log('[event.data]: ', event.data);
    }
}, false);

baseCtrl.prototype.initListeners = function() {
    let objDocEmailButton = document.getElementById("emailPolicy");
    objDocEmailButton.addEventListener("click", function(e) {
        window.alertShownAlready = true;
    });
}

baseCtrl.prototype.$scope.clickGenerateButtonInIFrameOnloadFunc = function() {
    document.getElementById('obj-doc-creation-os-iframe').onload = function() {
        let doc = this.contentDocument ? this.contentDocument : this.contentWindow.document;

        let buttonElements = doc.getElementsByClassName("objdoc-generate-doc-single-btn");
        if (buttonElements.length > 0) {
            buttonElements[0].click();
            window.shouldBreak = true;
            clearInterval(window.failsafeChecker);
        }

        let objDocBlockPdfElements = doc.getElementsByClassName("objdoc-block-pdf");
        if (objDocBlockPdfElements.length > 0) {
            objDocBlockPdfElements[0].setAttribute("style", "pointer-events:none");
        }

        let objDocPdfButtonsElements = doc.getElementsByClassName("objdoc-generate-doc-button-group");
        if (objDocPdfButtonsElements.length > 0) {
            objDocPdfButtonsElements[0].setAttribute("style", "pointer-events: all !important;");
        }

        let objDocPdfDownloadButton = doc.getElementsByClassName("objdoc-generate-doc-btn-download");
        if (objDocPdfButtonsElements.length > 0) {
            objDocPdfDownloadButton[0].addEventListener("click", function(e) {
                window.alertShownAlready = true;
            });
        }
    }
}

baseCtrl.prototype.$scope.activateButton = function(buttonId) {
    var emailQuoteButton = document.getElementById(buttonId);
    emailQuoteButton.setAttribute("style", "pointer-events: auto; background-color: #3EB5D5;");
}