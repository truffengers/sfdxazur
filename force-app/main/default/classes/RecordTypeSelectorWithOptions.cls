/*
* @name: RecordTypeSelectorWithOptions
* @description:  Used to call the recordType ids with the use of options
* @author: Dimitris Schizas dschizas@azuruw.com
* @date: 11/04/2018
* @modifiedBy:
*/
global with sharing class RecordTypeSelectorWithOptions implements vlocity_ins.VlocityOpenInterface {

    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Boolean result = false;
        
        try {
            if(String.isNotBlank(methodName)){
                for(String optionKey: options.keySet()){
                	String objectType = String.valueOf(options.get(optionKey));                
                    recordTypeSearch(inputMap, outMap, objectType);
                }
                result = true;
            }
        }   
        catch(Exception e) {
            system.debug('error is '+e.getMessage());
        }
        system.debug('****result is ' +result);
        return result;
    }
    
	private void recordTypeSearch(Map<String,Object> inputMap, Map<String,Object> outMap, string objectType) {
    	List<RecordType> recordTypeList = new List<RecordType>([SELECT Id, DeveloperName, sObjectType FROM RecordType WHERE sObjectType =: objectType]);
        system.debug(recordTypeList);
        for(RecordType r : recordTypeList) {
            outMap.put(r.DeveloperName, r.Id);
        }
    }
}