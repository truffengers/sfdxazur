/*
* @name: EWFinancialForceHelper
* @description: Handles Policy Transactions and line items for Smarthome policies.
* @author: bartoszdec
* @date: 12/04/2019
* @lastChangedBy: Antigoni D'Mello  06/05/2021  PR-138: Set Azur DD Fee to zero for non direct debit PTLs
*/
public without sharing class EWFinancialForceHelper {

    public static final String JSON_KEY_GWP = 'premiumGrossExIPT';
    public static final String JSON_KEY_BROKER_COMMISSION = 'brokerCommissionValue';
    public static final String JSON_KEY_IPT_TOTAL = 'iptTotal';
    public static final String JSON_KEY_CARRIER_FEE = 'premiumNet';

    private static final Map<String,String> transactionTypeMap = new Map<String,String>{
            'New Business' => 'ORIGINAL PREMIUM',
            'MTA' => 'ENDORSEMENT',
            'Renewal' => 'MANUAL RENEWAL',
            'Cancellation' => 'CANCELLATION'
    };
    //added by Aleksejs Jedamenko on 13/05/2020 to secure population of PHO_PolicyTransactionType__c field
    private static final Map<String, String> policyTransactionTypeMap = new Map<String, String>{
        
            'New Business' => 'New Business',
            'MTA' => 'Mid Term Adjustment',
            'Renewal' => 'Renewal',
            'Cancellation' => 'Cancellation'
    };

    private static Map<String, String> classOfBusinessMapping = new Map<String, String>{
            'emergingWealth' => 'Azur Emerging Wealth'
    };

    private static Map<String, Decimal> foreignTaxByISOCode = new Map<String, Decimal>{
            'GBP' => 0.0
    };

    // v2: 29/05/2020 ajedamenko@azuruw.com getPoliciesIdsByNewStatus method signature changed to use Set<String> instead of String, now method processes Policies with statuses Issued and Pending Cancellation
    public static void processIssuedPolicies(Map<Id, SObject> oldItems, Map<Id, SObject> newItems) {
        processIssuedPolicies(getPoliciesIdsByNewStatus(oldItems, newItems, new Set<String> {EWConstants.EW_POLICY_STATUS_ISSUED, EWConstants.EW_POLICY_STATUS_PENDING_CANCELLATION}));
    }

    public static void processIssuedPolicies(Set<Id> issuedPoliciesIds) {
        processIssuedPolicies(issuedPoliciesIds, new Map<Id, IBA_PolicyTransaction__c>());
    }

    public static void processIssuedPolicies(Set<Id> issuedPoliciesIds, Map<Id, IBA_PolicyTransaction__c> lastTransactionMap) {
        Map<Id, Asset> policiesWithCoverages = getPoliciesWithCoverages(issuedPoliciesIds);
        Map<Id, List<QuoteLineItem>> quoteLineItemsByPolicyId = getQuoteLineItemsByPolicyId(policiesWithCoverages);

        createFFRecordsForPolicies(policiesWithCoverages, quoteLineItemsByPolicyId, lastTransactionMap);
    }
// v2: 17/06/2020 Aleksejs Jedamenko ajedamenko@azuruw.com - logic for cancellation fixed price transaction line added
// v3: 08/09/2020 Aleksejs Jedamenko ajedamenko@azuruw.com - policy.EW_Quote__r.Status == EWConstants.QUOTE_STATUS_REVERSAL condition added to fixed expense trasaction line logic and PHO_PolicyTransactionType__c transaction field value calculation
    public static void createFFRecordsForPolicies(
            Map<Id, Asset> policiesWithCoverages,
            Map<Id, List<QuoteLineItem>> quoteLineItemsByPolicyId,
            Map<Id, IBA_PolicyTransaction__c> lastTransactionMap
    ) {
        Savepoint sp = Database.setSavepoint();

        try {
            /** ------------------------------------------------- **/
            /** --------------------- Setup --------------------- **/
            Set<Id> issuedPoliciesIds = policiesWithCoverages.keySet();
            Product2 fixedExpenseProduct = getFixedExpenseProduct();
            Map<String, EW_Coverages__c> coverageByNameMap = EW_Coverages__c.getAll();
            String mgaFeeAccId='';
            List<Account> mgaFeeAccList=[SELECT Id FROM Account WHERE EW_DRCode__c= :EWConstants.MGA_FEE_ACCOUNT_DR_CODE LIMIT 1];
            if(!mgaFeeAccList.isEmpty()){
                mgaFeeAccId=mgaFeeAccList[0].Id;                
            }
            
            List<IBA_PolicyTransaction__c> policyTransactions = new List<IBA_PolicyTransaction__c>();
            List<IBA_PolicyTransactionLine__c> policyTransactionLines = new List<IBA_PolicyTransactionLine__c>();
            List<IBA_FFPolicyCarrierLine__c> policyCarrierLines = new List<IBA_FFPolicyCarrierLine__c>();
            List<c2g__codaDimension1__c> policyDimensions = new List<c2g__codaDimension1__c>();
            Map<Id, IBA_PolicyTransaction__c> policyTransactionsByPolicyIds = new Map<Id, IBA_PolicyTransaction__c>();

            /** ------------------------------------------------- **/
            /** -------------- Policy Transactions -------------- **/
            for (Id policyId : issuedPoliciesIds) {
                Asset policy = policiesWithCoverages.get(policyId);
                String policyTransactionType = lastTransactionMap.containsKey(policyId) ? lastTransactionMap.get(policyId).IBA_ActivePolicy__r.EW_TypeofTransaction__c : policy.EW_TypeofTransaction__c;
                String policyTransactionTypePHO = policy.EW_Quote__r.Status == EWConstants.QUOTE_STATUS_REVERSAL && policy.EW_Quote__r.EW_TypeofTransaction__c !=null ? policy.EW_Quote__r.EW_TypeofTransaction__c : policyTransactionType;
                IBA_PolicyTransaction__c policyTransaction = new IBA_PolicyTransaction__c();
                policyTransaction.IBA_ClassofBusiness__c = classOfBusinessMapping.containsKey(policy.ProductCode) ? classOfBusinessMapping.get(policy.ProductCode) : policy.ProductCode;

                String originalPolicyBusinessType = findOriginalPolicyBusinessType(policyId);
                policyTransaction.IBA_BusinessType__c = originalPolicyBusinessType;
                //Added by Rajni Bajpai - EWH-2057
                if(String.isnotBlank(mgaFeeAccId))
                policyTransaction.IBA_MGAFeeAccountID__c = mgaFeeAccId;
                
                policyTransaction.IBA_InsuredName__c = policy.IBA_InsuredName__c;
                policyTransaction.IBA_Migration__c = false;
                policyTransaction.IBA_TransactionDate__c = (Datetime) policy.vlocity_ins__EffectiveDate__c;
                policyTransaction.IBA_TransactionType__c = transactionTypeMap.containsKey(policyTransactionTypePHO) ? transactionTypeMap.get(policyTransactionTypePHO) : null;
                policyTransaction.IBA_EffectiveDate__c = policy.vlocity_ins__EffectiveDate__c;
                policyTransaction.IBA_AIGClassofBusiness__c = EWConstants.DEFAULT_AIG_CLASS_FOR_BUSINESS_FOR_FF_TRANSACTION_RECORD;
                policyTransaction.IBA_ActivePolicy__c = policyId;
                policyTransaction.IBA_OriginalPolicy__c = policyId;
                policyTransaction.EW_Reversed_Policy_Transaction__c = lastTransactionMap.containsKey(policyId) ?  lastTransactionMap.get(policyId).Id : null ;
                if(policyTransaction.EW_Reversed_Policy_Transaction__c != null) {
                    policyTransaction.IBA_TransactionDate__c = (Datetime) lastTransactionMap.get(policyId).IBA_EffectiveDate__c;
                    policyTransaction.IBA_EffectiveDate__c = lastTransactionMap.get(policyId).IBA_EffectiveDate__c;
                }
                //added by Aleksejs Jedamenko on 13/05/2020 to secure population of PHO_PolicyTransactionType__c field
                policyTransaction.PHO_PolicyTransactionType__c = policyTransactionTypeMap.containsKey(policyTransactionTypePHO) ? policyTransactionTypeMap.get(policyTransactionTypePHO) : null;
                policyTransactions.add(policyTransaction);
                policyTransactionsByPolicyIds.put(policyId, policyTransaction);
            }

            insert policyTransactions;

            /** -------------------------------------------------- **/
            /** -------------- Policy Carrier Lines -------------- **/

            Map<Id, Map<Id, IBA_FFPolicyCarrierLine__c>> policyToCarrierLinesByCarrierIds = new Map<Id, Map<Id, IBA_FFPolicyCarrierLine__c>>();

            for (Id policyId : issuedPoliciesIds) {
                Asset policy = policiesWithCoverages.get(policyId);
                List<vlocity_ins__AssetCoverage__c> policyCoverages = policy.vlocity_ins__PolicyCoverages__r;
                IBA_PolicyTransaction__c policyTransaction = policyTransactionsByPolicyIds.get(policyId);

                Map<Id, IBA_FFPolicyCarrierLine__c> carrierLinesByCarrierIds = new Map<Id, IBA_FFPolicyCarrierLine__c>();

                for (vlocity_ins__AssetCoverage__c coverage : policyCoverages) {
                    String coverageKey = coverage.vlocity_ins__Product2Id__r.Name + '-' + EWConstants.CURRENCY_ISO_CODE_GBP;
                    Id carrierId = coverageByNameMap.get(coverageKey).CarrierId__c;
                    carrierLinesByCarrierIds.put(
                            carrierId,
                            new IBA_FFPolicyCarrierLine__c(
                                    IBA_CarrierAccount__c = carrierId,
                                    IBA_PolicyTransaction__c = policyTransaction.Id
                            )
                    );
                }

                policyCarrierLines.addAll(carrierLinesByCarrierIds.values());
                policyToCarrierLinesByCarrierIds.put(policyId, carrierLinesByCarrierIds);
            }

            insert policyCarrierLines;

            /** ------------------------------------------------------ **/
            /** -------------- Policy Transaction Lines -------------- **/
            List<IBA_FFPolicyCarrierLine__c> carrierLinesToDelete = new List<IBA_FFPolicyCarrierLine__c>();

            for (Id policyId : issuedPoliciesIds) {
                Asset policy = policiesWithCoverages.get(policyId);
                List<vlocity_ins__AssetCoverage__c> policyCoverages = policy.vlocity_ins__PolicyCoverages__r;
                List<QuoteLineItem> quoteLineItems = quoteLineItemsByPolicyId.containsKey(policyId) ? quoteLineItemsByPolicyId.get(policyId) : new List<QuoteLineItem>();
                IBA_PolicyTransaction__c policyTransaction = policyTransactionsByPolicyIds.get(policyId);

                Set<Id> usedCarriers = new Set<Id>();

                Decimal grossWrittenPremiumCoverages = 0;
                Decimal brokerCommissionCoverages = 0;
                Decimal insurancePremiumTaxCoverages = 0;
                Decimal carrierFeeCoverages = 0;

                for (QuoteLineItem qli : quoteLineItems) {
                    String coverageKey = qli.Product2.Name + '-' + EWConstants.CURRENCY_ISO_CODE_GBP;
                    Id carrierId = coverageByNameMap.get(coverageKey).CarrierId__c;

                    IBA_PolicyTransactionLine__c ptl = new IBA_PolicyTransactionLine__c();
                    ptl.IBA_GrossWrittenPremium__c = (qli.EW_Transactional_PremiumGrossExIPT__c != null) ? qli.EW_Transactional_PremiumGrossExIPT__c.setScale(2) : 0;

                    if (ptl.IBA_GrossWrittenPremium__c == 0) {
                        continue;
                    }

                    ptl.IBA_DuefromAccountingBroker__c = 0;
                    ptl.IBA_PolicyTransaction__c = policyTransaction.Id;
                    ptl.IBA_ForeignTax__c = foreignTaxByISOCode.get(qli.CurrencyIsoCode);
                    ptl.IBA_BrokerCommission__c = (qli.EW_Transactional_BrokerCommissionValue__c != null) ? qli.EW_Transactional_BrokerCommissionValue__c.setScale(2) : 0;
                    ptl.IBA_CarrierFee__c = (qli.EW_Transactional_PremiumNet__c != null) ? qli.EW_Transactional_PremiumNet__c.setScale(2) : 0;
                    ptl.IBA_InsurancePremiumTax__c = (qli.EW_Transactional_IPT__c != null) ? qli.EW_Transactional_IPT__c.setScale(2) : 0;
                    ptl.IBA_MGACommission__c = ptl.IBA_GrossWrittenPremium__c - ptl.IBA_BrokerCommission__c - ptl.IBA_CarrierFee__c;
                    ptl.IBA_DuetoMGA__c = ptl.IBA_MGACommission__c;
                    ptl.IBA_NetDuefromBroker__c = ptl.IBA_GrossWrittenPremium__c - ptl.IBA_BrokerCommission__c + ptl.IBA_InsurancePremiumTax__c + ptl.IBA_ForeignTax__c;
                    ptl.IBA_NetPayabletoCarrier__c = ptl.IBA_CarrierFee__c + ptl.IBA_InsurancePremiumTax__c + ptl.IBA_ForeignTax__c;
                    ptl.IBA_PolicyCarrierLine__c = policyToCarrierLinesByCarrierIds.get(policyId).get(carrierId).Id;
                    ptl.IBA_Product__c = qli.Product2Id;
                    ptl.IBA_AzurDDFee__c = 0;
					
					// ------------------ Fix for Direct Debit -------------------------------------------
                    if(policy.EW_Payment_Method__c == EWConstants.DIRECT_DEBIT_PAYMENT_METHOD){
                        ptl.IBA_AzurDDFee__c = (0.0105 * (ptl.IBA_GrossWrittenPremium__c + ptl.IBA_InsurancePremiumTax__c + ptl.IBA_ForeignTax__c)).setScale(2); 
                        ptl.IBA_PCLDDFee__c = (0.0495 * (ptl.IBA_GrossWrittenPremium__c + ptl.IBA_InsurancePremiumTax__c + ptl.IBA_ForeignTax__c)).setScale(2); 
                        ptl.IBA_TotalDirectDebitFee__c = (ptl.IBA_GrossWrittenPremium__c + ptl.IBA_InsurancePremiumTax__c + ptl.IBA_ForeignTax__c) * policy.EW_Quote__r.EW_DD_Interest_Percentage__c / 100;
                        ptl.IBA_DuefromAccountingBroker__c = ptl.IBA_GrossWrittenPremium__c + ptl.IBA_AzurDDFee__c + ptl.IBA_InsurancePremiumTax__c + ptl.IBA_ForeignTax__c; 
                        ptl.IBA_DuetoMGA__c = ptl.IBA_MGACommission__c + ptl.IBA_AzurDDFee__c;
                        ptl.IBA_NetDuefromBroker__c = 0 - ptl.IBA_BrokerCommission__c;
                    }
                    
                    grossWrittenPremiumCoverages += ptl.IBA_GrossWrittenPremium__c;
                    brokerCommissionCoverages += ptl.IBA_BrokerCommission__c;
                    insurancePremiumTaxCoverages += ptl.IBA_InsurancePremiumTax__c;
                    carrierFeeCoverages += ptl.IBA_CarrierFee__c;

                    usedCarriers.add(ptl.IBA_PolicyCarrierLine__c);
                    policyTransactionLines.add(ptl);
                }

                // ------------------ Fix for FixedExpenses Transaction Line -------------------------------------------
                String feCoverageKey = EWConstants.FIXED_EXPENSE_PRODUCT_NAME + '-' + EWConstants.CURRENCY_ISO_CODE_GBP;
                Id feCarrierId = coverageByNameMap.get(feCoverageKey).CarrierId__c;
                Decimal grossWrittenPremiumTotal;
                Decimal brokerCommissionTotal;
                Decimal insurancePremiumTaxTotal;
                Decimal carrierFeeTotal;
                if (policy.vlocity_ins__AttributeSelectedValues__c != null
                        && (policy.EW_TypeofTransaction__c == EWConstants.POLICY_TRANSACTION_TYPE_NB || policy.EW_TypeofTransaction__c == EWConstants.POLICY_TRANSACTION_TYPE_RENEWAL)
                ) {

                    Map<String, Object> calculatedPriceData = (Map<String, Object>) JSON.deserializeUntyped(policy.vlocity_ins__AttributeSelectedValues__c);
	
                    grossWrittenPremiumTotal = (Decimal) calculatedPriceData.get(JSON_KEY_GWP) * (policy.Status == EWConstants.EW_POLICY_STATUS_REVERSAL || policy.EW_Quote__r.Status == EWConstants.QUOTE_STATUS_REVERSAL ? -1 : 1);
                    brokerCommissionTotal = (Decimal) calculatedPriceData.get(JSON_KEY_BROKER_COMMISSION) * (policy.Status == EWConstants.EW_POLICY_STATUS_REVERSAL || policy.EW_Quote__r.Status == EWConstants.QUOTE_STATUS_REVERSAL ? -1 : 1);
                    insurancePremiumTaxTotal = (Decimal) calculatedPriceData.get(JSON_KEY_IPT_TOTAL) * (policy.Status == EWConstants.EW_POLICY_STATUS_REVERSAL || policy.EW_Quote__r.Status == EWConstants.QUOTE_STATUS_REVERSAL ? -1 : 1);
                    carrierFeeTotal = (Decimal) calculatedPriceData.get(JSON_KEY_CARRIER_FEE) * (policy.Status == EWConstants.EW_POLICY_STATUS_REVERSAL || policy.EW_Quote__r.Status == EWConstants.QUOTE_STATUS_REVERSAL ? -1 : 1);
                }

                if (policy.EW_TypeofTransaction__c == EWConstants.POLICY_TRANSACTION_TYPE_CANCELLATION) {

                    QuoteLineItem qliForCancellationFixedPrice = [SELECT Id, EW_Transactional_PremiumGrossExIPT__c, EW_Transactional_BrokerCommissionValue__c,	
                                                                    EW_Transactional_IPT__c, EW_Transactional_PremiumNet__c
                                                                    FROM QuoteLineItem 
                                                                    WHERE QuoteId =:quoteLineItems[0].QuoteId AND vlocity_ins__ProductName__c=:EWConstants.EW_PROD_NAME_EMERGING_WEALTH LIMIT 1];
                    grossWrittenPremiumTotal = qliForCancellationFixedPrice.EW_Transactional_PremiumGrossExIPT__c;
                    brokerCommissionTotal = qliForCancellationFixedPrice.EW_Transactional_BrokerCommissionValue__c;
                    insurancePremiumTaxTotal = qliForCancellationFixedPrice.EW_Transactional_IPT__c;
                    carrierFeeTotal = qliForCancellationFixedPrice.EW_Transactional_PremiumNet__c;
                }
                if (grossWrittenPremiumTotal != null && brokerCommissionTotal != null && insurancePremiumTaxTotal != null && carrierFeeTotal != null) {
                    IBA_PolicyTransactionLine__c fePtl = new IBA_PolicyTransactionLine__c();
                    fePtl.IBA_GrossWrittenPremium__c = grossWrittenPremiumTotal - grossWrittenPremiumCoverages;
                    fePtl.IBA_DuefromAccountingBroker__c = 0;
                    fePtl.IBA_PolicyTransaction__c = policyTransaction.Id;
                    fePtl.IBA_ForeignTax__c = foreignTaxByISOCode.get(policy.CurrencyIsoCode);
                    fePtl.IBA_BrokerCommission__c = brokerCommissionTotal - brokerCommissionCoverages;
                    fePtl.IBA_InsurancePremiumTax__c = insurancePremiumTaxTotal - insurancePremiumTaxCoverages;
                    fePtl.IBA_CarrierFee__c = carrierFeeTotal - carrierFeeCoverages;
                    fePtl.IBA_MGACommission__c = fePtl.IBA_GrossWrittenPremium__c - fePtl.IBA_BrokerCommission__c - fePtl.IBA_CarrierFee__c;
                    fePtl.IBA_DuetoMGA__c = fePtl.IBA_MGACommission__c;
                    fePtl.IBA_NetDuefromBroker__c = fePtl.IBA_GrossWrittenPremium__c - fePtl.IBA_BrokerCommission__c + fePtl.IBA_InsurancePremiumTax__c + fePtl.IBA_ForeignTax__c;
                    fePtl.IBA_NetPayabletoCarrier__c = fePtl.IBA_CarrierFee__c + fePtl.IBA_InsurancePremiumTax__c + fePtl.IBA_ForeignTax__c;
                    fePtl.IBA_PolicyCarrierLine__c = policyToCarrierLinesByCarrierIds.get(policyId).get(feCarrierId).Id;
                    fePtl.IBA_Product__c = fixedExpenseProduct.Id;
					
                    // ------------------ Fix for Direct Debit -------------------------------------------
                    if(policy.EW_Payment_Method__c == EWConstants.DIRECT_DEBIT_PAYMENT_METHOD){
                        fePtl.IBA_AzurDDFee__c = (0.0105 * (fePtl.IBA_GrossWrittenPremium__c + fePtl.IBA_InsurancePremiumTax__c + fePtl.IBA_ForeignTax__c)).setScale(2); 
                        fePtl.IBA_PCLDDFee__c = (0.0495 * (fePtl.IBA_GrossWrittenPremium__c + fePtl.IBA_InsurancePremiumTax__c + fePtl.IBA_ForeignTax__c)).setScale(2); 
                        fePtl.IBA_TotalDirectDebitFee__c = (fePtl.IBA_GrossWrittenPremium__c + fePtl.IBA_InsurancePremiumTax__c + fePtl.IBA_ForeignTax__c) * policy.EW_Quote__r.EW_DD_Interest_Percentage__c / 100;
                        fePtl.IBA_DuefromAccountingBroker__c = fePtl.IBA_GrossWrittenPremium__c + fePtl.IBA_AzurDDFee__c + fePtl.IBA_InsurancePremiumTax__c + fePtl.IBA_ForeignTax__c; 
                        fePtl.IBA_DuetoMGA__c = fePtl.IBA_MGACommission__c + fePtl.IBA_AzurDDFee__c;
                        fePtl.IBA_NetDuefromBroker__c = 0 - fePtl.IBA_BrokerCommission__c;
                    }
                    
                    usedCarriers.add(fePtl.IBA_PolicyCarrierLine__c);
                    policyTransactionLines.add(fePtl);
                }
                // -----------------------------------------------------------------------------------------------------

                Map<Id, IBA_FFPolicyCarrierLine__c> carrierLineMap = new Map<Id, IBA_FFPolicyCarrierLine__c>(policyToCarrierLinesByCarrierIds.get(policyId).values());

                for (Id usedCarrier : usedCarriers) {
                    carrierLineMap.remove(usedCarrier);
                }

                if (!carrierLineMap.isEmpty()) {
                    carrierLinesToDelete.addAll(carrierLineMap.values());
                }
            }

            insert policyTransactionLines;
            delete carrierLinesToDelete;

            /** -------------------------------------------------- **/
            /** ------------------ Dimension 1s ------------------ **/
            Map<Id, IBA_PolicyTransactionLine__c> policyTransactionLinesMap = new Map<Id, IBA_PolicyTransactionLine__c>(policyTransactionLines);
            policyTransactionLines = new List<IBA_PolicyTransactionLine__c>([
                    SELECT Id, Name, IBA_Dimension1__c
                    FROM IBA_PolicyTransactionLine__c
                    WHERE Id IN :policyTransactionLinesMap.keySet()
            ]);

            Map<Id, c2g__codaDimension1__c> dimension1sByPTLineIds = new Map<Id, c2g__codaDimension1__c>();
            for (IBA_PolicyTransactionLine__c ptLine : policyTransactionLines) {
                c2g__codaDimension1__c dimension1 = new c2g__codaDimension1__c();
                dimension1.IBA_PolicyTransactionLine__c = ptLine.Id;
                dimension1.c2g__ReportingCode__c = ptLine.Name;
                dimension1.Name = ptLine.Name;

                dimension1sByPTLineIds.put(ptLine.Id, dimension1);
            }

            insert dimension1sByPTLineIds.values();

            /** ------------------------------------------------------------------------------- **/
            /** -------------- Policy Transaction Lines Update (+ Dimension1.Id) -------------- **/
            policyTransactionLines.clear();
            for (Id ptLineId : dimension1sByPTLineIds.keySet()) {
                c2g__codaDimension1__c dimension1 = dimension1sByPTLineIds.get(ptLineId);
                IBA_PolicyTransactionLine__c ptLine = policyTransactionLinesMap.get(ptLineId);

                ptLine.IBA_Dimension1__c = dimension1.Id;
                policyTransactionLines.add(ptLine);
            }
            update policyTransactionLines;

        } catch (Exception ex) {
            Database.rollback(sp);

            LogBuilder logBuilder = new LogBuilder();
            Azur_Log__c log = logBuilder.createGenericLog(ex);
            log.Class__c = EWVlocityRemoteActionHelper.class.getName();
            log.Method__c = 'FF // createFFRecordsForPolicies';
            log.Log_message__c = log.Log_message__c + '\n\n method name = createFFRecordsForPolicies';
            log.Log_message__c += '\n\nln: ' + ex.getLineNumber();
            log.Log_message__c += '\n\nst: ' + ex.getStackTraceString();
            System.debug('FF DEBUG // message: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            insert log;

            throw ex;
        }
    }

    // v2: 29/05/2020 ajedamenko@azuruw.com method signature changed to use Set<String> instead of String
    private static Set<Id> getPoliciesIdsByNewStatus(Map<Id, SObject> oldItems, Map<Id, SObject> newItems, Set<String> availableNewStatuses) {
        Set<Id> changedPolicies = new Set<Id>();
        for (Id policyId : newItems.keySet()) {
            Asset newPolicy = (Asset) newItems.get(policyId);
            Asset oldPolicy = (Asset) oldItems.get(policyId);
            if (availableNewStatuses.contains(newPolicy.Status)  && newPolicy.Status != oldPolicy.Status) {
                changedPolicies.add(policyId);
            }
        }
        return changedPolicies;
    }

// V2 - 08/09/2020 Aleksejs Jedamenko ajedamenko@azuruw.com - EW_Quote__r.Status and EW_Quote__r.EW_TypeofTransaction__c added to SOQL query, used for a proper fixed expense transaction line handling 
    private static Map<Id, Asset> getPoliciesWithCoverages(Set<Id> policiesIds) {
        Map<Id, Asset> policiesWithCoverages = new Map<Id, Asset>([
                SELECT Id, vlocity_ins__EffectiveDate__c, IBA_InsuredName__c, ProductCode, EW_Quote__c, EW_Quote__r.Status, EW_Quote__r.EW_TypeofTransaction__c, EW_FixedExpense__c, EW_Quote__r.EW_DD_Interest_Percentage__c,  
                        IBA_InsurancePremiumTaxRate__c, IBA_MGACommissionRate__c, IBA_BrokerCommissionRate__c, CurrencyIsoCode, Status,
                        EW_Broker_Reference_Number__c, vlocity_ins__AttributeSelectedValues__c, EW_TypeofTransaction__c, EW_Payment_Method__c, (
                        SELECT Id, EW_IPTTotal__c, CurrencyIsoCode, vlocity_ins__Product2Id__c,
                                vlocity_ins__Product2Id__r.Name, IBA_Status__c, EW_PremiumNet__c, EW_PremiumGrossIncIPT__c,
                                EW_BrokerCommissionValue__c, EW_AzurCommissionValue__c, EW_PremiumGrossExIPT__c
                        FROM vlocity_ins__PolicyCoverages__r
                )
                FROM Asset
                WHERE Id IN :policiesIds
        ]);

        return policiesWithCoverages;
    }

    private static Map<Id, List<QuoteLineItem>> getQuoteLineItemsByPolicyId(Map<Id, Asset> policies) {
        Set<Id> quoteIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        Map<Id, List<QuoteLineItem>> quoteLineItemsByPolicyId = new Map<Id, List<QuoteLineItem>>();

        if (!policies.isEmpty()) {
            for (vlocity_ins__AssetCoverage__c coverage : policies.values().get(0).vlocity_ins__PolicyCoverages__r) {
                productIds.add(coverage.vlocity_ins__Product2Id__c);
            }
        }

        for (Asset policy : policies.values()) {
            quoteIds.add(policy.EW_Quote__c);
        }

        List<Quote> quotesWithQLIs = new List<Quote>([
                SELECT Id, EW_Policy__c, (
                        SELECT Id, CurrencyIsoCode, Product2Id, Product2.Name, EW_Transactional_PremiumNet__c,
                                EW_Transactional_PremiumGrossIncIPT__c, EW_Transactional_BrokerCommissionValue__c,
                                EW_Transactional_AzurCommissionValue__c, EW_Transactional_IPT__c,
                                EW_Transactional_PremiumGrossExIPT__c, QuoteId
                        FROM QuoteLineItems
                        WHERE Product2Id IN :productIds
                )
                FROM Quote
                WHERE Id IN :quoteIds
        ]);

        for (Quote quote : quotesWithQLIs) {
            quoteLineItemsByPolicyId.put(quote.EW_Policy__c, quote.QuoteLineItems);
        }

        return quoteLineItemsByPolicyId;
    }

    private static Product2 getFixedExpenseProduct() {
        if (Test.isRunningTest()) {
            Product2 fixedExpenseProduct = new Product2(
                    Name = 'Fixed Expense',
                    ProductCode = 'fixedExpense',
                    IsActive = true,
                    RecordTypeId = Product2.getSObjectType().getDescribe().getRecordTypeInfosByDeveloperName().get('Standard').getRecordTypeId());
            insert fixedExpenseProduct;
            return fixedExpenseProduct;
        } else {
            return [SELECT Id, Name FROM Product2 WHERE Name = :EWConstants.FIXED_EXPENSE_PRODUCT_NAME LIMIT 1];
        }
    }

    public static String findOriginalPolicyBusinessType(Id policyId) {
        String result = recursivelyFindOriginalPolicyBusinessType(policyId);
        return result;
    }

    private static String recursivelyFindOriginalPolicyBusinessType(Id policyId) {
        List<Asset> policies = [
            SELECT Id, EW_TypeofTransaction__c, vlocity_ins__PreviousVersionId__c
            FROM Asset
            WHERE Id=:policyId
            LIMIT 1
        ];

        if (!policies.isEmpty()) {
            Asset policy = policies[0];
            
            if (policy.EW_TypeofTransaction__c == EWConstants.QUOTE_TRANSACTION_TYPE_MTA
            || policy.EW_TypeofTransaction__c == EWConstants.QUOTE_TRANSACTION_TYPE_CANCELLATION) {
                return recursivelyFindOriginalPolicyBusinessType(
                    policy.vlocity_ins__PreviousVersionId__c
                );
            }
            return policy.EW_TypeofTransaction__c;
        }
        return null;
    }
}