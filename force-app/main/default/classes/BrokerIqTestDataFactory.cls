/**
* @name: BrokerIqTestDataFactory
* @description: Test Data factory to BrokerIq project
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @Modified: Ignacio Sarmiento	11/12/2017	Add method to create Broker Iq 2 guest users and remove custom labels from class properties
* @Modified: Ignacio Sarmiento  18/12/2017  Add methods to create CPD tests, CPD assessments, CPD test questions/answers, BrightTALK webcast,
*                                           BrightTALK talk channels, categories, broker IQ contributors and webinars
* @Modified: Ignacio Sarmiento	23/04/2018	BIQ-4 - Add 'createAgencyBrokerageAccount' and 'createAccount' method
*/
@isTest
public class BrokerIqTestDataFactory 
{
    //Profiles
	public static final String PROFILE_NAME_SYSTEMADMINISTRATOR = 'System Administrator';
    public static final String PROFILE_NAME_BROKER_IQ_COMMUNITY_PLUS ='Broker iQ Community Plus Login User';
    public static final String PROFILE_BROKER_IQ2_GUEST_USER_NAME = 'Broker iQ Profile';
    //Roles
    public static final String USERROLE_DEVNAME_TECHTEAM = 'Tech_team';
    
    
    public static User createPortalUser(String portalUserLastName, Id portalProfileId){
        //Create portal account
        Account portalAccount = createBusinessAccount('Account' + Math.random());
        insert portalAccount;
        
        //Create portal contact
        Contact portalContact = createBroker(portalAccount.Id, 'Contact ' + Math.random());
		insert portalContact;
        
        //Create portal User 
        User portalUser = createUser(portalUserLastName, portalProfileId, null);
        portalUser.ContactId = portalContact.Id;
        portalUser.ProfileId = portalProfileId;
		return portalUser;
	}

 	public static Account createBusinessAccount(String name){
        return createAccount(name, 'Business Account');
	}
    
    public static Account createAgencyBrokerageAccount(String name){
        return createAccount(name, 'Agency/Brokerage');
	}
    
    public static Account createAccount(String name, String recordTypeName){
		Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
		Account acc = new Account();
		acc.Name = name;
		acc.RecordTypeId = recordTypeId;
        acc.EW_DRCode__c = TestDataFactory.getRandomDRCode();
        acc.EW_MaximumCommission__c = 15;
        acc.EW_MinimumCommission__c = 5;
        acc.EW_DefaultCommission__c = 10;
		return acc;
	}
    
    public static Contact createBroker(Id accountId, String lastName){
		Id brokerRecId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Producer').getRecordTypeId();
		Contact con = new Contact();
		con.AccountId = accountId;
        con.LastName = lastName;
		con.Salutation = 'Mr';
        con.FirstName = 'test' + + Math.random();
		con.Email =  System.now().millisecond() + '@testEmail.com';
		con.MailingStreet = '1 ApexTest BrokerStreet';
		con.MailingCity = 'London';
		con.MailingPostalCode = 'BR1 2AT';
		con.MailingCountry = 'United Kingdom';
		con.RecordTypeId = brokerRecId;
		return con;
	}    
    
    public static User createUser(String lastName,Id profileId, Id roleId){
		User user = new User();
		user.FirstName = 'Tester' + Math.random();
		user.LastName = lastName;
		user.Email = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
		user.Username = lastName.replaceAll( '\\s+', '') + '@azuruw.com.test';
		user.Alias = 'ttest';
		user.EmailEncodingKey='UTF-8';
		user.LanguageLocaleKey='en_US';
        user.LocaleSidKey='en_US';
        user.ProfileId = profileId;
        user.UserRoleId = roleId;
        user.TimeZoneSidKey = 'Europe/London';
        user.isActive = true;
		return user;
	}
    
    public static User createBrokerIq2GuestUser(String guestUserLastName){
        Profile brokerIq2GuestUserProfile = [SELECT Id FROM Profile WHERE Name =: PROFILE_BROKER_IQ2_GUEST_USER_NAME LIMIT 1];
        User brokerIq2GuestUser = createUser(guestUserLastName, brokerIq2GuestUserProfile.Id, null);
		return brokerIq2GuestUser;
	}
    
    public static User createSystemAdmistratorUser(String lastName){
        Profile systemAdminProfile = [SELECT Id FROM Profile WHERE Name =: PROFILE_NAME_SYSTEMADMINISTRATOR LIMIT 1];
        UserRole systemAdminUserRole = [SELECT Id FROM UserRole WHERE DeveloperName =: USERROLE_DEVNAME_TECHTEAM LIMIT 1];
        User systemAdminstratorUser = createUser(lastName, systemAdminProfile.Id, systemAdminUserRole.Id);
		return systemAdminstratorUser;
	}
    
    public static List<GroupMember> addGroupMembersToAGroup(List<Id> groupMemberUserIds, Id groupParentId){
        List<GroupMember> groupMembers = new List<GroupMember>();
        for(Id groupMemberUserId: groupMemberUserIds){
            GroupMember groupMember = new GroupMember();
            groupMember.GroupId = groupParentId;
            groupMember.UserOrGroupId = groupMemberUserId;
            groupMembers.add(groupMember);
        }
        return groupMembers;
    }
    
    public static Group createAPublicGroup(String groupDeveloperName){
        Group currentGroup = new Group();
        currentGroup.Name = groupDeveloperName;
        currentGroup.DeveloperName = groupDeveloperName;
        return currentGroup;
    }
    
    public static Category__c createCategory(String categoryName){
        Category__c category = new Category__c();
        category.Name = categoryName;
        category.Icon__c ='icon-clipboard-check';
        category.Placeholder_Image__c  = '/assets/images/posts/intermediate-liability-insurance-thumb.jpg';
        category.Colour__c = 'froly';
        category.External_Icon__c = 'http://bit.ly/2e85okK';
        category.External_Tick__c = 'http://bit.ly/2eGm9PU';
        category.External_Download_Button__c = 'http://bit.ly/2fjbJtq';
        category.External_Colour__c = '#f77e84';
        return category;
    }

    public static Broker_iQ_Contributor__c createBrokerIQContributor (String brokerIQContributorName, Id contactId){
        Broker_iQ_Contributor__c brokerIQContributor = new Broker_iQ_Contributor__c();
        brokerIQContributor.Name = brokerIQContributorName;
        brokerIQContributor.Contact__c = contactId; 
        brokerIQContributor.Description__c = 'test description';
        brokerIQContributor.Portrait__c = '0695E0000004WQs';
        brokerIQContributor.Portrait_URL__c  = 'https://azuruwnebulatt--nebulatt--c.cs84.content.force.com/sfc/dist/version/download/?oid=00D5E0000008oBV&ids=0685E0000004XDc&d=%2Fa%2F5E000000Cc6B%2F0x6jVKPOYrPMN8mhwmTY0D.o3lsZWzDXfh1YUxfPex4&asPdf=false';
        brokerIQContributor.Active__c = true;
        return brokerIQContributor;
    }
    
    public static IQ_Content__c createWebinar(String webinarName, Id categoryId, Id brokerIQContributorId, Boolean isCIIContent){
		Id webinarRecordTypeId = Schema.SObjectType.IQ_Content__c.getRecordTypeInfosByName().get('Webinar').getRecordTypeId();
        IQ_Content__c iqContent = new IQ_Content__c();
		iqContent.Name = webinarName;  
        iqContent.RecordTypeId = webinarRecordTypeId;        
        iqContent.Category__c = categoryId;
        iqContent.Specific_BrokeriQ_Content__c = isCIIContent? 'CII' : '';
		iqContent.Broker_iQ_Contributor__c = brokerIQContributorId;
        iqContent.CPD_Time_minutes__c = 45;
        iqContent.Active__c = true;
        return iqContent;
    } 
    
    public static BrightTALK__Channel__c createBrightTalkChannel(String brightTalkChannelName){
		BrightTALK__Channel__c  btChannel = new BrightTALK__Channel__c();
        btChannel.Name = brightTalkChannelName;
        btChannel.BrightTALK__Name__c = brightTalkChannelName;
        btChannel.BrightTALK__Channel_Id__c  = 14.433;
        btChannel.BrightTALK__URL__c = 'https://www.brighttalk.com/channel/14433';
        return btChannel;
    } 
    
    public static BrightTALK__Webcast__c  createBrightTalkWebcast(String brightTalkWebcastName, Id webinarId, Id brightTalkChannelId, DateTime startDateTime){
		BrightTALK__Webcast__c   btChannel = new BrightTALK__Webcast__c ();
        btChannel.Name = brightTalkWebcastName;
        btChannel.BrightTALK__Name__c = brightTalkWebcastName;
        btChannel.BrightTALK__Channel__c = brightTalkChannelId;
        btChannel.BrightTALK__Channel_Webcast_Id__c = '235,101';
        btChannel.BrightTALK__Presenter__c  = 'Ignacio Sarmiento Developer in Azur Group';
        btChannel.BrightTALK__Start_Date__c  = startDateTime;
        btChannel.IQ_Content__c = webinarId;
        btChannel.Duration_s__c = 1.757;
        btChannel.BrightTALK__Channel_Webcast_Id__c = '14433-235101';
        btChannel.BrightTALK__URL__c = 'https://www.brighttalk.com/webcast/14433/283395';
        btChannel.BrightTALK__Description__c  ='Good negotiation skills are crucial to the role...';
        btChannel.BrightTALK__Keywords__c = 'Negotiation, Skills';
        btChannel.Preview_Thumbnail__c = 'https://www.brighttalk.com/communication/235101/thumbnail1479817257619.png';
        btChannel.Preview_Image__c  = 'https://www.brighttalk.com/communication/235101/thumbnail1479817257619.png';
        btChannel.Preview_Image_CII__c = 'https://www.brighttalk.com/communication/235101/thumbnail1479817257618.png';
        return btChannel;
    } 

    public static CPD_Test__c createCPDTest(String cpdTestName, Id iqContentId){
		CPD_Test__c cpdTest = new CPD_Test__c();
        cpdTest.Name = cpdTestName;
        cpdTest.IQ_Content__c = iqContentId;
        cpdTest.Pass_Score__c = 50;
        return cpdTest;
    } 

    
    public static CPD_Assessment__c createCPDAssessment(Id cpdTestId, Id contactId){
		CPD_Assessment__c cpdAssessment = new CPD_Assessment__c();
        cpdAssessment.CPD_Test__c = cpdTestId; 
        cpdAssessment.Contact__c = contactId;
        cpdAssessment.Date_Time_Taken__c= DateTime.now();
        return cpdAssessment;
    } 
    
        
    public static CPD_Test_Question__c createCPDTestQuestion(String question, Id cpdTestId){
		CPD_Test_Question__c cpdTestQuestion = new CPD_Test_Question__c();
        cpdTestQuestion.Question__c = question;
        cpdTestQuestion.CPD_Test__c = cpdTestId; 
        cpdTestQuestion.Option_1__c = 'A';
        cpdTestQuestion.Option_2__c = 'B';
        cpdTestQuestion.Option_3__c = 'C';
        cpdTestQuestion.Option_4__c = 'D';
        cpdTestQuestion.Answer__c = '1';
        return cpdTestQuestion;
    } 
    
    public static CPD_Assessment_Answer__c createCPDAnswer(Id cpdAssestmentId, Id cpdTestQuestionId, boolean isCorrectAnswer){
		CPD_Assessment_Answer__c  cpdAssessmentAnswer = new CPD_Assessment_Answer__c();
        cpdAssessmentAnswer.CPD_Assessment__c = cpdAssestmentId; 
        cpdAssessmentAnswer.CPD_Test_Question__c = cpdTestQuestionId;
        cpdAssessmentAnswer.Correct__c = isCorrectAnswer? 'true' : 'false';
        return cpdAssessmentAnswer;
    }  
    
    

}