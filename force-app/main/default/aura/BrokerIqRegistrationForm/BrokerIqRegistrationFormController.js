/*
* @name: BrokerIqRegistrationFormController.js
* @description: BrokerIqRegistrationForm js controller
* @author: Ignacio Sarmiento isarmiento@azuruw.com
* @date: 11/01/2018
* @modifiedBy: Ignacio Sarmiento    26/03/2018 		BIQ-57 - Fix the bug to redirect to "Terms and Conditions" page 
* @modifiedBy: Ignacio Sarmiento    23/04/2018 		BIQ-04 - Add  a comment to deploy lightning element
*/
({
    doInit: function(component, event, helper) {
        helper.setTermsOfUseURLHelper(component)
    },

	handleRegister : function(component, event, helper) {
        helper.handleRegisterHelper(component, event, helper);
    },
    handleCountrySelect : function(component, event, helper) {
        helper.handleCountrySelectHelper(component, event, helper);
    },
})