public class EWCaseTriggerHelper {
    
    public static void handlecases(List<Case> newItems) {
        
        Map<String,List<Case>> caseMap = new Map<String,List<Case>>();
        Map<String,List<User>> userMap = new Map<String,List<User>>();
        String CaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EWConstants.CASE_RECORD_TYPE_COMMUNITY).getRecordTypeId();
        
        For(Case ct : newItems){ 
            ct.Type ='SmartHome Support';
            if( ct.RecordtypeId == CaseRecordTypeId && ct.ContactId == null && ct.AccountId == null ){
                If(!caseMap.keySet().contains(ct.SuppliedEmail)){
                    caseMap.put(ct.SuppliedEmail, new List<Case>{ct});  
                }
                caseMap.get(ct.SuppliedEmail).add(ct);           
            }               
        }
        if(!caseMap.isEmpty()){
            For(User userList : [SELECT Id,Contact.AccountId,Email,ContactId FROM User WHERE Email = :caseMap.keySet() AND Profile.Name = :EWConstants.PROFILE_EW_PARTNER_NAME AND IsActive = true]){
                if(!userMap.keySet().contains(userList.Email)){
                    userMap.put(userList.Email, new List<User>{userList});  
                }else{
                    userMap.get(userList.Email).add(userList); 
                }
                
            }            
        }
        if(!caseMap.isEmpty()){			
            For(String emailVal : userMap.keySet()){
                if(userMap.get(emailVal).size() == 1 && caseMap.containsKey(emailVal)){
                    For(Case nwCase : caseMap.get(emailVal)){
                        nwCase.AccountId = userMap.get(emailVal)[0].Contact.AccountId;
                        nwCase.ContactId =userMap.get(emailVal)[0].ContactId;
                        nwCase.Type = 'SmartHome Support';
                    }  
                }            
            }
        }
    }
}