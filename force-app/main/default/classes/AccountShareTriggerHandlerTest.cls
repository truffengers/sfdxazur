/*
Name:            AccountShareTriggerHandlerTest
Description:     This test Class will cover the code for AccountShare Trigger and Apex classes EWAccountShrRecInsertBatch, AccountShareTriggerHandler,EWAccountShrRecDeleteBatch
Created By:      Rajni Bajpai 
Created On:      03 May 2021
************************************************************************************************
Sr.No.          ChangedBy              Date              Desc
************************************************************************************************
1               Rajni Bajpai          03May21            EWH-1983 Share Account ID
************************************************************************************************
*/
@isTest

public class AccountShareTriggerHandlerTest {
    
    private static User systemAdminUser;
    private static Account broker;
    private static Contact brokerContact1;
    private static Contact brokerContact2;
    private static User brokerUser1;
    private static User brokerUser2;
    private static Account insured;
    private static opportunity oppty;
    private static EW_AccountSharing__c accShare;
    
    private static Quote quote;
    
    
    private static testMethod void EWAccountShrRecDeleteBatchtest() {
        setupDelete();
        Test.startTest();
        List<AccountShare> ashares = [SELECT Id, UserOrGroupId, AccountaccessLevel FROM AccountShare WHERE AccountId = :insured.Id];
        system.assertEquals(!ashares.IsEmpty(),true);        
        Test.StopTest();
        
    }
    private static testMethod void EWAccountShrRecInsertBatchtest() {
        setup();
        Test.startTest();
        List<AccountShare> ashares = [SELECT Id, UserOrGroupId, AccountaccessLevel FROM AccountShare WHERE AccountId = :broker.Id];
        system.assertEquals(ashares.size()>0,true);
        Test.StopTest();       
        
    }
    
    static void setup() {
        
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);        
        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            techteamUserRole.Id,
            true);
        
        System.runAs(systemAdminUser) {
            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            
            Id partnerProfileId = [
                SELECT Id
                FROM Profile
                WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME
            ].Id;
            
            brokerContact1 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser1 = EWUserDataFactory.createPartnerUser('User1', partnerProfileId, brokerContact1.Id, true);
            brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser2 = EWUserDataFactory.createPartnerUser('User2', partnerProfileId, brokerContact2.Id, true);
            
        }
        
        System.runAs(brokerUser1) {
            insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                false);
            
            insured.EW_BrokerAccount__c = broker.Id;
            insert insured;
            
            oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);
            quote.vlocity_ins__AgencyBrokerageId__c=insured.id;
            update quote;
            accShare=new EW_AccountSharing__c();
            accShare.EW_ParentAccount__c=insured.id;
            accShare.EW_SharedWithAccount__c=broker.id;
            insert accShare;
            
        }
    }    
    static void setupDelete() {
        
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);        
        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            techteamUserRole.Id,
            true);
        
        System.runAs(systemAdminUser) {
            broker = EWAccountDataFactory.createBrokerAccount(null, true);
            
            Id partnerProfileId = [
                SELECT Id
                FROM Profile
                WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME
            ].Id;
            insured = EWAccountDataFactory.createInsuredAccount(
                'Mr',
                'Test',
                'Insured',
                Date.newInstance(1969, 12, 29),
                false);
            
            insured.EW_BrokerAccount__c = broker.Id;
            insert insured;
            brokerContact1 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser1 = EWUserDataFactory.createPartnerUser('User1', partnerProfileId, brokerContact1.Id, true);
            brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser2 = EWUserDataFactory.createPartnerUser('User2', partnerProfileId, brokerContact2.Id, true);
            AccountShare accShareNew= new AccountShare();
            accShareNew.AccountId=insured.id;
            accShareNew.UserOrGroupId=brokerUser1.Id;
            accShareNew.AccountAccessLevel='Edit';
            accShareNew.OpportunityAccessLevel='Read';
            insert accShareNew;
            
        }
        
        System.runAs(brokerUser1) {
            oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
            quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);
            quote.vlocity_ins__AgencyBrokerageId__c=insured.id;
            update quote;
            accShare=new EW_AccountSharing__c();
            accShare.EW_ParentAccount__c=insured.id;
            accShare.EW_SharedWithAccount__c=broker.id;
            insert accShare; 
            delete accShare;            
        }
    }    
}