/**
* @name: IBA_TransferBrokerOverdueScheduled
* @description: Allow to schedule IBA_TransferBrokerOverdueBatch class
*
*/
global class IBA_TransferBrokerOverdueScheduled implements Schedulable {
   global void execute(SchedulableContext sc) {
      IBA_TransferBrokerOverdueBatch b = new IBA_TransferBrokerOverdueBatch(); 
      Database.executebatch(b);
   }
}