/**
* @name: EWRenewalQuoteReminderBatch
* @description: test class for EWRenewalQuoteReminderBatch
* @author: Andrew Xu axu@azuruw.com
* @date: 11/06/2020
*
*/
@isTest
private class EWRenewalQuoteReminderBatchTest {

    /**
    * @methodName: setup
    * @description: setup test values for following test methods
    * @dateCreated: 11/06/2020
    */
    @testSetup
    static void setup(){

        Account testBroker = EWAccountDataFactory.createBrokerAccount(null, false);
        Account testInsured = EWAccountDataFactory.createInsuredAccount(
                'Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), false
        );

        insert new List<Account>{
                testBroker, testInsured
        };

        Opportunity testOpportunity = EWOpportunityDataFactory.createOpportunity(null, testInsured.Id, testBroker.Id, true);

        Quote testRenewalQuoteEffectiveIn7Days = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testRenewalQuoteEffectiveIn7Days.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        testRenewalQuoteEffectiveIn7Days.vlocity_ins__EffectiveDate__c = Date.today().addDays(7);

        Quote testRenewalQuoteEffectiveIn10Days = EWQuoteDataFactory.createQuote(null,testOpportunity.Id, testBroker.Id, false);
        testRenewalQuoteEffectiveIn10Days.EW_TypeofTransaction__c = EWConstants.QUOTE_TRANSACTION_TYPE_RENEWAL;
        testRenewalQuoteEffectiveIn10Days.vlocity_ins__EffectiveDate__c = Date.today().addDays(10);

        insert new List<Quote> {testRenewalQuoteEffectiveIn7Days, testRenewalQuoteEffectiveIn10Days};
    }

    /**
   * @methodName: renewalQuoteReminderEmailSentTest
   * @description: Check that only 1 email has been sent for the Quote with Effective Date in 7 days
   * @dateCreated: 11/06/2020
   */
    @isTest
    private static void renewalQuoteReminderEmailSentTest(){

        Test.startTest();
        Id batchId = Database.executeBatch(new EWRenewalQuoteReminderBatch());
        Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Azur_Log__c].size(), 'No Azur Logs were created due to errors');
    }

}