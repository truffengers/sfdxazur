({
    search : function(component) {
        var selectedTopic = component.get('v.selectedTopic');
        var selectedContentType = component.get('v.selectedContentType');

        var isWebinarVersion = component.get('v.isWebinarVersion');
        component.set('v.isLoading', true);
        var resultComponent = component.find('results');
        if($A.util.isArray(resultComponent)) {
            resultComponent = resultComponent[0];
        }
        var helper = this;
        if(isWebinarVersion) {
            helper.searchAsPromise(component, helper, true)
                .then($A.getCallback(function() {
                    return helper.searchAsPromise(component, helper, false);
                }))
                .then($A.getCallback(function() {
                    component.set('v.isLoading', false);
                }));
        } else {
            helper.searchAsPromise(component, helper)
                .then(function() {
                    component.set('v.isLoading', false);
                });
        }
    },
    doSearch : function(component, isUpcoming, resolve, reject) {
		var doSearchAction = component.get('c.doSearch');
		var currentPage = component.get('v.currentPage');
        var itemsPerPage = component.get('v.itemsPerPage');

        var searchString = component.get('v.searchString');

        var selectedTopic = component.get('v.selectedTopic');
        var selectedContentType = component.get('v.selectedContentType');
        var selectedContributor = component.get('v.selectedContributor');

        var upcomingCurrentPage = component.get('v.upcomingCurrentPage');

        if(currentPage && itemsPerPage) {

        	var params = {
                pageOffset: (currentPage - 1) * itemsPerPage,
                pageLimit: itemsPerPage,
                searchString: searchString
            };

            if(selectedTopic) {
            	params.selectedTopic = selectedTopic.value;
            }
            if(selectedContentType) {
            	params.selectedContentType = selectedContentType.value;
            }
            if(selectedContributor) {
            	params.selectedContributor = selectedContributor.value;
            }
            if(typeof isUpcoming != 'undefined') {
                params.isUpcoming = isUpcoming;
                if(isUpcoming) {
                    params.pageOffset = (component.get('v.upcomingCurrentPage') - 1) * itemsPerPage;
                }
            }

            doSearchAction.setParams(params);

    		doSearchAction.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    if(params.isUpcoming) {
                        component.set('v.upcomingIqContentPage', result.iqContentPage);
                        component.set('v.upcomingTotalItems', result.totalItems);
                    } else {
                        component.set('v.iqContentPage', result.iqContentPage);
                        component.set('v.totalItems', result.totalItems);
                    }

                    if(resolve) {
                        resolve('doSearchAction succeeded');
                    }
                } else {
                    if(reject) {
                        reject(Error(response.getError()));
                    }
                }
            });
            
            $A.enqueueAction(doSearchAction);		
        }
		
	},
    searchAsPromise : function(component, helper, isUpcoming) {
        return new Promise($A.getCallback(function(resolve, reject) {
            return helper.doSearch(component, isUpcoming, resolve, reject);
        }));
    }    
})