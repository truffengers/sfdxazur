/**
* @name: EWAccountHelperTest
* @description: Test class for EWAccountHelper
* @author: Steve Loftus sloftus@azuruw.com
* @date: 03/09/2018
* @lastChangedBy Steve Loftus sloftus@azuruw.com 11/09/2018
*/
@isTest
private class EWAccountHelperTest {

    private static User systemAdminUser;
    private static Account broker;
    private static Contact brokerContact1;
    private static Contact brokerContact2;
    private static User brokerUser1;
    private static User brokerUser2;
    private static Account insured;
    private static Opportunity opportunity;
    private static Quote quote;
    private static Asset policy;
    private static EWAccountHelper.DuplicateAccountDetails accountDetails;

    @isTest static void duplicateAccountNoCheckTest() {
        setup(false, false, false);

        Test.startTest();

        EWAccountHelper.CheckForDuplicateAccount(accountDetails);

        Test.stopTest();

        System.assertEquals(false, accountDetails.isDuplicate, 'Should not find duplicate account');
        System.assertEquals(false, accountDetails.hasLiveQuote, 'Should not find a live quote');
        System.assertEquals(false, accountDetails.hasLivePolicy, 'Should not find a live policy');
    }

    @isTest static void duplicateAccountTest() {
        setup(true, false, false);

        Test.startTest();

        EWAccountHelper.CheckForDuplicateAccount(accountDetails);

        Test.stopTest();

        System.assertEquals(true, accountDetails.isDuplicate, 'Should find duplicate account');
        System.assertEquals(false, accountDetails.hasLiveQuote, 'Should not find a live quote');
        System.assertEquals(false, accountDetails.hasLivePolicy, 'Should not find a live policy');
    }

    @isTest static void duplicateAccountWithLiveQuoteTest() {

        setup(true, true, false);

        Test.startTest();

        EWAccountHelper.CheckForDuplicateAccount(accountDetails);

        Test.stopTest();

        System.assertEquals(true, accountDetails.isDuplicate, 'Should find duplicate account');
        System.assertEquals(true, accountDetails.hasLiveQuote, 'Should find a live quote');
        System.assertEquals(false, accountDetails.hasLivePolicy, 'Should not find a live policy');
    }

    @isTest static void duplicateAccountWithLivePolicyTest() {

        setup(true, true, true);

        Test.startTest();

        EWAccountHelper.CheckForDuplicateAccount(accountDetails);

        Test.stopTest();

        System.assertEquals(true, accountDetails.isDuplicate, 'Should find duplicate account');
        System.assertEquals(true, accountDetails.hasLiveQuote, 'Should find a live quote');
        System.assertEquals(true, accountDetails.hasLivePolicy, 'Should find a live policy');
    }

    @isTest static void sharedPolicyTest() {

        setup(true, true, true);

        List<Asset> policyList;

        System.runAs(brokerUser2) {

            policyList = [
                    SELECT Id
                    FROM Asset
                    WHERE Id = :policy.Id
            ];
        }

        System.assert(!policyList.isEmpty(), 'Should find a live policy');
    }

    @isTest static void updateInsuredAccountsDataTest_editPrimeIns() {
        EWAccountHelper.InsuredAccountsDetails details = new EWAccountHelper.InsuredAccountsDetails();
        details.primeInsFirstName = 'FNameNew';
        details.primeInsLastName = 'LNameNew';
        details.primeInsBirthDateTime = Datetime.newInstance(1999, 01, 01);
        details.primeInsSalutation = 'Mr.';
        details.jointInsFirstName = 'Joint';
        details.jointInsLastName = 'Person';
        details.jointInsBirthDateTime = (Datetime) Date.newInstance(2000, 1, 1);
        details.jointInsSalutation = 'Mrs';
        details.hasJointPolicyHolder = 'yes';

        Map<String, SObject> updatedData = runTestForRecordUpdate(details);

        Quote updatedQuote = (Quote) updatedData.get('quote');
        Opportunity opp = (Opportunity) updatedData.get('opp');

        System.assert(opp.Name.contains('LNameNew'));
        System.assert(updatedQuote.Name.contains('LNameNew'));
    }

    @isTest static void updateInsuredAccountsDataTest_removePrimeIns() {
        EWAccountHelper.InsuredAccountsDetails details = new EWAccountHelper.InsuredAccountsDetails();
        details.jointInsFirstName = 'Joint';
        details.jointInsLastName = 'Person';
        details.jointInsBirthDateTime = (Datetime) Date.newInstance(2000, 1, 1);
        details.jointInsSalutation = 'Mrs';
        details.hasJointPolicyHolder = 'yes';

        Map<String, SObject> updatedData = runTestForRecordUpdate(details);

        Quote updatedQuote = (Quote) updatedData.get('quote');
        Opportunity opp = (Opportunity) updatedData.get('opp');

        System.assert(opp.Name.contains('Person'));
        System.assert(updatedQuote.Name.contains('Person'));
    }

    @isTest static void updateInsuredAccountsDataTest_updateJointIns() {
        EWAccountHelper.InsuredAccountsDetails details = new EWAccountHelper.InsuredAccountsDetails();
        details.jointInsFirstName = 'FNameNew';
        details.jointInsLastName = 'LNameNew';
        details.jointInsBirthDateTime = Datetime.newInstance(1999, 01, 01);
        details.jointInsSalutation = 'Mr.';
        details.hasJointPolicyHolder = 'yes';

        Map<String, SObject> updatedData = runTestForRecordUpdate(details);

        Quote updatedQuote = (Quote) updatedData.get('quote');
        Opportunity opp = (Opportunity) updatedData.get('opp');

        System.assert(opp.Name.contains('LNameNew'));
        System.assert(updatedQuote.Name.contains('LNameNew'));
    }

    private static Map<String, SObject> runTestForRecordUpdate(EWAccountHelper.InsuredAccountsDetails details) {
        setupForUpdate();
        details.recordId = quote.Id;

        Test.startTest();
        EWAccountHelper.updateInsuredAccountsData(details);
        Test.stopTest();

        Map<String, Object> updatedData = EWQuoteLightningController.getSetupData(quote.Id);

        Quote updatedQuote = (Quote) updatedData.get('quote');
        updatedQuote = [SELECT Id, Name, OpportunityId FROM Quote WHERE Id = :quote.Id];
        Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Id = :updatedQuote.OpportunityId];

        Map<String, SObject> result = new Map<String, SObject>();
        result.put('quote', updatedQuote);
        result.put('opp', opp);

        return result;
    }

    static void setup(Boolean haveDuplicateChecking, Boolean createLiveQuote, Boolean createLivePolicy) {

        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);

        systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
                EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
                techteamUserRole.Id,
                true);

        System.runAs(systemAdminUser) {

            EWFeatureSwitches__c featureSwitches = new EWFeatureSwitches__c();
            featureSwitches.InsuredDuplication__c = haveDuplicateChecking;
            insert featureSwitches;

            broker = EWAccountDataFactory.createBrokerAccount(null, true);

            Id partnerProfileId = [
                    SELECT Id
                    FROM Profile
                    WHERE Name = :EWConstants.PROFILE_EW_PARTNER_NAME
            ].Id;

            brokerContact1 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser1 = EWUserDataFactory.createPartnerUser('User1', partnerProfileId, brokerContact1.Id, true);
            brokerContact2 = EWContactDataFactory.createContact(null, null, broker.Id, true);
            brokerUser2 = EWUserDataFactory.createPartnerUser('User2', partnerProfileId, brokerContact2.Id, true);

        }

        System.runAs(brokerUser1) {

            insured = EWAccountDataFactory.createInsuredAccount(
                    'Mr',
                    'Test',
                    'Insured',
                    Date.newInstance(1969, 12, 29),
                    false);

            insured.EW_BrokerAccount__c = broker.Id;
            insert insured;

            if (createLiveQuote) {

                opportunity = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);

                quote = EWQuoteDataFactory.createQuote(null, opportunity.Id, broker.Id, true);
                quote.Status = EWConstants.QUOTE_STATUS_QUOTED;
                update quote;
            }

            if (createLivePolicy) {

                policy = EWPolicyDataFactory.createPolicy(null, opportunity.Id, broker.Id, insured.Id, true);
            }
        }

        accountDetails = new EWAccountHelper.DuplicateAccountDetails(
                broker.Id,
                'Test',
                'Insured',
                DateTime.newInstance(1969, 12, 29),
                false,
                false,
                false,
                false);
    }

    static void setupForUpdate() {
        Account testBroker = EWAccountDataFactory.createBrokerAccount('testBroker', true);
        Account insuredAccount = EWAccountDataFactory.createInsuredAccount('Mrs', 'Insured', 'Person', Date.newInstance(2000, 1, 1), true);
        Account jointInsuredAccount = EWAccountDataFactory.createInsuredAccount('Mrs', 'Joint', 'Person', Date.newInstance(2000, 1, 1), true);
        Opportunity testOppty = EWOpportunityDataFactory.createOpportunityForJointInsured('TestOppty', insuredAccount.Id, jointInsuredAccount.Id, testBroker.Id);

        quote = EWQuoteDataFactory.createQuote('Quote Name', testOppty.Id, testBroker.Id, true);
    }
}