public with sharing class AssetPolicyTriggerHandler implements ITriggerHandler {
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return AssetPolicyTriggerHandler.IsDisabled != null ? AssetPolicyTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {
        PolicyHelper.setFormulaNumberField(newItems);
        EWPolicyHelper.onBeforeInsert(newItems);
    }

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        EWPolicyHelper.onBeforeUpdate(oldItems, newItems);
    }

    public void BeforeDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterInsert(Map<Id, SObject> newItems) {
        EWPolicyHelper.processNewPolicies(newItems);
        AuditLogUtil.createAuditLog('Asset', newItems.values(), null);
    }

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        EWPolicyHelper.processActivatedPolicies(oldItems, newItems);
        EWFinancialForceHelper.processIssuedPolicies(oldItems, newItems);
        AuditLogUtil.createAuditLog('Asset', newItems.values(), oldItems);
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {
    }

    public void AfterUndelete(Map<Id, SObject> oldItems) {
    }
}