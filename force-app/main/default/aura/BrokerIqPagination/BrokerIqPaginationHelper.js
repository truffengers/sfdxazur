({
	updatedBounds : function(component) {
        var totalItems = component.get('v.totalItems');
        var itemsPerPage = parseInt(component.get('v.itemsPerPage'), 10);

        document.cookie = "itemsPerPage=" + itemsPerPage;

        var pages = [1];
        
        for(var i=itemsPerPage; i < totalItems; i += itemsPerPage) {
            pages.push(pages.length + 1);
        }

        component.set('v.pages', pages);
	},
	updatedCurrent : function(component) {
		var totalItems = component.get('v.totalItems');
        var currentPage = component.get('v.currentPage');
        var itemsPerPage = parseInt(component.get('v.itemsPerPage'), 10);

        var pages = component.get('v.pages');

        if(currentPage > pages.length) {
        	currentPage = pages.length;
        	component.set('v.currentPage', currentPage);
        }

        component.set('v.firstVisibleItem', (currentPage-1)*itemsPerPage+1);
        var maybeLastItem = (currentPage)*itemsPerPage;
        component.set('v.lastVisibleItem', maybeLastItem > totalItems ? totalItems : maybeLastItem);
	}
})