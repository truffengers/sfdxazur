import { LightningElement, api } from 'lwc';

export default class Ix_SearchResult extends LightningElement {
    @api itemData;

    // For analytics
    @api currentlySelectedItem;

    displayResultingItem(event) {
        this.dispatchEvent(new CustomEvent('displayresultingitem', {
            detail: {
                value: event.detail.value
            }
        }))
    }
}