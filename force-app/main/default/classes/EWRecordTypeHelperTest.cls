/*
* @name: EWRecordTypeHelperTest
* @description:  Test class for EWRecordTypeHelper
* @author: Steve Loftus sloftus@azuruw.com
* @date: 04/11/2018
*/
@isTest
private class EWRecordTypeHelperTest {

    static testMethod void getRecordTypeIdByDeveloperNameTest() {
        string[] supportedSObjectTypes = new string[] { 'Account', 'Opportunity', 'Quote' };
        List<RecordType> recordTypes = [SELECT Id, Name, DeveloperName, Description, SobjectType, IsActive 
                                        FROM RecordType 
                                        WHERE SobjectType IN :supportedSObjectTypes];
        
        Id recordTypeId = null;

        // cache active record type(s)
        integer cachedNumber = 0;
        for (RecordType recordType : recordTypes) {

            if (recordType.SobjectType.equals(String.valueOf(Account.getSObjectType()))) {
                recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(String.valueOf(Account.getSObjectType()), recordType.DeveloperName);
                if (recordTypeId != null) {
                	cachedNumber++;
                }
            } else if (recordType.SobjectType.equals(String.valueOf(Opportunity.getSObjectType()))) {
                recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(String.valueOf(Opportunity.getSObjectType()), recordType.DeveloperName);
                if (recordTypeId != null) {
                	cachedNumber++;
                }
            } else if (recordType.SobjectType.equals(String.valueOf(Quote.getSObjectType()))) {
                recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(String.valueOf(Quote.getSObjectType()), recordType.DeveloperName);
                if (recordTypeId != null) {
                	cachedNumber++;
                }
            }
        }
        
        // run again to test caching
        integer retrievedNumber = 0;
        for (RecordType recordType : recordTypes) {

            if (recordType.SobjectType.equals(String.valueOf(Account.getSObjectType()))) {
                recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(String.valueOf(Account.getSObjectType()), recordType.DeveloperName);
                if (recordTypeId != null) {
	                retrievedNumber++;
	            }
            } else if (recordType.SobjectType.equals(String.valueOf(Opportunity.getSObjectType()))) {
                recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(String.valueOf(Opportunity.getSObjectType()), recordType.DeveloperName);
                if (recordTypeId != null) {
	                retrievedNumber++;
	            }
            } else if (recordType.SobjectType.equals(String.valueOf(Quote.getSObjectType()))) {
                recordTypeId = EWRecordTypeHelper.getInstance().getRecordTypeIdByDeveloperName(String.valueOf(Quote.getSObjectType()), recordType.DeveloperName);
                if (recordTypeId != null) {
	                retrievedNumber++;
	            }
            }
        }
        system.assertEquals(cachedNumber, retrievedNumber, 'Record Type Info caching has failed!');
    }

    static testMethod void getRecordTypeIdByObjectNameTest() {

        Map<String, Id> recordTypeIdByNameMap = new Map<String, Id>();

        // cache active record type(s)
        integer cachedNumber = 0;
        recordTypeIdByNameMap = EWRecordTypeHelper.getInstance().getRecordTypeIdMapByObjectName('Account');
        cachedNumber = recordTypeIdByNameMap.size();
        
        // run again to test caching
        integer retrievedNumber = 0;
        recordTypeIdByNameMap = EWRecordTypeHelper.getInstance().getRecordTypeIdMapByObjectName('Account');
        retrievedNumber = recordTypeIdByNameMap.size();

        system.assertEquals(cachedNumber, retrievedNumber, 'Record Type Info caching has failed!');
    }
}