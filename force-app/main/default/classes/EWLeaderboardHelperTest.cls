@isTest
public class EWLeaderboardHelperTest {
    private static User brokerUser;
    
    private static Account brokerAccount;
    private static Contact brokerContact;
    private static Profile brokerProfile;

    static testMethod void testGetLeaderboardData() {
        setup();

        List<Quote> resultQuotesList = new List<Quote>();

        System.runAs(brokerUser) {
            Opportunity opportunity = new Opportunity();

            opportunity.Name = 'Test Opp';
            opportunity.StageName = 'Open';
            opportunity.CloseDate = Date.today().addDays(30);

            insert opportunity;

            List<Quote> quotesList = new List<Quote>();

            for (integer i = 0; i < 5; i++) {
                Quote quote = new Quote();
                quote.Name = 'Broker test';
                quote.vlocity_ins__AgencyBrokerageId__c = brokerAccount.Id;
                quote.OpportunityId = opportunity.Id;

                quotesList.add(quote);
            }

            insert quotesList;

            Test.startTest();
            resultQuotesList = EWLeaderboardHelper.getLeaderboardData();
            Test.stopTest();
        }

        List<Quote> desiredResult = new List<Quote>([
            SELECT Id, Owner.Name
            FROM Quote
        ]);

        Map<Id,Quote> resultQuotesMap = new Map<Id,Quote>(resultQuotesList);
        Integer check = 0;
        for(Quote q : desiredResult){
            if(resultQuotesMap.containsKey(q.Id)) check++;
        }
        System.assertEquals(desiredResult.size(), check);
    }

    static void setup() {
        UserRole techteamUserRole = EWUserDataFactory.getUserRole(EWConstants.ROLE_TECHTEAM_NAME);
        User systemAdminUser = EWUserDataFactory.createSystemAdmistratorUser(
            EWConstants.PROFILE_SYSTEMADMINISTRATOR_NAME,
            techteamUserRole.Id,
            true);

        System.runAs(systemAdminUser) {
            brokerAccount = EWAccountDataFactory.createBrokerAccount(null, true);
            brokerContact = EWContactDataFactory.createContact(null, null, brokerAccount.Id, true);
            brokerProfile = EWUserDataFactory.getProfile(EWConstants.PROFILE_EW_PARTNER_NAME);
        }

        brokerUser = EWUserDataFactory.createPartnerUser('testBroker', brokerProfile.Id, brokerContact.Id, true);
    }

    static testMethod void testGetTopTips() {
        List<EW_TopTip__c> tipsToInsert = new List<EW_TopTip__c>();

        EW_TopTip__c tip1 = new EW_TopTip__c(Name = 'Tip 1', EW_TextContent__c = 'Some content');
        tipsToInsert.add(tip1);

        EW_TopTip__c tip2 = new EW_TopTip__c(Name = 'Tip 2', EW_TextContent__c = 'Some more content');
        tipsToInsert.add(tip2);

        insert tipsToInsert;

        Test.startTest();
        List<EW_TopTip__c> result = EWLeaderboardHelper.getTopTips();
        Test.stopTest();

        System.assertEquals(tipsToInsert, result);
    }

    static testMethod void testThumbsUp() {
        EW_TopTip__c tip = new EW_TopTip__c(Name = 'Tip', EW_TextContent__c = 'Some content');
        insert tip;

        Id tipId = tip.Id;

        Test.startTest();
        EWLeaderboardHelper.thumbsUpClicked(tipId);
        Test.stopTest();

        EW_TopTip__c currentTip = [
                SELECT Id, EW_ThumbsUp__c
                FROM EW_TopTip__c
                WHERE Id = :tipId
        ];

        System.assertEquals(currentTip.EW_ThumbsUp__c, 1);
    }

    static testMethod void testThumbsDown() {
        EW_TopTip__c tip = new EW_TopTip__c(Name = 'Tip', EW_TextContent__c = 'Some content');
        insert tip;

        Id tipId = tip.Id;

        Test.startTest();
        EWLeaderboardHelper.thumbsDownClicked(tipId);
        Test.stopTest();

        EW_TopTip__c currentTip = [
                SELECT Id, EW_ThumbsDown__c
                FROM EW_TopTip__c
                WHERE Id = :tipId
        ];

        System.assertEquals(currentTip.EW_ThumbsDown__c, 1);
    }
}