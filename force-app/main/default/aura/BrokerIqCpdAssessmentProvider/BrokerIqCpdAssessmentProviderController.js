({
	doInit : function(component, event, helper) {
		var recordId = component.get('v.recordId');

		if(!$A.util.isEmpty(recordId)) {
			var getSingleRecordAllFieldsAction = component.get('c.getSingleRecordAllFields');

			getSingleRecordAllFieldsAction.setParams({
				'recordId': recordId,
				'typeName': 'CPD_Assessment__c'
			});

			getSingleRecordAllFieldsAction.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === 'SUCCESS') {
	            	var cpdAssessmentRecord = response.getReturnValue();
	                component.set('v.cpdAssessmentRecord', cpdAssessmentRecord);
		        }
	        });
	        
	        $A.enqueueAction(getSingleRecordAllFieldsAction);	
		}
	}
})