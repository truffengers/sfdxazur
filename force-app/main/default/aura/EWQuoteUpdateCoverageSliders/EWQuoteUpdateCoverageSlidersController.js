({
    doInit: function (component, event, helper) {
        const recordId = component.get('v.recordId');
        let action = component.get("c.saveQuoteCoverageSliders");
        let brokerPercentageCommission = undefined;
        let excess = undefined;
        let premiumGrossIncIPT = undefined;

        try {
            let sPageURL = decodeURIComponent(window.location.search.substring(1));
            let sURLVariables = sPageURL.split('&');

            for (let i = 0; i < sURLVariables.length; i++) {
                let sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === 'bpc') {
                    brokerPercentageCommission = sParameterName[1] * 100;
                } else if (sParameterName[0] === 'ex') {
                    excess = sParameterName[1].replace('[', '').replace(']', '');
                } else if (sParameterName[0] === 'total') {
                    premiumGrossIncIPT = sParameterName[1];
                }
            }

            if (brokerPercentageCommission !== undefined && excess !== undefined) {
                action.setParams({
                    quoteId: recordId,
                    brokerPercentageCommission: brokerPercentageCommission,
                    excess: excess,
                    premiumGrossIncIPT: premiumGrossIncIPT
                });
                $A.enqueueAction(action);
            }
        } catch (e) {
            console.exception('EWQuoteUpdateCoverageSliders // Exception', e);
        }
    }
});