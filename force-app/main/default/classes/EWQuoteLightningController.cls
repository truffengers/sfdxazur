public with sharing class EWQuoteLightningController {

    @AuraEnabled
    public static Map<String, Object> getSetupData(String recordId) {
        Map<String, Object> result = new Map<String, Object>();
        if (String.isBlank(recordId)) {
            return result;
        }

        Id opportunityId;

        if (recordId.startsWith(Asset.getSObjectType().getDescribe().getKeyPrefix())) {
            Asset policy = [
                    SELECT Id, EW_Quote__c, EW_Quote__r.EW_JointInsuredQuote__c, EW_Quote__r.OpportunityId, AccountId
                    FROM Asset
                    WHERE Id = :recordId
            ];

            opportunityId = policy.EW_Quote__r.OpportunityId;
            result.put('quote', new Quote(Id = policy.EW_Quote__c, EW_JointInsuredQuote__c = policy.EW_Quote__r.EW_JointInsuredQuote__c, OpportunityId = policy.EW_Quote__r.OpportunityId));
            result.put('accountId', policy.AccountId);

        } else {
            Quote qte = [
                    SELECT Id,EW_JointInsuredQuote__c, OpportunityId, AccountId
                    FROM Quote
                    WHERE Id = :recordId
            ];
            opportunityId = qte.OpportunityId;
            result.put('quote', qte);
            result.put('accountId', qte.AccountId);
        }

        result.put('jointInsured', getJointInsured(opportunityId));
        return result;
    }

    @TestVisible
    private static Account getJointInsured(String opportunityId) {
        // gets the Joint Insured from the Opportunity details

        Account jointInsured = new Account();

        List<vlocity_ins__Application__c> applications = [
                SELECT Id,vlocity_ins__AccountId__c,vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
                FROM vlocity_ins__Application__c
                WHERE vlocity_ins__OpportunityId__c = :opportunityId
        ];
        if (!applications.isEmpty()) {
            List<vlocity_ins__Party__c> partys = [
                    SELECT Id,vlocity_ins__AccountId__c, vlocity_ins__AccountId__r.FirstName, vlocity_ins__AccountId__r.LastName,
                            vlocity_ins__AccountId__r.Salutation, vlocity_ins__AccountId__r.PersonBirthdate, vlocity_ins__AccountId__r.EW_BrokerAccount__c,
                            vlocity_ins__AccountId__r.PersonContactId
                    FROM vlocity_ins__Party__c
                    WHERE vlocity_ins__HouseholdId__c = :applications[0].vlocity_ins__PrimaryPartyId__r.vlocity_ins__HouseholdId__c
                    AND vlocity_ins__AccountId__c != null
                    AND vlocity_ins__PartyEntityType__c = 'Account'
                    AND vlocity_ins__AccountId__c != :applications[0].vlocity_ins__AccountId__c
            ];

            if (partys.size() == 1) {
                jointInsured.Id = partys[0].vlocity_ins__AccountId__c;
                jointInsured.FirstName = partys[0].vlocity_ins__AccountId__r.FirstName;
                jointInsured.LastName = partys[0].vlocity_ins__AccountId__r.LastName;
                jointInsured.Salutation = partys[0].vlocity_ins__AccountId__r.Salutation;
                jointInsured.PersonBirthdate = partys[0].vlocity_ins__AccountId__r.PersonBirthdate;
                jointInsured.EW_BrokerAccount__c = partys[0].vlocity_ins__AccountId__r.EW_BrokerAccount__c;
            }
        }
        return jointInsured;

    }

    @AuraEnabled
    public static vlocity_ins__InsurableItem__c getInsurableItem(String recordId) {
        if (String.isBlank(recordId)) {
            return new vlocity_ins__InsurableItem__c();
        }

        if (recordId.startsWith(Asset.getSObjectType().getDescribe().getKeyPrefix())) {
            Asset policy = [
                    SELECT Id, EW_Quote__c
                    FROM Asset
                    WHERE Id = :recordId
            ];
            recordId = policy.EW_Quote__c;
        }

        List<vlocity_ins__InsurableItem__c> insurableItems = new List<vlocity_ins__InsurableItem__c>([
                SELECT Id, EW_Quote__c, vlocity_ins__AccountId__c, EW_Insurance_In_Place__c, EW_PreviousInsuranceReason__c,
                        EW_Standard_Construction__c, EW_FlatRoofPercentage__c, EW_RoofConstruction__c, EW_WallConstruction__c,
                        EW_Not_Listed__c, EW_ListedStatus__c, EW_Not_Used_For_Business__c, EW_BusinessUse__c, EW_OpenToThePublic__c,
                        EW_UPRN__c, EW_AddressId__c, EW_RebuildConfidenceScore__c, EW_MarketValue__c,
                        EW_BuildingName__c, EW_BuildingNumber__c, EW_FlatName__c, EW_Address1__c, EW_Address2__c, EW_City__c, EW_State__c, EW_PostalCode__c, EW_Country__c,EW_PostTown__c
                FROM vlocity_ins__InsurableItem__c
                WHERE (EW_Quote__c = :recordId) AND EW_CoverType__c = :EWConstants.INSURABLE_ITEM_COVER_TYPE_PROPERTY AND EW_Active__c = TRUE
                LIMIT 1
        ]);
        return insurableItems.isEmpty() ? new vlocity_ins__InsurableItem__c() : insurableItems.get(0);
    }
}