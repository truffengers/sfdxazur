/**
* @name: IBA_SalesCreditNoteTriggerHandler
* @description: This class is handling and dispatching the relevant c2g__codaCreditNote__c trigger functionality
*
*/

public with sharing class IBA_SalesCreditNoteTriggerHandler implements ITriggerHandler
{
    public static Boolean IsDisabled;

    public Boolean IsDisabled() {
        return IBA_SalesCreditNoteTriggerHandler.IsDisabled != null ? IBA_SalesCreditNoteTriggerHandler.IsDisabled : false;
    }

    public void BeforeInsert(List<SObject> newItems) {}

    public void BeforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {}

    public void BeforeDelete(Map<Id, SObject> oldItems) {}

    public void AfterInsert(Map<Id, SObject> newItems) {}

    public void AfterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems) {
        Map<Id, c2g__codaCreditNote__c> policyTransactionMap = new Map<Id, c2g__codaCreditNote__c>();
        Map<Id, c2g__codaCreditNote__c> policyTransactionDueDateMap = new Map<Id, c2g__codaCreditNote__c>();
        for(Id recordId :newItems.keySet()) {
            c2g__codaCreditNote__c salesCreditNote = (c2g__codaCreditNote__c)newItems.get(recordId);
            c2g__codaCreditNote__c salesCreditNoteOld = (c2g__codaCreditNote__c)oldItems.get(recordId);
            if(salesCreditNote.IBA_PolicyTransaction__c != null && salesCreditNote.c2g__CreditNoteStatus__c == IBA_UtilConst.SALES_CREDIT_NOTE_STATUS_COMPLETE 
                && salesCreditNoteOld.c2g__CreditNoteStatus__c != IBA_UtilConst.SALES_CREDIT_NOTE_STATUS_COMPLETE) {
                    policyTransactionMap.put(salesCreditNote.IBA_PolicyTransaction__c, salesCreditNote);
            }
            if(salesCreditNote.c2g__DueDate__c != salesCreditNoteOld.c2g__DueDate__c && salesCreditNote.IBA_PolicyTransaction__c != null) {
                policyTransactionDueDateMap.put(salesCreditNote.IBA_PolicyTransaction__c, salesCreditNote);
            }
        }

        if(policyTransactionMap.size() > 0 || policyTransactionDueDateMap.size() > 0) {
            Set<Id> allPolicyTransactionIds = new Set<Id>();
            allPolicyTransactionIds.addAll(policyTransactionMap.keySet());
            allPolicyTransactionIds.addAll(policyTransactionDueDateMap.keySet());
            IBA_PolicyTransactionDataTransfer transferData = new IBA_PolicyTransactionDataTransfer(allPolicyTransactionIds);
            transferData.subtractCreditNoteTotal(policyTransactionMap)
                .changeSalesCreditNoteDueDate(policyTransactionDueDateMap)
                .transfer();
        }
    }

    public void AfterDelete(Map<Id, SObject> oldItems) {}

    public void AfterUndelete(Map<Id, SObject> oldItems) {}
}