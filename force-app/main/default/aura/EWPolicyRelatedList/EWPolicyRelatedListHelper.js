({
    fetchData: function (component) {
        const recordId = component.get('v.recordId');
        const baseUrl = component.get('v.baseUrl');
        
        var action = component.get('c.policyRelatedList');
        action.setParam('recordId', recordId);
        
        action.setCallback(this, function(response) {
         var records = response.getReturnValue();       
         records.forEach(function(record){
             record.linkName = baseUrl +'/asset/'+record.Id;
         });
         component.set('v.policies', records);
            
        });
        $A.enqueueAction(action);
        
    },
    
    getBaseUrl : function (component) {
        var urlString = window.location.href;
        var baseURL = urlString.substring(0, urlString.indexOf("/s")+2);
        component.set("v.baseUrl", baseURL);
    }
    
})