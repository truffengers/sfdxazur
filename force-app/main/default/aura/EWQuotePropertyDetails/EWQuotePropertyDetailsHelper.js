({
    doInit: function (component) {
    },
    getRecords: function (cmp) {
        let action = cmp.get("c.getInsurableItem");
        action.setParams({
            "recordId": cmp.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            if (response.getState() === "SUCCESS") {
                var returnValue = response.getReturnValue();
                
                cmp.set("v.insurableItem", returnValue);
                cmp.set("v.theRiskAddress", getAddress(returnValue));
                
            } else if (a.getState() === "ERROR") {
                console.log("Errors", response.getError());
            }
        });
        $A.enqueueAction(action);
        
        
        function getAddress(insurableItem){
   
        	var address =[];
            if(insurableItem.EW_BuildingName__c) address.push(insurableItem.EW_BuildingName__c);
    		if(insurableItem.EW_BuildingNumber__c) address.push(insurableItem.EW_BuildingNumber__c);
            if(insurableItem.EW_FlatName__c) address.push(insurableItem.EW_FlatName__c);
            if(insurableItem.EW_Address1__c) address.push(insurableItem.EW_Address1__c);
            if(insurableItem.EW_Address2__c) address.push(insurableItem.EW_Address2__c);
            if(insurableItem.EW_City__c) address.push(insurableItem.EW_City__c);
            //Added by Rajni - EWH-2053
            if(insurableItem.EW_PostTown__c) address.push(insurableItem.EW_PostTown__c);
            if(insurableItem.EW_State__c) address.push(insurableItem.EW_State__c);
            if(insurableItem.EW_PostalCode__c) address.push(insurableItem.EW_PostalCode__c);
            if(insurableItem.EW_Country__c) address.push(insurableItem.EW_Country__c);
        	return address.join(', ');
    	}
        
    }
    
});