public class BrokerIqLetMeInModalController {

	@AuraEnabled
	public static Map<String, Object> getProspectFromVisitorId(String visitorId) {
		PardotApi api = new PardotApi();

		Map<String, Object> visitorInfo = api.getVisitorInfo(visitorId);

		if(visitorInfo != null) {
			return (Map<String, Object>)visitorInfo.get('prospect');
		} else { 
			return null;
		}
	}
	@AuraEnabled
	public static Boolean isProspectFromVisitorId(String visitorId) {
		try {
			return getProspectFromVisitorId(visitorId) != null;
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage() + '\n' + e.getStackTraceString());
		}
	}
}