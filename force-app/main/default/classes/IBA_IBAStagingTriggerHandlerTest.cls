/**
* @name: IBA_IBAStagingTriggerHandlerTest
* @description: Test class for IBA_IBAStagingTriggerHandler
*
*/

@isTest
private class IBA_IBAStagingTriggerHandlerTest {

    private static final String PRODUCER_BROKER_CODE = 'TEST01';
    private static final String CARRIER_ACCOUNT_CODE = 'TEST02';
    private static final String ACCOUNTING_BROKER_CODE = 'TEST03';
    private static final String ACCOUNT_MGA_CODE = 'TEST04';
    private static final String ORIGINAL_CURRENCY_CODE = 'GBP';
    private static final String AIG_POLICY_NUMBER = '000000test';
    private static final String MINOR_LINE = 'PCG Test Liability';
    
    private static final Integer NUMBER_OF_STAGING_RECORDS = 250;

    @testSetup
    static void createTestRecords() {
        IBA_TestDataFactory.createUser(IBA_TestDataFactory.IBA_ACCOUNTANT_ID);

        List<Account> accounts = new List<Account>();
        Account producingBroker = IBA_TestDataFactory.createAccount(false, 'Producing Broker', IBA_UtilConst.ACCOUNT_RT_BROKER);
        producingBroker.EW_DRCode__c = PRODUCER_BROKER_CODE;
        producingBroker.c2g__CODAAccountTradingCurrency__c = ORIGINAL_CURRENCY_CODE;

        Account carrierAccount = IBA_TestDataFactory.createAccount(false, 'Carrier Account', IBA_UtilConst.ACCOUNT_RT_CARRIER);
        carrierAccount.EW_DRCode__c = CARRIER_ACCOUNT_CODE;
        carrierAccount.c2g__CODAAccountTradingCurrency__c = ORIGINAL_CURRENCY_CODE;

        Account mgaAccount = IBA_TestDataFactory.createAccount(false, 'MGA Account', IBA_UtilConst.ACCOUNT_RT_BUSINESS);
        mgaAccount.EW_DRCode__c = ACCOUNT_MGA_CODE;
        mgaAccount.c2g__CODAAccountTradingCurrency__c = ORIGINAL_CURRENCY_CODE;

        Account accountingBroker = IBA_TestDataFactory.createAccount(false, 'Accounting Broker', IBA_UtilConst.ACCOUNT_RT_BROKER);
        accountingBroker.EW_DRCode__c = ACCOUNTING_BROKER_CODE;
        accountingBroker.c2g__CODAAccountTradingCurrency__c = ORIGINAL_CURRENCY_CODE;

        accounts.add(producingBroker);
        accounts.add(carrierAccount);
        accounts.add(mgaAccount);
        accounts.add(accountingBroker);

        insert accounts;

        Product2 product = IBA_TestDataFactory.createProduct(false, 'Product 1', IBA_UtilConst.PRODUCT_RT_Master);
        product.IBA_AIGProduct__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
        product.IBA_AIGMinorLine__c = MINOR_LINE;
        product.IBA_AIGPolicyCodeNumber__c = AIG_POLICY_NUMBER;
        insert product;
    }

    @isTest
    static void matchFieldsBeforeInsertTest() {
        User accountantUser = [SELECT Id FROM User WHERE LastName = 'XYZ'];
        System.runAs(accountantUser) {
            List<IBA_IBAStaging__c> stagingRecords = IBA_TestDataFactory.createIBAStagingRecords(false, NUMBER_OF_STAGING_RECORDS);
            for(IBA_IBAStaging__c staging :stagingRecords) {
                staging.IBA_OriginalCurrencyCode__c = ORIGINAL_CURRENCY_CODE;
                staging.IBA_AIGProductName__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
                staging.IBA_AIGPolicyNumber__c = AIG_POLICY_NUMBER;
                staging.IBA_MinorLine__c = MINOR_LINE;
                staging.IBA_ProducerBrokerCode__c = PRODUCER_BROKER_CODE;
                staging.IBA_CarrierCode__c = CARRIER_ACCOUNT_CODE;
                staging.IBA_AccountBrokerCode__c = ACCOUNTING_BROKER_CODE;
                staging.IBA_MGACode__c = ACCOUNT_MGA_CODE;
            }

            Test.startTest();
            insert stagingRecords;
            Test.stopTest();

            Account producingBroker = new Account();
            Account carrierAccount = new Account();
            Account mgaAccount = new Account();
            Account accountingBroker = new Account();
            for(Account acc : [SELECT Id, EW_DRCode__c FROM Account WHERE c2g__CODAAccountTradingCurrency__c = :ORIGINAL_CURRENCY_CODE LIMIT 4]) {
                String code = acc.EW_DRCode__c;
                if(code == PRODUCER_BROKER_CODE) {
                    producingBroker = acc;
                } else if(code == CARRIER_ACCOUNT_CODE) {
                    carrierAccount = acc;
                } else if(code == ACCOUNT_MGA_CODE) {
                    mgaAccount = acc;
                } else if(code == ACCOUNTING_BROKER_CODE) {
                    accountingBroker = acc;
                }
            }
            
            Product2 product = [SELECT Id FROM Product2 WHERE IBA_AIGProduct__c = :IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY LIMIT 1];

            for(IBA_IBAStaging__c staging :[SELECT Id, IBA_ProducingBroker__c, IBA_AccountingBroker__c, IBA_CarrierAccount__c, IBA_MGAAccount__c, IBA_Product__c
                FROM IBA_IBAStaging__c WHERE Id IN :stagingRecords]) {
                System.assertEquals(producingBroker.Id, staging.IBA_ProducingBroker__c);
                System.assertEquals(carrierAccount.Id, staging.IBA_CarrierAccount__c);
                System.assertEquals(mgaAccount.Id, staging.IBA_MGAAccount__c);
                System.assertEquals(accountingBroker.Id, staging.IBA_AccountingBroker__c);
                System.assertEquals(product.Id, staging.IBA_Product__c);
            }
        }
    }


    @isTest
    static void matchFieldsBeforeUpdateTest() {
        User accountantUser = [SELECT Id FROM User WHERE LastName = 'XYZ'];
        System.runAs(accountantUser) {
            List<IBA_IBAStaging__c> stagingRecords = IBA_TestDataFactory.createIBAStagingRecords(false, NUMBER_OF_STAGING_RECORDS);
            for(IBA_IBAStaging__c staging :stagingRecords) {
                staging.IBA_OriginalCurrencyCode__c = ORIGINAL_CURRENCY_CODE;
                staging.IBA_AIGProductName__c = IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY;
                staging.IBA_AIGPolicyNumber__c = AIG_POLICY_NUMBER;
                staging.IBA_MinorLine__c = MINOR_LINE;
                staging.IBA_ProducerBrokerCode__c = PRODUCER_BROKER_CODE;
                staging.IBA_CarrierCode__c = CARRIER_ACCOUNT_CODE;
                staging.IBA_AccountBrokerCode__c = ACCOUNTING_BROKER_CODE;
                staging.IBA_MGACode__c = ACCOUNT_MGA_CODE;
            }
            insert stagingRecords;

            Test.startTest();
            update stagingRecords;
            Test.stopTest();

            Account producingBroker = new Account();
            Account carrierAccount = new Account();
            Account mgaAccount = new Account();
            Account accountingBroker = new Account();
            for(Account acc : [SELECT Id, EW_DRCode__c FROM Account WHERE c2g__CODAAccountTradingCurrency__c = :ORIGINAL_CURRENCY_CODE LIMIT 4]) {
                String code = acc.EW_DRCode__c;
                if(code == PRODUCER_BROKER_CODE) {
                    producingBroker = acc;
                } else if(code == CARRIER_ACCOUNT_CODE) {
                    carrierAccount = acc;
                } else if(code == ACCOUNT_MGA_CODE) {
                    mgaAccount = acc;
                } else if(code == ACCOUNTING_BROKER_CODE) {
                    accountingBroker = acc;
                }
            }
            
            Product2 product = [SELECT Id FROM Product2 WHERE IBA_AIGProduct__c = :IBA_UtilConst.PRODUCT_NAME_LOCAL_AUTHORITY LIMIT 1];

            for(IBA_IBAStaging__c staging :[SELECT Id, IBA_ProducingBroker__c, IBA_AccountingBroker__c, IBA_CarrierAccount__c, IBA_MGAAccount__c, IBA_Product__c
                FROM IBA_IBAStaging__c WHERE Id IN :stagingRecords]) {
                System.assertEquals(producingBroker.Id, staging.IBA_ProducingBroker__c);
                System.assertEquals(carrierAccount.Id, staging.IBA_CarrierAccount__c);
                System.assertEquals(mgaAccount.Id, staging.IBA_MGAAccount__c);
                System.assertEquals(accountingBroker.Id, staging.IBA_AccountingBroker__c);
                System.assertEquals(product.Id, staging.IBA_Product__c);
            }
        }
    }
}