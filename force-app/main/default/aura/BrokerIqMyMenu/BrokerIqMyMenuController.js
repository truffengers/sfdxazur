({
    doInit : function(component, event, helper) {
        var getUserAction = component.get('c.getUser');
        
        getUserAction.setStorable();
        
        getUserAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.user', response.getReturnValue());
            }
        });
        
        $A.enqueueAction(getUserAction);		
    }
})