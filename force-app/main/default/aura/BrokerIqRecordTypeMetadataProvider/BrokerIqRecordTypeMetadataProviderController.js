({
	doInit : function(component, event, helper) {
        if($A.util.isEmpty(component.get('v.recordTypeMetadatas'))) {
	        var getBrokerIqRecordTypeMetadataAction = component.get('c.getRecordsAllFields');

			getBrokerIqRecordTypeMetadataAction.setParams({
				typeName: "Broker_iQ_Record_Type__mdt",
				whereClause: "Id != null"
			});
			
			getBrokerIqRecordTypeMetadataAction.setStorable();

			getBrokerIqRecordTypeMetadataAction.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	            	var metadata = response.getReturnValue();
	            	var metadataByRecordTypeDeveloperName = {};

	            	for (var i = metadata.length - 1; i >= 0; i--) {
	            		metadataByRecordTypeDeveloperName[metadata[i].DeveloperName] = metadata[i];
	            	}
	                component.set('v.recordTypeMetadatas', metadata);
	                component.set('v.metadataByRecordTypeDeveloperName', metadataByRecordTypeDeveloperName);
		        }
	        });
	        
	        $A.enqueueAction(getBrokerIqRecordTypeMetadataAction);
		}
        
	},
	recordTypeDeveloperNameChanged : function(component, event, helper) {
		var metadataByRecordTypeDeveloperName = component.get('v.metadataByRecordTypeDeveloperName');
		var recordTypeMetadatas = component.get('v.recordTypeMetadatas');
		if(metadataByRecordTypeDeveloperName) {
			var recordTypeDeveloperName = component.get('v.recordTypeDeveloperName');
			var recordTypeId = component.get('v.recordTypeId');
			var recordTypeMetadata
			if(recordTypeDeveloperName) {
				recordTypeMetadata = metadataByRecordTypeDeveloperName[recordTypeDeveloperName];
			} else if(recordTypeId && recordTypeMetadatas) {
				recordTypeMetadata = recordTypeMetadatas.find(function(val) {return val.Id === recordTypeId});
			}
			if(recordTypeMetadata) {
				component.set('v.recordTypeMetadata', recordTypeMetadata);
			}
		}
	}
})