TT.brokerIqMenuItem = function($, params) {

  'use strict';

  var id            = params.id;
  var isMouseInput  = params.isMouseInput;
  var enterCallback = params.enterCallback;
  var leaveCallback = params.leaveCallback;
  var clickCallback = params.clickCallback;
  var touchCallback = params.touchCallback;
  var $element      = $(params.element);
  var $link;
  var isExpandable;
  var isExpanded;
  var payload;

  var HIDING  = 'hiding';
  var SHOWING = 'showing';

  var OPEN_CLASS     = 'is-overlayed';
  var EXPANDED_CLASS = 'is-expanded';

  var ON  = 'on';
  var OFF = 'off';

  var state;

  var CALLBACKS = {
    click      : clickCallback,
    mouseenter : enterCallback,
    mouseleave : leaveCallback,
    touchstart : touchCallback
  };



  function init(params) {
    state        = HIDING;
    isExpandable = $element.find('ul, ol').length > 0;
    isExpanded   = $element.hasClass(EXPANDED_CLASS);
    $link        = $element.find(params.linkSelector);
    payload      = { id: id, isExpandable: isExpandable };
    turnListeners(ON);
  }

  function turnListeners(onOrOff) {
    isMouseInput ? turnMouseListeners(onOrOff) : turnTouchListeners(onOrOff);
    $element[onOrOff]('click', eventHandler);
  }

  function eventHandler(event) {
    event.data = payload;
    CALLBACKS[event.type](event);
  }

  function turnMouseListeners(onOrOff) {
    $element[onOrOff]('mouseenter', eventHandler);
    $element[onOrOff]('mouseleave', eventHandler);
  }

  function turnTouchListeners(onOrOff) {
    $element[onOrOff]('touchstart', eventHandler);
  }

  function reveal() {
    if(state === HIDING) {
      state = SHOWING;
      $element.addClass(OPEN_CLASS);
    }
  }

  function conceal() {
    if(state === SHOWING) {
      state = HIDING;
      $element.removeClass(OPEN_CLASS);
    }
  }

  function expand() {
    $element.addClass(EXPANDED_CLASS);
    isExpanded = true;
  }

  function contract() {
    $element.removeClass(EXPANDED_CLASS);
    isExpanded = false;
  }

  function getIsExpanded() {
    return isExpanded;
  }

  function teardown() {
    turnListeners(OFF);
    id            = undefined;
    isMouseInput  = undefined;
    enterCallback = undefined;
    leaveCallback = undefined;
    clickCallback = undefined;
    touchCallback = undefined;
    $element      = undefined;
    $link         = undefined;
    isExpandable  = undefined;
    isExpanded    = undefined;
    payload       = undefined;
    state         = undefined;
  }



  return {
    init          : init,
    reveal        : reveal,
    conceal       : conceal,
    expand        : expand,
    contract      : contract,
    getIsExpanded : getIsExpanded,
    teardown      : teardown
  };

};