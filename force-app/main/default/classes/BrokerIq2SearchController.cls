/**
* @name: Azur Group
* @description: Apex controller to BrokerIqSearch lightning component
* @author: Azur Group
* @date: 
* @Modified: 28/11/2017 Ignacio Sarmiento 	Add the CII user logic
*/
public with sharing class BrokerIq2SearchController {
	
	public class Results {
		@AuraEnabled
		public List<IQ_Content__c> iqContentPage;
		@AuraEnabled 
		public Integer totalItems;
	}

	@AuraEnabled
	public static Results doSearch(String searchString,
	                               Integer pageOffset, Integer pageLimit, 
	                               String selectedTopic,
	                               String selectedContentType,
	                               String selectedContributor,
	                               Boolean isUpcoming) {
		BrokerIq2SearchController bisc = new BrokerIq2SearchController();

		// Re-evaluate page Integers as Integer to workaround bug in Lightning
		// See https://salesforce.stackexchange.com/questions/108355/limit-expression-must-be-of-type-integer-error-when-using-apex-variable-in-soq
		bisc.searchString = searchString;
		bisc.pageOffset = Integer.valueOf(pageOffset);
		bisc.pageLimit = Integer.valueOf(pageLimit);
		bisc.selectedTopic = selectedTopic;
		bisc.selectedContentType = selectedContentType;
		bisc.selectedContributor = selectedContributor;
		bisc.isUpcoming = isUpcoming;

		Results rval = bisc.pDoSearch();
		return rval;
	}

	private String limitClause;
	private String orderByClause;
	private String whereClause;
	private DateTime now;
	private Id networkId;

	private String searchString;
	private Integer pageOffset;
	private Integer pageLimit;
	private String selectedTopic;
	private String selectedContentType;
	private String selectedContributor;
	private Boolean isUpcoming;

	public BrokerIq2SearchController() {
		now = DateTime.now();
		networkId = Network.getNetworkId();
	}

	//NB: We don't just get the where clauses and do one big soql query because:
	// "Semi join sub-selects are only allowed at the top level WHERE expressions 
	// and not in nested WHERE expressions."
	private List<IQ_Content__c> getIqContentFromTopics(List<Topic> topics) {
		Map<Id, Topic> topicMap = new Map<Id, Topic>(topics);
		Set<Id> topicIds = topicMap.keySet();

		String topicWhereClause = whereClause + ' AND Id IN (SELECT EntityId FROM TopicAssignment WHERE TopicId IN :topicIds AND NetworkId = :networkId AND EntityType = \'Iq_Content\')';

		String topicQuery = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 'BiQ_2_Card_Grid', new Set<String>(), topicWhereClause, null, null);
		return Database.query(topicQuery);
	}
	private List<IQ_Content__c> getIqContentFromWebcasts(List<BrightTALK__Webcast__c> webcasts) {
		Map<Id, BrightTALK__Webcast__c> webcastMap = new Map<Id, BrightTALK__Webcast__c>(webcasts);
		Set<Id> webcastIds = webcastMap.keySet();

		String webcastWhereClause = whereClause + ' AND BrightTALK_Webcast__c IN :webcastIds';

		String webcastQuery = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 'BiQ_2_Card_Grid', new Set<String>(), webcastWhereClause, null, null);
		return Database.query(webcastQuery);
	}
	private List<IQ_Content__c> getIqContentFromContributors(List<Broker_iQ_Contributor__c> contributors) {
		Map<Id, Broker_iQ_Contributor__c> contributorMap = new Map<Id, Broker_iQ_Contributor__c>(contributors);
		Set<Id> contributorIds = contributorMap.keySet();

		String contributorWhereClause = whereClause + ' AND Broker_iQ_Contributor__c IN :contributorIds';

		String contributorQuery = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 'BiQ_2_Card_Grid', new Set<String>(), contributorWhereClause, null, null);
		return Database.query(contributorQuery);
	}

	private void fillResultsSearch(Results rval) {
		String query = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 'BiQ_2_Card_Grid', new Set<String>(), whereClause, null, null);
		searchString = String.escapeSingleQuotes(searchString);
		String searchQuery = 'FIND \'' + searchString + '\' ' + query;
		searchQuery = searchQuery.replace('SELECT ', 'RETURNING IQ_Content__c(');
		searchQuery = searchQuery.replace('FROM IQ_Content__c ', '');
		searchQuery = searchQuery + '), Topic(Name, Id), BrightTALK__Webcast__c(Name, Id), Broker_iQ_Contributor__c(Full_Name__c) ';
		List<List<sObject>> searchResultsAllObjects = Search.query(searchQuery);
		
		Map<Id, IQ_Content__c> results = new Map<Id, IQ_Content__c>((List<IQ_Content__c>)searchResultsAllObjects[0]);
		results.putAll(getIqContentFromTopics(searchResultsAllObjects[1]));
		results.putAll(getIqContentFromWebcasts(searchResultsAllObjects[2]));
		results.putAll(getIqContentFromContributors(searchResultsAllObjects[3]));
		List<IQ_Content__c> iqContentResults = results.values();

		List<sObjectComparable> comparableResults = sObjectComparable.wrapSObjectList(iqContentResults, IQ_Content__c.Content_Date__c, true);
		comparableResults.sort();
		List<IQ_Content__c> sortedIqContentResults = sObjectComparable.unwrapSObjectComparableList(comparableResults);

		rval.totalItems = sortedIqContentResults.size();
		rval.iqContentPage = new List<IQ_Content__c>();
		Integer pageStart = pageOffset;
		for(Integer i=pageStart; i < Math.min(sortedIqContentResults.size(), pageStart + pageLimit); i++) {
        	rval.iqContentPage.add((IQ_Content__c)sortedIqContentResults[i]);
        }

	}

	private void fillResultsQuery(Results rval) {
		String query = Nebula_Tools.FieldSetUtils.queryFieldSet('IQ_Content__c', 'BiQ_2_Card_Grid', new Set<String>(), whereClause, orderByClause, limitClause);
		rval.iqContentPage = Database.query(query);
		List<AggregateResult> totals = Database.query('SELECT COUNT(Id) total FROM IQ_Content__c WHERE ' + whereClause);
		rval.totalItems = (Integer)totals[0].get('total');
	}

	private Results pDoSearch() {
		Results rval = new Results();
		limitClause = ' LIMIT :pageLimit OFFSET :pageOffset ';
		whereClause = 'Active__c = true ';

		if(selectedContentType != null) {
			whereClause += 'AND RecordType.Id = :selectedContentType ';
		}
		if(selectedContributor != null) {
			whereClause += 'AND Broker_iQ_Contributor__c = :selectedContributor ';
		}

		if(selectedTopic != null) {
			whereClause += 'AND Id IN (SELECT EntityId FROM TopicAssignment WHERE TopicId = :selectedTopic AND NetworkId = :networkId AND EntityType = \'Iq_Content\') ';
		}
		
		if(isUpcoming == true) {
			whereClause += ' AND BrightTALK_Webcast__r.BrightTALK__Start_Date__c > :now ';
			orderByClause = 'BrightTALK_Webcast__r.BrightTALK__Start_Date__c ASC';
		} else if(isUpcoming == false) {
			whereClause += ' AND BrightTALK_Webcast__r.BrightTALK__Start_Date__c < :now ';
			orderByClause = 'BrightTALK_Webcast__r.BrightTALK__Start_Date__c DESC';
		} else {
			orderByClause = 'Content_Date__c DESC';
		}
		

		if(searchString != null) {
			fillResultsSearch(rval);
		} else {
			fillResultsQuery(rval);
		}


		return rval;
	}
}