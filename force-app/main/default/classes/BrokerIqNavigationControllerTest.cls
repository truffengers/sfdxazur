@isTest
private class BrokerIqNavigationControllerTest {
	
	@isTest static void basic() {
		BrokerIqNavigationController nc = new BrokerIqNavigationController();
		List<BrokerIqNavigationController.NavigationLink> links = new List<BrokerIqNavigationController.NavigationLink>{
			new BrokerIqNavigationController.NavigationLink('CPD Webinars', '#webinarsLink'),
			new BrokerIqNavigationController.NavigationLink('Test Name', '#a_test')
		};

		nc.navigationListJsonProp = JSON.serialize(links);

		System.assertEquals(1, nc.links.size());
		System.assertEquals(links[0].label, nc.links[0].label);
		System.assertEquals(links[0].link, nc.links[0].link);
		System.assertEquals(links[1].label, nc.lastLink.label);
		System.assertEquals(links[1].link, nc.lastLink.link);
	}
	
}