/**
* @name: IBA_PolicyTransactionServiceTest
* @description: Test class for IBA_PolicyTransactionService class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 02/01/2020
*/
@IsTest
public with sharing class IBA_PolicyTransactionServiceTest {

    @TestSetup
    private static void createTestData(){
        IBA_TestDataFactory.createCustomSettings();
        IBA_IBACustomSettings__c settings = IBA_IBACustomSettings__c.getOrgDefaults();

        Asset policy1 = IBA_TestDataFactory.createPolicy(false, '1', settings.IBA_DummyAccount__c);
        policy1.Currency_Type__c = 'GBP';
        Asset policy2 = IBA_TestDataFactory.createPolicy(false, '2', settings.IBA_DummyAccount__c);
        policy2.Currency_Type__c = 'USD';
        Asset policy3 = IBA_TestDataFactory.createPolicy(false, '3', settings.IBA_DummyAccount__c);
        policy3.Currency_Type__c = 'EUR';
        insert new List<Asset>{policy1, policy2, policy3};

        c2g__codaCompany__c company = IBA_TestDataFactory.createCompany(true, 'IBA Trust Accounts', IBA_UtilConst.COMPANY_RT_VAT);
        System.runAs(new User(Id=UserInfo.getUserId())) {
            c2g.CODACompanyWebService.createQueue(company.Id, 'GBP', company.Name + ' Test');
            c2g.CODAYearWebService.calculatePeriods(null);
            c2g.CODACompanyWebService.activateCompany(company.Id, 'GBP', company.Name);
            IBA_TestDataFactory.createUserCompany(true, company.Id, UserInfo.getUserId());
            String queueName = 'FF ' + company.Name;
            Group companyGroup = [SELECT Id FROM Group WHERE Name = :queueName AND Type = 'Queue' LIMIT 1];
            insert new GroupMember(GroupId = companyGroup.Id, UserOrGroupId = UserInfo.getUserId());
        }

        c2g__codaAccountingCurrency__c currencyUSD = new c2g__codaAccountingCurrency__c(
                Name = 'USD',
                c2g__DecimalPlaces__c = 2,
                c2g__OwnerCompany__c = company.Id,
                CurrencyIsoCode = 'USD'
        );
        c2g__codaAccountingCurrency__c currencyEUR = new c2g__codaAccountingCurrency__c(
                Name = 'EUR',
                c2g__DecimalPlaces__c = 2,
                c2g__OwnerCompany__c = company.Id,
                CurrencyIsoCode = 'EUR'
        );
        insert new List<c2g__codaAccountingCurrency__c>{
                currencyUSD,
                currencyEUR
        };

        c2g__codaExchangeRate__c exchangeRateUSDGoodA = new c2g__codaExchangeRate__c (
                c2g__ExchangeRateCurrency__c = currencyUSD.Id,
                c2g__StartDate__c = Date.newInstance(2019, 1, 1),
                c2g__Rate__c = 2.1
        );
        c2g__codaExchangeRate__c exchangeRateUSDGoodB = new c2g__codaExchangeRate__c (
                c2g__ExchangeRateCurrency__c = currencyUSD.Id,
                c2g__StartDate__c = Date.newInstance(2019, 2, 1),
                c2g__Rate__c = 2.2
        );
        c2g__codaExchangeRate__c exchangeRateUSDBad = new c2g__codaExchangeRate__c (
                c2g__ExchangeRateCurrency__c = currencyUSD.Id,
                c2g__StartDate__c = Date.newInstance(2019, 3, 1),
                c2g__Rate__c = 2.0
        );
        c2g__codaExchangeRate__c exchangeRateEURGood = new c2g__codaExchangeRate__c (
                c2g__ExchangeRateCurrency__c = currencyEUR.Id,
                c2g__StartDate__c = Date.newInstance(2019, 1, 1),
                c2g__Rate__c = 3.0
        );
        insert new List<c2g__codaExchangeRate__c>{
                exchangeRateUSDGoodA,
                exchangeRateUSDGoodB,
                exchangeRateUSDBad,
                exchangeRateEURGood
        };

    }

    @IsTest
    private static void testBeforeInsertPopulateExchangeRate() {
        // Prepare data
        List<Asset> policies = [
                SELECT Id
                FROM Asset
                ORDER BY Name
        ];

        IBA_PolicyTransaction__c transaction1 = IBA_TestDataFactory.createPolicyTransaction(false, policies[0].Id);
        transaction1.IBA_PeriodInput__c = '2019/001';
        IBA_PolicyTransaction__c transaction2A = IBA_TestDataFactory.createPolicyTransaction(false, policies[1].Id);
        transaction2A.IBA_PeriodInput__c = '2019/001';
        IBA_PolicyTransaction__c transaction2B = IBA_TestDataFactory.createPolicyTransaction(false, policies[1].Id);
        transaction2B.IBA_PeriodInput__c = '2019/002';
        IBA_PolicyTransaction__c transaction3 = IBA_TestDataFactory.createPolicyTransaction(false, policies[2].Id);
        transaction3.IBA_PeriodInput__c = '2019/001';
        IBA_PolicyTransaction__c transaction4 = IBA_TestDataFactory.createPolicyTransaction(false, policies[2].Id);
        transaction4.IBA_PeriodInput__c = '2019/002';

        // Perform test
        Test.startTest();
        insert new List<IBA_PolicyTransaction__c> {
                transaction1,
                transaction2A,
                transaction2B,
                transaction3,
                transaction4
        };
        Test.stopTest();

        // Verify results
        List<IBA_PolicyTransaction__c> transactions = [
                SELECT IBA_ExchangeRate__c
                FROM IBA_PolicyTransaction__c
                ORDER BY Name
        ];

        System.assertEquals(1, transactions[0].IBA_ExchangeRate__c);
        System.assertEquals(2.1, transactions[1].IBA_ExchangeRate__c);
        System.assertEquals(2.2, transactions[2].IBA_ExchangeRate__c);
        System.assertEquals(3, transactions[3].IBA_ExchangeRate__c);
        System.assertEquals(null, transactions[4].IBA_ExchangeRate__c);
    }

    @IsTest
    private static void testBeforeUpdatePopulateExchangeRate() {
        // Prepare data
        List<Asset> policies = [
                SELECT Id
                FROM Asset
                ORDER BY Name
        ];

        IBA_PolicyTransaction__c transaction1 = IBA_TestDataFactory.createPolicyTransaction(false, policies[0].Id);
        transaction1.IBA_PeriodInput__c = '2018/001';
        IBA_PolicyTransaction__c transaction2A = IBA_TestDataFactory.createPolicyTransaction(false, policies[1].Id);
        transaction2A.IBA_PeriodInput__c = '2018/001';
        IBA_PolicyTransaction__c transaction2B = IBA_TestDataFactory.createPolicyTransaction(false, policies[1].Id);
        transaction2B.IBA_PeriodInput__c = '2018/002';
        IBA_PolicyTransaction__c transaction3 = IBA_TestDataFactory.createPolicyTransaction(false, policies[2].Id);
        transaction3.IBA_PeriodInput__c = '2018/001';
        IBA_PolicyTransaction__c transaction4 = IBA_TestDataFactory.createPolicyTransaction(false, policies[2].Id);
        transaction4.IBA_PeriodInput__c = '2019/002';

        List<IBA_PolicyTransaction__c> transactions = new List<IBA_PolicyTransaction__c> {
                transaction1,
                transaction2A,
                transaction2B,
                transaction3,
                transaction4
        };

        insert transactions;

        transaction1.IBA_PeriodInput__c = '2019/001';
        transaction2A.IBA_PeriodInput__c = '2019/001';
        transaction2B.IBA_PeriodInput__c = '2019/002';
        transaction3.IBA_PeriodInput__c = '2019/001';

        // Perform test
        Test.startTest();
        update transactions;
        Test.stopTest();

        // Verify results
        transactions = [
                SELECT IBA_ExchangeRate__c
                FROM IBA_PolicyTransaction__c
                ORDER BY Name
        ];

        System.assertEquals(1, transactions[0].IBA_ExchangeRate__c);
        System.assertEquals(2.1, transactions[1].IBA_ExchangeRate__c);
        System.assertEquals(2.2, transactions[2].IBA_ExchangeRate__c);
        System.assertEquals(3, transactions[3].IBA_ExchangeRate__c);
        System.assertEquals(null, transactions[4].IBA_ExchangeRate__c);
    }

    @IsTest
    private static void volumeTestBeforeInsertPopulateExchangeRate() {
        // Prepare data
        List<Asset> policies = [
                SELECT Id
                FROM Asset
                ORDER BY Name
        ];

        List<IBA_PolicyTransaction__c> transactions = new List<IBA_PolicyTransaction__c>();
        List<IBA_PolicyTransaction__c> transactions1 = new List<IBA_PolicyTransaction__c>();
        List<IBA_PolicyTransaction__c> transactions2A = new List<IBA_PolicyTransaction__c>();
        List<IBA_PolicyTransaction__c> transactions2B = new List<IBA_PolicyTransaction__c>();
        List<IBA_PolicyTransaction__c> transactions3 = new List<IBA_PolicyTransaction__c>();
        List<IBA_PolicyTransaction__c> transactions4 = new List<IBA_PolicyTransaction__c>();

        for (Integer i = 0; i<40; ++i) {
            IBA_PolicyTransaction__c transaction1 = IBA_TestDataFactory.createPolicyTransaction(false, policies[0].Id);
            transaction1.IBA_PeriodInput__c = '2019/001';
            IBA_PolicyTransaction__c transaction2A = IBA_TestDataFactory.createPolicyTransaction(false, policies[1].Id);
            transaction2A.IBA_PeriodInput__c = '2019/001';
            IBA_PolicyTransaction__c transaction2B = IBA_TestDataFactory.createPolicyTransaction(false, policies[1].Id);
            transaction2B.IBA_PeriodInput__c = '2019/002';
            IBA_PolicyTransaction__c transaction3 = IBA_TestDataFactory.createPolicyTransaction(false, policies[2].Id);
            transaction3.IBA_PeriodInput__c = '2019/001';
            IBA_PolicyTransaction__c transaction4 = IBA_TestDataFactory.createPolicyTransaction(false, policies[2].Id);
            transaction4.IBA_PeriodInput__c = '2019/002';

            transactions1.add(transaction1);
            transactions2A.add(transaction2A);
            transactions2B.add(transaction2B);
            transactions3.add(transaction3);
            transactions4.add(transaction4);
        }
        transactions.addAll(transactions1);
        transactions.addAll(transactions2A);
        transactions.addAll(transactions2B);
        transactions.addAll(transactions3);
        transactions.addAll(transactions4);

        // Perform test
        Test.startTest();
        insert transactions;
        Test.stopTest();

        // Verify results
        transactions = [
                SELECT IBA_ExchangeRate__c
                FROM IBA_PolicyTransaction__c
                ORDER BY IBA_OriginalPolicy__r.Name, IBA_PeriodInput__c
        ];

        System.assertEquals(1, transactions[0].IBA_ExchangeRate__c);
        System.assertEquals(2.1, transactions[40].IBA_ExchangeRate__c);
        System.assertEquals(2.2, transactions[80].IBA_ExchangeRate__c);
        System.assertEquals(3, transactions[120].IBA_ExchangeRate__c);
        System.assertEquals(null, transactions[160].IBA_ExchangeRate__c);
    }
}