/**
* @name: PHO_CreateOperationalDataJobTest
* @description: Test class for PHO_CreateOperationalDataJob class.
* @author: Konrad Wlazlo kwlazlo@azuruw.com
* @date: 15/01/2020
* @lastChangedBy: Antigoni D'Mello     24/03/2020    PR-35 Added logic to send email with job result
* @lastChangedBy: Antigoni D'Mello     26/08/2020    PR-107: Changed the amount of logs expected
* @lastChangedBy: Antigoni D'Mello     26/01/2021    PR-113: Changed carrier DR Code and added Carrier in products
*/
@IsTest
public class PHO_CreateOperationalDataJobTest {

    private static final String CURRENCY_GBP = 'GBP';

    @TestSetup
    private static void createTestData() {
        String brokerDrCode = 'BAD';
        String carrierDrCode = 'CR0002';
        String mgaDrCode = 'MAD';
        String alternateReference = 'DR8059';

        Account dummyAccount = TestDataFactory.createBrokerageAccount('Dummy Account');

        Account producingBrokerAccount1 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAccount_DR8059');
        producingBrokerAccount1.EW_DRCode__c = alternateReference;
        producingBrokerAccount1.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account accountingBrokerAccount = TestDataFactory.createBrokerageAccount('TestAccountingBrokerAccount');
        accountingBrokerAccount.EW_DRCode__c = brokerDrCode;
        accountingBrokerAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account carrierAccount = TestDataFactory.createCarrierAccount('TestCarrierAccount');
        carrierAccount.EW_DRCode__c = carrierDrCode;
        carrierAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        Account mgaAccount = TestDataFactory.createBrokerageAccount('TestMGAAccount');
        mgaAccount.EW_DRCode__c = mgaDrCode;
        mgaAccount.c2g__CODAAccountTradingCurrency__c = CURRENCY_GBP;

        insert new List<Account>{
                dummyAccount,
                producingBrokerAccount1,
                accountingBrokerAccount,
                carrierAccount,
                mgaAccount
        };
        IBA_IBACustomSettings__c settings = new IBA_IBACustomSettings__c (
                PHO_PhoenixAccBrokerAccountDRCode__c = brokerDrCode,
                PHO_PhoenixMGAAccountDRCode__c = mgaDrCode,
                PHO_FixedHomeBrokerCommission__c = 36.7,
                PHO_FixedMotorBrokerCommission__c = 26.45,
                IBA_DummyAccount__c = dummyAccount.Id
        );
        insert settings;

        carrierAccount = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Carrier'];

        Product2 p1 = new Product2(Name = 'Motor Comprehensive', PHO_PhoenixProductCode__c = 'MOT01',  PHO_PhoenixSectionCode__c = 'MT01',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor');
        insert p1;

        PHO_PhoenixStaging__c stagingRecord = new PHO_PhoenixStaging__c(
                PHO_CurrencyType__c = CURRENCY_GBP,
                PHO_AlternateReference__c = alternateReference,
                PHO_ProductCode__c = 'MOT01',
                PHO_SectionCode__c = 'MT01',
                PHO_Premium__c = 67,
                PHO_Commission__c = 10.05,
                PHO_ExpiryDate__c = Datetime.now(),
                PHO_InceptionDate__c = Datetime.now(),
                PHO_EffectiveDate__c = Datetime.now(),
                PHO_EntryType__c = 'NB',
                PHO_PaymentMethod__c = 'Money',
                PHO_PaymentReference__c = '123456',
                PHO_CarrierDRCode__c = carrierDrCode,
                PHO_CompanyCode__c = 'BRD01'
        );
        insert stagingRecord;
    }

    @IsTest
    private static void testExecuteSuccess() {
        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_CreateOperationalDataJob(1, null));
        Test.stopTest();

        // Verify results
        PHO_PhoenixStaging__c stagingRecordQueried = [
                SELECT PHO_Premium__c
                FROM PHO_PhoenixStaging__c
        ];
        IBA_PolicyTransactionLine__c transactionLineQueried = [
                SELECT IBA_GrossWrittenPremium__c
                FROM IBA_PolicyTransactionLine__c
        ];
        List<Azur_Log__c> azurLogs = [SELECT Id FROM Azur_Log__c LIMIT 1];

        System.assertEquals(stagingRecordQueried.PHO_Premium__c, transactionLineQueried.IBA_GrossWrittenPremium__c, 'Financial data should be copied from staging record to Transaction Line Item');
        System.assertEquals(1, azurLogs.size(), 'There should not have been any custom logs generated');
    }

    @IsTest
    private static void testExecuteNoRecords() {
        // Prepare data
        PHO_PhoenixStaging__c stagingRecord = [
                SELECT PHO_ProductCode__c,
                        PHO_Product__c
                FROM PHO_PhoenixStaging__c
        ];
        // with no product staging record will have a status "Missing data" and so will not be queried in job below
        stagingRecord.PHO_ProductCode__c = 'x';
        stagingRecord.PHO_Product__c = null;
        update stagingRecord;

        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_CreateOperationalDataJob(1, null));
        Test.stopTest();

        List<Azur_Log__c> azurLogs = [SELECT Id FROM Azur_Log__c LIMIT 1];

        // Verify results
        System.assertEquals(0, [SELECT COUNT() FROM Asset], 'No operational data should be created');
        System.assertEquals(0, azurLogs.size(), 'Theres should not have been any custom logs generated');
    }

    @IsTest
    private static void testExecuteException() {
        // Prepare data
        PHO_PhoenixStaging__c stagingRecord = [
                SELECT PHO_EntryType__c
                FROM PHO_PhoenixStaging__c
        ];
        // empty PHO_InceptionDate__c field causes an exception
        stagingRecord.PHO_InceptionDate__c = null;
        update stagingRecord;

        // Perform test
        Test.startTest();
        // Chaining jobs in test classes is not possible...
        System.enqueueJob(new PHO_CreateOperationalDataJob(1, null));
        Test.stopTest();

        List<Azur_Log__c> azurLogs = [SELECT Id FROM Azur_Log__c];

        // Verify results
        System.assertEquals(0, [SELECT COUNT() FROM Asset], 'No operational data should be created');
        System.assertEquals(1, azurLogs.size(), 'Theres should have been one custom log generated');
    }

    @IsTest
    private static void testGroupStagingRecordsByEventNumber() {
        // Prepare data
        String policyNumber1 = '1';
        String policyNumber2 = '2';
        String policyNumber3 = '3';
        Datetime effectiveDate2 = Datetime.now().addDays(1);
        Datetime effectiveDate3 = Datetime.now().addDays(2);
        Decimal eventNumber1 = 1;
        Decimal eventNumber2 = 2;
        Decimal eventNumber3 = 3;
        PHO_PhoenixStaging__c stagingRecord1 = [
                SELECT PHO_CurrencyType__c,
                        PHO_AlternateReference__c,
                        PHO_ProductCode__c,
                        PHO_SectionCode__c,
                        PHO_Premium__c,
                        PHO_Commission__c,
                        PHO_ExpiryDate__c,
                        PHO_InceptionDate__c,
                        PHO_EffectiveDate__c,
                        PHO_EntryType__c,
                        PHO_PaymentMethod__c,
                        PHO_CarrierDRCode__c
                FROM PHO_PhoenixStaging__c
        ];
        stagingRecord1.PHO_PolicyNumber__c = policyNumber1;
        stagingRecord1.PHO_EventNumber__c = eventNumber1;
        update stagingRecord1;
        PHO_PhoenixStaging__c stagingRecord1A = stagingRecord1.clone();
        PHO_PhoenixStaging__c stagingRecord2 = stagingRecord1.clone();
        stagingRecord2.PHO_PolicyNumber__c = policyNumber2;
        stagingRecord2.PHO_EffectiveDate__c = effectiveDate2;
        stagingRecord2.PHO_EventNumber__c = eventNumber2;
        PHO_PhoenixStaging__c stagingRecord3 = stagingRecord1.clone();
        stagingRecord3.PHO_PolicyNumber__c = policyNumber3;
        stagingRecord3.PHO_EffectiveDate__c = effectiveDate3;
        stagingRecord3.PHO_EventNumber__c = eventNumber3;
        PHO_PhoenixStaging__c stagingRecord3A = stagingRecord3.clone();

        insert new List<PHO_PhoenixStaging__c>{stagingRecord1A, stagingRecord2, stagingRecord3, stagingRecord3A};

        // Perform test
        Test.startTest();
        PHO_CreateOperationalDataJob job = new PHO_CreateOperationalDataJob(4, null);
        Test.stopTest();

        // Verify results
        System.assertEquals(3, job.stagingRecords.size(), 'Only 3 staging records should be selected.');
        System.assertEquals(stagingRecord1.Id, job.stagingRecords[0].Id, 'Staging records should be put in a specific order.');
        System.assertEquals(stagingRecord1A.Id, job.stagingRecords[1].Id, 'Staging records should be put in a specific order.');
        System.assertEquals(stagingRecord2.Id, job.stagingRecords[2].Id, 'Staging records should be put in a specific order.');
    }
}