global class sObjectComparable implements Comparable {

	private sObject obj;
	private Schema.SObjectField fieldToCompare;
	private Boolean isDescending;

	global sObjectComparable(sObject obj, Schema.SObjectField fieldToCompare, Boolean isDescending) {
		this.obj = obj;
		this.fieldToCompare = fieldToCompare;
		this.isDescending = isDescending;	
	}

	global Integer compareTo(Object other) {
		Object thisVal = obj.get(fieldToCompare);
		Object otherVal = ((sObjectComparable)other).obj.get(fieldToCompare);

		Integer result;
		if(thisVal instanceof Date) {
			Date thisDate = (Date)thisVal;
			Date otherDate = (Date)otherVal;

			result = thisDate > otherDate ? 1 : (thisDate < otherDate ? -1 : 0);
		}
		
		return isDescending ? -result : result;
	}

	global static List<sObjectComparable> wrapSObjectList(List<sObject> sObjects, Schema.SObjectField fieldToCompare, Boolean isDescending) {
		List<sObjectComparable> rval = new List<sObjectComparable>();

		for(sObject obj : sObjects) {
			rval.add(new sObjectComparable(obj, fieldToCompare, isDescending));
		}

		return rval;
	}
	global static List<sObject> unwrapSObjectComparableList(List<sObjectComparable> sObjectComparables) {
		List<sObject> rval = new List<sObject>();

		for(sObjectComparable wrappedObj : sObjectComparables) {
			rval.add(wrappedObj.obj);
		}

		return rval;
	}
}