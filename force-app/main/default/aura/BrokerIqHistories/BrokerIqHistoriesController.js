({
	appendToTopics : function(component, event, helper) {
		var metadataByTopicName = component.get('v.metadataByTopicName');
		var yearList = component.get('v.yearList');
        var yearListNeedsUpdate = false;

        if(!$A.util.isEmpty(yearList) && !$A.util.isEmpty(metadataByTopicName)) {
            yearList.forEach(function(thisYear) {
                thisYear.data.forEach(function(topicData) {
                    var thisTopicKey = topicData.primaryTopic.topic.name.toLowerCase();
                    if($A.util.isEmpty(topicData.metadata) && !$A.util.isEmpty(metadataByTopicName[thisTopicKey])) {
                        yearListNeedsUpdate = true;
                        topicData.metadata = metadataByTopicName[thisTopicKey];
                    }
                });
            });
        }

		if(yearListNeedsUpdate) {
		    component.set('v.yearList', yearList);
        }
  	},
	doInit : function(component, event, helper) {
		var getYearToPrimaryTopicToDataAction = component.get('c.getYearToPrimaryTopicToData');

		getYearToPrimaryTopicToDataAction.setCallback(this, function(response){
            var state = response.getState();
            if (state === 'SUCCESS') {
                var yearToPrimaryTopicToData = response.getReturnValue();
                yearList = [];
                for(var yearKey in yearToPrimaryTopicToData) {
                    yearList.push({year: yearKey, data: yearToPrimaryTopicToData[yearKey]});
                }

                yearList.sort(function(a, b) { return b.year - a.year; } );

            	component.set('v.yearList', yearList);
	        }
        });

        $A.enqueueAction(getYearToPrimaryTopicToDataAction);
	}
})