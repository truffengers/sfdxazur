/**
 * Author: aidan@nebulaconsulting.co.uk
 * Created: 30/10/2017
 * Description: (if required)
 */
({
    doNavigation : function(component) {
      //  console.log('======> doNavigation');
        var recordId = component.get('v.recordId');
      //  console.log('======> recordId: '+recordId);
        var navEvt;
        if(recordId) {
            navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParam("recordId", recordId);
        } else {
            navEvt = $A.get("e.force:navigateToURL");
            navEvt.setParam("url", component.get('v.relativeUrl'));
            console.log('======> url: '+component.get('v.relativeUrl'));
        }
      //  console.log('======> navEvt: ',navEvt);
        navEvt.fire();
    }
})