import { CONSTANTS, STATIC_EXCLUSIONS, STATIC_CLAIMS } from 'c/ix_constants';
import STATIC_DATA_2 from 'c/ix_staticData2';

const STATIC_DATA = {
    ...STATIC_DATA_2,
    [CONSTANTS.LIABILITY]: {
        [CONSTANTS.SUM_INSURED]: 'Employers Liability: £10m; Public Liability: £5m; Reversal of Damages: £5m; Defective Premised Act Liability: £5m; Credit Cards, Forgery and Counterfeiting: £10,000',
        [CONSTANTS.SUBHEADING]: '<p>We will pay damages an insured person is legally obligated to pay for personal injury or property damage caused by an occurrence covered by this policy anywhere in the world, unless stated otherwise or an exclusion applies.</p><p><strong>Defence Cover:</strong> We will pay the legal defence costs and legal expenses incurred by an insured person with our prior written consent. In jurisdictions where we may be prevented from defending an insured person for a covered loss because of local laws or other reasons, we will pay only those legal defence expenses incurred with our prior written consent for the insured person’s defence.</p><p>Our duty to pay the defence costs and legal expenses of any claim or suit arising out of a single occurrence ends when the amount we have paid in damages for that occurrence equals the liability cover limit shown on the schedule of this policy.</p><p>These payments are in addition to the sum insured for damages unless stated otherwise or an exclusion applies.</p>',
        [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>The most we will pay for all claims for personal injury and property damage as a result of any one occurrence is the liability sum insured shown in your schedule.</p><p>This insurance applies separately to each insured person against whom a claim is made or lawsuit is brought, but we will not pay more than the limit shown in your schedule for any single occurrence regardless of the number of insured persons, claims made or persons injured.<br />Payments under Section C. Defence Cover, except any settlement payment, are in addition to the liability sum insured shown in your schedule.</p>',
        [CONSTANTS.LIABILITY_ADDITIONAL_COVERS]: {
            [CONSTANTS.ADDITIONAL_COVERS.CREDIT_CARDS_FORGERY]: {
                [CONSTANTS.SUM_INSURED]: '£10,000',
                [CONSTANTS.SUBHEADING]: '<p>We will pay up to the sum insured shown in your schedule for:</p><ol type="a"><li>any amount you are legally obligated to pay resulting from:</li><li><ol type="i"><li>theft or loss of a bank card or credit card issued in your name that is not held for any trade, business or professional purposes; or</li><li>loss caused by forgery or alteration of any cheque or negotiable document</li></ol></li><li>loss caused by accepting in good faith any counterfeit paper currency.</li></ol><p>These payments are in excess of any other insurance cover in force.</p>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_90]: STATIC_EXCLUSIONS.USA_OR_CANADA_90,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.BUSINESS_OR_INVESTMENT]: STATIC_EXCLUSIONS.BUSINESS_OR_INVESTMENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_RESIDENT]: STATIC_EXCLUSIONS.USA_OR_CANADA_RESIDENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSONS_PROPERTY]: STATIC_EXCLUSIONS.INSURED_PERSONS_PROPERTY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CONTRACT_CAUSED]: STATIC_EXCLUSIONS.CONTRACT_CAUSED,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CRIMINAL_ACT]: STATIC_EXCLUSIONS.CRIMINAL_ACT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSON_INJURY]: STATIC_EXCLUSIONS.INSURED_PERSON_INJURY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COURT_ORDER_PAYMENT]: STATIC_EXCLUSIONS.COURT_ORDER_PAYMENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE]: STATIC_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MALICIOUS_COMPUTER_USE]: STATIC_EXCLUSIONS.MALICIOUS_COMPUTER_USE
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.DEFECTIVE_PREMISES_ACT]: {
                [CONSTANTS.SUM_INSURED]: '£5m',
                [CONSTANTS.SUBHEADING]: '<p>We will cover damages you are legally liable to pay under the Defective Premises Act 1972 or the Defective Premises (Northern Ireland) Order 1975 or the comparable enacting legislation in Scotland, in connection with any home which you have previously owned or occupied provided that at the time of the incident giving rise to liability you had disposed of all legal title to and interest in that home, and no other insurance covers the liability.</p><p>If the policy is terminated pursuant to a sale of the home you will be insured for a period of seven years after the date of termination but the insurance will cover only liability incurred in connection with the home and will not apply if the liability is covered under a more recently effected or current policy.</p>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSONS_PROPERTY]: STATIC_EXCLUSIONS.INSURED_PERSONS_PROPERTY
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.EMPLOYERS_LIABILITY]: {
                [CONSTANTS.SUM_INSURED]: '£10m',
                [CONSTANTS.SUBHEADING]: '<p>We will pay damages you are legally obligated to pay for injury or illness incurred pursuant to the Employers\' Liability (Compulsary Insurance) Act 1969 or the comparable encting legislation in Northern Ireland arising from work that is undertaken during the policy period.</p><p>We may pay defence costs and legal expenses incurred by you with our prior written consent. The most we will pay is the domestic employers liability cover limit in your schedule.</p>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_90]: STATIC_EXCLUSIONS.USA_OR_CANADA_90,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.AIRCRAFT_LIABILITY]: STATIC_EXCLUSIONS.AIRCRAFT_LIABILITY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_RESIDENT]: STATIC_EXCLUSIONS.USA_OR_CANADA_RESIDENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PROPERTY_NOT_IN_SCHEDULE]: STATIC_EXCLUSIONS.PROPERTY_NOT_IN_SCHEDULE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CONTRACT_CAUSED]: STATIC_EXCLUSIONS.CONTRACT_CAUSED,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.DANGEROUS_DOGS]: STATIC_EXCLUSIONS.DANGEROUS_DOGS,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.BOARD_MEMBER]: STATIC_EXCLUSIONS.BOARD_MEMBER,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.DISCRIMINATION]: STATIC_EXCLUSIONS.DISCRIMINATION,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CRIMINAL_ACT]: STATIC_EXCLUSIONS.CRIMINAL_ACT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MOULD]: STATIC_EXCLUSIONS.MOULD,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MOTORISED_LAND_VEHICLES_LIABILITY]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES_LIABILITY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PROFESSIONAL_SERVICES_FAILURE]: STATIC_EXCLUSIONS.PROFESSIONAL_SERVICES_FAILURE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PHYSICAL_OR_MENTAL_ABUSE]: STATIC_EXCLUSIONS.PHYSICAL_OR_MENTAL_ABUSE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.SICKNESS_OR_DISEASE]: STATIC_EXCLUSIONS.SICKNESS_OR_DISEASE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WATERCRAFT]: STATIC_EXCLUSIONS.WATERCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WIND_POWERED_LAND_VEHICLE]: STATIC_EXCLUSIONS.WIND_POWERED_LAND_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WRONGFUL_EMPLOYMENT_TERMINATION]: STATIC_EXCLUSIONS.WRONGFUL_EMPLOYMENT_TERMINATION,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.NON_DOMESTIC_EMPLOYEES]: STATIC_EXCLUSIONS.NON_DOMESTIC_EMPLOYEES,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COURT_ORDER_PAYMENT]: STATIC_EXCLUSIONS.COURT_ORDER_PAYMENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE]: STATIC_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.OTHER_INSURANCE_APPLIES]: STATIC_EXCLUSIONS.OTHER_INSURANCE_APPLIES,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MALICIOUS_COMPUTER_USE]: STATIC_EXCLUSIONS.MALICIOUS_COMPUTER_USE
                }
            },
            [CONSTANTS.ADDITIONAL_COVERS.REVERSAL_OF_DAMAGES]: {
                [CONSTANTS.SUM_INSURED]: '£5m',
                [CONSTANTS.SUBHEADING]: '<p>We will pay up to the liability limit shown in your schedule for damages and assessed costs which have been awarded to you but which have not been paid within 3 months of the date of the award for:</p><ol type="a"><li>accidental or bodily injury; or</li><li>accidental loss or damage to property</li></ol><p>Provided that:</p><ol type="a"><li>the damages awarded were not in resepct of an incident arising out of your profession, occupation or business;</li><li>you would have been covered by this policy if you were in the position of the person you are claiming damages against; and</li><li>there is no appeal in progress</li></ol>',
                [CONSTANTS.EXCLUSIONS]: {
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_90]: STATIC_EXCLUSIONS.USA_OR_CANADA_90,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.AIRCRAFT_LIABILITY]: STATIC_EXCLUSIONS.AIRCRAFT_LIABILITY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.BUSINESS_OR_INVESTMENT]: STATIC_EXCLUSIONS.BUSINESS_OR_INVESTMENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_RESIDENT]: STATIC_EXCLUSIONS.USA_OR_CANADA_RESIDENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PROPERTY_NOT_IN_SCHEDULE]: STATIC_EXCLUSIONS.PROPERTY_NOT_IN_SCHEDULE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSONS_PROPERTY]: STATIC_EXCLUSIONS.INSURED_PERSONS_PROPERTY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CONTRACT_CAUSED]: STATIC_EXCLUSIONS.CONTRACT_CAUSED,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.DANGEROUS_DOGS]: STATIC_EXCLUSIONS.DANGEROUS_DOGS,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.BOARD_MEMBER]: STATIC_EXCLUSIONS.BOARD_MEMBER,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.DISCRIMINATION]: STATIC_EXCLUSIONS.DISCRIMINATION,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.HOME_AS_FINANCIAL_GUARANTEE]: STATIC_EXCLUSIONS.HOME_AS_FINANCIAL_GUARANTEE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CRIMINAL_ACT]: STATIC_EXCLUSIONS.CRIMINAL_ACT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSON_INJURY]: STATIC_EXCLUSIONS.INSURED_PERSON_INJURY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MOULD]: STATIC_EXCLUSIONS.MOULD,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MOTORISED_LAND_VEHICLES_LIABILITY]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES_LIABILITY,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PROFESSIONAL_SERVICES_FAILURE]: STATIC_EXCLUSIONS.PROFESSIONAL_SERVICES_FAILURE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PHYSICAL_OR_MENTAL_ABUSE]: STATIC_EXCLUSIONS.PHYSICAL_OR_MENTAL_ABUSE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.SICKNESS_OR_DISEASE]: STATIC_EXCLUSIONS.SICKNESS_OR_DISEASE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WATERCRAFT]: STATIC_EXCLUSIONS.WATERCRAFT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WIND_POWERED_LAND_VEHICLE]: STATIC_EXCLUSIONS.WIND_POWERED_LAND_VEHICLE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WRONGFUL_EMPLOYMENT_TERMINATION]: STATIC_EXCLUSIONS.WRONGFUL_EMPLOYMENT_TERMINATION,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.NON_DOMESTIC_EMPLOYEES]: STATIC_EXCLUSIONS.NON_DOMESTIC_EMPLOYEES,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COURT_ORDER_PAYMENT]: STATIC_EXCLUSIONS.COURT_ORDER_PAYMENT,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE]: STATIC_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.OTHER_INSURANCE_APPLIES]: STATIC_EXCLUSIONS.OTHER_INSURANCE_APPLIES,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
                    [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MALICIOUS_COMPUTER_USE]: STATIC_EXCLUSIONS.MALICIOUS_COMPUTER_USE,
                }
            }
        },
        [CONSTANTS.ALL_LIABILITY_EXCLUSIONS]: {
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_90]: STATIC_EXCLUSIONS.USA_OR_CANADA_90,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.AIRCRAFT_LIABILITY]: STATIC_EXCLUSIONS.AIRCRAFT_LIABILITY,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.BUSINESS_OR_INVESTMENT]: STATIC_EXCLUSIONS.BUSINESS_OR_INVESTMENT,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.USA_OR_CANADA_RESIDENT]: STATIC_EXCLUSIONS.USA_OR_CANADA_RESIDENT,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PROPERTY_NOT_IN_SCHEDULE]: STATIC_EXCLUSIONS.PROPERTY_NOT_IN_SCHEDULE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSONS_PROPERTY]: STATIC_EXCLUSIONS.INSURED_PERSONS_PROPERTY,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CONTRACT_CAUSED]: STATIC_EXCLUSIONS.CONTRACT_CAUSED,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.DANGEROUS_DOGS]: STATIC_EXCLUSIONS.DANGEROUS_DOGS,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.BOARD_MEMBER]: STATIC_EXCLUSIONS.BOARD_MEMBER,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.DISCRIMINATION]: STATIC_EXCLUSIONS.DISCRIMINATION,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.HOME_AS_FINANCIAL_GUARANTEE]: STATIC_EXCLUSIONS.HOME_AS_FINANCIAL_GUARANTEE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.CRIMINAL_ACT]: STATIC_EXCLUSIONS.CRIMINAL_ACT,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.INSURED_PERSON_INJURY]: STATIC_EXCLUSIONS.INSURED_PERSON_INJURY,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MOULD]: STATIC_EXCLUSIONS.MOULD,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MOTORISED_LAND_VEHICLES_LIABILITY]: STATIC_EXCLUSIONS.MOTORISED_LAND_VEHICLES_LIABILITY,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PROFESSIONAL_SERVICES_FAILURE]: STATIC_EXCLUSIONS.PROFESSIONAL_SERVICES_FAILURE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.PHYSICAL_OR_MENTAL_ABUSE]: STATIC_EXCLUSIONS.PHYSICAL_OR_MENTAL_ABUSE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.SICKNESS_OR_DISEASE]: STATIC_EXCLUSIONS.SICKNESS_OR_DISEASE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WATERCRAFT]: STATIC_EXCLUSIONS.WATERCRAFT,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WIND_POWERED_LAND_VEHICLE]: STATIC_EXCLUSIONS.WIND_POWERED_LAND_VEHICLE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.WRONGFUL_EMPLOYMENT_TERMINATION]: STATIC_EXCLUSIONS.WRONGFUL_EMPLOYMENT_TERMINATION,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.NON_DOMESTIC_EMPLOYEES]: STATIC_EXCLUSIONS.NON_DOMESTIC_EMPLOYEES,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COURT_ORDER_PAYMENT]: STATIC_EXCLUSIONS.COURT_ORDER_PAYMENT,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE]: STATIC_EXCLUSIONS.COMPUTER_VIRUS_DAMAGE,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.OTHER_INSURANCE_APPLIES]: STATIC_EXCLUSIONS.OTHER_INSURANCE_APPLIES,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.COVERED_UNDER_PART_VI]: STATIC_EXCLUSIONS.COVERED_UNDER_PART_VI,
            [CONSTANTS.EXCLUSION_NAMES.LIABILITY_EXCLUSIONS.MALICIOUS_COMPUTER_USE]: STATIC_EXCLUSIONS.MALICIOUS_COMPUTER_USE
        }
    },
    'Legal Expenses': {
        [CONSTANTS.SUM_INSURED]: '£100,000',
        [CONSTANTS.SUBHEADING]: '<p>Following an Insured event the insurer will pay the insured’s legal costs & expenses up to £100,000 (including the cost of appeals) for all claims related by time or originating cause, subject to all of the following requirements being met:</p><ol><li>The insured keeps to the terms of this section and co-operates fully with us;</li><li>The Insured event happens within the territorial limit;</li><li><p>The claim</p><ol><li>always has reasonable prospects of success;</li><li>is reported to us during the policy period; and as soon as the insured first becomes aware of circumstances which should give rise to a claim;</li></ol></li><li><p>Unless there is a conflict of interest the insured always agrees to use the appointed advisor chosen by us in any claim</p><ol><li>to be heard by the small claims court and/or</li><li>before proceedings have been or need to be issued;</li></ol></li><li>Any dispute will be dealt with through mediation or by a court, tribunal, Advisory Conciliation and Arbitration Service or a relevant regulatory body agreed with us.</li></ol>',
        [CONSTANTS.LEGAL_EXPENSES_ADDITIONAL_COVERS]: {
            [CONSTANTS.ADDITIONAL_COVERS.EMPLOYMENT]: {
                [CONSTANTS.SUBHEADING]: '<p>You are covered for:</p><p>A dispute with the insured’s current, former or  prospective employer relating to their contract of employment or related  legal rights. A claim can be brought once all internal dismissal, disciplinary  and grievance procedures as set out in the</p><ol><li>ACAS Code of Practice  for Disciplinary and Grievance  Procedures, or</li><li>Labour Relations Agency Code of Practice on Disciplinary and Grievance Procedures in Northern Ireland</li></ol><p>have been or ought to have been concluded.</p><p>The insured is required to co-operate fully with ACAS regarding mediation and not do anything that hinders a successful outcome.</p>',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>a dispute arising solely from personal injury;</li><li>defending the insured other than defending an appeal;</li><li>legal costs & expenses for an employer’s internal disciplinary process or an employee’s grievance hearing or appeal;</li><li>an insured’s employer or ex-employer’s pension scheme;</li><li>a compromise or settlement agreement between the insured and their employer unless such agreement arises from an ongoing claim under this section.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.DOMESTIC_EMPLOYEES_DISPUTES]: {
                [CONSTANTS.SUBHEADING]: '<p>A dispute with your domestic employee that arises from:</p><ul><li>their dismissal by you;</li><li>the terms of a contract of service occupancy agreement between you and your domestic employer;</li><li>an alleged breach of your domestic employee\'s legal rights under employment laws.</li></ul>',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>disciplinary hearings or internal grivance procedures;</li><li>personal injury;</li><li>you pursuing a claim against your domestic employee other than a claim to recover possession of a part of your home or other accommodation provided by you under a service occupancy agreement.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.CONTRACT]: {
                [CONSTANTS.SUBHEADING]: '<p>A dispute arising out of an agreement or alleged agreement which has been entered into by the insured for</p><ul><li>buying or hiring consumer goods or services;</li><li>privately selling goods;</li><li>buying or selling your home, your let property and other residences owned and occupiedby you from time to time;</li><li>renting your main home as a tenant;</li><li>the occupation of your home under a lease.</li></ul>',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>a dispute with a tenant or leasee where the insured is the landlord or lessor;</li><li>loans, mortgages, pensions, or any other banking, life or long-term insurance products, savings or investments;</li><li>the insured\'s business activities, trade, venture for gain, profession or employment;</li><li>a contract involving a motor vehicle;</li><li>a settlement due under an insurance policy;</li><li>construction work, or designing, converting or extending any building where the contract value exceeds £6,000 including VAT.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.PROPERTY]: {
                [CONSTANTS.SUBHEADING]: '<p>A dispute relating to visible property which the insured owns following</p><ul><li>an event which causes physical damage to the insured’s property, including your home, your let property and other residences owned and occupied by you from time to time provided that for a claim against your tenant you have prepared, before to the granting of the tenancy, a detailed inventory of the contents and condition of the let property which the tenant has signed;</li><li>a public or private nuisance or trespass provided that where any boundary is in dispute, you have proof of where the boundary lies.</li></ul>',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for:</p><ol><li>The first £250 of any claim arising from a public or private nuisance or trespass. This is payable by the insured as soon as we accept the claim.</li><li><p>Any claim arising from or related to:</p><ol><li>a contract entered into by an insured other than a tenancy agreement;</li><li>any building or land other than your home, your let property and other residences owned and occupied by you from time to time;</li><li>a motor vehicle;</li><li>the compulsory purchase of, or demolition, restrictions, controls or permissions placed on your property by any government, local or public authority;</li><li>defending any dispute arising from property damage other than defending a counter claim or an appeal;</li><li>a dispute with any party other than the person(s) who caused the damage, nuisance or trespass.</li></ol></li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.PERSONAL_INJURY]: {
                [CONSTANTS.SUBHEADING]: 'A sudden event directly causing the insured physical bodily injury or death.',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>a condition, illness or disease which develops gradually over time;</li><li>mental injury, nervous shock, depression or psychological symptoms where the insured has not sustained physical injury to their body;</li><li>defending any dispute other than an appeal.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.CLINICAL_NEGLIGENCE]: {
                [CONSTANTS.SUBHEADING]: 'A dispute arising from alleged clinical negligence or malpractice.',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for</p><ol><li>Any claim arising from or relating to a contract dispute;</li><li>Defending any dispute other than an appeal.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.TAX]: {
                [CONSTANTS.SUBHEADING]: 'A formal enquiry into the insured’s personal tax affairs provided that all returns are complete and have been submitted within the legal timescales permitted.',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>tax returns where HMRC Revenue & Customs impose a penalty or which contain careless and/or negligent misstatements;</li><li>a business venture for gain of the insured;</li><li>where the Disclosure of Tax Avoidance Scheme Regulations apply or should apply to the insured’s financial arrangements;</li><li>any enquiry that concerns assets,monies or wealth outside of Great Britain and Northern Ireland;</li><li>an investigation by the Fraud Investigation Service of HM Revenue & Customs.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.LEGAL_DEFENCE]: {
                [CONSTANTS.SUBHEADING]: '<ol><li><p><strong>Work</strong></p><p>An alleged act or omission of the insured that arises from their work as an employee and results in:</p><ol><li>the insured being interviewed by the police or others with the power to prosecute;</li><li>a prosecution being brought against the insured in a court of criminal jurisdiction;</li><li>civil proceedings being brought against the insured under unfair discrimination laws.</li></ol></li><li><p><strong>Motor</strong></p><p>A motoring prosecution being brought against the insured.</p><li><p><strong>Other</strong></p><p>A formal investigation or disciplinary hearing being brought against the insured by a professional or regulatory body.</p></li></ol>',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>owning a vehicle or driving without motor insurance or driving without a valid driving licence;</li><li>a parking offence.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.LOSS_OF_EARNINGS]: {
                [CONSTANTS.SUBHEADING]: 'The insured\'s absence from work to attend court, tribunal, arbitration or regulatory proceedings at the request of the appointed advisor or whilst on jury service which results in loss of earnings.',
                [CONSTANTS.EXCLUSIONS]: '<p>You are not covered for any claim arising from or relating to:</p><ol><li>Loss of earnings in excess of £1,000;</li><li>Any sum which can be recovered from the court or tribunal.</li></ol>'
            },
            [CONSTANTS.ADDITIONAL_COVERS.IDENTITY_THEFT]: {
                [CONSTANTS.SUBHEADING]: '<p>A dispute arising from the use of the insured’s personal information without their permission to commit fraud or other crimes provided the insured contacts our Identity Theft Advice and Resolution Service as soon as they suspect that their identity may have been stolen.</p><p>The insured must settle communication costs in the first instance and make a receipted claim to us for reimbursement.</p>',
                [CONSTANTS.EXCLUSIONS]: 'The insurer will not pay for any money claimed, goods, loans, or other property or financial loss or other benefit obtained as a result of the identity theft.'
            }
        },
        [CONSTANTS.EXCLUSIONS]: '<p>The insured is not covered for any claim arising from or relating to:</p><ol><li>legal costs & expenses</li><li>any actual or alleged act or omission or dispute happening before, or existing at the start of cover under this section and which the insured believed or ought reasonably to have believed could lead to a claim under this section;</li><li>an amount below £100;</li><li><p>an allegation against the insured involving:</p><ol><li>assault, violence or dishonesty, malicious falsehood or defamation;</li><li>the manufacture, dealing in or use of alcohol, illegal drugs, indecent or obscene materials;</li><li>illegal immigration;</li><li>offences under Part 7 of the Proceeds of Crime Act 2002 (money laundering offences);</li></ol></li><li>a dispute between your family members</li><li>an insured\'s deliberate or reckless act;</li><li>a judicial review;</li><li>a dispute arising from or relating to clinical negligence except as provided for in Insured event Clinical Negligence;</li><li>a dispute with us not dealt with under Condition 6, or the insurer or the company that sold this policy;</li><li>a group litigation order</li><li>the payment of fines, penalties or compensation awarded against the insured;</li><li>a let property which is or should habe been registered as a House of Multiple Occupation</li></ol>',
        [CONSTANTS.CONDITIONS]: {
            [CONSTANTS.SUBHEADING]: 'Where the insurer’s risk is affected by the insured’s failure to keep to these conditions the insurer can cancel this section, refuse a claim or withdraw from an ongoing claim. The insurer also reserves the right to recover legal costs & expenses from the insured if this happens.',
            'The Insured\'s Responsibilities': '<p>An Insured Must:</p><ol><li>tell us immediately of anything that may make it more costly or difficult for the appointed advisor to resolve the claim in the insured’s favour;</li><li>cooperate fully with us, give the appointed advisor any instructions required, and keep them updated with progress of the claim and not hinder them;</li><li>take reasonable steps to claim back legal costs & expenses, employment tribunal and employment appeal tribunal fees and, where recovered, pay them to the insurer;</li><li>keep legal costs & expenses as low as possible;</li><li>allow the insurer at any time to take over and conduct in the insured’s name, any claim.</li></ol>',
            'Freedom to Choose Appointed Advisor': '<ol><li>In certain circumstances as set out below the insured may choose an appointed advisor. In all other cases no such right exists and we shall choose the appointed advisor.</li><li><p>The insured may choose an appointed advisor if:</p><ol><li>we agree to start proceedings or proceedings are issued against an insured, or</li><li>there is a conflict of interest</li></ol>except where the insured’s claim is to be dealt with by the small claims court where we shall choose the appointed advisor. </li><li>Where the insured wishes to exercise the right to choose, the insured must write to us with their preferred representative’s contact details. Where the insured chooses to use their preferred representative, the insurer will not pay more than we agree to pay a solicitor from our panel. (Our panel solicitor firms are chosen with care and we agree special terms with them which may be less than the rates available from other firms.)</li><li>If the insured dismisses the appointed advisor without good reason, or withdraws from the claim without our written agreement, or if the appointed advisor refuses with good reason to continue acting for an insured, cover will end immediately.</li><li>In respect of a claim under Insured event Employment, Contract, Personal Injury or Clinical Negligence, the insured must enter into a conditional fee agreement or the appointed advisor must enter into a collective conditional fee agreement, where legally permitted.</li></ol>',
            'Consent': '<ol><li>The insured must agree to us having sight of the appointed advisor’s file relating to the insured’s claim. The insured is considered to have provided consent to us or our appointed agent to have sight of their file for auditing and quality and cost control purposes.</li><li>An insured must have your agreement to claim under this section.</li></ol>',
            'Settlement': '<ol><li>The insurer has the right to settle the claim by paying the reasonable value of the insured’s claim.</li><li>The insured must not negotiate, settle the claim or agree to pay legal costs & expenses without our written agreement.</li><li>If the insured refuses to settle the claim following advice to do so from the sappointed advisor, the insurer reserves the right to refuse to pay further legal costs & expenses. </li></ol>',
            'Barrister\'s Opinion': '<p>We may require the insured to obtain and pay for an opinion from a barrister if a dispute arises regarding the merits or value of the claim. If the opinion supports the insured, then the insurer will reimburse the reasonable costs of that opinion. If that opinion conflicts with advice obtained by us, then the insurer will pay for a final opinion which shall be binding on the insured and us. This does not affect the insured’s right under the next condition below.</p>',
            'Disputes': '<p>If any dispute between the insured and us arises from this section, the insured can make a complaint to us as described in your schedule and we will try to resolve the matter. If we are unable to satisfy the insured’s concerns and the matter can be dealt with by the Financial Ombudsman Service the insured can ask them to arbitrate over the complaint.</p>',
            'Fraudulent Claims and Claims Tainted by Dishonesty': '<ol><li>If an insured makes any claim which is fraudulent or false, the cover under this section shall become void and all benefit under it will be lost.</li><li><p>An insured shall at all times be entirely truthful and open in any evidence, disclosure or statement they give and shall act with complete honesty and integrity at all times. Where, on the balance of probabilities and having considered carefully all the facts of the claim, it appears that the insured has breached this condition and that the breach has:</p><ul><li>affected our assessment of reasonable prospects of success, and/or</li><li>prejudiced in any part the outcome of the insured\'s claim</li></ul>the insurer shall have no liability for legal costs & expenses under this section.</li></ol>'
        },
        [CONSTANTS.LEGAL_AND_TAX_HELPLINE]: {
            [CONSTANTS.SUBHEADING]: '<p>You can get advice by calling 0344 571 7976</p><p>All helplines are subject to fair and reasonable use. The level of fair usage will depend on individual circumstances. However, if our advisors consider that your helpline usage is becoming excessive they will tell you. If following that warning usage is not reduced to a more reasonable level, we can refuse to accept further calls.</p><p>If you have a legal or tax problem we recommend that you call our confidential legal and tax advice helpline. Legal advice is available 24 hours a day, 7 days a week, and tax advice is available between 9am and 5pm on weekdays (except bank holidays).</p><p>The advice covers personal legal matters within EU law or personal tax matters or personal tax matters within the UK.</p><p>Your query will be dealt with by a qualified specialist who is experienced in handling legal and tax-related matters. Use of this service does not constitute reporting of a claim.</p>'
        }
    },
    'Home Emergency': {
        [CONSTANTS.SUM_INSURED]: '£1,000',
        [CONSTANTS.SUBHEADING]: '<p>Following an insured event which results in a home emergency the insurer will pay the emergency costs provided that all of the following requirements are met:</p><ol><li>The claim is reported to us during the policy period and as soon as possible after you first become aware of the emrgency.</li><li>Your home is located in the United Kingdom</li><li>You always agree to use the contractor chosen by us.</li></ol>',
        [CONSTANTS.HOME_EMERGENCY_COVERS.MAIN_HEATING_SYSTEM]: {
            [CONSTANTS.SUBHEADING]: 'The total failure or complete breakdown, whether or not caused by accidental damage, of the main heating system (including central heating boiler, all radiators, hot water pipes and water storage tanks) in your home.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.PLUMBING_AND_DRAINING]: {
            [CONSTANTS.SUBHEADING]: 'The sudden damage to, or blockage or breakage or flooding of, the drains or plumbing system including water storage tanks, taps and pipework located within your home, which results in a home emergency.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.HOME_SECURITY]: {
            [CONSTANTS.SUBHEADING]: 'Damage (whether or not accidental) or the failure of external doors, windows or locks, which compromises the security of your home.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.TOILET_UNIT]: {
            [CONSTANTS.SUBHEADING]: 'Breakage or mechanical failure of the toilet bowl or cistern resulting in the loss of function of a toilet in your home.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.DOMESTIC_POWER_SUPPLY]: {
            [CONSTANTS.SUBHEADING]: 'The failure, whether or not caused accidentally, of your home\'s domestic electricity or gas supply.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.LOST_KEYS]: {
            [CONSTANTS.SUBHEADING]: 'The loss or theft of the only available keys, if you cannot replace them to gain access to your home.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.VERMIN_INFESTATION]: {
            [CONSTANTS.SUBHEADING]: 'Vermin causing damage inside your home or a health risk to you.'
        },
        [CONSTANTS.HOME_EMERGENCY_COVERS.ALTERNATIVE_ACCOMMODATION]: {
            [CONSTANTS.SUBHEADING]: 'Your overnight accommodation costs including transport to such accommodation following a home emrgency which makes your home unsafe, unsecure or uncomfortable to stay in overnight.'
        },
        [CONSTANTS.ALL_HOME_EMERGENCY_EXCLUSIONS]: {
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.PRE_CLAIM_COSTS]: STATIC_EXCLUSIONS.PRE_CLAIM_COSTS,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.WITHIN_48HRS_OF_COVER]: STATIC_EXCLUSIONS.WITHIN_48HRS_OF_COVER,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.NOBODY_TO_MEET_CONTRACTOR]: STATIC_EXCLUSIONS.NOBODY_TO_MEET_CONTRACTOR,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.PRIOR_TO_COVER_MATTERS]: STATIC_EXCLUSIONS.PRIOR_TO_COVER_MATTERS,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.BAD_WORKMANSHIP]: STATIC_EXCLUSIONS.BAD_WORKMANSHIP,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.OLD_HEATING_SYSTEM]: STATIC_EXCLUSIONS.OLD_HEATING_SYSTEM,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.WARM_AIR]: STATIC_EXCLUSIONS.WARM_AIR,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.PERMANENT_REPAIR]: STATIC_EXCLUSIONS.PERMANENT_REPAIR,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.AFTER_EMERGENCY_RESOLVED]: STATIC_EXCLUSIONS.AFTER_EMERGENCY_RESOLVED,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.REPLACEMENT_OF_PARTS]: STATIC_EXCLUSIONS.REPLACEMENT_OF_PARTS,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.GARAGE_OUTBUILDINGS]: STATIC_EXCLUSIONS.GARAGE_OUTBUILDINGS,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.UNOCCUPIED_60]: STATIC_EXCLUSIONS.UNOCCUPIED_60,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.COVERED_UNDER_WARRANTY]: STATIC_EXCLUSIONS.COVERED_UNDER_WARRANTY,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.DESIGN_OR_INSTALLATION_FAULTS]: STATIC_EXCLUSIONS.DESIGN_OR_INSTALLATION_FAULTS,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.COVERED_UNDER_ANOTHER_POLICY]: STATIC_EXCLUSIONS.COVERED_UNDER_ANOTHER_POLICY,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.SUBSIDENCE]: STATIC_EXCLUSIONS.SUBSIDENCE,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.LET_PROPERTY]: STATIC_EXCLUSIONS.LET_PROPERTY,
            [CONSTANTS.EXCLUSION_NAMES.HOME_EMERGENCY_EXCLUSIONS.FROZEN_WASTE]: STATIC_EXCLUSIONS.FROZEN_WASTE
        },
        [CONSTANTS.CONDITIONS]: {
            'Your Responsibilities': '<p>You must:</p><br /><ol type="a"><li>not to do anything that hinders us or the contractor;</li><li>tell us immediately after first becoming aware of any home emergency;</li><li>tell us immediately of anything that may materially alter our assessment of the claim</li><li>cooperate fully with the contractor;</li><li>provide us with everything we need to help us handle the claim;</li><li>take reasonable steps to recover emergency costs that the insurer pays and pay to the insurer all costs that are recovered should these be paid to you;</li><li>minimise any emergency costs and try to prevent anything happening that may cause a claim;</li><li>allow the insurer at any time to take over and conduct in your name any claims, proceedings or investigation;</li><li>be able to prove that the central heating boiler has been serviced within 12 months prior to a home emergency claim.</li></ol>',
            'Our Consent': 'We must give you our consent to incur emergency costs. The insurer does not accept liability for emergency costs incurred without our consent.',
            'Settlement': 'You must not settle the contractor\'s invoice or agree to pay emergency costs that you wish to claim for under this section without our agreement.',
            'Disputes': 'If any dispute between you and us arises from this section, you can make a complaint to us as described in your scheduld and we will try to resolve the matter. If we are unable to satisfy your concerns you can satisfy the Financial Ombudsman Service to arbitrate over the complaint.',
            'Fraudulent Claims': 'If you make any claim under this policy which is fraudulent or false, this section of the policy shall become void and all benefit under it will be lost.'
        }
    },
    [CONSTANTS.PERSONAL_CYBER]: {
        [CONSTANTS.CYBER_HOME_SYSTEMS_DAMAGE]: {
            [CONSTANTS.SUM_INSURED]: '£50,000',
            [CONSTANTS.SUBHEADING]: '<p><p>Cover is only operative if indicated on your policy schedule.</p><p>We will pay for the following arising as a result of a cyber event you discover during the policy period:</p></p><ol><li><p><strong>Home Systems Restoration</strong></p><p>The cost of investigating, reconfiguring and rectifying any damage to your home systems, and restoring data (but not the cost to recreate data if you cannot restore it from other sources).</p><p>This does not include the value of data to you, even if the data cannot be restored.</p></li><li><p><strong>Computer Virus Removal</strong></p><p>The cost of locating and removing a computer virus from your home systems</p></li><li><p><strong>Professional Assistance</strong></p><p>The cost of hiring professional consultants to make recommendations on how to prevent your home systems from being infected by computer virus or to prevent hacking.</p></li></ol>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>The most we will pay for all claims we accept under this policy in total for the policy period is the sum insured, regardless of the number of claims.</p><p>If there is more than one person named in the schedule, the total amount we will pay following a claim will not exceed the amount we would be liable to pay to any one of you.</p><p><p><strong>Defence costs:</strong></p><p>Any defence costs we pay will be included within,not in addition to, the sum insured.</p></p><p><p><strong>Paying out the sum insured:</strong></p><p>For any and all claims arising for the policy period we may pay the full sum insured that applies.</p><p>When we have paid the full sum insured, we will not pay any further amounts for any claims or for associated defence costs.</p></p>'
        },
        [CONSTANTS.CYBER_CRIME]: {
            [CONSTANTS.SUM_INSURED]: '£50,000',
            [CONSTANTS.SUBHEADING]: '<p>We will pay for the following which you discover during the policy period:</p><ol><li><p><strong>Fraud</strong></p><p>Your financial loss as the result of a fraudulent communication or input, destruction or modification of data in your home systems which results in:</p><ul><li>money being taken from any account;</li><li>goods, services, propertyor financial benefitbeing transferred; or</li><li>any credit arrangement being made;</li></ul><p>as long as you have not received any benefit in return. We will also pay the cost of proving that transactions are fraudulent and that contracts or agreements were entered into fraudulently.</p></li><li><p><strong>Telephone hacking</strong></p><p>Your liability to make any payment to your telephone service provider as the result of hacking into your home systems.</p></li><li><p><strong>Cyber Ransom</strong></p><p>The cost of responding, and with our written agreement the payment of a ransom demand, if anyone has or threatens to:</p><ul><li>disrupt your home systems by introducing a computer virus, or to initiate a hacking attack or denial of service attack against you;</li><li>release, publish, corrupt,delete or alter your data if this would cause you harm or damage your reputation;</li></ul><p>as long as you can demonstrate that you have reasonable grounds to believe that the threat is not a hoax, and you have reported it to the police.</p></li><li><p><strong>Identity Theft Assistance</strong></p><p>The cost of identity theft assistance, and monitoring your credit records, to help you to correct your credit records and to take back control of your identity following the fraudulent use of your personal data.</p></li></ol>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>The most we will pay for all claims we accept under this policy in total for the policy period is the sum insured, regardless of the number of claims.</p><p>If there is more than one person named in the schedule, the total amount we will pay following a claim will not exceed the amount we would be liable to pay to any one of you.</p><p><p><strong>Defence costs:</strong></p><p>Any defence costs we pay will be included within, not in addition to, the sum insured.</p></p><p><p><strong>Paying out the sum insured:</strong></p><p>For any and all claims arising for the policy period we may pay the full sum insured that applies.</p><p>When we have paid the full sum insured, we will not pay any further amounts for any claims or for associated defence costs.</p></p>'
        },
        [CONSTANTS.CYBER_ONLINE_LIABILITY]: {
            [CONSTANTS.SUM_INSURED]: '£50,000',
            [CONSTANTS.SUBHEADING]: '<p>We will pay damages and defence costs arising from a claim first made against you by a third party during the policy period as the result of:</p><ol><li><p><strong>Data Privacy</strong></p><p>You failing to secure, or prevent unauthorised access to, publication of or use of data (including any inadvertent interference with any right to privacy or publicity or breach of confidence);</p></li><li><p><strong>Computer Virus Transmission</strong></p><p>You unintentionally transmitting, or failing to prevent or restrict the transmission of, a computer virus, hacking attack or denial of service attack from your home systems to a third party; or</p></li><li><p><strong>Defamation and Disparagement</strong></p><p>Loss of reputation (including that of a product) or intellectual property rights being breached as a result of your activities online.</p></li></ol>',
            [CONSTANTS.WHEN_PAYING_A_LOSS]: '<p>The most we will pay for all claims we accept under this policy in total for the policy period is the sum insured, regardless of the number of claims.</p><p>If there is more than one person named in the schedule, the total amount we will pay following a claim will not exceed the amount we would be liable to pay to any one of you.</p><p><p><strong>Defence costs:</strong></p><p>Any defence costs we pay will be included within, not in addition to, the sum insured.</p></p><p><p><strong>Paying out the sum insured:</strong></p><p>For any and all claims arising for the policy period we may pay the full sum insured that applies.</p><p>When we have paid the full sum insured, we will not pay any further amounts for any claims or for associated defence costs.</p></p>'
        },
        [CONSTANTS.CONDITIONS]: {
            'Reporting a Claim': '<p>As soon as you know about any incident or circumstance that may result in a claim against you or a claim under your policy you must:</p><ul><li>Not admit responsibility or liability, or agree to pay any money or provide any services on our behalf, without our written permission;</li><li>Take all reasonable steps and precautions to prevent further damage or other loss covered by your policy;</li><li>Immediately tell the police about any loss or damage relating to crime and get a crime reference number;</li><li>Tell the person who arranged your policy (or us), providing full details, as soon after the incident or circumstances as possible;</li><li>Tell the person who arranged your policy (or us), providing full details, within 14 days in the case of you knowing about an incident or circumstance that has resulted in or may result in you receiving a claim against you;</li><li>Immediately send us every letter, writ, summons or other document you receive in connection with the claim or circumstance, and record all information relating to a claim against you covered under Section 3 –‘Cyber Online Liability’;</li><li>Keep any damaged home systems and other evidence, and allow us to inspect it;</li><li>Co-operate with us fully and provide all the information we need to investigate your claim or circumstance;</li><li>Give us details of any other insurances you may have which may cover loss covered by this policy;</li><li>Attempt to recover financial loss relating to your claim under Section 2 – Cyber Crime from a bank or other financial institution that may be responsible for refunding all or part of the loss; and</li><li>Tell us if you recover money from a third party in relation to a claim (you may need to give the money to us).</li></ul>',
            'Fraudulent Claims': 'If you (or anyone acting for you) make a claim knowing that any part of the claim is fraudulent (dishonest, false or exaggerated), we will not pay the claim and we may cancel your policy from the time of the fraudulent act relating to your claim and we may keep any premium you have paid (see ‘Part II – General conditions which apply to this insurance policy’).',
            'Enforcing your rights': 'We may, at our expense, take all necessary steps to enforce your rights against any third party. We can do this before or after we pay a claim. You must not do anything before or after we pay your claim to affect our rights and you must give us any help and information we ask for. You must take reasonable steps to make sure that you enforce your rights.',
            'Protecting data': 'You must make sure that you take precautions for disposing of and destroying home systems in order to protect data.',
            'Controlling defence': 'We can, but do not have to, take control of investigating, settling or defending any claim made against you. We would take this action in your name. If necessary, we will appoint an adjuster, solicitor or any other appropriate person to deal with the claim. We may appoint your solicitor, but only on a fee basis similar to that of our own solicitor, and only for work done with our permission in writing. We will only defend claims if we think that there is a reasonable chance of being successful, and after taking the costs of the defence into account.',
            'Other Insurances': 'If there is any other insurance covering your claim, we will only pay our share, even if the other insurer refuses to pay the claim.',
            'Change in Risk': '<p>As soon as you (or anyone acting for you) become aware of any change in circumstance which may affect the policy, you must tell us of the change. We will decide if the change alters the risk and if we need to change the terms and conditions of your policy including the premium. This applies to any change which arises, whether it is before or during the policy period, including before we renew your policy.</p><p>If you do not tell us about a change, a claim may be reduced or rejected and in some cases your policy might be treated as if it never existed and you may not be entitled to a refund of premium.</p><p>Your cover will not be affected by any change in circumstance which increases the risk covered by your policy and which you could not have known about.</p>',
            'Defence software': 'Your home systems must be protected by anti-virus software, where available, which is updated regularly in accordance with the provider’s recommendations.',
            'Contracts Act 1999': 'Any person who is not named in the schedule has no right under the Contracts (Rights of Third-Parties) Act 1999 (or any other law) to enforce any term of the policy.',
            'Correct information': '<p>You must give us correct information. If you fail to do so and:</p><ul><li>your failure was deliberate or reckless, we will treat your policy as if it never existed, refuse all claims and keep any premium you have paid. If your failure occurs during a change to your policy we will terminate your policy from the date of that change, refuse subsequent claims and keep any premium you have paid.</li><li>your failure was careless and we would not have issued your policy had you told us the correct information, we will treat your policy as if it never existed and return any premium you have paid. If your failure occurs during a change to your policy we will treat your policy as though the change was not made and where appropriate return any additional premium charged for the change. You may want to cancel your policy if it does not meet your needs (see ‘Part II – General conditions which apply to this insurance policy’).</li><li><p>your failure was careless and we would have insured you on different terms had you given us correct information, we will;</p><ol><li>alter the terms of your policy to those we would have imposed (other than those relating to premium); and</li><li>reduce the amount paid or payable on any claim in proportion to the amount of additional premium we would have charged.</li></ol></li></ul>'
        },
        [CONSTANTS.ALL_PERSONAL_CYBER_EXCLUSIONS]: {
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.ADVANCE_FEE_FRAUD]: STATIC_EXCLUSIONS.ADVANCE_FEE_FRAUD,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.BUSINESS_ACTIVITIES]: STATIC_EXCLUSIONS.BUSINESS_ACTIVITIES,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.CIRCUMSTANCES_BEFORE_POLICY]: STATIC_EXCLUSIONS.CIRCUMSTANCES_BEFORE_POLICY,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.CONFISCATION]: STATIC_EXCLUSIONS.CONFISCATION,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.EXTERNAL_NETWORK_FAILURE]: STATIC_EXCLUSIONS.EXTERNAL_NETWORK_FAILURE,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.INTELLECTUAL_PROPERTY_RIGHTS]: STATIC_EXCLUSIONS.INTELLECTUAL_PROPERTY_RIGHTS,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.MALICIOUS_DEFAMATION]: STATIC_EXCLUSIONS.MALICIOUS_DEFAMATION,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.OTHER_INSURED_PARTIES]: STATIC_EXCLUSIONS.OTHER_INSURED_PARTIES,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.WEAR_AND_TEAR_CYBER]: STATIC_EXCLUSIONS.WEAR_AND_TEAR_CYBER,
            [CONSTANTS.EXCLUSION_NAMES.PERSONAL_CYBER_EXCLUSIONS.PATENT]: STATIC_EXCLUSIONS.PATENT,
        }
    },
    [CONSTANTS.CLAIMS]: {
        [CONSTANTS.BUILDINGS_CLAIMS]: {
            [CONSTANTS.MAKE_A_CLAIM]: '<p>Cover for loss, theft or damage to your building, contents, pedal cycles, collections listed in your policy schedule and personal injuries and liabilites.</p><p><p><strong>Telephone</strong></p><p>+44 (0)20 3319 7909</p></p><p><p><strong>Email</strong></p><p>smartclaims@azuruw.com</p></p><p><p><strong>In writing</strong></p><p>Claims Department, Azur Underwriting Limited,<br />Linen Court, 10 East Road, London, N1 6AD.</p></p>',
            [CONSTANTS.MAKING_A_CLAIM]: {
                [CONSTANTS.CLAIM_NAMES.NOTIFY_US]: STATIC_CLAIMS.NOTIFY_US,
                [CONSTANTS.CLAIM_NAMES.LATE_NOTIFICATIONS]: STATIC_CLAIMS.LATE_NOTIFICATIONS,
                [CONSTANTS.CLAIM_NAMES.DO_NOT_ADMIT_RESPONSIBILITY]: STATIC_CLAIMS.DO_NOT_ADMIT_RESPONSIBILITY,
                [CONSTANTS.CLAIM_NAMES.CORRESPONDENCE]: STATIC_CLAIMS.CORRESPONDENCE,
                [CONSTANTS.CLAIM_NAMES.SETTLING_CLAIMS]: STATIC_CLAIMS.SETTLING_CLAIMS,
                [CONSTANTS.CLAIM_NAMES.CAUSING_INJURY_OR_DAMAGE]: STATIC_CLAIMS.CAUSING_INJURY_OR_DAMAGE,
                [CONSTANTS.CLAIM_NAMES.RECOVERY_PROCEEDINGS]: STATIC_CLAIMS.RECOVERY_PROCEEDINGS,
                [CONSTANTS.CLAIM_NAMES.FRAUDULENT_CLAIMS]: STATIC_CLAIMS.FRAUDULENT_CLAIMS
            }
        },
        [CONSTANTS.LIABILITY_CLAIMS]: {
            [CONSTANTS.MAKE_A_CLAIM]: '<p>Cover for loss, theft or damage to your building, contents, pedal cycles, collections listed in your policy schedule and personal injuries and liabilites.</p><p><p><strong>Telephone</strong></p><p>+44 (0)20 3319 7909</p></p><p><p><strong>Email:</strong></p><p>smartclaims@azuruw.com</p></p><p><p><strong>In writing:</strong></p><p>Claims Department, Azur Underwriting Limited,<br />Linen Court, 10 East Road, London, N1 6AD.</p></p>',
            [CONSTANTS.MAKING_A_CLAIM]: {
                [CONSTANTS.CLAIM_NAMES.NOTIFY_US]: STATIC_CLAIMS.NOTIFY_US,
                [CONSTANTS.CLAIM_NAMES.LATE_NOTIFICATIONS]: STATIC_CLAIMS.LATE_NOTIFICATIONS,
                [CONSTANTS.CLAIM_NAMES.DO_NOT_ADMIT_RESPONSIBILITY]: STATIC_CLAIMS.DO_NOT_ADMIT_RESPONSIBILITY,
                [CONSTANTS.CLAIM_NAMES.CORRESPONDENCE]: STATIC_CLAIMS.CORRESPONDENCE,
                [CONSTANTS.CLAIM_NAMES.SETTLING_CLAIMS]: STATIC_CLAIMS.SETTLING_CLAIMS,
                [CONSTANTS.CLAIM_NAMES.CAUSING_INJURY_OR_DAMAGE]: STATIC_CLAIMS.CAUSING_INJURY_OR_DAMAGE,
                [CONSTANTS.CLAIM_NAMES.RECOVERY_PROCEEDINGS]: STATIC_CLAIMS.RECOVERY_PROCEEDINGS,
                [CONSTANTS.CLAIM_NAMES.FRAUDULENT_CLAIMS]: STATIC_CLAIMS.FRAUDULENT_CLAIMS
            }
        },
        [CONSTANTS.CYBER_CLAIMS]: {
            [CONSTANTS.MAKE_A_CLAIM]: '<p>Cover for fraud, telephone hacking, cyber ransom, identity theft assistance, data privacy, transmission of computer viruses and loss of reputation.</p><p><p><strong>Telephone</strong></p><p>+44 (0)20 3319 7909</p></p><p><p><strong>Email</strong></p><p>new.loss@hsbeil.com</p></p><p><p><strong>In writing</strong></p><p>Claims Department, HSB Engineering Insurance Limited, Chancery Place, 50 Brown Street, Manchester, M2 2JT.</p></p>',
            [CONSTANTS.MAKING_A_CLAIM]: {
                [CONSTANTS.SUBHEADING]: 'Please also refer to \'<strong>Reporting a Claim</strong>\', within Conditions under Section IV - Personal Cyber.',
                [CONSTANTS.CLAIM_NAMES.NOTIFY_US]: STATIC_CLAIMS.NOTIFY_US,
                [CONSTANTS.CLAIM_NAMES.LATE_NOTIFICATIONS]: STATIC_CLAIMS.LATE_NOTIFICATIONS,
                [CONSTANTS.CLAIM_NAMES.DO_NOT_ADMIT_RESPONSIBILITY]: STATIC_CLAIMS.DO_NOT_ADMIT_RESPONSIBILITY,
                [CONSTANTS.CLAIM_NAMES.CORRESPONDENCE]: STATIC_CLAIMS.CORRESPONDENCE,
                [CONSTANTS.CLAIM_NAMES.SETTLING_CLAIMS]: STATIC_CLAIMS.SETTLING_CLAIMS,
                [CONSTANTS.CLAIM_NAMES.CAUSING_INJURY_OR_DAMAGE]: STATIC_CLAIMS.CAUSING_INJURY_OR_DAMAGE,
                [CONSTANTS.CLAIM_NAMES.RECOVERY_PROCEEDINGS]: STATIC_CLAIMS.RECOVERY_PROCEEDINGS
            }
        },
        [CONSTANTS.LEGAL_EXPENSES_CLAIMS]: {
            [CONSTANTS.MAKE_A_CLAIM]: '<p>Cover for legal disputes in employment, domestic employment, contracts or property and loss of earnings while attending court.</p><p><p><strong>Telephone:</strong></p><p>+44 (0)117 917 1698</p></p><p><p><strong>Download claims form</strong></p><p><a href="https://www.arag.co.uk/newclaims">www.arag.co.uk/newclaims</a></p></p>',
            [CONSTANTS.MAKING_A_CLAIM]: {
                [CONSTANTS.CLAIM_NAMES.LAWYERS_AND_ACCOUNTANTS]: STATIC_CLAIMS.LAWYERS_AND_ACCOUNTANTS,
                [CONSTANTS.CLAIM_NAMES.NOTIFY_US]: STATIC_CLAIMS.NOTIFY_US,
                [CONSTANTS.CLAIM_NAMES.LATE_NOTIFICATIONS]: STATIC_CLAIMS.LATE_NOTIFICATIONS,
                [CONSTANTS.CLAIM_NAMES.DO_NOT_ADMIT_RESPONSIBILITY]: STATIC_CLAIMS.DO_NOT_ADMIT_RESPONSIBILITY,
                [CONSTANTS.CLAIM_NAMES.CORRESPONDENCE]: STATIC_CLAIMS.CORRESPONDENCE,
                [CONSTANTS.CLAIM_NAMES.SETTLING_CLAIMS]: STATIC_CLAIMS.SETTLING_CLAIMS,
                [CONSTANTS.CLAIM_NAMES.CAUSING_INJURY_OR_DAMAGE]: STATIC_CLAIMS.CAUSING_INJURY_OR_DAMAGE,
                [CONSTANTS.CLAIM_NAMES.RECOVERY_PROCEEDINGS]: STATIC_CLAIMS.RECOVERY_PROCEEDINGS,
                [CONSTANTS.CLAIM_NAMES.FRAUDULENT_CLAIMS]: STATIC_CLAIMS.FRAUDULENT_CLAIMS
            }
        },
        [CONSTANTS.HOME_EMERGENCY_CLAIMS]: {
            [CONSTANTS.MAKE_A_CLAIM]: '<p>Cover for home emergencies which require immediate action in order to prevent damage or further damage to your main home, or alleviate any health risk to you.</p><p><p><strong>Telephone</strong></p><p>+44 (0)330 30 31 851</p></p>',
            [CONSTANTS.MAKING_A_CLAIM]: {
                [CONSTANTS.CLAIM_NAMES.REPORT_TO_EMERGENCY_SERVICES]: STATIC_CLAIMS.REPORT_TO_EMERGENCY_SERVICES,
                [CONSTANTS.CLAIM_NAMES.NOTIFY_US]: STATIC_CLAIMS.NOTIFY_US,
                [CONSTANTS.CLAIM_NAMES.LATE_NOTIFICATIONS]: STATIC_CLAIMS.LATE_NOTIFICATIONS,
                [CONSTANTS.CLAIM_NAMES.DO_NOT_ADMIT_RESPONSIBILITY]: STATIC_CLAIMS.DO_NOT_ADMIT_RESPONSIBILITY,
                [CONSTANTS.CLAIM_NAMES.CORRESPONDENCE]: STATIC_CLAIMS.CORRESPONDENCE,
                [CONSTANTS.CLAIM_NAMES.SETTLING_CLAIMS]: STATIC_CLAIMS.SETTLING_CLAIMS,
                [CONSTANTS.CLAIM_NAMES.CAUSING_INJURY_OR_DAMAGE]: STATIC_CLAIMS.CAUSING_INJURY_OR_DAMAGE,
                [CONSTANTS.CLAIM_NAMES.RECOVERY_PROCEEDINGS]: STATIC_CLAIMS.RECOVERY_PROCEEDINGS,
                [CONSTANTS.CLAIM_NAMES.FRAUDULENT_CLAIMS]: STATIC_CLAIMS.FRAUDULENT_CLAIMS
            }
        }
    }
};

export default STATIC_DATA;