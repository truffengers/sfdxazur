/**
* @name: EWProductDataFactory
* @description: Reusable factory methods for product object
* @author: Steve Loftus sloftus@azuruw.com
* @date: 23/10/2018
* @ 01/02/2019 Roy Lloyd. Updated to include ProductRequirement and VlocityStateTransition
*/
@isTest
public with sharing class EWProductDataFactory {

	public static Product2 createProduct(String name,
	    								 Boolean insertRecord) {

        Product2 product = new Product2();
		product.Name = (name == null) ? 'testProduct' : name;
        
        if (insertRecord) {
			insert product;
		}

        return product;
    }


	public static vlocity_ins__ProductRequirement__c createRule(String prodId, String transitionId, String stateModelVersionId, String conditions, String message){
		vlocity_ins__ProductRequirement__c rule = new vlocity_ins__ProductRequirement__c();
		rule.vlocity_ins__Conditions__c = conditions;
		rule.vlocity_ins__Message__c = message;
		rule.vlocity_ins__Product2Id__c = prodId;
		rule.vlocity_ins__StateTransitionId__c = transitionId;
		rule.vlocity_ins__StateModelVersionId__c = stateModelVersionId;
		rule.vlocity_ins__IsActive__c = true;
		return rule;
	}

	public static vlocity_ins__VlocityStateTransition__c createStateTransition(String n, String stateModelId){
		return new vlocity_ins__VlocityStateTransition__c(Name  = n, vlocity_ins__StateModelVersionId__c = stateModelId);
	}


	public static vlocity_ins__VlocityStateModel__c createStateModel(String n){
		return new vlocity_ins__VlocityStateModel__c(Name = n);
	}

	public static vlocity_ins__VlocityStateModelVersion__c createStateModelVerion(String stateModelId, String stateModelName, Integer version){
		return new vlocity_ins__VlocityStateModelVersion__c(Name = stateModelName + ' V'+version, vlocity_ins__StateModelId__c = stateModelId, vlocity_ins__VersionNumber__c = version );
	}

}