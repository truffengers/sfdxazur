@IsTest
private class EWJSONHelperTest {

    public static final String FAKE_JSON_PAYLOAD = '{' +
            '"ambNode": {"message": "OK"},' +
            '"subsNode": {"message": "OK"},' +
            '"outraNode": {"message": "OK"},' +
            '"propertyNode": {"message": "OK"},' +
            '"coverageNode": {"message": "OK"},' +
            '"clientNode": {"message": "OK"}}';

    static testMethod void testPostQuoteJSONBehavior() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        Map<String, Object> payload = (Map<String, Object>) JSON.deserializeUntyped(FAKE_JSON_PAYLOAD);

        EWJSONHelper.postQuoteJSONData(quote.Id, payload);

        quote = [
                SELECT Id, (
                        SELECT Id, EW_ClientNode__c, EW_PropertyNode__c, EW_AmbNode__c, EW_SubsNode__c, EW_OutraNode__c, EW_CoverageNode__c
                        FROM Quote_JSON_Data__r
                )
                FROM Quote
                WHERE Id = :quote.Id
        ];

        System.assert(!quote.Quote_JSON_Data__r.isEmpty());

        EW_QuoteJSONData__c quoteJSONData = quote.Quote_JSON_Data__r.get(0);

        System.assertNotEquals(null, quoteJSONData.EW_ClientNode__c);
        System.assertNotEquals(null, quoteJSONData.EW_PropertyNode__c);
        System.assertNotEquals(null, quoteJSONData.EW_AmbNode__c);
        System.assertNotEquals(null, quoteJSONData.EW_SubsNode__c);
        System.assertNotEquals(null, quoteJSONData.EW_OutraNode__c);
        System.assertNotEquals(null, quoteJSONData.EW_CoverageNode__c);

    }

    static testMethod void testGetQuoteJSONBehavior() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        EW_QuoteJSONData__c quoteJSONData = new EW_QuoteJSONData__c();

        quoteJSONData.EW_ClientNode__c = '{"message": "OK"}';
        quoteJSONData.EW_PropertyNode__c = '{"message": "OK"}';
        quoteJSONData.EW_AmbNode__c = '{"message": "OK"}';
        quoteJSONData.EW_SubsNode__c = '{"message": "OK"}';
        quoteJSONData.EW_OutraNode__c = '{"message": "OK"}';
        quoteJSONData.EW_CoverageNode__c = '{"message": "OK"}';
        quoteJSONData.EW_Quote__c = quote.Id;

        insert quoteJSONData;

        Map<String, Object> payloadRead = EWJSONHelper.getQuoteJSONData(new Map<String, Object> {'recordId' => quote.Id, 'includeCoverageNode' => true});

        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.CLIENT_DETAILS_NODE));
        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.PROPERTY_NODE));
        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.AMB_NODE));
        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.SUBS_NODE));
        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.OUTRA_NODE));
        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.COVERAGE_NODE));
    }

    static testMethod void testGetCoverageJSONBehavior() {
        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, true);

        EW_QuoteJSONData__c quoteJSONData = new EW_QuoteJSONData__c();

        quoteJSONData.EW_ClientNode__c = '{"message": "OK"}';
        quoteJSONData.EW_PropertyNode__c = '{"message": "OK"}';
        quoteJSONData.EW_AmbNode__c = '{"message": "OK"}';
        quoteJSONData.EW_SubsNode__c = '{"message": "OK"}';
        quoteJSONData.EW_OutraNode__c = '{"message": "OK"}';
        quoteJSONData.EW_Quote__c = quote.Id;
        insert quoteJSONData;

        Attachment coverageNode = new Attachment();
        coverageNode.Body = Blob.valueOf('{"message": "OK"}');
        coverageNode.Name = EWJSONHelper.COVERAGE_NODE;
        coverageNode.ParentId = quoteJSONData.Id;
        insert coverageNode;

        Map<String, Object> payloadRead = EWJSONHelper.getQuoteCoverageData(quote.Id);

        System.assertNotEquals(null, payloadRead.get(EWJSONHelper.COVERAGE_NODE));
    }



    static testMethod void testGetJsonValue() {

        Product2 product = EWProductDataFactory.createProduct('EmergingWealth', false);
        product.productCode = 'emergingWealth';
        insert product;
        PricebookEntry pbe = new PricebookEntry (Product2Id = product.Id, Pricebook2ID = Test.getStandardPricebookId(), UnitPrice = 50, isActive = true);
        insert pbe;

        Account broker = EWAccountDataFactory.createBrokerAccount(null, true);
        Account insured = EWAccountDataFactory.createInsuredAccount('Mr', 'Test', 'Insured', Date.newInstance(1969, 12, 29), true);
        Opportunity oppty = EWOpportunityDataFactory.createOpportunity(null, insured.Id, broker.Id, true);
        Quote quote = EWQuoteDataFactory.createQuote(null, oppty.Id, broker.Id, Test.getStandardPricebookId(),  true);
        QuoteLineItem qli = EWQuoteDataFactory.createQuoteLineItem(quote.Id, pbe.Id, product.Id, 100,false);
        qli.vlocity_ins__AttributeSelectedValues__c = '{"doNotAutoRenew":false,"excess":500,"paymentMethod":"Broker Collection","azurCommissionValue":85.1,"iptPercent":12,"brokerCommissionValue":0,"premiumGrossExIPT":709.16,"premiumNet":624.06,"basePremium":1,"brokerPercentMaximum":30,"brokerPercentMinimum":0,"brokerPercentCommission":0,"points":0,"randomNumber":2,"policyAge":0,"campaign":"Base Campaign","quoteDateOfIssue":"2020-02-14T00:00:00.000Z","quoteExpiryDate":"2020-03-15T14:35:42.495Z","azurPercentCommission":12,"quotedDate":"2020-02-12 14:34:40","underwriterAdjustmentPercent":0,"totalPercentCommission":0.12,"totalCommission":1.13636,"quoteTransactionType":"Renewal","premiumGrossIncIPT":794.26,"jointPolicyCategory":"Not a joint policy","iptTotal":85.1,"fixedExpenseGrossIncIPT":28.64,"coverage":"Building & Contents","claimedInLivePolicy":false,"calculateBuildingsPremium":"true","aigPremiumNet":560.15,"aigPremiumGrossExIPT":636.53,"aigIPTTotal":0,"aigAzurCommissionValue":76.3836}';
        insert qli;

        String target = 'aigPremiumGrossExIPT';

        Map<String, Object> inputMap = new Map<String, Object>();
        Map<String, Object> outputMap = new Map<String, Object>();
        Map<String, Object> optionMap = new Map<String, Object>{
                'quoteId' => quote.Id,
                'productCode' => 'emergingWealth',
                'targets' => new List<String>{target}
        };

        new EWVlocityRemoteActionHelper().invokeMethod('getQuoteQLIvalue', inputMap, outputMap, optionMap);

        System.assertEquals(636.53, (Decimal) outputMap.get('QLI'+target) );

    }

}