({
    afterScriptsLoaded: function (component, event, helper) {
        helper.initTT();
    },
    handleCloseMenu: function(component, event, helper) {
        if(TT.menu && TT.menu.getIsOpen()) {
            TT.menu.openOrClose();
        }
    }
});