/**
* @name: PHO_PhoenixStagingTriggerHandler
* @description: Test class for Phoenix Staging trigger handler
* @author: Unknown
* @lastChangedBy: Antigoni D'Mello  26/01/2021  PR-113: Added carrier and carrier DR Code
*/
@isTest
public with sharing class PHO_PhoenixStagingTriggerHandlerTest {
    private static final String brAccDrCode = 'BAD';
    private static final String caAccDrCode = 'CR0004';
    private static final String mgaAccDrCode = 'MAD';
    private static final String altRef = 'ARF';
    private static final String currencyType = 'GBP';

    @TestSetup
    public static void testSetup() {

        IBA_IBACustomSettings__c ibaCS = new IBA_IBACustomSettings__c(
                PHO_PhoenixAccBrokerAccountDRCode__c = brAccDrCode,
                PHO_PhoenixMGAAccountDRCode__c = mgaAccDrCode
        );

        insert ibaCS;

        Account prodBrokerAcc = TestDataFactory.createBrokerageAccount('TestProducingBrokerAcc');
        prodBrokerAcc.EW_DRCode__c = altRef;
        prodBrokerAcc.c2g__CODAAccountTradingCurrency__c = currencyType;

        Account accountingBrokerAcc = TestDataFactory.createBrokerageAccount('TestAccountingBrokerAcc');
        accountingBrokerAcc.EW_DRCode__c = brAccDrCode;
        accountingBrokerAcc.c2g__CODAAccountTradingCurrency__c = currencyType;

        Account carrAcc = TestDataFactory.createCarrierAccount('TestCarrierAcc1');
        carrAcc.EW_DRCode__c = caAccDrCode;
        carrAcc.c2g__CODAAccountTradingCurrency__c = currencyType;

        Account mgaAcc = TestDataFactory.createBrokerageAccount('TestMGAAcc1');
        mgaAcc.EW_DRCode__c = mgaAccDrCode;
        mgaAcc.c2g__CODAAccountTradingCurrency__c = currencyType;

        insert new List<Account>{
                prodBrokerAcc, accountingBrokerAcc, carrAcc, mgaAcc
        };

        Account carrierAccount = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Carrier' LIMIT 1];

        Product2 p = new Product2(Name = 'TestProd1', PHO_PhoenixProductCode__c = 'AB',  PHO_PhoenixSectionCode__c = 'CD',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor');
        Product2 p1 = new Product2(Name = 'TestProd2', PHO_PhoenixProductCode__c = 'BA', PHO_PhoenixSectionCode__c = 'DC',
                                    vlocity_ins__CarrierId__c = carrierAccount.Id, IBA_ClassofBusiness__c = 'Motor');
        insert new List<Product2>{p,p1};
    }

    @IsTest
    public static void testPhoFieldMapOnInsert() {
        Map<String, Account> accMap = new Map<String, Account>([
                SELECT Id, EW_DRCode__c, c2g__CODAAccountTradingCurrency__c
                FROM Account
        ]);

        Map<String, Product2> prodMap = new Map<String, Product2>([
                SELECT PHO_PhoenixProductCode__c, PHO_PhoenixSectionCode__c
                FROM Product2
        ]);

        PHO_PhoenixStaging__c phoStg1 = new PHO_PhoenixStaging__c(
                PHO_Woundback__c = false,
                PHO_PaymentMethod__c = 'D',
                PHO_CurrencyType__c = currencyType,
                PHO_AlternateReference__c = altRef,
                PHO_ProductCode__c = 'AB',
                PHO_SectionCode__c = 'CD',
                PHO_CarrierDRCode__c = caAccDrCode
        );

        PHO_PhoenixStaging__c phoStg2 = new PHO_PhoenixStaging__c(
                PHO_Woundback__c = false,
                PHO_CurrencyType__c = currencyType,
                PHO_AlternateReference__c = altRef,
                PHO_ProductCode__c = 'BA',
                PHO_SectionCode__c = 'DC',
                PHO_CarrierDRCode__c = caAccDrCode
        );

        Test.startTest();
        insert new List<PHO_PhoenixStaging__c>{
                phoStg1, phoStg2
        };
        Test.stopTest();

        Map<Id, PHO_PhoenixStaging__c> testPhoMap = new Map<Id, PHO_PhoenixStaging__c>([
                SELECT PHO_AccountingBroker__c, PHO_CarrierAccount__c, PHO_MGAAccount__c,
                        PHO_ProducingBroker__c,PHO_AlternateReference__c, PHO_Product__c,
                        PHO_ProductCode__c, PHO_SectionCode__c
                FROM PHO_PhoenixStaging__c
        ]);

        phoStg1 = testPhoMap.get(phoStg1.Id);
        System.assertEquals(false, String.isBlank(phoStg1.PHO_AccountingBroker__c));
        System.assertEquals(brAccDrCode, accMap.get(phoStg1.PHO_AccountingBroker__c).EW_DRCode__c);
        System.assertEquals(caAccDrCode, accMap.get(phoStg1.PHO_CarrierAccount__c).EW_DRCode__c);
        System.assertEquals(mgaAccDrCode, accMap.get(phoStg1.PHO_MGAAccount__c).EW_DRCode__c);
        System.assertEquals(phoStg1.PHO_AlternateReference__c, accMap.get(phoStg1.PHO_ProducingBroker__c).EW_DRCode__c);
        System.assertEquals(phoStg1.PHO_ProductCode__c, prodMap.get(phoStg1.PHO_Product__c).PHO_PhoenixProductCode__c);
        System.assertEquals(phoStg1.PHO_SectionCode__c, prodMap.get(phoStg1.PHO_Product__c).PHO_PhoenixSectionCode__c);

        phoStg2 = testPhoMap.get(phoStg2.Id);
        System.assertEquals(true, String.isBlank(phoStg2.PHO_AccountingBroker__c));
        System.assertEquals(caAccDrCode, accMap.get(phoStg2.PHO_CarrierAccount__c).EW_DRCode__c);
        System.assertEquals(mgaAccDrCode, accMap.get(phoStg2.PHO_MGAAccount__c).EW_DRCode__c);
        System.assertEquals(phoStg2.PHO_AlternateReference__c, accMap.get(phoStg2.PHO_ProducingBroker__c).EW_DRCode__c);
        System.assertEquals(phoStg2.PHO_ProductCode__c, prodMap.get(phoStg2.PHO_Product__c).PHO_PhoenixProductCode__c);
        System.assertEquals(phoStg2.PHO_SectionCode__c, prodMap.get(phoStg2.PHO_Product__c).PHO_PhoenixSectionCode__c);
    }

    @IsTest
    public static void testPhoFieldMapOnUpdate() {

        // prepare more data
        final String altRef2 = 'ARF2';
        final String currencyType2 = 'USD';

        Account prodBrokerAcc2 = TestDataFactory.createBrokerageAccount('TestProducingBrokerAcc2');
        prodBrokerAcc2.EW_DRCode__c = altRef2;
        prodBrokerAcc2.c2g__CODAAccountTradingCurrency__c = currencyType2;
        prodBrokerAcc2.CurrencyIsoCode = currencyType2;

        Account accountingBrokerAcc2 = TestDataFactory.createBrokerageAccount('TestAccountingBrokerAcc2');
        accountingBrokerAcc2.EW_DRCode__c = brAccDrCode;
        accountingBrokerAcc2.c2g__CODAAccountTradingCurrency__c = currencyType2;
        accountingBrokerAcc2.CurrencyIsoCode = currencyType2;

        Account carrAcc2 = TestDataFactory.createCarrierAccount('TestCarrierAcc2');
        carrAcc2.EW_DRCode__c = caAccDrCode;
        carrAcc2.c2g__CODAAccountTradingCurrency__c = currencyType2;
        carrAcc2.CurrencyIsoCode = currencyType2;

        Account mgaAcc2 = TestDataFactory.createBrokerageAccount('TestMGAAcc2');
        mgaAcc2.EW_DRCode__c = mgaAccDrCode;
        mgaAcc2.c2g__CODAAccountTradingCurrency__c = currencyType2;
        mgaAcc2.CurrencyIsoCode = currencyType2;

        insert new List<Account>{
                prodBrokerAcc2, accountingBrokerAcc2, carrAcc2, mgaAcc2
        };

        // insert initial staging records
        PHO_PhoenixStaging__c phoStg1 = new PHO_PhoenixStaging__c(
                PHO_Woundback__c = false,
                PHO_PaymentMethod__c = 'D',
                PHO_CurrencyType__c = currencyType,
                PHO_AlternateReference__c = altRef,
                PHO_ProductCode__c = 'AB',
                PHO_SectionCode__c = 'CD',
                PHO_CarrierDRCode__c = caAccDrCode
        );

        PHO_PhoenixStaging__c phoStg2 = new PHO_PhoenixStaging__c(
                PHO_Woundback__c = false,
                PHO_CurrencyType__c = currencyType,
                PHO_AlternateReference__c = altRef,
                PHO_ProductCode__c = 'BA',
                PHO_SectionCode__c = 'DC',
                PHO_CarrierDRCode__c = caAccDrCode
        );

        insert new List<PHO_PhoenixStaging__c>{phoStg1, phoStg2};

        PHO_PhoenixStaging__c phoStg2AfterInsert = [
                SELECT PHO_AccountingBroker__c, PHO_CarrierAccount__c, PHO_MGAAccount__c,
                        PHO_ProducingBroker__c, PHO_Product__c
                FROM PHO_PhoenixStaging__c
                WHERE Id = :phoStg2.Id
                LIMIT 1
        ];

        // update initial data
        phoStg1.PHO_CurrencyType__c = currencyType2;
        phoStg1.PHO_AlternateReference__c = altRef2;
        phoStg1.PHO_ProductCode__c = 'BA';
        phoStg1.PHO_SectionCode__c = 'DC';

        phoStg2.PHO_Woundback__c = true;

        Test.startTest();
        update new List<PHO_PhoenixStaging__c>{phoStg1, phoStg2};
        Test.stopTest();

        Map<Id, PHO_PhoenixStaging__c> testPhoMap = new Map<Id, PHO_PhoenixStaging__c>([
                SELECT PHO_AccountingBroker__c, PHO_CarrierAccount__c, PHO_MGAAccount__c,
                        PHO_ProducingBroker__c, PHO_Product__c,
                        PHO_Product__r.PHO_PhoenixProductCode__c
                FROM PHO_PhoenixStaging__c
        ]);

        // this record should be updated
        phoStg1 = testPhoMap.get(phoStg1.Id);
        System.assertEquals(accountingBrokerAcc2.Id, phoStg1.PHO_AccountingBroker__c);
        System.assertEquals(carrAcc2.Id, phoStg1.PHO_CarrierAccount__c);
        System.assertEquals(mgaAcc2.Id, phoStg1.PHO_MGAAccount__c);
        System.assertEquals(prodBrokerAcc2.Id, phoStg1.PHO_ProducingBroker__c);
        System.assertEquals('BA', phoStg1.PHO_Product__r.PHO_PhoenixProductCode__c);

        // this record should not be updated
        PHO_PhoenixStaging__c phoStg2AfterUpdate = testPhoMap.get(phoStg2.Id);
        System.assertEquals(phoStg2AfterInsert.PHO_AccountingBroker__c, phoStg2AfterUpdate.PHO_AccountingBroker__c);
        System.assertEquals(phoStg2AfterInsert.PHO_CarrierAccount__c, phoStg2AfterUpdate.PHO_CarrierAccount__c);
        System.assertEquals(phoStg2AfterInsert.PHO_MGAAccount__c, phoStg2AfterUpdate.PHO_MGAAccount__c);
        System.assertEquals(phoStg2AfterInsert.PHO_ProducingBroker__c, phoStg2AfterUpdate.PHO_ProducingBroker__c);
        System.assertEquals(phoStg2AfterInsert.PHO_Product__c, phoStg2AfterUpdate.PHO_Product__c);
    }

    @IsTest
    public static void testDelete() {
        // test only for coverage
        PHO_PhoenixStaging__c stagingRecord = new PHO_PhoenixStaging__c();
        insert stagingRecord;
        delete stagingRecord;
    }
}